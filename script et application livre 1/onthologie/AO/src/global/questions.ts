import {
	Guard 
	/*ScoreCondition
	Disjunction
	RadioAnswerCondition
	Conjunction
	BoolCondition,*/
	} from './guard';
	/*import { PANDEMIC_TRACKING_IS_ENABLED }, from './custom';*/
export type Question = {
	id: string;
	category: string;
	comment?: string;
	text: string;
	inputType: 'radio' | 'date' | 'checkbox' | 'postal';
	options?: string[] | CheckboxOption[];
	nextQuestionMap?: string | string[];
	scoreMap?: number[];
	guard?: Guard;
 };
export type CheckboxOption = {
	label: string;
	id: string;
};
export const CATEGORIES = {
	PERSONAL: 'personalInfo',
	// CONTACT: 'contact',
	SYMPTOMS: 'symptoms',
	// RESPIRATORY_SYMPTOMS: 'respiratorySymptoms',
	// ILLNESS: 'illnesses',
	// MEDICATION: 'medication',
};
export const NO_XML = 'X';
export const QUESTION = {
	POSTAL_CODE: 'V1',
	AGE: 'P0',
	 ABOVE_65: 'P1',
	 LIVING_SITUATION: 'P2',
	 CARING: 'P3',
 	WORKSPACE: 'P4',
 	CONTACT_DATE: 'CZ',
	OUT_OF_BREATH: 'SB',
	SYMPTOM_DATE: 'SZ',
	 DATA_DONATION: `${NO_XML},1`,
};
export const XML_ORDER = ['V', 'P', 'C', 'S', 'D', 'M'];
export const QUESTIONS: Question[] = [
    {
        category: CATEGORIES.PERSONAL,
        id: 'ARBO',
        text: 'ARBO',
        inputType: 'radio',
        options: [
          'MOCHA-EPI_0000033',
          'MOCHA-EPI_0000034',
          'MOCHA-EPI_0000035',
          'MOCHA-EPI_0000036',
          'MOCHA-EPI_0000037'
      ],
        nextQuestionMap: [
            'MOCHA-EPI_0000033',
            'MOCHA-EPI_0000034',
            'MOCHA-EPI_0000035',
            'MOCHA-EPI_0000036',
            'MOCHA-EPI_0000037'
        ],
       
        scoreMap: [
            1,
            1,
            1,
            1,
            1

        ],
        
    },

{
    category: CATEGORIES.PERSONAL,
    id: "Class ID",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: ""
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA_0300",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Preferred Label"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000056",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "national asthma program"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000162",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "extrinsic asthma with status asthmaticus"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000052",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "passive smoking exposure"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000283",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "intrinsic asthma with status asthmaticus"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000071",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "primary care asthma register"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000062",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "premature/gestational age"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000166",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "recorded/predicted PEFR ratio"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000177",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "lung function"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000025",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "stresses"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000099",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "chronic asthma with fixed airflow obstruction"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000298",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "adrenaline"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000086",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000138",
        "MOCHA-Asthma_000139",
        "MOCHA-Asthma_000137"
    ],
    options: [
        "MOCHA-Asthma_000138",
        "MOCHA-Asthma_000139",
        "MOCHA-Asthma_000137"
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: "asthma monitoring by nurse"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000029",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "shortness of breath"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000289",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "acute asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000172",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma annual review"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000012",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "gender effects"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000026",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "inactive asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000095",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "acute exacerbation of asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000294",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "xanthine bronchodilators"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000043",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "patient has a written asthma personal action plan"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000082",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA_0300",
        "MOCHA-Asthma_000298",
        "MOCHA-Asthma_000289",
        "MOCHA-Asthma_000294",
        "MOCHA-Asthma_000296",
        "MOCHA-Asthma_000290",
        "MOCHA-Asthma_000297",
        "MOCHA-Asthma_000293",
        "MOCHA-Asthma_000292",
        "MOCHA-Asthma_000299",
        "MOCHA-Asthma_000295",
        "MOCHA-Asthma_000291",
        "MOCHA-asthma_000000",
        "MOCHA-Asthma_000300",
        "MOCHA-asthma_000001"
    ],
    options: [
        "MOCHA_0300",
        "MOCHA-Asthma_000298",
        "MOCHA-Asthma_000289",
        "MOCHA-Asthma_000294",
        "MOCHA-Asthma_000296",
        "MOCHA-Asthma_000290",
        "MOCHA-Asthma_000297",
        "MOCHA-Asthma_000293",
        "MOCHA-Asthma_000292",
        "MOCHA-Asthma_000299",
        "MOCHA-Asthma_000295",
        "MOCHA-Asthma_000291",
        "MOCHA-asthma_000000",
        "MOCHA-Asthma_000300",
        "MOCHA-asthma_000001"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "mild intermittent asthma uncomplicated"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000281",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "management"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000017",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "absent from work or school due to asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000013",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "moderate persistent asthma with status asthmaticus"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000188",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "severe persistent asthma with status asthmaticus"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000285",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "weather"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000063",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "International Study of Asthma and Allergies in Childhood (ISAAC) questionnaire"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000160",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "serial peak expiratory flow rate"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000179",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "exercise"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-EPI_0000037",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000082",
        "MOCHA-Asthma_000080",
        "MOCHA-Asthma_000078",
        "MOCHA-Asthma_000081",
        "MOCHA-Asthma_000079",
        "MOCHA-Asthma_000077"
    ],
    options: [
        "MOCHA-Asthma_000082",
        "MOCHA-Asthma_000080",
        "MOCHA-Asthma_000078",
        "MOCHA-Asthma_000081",
        "MOCHA-Asthma_000079",
        "MOCHA-Asthma_000077"
    ],
    scoreMap: [
        1,
        1,
        1,
        1,
        1,
        1
    ],
    text: "respiratory infections"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-EPI_0000033",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-ADHD_000001",
        "MOCHA-ADHD_000002",
        "MOCHA-ADHD_000003",
        "MOCHA-ADHD_000004",
        "MOCHA-ADHD_000000"
    ],
    options: [
        "MOCHA-ADHD_000001",
        "MOCHA-ADHD_000002",
        "MOCHA-ADHD_000003",
        "MOCHA-ADHD_000004",
        "MOCHA-ADHD_000000"
    ],
    scoreMap: [
        1,
        1,
        1,
        1,
        1
    ],
    text: "process of care"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000093",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "diagnosis"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000008",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "short-acting bronchodilator"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000080",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000309",
        "MOCHA-Asthma_000308"
    ],
    options: [
        "MOCHA-Asthma_000309",
        "MOCHA-Asthma_000308"
    ],
    scoreMap: [
        0,
        0
    ],
    text: "radioallergosorbent test (RAST)"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000001",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-ADHD_000029",
        "MOCHA-ADHD_000026",
        "MOCHA-ADHD_000030",
        "MOCHA-ADHD_000027",
        "MOCHA-ADHD_000028"
    ],
    options: [
        "MOCHA-ADHD_000029",
        "MOCHA-ADHD_000026",
        "MOCHA-ADHD_000030",
        "MOCHA-ADHD_000027",
        "MOCHA-ADHD_000028"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0
    ],
    text: "clinical procedure"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000117",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "acute"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000279",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "nedocromil sodium"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000104",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "QOF indicator 2"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000175",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "nebulized ipratropium bromide"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-EPI_0000034",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000086",
        "MOCHA-Asthma_000087",
        "MOCHA-Asthma_000083",
        "MOCHA-Asthma_000085",
        "MOCHA-Asthma_000084",
        "MOCHA-Asthma_000088"
    ],
    options: [
        "MOCHA-Asthma_000086",
        "MOCHA-Asthma_000087",
        "MOCHA-Asthma_000083",
        "MOCHA-Asthma_000085",
        "MOCHA-Asthma_000084",
        "MOCHA-Asthma_000088"
    ],
    scoreMap: [
        1,
        1,
        1,
        1,
        1,
        1
    ],
    text: "maternal asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000296",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "symptoms"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000108",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "attends asthma monitoring"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000307",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "short and long acting beta agonists"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000087",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000136",
        "MOCHA-Asthma_000135"
    ],
    options: [
        "MOCHA-Asthma_000136",
        "MOCHA-Asthma_000135"
    ],
    scoreMap: [
        0,
        0
    ],
    text: "asthma control step 0"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000057",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "cough"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000074",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000012",
        "MOCHA-Asthma_000013"
    ],
    options: [
        "MOCHA-Asthma_000012",
        "MOCHA-Asthma_000013"
    ],
    scoreMap: [
        0,
        0
    ],
    text: "extrinsic asthma without status asthmaticus"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000065",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000071",
        "MOCHA-Asthma_000166",
        "MOCHA-Asthma_000167",
        "MOCHA-Asthma_000169",
        "MOCHA-Asthma_000165",
        "MOCHA-Asthma_000168",
        "MOCHA-Asthma_000164"
    ],
    options: [
        "MOCHA-Asthma_000071",
        "MOCHA-Asthma_000166",
        "MOCHA-Asthma_000167",
        "MOCHA-Asthma_000169",
        "MOCHA-Asthma_000165",
        "MOCHA-Asthma_000168",
        "MOCHA-Asthma_000164"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "fractional exhaled nitric oxide (FeNO) test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000091",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000117",
        "MOCHA-Asthma_000115",
        "MOCHA-Asthma_000119",
        "MOCHA-Asthma_000113",
        "MOCHA-Asthma_000111",
        "MOCHA-Asthma_000118",
        "MOCHA-Asthma_000109",
        "MOCHA-Asthma_000126",
        "MOCHA-Asthma_000123",
        "MOCHA-Asthma_000114",
        "MOCHA-Asthma_000110",
        "MOCHA-Asthma_000125",
        "MOCHA-Asthma_000112",
        "MOCHA-Asthma_000121",
        "MOCHA-Asthma_000124",
        "MOCHA-Asthma_000120",
        "MOCHA-Asthma_000122"
    ],
    options: [
        "MOCHA-Asthma_000117",
        "MOCHA-Asthma_000115",
        "MOCHA-Asthma_000119",
        "MOCHA-Asthma_000113",
        "MOCHA-Asthma_000111",
        "MOCHA-Asthma_000118",
        "MOCHA-Asthma_000109",
        "MOCHA-Asthma_000126",
        "MOCHA-Asthma_000123",
        "MOCHA-Asthma_000114",
        "MOCHA-Asthma_000110",
        "MOCHA-Asthma_000125",
        "MOCHA-Asthma_000112",
        "MOCHA-Asthma_000121",
        "MOCHA-Asthma_000124",
        "MOCHA-Asthma_000120",
        "MOCHA-Asthma_000122"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "host"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000173",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "controller"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000078",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000279",
        "MOCHA-Asthma_000280",
        "MOCHA-Asthma_000278"
    ],
    options: [
        "MOCHA-Asthma_000279",
        "MOCHA-Asthma_000280",
        "MOCHA-Asthma_000278"
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: "parent-of-origin/ethnicity effects"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000115",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "QOF"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000083",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000310"
    ],
    options: [
        "MOCHA-Asthma_000310"
    ],
    scoreMap: [
        0
    ],
    text: "theophyllines"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000186",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "other"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000128",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "occupational exposure"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000061",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "severe wheeze"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000119",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "predict PEFR using EN13826 std"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000106",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "sodium chromoglycate"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000040",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "anticholinergic bronchodilators: ipratropium, tiotroprium, aclidinum"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000098",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "other and unspecified asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000290",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "epinephrine"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000297",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma monitoring due"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000085",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000149",
        "MOCHA-Asthma_000143",
        "MOCHA-Asthma_000147",
        "MOCHA-Asthma_000145",
        "MOCHA-Asthma_000141",
        "MOCHA-Asthma_000144",
        "MOCHA-Asthma_000148",
        "MOCHA-Asthma_000140",
        "MOCHA-Asthma_000146",
        "MOCHA-Asthma_000142"
    ],
    options: [
        "MOCHA-Asthma_000149",
        "MOCHA-Asthma_000143",
        "MOCHA-Asthma_000147",
        "MOCHA-Asthma_000145",
        "MOCHA-Asthma_000141",
        "MOCHA-Asthma_000144",
        "MOCHA-Asthma_000148",
        "MOCHA-Asthma_000140",
        "MOCHA-Asthma_000146",
        "MOCHA-Asthma_000142"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "asthma follow-up"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000044",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "airway inflammation/limitation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000309",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mild intermittent asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000184",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "emergency asthma admission since last appointment"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000089",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000092"
    ],
    options: [
        "MOCHA-Asthma_000092"
    ],
    scoreMap: [
        0
    ],
    text: "pharmacological agents such as histamine or metacholine"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000288",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "other"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000171",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Prevention and Incidence of Asthma and Mite Allergy (PIAMA) risk score"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000014",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "gene-gene interactions"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000010",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "severe persistent asthma with (acute) exacerbation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000076",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000010",
        "MOCHA-Asthma_000011"
    ],
    options: [
        "MOCHA-Asthma_000010",
        "MOCHA-Asthma_000011"
    ],
    scoreMap: [
        0,
        0
    ],
    text: "non-allergic asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000293",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "other"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000094",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "step down change in asthma management plan"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000041",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "muscarinic antagonists"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000009",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mild intermittent asthma with status asthmaticus"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000081",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000307",
        "MOCHA-Asthma_000305",
        "MOCHA-Asthma_000303",
        "MOCHA-Asthma_000304",
        "MOCHA-Asthma_000302",
        "MOCHA-Asthma_000306"
    ],
    options: [
        "MOCHA-Asthma_000307",
        "MOCHA-Asthma_000305",
        "MOCHA-Asthma_000303",
        "MOCHA-Asthma_000304",
        "MOCHA-Asthma_000302",
        "MOCHA-Asthma_000306"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "atopy"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000305",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "control"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000180",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma control step 2"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000002",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-ADHD_000025",
        "MOCHA-ADHD_000017",
        "MOCHA-ADHD_000013",
        "MOCHA-ADHD_000014",
        "MOCHA-ADHD_000018",
        "MOCHA-ADHD_000019",
        "MOCHA-ADHD_000015",
        "MOCHA-ADHD_000016",
        "MOCHA-ADHD_000022",
        "MOCHA-ADHD_000023",
        "MOCHA-ADHD_000024",
        "MOCHA-ADHD_000020",
        "MOCHA-ADHD_000021"
    ],
    options: [
        "MOCHA-ADHD_000025",
        "MOCHA-ADHD_000017",
        "MOCHA-ADHD_000013",
        "MOCHA-ADHD_000014",
        "MOCHA-ADHD_000018",
        "MOCHA-ADHD_000019",
        "MOCHA-ADHD_000015",
        "MOCHA-ADHD_000016",
        "MOCHA-ADHD_000022",
        "MOCHA-ADHD_000023",
        "MOCHA-ADHD_000024",
        "MOCHA-ADHD_000020",
        "MOCHA-ADHD_000021"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "gut colonization"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000284",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "chronic"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000072",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000000"
    ],
    options: [
        "MOCHA-Asthma_000000"
    ],
    scoreMap: [
        0
    ],
    text: "asthma screening questionnaires"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000011",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "airways responsiveness test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000113",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma - currently dormant"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000100",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Cromolyn"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000007",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "aminophylline"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000048",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "IgE antibody allergy test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000292",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "occasional asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000299",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "step up change in asthma management plan"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000303",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma monitoring by doctor"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-EPI_0000035",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000074",
        "MOCHA-Asthma_000076",
        "MOCHA-Asthma_000072",
        "MOCHA-Asthma_000014",
        "MOCHA-Asthma_000073",
        "MOCHA-Asthma_000075"
    ],
    options: [
        "MOCHA-Asthma_000074",
        "MOCHA-Asthma_000076",
        "MOCHA-Asthma_000072",
        "MOCHA-Asthma_000014",
        "MOCHA-Asthma_000073",
        "MOCHA-Asthma_000075"
    ],
    scoreMap: [
        1,
        1,
        1,
        1,
        1,
        1
    ],
    text: "asthma control step 4"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000092",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "history and investigations"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000295",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "adjuncts used to administer therapies such as spacer devices"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000096",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma medication review"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000107",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "compound bronchodilators"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000049",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "beta agonist"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000045",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma NOS"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000282",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "severe asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000003",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "number of days absent from school due to wheeze in past 6 months"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000111",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "pollen allergy test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000182",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Albuterol"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000027",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "outdoor air quality"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000286",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV1/FVC ratio abnormal"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000005",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Asthma Prediction Index (API)"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000102",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "pet allergy test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000018",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Heliox"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000018",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV1 pre steroids"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000185",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "moderate persistent asthma with (acute) exacerbation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000023",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "medications"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000127",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV1 reversibility"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000105",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mild wheeze"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000019",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "anticholinergics"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000015",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "moderate persistent asthma uncomplicated"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000014",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000027",
        "MOCHA-Asthma_000018",
        "MOCHA-Asthma_000023",
        "MOCHA-Asthma_000038",
        "MOCHA-Asthma_000029",
        "MOCHA-Asthma_000034",
        "MOCHA-Asthma_000025",
        "MOCHA-Asthma_000016",
        "MOCHA-Asthma_000019",
        "MOCHA-Asthma_000036",
        "MOCHA-Asthma_000017",
        "MOCHA-Asthma_000030",
        "MOCHA-Asthma_000028",
        "MOCHA-Asthma_000021",
        "MOCHA-Asthma_000033",
        "MOCHA-Asthma_000037",
        "MOCHA-Asthma_000024",
        "MOCHA-Asthma_000015",
        "MOCHA-Asthma_000032",
        "MOCHA-Asthma_000039",
        "MOCHA-Asthma_000035",
        "MOCHA-Asthma_000026",
        "MOCHA-Asthma_000022",
        "MOCHA-Asthma_000020",
        "MOCHA-Asthma_000031"
    ],
    options: [
        "MOCHA-Asthma_000027",
        "MOCHA-Asthma_000018",
        "MOCHA-Asthma_000023",
        "MOCHA-Asthma_000038",
        "MOCHA-Asthma_000029",
        "MOCHA-Asthma_000034",
        "MOCHA-Asthma_000025",
        "MOCHA-Asthma_000016",
        "MOCHA-Asthma_000019",
        "MOCHA-Asthma_000036",
        "MOCHA-Asthma_000017",
        "MOCHA-Asthma_000030",
        "MOCHA-Asthma_000028",
        "MOCHA-Asthma_000021",
        "MOCHA-Asthma_000033",
        "MOCHA-Asthma_000037",
        "MOCHA-Asthma_000024",
        "MOCHA-Asthma_000015",
        "MOCHA-Asthma_000032",
        "MOCHA-Asthma_000039",
        "MOCHA-Asthma_000035",
        "MOCHA-Asthma_000026",
        "MOCHA-Asthma_000022",
        "MOCHA-Asthma_000020",
        "MOCHA-Asthma_000031"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "severe persistent asthma uncomplicated"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000032",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "spirometry"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000118",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "hay fever with asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000001",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "ketotifen"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000038",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "dust mite allergy test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-EPI_0000036",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000091",
        "MOCHA-Asthma_000089",
        "MOCHA-Asthma_000090"
    ],
    options: [
        "MOCHA-Asthma_000091",
        "MOCHA-Asthma_000089",
        "MOCHA-Asthma_000090"
    ],
    scoreMap: [
        1,
        1,
        1
    ],
    text: "reversibility to salbutamol"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000109",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "therapy"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000308",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "long-acting bronchodiltator"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000090",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000099",
        "MOCHA-Asthma_000095",
        "MOCHA-Asthma_000093",
        "MOCHA-Asthma_000104",
        "MOCHA-Asthma_000108",
        "MOCHA-Asthma_000106",
        "MOCHA-Asthma_000098",
        "MOCHA-Asthma_000094",
        "MOCHA-Asthma_000100",
        "MOCHA-Asthma_000096",
        "MOCHA-Asthma_000107",
        "MOCHA-Asthma_000102",
        "MOCHA-Asthma_000105",
        "MOCHA-Asthma_000103",
        "MOCHA-Asthma_000101",
        "MOCHA-Asthma_000097"
    ],
    options: [
        "MOCHA-Asthma_000099",
        "MOCHA-Asthma_000095",
        "MOCHA-Asthma_000093",
        "MOCHA-Asthma_000104",
        "MOCHA-Asthma_000108",
        "MOCHA-Asthma_000106",
        "MOCHA-Asthma_000098",
        "MOCHA-Asthma_000094",
        "MOCHA-Asthma_000100",
        "MOCHA-Asthma_000096",
        "MOCHA-Asthma_000107",
        "MOCHA-Asthma_000102",
        "MOCHA-Asthma_000105",
        "MOCHA-Asthma_000103",
        "MOCHA-Asthma_000101",
        "MOCHA-Asthma_000097"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "asthma prophylactic medication used"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000029",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "reliever"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000280",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV1/FVC ratio before bronchodilation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000007",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "QOF indicator 3"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000003",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-ADHD_000012",
        "MOCHA-ADHD_000011"
    ],
    options: [
        "MOCHA-ADHD_000012",
        "MOCHA-ADHD_000011"
    ],
    scoreMap: [
        0,
        0
    ],
    text: "asthma with fixed airflow limitation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000183",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "inactive"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000126",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "indoor air quality"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000138",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "corticosteroids"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000304",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "breathlessness"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000047",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma control step 3"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000034",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FVC after bronchodilation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000025",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "positive reversibility to salbutamol"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000012",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV1/FVC percent"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000129",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "fractional exhaled nitric oxide (FENO) test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000004",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-ADHD_000010",
        "MOCHA-ADHD_000007",
        "MOCHA-ADHD_000008",
        "MOCHA-ADHD_000009",
        "MOCHA-ADHD_000005",
        "MOCHA-ADHD_000006"
    ],
    options: [
        "MOCHA-ADHD_000010",
        "MOCHA-ADHD_000007",
        "MOCHA-ADHD_000008",
        "MOCHA-ADHD_000009",
        "MOCHA-ADHD_000005",
        "MOCHA-ADHD_000006"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "moderate wheeze"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000000",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-ADHD_000056",
        "MOCHA-ADHD_000052",
        "MOCHA-ADHD_000043",
        "MOCHA-ADHD_000057",
        "MOCHA-ADHD_000040",
        "MOCHA-ADHD_000044",
        "MOCHA-ADHD_000041",
        "MOCHA-ADHD_000048",
        "MOCHA-ADHD_000049",
        "MOCHA-ADHD_000045",
        "MOCHA-ADHD_000032",
        "MOCHA-ADHD_000046",
        "MOCHA-ADHD_000042",
        "MOCHA-ADHD_000060",
        "MOCHA-ADHD_000047",
        "MOCHA-ADHD_000033",
        "MOCHA-ADHD_000034",
        "MOCHA-ADHD_000038",
        "MOCHA-ADHD_000039",
        "MOCHA-ADHD_000035",
        "MOCHA-ADHD_000053",
        "MOCHA-ADHD_000031",
        "MOCHA-ADHD_000058",
        "MOCHA-ADHD_000036",
        "MOCHA-ADHD_000054",
        "MOCHA-ADHD_000050",
        "MOCHA-ADHD_000037",
        "MOCHA-ADHD_000051",
        "MOCHA-ADHD_000059",
        "MOCHA-ADHD_000055"
    ],
    options: [
        "MOCHA-ADHD_000056",
        "MOCHA-ADHD_000052",
        "MOCHA-ADHD_000043",
        "MOCHA-ADHD_000057",
        "MOCHA-ADHD_000040",
        "MOCHA-ADHD_000044",
        "MOCHA-ADHD_000041",
        "MOCHA-ADHD_000048",
        "MOCHA-ADHD_000049",
        "MOCHA-ADHD_000045",
        "MOCHA-ADHD_000032",
        "MOCHA-ADHD_000046",
        "MOCHA-ADHD_000042",
        "MOCHA-ADHD_000060",
        "MOCHA-ADHD_000047",
        "MOCHA-ADHD_000033",
        "MOCHA-ADHD_000034",
        "MOCHA-ADHD_000038",
        "MOCHA-ADHD_000039",
        "MOCHA-ADHD_000035",
        "MOCHA-ADHD_000053",
        "MOCHA-ADHD_000031",
        "MOCHA-ADHD_000058",
        "MOCHA-ADHD_000036",
        "MOCHA-ADHD_000054",
        "MOCHA-ADHD_000050",
        "MOCHA-ADHD_000037",
        "MOCHA-ADHD_000051",
        "MOCHA-ADHD_000059",
        "MOCHA-ADHD_000055"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "specific"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000016",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "general"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000291",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "post bronchodilator spirometry"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000103",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "change in asthma management plan"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-asthma_000000",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "inspired oxygen"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000063",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000162",
        "MOCHA-Asthma_000177",
        "MOCHA-Asthma_000188",
        "MOCHA-Asthma_000160",
        "MOCHA-Asthma_000179",
        "MOCHA-Asthma_000186",
        "MOCHA-Asthma_000184",
        "MOCHA-Asthma_000180",
        "MOCHA-Asthma_000182",
        "MOCHA-Asthma_000185",
        "MOCHA-Asthma_000183",
        "MOCHA-Asthma_000190",
        "MOCHA-Asthma_000181",
        "MOCHA-Asthma_000163",
        "MOCHA-Asthma_000187",
        "MOCHA-Asthma_000178",
        "MOCHA-Asthma_000161",
        "MOCHA-Asthma_000189",
        "MOCHA-Asthma_000159"
    ],
    options: [
        "MOCHA-Asthma_000162",
        "MOCHA-Asthma_000177",
        "MOCHA-Asthma_000188",
        "MOCHA-Asthma_000160",
        "MOCHA-Asthma_000179",
        "MOCHA-Asthma_000186",
        "MOCHA-Asthma_000184",
        "MOCHA-Asthma_000180",
        "MOCHA-Asthma_000182",
        "MOCHA-Asthma_000185",
        "MOCHA-Asthma_000183",
        "MOCHA-Asthma_000190",
        "MOCHA-Asthma_000181",
        "MOCHA-Asthma_000163",
        "MOCHA-Asthma_000187",
        "MOCHA-Asthma_000178",
        "MOCHA-Asthma_000161",
        "MOCHA-Asthma_000189",
        "MOCHA-Asthma_000159"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "guidelines for management and treatment"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000046",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "environmental"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000042",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "moderate asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000058",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mild intermittent asthma with (acute) exacerbation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000302",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "percentage of PEFR variability"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000045",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma control step 5"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000019",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "expected peak expiratory flow rate"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000149",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "airway obstruction reversible"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000036",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "bronchoconstriction"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000190",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "reversibility to corticosteroids"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000306",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "parental smoking"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000064",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000172",
        "MOCHA-Asthma_000175",
        "MOCHA-Asthma_000173",
        "MOCHA-Asthma_000171",
        "MOCHA-Asthma_000176",
        "MOCHA-Asthma_000170",
        "MOCHA-Asthma_000174"
    ],
    options: [
        "MOCHA-Asthma_000172",
        "MOCHA-Asthma_000175",
        "MOCHA-Asthma_000173",
        "MOCHA-Asthma_000171",
        "MOCHA-Asthma_000176",
        "MOCHA-Asthma_000170",
        "MOCHA-Asthma_000174"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "asthma control step 1"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000060",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "genetic"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000049",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000047",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "more than 80% of predicted peak expiratory flow rate"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000008",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mild asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000123",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma with obesity"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000181",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "inhaled corticosteriods"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000101",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "nutrition"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000033",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "magnesium sulfate"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000016",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "status asthmaticus NOS"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000010",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "severe persistent asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000017",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "phlem sample"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000114",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV (forced expiratory volume) 1 after bronchodilation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000022",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Salmeterol"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000009",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mild persistent asthma with (acute) exacerbation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000056",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "late-onset asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000005",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "PEFR using EN 13826 device"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000013",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma in pregnancy"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000034",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "exhaled nitric oxide test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000030",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma - currently active"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000143",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "difficult asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000110",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "variable reversible airflow obstruction"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000134",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Levalbuterol"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000300",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "inspiratory wheeze"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000043",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma monitoring check done"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000030",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "best ever peak expiratory rate"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000004",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV1/FVC ratio post steroids"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000147",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "food allergy test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000028",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "spasmodic contraction of the bronchi"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000021",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV1/FVC ratio after bronchdilation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000125",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV1 before bronchodilation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-asthma_000001",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "low-dose inhaled corticosteriods (ICS)"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000112",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "formal procedure for reporting adherence to guidelines"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000000",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mast cell stabilizer"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000033",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "airways responsiveness test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000130",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "positive reversibility to corticosteroids"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000167",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "very severe wheeze"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000050",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "sex effects"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000121",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "peak expiratory flow rate flow rate: PEFR/PFR"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000037",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Montelukast"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000024",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "reversibility to ipratropium"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000054",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV1/FVC < 70% of predicted"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000015",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "PEFR post steroids"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000041",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "spirometry"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000038",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "diurinal variation >20% for 3 days in a week over several week"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000158",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "unspecified asthma uncomplicated"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000002",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "cockroach"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000145",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mould allergy test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000136",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "bronchial hyperresponsiveness"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000061",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000065",
        "MOCHA-ADHD_000063",
        "MOCHA-ADHD_000064",
        "MOCHA-ADHD_000062"
    ],
    options: [
        "MOCHA-Asthma_000065",
        "MOCHA-ADHD_000063",
        "MOCHA-ADHD_000064",
        "MOCHA-ADHD_000062"
    ],
    scoreMap: [
        1,
        1,
        1,
        1
    ],
    text: "nocturnal cough"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000032",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "risk factors"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000039",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "peak flow meter"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000006",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "spirometry screening"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000011",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "skin prick test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000154",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mucus sample"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000141",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mould"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000039",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "airway hyper responsiveness to direct or indirect stimuli"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000035",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "unspecified asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000057",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "allergic asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000132",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "percentage of best ever PEFR"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000048",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "nocturnal wheeze"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000053",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "less than 60% of predicted peak expiratory flow rate"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000035",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "intrinsic asthma without status asthmaticus"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000139",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "reversibility positive"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000027",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "difficulty breathing"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000026",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "brittle asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000169",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV1/FVC ratio"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000023",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "maternal smoking during pregnancy"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000052",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mild persistent asthma uncomplicated"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000156",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "PEFR after exercise"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000006",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "food"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000031",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "occupational asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000165",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "atopic asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000152",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "airway inflammation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000024",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "dust mite"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000020",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mild persistent asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000022",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "moderate persistent asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000059",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV1 post steroids"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000046",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "PFR - after bronchodilation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000124",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "forced vital capacity - FVC"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000150",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Omalizumab"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000137",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "chest tightness"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000020",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "shortness of breath"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000163",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "FEV1 after change bronchodilator"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000079",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000285",
        "MOCHA-Asthma_000288",
        "MOCHA-Asthma_000284",
        "MOCHA-Asthma_000286",
        "MOCHA-Asthma_000287"
    ],
    options: [
        "MOCHA-Asthma_000285",
        "MOCHA-Asthma_000288",
        "MOCHA-Asthma_000284",
        "MOCHA-Asthma_000286",
        "MOCHA-Asthma_000287"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0
    ],
    text: "active smoking"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000187",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "screening"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000058",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "chemicals"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000178",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "extrinsic (atopic) asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000062",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000158",
        "MOCHA-Asthma_000154",
        "MOCHA-Asthma_000156",
        "MOCHA-Asthma_000152",
        "MOCHA-Asthma_000157",
        "MOCHA-Asthma_000153",
        "MOCHA-Asthma_000155"
    ],
    options: [
        "MOCHA-Asthma_000158",
        "MOCHA-Asthma_000154",
        "MOCHA-Asthma_000156",
        "MOCHA-Asthma_000152",
        "MOCHA-Asthma_000157",
        "MOCHA-Asthma_000153",
        "MOCHA-Asthma_000155"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "psychosocial environment"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000120",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "allergens"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000157",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Zafirlukast"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000040",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "pet/animal"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000036",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "PEFR increases by at least 20% in response to asthma treatment"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000144",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "unspecified asthma with status asthmaticus"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000310",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "expiratory airflow limitation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000053",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "nocturnal awakening with at least once a week with dyspnea and/or cough and/or wheezing in the absence of other medical conditions known to cause these symptoms"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000044",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "PEFR monitoring"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000077",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000283",
        "MOCHA-Asthma_000281",
        "MOCHA-Asthma_000282"
    ],
    options: [
        "MOCHA-Asthma_000283",
        "MOCHA-Asthma_000281",
        "MOCHA-Asthma_000282"
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: "diurnal variation of PEFR"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000031",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "other"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000054",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "obstructive airway pattern FEV1/FVC <0.7 on spirometry"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000148",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "intrinsic asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000050",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "paroxysmal dyspnea"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000135",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mixed asthma"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000161",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "cough"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000140",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "high BMI"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000037",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "airway inflammation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000028",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "unspecified asthma with (acute) exacerbation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000153",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma attack"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000073",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000062",
        "MOCHA-Asthma_000063",
        "MOCHA-Asthma_000061",
        "MOCHA-Asthma_000047",
        "MOCHA-Asthma_000058",
        "MOCHA-Asthma_000045",
        "MOCHA-Asthma_000049",
        "MOCHA-Asthma_000056",
        "MOCHA-Asthma_000043",
        "MOCHA-Asthma_000050",
        "MOCHA-Asthma_000054",
        "MOCHA-Asthma_000041",
        "MOCHA-Asthma_000057",
        "MOCHA-Asthma_000048",
        "MOCHA-Asthma_000052",
        "MOCHA-Asthma_000059",
        "MOCHA-Asthma_000046",
        "MOCHA-Asthma_000040",
        "MOCHA-Asthma_000053",
        "MOCHA-Asthma_000044",
        "MOCHA-Asthma_000064",
        "MOCHA-Asthma_000051",
        "MOCHA-Asthma_000042",
        "MOCHA-Asthma_000055",
        "MOCHA-Asthma_000060"
    ],
    options: [
        "MOCHA-Asthma_000062",
        "MOCHA-Asthma_000063",
        "MOCHA-Asthma_000061",
        "MOCHA-Asthma_000047",
        "MOCHA-Asthma_000058",
        "MOCHA-Asthma_000045",
        "MOCHA-Asthma_000049",
        "MOCHA-Asthma_000056",
        "MOCHA-Asthma_000043",
        "MOCHA-Asthma_000050",
        "MOCHA-Asthma_000054",
        "MOCHA-Asthma_000041",
        "MOCHA-Asthma_000057",
        "MOCHA-Asthma_000048",
        "MOCHA-Asthma_000052",
        "MOCHA-Asthma_000059",
        "MOCHA-Asthma_000046",
        "MOCHA-Asthma_000040",
        "MOCHA-Asthma_000053",
        "MOCHA-Asthma_000044",
        "MOCHA-Asthma_000064",
        "MOCHA-Asthma_000051",
        "MOCHA-Asthma_000042",
        "MOCHA-Asthma_000055",
        "MOCHA-Asthma_000060"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "house dust"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000176",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "peak expiratory flow test (PEFR)"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000131",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "parental atopy"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000189",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "expiratory wheeze"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000051",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "geography/place of birth"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000122",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "intrinsic asthma NOS"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000064",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Leukotriene receptor antagonists (LTRA)"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000051",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "serial PEFR abnormal"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000021",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "peak monitoring using diary"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000168",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "mild persistent asthma with status asthmaticus"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000159",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "family size/birth order/number of siblings"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000042",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "cold air"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000146",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "60-80% of predicted peak expiratory flow rate"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000055",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "obstructive airway pattern (reversible)"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000060",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "PEFR pre steroids"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000164",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "PFR - before bronchodilation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000151",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "immunity"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000155",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "tight chest"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000142",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "pollen"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000097",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "chronic airway inflammation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000084",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000150",
        "MOCHA-Asthma_000151"
    ],
    options: [
        "MOCHA-Asthma_000150",
        "MOCHA-Asthma_000151"
    ],
    scoreMap: [
        0,
        0
    ],
    text: "ventilation"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000059",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "chest tightness"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-ADHD_000055",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "asthma unspecified"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000075",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000008",
        "MOCHA-Asthma_000009",
        "MOCHA-Asthma_000007",
        "MOCHA-Asthma_000003",
        "MOCHA-Asthma_000005",
        "MOCHA-Asthma_000001",
        "MOCHA-Asthma_000004",
        "MOCHA-Asthma_000002",
        "MOCHA-Asthma_000006"
    ],
    options: [
        "MOCHA-Asthma_000008",
        "MOCHA-Asthma_000009",
        "MOCHA-Asthma_000007",
        "MOCHA-Asthma_000003",
        "MOCHA-Asthma_000005",
        "MOCHA-Asthma_000001",
        "MOCHA-Asthma_000004",
        "MOCHA-Asthma_000002",
        "MOCHA-Asthma_000006"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "extrinsic asthma NOS"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000088",
    inputType: "radio",
    nextQuestionMap: [
        "MOCHA-Asthma_000128",
        "MOCHA-Asthma_000127",
        "MOCHA-Asthma_000129",
        "MOCHA-Asthma_000134",
        "MOCHA-Asthma_000130",
        "MOCHA-Asthma_000132",
        "MOCHA-Asthma_000131"
    ],
    options: [
        "MOCHA-Asthma_000128",
        "MOCHA-Asthma_000127",
        "MOCHA-Asthma_000129",
        "MOCHA-Asthma_000134",
        "MOCHA-Asthma_000130",
        "MOCHA-Asthma_000132",
        "MOCHA-Asthma_000131"
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: "allergy test"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000287",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "wheeze"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000170",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "Isle of Wright score"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000174",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "gene-environment interactions"
},{
    category: CATEGORIES.PERSONAL,
    id: "MOCHA-Asthma_000278",
    inputType: "radio",
    nextQuestionMap: ['ARBO'],
    options: ['ARBO'],
    scoreMap: [0],
    text: "epigenetics"
},]