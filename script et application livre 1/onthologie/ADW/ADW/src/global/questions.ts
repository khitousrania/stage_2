import {
	Guard 
	/*ScoreCondition
	Disjunction
	RadioAnswerCondition
	Conjunction
	BoolCondition,*/
	} from './guard';
	/*import { PANDEMIC_TRACKING_IS_ENABLED } from './custom';*/
export type Question = {
	id: string;
	category: string;
	comment?: string;
	text: string;
	inputType: 'radio' | 'date' | 'checkbox' | 'postal';
	options?: string[] | CheckboxOption[];
	nextQuestionMap?: string | string[];
	scoreMap?: number[];
	guard?: Guard;
 };
export type CheckboxOption = {
	label: string;
	id: string;
};
export const CATEGORIES = {
	PERSONAL: 'personalInfo',
	// CONTACT: 'contact',
	SYMPTOMS: 'symptoms',
	// RESPIRATORY_SYMPTOMS: 'respiratorySymptoms',
	// ILLNESS: 'illnesses',
	// MEDICATION: 'medication',
};
export const NO_XML = 'X';
export const QUESTION = {
	POSTAL_CODE: 'V1',
	AGE: 'P0',
	 ABOVE_65: 'P1',
	 LIVING_SITUATION: 'P2',
	 CARING: 'P3',
 	WORKSPACE: 'P4',
 	CONTACT_DATE: 'CZ',
	OUT_OF_BREATH: 'SB',
	SYMPTOM_DATE: 'SZ',
	 DATA_DONATION: `${NO_XML}1`,
};
export const XML_ORDER = ['V', 'P', 'C', 'S', 'D', 'M'];
export const QUESTIONS: Question[] = [
{
    category: CATEGORIES.PERSONAL,
    id: 'ARBO',
    inputType: 'radio',
    nextQuestionMap: ['structures','structure_graphs'],
    
options: ['structures','structure_graphs'],
    scoreMap: [1,1],
    text: 'ARBO'


},

{
	id:'SUB',
	category:CATEGORIES.PERSONAL,
	text:'Subiculum',
	inputType:'radio',
	options:['SUBd', 'SUBv'],
	nextQuestionMap:['SUBd', 'SUBv'],
	scoreMap:[1, 1]
},{
	id:'PVi',
	category:CATEGORIES.PERSONAL,
	text:'Periventricular hypothalamic nucleus, intermediate part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'GU5',
	category:CATEGORIES.PERSONAL,
	text:'Gustatory areas, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PALv',
	category:CATEGORIES.PERSONAL,
	text:'Pallidum, ventral region',
	inputType:'radio',
	options:['SI', 'MA'],
	nextQuestionMap:['SI', 'MA'],
	scoreMap:[0, 0]
},{
	id:'CN',
	category:CATEGORIES.PERSONAL,
	text:'Cochlear nuclei',
	inputType:'radio',
	options:['CNspg', 'VCO', 'DCO', 'CNlam'],
	nextQuestionMap:['CNspg', 'VCO', 'DCO', 'CNlam'],
	scoreMap:[0, 0, 0, 0]
},{
	id:'VISal4',
	category:CATEGORIES.PERSONAL,
	text:'Anterolateral visual area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AIp6b',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, posterior part, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BA',
	category:CATEGORIES.PERSONAL,
	text:'Bed nucleus of the accessory olfactory tract',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MM',
	category:CATEGORIES.PERSONAL,
	text:'Medial mammillary nucleus',
	inputType:'radio',
	options:['Mmme'],
	nextQuestionMap:['Mmme'],
	scoreMap:[0]
},{
	id:'TU',
	category:CATEGORIES.PERSONAL,
	text:'Tuberal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MEV',
	category:CATEGORIES.PERSONAL,
	text:'Midbrain trigeminal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MH',
	category:CATEGORIES.PERSONAL,
	text:'Medial habenula',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RHP',
	category:CATEGORIES.PERSONAL,
	text:'Retrohippocampal region',
	inputType:'radio',
	options:['SUB', 'PRE', 'PAR', 'ENT', 'POST'],
	nextQuestionMap:['SUB', 'PRE', 'PAR', 'ENT', 'POST'],
	scoreMap:[1, 0, 0, 1, 0]
},{
	id:'VIS6a',
	category:CATEGORIES.PERSONAL,
	text:'Visual areas, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'KF',
	category:CATEGORIES.PERSONAL,
	text:'Kolliker-Fuse subnucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBl6a',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, lateral part, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'isl',
	category:CATEGORIES.PERSONAL,
	text:'Islands of Calleja',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISpm6a',
	category:CATEGORIES.PERSONAL,
	text:'posteromedial visual area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TMd',
	category:CATEGORIES.PERSONAL,
	text:'Tuberomammillary nucleus, dorsal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPd4',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, dorsal part, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NTSm',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the solitary tract, medial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVZ',
	category:CATEGORIES.PERSONAL,
	text:'Periventricular zone',
	inputType:'radio',
	options:['PVi', 'ARH', 'PVH', 'PVa', 'SO', 'ASO'],
	nextQuestionMap:['PVi', 'ARH', 'PVH', 'PVa', 'SO', 'ASO'],
	scoreMap:[0, 0, 1, 0, 0, 1]
},{
	id:'ARH',
	category:CATEGORIES.PERSONAL,
	text:'Arcuate hypothalamic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VM',
	category:CATEGORIES.PERSONAL,
	text:'Ventral medial nucleus of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MV',
	category:CATEGORIES.PERSONAL,
	text:'Medial vestibular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TTd1',
	category:CATEGORIES.PERSONAL,
	text:'Taenia tecta, dorsal part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPagl',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, lateral agranular part',
	inputType:'radio',
	options:['RSPagl6a', 'RSPagl2-3', 'RSPagl6b', 'RSPagl5', 'RSPagl1'],
	nextQuestionMap:['RSPagl6a', 'RSPagl2-3', 'RSPagl6b', 'RSPagl5', 'RSPagl1'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'VISal2-3',
	category:CATEGORIES.PERSONAL,
	text:'Anterolateral visual area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACA1',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MDRNd',
	category:CATEGORIES.PERSONAL,
	text:'Medullary reticular nucleus, dorsal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COAa2',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, anterior part, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSs5',
	category:CATEGORIES.PERSONAL,
	text:'Supplemental somatosensory area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area',
	inputType:'radio',
	options:['SSp2-3', 'SSp-bfd', 'SSp6a', 'SSp-ul', 'SSp4', 'SSp6b', 'SSp5', 'SSp-ll', 'SSp1', 'SSp-m', 'SSp-tr', 'SSp-n'],
	nextQuestionMap:['SSp2-3', 'SSp-bfd', 'SSp6a', 'SSp-ul', 'SSp4', 'SSp6b', 'SSp5', 'SSp-ll', 'SSp1', 'SSp-m', 'SSp-tr', 'SSp-n'],
	scoreMap:[0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1]
},{
	id:'LAT',
	category:CATEGORIES.PERSONAL,
	text:'Lateral group of the dorsal thalamus',
	inputType:'radio',
	options:['SGN', 'PO', 'LP', 'POL'],
	nextQuestionMap:['SGN', 'PO', 'LP', 'POL'],
	scoreMap:[0, 0, 0, 0]
},{
	id:'SUM',
	category:CATEGORIES.PERSONAL,
	text:'Supramammillary nucleus',
	inputType:'radio',
	options:['SUMl', 'SUMm'],
	nextQuestionMap:['SUMl', 'SUMm'],
	scoreMap:[0, 0]
},{
	id:'SUBv-m',
	category:CATEGORIES.PERSONAL,
	text:'Subiculum, ventral part, molecular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DG-sg',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus, granule cell layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUD',
	category:CATEGORIES.PERSONAL,
	text:'Auditory areas',
	inputType:'radio',
	options:['AUDd', 'AUDpo', 'AUDp', 'AUDv'],
	nextQuestionMap:['AUDd', 'AUDpo', 'AUDp', 'AUDv'],
	scoreMap:[1, 1, 1, 1]
},{
	id:'VERM',
	category:CATEGORIES.PERSONAL,
	text:'Vermal regions',
	inputType:'radio',
	options:['NOD', 'UVU', 'PYR', 'LING', 'CUL', 'FOTU', 'CENT', 'DEC'],
	nextQuestionMap:['NOD', 'UVU', 'PYR', 'LING', 'CUL', 'FOTU', 'CENT', 'DEC'],
	scoreMap:[0, 0, 0, 0, 1, 0, 1, 0]
},{
	id:'CA2',
	category:CATEGORIES.PERSONAL,
	text:'Field CA2',
	inputType:'radio',
	options:['CA2sr', 'CA2so', 'CA2slm', 'CA2sp'],
	nextQuestionMap:['CA2sr', 'CA2so', 'CA2slm', 'CA2sp'],
	scoreMap:[0, 0, 0, 0]
},{
	id:'PIR2',
	category:CATEGORIES.PERSONAL,
	text:'Piriform area, pyramidal layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LD',
	category:CATEGORIES.PERSONAL,
	text:'Lateral dorsal nucleus of thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DMHv',
	category:CATEGORIES.PERSONAL,
	text:'Dorsomedial nucleus of the hypothalamus, ventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AIp5',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, posterior part, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PCG',
	category:CATEGORIES.PERSONAL,
	text:'Pontine central gray',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TT',
	category:CATEGORIES.PERSONAL,
	text:'Taenia tecta',
	inputType:'radio',
	options:['TTd1', 'TTd2', 'TTd', 'TTv1', 'TTv2', 'TTv3', 'TTv1-3', 'TTd3', 'TTd1-4', 'TTd4', 'TTv'],
	nextQuestionMap:['TTd1', 'TTd2', 'TTd', 'TTv1', 'TTv2', 'TTv3', 'TTv1-3', 'TTd3', 'TTd1-4', 'TTd4', 'TTv'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},{
	id:'CTX5',
	category:CATEGORIES.PERSONAL,
	text:'Cerebral cortex, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-m4',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, mouth, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TR2',
	category:CATEGORIES.PERSONAL,
	text:'Postpiriform transition area, layers 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CH',
	category:CATEGORIES.PERSONAL,
	text:'Cerebrum',
	inputType:'radio',
	options:['CNU', 'CTX'],
	nextQuestionMap:['CNU', 'CTX'],
	scoreMap:[1, 1]
},{
	id:'ACAv6b',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, ventral part, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA1sr',
	category:CATEGORIES.PERSONAL,
	text:'Field CA1, stratum radiatum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COApm2',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part, medial zone, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVHmpv',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, descending division, medial parvicellular part, ventral zone',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MG',
	category:CATEGORIES.PERSONAL,
	text:'Medial geniculate complex',
	inputType:'radio',
	options:['MGd', 'MGm', 'MGv'],
	nextQuestionMap:['MGd', 'MGm', 'MGv'],
	scoreMap:[0, 0, 0]
},{
	id:'DMHp',
	category:CATEGORIES.PERSONAL,
	text:'Dorsomedial nucleus of the hypothalamus, posterior part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AId5',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, dorsal part, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VMHdm',
	category:CATEGORIES.PERSONAL,
	text:'Ventromedial hypothalamic nucleus, dorsomedial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AMBd',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus ambiguus, dorsal division',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SCiw',
	category:CATEGORIES.PERSONAL,
	text:'Superior colliculus, motor related, intermediate white layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-ll6b',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, lower limb, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSP',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area',
	inputType:'radio',
	options:['RSPagl', 'RSPv', 'RSPd'],
	nextQuestionMap:['RSPagl', 'RSPv', 'RSPd'],
	scoreMap:[1, 1, 1]
},{
	id:'ENTm6',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, dorsal zone, layer 6',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-n6b',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, nose, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TTd2',
	category:CATEGORIES.PERSONAL,
	text:'Taenia tecta, dorsal part, layers 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CEAc',
	category:CATEGORIES.PERSONAL,
	text:'Central amygdalar nucleus, capsular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PERI1',
	category:CATEGORIES.PERSONAL,
	text:'Perirhinal area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SBPV',
	category:CATEGORIES.PERSONAL,
	text:'Subparaventricular zone',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-ll5',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, lower limb, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPv',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, ventral part',
	inputType:'radio',
	options:['RSPv6a', 'RSPv6b', 'RSPv1', 'RSPv5', 'RSPv2-3', 'RSPv2'],
	nextQuestionMap:['RSPv6a', 'RSPv6b', 'RSPv1', 'RSPv5', 'RSPv2-3', 'RSPv2'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'DR',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal nucleus raphé',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MDm',
	category:CATEGORIES.PERSONAL,
	text:'Mediodorsal nucleus of the thalamus, medial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISC4',
	category:CATEGORIES.PERSONAL,
	text:'Visceral area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AIv6a',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, ventral part, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDpo4',
	category:CATEGORIES.PERSONAL,
	text:'Posterior auditory area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVHlp',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, descending division, lateral parvicellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'GU4',
	category:CATEGORIES.PERSONAL,
	text:'Gustatory areas, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-tr2-3',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, trunk, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SPA',
	category:CATEGORIES.PERSONAL,
	text:'Subparafascicular area',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'FRP',
	category:CATEGORIES.PERSONAL,
	text:'Frontal pole, cerebral cortex',
	inputType:'radio',
	options:['FRP2-3', 'FRP1'],
	nextQuestionMap:['FRP2-3', 'FRP1'],
	scoreMap:[0, 0]
},{
	id:'VISam6b',
	category:CATEGORIES.PERSONAL,
	text:'Anteromedial visual area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDp4',
	category:CATEGORIES.PERSONAL,
	text:'Primary auditory area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVHmpd',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, parvicellular division, medial parvicellular part, dorsal zone',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IRN',
	category:CATEGORIES.PERSONAL,
	text:'Intermediate reticular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBvl',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, ventrolateral part',
	inputType:'radio',
	options:['ORBvl6a', 'ORBvl5', 'ORBvl2-3', 'ORBvl6b', 'ORBvl1'],
	nextQuestionMap:['ORBvl6a', 'ORBvl5', 'ORBvl2-3', 'ORBvl6b', 'ORBvl1'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'CA1',
	category:CATEGORIES.PERSONAL,
	text:'Field CA1',
	inputType:'radio',
	options:['CA1sr', 'CA1slm', 'CA1so', 'CA1sp'],
	nextQuestionMap:['CA1sr', 'CA1slm', 'CA1so', 'CA1sp'],
	scoreMap:[0, 0, 0, 0]
},{
	id:'VISl6a',
	category:CATEGORIES.PERSONAL,
	text:'Lateral visual area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DGmb-mo',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus medial blade, molecular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MB',
	category:CATEGORIES.PERSONAL,
	text:'Midbrain',
	inputType:'radio',
	options:['MBsta', 'MBsen', 'MBmot'],
	nextQuestionMap:['MBsta', 'MBsen', 'MBmot'],
	scoreMap:[1, 1, 1]
},{
	id:'structures',
	category:CATEGORIES.PERSONAL,
	text:'structures',
	inputType:'radio',
	options:['Brain'],
	nextQuestionMap:['Brain'],
	scoreMap:[1]
},{
	id:'SUMl',
	category:CATEGORIES.PERSONAL,
	text:'Supramammillary nucleus, lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IGL',
	category:CATEGORIES.PERSONAL,
	text:'Intergeniculate leaflet of the lateral geniculate complex',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-m2-3',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, mouth, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPv6a',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, ventral part, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACAd1',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, dorsal part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MBsta',
	category:CATEGORIES.PERSONAL,
	text:'Midbrain, behavioral state related',
	inputType:'radio',
	options:['PPN', 'RAmb', 'SNc'],
	nextQuestionMap:['PPN', 'RAmb', 'SNc'],
	scoreMap:[0, 1, 0]
},{
	id:'MEApd-c',
	category:CATEGORIES.PERSONAL,
	text:'Medial amygdalar nucleus, posterodorsal part, sublayer c',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NTSco',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the solitary tract, commissural part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NOD',
	category:CATEGORIES.PERSONAL,
	text:'Nodulus (X)',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AV',
	category:CATEGORIES.PERSONAL,
	text:'Anteroventral nucleus of thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTm2',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, dorsal zone, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOs5',
	category:CATEGORIES.PERSONAL,
	text:'Secondary motor area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MDRN',
	category:CATEGORIES.PERSONAL,
	text:'Medullary reticular nucleus',
	inputType:'radio',
	options:['MDRNd', 'MDRNv'],
	nextQuestionMap:['MDRNd', 'MDRNv'],
	scoreMap:[0, 0]
},{
	id:'NB',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the brachium of the inferior colliculus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ICB',
	category:CATEGORIES.PERSONAL,
	text:'Infracerebellar nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PPN',
	category:CATEGORIES.PERSONAL,
	text:'Pedunculopontine nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MO6a',
	category:CATEGORIES.PERSONAL,
	text:'Somatomotor areas, Layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-tr6a',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, trunk, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DORpm',
	category:CATEGORIES.PERSONAL,
	text:'Thalamus, polymodal association cortex related',
	inputType:'radio',
	options:['LAT', 'EPI', 'MTN', 'GENv', 'ATN', 'RT', 'MED', 'ILM'],
	nextQuestionMap:['LAT', 'EPI', 'MTN', 'GENv', 'ATN', 'RT', 'MED', 'ILM'],
	scoreMap:[1, 1, 1, 1, 1, 0, 1, 1]
},{
	id:'SGN',
	category:CATEGORIES.PERSONAL,
	text:'Suprageniculate nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-ul4',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, upper limb, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VMHvl',
	category:CATEGORIES.PERSONAL,
	text:'Ventromedial hypothalamic nucleus, ventrolateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SOCm',
	category:CATEGORIES.PERSONAL,
	text:'Superior olivary complex, medial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AMv',
	category:CATEGORIES.PERSONAL,
	text:'Anteromedial nucleus, ventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ICc',
	category:CATEGORIES.PERSONAL,
	text:'Inferior colliculus, central nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISl5',
	category:CATEGORIES.PERSONAL,
	text:'Lateral visual area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSs1',
	category:CATEGORIES.PERSONAL,
	text:'Supplemental somatosensory area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDpo2-3',
	category:CATEGORIES.PERSONAL,
	text:'Posterior auditory area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MD',
	category:CATEGORIES.PERSONAL,
	text:'Mediodorsal nucleus of thalamus',
	inputType:'radio',
	options:['MDm', 'MDl', 'MDc'],
	nextQuestionMap:['MDm', 'MDl', 'MDc'],
	scoreMap:[0, 0, 0]
},{
	id:'VPL',
	category:CATEGORIES.PERSONAL,
	text:'Ventral posterolateral nucleus of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PRE',
	category:CATEGORIES.PERSONAL,
	text:'Presubiculum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA1slm',
	category:CATEGORIES.PERSONAL,
	text:'Field CA1, stratum lacunosum-moleculare',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'INC',
	category:CATEGORIES.PERSONAL,
	text:'Interstitial nucleus of Cajal',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'GRN',
	category:CATEGORIES.PERSONAL,
	text:'Gigantocellular reticular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp2-3',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISal5',
	category:CATEGORIES.PERSONAL,
	text:'Anterolateral visual area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'GPe',
	category:CATEGORIES.PERSONAL,
	text:'Globus pallidus, external segment',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'OT',
	category:CATEGORIES.PERSONAL,
	text:'Olfactory tubercle',
	inputType:'radio',
	options:['isl', 'islm', 'OT1-3', 'OT3', 'OT2', 'OT1'],
	nextQuestionMap:['isl', 'islm', 'OT1-3', 'OT3', 'OT2', 'OT1'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'AON1',
	category:CATEGORIES.PERSONAL,
	text:'Anterior olfactory nucleus, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVHpm',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, magnocellular division, posterior magnocellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CS',
	category:CATEGORIES.PERSONAL,
	text:'Superior central nucleus raphé',
	inputType:'radio',
	options:['CSl', 'CSm'],
	nextQuestionMap:['CSl', 'CSm'],
	scoreMap:[0, 0]
},{
	id:'VIS4',
	category:CATEGORIES.PERSONAL,
	text:'Visual areas, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AId1',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, dorsal part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ECT',
	category:CATEGORIES.PERSONAL,
	text:'Ectorhinal area',
	inputType:'radio',
	options:['ECT2-3', 'ECT6a', 'ECT6b', 'ECT5', 'ECT1'],
	nextQuestionMap:['ECT2-3', 'ECT6a', 'ECT6b', 'ECT5', 'ECT1'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'BSTp',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, posterior division',
	inputType:'radio',
	options:['BSTtr', 'BSTpr', 'BSTif', 'BSTd', 'BSTse'],
	nextQuestionMap:['BSTtr', 'BSTpr', 'BSTif', 'BSTd', 'BSTse'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'AIp1',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, posterior part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PERI5',
	category:CATEGORIES.PERSONAL,
	text:'Perirhinal area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ICe',
	category:CATEGORIES.PERSONAL,
	text:'Inferior colliculus, external nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPagl6a',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, lateral agranular part, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACAv5',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, ventral part, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PPY',
	category:CATEGORIES.PERSONAL,
	text:'Parapyramidal nucleus',
	inputType:'radio',
	options:['PPYd', 'PPYs'],
	nextQuestionMap:['PPYd', 'PPYs'],
	scoreMap:[0, 0]
},{
	id:'VISam1',
	category:CATEGORIES.PERSONAL,
	text:'Anteromedial visual area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MY-mot',
	category:CATEGORIES.PERSONAL,
	text:'Medulla, motor related',
	inputType:'radio',
	options:['IRN', 'MDRN', 'ICB', 'GRN', 'PPY', 'MARN', 'PAS', 'ISN', 'PMR', 'VNC', 'PGRN', 'XII', 'x', 'LRN', 'ACVII', 'IO', 'VI', 'EV', 'PARN', 'y', 'LIN', 'PHY', 'AMB', 'ECO', 'ACVI', 'DMX', 'INV', 'VII'],
	nextQuestionMap:['IRN', 'MDRN', 'ICB', 'GRN', 'PPY', 'MARN', 'PAS', 'ISN', 'PMR', 'VNC', 'PGRN', 'XII', 'x', 'LRN', 'ACVII', 'IO', 'VI', 'EV', 'PARN', 'y', 'LIN', 'PHY', 'AMB', 'ECO', 'ACVI', 'DMX', 'INV', 'VII'],
	scoreMap:[0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0]
},{
	id:'RSPd6b',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, dorsal part, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AHNp',
	category:CATEGORIES.PERSONAL,
	text:'Anterior hypothalamic nucleus, posterior part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CTX6',
	category:CATEGORIES.PERSONAL,
	text:'Cerebral cortex, layer 6',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PD',
	category:CATEGORIES.PERSONAL,
	text:'Posterodorsal preoptic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MARN',
	category:CATEGORIES.PERSONAL,
	text:'Magnocellular reticular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACAd6a',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, dorsal part, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MBsen',
	category:CATEGORIES.PERSONAL,
	text:'Midbrain, sensory related',
	inputType:'radio',
	options:['MEV', 'NB', 'SCs', 'SAG', 'PBG', 'IC'],
	nextQuestionMap:['MEV', 'NB', 'SCs', 'SAG', 'PBG', 'IC'],
	scoreMap:[0, 0, 1, 0, 0, 1]
},{
	id:'MO6b',
	category:CATEGORIES.PERSONAL,
	text:'Somatomotor areas, Layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DGlb-mo',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus lateral blade, molecular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TEa6a',
	category:CATEGORIES.PERSONAL,
	text:'Temporal association areas, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'HEM',
	category:CATEGORIES.PERSONAL,
	text:'Hemispheric regions',
	inputType:'radio',
	options:['AN', 'FL', 'COPY', 'PFL', 'PRM', 'SIM'],
	nextQuestionMap:['AN', 'FL', 'COPY', 'PFL', 'PRM', 'SIM'],
	scoreMap:[1, 0, 0, 0, 0, 0]
},{
	id:'PRP',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus prepositus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDv2-3',
	category:CATEGORIES.PERSONAL,
	text:'Ventral auditory area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISC6b',
	category:CATEGORIES.PERSONAL,
	text:'Visceral area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MDl',
	category:CATEGORIES.PERSONAL,
	text:'Mediodorsal nucleus of the thalamus, lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'UVU',
	category:CATEGORIES.PERSONAL,
	text:'Uvula (IX)',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VAL',
	category:CATEGORIES.PERSONAL,
	text:'Ventral anterior-lateral complex of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PL6a',
	category:CATEGORIES.PERSONAL,
	text:'Prelimbic area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISp5',
	category:CATEGORIES.PERSONAL,
	text:'Primary visual area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PL2-3',
	category:CATEGORIES.PERSONAL,
	text:'Prelimbic area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PAS',
	category:CATEGORIES.PERSONAL,
	text:'Parasolitary nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ILA2-3',
	category:CATEGORIES.PERSONAL,
	text:'Infralimbic area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SUBv-sr',
	category:CATEGORIES.PERSONAL,
	text:'Subiculum, ventral part, stratum radiatum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'STRd',
	category:CATEGORIES.PERSONAL,
	text:'Striatum dorsal region',
	inputType:'radio',
	options:['CP'],
	nextQuestionMap:['CP'],
	scoreMap:[0]
},{
	id:'ISN',
	category:CATEGORIES.PERSONAL,
	text:'Inferior salivatory nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACAd5',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, dorsal part, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CUL5',
	category:CATEGORIES.PERSONAL,
	text:'Lobule V',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MO1',
	category:CATEGORIES.PERSONAL,
	text:'Somatomotor areas, Layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-ll2-3',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, lower limb, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'islm',
	category:CATEGORIES.PERSONAL,
	text:'Major island of Calleja',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PMv',
	category:CATEGORIES.PERSONAL,
	text:'Ventral premammillary nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PBls',
	category:CATEGORIES.PERSONAL,
	text:'Parabrachial nucleus, lateral division, superior lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AHNd',
	category:CATEGORIES.PERSONAL,
	text:'Anterior hypothalamic nucleus, dorsal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AI',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area',
	inputType:'radio',
	options:['AIp', 'AId', 'AIv'],
	nextQuestionMap:['AIp', 'AId', 'AIv'],
	scoreMap:[1, 1, 1]
},{
	id:'SLC',
	category:CATEGORIES.PERSONAL,
	text:'Subceruleus nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOp5',
	category:CATEGORIES.PERSONAL,
	text:'Primary motor area, Layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AId6b',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, dorsal part, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COApm3',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part, medial zone, layer 3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PMR',
	category:CATEGORIES.PERSONAL,
	text:'Paramedian reticular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORB5',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DGlb-po',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus lateral blade, polymorph layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AN',
	category:CATEGORIES.PERSONAL,
	text:'Ansiform lobule',
	inputType:'radio',
	options:['ANcr1', 'ANcr2'],
	nextQuestionMap:['ANcr1', 'ANcr2'],
	scoreMap:[0, 0]
},{
	id:'SPVOmdmd',
	category:CATEGORIES.PERSONAL,
	text:'Spinal nucleus of the trigeminal, oral part, middle dorsomedial part, dorsal zone',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA1so',
	category:CATEGORIES.PERSONAL,
	text:'Field CA1, stratum oriens',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LZ',
	category:CATEGORIES.PERSONAL,
	text:'Hypothalamic lateral zone',
	inputType:'radio',
	options:['TU', 'ZI', 'STN', 'RCH', 'PST', 'LHA', 'LPO', 'PSTN'],
	nextQuestionMap:['TU', 'ZI', 'STN', 'RCH', 'PST', 'LHA', 'LPO', 'PSTN'],
	scoreMap:[0, 1, 0, 0, 0, 0, 0, 0]
},{
	id:'VIS6b',
	category:CATEGORIES.PERSONAL,
	text:'Visual areas, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DP6a',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal peduncular area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NIS',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus intercalates',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CTX1',
	category:CATEGORIES.PERSONAL,
	text:'Cerebral cortex, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TTd',
	category:CATEGORIES.PERSONAL,
	text:'Taenia tecta, dorsal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PO',
	category:CATEGORIES.PERSONAL,
	text:'Posterior complex of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LSc',
	category:CATEGORIES.PERSONAL,
	text:'Lateral septal nucleus, caudal (caudodorsal) part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VIS5',
	category:CATEGORIES.PERSONAL,
	text:'Visual areas, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTm5-6',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, dorsal zone, layer 5/6',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISal',
	category:CATEGORIES.PERSONAL,
	text:'Anterolateral visual area',
	inputType:'radio',
	options:['VISal4', 'VISal2-3', 'VISal5', 'VISal6a', 'VISal6b', 'VISal1'],
	nextQuestionMap:['VISal4', 'VISal2-3', 'VISal5', 'VISal6a', 'VISal6b', 'VISal1'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'SUMm',
	category:CATEGORIES.PERSONAL,
	text:'Supramammillary nucleus, medial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISpl6b',
	category:CATEGORIES.PERSONAL,
	text:'Posterolateral visual area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-m5',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, mouth, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBm1',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, medial part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PL6b',
	category:CATEGORIES.PERSONAL,
	text:'Prelimbic area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PAG',
	category:CATEGORIES.PERSONAL,
	text:'Periaqueductal gray',
	inputType:'radio',
	options:['INC', 'PRC', 'ND'],
	nextQuestionMap:['INC', 'PRC', 'ND'],
	scoreMap:[0, 0, 0]
},{
	id:'AHA',
	category:CATEGORIES.PERSONAL,
	text:'Anterior hypothalamic area',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SUBd-sp',
	category:CATEGORIES.PERSONAL,
	text:'Subiculum, dorsal part, pyramidal layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOBgr',
	category:CATEGORIES.PERSONAL,
	text:'Main olfactory bulb, granule layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBvl6a',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, ventrolateral part, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA',
	category:CATEGORIES.PERSONAL,
	text:'Ammon\'s Horn',
	inputType:'radio',
	options:['CA2', 'CA1', 'CA3'],
	nextQuestionMap:['CA2', 'CA1', 'CA3'],
	scoreMap:[1, 1, 1]
},{
	id:'ORBvl5',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, ventrolateral part, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DN',
	category:CATEGORIES.PERSONAL,
	text:'Dentate nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'EMTmv1',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, ventral zone, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MPT',
	category:CATEGORIES.PERSONAL,
	text:'Medial pretectal area',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CTXsp',
	category:CATEGORIES.PERSONAL,
	text:'Cortical subplate',
	inputType:'radio',
	options:['BLA', 'PA', 'BMA', 'EP', 'CLA', 'LA', 'CTXsp6b'],
	nextQuestionMap:['BLA', 'PA', 'BMA', 'EP', 'CLA', 'LA', 'CTXsp6b'],
	scoreMap:[1, 0, 1, 1, 0, 0, 0]
},{
	id:'PIR1-3',
	category:CATEGORIES.PERSONAL,
	text:'Piriform area, layers 1-3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DP2',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal peduncular area, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MS',
	category:CATEGORIES.PERSONAL,
	text:'Medial septal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-n4',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, nose, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VNC',
	category:CATEGORIES.PERSONAL,
	text:'Vestibular nuclei',
	inputType:'radio',
	options:['MV', 'SUV', 'SPIV', 'LAV'],
	nextQuestionMap:['MV', 'SUV', 'SPIV', 'LAV'],
	scoreMap:[0, 0, 0, 0]
},{
	id:'AD',
	category:CATEGORIES.PERSONAL,
	text:'Anterodorsal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ICd',
	category:CATEGORIES.PERSONAL,
	text:'Inferior colliculus, dorsal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MEAav',
	category:CATEGORIES.PERSONAL,
	text:'Medial amygdalar nucleus, anteroventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-bfd5',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, barrel field, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISpm1',
	category:CATEGORIES.PERSONAL,
	text:'posteromedial visual area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'FF',
	category:CATEGORIES.PERSONAL,
	text:'Fields of Forel',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COApm1-3',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part, medial zone, layer 1-3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RAmb',
	category:CATEGORIES.PERSONAL,
	text:'Midbrain raphe nuclei',
	inputType:'radio',
	options:['DR', 'CLI', 'IPN', 'IF', 'RL'],
	nextQuestionMap:['DR', 'CLI', 'IPN', 'IF', 'RL'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'PAA1',
	category:CATEGORIES.PERSONAL,
	text:'Piriform-amygdalar area, molecular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LT',
	category:CATEGORIES.PERSONAL,
	text:'Lateral terminal nucleus of the accessory optic tract',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PL2',
	category:CATEGORIES.PERSONAL,
	text:'Prelimbic area, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACAv6a',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, ventral part, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CTX2',
	category:CATEGORIES.PERSONAL,
	text:'Cerebral cortex, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COApm1',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part, medial zone, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SCs',
	category:CATEGORIES.PERSONAL,
	text:'Superior colliculus, sensory related',
	inputType:'radio',
	options:['SCsg', 'SCzo', 'SCop'],
	nextQuestionMap:['SCsg', 'SCzo', 'SCop'],
	scoreMap:[0, 0, 0]
},{
	id:'SPVO',
	category:CATEGORIES.PERSONAL,
	text:'Spinal nucleus of the trigeminal, oral part',
	inputType:'radio',
	options:['SPVOmdmd', 'SPVOcdm', 'SPVOrdm', 'SPVOmdmv', 'SPVOvl'],
	nextQuestionMap:['SPVOmdmd', 'SPVOcdm', 'SPVOrdm', 'SPVOmdmv', 'SPVOvl'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'MEApd-a',
	category:CATEGORIES.PERSONAL,
	text:'Medial amygdalar nucleus, posterodorsal part, sublayer a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'OT1-3',
	category:CATEGORIES.PERSONAL,
	text:'Olfactory tubercle, layers 1-3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AM',
	category:CATEGORIES.PERSONAL,
	text:'Anteromedial nucleus',
	inputType:'radio',
	options:['AMv', 'AMd'],
	nextQuestionMap:['AMv', 'AMd'],
	scoreMap:[0, 0]
},{
	id:'PRC',
	category:CATEGORIES.PERSONAL,
	text:'Precommissural nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PS',
	category:CATEGORIES.PERSONAL,
	text:'Parastrial nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'POR',
	category:CATEGORIES.PERSONAL,
	text:'Superior olivary complex, periolivary region',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISam2-3',
	category:CATEGORIES.PERSONAL,
	text:'Anteromedial visual area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOp2-3',
	category:CATEGORIES.PERSONAL,
	text:'Primary motor area, Layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PGRN',
	category:CATEGORIES.PERSONAL,
	text:'Paragigantocellular reticular nucleus',
	inputType:'radio',
	options:['PGRNd', 'PGRNl'],
	nextQuestionMap:['PGRNd', 'PGRNl'],
	scoreMap:[0, 0]
},{
	id:'COAa3',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, anterior part, layer 3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'OP',
	category:CATEGORIES.PERSONAL,
	text:'Olivary pretectal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SCig-a',
	category:CATEGORIES.PERSONAL,
	text:'Superior colliculus, motor related, intermediate gray layer, sublayer a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NC',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus circularis',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BMAp',
	category:CATEGORIES.PERSONAL,
	text:'Basomedial amygdalar nucleus, posterior part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACA',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area',
	inputType:'radio',
	options:['ACA1', 'ACA2-3', 'ACA6a', 'ACAd', 'ACAv', 'ACA5', 'ACA6b'],
	nextQuestionMap:['ACA1', 'ACA2-3', 'ACA6a', 'ACAd', 'ACAv', 'ACA5', 'ACA6b'],
	scoreMap:[0, 0, 0, 1, 1, 0, 0]
},{
	id:'STRv',
	category:CATEGORIES.PERSONAL,
	text:'Striatum ventral region',
	inputType:'radio',
	options:['OT', 'ACB', 'FS'],
	nextQuestionMap:['OT', 'ACB', 'FS'],
	scoreMap:[1, 0, 0]
},{
	id:'MOp1',
	category:CATEGORIES.PERSONAL,
	text:'Primary motor area, Layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORB1',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PL1',
	category:CATEGORIES.PERSONAL,
	text:'Prelimbic area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BSTju',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, anterior division, juxtacapsular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AId2-3',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, dorsal part, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TTv1',
	category:CATEGORIES.PERSONAL,
	text:'Taenia tecta, ventral part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-n2-3',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, nose, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BSTtr',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, posterior division, transverse nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'OLF',
	category:CATEGORIES.PERSONAL,
	text:'Olfactory bulb',
	inputType:'radio',
	options:['TT', 'PIR', 'MOB', 'PAA', 'AOB', 'AON', 'DP', 'NLOT', 'TR', 'COA'],
	nextQuestionMap:['TT', 'PIR', 'MOB', 'PAA', 'AOB', 'AON', 'DP', 'NLOT', 'TR', 'COA'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},{
	id:'SSp-bfd',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, barrel field',
	inputType:'radio',
	options:['SSp-bfd5', 'SSp-bfd1', 'SSp-bfd2-3', 'SSp-bfd6b', 'SSp-bfd6a', 'SSp-bfd4'],
	nextQuestionMap:['SSp-bfd5', 'SSp-bfd1', 'SSp-bfd2-3', 'SSp-bfd6b', 'SSp-bfd6a', 'SSp-bfd4'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'AIp2-3',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, posterior part, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AT',
	category:CATEGORIES.PERSONAL,
	text:'Anterior tegmental nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA3slu',
	category:CATEGORIES.PERSONAL,
	text:'Field CA3, stratum lucidum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVHp',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, parvicellular division',
	inputType:'radio',
	options:['PVHmpd', 'PVHpv', 'PVHap'],
	nextQuestionMap:['PVHmpd', 'PVHpv', 'PVHap'],
	scoreMap:[0, 0, 0]
},{
	id:'MRNmg',
	category:CATEGORIES.PERSONAL,
	text:'Midbrain reticular nucleus, magnocellular part, general',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'EW',
	category:CATEGORIES.PERSONAL,
	text:'Edinger-Westphal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBl6b',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, lateral part, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SOC',
	category:CATEGORIES.PERSONAL,
	text:'Superior olivary complex',
	inputType:'radio',
	options:['SOCm', 'POR', 'SOCl'],
	nextQuestionMap:['SOCm', 'POR', 'SOCl'],
	scoreMap:[0, 0, 0]
},{
	id:'CTX3',
	category:CATEGORIES.PERSONAL,
	text:'Cerebral cortex, layer 3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AMBv',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus ambiguus, ventral division',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPd1',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, dorsal part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'OV',
	category:CATEGORIES.PERSONAL,
	text:'Vascular organ of the lamina terminalis',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl3',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part, layer 3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NTSge',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the solitary tract, gelatinous part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IAD',
	category:CATEGORIES.PERSONAL,
	text:'Interanterodorsal nucleus of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBm5',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, medial part, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DGmb-po',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus medial blade, polymorph layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MPNc',
	category:CATEGORIES.PERSONAL,
	text:'Medial preoptic nucleus, central part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LSX',
	category:CATEGORIES.PERSONAL,
	text:'Lateral septal complex',
	inputType:'radio',
	options:['SF', 'SH', 'LS'],
	nextQuestionMap:['SF', 'SH', 'LS'],
	scoreMap:[0, 0, 1]
},{
	id:'COApl',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part, lateral zone',
	inputType:'radio',
	options:['COApl1-2', 'COApl2', 'COApl3', 'COApl1-3', 'COApl1'],
	nextQuestionMap:['COApl1-2', 'COApl2', 'COApl3', 'COApl1-3', 'COApl1'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'CB',
	category:CATEGORIES.PERSONAL,
	text:'Cerebellum',
	inputType:'radio',
	options:['CBN', 'CBX'],
	nextQuestionMap:['CBN', 'CBX'],
	scoreMap:[1, 1]
},{
	id:'MOp6b',
	category:CATEGORIES.PERSONAL,
	text:'Primary motor area, Layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MO',
	category:CATEGORIES.PERSONAL,
	text:'Somatomotor areas',
	inputType:'radio',
	options:['MO6a', 'MO6b', 'MO1', 'MO5', 'MOp', 'MOs', 'MO2-3'],
	nextQuestionMap:['MO6a', 'MO6b', 'MO1', 'MO5', 'MOp', 'MOs', 'MO2-3'],
	scoreMap:[0, 0, 0, 0, 1, 1, 0]
},{
	id:'VMHc',
	category:CATEGORIES.PERSONAL,
	text:'Ventromedial hypothalamic nucleus, central part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CLI',
	category:CATEGORIES.PERSONAL,
	text:'Central linear nucleus raphe',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-tr1',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, trunk, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AIv5',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, ventral part, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BLAp',
	category:CATEGORIES.PERSONAL,
	text:'Basolateral amygdalar nucleus, posterior part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACB',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus accumbens',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PIR',
	category:CATEGORIES.PERSONAL,
	text:'Piriform area',
	inputType:'radio',
	options:['PIR2', 'PIR1-3', 'PIR3', 'PIR1'],
	nextQuestionMap:['PIR2', 'PIR1-3', 'PIR3', 'PIR1'],
	scoreMap:[0, 0, 0, 0]
},{
	id:'CU',
	category:CATEGORIES.PERSONAL,
	text:'Cuneate nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RPO',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus raphe pontis',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MT',
	category:CATEGORIES.PERSONAL,
	text:'Medial terminal nucleus of the accessory optic tract',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BLAv',
	category:CATEGORIES.PERSONAL,
	text:'Basolateral amygdalar nucleus, ventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AON2',
	category:CATEGORIES.PERSONAL,
	text:'Anterior olfactory nucleus, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'FRP2-3',
	category:CATEGORIES.PERSONAL,
	text:'Frontal pole, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISC1',
	category:CATEGORIES.PERSONAL,
	text:'Visceral area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CP',
	category:CATEGORIES.PERSONAL,
	text:'Caudoputamen',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BST',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis',
	inputType:'radio',
	options:['BSTp', 'BSTa'],
	nextQuestionMap:['BSTp', 'BSTa'],
	scoreMap:[1, 1]
},{
	id:'AOBgr',
	category:CATEGORIES.PERSONAL,
	text:'Accessory olfactory bulb, granular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'P-sat',
	category:CATEGORIES.PERSONAL,
	text:'Pons, behavioral state related',
	inputType:'radio',
	options:['CS', 'SLC', 'RPO', 'NI', 'SLD', 'PRNr', 'LDT', 'LC'],
	nextQuestionMap:['CS', 'SLC', 'RPO', 'NI', 'SLD', 'PRNr', 'LDT', 'LC'],
	scoreMap:[1, 0, 0, 0, 0, 0, 0, 0]
},{
	id:'SUBv-sp',
	category:CATEGORIES.PERSONAL,
	text:'Subiculum, ventral part, pyramidal layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORB6b',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AIp',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, posterior part',
	inputType:'radio',
	options:['AIp6b', 'AIp5', 'AIp1', 'AIp2-3', 'AIp6a'],
	nextQuestionMap:['AIp6b', 'AIp5', 'AIp1', 'AIp2-3', 'AIp6a'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'EPI',
	category:CATEGORIES.PERSONAL,
	text:'Epithalamus',
	inputType:'radio',
	options:['MH', 'PIN', 'LH'],
	nextQuestionMap:['MH', 'PIN', 'LH'],
	scoreMap:[0, 0, 0]
},{
	id:'PVHdp',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, descending division, dorsal parvicellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TTv2',
	category:CATEGORIES.PERSONAL,
	text:'Taenia tecta, ventral part, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVT',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular nucleus of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-bfd1',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, barrel field, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MY',
	category:CATEGORIES.PERSONAL,
	text:'Medulla',
	inputType:'radio',
	options:['MY-mot', 'My-sen', 'MY-sat'],
	nextQuestionMap:['MY-mot', 'My-sen', 'MY-sat'],
	scoreMap:[1, 1, 1]
},{
	id:'PF',
	category:CATEGORIES.PERSONAL,
	text:'Parafascicular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SPVOcdm',
	category:CATEGORIES.PERSONAL,
	text:'Spinal nucleus of the trigeminal, oral part, caudal dorsomedial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DGcr',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus crest',
	inputType:'radio',
	options:['DGcr-mo', 'DGcr-sg', 'DGcr-po'],
	nextQuestionMap:['DGcr-mo', 'DGcr-sg', 'DGcr-po'],
	scoreMap:[0, 0, 0]
},{
	id:'AId',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, dorsal part',
	inputType:'radio',
	options:['AId5', 'AId1', 'AId6b', 'AId2-3', 'AId6a'],
	nextQuestionMap:['AId5', 'AId1', 'AId6b', 'AId2-3', 'AId6a'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'PVH',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus',
	inputType:'radio',
	options:['PVHp', 'PVHm'],
	nextQuestionMap:['PVHp', 'PVHm'],
	scoreMap:[1, 1]
},{
	id:'MEA',
	category:CATEGORIES.PERSONAL,
	text:'Medial amygdalar nucleus',
	inputType:'radio',
	options:['MEAav', 'MEApv', 'MEAad', 'MEApd'],
	nextQuestionMap:['MEAav', 'MEApv', 'MEAad', 'MEApd'],
	scoreMap:[0, 0, 0, 1]
},{
	id:'FL',
	category:CATEGORIES.PERSONAL,
	text:'Flocculus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CNU',
	category:CATEGORIES.PERSONAL,
	text:'Cerebral nuclei',
	inputType:'radio',
	options:['PAL', 'STR'],
	nextQuestionMap:['PAL', 'STR'],
	scoreMap:[1, 1]
},{
	id:'NOT',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the optic tract',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BLA',
	category:CATEGORIES.PERSONAL,
	text:'Basolateral amygdalar nucleus',
	inputType:'radio',
	options:['BLAp', 'BLAv', 'BLAa'],
	nextQuestionMap:['BLAp', 'BLAv', 'BLAa'],
	scoreMap:[0, 0, 0]
},{
	id:'ENTm3',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, dorsal zone, layer 3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISpl1',
	category:CATEGORIES.PERSONAL,
	text:'Posterolateral visual area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TEa6b',
	category:CATEGORIES.PERSONAL,
	text:'Temporal association areas, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VIS1',
	category:CATEGORIES.PERSONAL,
	text:'Visual areas, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PT',
	category:CATEGORIES.PERSONAL,
	text:'Parataenial nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SPVOrdm',
	category:CATEGORIES.PERSONAL,
	text:'Spinal nucleus of the trigeminal, oral part, rostral dorsomedial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NI',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus incertus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PAA2',
	category:CATEGORIES.PERSONAL,
	text:'Piriform-amygdalar area, pyramidal layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SPFp',
	category:CATEGORIES.PERSONAL,
	text:'Subparafascicular nucleus, parvicellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ND',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of Darkschewitsch',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDd',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal auditory area',
	inputType:'radio',
	options:['AUDd6a', 'AUDd6b', 'AUDd4', 'AUDd1', 'AUDd5', 'AUDd2-3'],
	nextQuestionMap:['AUDd6a', 'AUDd6b', 'AUDd4', 'AUDd1', 'AUDd5', 'AUDd2-3'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'SSp-m6a',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, mouth, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SF',
	category:CATEGORIES.PERSONAL,
	text:'Septofimbrial nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COAp',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part',
	inputType:'radio',
	options:['COApl', 'COApm'],
	nextQuestionMap:['COApl', 'COApm'],
	scoreMap:[1, 1]
},{
	id:'AUDd6a',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal auditory area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDp1',
	category:CATEGORIES.PERSONAL,
	text:'Primary auditory area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PGRNd',
	category:CATEGORIES.PERSONAL,
	text:'Paragigantocellular reticular nucleus, dorsal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISam4',
	category:CATEGORIES.PERSONAL,
	text:'Anteromedial visual area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SCsg',
	category:CATEGORIES.PERSONAL,
	text:'Superior colliculus, superficial gray layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NR',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of Roller',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PA',
	category:CATEGORIES.PERSONAL,
	text:'Posterior amygdalar nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PYR',
	category:CATEGORIES.PERSONAL,
	text:'Pyramus (VIII)',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDpo5',
	category:CATEGORIES.PERSONAL,
	text:'Posterior auditory area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VENT',
	category:CATEGORIES.PERSONAL,
	text:'Ventral group of the dorsal thalamus',
	inputType:'radio',
	options:['VM', 'VAL', 'VP'],
	nextQuestionMap:['VM', 'VAL', 'VP'],
	scoreMap:[0, 0, 1]
},{
	id:'CENT2',
	category:CATEGORIES.PERSONAL,
	text:'Lobule II',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ZI',
	category:CATEGORIES.PERSONAL,
	text:'Zona incerta',
	inputType:'radio',
	options:['FF', 'A13'],
	nextQuestionMap:['FF', 'A13'],
	scoreMap:[0, 0]
},{
	id:'MGd',
	category:CATEGORIES.PERSONAL,
	text:'Medial geniculate complex, dorsal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CEA',
	category:CATEGORIES.PERSONAL,
	text:'Central amygdalar nucleus',
	inputType:'radio',
	options:['CEAc', 'CEAl', 'CEAm'],
	nextQuestionMap:['CEAc', 'CEAl', 'CEAm'],
	scoreMap:[0, 0, 0]
},{
	id:'PPT',
	category:CATEGORIES.PERSONAL,
	text:'Posterior pretectal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PAR',
	category:CATEGORIES.PERSONAL,
	text:'Parasubiculum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVa',
	category:CATEGORIES.PERSONAL,
	text:'Periventricular hypothalamic nucleus, anterior part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENT',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area',
	inputType:'radio',
	options:['ENTl', 'ENTm', 'ENTmv'],
	nextQuestionMap:['ENTl', 'ENTm', 'ENTmv'],
	scoreMap:[1, 1, 1]
},{
	id:'SNr',
	category:CATEGORIES.PERSONAL,
	text:'Substantia nigra, reticular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TRN',
	category:CATEGORIES.PERSONAL,
	text:'Tegmental reticular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IPN',
	category:CATEGORIES.PERSONAL,
	text:'Interpeduncular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPv6b',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, ventral part, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp6a',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPd5',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, dorsal part, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BSTfu',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, anterior division, fusiform nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SAG',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus sagulum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NTS',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the solitary tract',
	inputType:'radio',
	options:['NTSm', 'NTSco', 'NTSge', 'NTSce', 'NTSl'],
	nextQuestionMap:['NTSm', 'NTSco', 'NTSge', 'NTSce', 'NTSl'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'LP',
	category:CATEGORIES.PERSONAL,
	text:'Lateral posterior nucleus of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CNspg',
	category:CATEGORIES.PERSONAL,
	text:'Cochlear nucleus, subpedunclular granular region',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AMd',
	category:CATEGORIES.PERSONAL,
	text:'Anteromedial nucleus, dorsal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PGRNl',
	category:CATEGORIES.PERSONAL,
	text:'Paragigantocellular reticular nucleus, lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPv1',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, ventral part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PAA3',
	category:CATEGORIES.PERSONAL,
	text:'Piriform-amygdalar area, polymorph layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CM',
	category:CATEGORIES.PERSONAL,
	text:'Central medial nucleus of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NTSce',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the solitary tract, central part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LING',
	category:CATEGORIES.PERSONAL,
	text:'Lingula (I)',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'STN',
	category:CATEGORIES.PERSONAL,
	text:'Subthalamic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RN',
	category:CATEGORIES.PERSONAL,
	text:'Red Nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOB',
	category:CATEGORIES.PERSONAL,
	text:'Main olfactory bulb',
	inputType:'radio',
	options:['MOBgr', 'MOBmi', 'MOBgl', 'MOBopl', 'MOBipl'],
	nextQuestionMap:['MOBgr', 'MOBmi', 'MOBgl', 'MOBopl', 'MOBipl'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'MGm',
	category:CATEGORIES.PERSONAL,
	text:'Medial geniculate complex, medial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MTN',
	category:CATEGORIES.PERSONAL,
	text:'Midline group of the dorsal thalamus',
	inputType:'radio',
	options:['PVT', 'PT', 'RE'],
	nextQuestionMap:['PVT', 'PT', 'RE'],
	scoreMap:[0, 0, 0]
},{
	id:'BS',
	category:CATEGORIES.PERSONAL,
	text:'Brain stem',
	inputType:'radio',
	options:['MB', 'HB', 'IB'],
	nextQuestionMap:['MB', 'HB', 'IB'],
	scoreMap:[1, 1, 1]
},{
	id:'VISal6a',
	category:CATEGORIES.PERSONAL,
	text:'Anterolateral visual area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NLL',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the lateral lemniscus',
	inputType:'radio',
	options:['NLLv', 'NLLh', 'NLLd'],
	nextQuestionMap:['NLLv', 'NLLh', 'NLLd'],
	scoreMap:[0, 0, 0]
},{
	id:'PL',
	category:CATEGORIES.PERSONAL,
	text:'Prelimbic area',
	inputType:'radio',
	options:['PL6a', 'PL2-3', 'PL6b', 'PL2', 'PL1', 'PL5'],
	nextQuestionMap:['PL6a', 'PL2-3', 'PL6b', 'PL2', 'PL1', 'PL5'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'GU',
	category:CATEGORIES.PERSONAL,
	text:'Gustatory areas',
	inputType:'radio',
	options:['GU5', 'GU4', 'GU2-3', 'GU6a', 'GU6b', 'GU1'],
	nextQuestionMap:['GU5', 'GU4', 'GU2-3', 'GU6a', 'GU6b', 'GU1'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'SSp-m1',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, mouth, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VTN',
	category:CATEGORIES.PERSONAL,
	text:'Ventral tegmental nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'OT3',
	category:CATEGORIES.PERSONAL,
	text:'Olfactory tubercle, polymorph layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOs6a',
	category:CATEGORIES.PERSONAL,
	text:'Secondary motor area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA3sr',
	category:CATEGORIES.PERSONAL,
	text:'Field CA3, stratum radiatum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SOCl',
	category:CATEGORIES.PERSONAL,
	text:'Superior olivary complex, lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PG',
	category:CATEGORIES.PERSONAL,
	text:'Pontine gray',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AIv1',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, ventral part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVHmm',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, magnocellular division, medial magnocellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PBm',
	category:CATEGORIES.PERSONAL,
	text:'Parabrachial nucleus, medial division',
	inputType:'radio',
	options:['PBmm', 'PBme', 'PBmv'],
	nextQuestionMap:['PBmm', 'PBme', 'PBmv'],
	scoreMap:[0, 0, 0]
},{
	id:'BSTov',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, anterior division, oval nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISpl2-3',
	category:CATEGORIES.PERSONAL,
	text:'Posterolateral visual area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-ul6b',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, upper limb, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PRT',
	category:CATEGORIES.PERSONAL,
	text:'Pretectal region',
	inputType:'radio',
	options:['MPT', 'OP', 'NOT', 'PPT', 'APN', 'NPC'],
	nextQuestionMap:['MPT', 'OP', 'NOT', 'PPT', 'APN', 'NPC'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'GENv',
	category:CATEGORIES.PERSONAL,
	text:'Geniculate group, ventral thalamus',
	inputType:'radio',
	options:['IGL', 'SubG', 'LGv'],
	nextQuestionMap:['IGL', 'SubG', 'LGv'],
	scoreMap:[0, 0, 1]
},{
	id:'SPF',
	category:CATEGORIES.PERSONAL,
	text:'Subparafascicular nucleus',
	inputType:'radio',
	options:['SPFp', 'SPFm'],
	nextQuestionMap:['SPFp', 'SPFm'],
	scoreMap:[0, 0]
},{
	id:'SCdg',
	category:CATEGORIES.PERSONAL,
	text:'Superior colliculus, motor related, deep gray layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDpo',
	category:CATEGORIES.PERSONAL,
	text:'Posterior auditory area',
	inputType:'radio',
	options:['AUDpo4', 'AUDpo2-3', 'AUDpo5', 'AUDpo6a', 'AUDpo6b', 'AUDpo1'],
	nextQuestionMap:['AUDpo4', 'AUDpo2-3', 'AUDpo5', 'AUDpo6a', 'AUDpo6b', 'AUDpo1'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'SSp-ll1',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, lower limb, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IMD',
	category:CATEGORIES.PERSONAL,
	text:'Intermediodorsal nucleus of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BMA',
	category:CATEGORIES.PERSONAL,
	text:'Basomedial amygdalar nucleus',
	inputType:'radio',
	options:['BMAp', 'BMAa'],
	nextQuestionMap:['BMAp', 'BMAa'],
	scoreMap:[0, 0]
},{
	id:'ENTm2a',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, dorsal zone, layer 2a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PTLp1',
	category:CATEGORIES.PERSONAL,
	text:'Posterior parietal association areas, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MEZ',
	category:CATEGORIES.PERSONAL,
	text:'Hypothalamic medial zone',
	inputType:'radio',
	options:['PMv', 'MPN', 'PH', 'AHN', 'MBO', 'PVHd', 'VMH', 'PMd'],
	nextQuestionMap:['PMv', 'MPN', 'PH', 'AHN', 'MBO', 'PVHd', 'VMH', 'PMd'],
	scoreMap:[0, 1, 0, 1, 1, 1, 1, 0]
},{
	id:'BSTpr',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, posterior division, principal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NLLv',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the lateral lemniscus, ventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SCdw',
	category:CATEGORIES.PERSONAL,
	text:'Superior colliculus, motor related, deep white layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ECT2-3',
	category:CATEGORIES.PERSONAL,
	text:'Ectorhinal area/Layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ILA6a',
	category:CATEGORIES.PERSONAL,
	text:'Infralimbic area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DP1',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal peduncular area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VTA',
	category:CATEGORIES.PERSONAL,
	text:'Ventral tegmental area',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part',
	inputType:'radio',
	options:['ENTl3', 'ENTl6b', 'ENTl2b', 'ENTl2-3', 'ENTl4', 'ENTl5-6', 'ENTl2a', 'ENTl1', 'ENTl6a', 'ENTl5', 'ENTl4-5', 'ENTl2'],
	nextQuestionMap:['ENTl3', 'ENTl6b', 'ENTl2b', 'ENTl2-3', 'ENTl4', 'ENTl5-6', 'ENTl2a', 'ENTl1', 'ENTl6a', 'ENTl5', 'ENTl4-5', 'ENTl2'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},{
	id:'SSs',
	category:CATEGORIES.PERSONAL,
	text:'Supplemental somatosensory area',
	inputType:'radio',
	options:['SSs5', 'SSs1', 'SSs2-3', 'SSs6a', 'SSs6b', 'SSs4'],
	nextQuestionMap:['SSs5', 'SSs1', 'SSs2-3', 'SSs6a', 'SSs6b', 'SSs4'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'TEa4',
	category:CATEGORIES.PERSONAL,
	text:'Temporal association areas, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ATN',
	category:CATEGORIES.PERSONAL,
	text:'Anterior group of the dorsal thalamus',
	inputType:'radio',
	options:['LD', 'AV', 'AD', 'AM', 'IAD', 'IAM'],
	nextQuestionMap:['LD', 'AV', 'AD', 'AM', 'IAD', 'IAM'],
	scoreMap:[0, 0, 0, 1, 0, 0]
},{
	id:'ORBvl2-3',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, ventrolateral part, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SUBd',
	category:CATEGORIES.PERSONAL,
	text:'Subiculum, dorsal part',
	inputType:'radio',
	options:['SUBd-sp', 'SUBd-m', 'SUBd-sr'],
	nextQuestionMap:['SUBd-sp', 'SUBd-m', 'SUBd-sr'],
	scoreMap:[0, 0, 0]
},{
	id:'GENd',
	category:CATEGORIES.PERSONAL,
	text:'Geniculate group, dorsal thalamus',
	inputType:'radio',
	options:['MG', 'LGd'],
	nextQuestionMap:['MG', 'LGd'],
	scoreMap:[1, 0]
},{
	id:'CL',
	category:CATEGORIES.PERSONAL,
	text:'Central lateral nucleus of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PBG',
	category:CATEGORIES.PERSONAL,
	text:'Parabigeminal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AIv6b',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, ventral part, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSN',
	category:CATEGORIES.PERSONAL,
	text:'Superior salivatory nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DT',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal terminal nucleus of the accessory optic tract',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'OT2',
	category:CATEGORIES.PERSONAL,
	text:'Olfactory tubercle, pyramidal layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RCH',
	category:CATEGORIES.PERSONAL,
	text:'Retrochiasmatic area',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RM',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus raphe magnus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'XII',
	category:CATEGORIES.PERSONAL,
	text:'Hypoglossal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'V',
	category:CATEGORIES.PERSONAL,
	text:'Motor nucleus of trigeminal',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDp5',
	category:CATEGORIES.PERSONAL,
	text:'Primary auditory area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDv6a',
	category:CATEGORIES.PERSONAL,
	text:'Ventral auditory area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ANcr1',
	category:CATEGORIES.PERSONAL,
	text:'Crus 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'EP',
	category:CATEGORIES.PERSONAL,
	text:'Endopiriform nucleus',
	inputType:'radio',
	options:['EPv', 'EPd'],
	nextQuestionMap:['EPv', 'EPd'],
	scoreMap:[0, 0]
},{
	id:'PSCH',
	category:CATEGORIES.PERSONAL,
	text:'Suprachiasmatic preoptic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISC5',
	category:CATEGORIES.PERSONAL,
	text:'Visceral area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSs2-3',
	category:CATEGORIES.PERSONAL,
	text:'Supplemental somatosensory area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RR',
	category:CATEGORIES.PERSONAL,
	text:'Midbrain reticular nucleus, retrorubral area',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl6b',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISpm',
	category:CATEGORIES.PERSONAL,
	text:'posteromedial visual area',
	inputType:'radio',
	options:['VISpm6a', 'VISpm1', 'VISpm6b', 'VISpm4', 'VISpm2-3', 'VISpm5'],
	nextQuestionMap:['VISpm6a', 'VISpm1', 'VISpm6b', 'VISpm4', 'VISpm2-3', 'VISpm5'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'SSp-bfd2-3',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, barrel field, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'FC',
	category:CATEGORIES.PERSONAL,
	text:'Fasciola cinerea',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CBXpu',
	category:CATEGORIES.PERSONAL,
	text:'Cerebellar cortex, Purkinje layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CUL',
	category:CATEGORIES.PERSONAL,
	text:'Culmen',
	inputType:'radio',
	options:['CUL5', 'CUL4-5', 'CUL4'],
	nextQuestionMap:['CUL5', 'CUL4-5', 'CUL4'],
	scoreMap:[0, 0, 0]
},{
	id:'PPYd',
	category:CATEGORIES.PERSONAL,
	text:'Parapyramidal nucleus, deep part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PB',
	category:CATEGORIES.PERSONAL,
	text:'Parabrachial nucleus',
	inputType:'radio',
	options:['KF', 'PBm', 'PBl'],
	nextQuestionMap:['KF', 'PBm', 'PBl'],
	scoreMap:[0, 1, 1]
},{
	id:'LRNp',
	category:CATEGORIES.PERSONAL,
	text:'Lateral reticular nucleus, parvicellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SG',
	category:CATEGORIES.PERSONAL,
	text:'Supragenual nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBv',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, ventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CTXpl',
	category:CATEGORIES.PERSONAL,
	text:'Cortical plate',
	inputType:'radio',
	options:['OLF', 'HPF', 'Isocortex'],
	nextQuestionMap:['OLF', 'HPF', 'Isocortex'],
	scoreMap:[1, 1, 1]
},{
	id:'EPv',
	category:CATEGORIES.PERSONAL,
	text:'Endopiriform nucleus, ventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-ul5',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, upper limb, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'OT1',
	category:CATEGORIES.PERSONAL,
	text:'Olfactory tubercle, molecular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MPN',
	category:CATEGORIES.PERSONAL,
	text:'Medial preoptic nucleus',
	inputType:'radio',
	options:['MPNc', 'MPNl', 'MPNm'],
	nextQuestionMap:['MPNc', 'MPNl', 'MPNm'],
	scoreMap:[0, 0, 0]
},{
	id:'CTX1-6b',
	category:CATEGORIES.PERSONAL,
	text:'Cerebral cortex, layers 1-6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DP2-3',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal peduncular area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MO5',
	category:CATEGORIES.PERSONAL,
	text:'Somatomotor areas, Layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MRNm',
	category:CATEGORIES.PERSONAL,
	text:'Midbrain reticular nucleus, magnocellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SMT',
	category:CATEGORIES.PERSONAL,
	text:'Submedial nucleus of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PAA',
	category:CATEGORIES.PERSONAL,
	text:'Piriform-amygdalar area',
	inputType:'radio',
	options:['PAA1', 'PAA2', 'PAA3', 'PAA1-3'],
	nextQuestionMap:['PAA1', 'PAA2', 'PAA3', 'PAA1-3'],
	scoreMap:[0, 0, 0, 0]
},{
	id:'RSPv5',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, ventral part, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PP',
	category:CATEGORIES.PERSONAL,
	text:'Peripeduncular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'HIP',
	category:CATEGORIES.PERSONAL,
	text:'Hippocampal region',
	inputType:'radio',
	options:['CA', 'FC', 'DG', 'IG'],
	nextQuestionMap:['CA', 'FC', 'DG', 'IG'],
	scoreMap:[1, 0, 1, 0]
},{
	id:'MDc',
	category:CATEGORIES.PERSONAL,
	text:'Mediodorsal nucleus of the thalamus, central part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA3',
	category:CATEGORIES.PERSONAL,
	text:'Field CA3',
	inputType:'radio',
	options:['CA3slu', 'CA3sr', 'CA3slm', 'CA3so', 'CA3sp'],
	nextQuestionMap:['CA3slu', 'CA3sr', 'CA3slm', 'CA3so', 'CA3sp'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'RH',
	category:CATEGORIES.PERSONAL,
	text:'Rhomboid nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ECT6a',
	category:CATEGORIES.PERSONAL,
	text:'Ectorhinal area/Layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AOB',
	category:CATEGORIES.PERSONAL,
	text:'Accessory olfactory bulb',
	inputType:'radio',
	options:['AOBgr', 'AOBmi', 'AOBgl'],
	nextQuestionMap:['AOBgr', 'AOBmi', 'AOBgl'],
	scoreMap:[0, 0, 0]
},{
	id:'EMTmv2',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, ventral zone, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DGcr-mo',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus crest, molecular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AON',
	category:CATEGORIES.PERSONAL,
	text:'Anterior olfactory nucleus',
	inputType:'radio',
	options:['AON1', 'AON2', 'AONl', 'AONm', 'AONpv', 'AONe', 'AONd'],
	nextQuestionMap:['AON1', 'AON2', 'AONl', 'AONm', 'AONpv', 'AONe', 'AONd'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0]
},{
	id:'AOBmi',
	category:CATEGORIES.PERSONAL,
	text:'Accessory olfactory bulb, mitral layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDd6b',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal auditory area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NLOT1',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the lateral olfactory tract, molecular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'GU2-3',
	category:CATEGORIES.PERSONAL,
	text:'Gustatory areas, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PRNc',
	category:CATEGORIES.PERSONAL,
	text:'Pontine reticular nucleus, caudal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTm',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, dorsal zone',
	inputType:'radio',
	options:['ENTm6', 'ENTm2', 'ENTm5-6', 'ENTm3', 'ENTm2a', 'ENTm2b', 'ENTm4', 'ENTm1', 'ENTm5'],
	nextQuestionMap:['ENTm6', 'ENTm2', 'ENTm5-6', 'ENTm3', 'ENTm2a', 'ENTm2b', 'ENTm4', 'ENTm1', 'ENTm5'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
},{
	id:'ACA2-3',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISam5',
	category:CATEGORIES.PERSONAL,
	text:'Anteromedial visual area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CEAl',
	category:CATEGORIES.PERSONAL,
	text:'Central amygdalar nucleus, lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA2sr',
	category:CATEGORIES.PERSONAL,
	text:'Field CA2, stratum radiatum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PBl',
	category:CATEGORIES.PERSONAL,
	text:'Parabrachial nucleus, lateral division',
	inputType:'radio',
	options:['PBls', 'PBlc', 'PBld', 'PBle', 'PBlv'],
	nextQuestionMap:['PBls', 'PBlc', 'PBld', 'PBle', 'PBlv'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'PST',
	category:CATEGORIES.PERSONAL,
	text:'Preparasubthalamic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-ul',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, upper limb',
	inputType:'radio',
	options:['SSp-ul4', 'SSp-ul6b', 'SSp-ul5', 'SSp-ul2-3', 'SSp-ul1', 'SSP-ul6a'],
	nextQuestionMap:['SSp-ul4', 'SSp-ul6b', 'SSp-ul5', 'SSp-ul2-3', 'SSp-ul1', 'SSP-ul6a'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'SSp-n5',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, nose, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl2b',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part, layer 2b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISpm6b',
	category:CATEGORIES.PERSONAL,
	text:'posteromedial visual area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CBN',
	category:CATEGORIES.PERSONAL,
	text:'Cerebellar nuclei',
	inputType:'radio',
	options:['DN', 'FN', 'IP'],
	nextQuestionMap:['DN', 'FN', 'IP'],
	scoreMap:[0, 0, 0]
},{
	id:'CA1sp',
	category:CATEGORIES.PERSONAL,
	text:'Field CA1, pyramidal layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PSV',
	category:CATEGORIES.PERSONAL,
	text:'Principal sensory nucleus of the trigeminal',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'GPi',
	category:CATEGORIES.PERSONAL,
	text:'Globus pallidus, internal segment',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl2-3',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TTv3',
	category:CATEGORIES.PERSONAL,
	text:'Taenia tecta, ventral part, layer 3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SCH',
	category:CATEGORIES.PERSONAL,
	text:'Suprachiasmatic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DG',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus',
	inputType:'radio',
	options:['DG-sg', 'DGcr', 'DGlb', 'DGmb'],
	nextQuestionMap:['DG-sg', 'DGcr', 'DGlb', 'DGmb'],
	scoreMap:[0, 1, 1, 1]
},{
	id:'AUDpo6a',
	category:CATEGORIES.PERSONAL,
	text:'Posterior auditory area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPd',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, dorsal part',
	inputType:'radio',
	options:['RSPd4', 'RSPd6b', 'RSPd1', 'RSPd5', 'RSPd2-3', 'RSPd6a'],
	nextQuestionMap:['RSPd4', 'RSPd6b', 'RSPd1', 'RSPd5', 'RSPd2-3', 'RSPd6a'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'VISl1',
	category:CATEGORIES.PERSONAL,
	text:'Lateral visual area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVHpv',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, parvicellular division, periventricular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SNI',
	category:CATEGORIES.PERSONAL,
	text:'Substantia nigra, lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ABAMouse',
	category:CATEGORIES.PERSONAL,
	text:'Allen Brain Atlas Adult Mouse Ontology',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COApm',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part, medial zone',
	inputType:'radio',
	options:['COApm2', 'COApm3', 'COApm1-3', 'COApm1', 'COApm1-2'],
	nextQuestionMap:['COApm2', 'COApm3', 'COApm1-3', 'COApm1', 'COApm1-2'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'MEApv',
	category:CATEGORIES.PERSONAL,
	text:'Medial amygdalar nucleus, posteroventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BSTrh',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, anterior division, rhomboid nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CSl',
	category:CATEGORIES.PERSONAL,
	text:'Superior central nucleus raphe, lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl4',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CTX2-3',
	category:CATEGORIES.PERSONAL,
	text:'Cerebral cortex, layer 2-3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'My-sen',
	category:CATEGORIES.PERSONAL,
	text:'Medulla, sensory related',
	inputType:'radio',
	options:['CN', 'SPVO', 'NTS', 'ECU', 'SPVI', 'NTB', 'SPVC', 'DCN', 'z', 'AP'],
	nextQuestionMap:['CN', 'SPVO', 'NTS', 'ECU', 'SPVI', 'NTB', 'SPVC', 'DCN', 'z', 'AP'],
	scoreMap:[1, 1, 1, 0, 0, 0, 0, 1, 0, 0]
},{
	id:'RE',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of reunions',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'x',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus x',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'POL',
	category:CATEGORIES.PERSONAL,
	text:'Posterior limiting nucleus of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-tr4',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, trunk, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ILA5',
	category:CATEGORIES.PERSONAL,
	text:'Infralimbic area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VIS',
	category:CATEGORIES.PERSONAL,
	text:'Visual areas',
	inputType:'radio',
	options:['VIS6a', 'VIS4', 'VIS6b', 'VIS5', 'VISal', 'VIS1', 'VISpm', 'VISam', 'VISl', 'VISp', 'VISpl', 'VIS2-3'],
	nextQuestionMap:['VIS6a', 'VIS4', 'VIS6b', 'VIS5', 'VISal', 'VIS1', 'VISpm', 'VISam', 'VISl', 'VISp', 'VISpl', 'VIS2-3'],
	scoreMap:[0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0]
},{
	id:'SPFm',
	category:CATEGORIES.PERSONAL,
	text:'Subparafascicular nucleus, magnocellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CEAm',
	category:CATEGORIES.PERSONAL,
	text:'Central amygdalar nucleus, medial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MEAad',
	category:CATEGORIES.PERSONAL,
	text:'Medial amygdalar nucleus, anterodorsal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DGlb-sg',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus lateral blade, granule cell layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PH',
	category:CATEGORIES.PERSONAL,
	text:'Posterior hypothalamic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TEa5',
	category:CATEGORIES.PERSONAL,
	text:'Temporal association areas, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SCig-b',
	category:CATEGORIES.PERSONAL,
	text:'Superior colliculus, motor related, intermediate gray layer, sublayer b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MPNl',
	category:CATEGORIES.PERSONAL,
	text:'Medial preoptic nucleus, lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LHA',
	category:CATEGORIES.PERSONAL,
	text:'Lateral hypothalamic area',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISam',
	category:CATEGORIES.PERSONAL,
	text:'Anteromedial visual area',
	inputType:'radio',
	options:['VISam6b', 'VISam1', 'VISam2-3', 'VISam4', 'VISam5', 'VISam6a'],
	nextQuestionMap:['VISam6b', 'VISam1', 'VISam2-3', 'VISam4', 'VISam5', 'VISam6a'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'VISpl4',
	category:CATEGORIES.PERSONAL,
	text:'Posterolateral visual area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DMH',
	category:CATEGORIES.PERSONAL,
	text:'Dorsomedial nucleus of the hypothalamus',
	inputType:'radio',
	options:['DMHv', 'DMHp', 'DMHa'],
	nextQuestionMap:['DMHv', 'DMHp', 'DMHa'],
	scoreMap:[0, 0, 0]
},{
	id:'AONl',
	category:CATEGORIES.PERSONAL,
	text:'Anterior olfactory nucleus, lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'structure_graphs',
	category:CATEGORIES.PERSONAL,
	text:'structure_graphs',
	inputType:'radio',
	options:['ABAMouse'],
	nextQuestionMap:['ABAMouse'],
	scoreMap:[0]
},{
	id:'CLA',
	category:CATEGORIES.PERSONAL,
	text:'Claustrum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BSTal',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, anterior division, anterolateral area',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NDB',
	category:CATEGORIES.PERSONAL,
	text:'Diagonal band nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AAA',
	category:CATEGORIES.PERSONAL,
	text:'Anterior amygdalar area',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDd4',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal auditory area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'HPF',
	category:CATEGORIES.PERSONAL,
	text:'Hippocampal formation',
	inputType:'radio',
	options:['RHP', 'HIP'],
	nextQuestionMap:['RHP', 'HIP'],
	scoreMap:[1, 1]
},{
	id:'ENTm2b',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, dorsal zone, layer 2b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COApl1-2',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part, lateral zone, layer 1-2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VMHa',
	category:CATEGORIES.PERSONAL,
	text:'Ventromedial hypothalamic nucleus, anterior part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'FOTU',
	category:CATEGORIES.PERSONAL,
	text:'Folium-tuber vermis (VII)',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DP',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal peduncular area',
	inputType:'radio',
	options:['DP6a', 'DP2', 'DP1', 'DP2-3', 'DP5'],
	nextQuestionMap:['DP6a', 'DP2', 'DP1', 'DP2-3', 'DP5'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'CSm',
	category:CATEGORIES.PERSONAL,
	text:'Superior central nucleus raphe, medial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VPM',
	category:CATEGORIES.PERSONAL,
	text:'Ventral posteromedial nucleus of the thalamus',
	inputType:'radio',
	options:['VPMpc'],
	nextQuestionMap:['VPMpc'],
	scoreMap:[0]
},{
	id:'MOBmi',
	category:CATEGORIES.PERSONAL,
	text:'Main olfactory bulb, mitral layer]',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MPO',
	category:CATEGORIES.PERSONAL,
	text:'Medial preoptic area',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp4',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBm2',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, medial part, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVR',
	category:CATEGORIES.PERSONAL,
	text:'Periventricular region',
	inputType:'radio',
	options:['SBPV', 'PD', 'AHA', 'PS', 'OV', 'PSCH', 'SCH', 'DMH', 'MPO', 'PVpo', 'AVP', 'AVPV', 'ADP', 'MEPO', 'PVp', 'SFO', 'VLPO'],
	nextQuestionMap:['SBPV', 'PD', 'AHA', 'PS', 'OV', 'PSCH', 'SCH', 'DMH', 'MPO', 'PVpo', 'AVP', 'AVPV', 'ADP', 'MEPO', 'PVp', 'SFO', 'VLPO'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},{
	id:'VISC',
	category:CATEGORIES.PERSONAL,
	text:'Visceral area',
	inputType:'radio',
	options:['VISC4', 'VISC6b', 'VISC1', 'VISC5', 'VISC2-3', 'VISC6a'],
	nextQuestionMap:['VISC4', 'VISC6b', 'VISC1', 'VISC5', 'VISC2-3', 'VISC6a'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'PBmm',
	category:CATEGORIES.PERSONAL,
	text:'Parabrachial nucleus, medial division, medial medial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-ul2-3',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, upper limb, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LRN',
	category:CATEGORIES.PERSONAL,
	text:'Lateral reticular nucleus',
	inputType:'radio',
	options:['LRNp', 'LRNm'],
	nextQuestionMap:['LRNp', 'LRNm'],
	scoreMap:[0, 0]
},{
	id:'APN',
	category:CATEGORIES.PERSONAL,
	text:'Anterior pretectal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDp',
	category:CATEGORIES.PERSONAL,
	text:'Primary auditory area',
	inputType:'radio',
	options:['AUDp4', 'AUDp1', 'AUDp5', 'AUDp6b', 'AUDp2-3', 'AUDp6a'],
	nextQuestionMap:['AUDp4', 'AUDp1', 'AUDp5', 'AUDp6b', 'AUDp2-3', 'AUDp6a'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'AUDv',
	category:CATEGORIES.PERSONAL,
	text:'Ventral auditory area',
	inputType:'radio',
	options:['AUDv2-3', 'AUDv6a', 'AUDv6b', 'AUDv4', 'AUDv1', 'AUDv5'],
	nextQuestionMap:['AUDv2-3', 'AUDv6a', 'AUDv6b', 'AUDv4', 'AUDv1', 'AUDv5'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'PVHm',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, magnocellular division',
	inputType:'radio',
	options:['PVHpm', 'PVHmm', 'PVHam'],
	nextQuestionMap:['PVHpm', 'PVHmm', 'PVHam'],
	scoreMap:[0, 0, 0]
},{
	id:'ECU',
	category:CATEGORIES.PERSONAL,
	text:'External cuneate nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBl5',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, lateral part, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BSTdm',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, anterior division, dorsomedial nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACVII',
	category:CATEGORIES.PERSONAL,
	text:'Accessory facial motor nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NLOT1-3',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the lateral olfactory tract, layers 1-3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA2so',
	category:CATEGORIES.PERSONAL,
	text:'Field CA2, stratum oriens',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-n1',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, nose, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOs2-3',
	category:CATEGORIES.PERSONAL,
	text:'Secondary motor area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDp6b',
	category:CATEGORIES.PERSONAL,
	text:'Primary auditory area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDv6b',
	category:CATEGORIES.PERSONAL,
	text:'Ventral auditory area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISl',
	category:CATEGORIES.PERSONAL,
	text:'Lateral visual area',
	inputType:'radio',
	options:['VISl6a', 'VISl5', 'VISl1', 'VISl4', 'VISl6b', 'VISl2-3'],
	nextQuestionMap:['VISl6a', 'VISl5', 'VISl1', 'VISl4', 'VISl6b', 'VISl2-3'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'COPY',
	category:CATEGORIES.PERSONAL,
	text:'Copula pyramidis',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOs6b',
	category:CATEGORIES.PERSONAL,
	text:'Secondary motor area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACAv1',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, ventral part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SPVI',
	category:CATEGORIES.PERSONAL,
	text:'Spinal nucleus of the trigeminal, interpolar part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CENT3',
	category:CATEGORIES.PERSONAL,
	text:'Lobule III',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TRS',
	category:CATEGORIES.PERSONAL,
	text:'Triangular nucleus of septum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVHap',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, parvicellular division, anterior parvicellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'B',
	category:CATEGORIES.PERSONAL,
	text:'Barrington\'s nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MEApd-b',
	category:CATEGORIES.PERSONAL,
	text:'Medial amygdalar nucleus, posterodorsal part, sublayer b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AONm',
	category:CATEGORIES.PERSONAL,
	text:'Anterior olfactory nucleus, medial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NTB',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the trapezoid body',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACA6a',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp6b',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TR1',
	category:CATEGORIES.PERSONAL,
	text:'Postpiriform transition area, layers 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPagl2-3',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, lateral agranular part, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NLLh',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the lateral lemniscus, horizontal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'EMTmv5-6',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, ventral zone, layer 5/6',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl5-6',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part, layer 5/6',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'POST',
	category:CATEGORIES.PERSONAL,
	text:'Postsubiculum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPd2-3',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, dorsal part, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CBX',
	category:CATEGORIES.PERSONAL,
	text:'Cerebellar cortex',
	inputType:'radio',
	options:['VERM', 'HEM', 'CBXpu', 'CBXgr'],
	nextQuestionMap:['VERM', 'HEM', 'CBXpu', 'CBXgr'],
	scoreMap:[1, 1, 0, 0]
},{
	id:'SLD',
	category:CATEGORIES.PERSONAL,
	text:'Sublaterodorsal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISp',
	category:CATEGORIES.PERSONAL,
	text:'Primary visual area',
	inputType:'radio',
	options:['VISp5', 'VISp2-3', 'VISp6b', 'VISp4', 'VISp1', 'VISp6a'],
	nextQuestionMap:['VISp5', 'VISp2-3', 'VISp6b', 'VISp4', 'VISp1', 'VISp6a'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'SSp5',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RT',
	category:CATEGORIES.PERSONAL,
	text:'Reticular nucleus of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DGlb',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus lateral blade',
	inputType:'radio',
	options:['DGlb-mo', 'DGlb-po', 'DGlb-sg'],
	nextQuestionMap:['DGlb-mo', 'DGlb-po', 'DGlb-sg'],
	scoreMap:[0, 0, 0]
},{
	id:'TEa2-3',
	category:CATEGORIES.PERSONAL,
	text:'Temporal association areas, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVpo',
	category:CATEGORIES.PERSONAL,
	text:'Periventricular hypothalamic nucleus, preoptic part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'GR',
	category:CATEGORIES.PERSONAL,
	text:'Gracile nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PR',
	category:CATEGORIES.PERSONAL,
	text:'Perireunensis nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LGd',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal part of the lateral geniculate complex',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PAL',
	category:CATEGORIES.PERSONAL,
	text:'Pallidum',
	inputType:'radio',
	options:['PALv', 'PALm', 'PALc', 'PALd'],
	nextQuestionMap:['PALv', 'PALm', 'PALc', 'PALd'],
	scoreMap:[1, 1, 1, 1]
},{
	id:'DGmb-sg',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus medial blade, granule cell layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-ll',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, lower limb',
	inputType:'radio',
	options:['SSp-ll6b', 'SSp-ll5', 'SSp-ll2-3', 'SSp-ll1', 'SSp-ll6a', 'SSp-ll4'],
	nextQuestionMap:['SSp-ll6b', 'SSp-ll5', 'SSp-ll2-3', 'SSp-ll1', 'SSp-ll6a', 'SSp-ll4'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'RSPagl6b',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, lateral agranular part, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDp2-3',
	category:CATEGORIES.PERSONAL,
	text:'Primary auditory area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISpl5',
	category:CATEGORIES.PERSONAL,
	text:'Posterolateral visual area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TEa1',
	category:CATEGORIES.PERSONAL,
	text:'Temporal association areas, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOp',
	category:CATEGORIES.PERSONAL,
	text:'Primary motor area',
	inputType:'radio',
	options:['MOp5', 'MOp2-3', 'MOp1', 'MOp6b', 'MOp6a'],
	nextQuestionMap:['MOp5', 'MOp2-3', 'MOp1', 'MOp6b', 'MOp6a'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'ORBl1',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, lateral part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISC2-3',
	category:CATEGORIES.PERSONAL,
	text:'Visceral area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'P-sen',
	category:CATEGORIES.PERSONAL,
	text:'Pons, sensory related',
	inputType:'radio',
	options:['SOC', 'NLL', 'PB', 'PSV'],
	nextQuestionMap:['SOC', 'NLL', 'PB', 'PSV'],
	scoreMap:[1, 1, 1, 0]
},{
	id:'PTLp',
	category:CATEGORIES.PERSONAL,
	text:'Posterior parietal association areas',
	inputType:'radio',
	options:['PTLp1', 'PTLp6b', 'PTLp2-3', 'PTLp4', 'PTLp6a', 'PTLp5'],
	nextQuestionMap:['PTLp1', 'PTLp6b', 'PTLp2-3', 'PTLp4', 'PTLp6a', 'PTLp5'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'FS',
	category:CATEGORIES.PERSONAL,
	text:'Fundus of striatum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSs6a',
	category:CATEGORIES.PERSONAL,
	text:'Supplemental somatosensory area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'HB',
	category:CATEGORIES.PERSONAL,
	text:'Hindbrain',
	inputType:'radio',
	options:['MY', 'P'],
	nextQuestionMap:['MY', 'P'],
	scoreMap:[1, 1]
},{
	id:'ILA6b',
	category:CATEGORIES.PERSONAL,
	text:'Infralimbic area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NPC',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the posterior commissure',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDd1',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal auditory area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACAd',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, dorsal part',
	inputType:'radio',
	options:['ACAd1', 'ACAd6a', 'ACAd5', 'ACAd2-3', 'ACAd6b'],
	nextQuestionMap:['ACAd1', 'ACAd6a', 'ACAd5', 'ACAd2-3', 'ACAd6b'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'BAC',
	category:CATEGORIES.PERSONAL,
	text:'Bed nucleus of the anterior commissure',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PTLp6b',
	category:CATEGORIES.PERSONAL,
	text:'Posterior parietal association areas, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LSr',
	category:CATEGORIES.PERSONAL,
	text:'Lateral septal nucleus, rostral (rostroventral) part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SI',
	category:CATEGORIES.PERSONAL,
	text:'Substantia innominata',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDv4',
	category:CATEGORIES.PERSONAL,
	text:'Ventral auditory area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LA',
	category:CATEGORIES.PERSONAL,
	text:'Lateral amygdalar nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AHN',
	category:CATEGORIES.PERSONAL,
	text:'Anterior hypothalamic nucleus',
	inputType:'radio',
	options:['AHNp', 'AHNd', 'AHNa', 'AHNc'],
	nextQuestionMap:['AHNp', 'AHNd', 'AHNa', 'AHNc'],
	scoreMap:[0, 0, 0, 0]
},{
	id:'PFL',
	category:CATEGORIES.PERSONAL,
	text:'Paraflocculus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IO',
	category:CATEGORIES.PERSONAL,
	text:'Inferior olivary complex',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RO',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus raphe obscurus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SPVC',
	category:CATEGORIES.PERSONAL,
	text:'Spinal nucleus of the trigeminal, caudal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MRNp',
	category:CATEGORIES.PERSONAL,
	text:'Midbrain reticular nucleus, parvicellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SCig-c',
	category:CATEGORIES.PERSONAL,
	text:'Superior colliculus, motor related, intermediate gray layer, sublayer c',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PIR3',
	category:CATEGORIES.PERSONAL,
	text:'Piriform area, polymorph layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA3slm',
	category:CATEGORIES.PERSONAL,
	text:'Field CA3, stratum lacunosum-moleculare',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PALm',
	category:CATEGORIES.PERSONAL,
	text:'Pallidum, medial region',
	inputType:'radio',
	options:['TRS', 'MSC'],
	nextQuestionMap:['TRS', 'MSC'],
	scoreMap:[0, 1]
},{
	id:'DMHa',
	category:CATEGORIES.PERSONAL,
	text:'Dorsomedial nucleus of the hypothalamus, anterior part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VCO',
	category:CATEGORIES.PERSONAL,
	text:'Ventral cochlear nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VPMpc',
	category:CATEGORIES.PERSONAL,
	text:'Ventral posteromedial nucleus of the thalamus, parvicellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VI',
	category:CATEGORIES.PERSONAL,
	text:'Abducens nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA3so',
	category:CATEGORIES.PERSONAL,
	text:'Field CA3, stratum oriens',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AVP',
	category:CATEGORIES.PERSONAL,
	text:'Anteroventral preoptic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'EV',
	category:CATEGORIES.PERSONAL,
	text:'Efferent vestibular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'FN',
	category:CATEGORIES.PERSONAL,
	text:'Fastigial nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISp2-3',
	category:CATEGORIES.PERSONAL,
	text:'Primary visual area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TTv1-3',
	category:CATEGORIES.PERSONAL,
	text:'Taenia tecta, ventral part, layers 1-3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TTd3',
	category:CATEGORIES.PERSONAL,
	text:'Taenia tecta, dorsal part, layers 3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PARN',
	category:CATEGORIES.PERSONAL,
	text:'Parvicellular reticular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AONpv',
	category:CATEGORIES.PERSONAL,
	text:'Anterior olfactory nucleus, posteroventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BSTif',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, posterior division, interfascicular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DP5',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal peduncular area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'GU6a',
	category:CATEGORIES.PERSONAL,
	text:'Gustatory areas, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PRNv',
	category:CATEGORIES.PERSONAL,
	text:'Pontine reticular nucleus, ventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBl',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, lateral part',
	inputType:'radio',
	options:['ORBl6a', 'ORBl6b', 'ORBl5', 'ORBl1', 'ORBl2-3'],
	nextQuestionMap:['ORBl6a', 'ORBl6b', 'ORBl5', 'ORBl1', 'ORBl2-3'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'Isocortex',
	category:CATEGORIES.PERSONAL,
	text:'Isocortex',
	inputType:'radio',
	options:['AUD', 'RSP', 'FRP', 'ECT', 'AI', 'ACA', 'MO', 'PL', 'GU', 'VIS', 'VISC', 'PTLp', 'PERI', 'SS', 'TEa', 'ILA', 'ORB'],
	nextQuestionMap:['AUD', 'RSP', 'FRP', 'ECT', 'AI', 'ACA', 'MO', 'PL', 'GU', 'VIS', 'VISC', 'PTLp', 'PERI', 'SS', 'TEa', 'ILA', 'ORB'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},{
	id:'ORBvl6b',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, ventrolateral part, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NLOT',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the lateral olfactory tract',
	inputType:'radio',
	options:['NLOT1', 'NLOT1-3', 'NLOT2', 'NLOT3'],
	nextQuestionMap:['NLOT1', 'NLOT1-3', 'NLOT2', 'NLOT3'],
	scoreMap:[0, 0, 0, 0]
},{
	id:'SH',
	category:CATEGORIES.PERSONAL,
	text:'Septohippocampal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TTd1-4',
	category:CATEGORIES.PERSONAL,
	text:'Taenia tecta, dorsal part, layers 1-4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTm4',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, dorsal zone, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SPVOmdmv',
	category:CATEGORIES.PERSONAL,
	text:'Spinal nucleus of the trigeminal, oral part, middle dorsomedial part, ventral zone',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MBO',
	category:CATEGORIES.PERSONAL,
	text:'Mammillary body',
	inputType:'radio',
	options:['MM', 'SUM', 'TM', 'LM'],
	nextQuestionMap:['MM', 'SUM', 'TM', 'LM'],
	scoreMap:[1, 1, 1, 0]
},{
	id:'CA2slm',
	category:CATEGORIES.PERSONAL,
	text:'Field CA2, stratum lacunosum-moleculare',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PTLp2-3',
	category:CATEGORIES.PERSONAL,
	text:'Posterior parietal association areas, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISpl',
	category:CATEGORIES.PERSONAL,
	text:'Posterolateral visual area',
	inputType:'radio',
	options:['VISpl6b', 'VISpl1', 'VISpl2-3', 'VISpl4', 'VISpl5', 'VISpl6a'],
	nextQuestionMap:['VISpl6b', 'VISpl1', 'VISpl2-3', 'VISpl4', 'VISpl5', 'VISpl6a'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'RSPagl5',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, lateral agranular part, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PL5',
	category:CATEGORIES.PERSONAL,
	text:'Prelimbic area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CTX6a',
	category:CATEGORIES.PERSONAL,
	text:'Cerebral cortex, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl2a',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part, layer 2a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NLLd',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the lateral lemniscus, dorsal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PAA1-3',
	category:CATEGORIES.PERSONAL,
	text:'Piriform-amygdalar area, layers 1-3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ECT6b',
	category:CATEGORIES.PERSONAL,
	text:'Ectorhinal area/Layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PTLp4',
	category:CATEGORIES.PERSONAL,
	text:'Posterior parietal association areas, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPv2-3',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, ventral part, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DCN',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal column nuclei',
	inputType:'radio',
	options:['CU', 'GR'],
	nextQuestionMap:['CU', 'GR'],
	scoreMap:[0, 0]
},{
	id:'ENTmv',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, ventral zone',
	inputType:'radio',
	options:['EMTmv1', 'EMTmv2', 'EMTmv5-6', 'EMTmv3', 'EMTmv4'],
	nextQuestionMap:['EMTmv1', 'EMTmv2', 'EMTmv5-6', 'EMTmv3', 'EMTmv4'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'NLOT2',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the lateral olfactory tract, pyramidal layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TR1-3',
	category:CATEGORIES.PERSONAL,
	text:'Postpiriform transition area, layers 1-3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp1',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BSTv',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, anterior division, ventral nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IP',
	category:CATEGORIES.PERSONAL,
	text:'Interposed nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AOBgl',
	category:CATEGORIES.PERSONAL,
	text:'Accessory olfactory bulb, glomerular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'y',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus y',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISpm4',
	category:CATEGORIES.PERSONAL,
	text:'posteromedial visual area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TTd4',
	category:CATEGORIES.PERSONAL,
	text:'Taenia tecta, dorsal part, layers 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SO',
	category:CATEGORIES.PERSONAL,
	text:'Supraoptic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ECT5',
	category:CATEGORIES.PERSONAL,
	text:'Ectorhinal area/Layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOs1',
	category:CATEGORIES.PERSONAL,
	text:'Secondary motor area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CENT',
	category:CATEGORIES.PERSONAL,
	text:'Central lobule',
	inputType:'radio',
	options:['CENT2', 'CENT3'],
	nextQuestionMap:['CENT2', 'CENT3'],
	scoreMap:[0, 0]
},{
	id:'SCzo',
	category:CATEGORIES.PERSONAL,
	text:'Superior colliculus, zonal layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COApl2',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part, lateral zone, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'GU6b',
	category:CATEGORIES.PERSONAL,
	text:'Gustatory areas, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BSTam',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, anterior division, anteromedial area]',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDd5',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal auditory area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LRNm',
	category:CATEGORIES.PERSONAL,
	text:'Lateral reticular nucleus, magnocellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'Mmme',
	category:CATEGORIES.PERSONAL,
	text:'Medial mammillary nucleus, median part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TR',
	category:CATEGORIES.PERSONAL,
	text:'Postpiriform transition area',
	inputType:'radio',
	options:['TR2', 'TR1', 'TR1-3', 'TR3'],
	nextQuestionMap:['TR2', 'TR1', 'TR1-3', 'TR3'],
	scoreMap:[0, 0, 0, 0]
},{
	id:'SUT',
	category:CATEGORIES.PERSONAL,
	text:'Supratrigeminal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ILA1',
	category:CATEGORIES.PERSONAL,
	text:'Infralimbic area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl1',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SUV',
	category:CATEGORIES.PERSONAL,
	text:'Superior vestibular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PBlc',
	category:CATEGORIES.PERSONAL,
	text:'Parabrachial nucleus, lateral division, central lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PCN',
	category:CATEGORIES.PERSONAL,
	text:'Paracentral nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPv2',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, ventral part, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BSTd',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, posterior division, dorsal nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LIN',
	category:CATEGORIES.PERSONAL,
	text:'Linear nucleus of the medulla',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PERI',
	category:CATEGORIES.PERSONAL,
	text:'Perirhinal area',
	inputType:'radio',
	options:['PERI1', 'PERI5', 'PERI6a', 'PERI2-3', 'PERI6b'],
	nextQuestionMap:['PERI1', 'PERI5', 'PERI6a', 'PERI2-3', 'PERI6b'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'ORBm',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, medial part',
	inputType:'radio',
	options:['ORBm1', 'ORBm5', 'ORBm2', 'ORBm6a', 'ORBm2-3'],
	nextQuestionMap:['ORBm1', 'ORBm5', 'ORBm2', 'ORBm6a', 'ORBm2-3'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'SSp-ll6a',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, lower limb, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PRM',
	category:CATEGORIES.PERSONAL,
	text:'Paramedian lobule',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AVPV',
	category:CATEGORIES.PERSONAL,
	text:'Anteroventral periventricular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SCop',
	category:CATEGORIES.PERSONAL,
	text:'Superior colliculus, optic layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ADP',
	category:CATEGORIES.PERSONAL,
	text:'Anterodorsal preoptic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVHd',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, descending division',
	inputType:'radio',
	options:['PVHmpv', 'PVHlp', 'PVHdp', 'PVHf'],
	nextQuestionMap:['PVHmpv', 'PVHlp', 'PVHdp', 'PVHf'],
	scoreMap:[0, 0, 0, 0]
},{
	id:'VISpm2-3',
	category:CATEGORIES.PERSONAL,
	text:'posteromedial visual area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IA',
	category:CATEGORIES.PERSONAL,
	text:'Intercalated amygdalar nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISam6a',
	category:CATEGORIES.PERSONAL,
	text:'Anteromedial visual area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AIv',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, ventral part',
	inputType:'radio',
	options:['AIv6a', 'AIv5', 'AIv1', 'AIv6b', 'AIv2-3'],
	nextQuestionMap:['AIv6a', 'AIv5', 'AIv1', 'AIv6b', 'AIv2-3'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'DCO',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal cochlear nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SCig',
	category:CATEGORIES.PERSONAL,
	text:'Superior colliculus, motor related, intermediate gray layer',
	inputType:'radio',
	options:['SCig-a', 'SCig-b', 'SCig-c'],
	nextQuestionMap:['SCig-a', 'SCig-b', 'SCig-c'],
	scoreMap:[0, 0, 0]
},{
	id:'MPNm',
	category:CATEGORIES.PERSONAL,
	text:'Medial preoptic nucleus, medial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TH',
	category:CATEGORIES.PERSONAL,
	text:'Thalamus',
	inputType:'radio',
	options:['DORpm', 'DORsm'],
	nextQuestionMap:['DORpm', 'DORsm'],
	scoreMap:[1, 1]
},{
	id:'EMTmv3',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, ventral zone, layer 3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBl2-3',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, lateral part, layer 2-3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOBgl',
	category:CATEGORIES.PERSONAL,
	text:'Main olfactory bulb, glomerular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-ll4',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, lower limb, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-m',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, mouth',
	inputType:'radio',
	options:['SSp-m4', 'SSp-m2-3', 'SSp-m5', 'SSp-m6a', 'SSp-m1', 'SSp-m6b'],
	nextQuestionMap:['SSp-m4', 'SSp-m2-3', 'SSp-m5', 'SSp-m6a', 'SSp-m1', 'SSp-m6b'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'PERI6a',
	category:CATEGORIES.PERSONAL,
	text:'Perirhinal area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBm6a',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, medial part, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PRNr',
	category:CATEGORIES.PERSONAL,
	text:'Pontine reticular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MED',
	category:CATEGORIES.PERSONAL,
	text:'Medial group of the dorsal thalamus',
	inputType:'radio',
	options:['MD', 'IMD', 'SMT', 'PR'],
	nextQuestionMap:['MD', 'IMD', 'SMT', 'PR'],
	scoreMap:[1, 0, 0, 0]
},{
	id:'PHY',
	category:CATEGORIES.PERSONAL,
	text:'Perihypoglossal nuclei',
	inputType:'radio',
	options:['PRP', 'NIS', 'NR'],
	nextQuestionMap:['PRP', 'NIS', 'NR'],
	scoreMap:[0, 0, 0]
},{
	id:'CUL4-5',
	category:CATEGORIES.PERSONAL,
	text:'Lobules IV-V',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISp6b',
	category:CATEGORIES.PERSONAL,
	text:'Primary visual area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TM',
	category:CATEGORIES.PERSONAL,
	text:'Tuberomammillary nucleus',
	inputType:'radio',
	options:['TMd', 'TMv'],
	nextQuestionMap:['TMd', 'TMv'],
	scoreMap:[0, 0]
},{
	id:'EPd',
	category:CATEGORIES.PERSONAL,
	text:'Endopiriform nucleus, dorsal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IF',
	category:CATEGORIES.PERSONAL,
	text:'Interfascicular nucleus raphe',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COApm1-2',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part, medial zone, layer 1-2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ASO',
	category:CATEGORIES.PERSONAL,
	text:'Accessory supraoptic group',
	inputType:'radio',
	options:['NC'],
	nextQuestionMap:['NC'],
	scoreMap:[0]
},{
	id:'LTN',
	category:CATEGORIES.PERSONAL,
	text:'Lateral tegmental nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'z',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus z',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PBld',
	category:CATEGORIES.PERSONAL,
	text:'Parabrachial nucleus, lateral division, dorsal lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AMB',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus ambiguus',
	inputType:'radio',
	options:['AMBd', 'AMBv'],
	nextQuestionMap:['AMBd', 'AMBv'],
	scoreMap:[0, 0]
},{
	id:'MEPO',
	category:CATEGORIES.PERSONAL,
	text:'Median preoptic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TMv',
	category:CATEGORIES.PERSONAL,
	text:'Tuberomammillary nucleus, ventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'GU1',
	category:CATEGORIES.PERSONAL,
	text:'Gustatory areas, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BSTse',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, posterior division, strial extension',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl6a',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTm1',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, dorsal zone, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ILA2',
	category:CATEGORIES.PERSONAL,
	text:'Infralimbic area, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISl4',
	category:CATEGORIES.PERSONAL,
	text:'Lateral visual area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACAv',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, ventral part',
	inputType:'radio',
	options:['ACAv6b', 'ACAv5', 'ACAv6a', 'ACAv1', 'ACAv2-3'],
	nextQuestionMap:['ACAv6b', 'ACAv5', 'ACAv6a', 'ACAv1', 'ACAv2-3'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'SS',
	category:CATEGORIES.PERSONAL,
	text:'Somatosensory areas',
	inputType:'radio',
	options:['SSp', 'SSs'],
	nextQuestionMap:['SSp', 'SSs'],
	scoreMap:[1, 1]
},{
	id:'VIS2-3',
	category:CATEGORIES.PERSONAL,
	text:'Visual areas, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'P-mot',
	category:CATEGORIES.PERSONAL,
	text:'Pons, motor related',
	inputType:'radio',
	options:['PCG', 'TRN', 'PG', 'SSN', 'V', 'SG', 'PRNc', 'B', 'PRNv', 'SUT', 'LTN', 'DTN'],
	nextQuestionMap:['PCG', 'TRN', 'PG', 'SSN', 'V', 'SG', 'PRNc', 'B', 'PRNv', 'SUT', 'LTN', 'DTN'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},{
	id:'SSp-ul1',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, upper limb, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ANcr2',
	category:CATEGORIES.PERSONAL,
	text:'Crus 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SubG',
	category:CATEGORIES.PERSONAL,
	text:'Subgeniculate nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MDRNv',
	category:CATEGORIES.PERSONAL,
	text:'Medullary reticular nucleus, ventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'sAMY',
	category:CATEGORIES.PERSONAL,
	text:'Striatum-like amygdalar nuclei',
	inputType:'radio',
	options:['BA', 'MEA', 'CEA', 'AAA', 'IA'],
	nextQuestionMap:['BA', 'MEA', 'CEA', 'AAA', 'IA'],
	scoreMap:[0, 1, 1, 0, 0]
},{
	id:'BSTmg',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, anterior division, magnocellular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MSC',
	category:CATEGORIES.PERSONAL,
	text:'Medial septal complex',
	inputType:'radio',
	options:['MS', 'NDB'],
	nextQuestionMap:['MS', 'NDB'],
	scoreMap:[0, 0]
},{
	id:'VISp4',
	category:CATEGORIES.PERSONAL,
	text:'Primary visual area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVp',
	category:CATEGORIES.PERSONAL,
	text:'Periventricular hypothalamic nucleus, posterior part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISpl6a',
	category:CATEGORIES.PERSONAL,
	text:'Posterolateral visual area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl5',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MRN',
	category:CATEGORIES.PERSONAL,
	text:'Midbrain reticular nucleus',
	inputType:'radio',
	options:['MRNmg', 'MRNm', 'MRNp'],
	nextQuestionMap:['MRNmg', 'MRNm', 'MRNp'],
	scoreMap:[0, 0, 0]
},{
	id:'VP',
	category:CATEGORIES.PERSONAL,
	text:'Ventral posterior complex of the thalamus',
	inputType:'radio',
	options:['VPL', 'VPM', 'VPLpc'],
	nextQuestionMap:['VPL', 'VPM', 'VPLpc'],
	scoreMap:[0, 1, 0]
},{
	id:'SSp-bfd6b',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, barrel field, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PIN',
	category:CATEGORIES.PERSONAL,
	text:'Pineal body',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LPO',
	category:CATEGORIES.PERSONAL,
	text:'Lateral preoptic area',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AONe',
	category:CATEGORIES.PERSONAL,
	text:'Anterior olfactory nucleus, external part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CTXsp6b',
	category:CATEGORIES.PERSONAL,
	text:'Layer 6b, isocortex',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ECO',
	category:CATEGORIES.PERSONAL,
	text:'Efferent vestibular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PBle',
	category:CATEGORIES.PERSONAL,
	text:'Parabrachial nucleus, lateral division, external lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACA5',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SUBd-m',
	category:CATEGORIES.PERSONAL,
	text:'Subiculum, dorsal part, molecular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COA',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area',
	inputType:'radio',
	options:['COAp', 'COAa'],
	nextQuestionMap:['COAp', 'COAa'],
	scoreMap:[1, 1]
},{
	id:'CUN',
	category:CATEGORIES.PERSONAL,
	text:'Cuneiform nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TEa',
	category:CATEGORIES.PERSONAL,
	text:'Temporal association areas',
	inputType:'radio',
	options:['TEa6a', 'TEa6b', 'TEa4', 'TEa5', 'TEa2-3', 'TEa1'],
	nextQuestionMap:['TEa6a', 'TEa6b', 'TEa4', 'TEa5', 'TEa2-3', 'TEa1'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'PVHf',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, descending division, forniceal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IB',
	category:CATEGORIES.PERSONAL,
	text:'Interbrain',
	inputType:'radio',
	options:['TH', 'HY'],
	nextQuestionMap:['TH', 'HY'],
	scoreMap:[1, 1]
},{
	id:'PERI2-3',
	category:CATEGORIES.PERSONAL,
	text:'Perirhinal area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBm2-3',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, medial part, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AIp6a',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, posterior part, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RPA',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus raphe pallidus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACAv2-3',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, ventral part, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISpm5',
	category:CATEGORIES.PERSONAL,
	text:'posteromedial visual area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DGcr-sg',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus crest, granule cell layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COApl3',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part, lateral zone, layer 3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MA',
	category:CATEGORIES.PERSONAL,
	text:'Magnocellular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LDT',
	category:CATEGORIES.PERSONAL,
	text:'Laterodorsal tegmental nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AId6a',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, dorsal part, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IG',
	category:CATEGORIES.PERSONAL,
	text:'Induseum griseum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PPYs',
	category:CATEGORIES.PERSONAL,
	text:'Parapyramidal nucleus, superficial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDv1',
	category:CATEGORIES.PERSONAL,
	text:'Ventral auditory area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AONd',
	category:CATEGORIES.PERSONAL,
	text:'Anterior olfactory nucleus, dorsal part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTm5',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, dorsal zone, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-m6b',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, mouth, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VPLpc',
	category:CATEGORIES.PERSONAL,
	text:'Ventral posterolateral nucleus of the thalamus, parvicellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PVHam',
	category:CATEGORIES.PERSONAL,
	text:'Paraventricular hypothalamic nucleus, magnocellular division, anterior magnocellular part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORB2-3',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IAM',
	category:CATEGORIES.PERSONAL,
	text:'Interanteromedial nucleus of the thalamus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RL',
	category:CATEGORIES.PERSONAL,
	text:'Rostral linear nucleus raphe',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DORsm',
	category:CATEGORIES.PERSONAL,
	text:'Thalamus, sensory-motor cortex related',
	inputType:'radio',
	options:['SPA', 'VENT', 'SPF', 'GENd', 'PP'],
	nextQuestionMap:['SPA', 'VENT', 'SPF', 'GENd', 'PP'],
	scoreMap:[0, 1, 1, 1, 0]
},{
	id:'FRP1',
	category:CATEGORIES.PERSONAL,
	text:'Frontal pole, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SPIV',
	category:CATEGORIES.PERSONAL,
	text:'Spinal vestibular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SIM',
	category:CATEGORIES.PERSONAL,
	text:'Simple lobule',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'EMTmv4',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, medial part, ventral zone, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'III',
	category:CATEGORIES.PERSONAL,
	text:'Oculomotor nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COAa1',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, anterior part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PERI6b',
	category:CATEGORIES.PERSONAL,
	text:'Perirhinal area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORBvl1',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, ventrolateral part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AHNa',
	category:CATEGORIES.PERSONAL,
	text:'Anterior hypothalamic nucleus, anterior part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PALc',
	category:CATEGORIES.PERSONAL,
	text:'Pallidum, caudal region',
	inputType:'radio',
	options:['BST', 'BAC'],
	nextQuestionMap:['BST', 'BAC'],
	scoreMap:[1, 0]
},{
	id:'PBme',
	category:CATEGORIES.PERSONAL,
	text:'Parabrachial nucleus, medial division, external medial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LGv',
	category:CATEGORIES.PERSONAL,
	text:'Ventral part of the lateral geniculate complex',
	inputType:'radio',
	options:['LGvl', 'LGvm'],
	nextQuestionMap:['LGvl', 'LGvm'],
	scoreMap:[0, 0]
},{
	id:'BSTa',
	category:CATEGORIES.PERSONAL,
	text:'Bed nuclei of the stria terminalis, anterior division',
	inputType:'radio',
	options:['BSTju', 'BSTfu', 'BSTov', 'BSTrh', 'BSTal', 'BSTdm', 'BSTv', 'BSTam', 'BSTmg'],
	nextQuestionMap:['BSTju', 'BSTfu', 'BSTov', 'BSTrh', 'BSTal', 'BSTdm', 'BSTv', 'BSTam', 'BSTmg'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
},{
	id:'ACVI',
	category:CATEGORIES.PERSONAL,
	text:'Accessory abducens nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'STR',
	category:CATEGORIES.PERSONAL,
	text:'Striatum',
	inputType:'radio',
	options:['STRd', 'STRv', 'LSX', 'sAMY'],
	nextQuestionMap:['STRd', 'STRv', 'LSX', 'sAMY'],
	scoreMap:[1, 1, 1, 1]
},{
	id:'ACAd2-3',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, dorsal part, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-tr',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, trunk',
	inputType:'radio',
	options:['SSp-tr2-3', 'SSp-tr6a', 'SSp-tr1', 'SSp-tr4', 'SSp-tr6b', 'SSp-tr5'],
	nextQuestionMap:['SSp-tr2-3', 'SSp-tr6a', 'SSp-tr1', 'SSp-tr4', 'SSp-tr6b', 'SSp-tr5'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'RSPagl1',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, lateral agranular part, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA3sp',
	category:CATEGORIES.PERSONAL,
	text:'Field CA3, pyramidal layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-bfd6a',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, barrel field, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOs',
	category:CATEGORIES.PERSONAL,
	text:'Secondary motor area',
	inputType:'radio',
	options:['MOs5', 'MOs6a', 'MOs2-3', 'MOs6b', 'MOs1'],
	nextQuestionMap:['MOs5', 'MOs6a', 'MOs2-3', 'MOs6b', 'MOs1'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'AUDpo6b',
	category:CATEGORIES.PERSONAL,
	text:'Posterior auditory area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-bfd4',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, barrel field, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'RSPd6a',
	category:CATEGORIES.PERSONAL,
	text:'Retrosplenial area, dorsal part, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDpo1',
	category:CATEGORIES.PERSONAL,
	text:'Posterior auditory area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CTX4',
	category:CATEGORIES.PERSONAL,
	text:'Cerebral cortex, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COApl1-3',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part, lateral zone, layer 1-3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MY-sat',
	category:CATEGORIES.PERSONAL,
	text:'Medulla, behavioral state related',
	inputType:'radio',
	options:['RM', 'RO', 'RPA'],
	nextQuestionMap:['RM', 'RO', 'RPA'],
	scoreMap:[0, 0, 0]
},{
	id:'SSP-ul6a',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, upper limb, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LAV',
	category:CATEGORIES.PERSONAL,
	text:'Lateral vestibular nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'BLAa',
	category:CATEGORIES.PERSONAL,
	text:'Basolateral amygdalar nucleus, anterior part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISl6b',
	category:CATEGORIES.PERSONAL,
	text:'Lateral visual area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AP',
	category:CATEGORIES.PERSONAL,
	text:'Area postrema',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDp6a',
	category:CATEGORIES.PERSONAL,
	text:'Primary auditory area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CNlam',
	category:CATEGORIES.PERSONAL,
	text:'Granular lamina of the cochlear nuclei',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DMX',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal motor nucleus of the vagus nerve',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISC6a',
	category:CATEGORIES.PERSONAL,
	text:'Visceral area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PSTN',
	category:CATEGORIES.PERSONAL,
	text:'Parasubthalamic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISp1',
	category:CATEGORIES.PERSONAL,
	text:'Primary visual area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PALd',
	category:CATEGORIES.PERSONAL,
	text:'Pallidum, dorsal region',
	inputType:'radio',
	options:['GPe', 'GPi'],
	nextQuestionMap:['GPe', 'GPi'],
	scoreMap:[0, 0]
},{
	id:'MOBopl',
	category:CATEGORIES.PERSONAL,
	text:'Main olfactory bulb, outer plexiform layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CA2sp',
	category:CATEGORIES.PERSONAL,
	text:'Field CA2, pyramidal layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IC',
	category:CATEGORIES.PERSONAL,
	text:'Inferior colliculus',
	inputType:'radio',
	options:['ICc', 'ICe', 'ICd'],
	nextQuestionMap:['ICc', 'ICe', 'ICd'],
	scoreMap:[0, 0, 0]
},{
	id:'VISal6b',
	category:CATEGORIES.PERSONAL,
	text:'Anterolateral visual area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PBmv',
	category:CATEGORIES.PERSONAL,
	text:'Parabrachial nucleus, medial division, ventral medial part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-tr6b',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, trunk, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOBipl',
	category:CATEGORIES.PERSONAL,
	text:'Main olfactory bulb, inner plexiform layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl4-5',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part, layer 4/5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VMH',
	category:CATEGORIES.PERSONAL,
	text:'Ventromedial hypothalamic nucleus',
	inputType:'radio',
	options:['VMHdm', 'VMHvl', 'VMHc', 'VMHa'],
	nextQuestionMap:['VMHdm', 'VMHvl', 'VMHc', 'VMHa'],
	scoreMap:[0, 0, 0, 0]
},{
	id:'MO2-3',
	category:CATEGORIES.PERSONAL,
	text:'Somatomotor areas, Layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'INV',
	category:CATEGORIES.PERSONAL,
	text:'Interstitial nucleus of the vestibular nerve',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'Brain',
	category:CATEGORIES.PERSONAL,
	text:'Basic cell groups and regions',
	inputType:'radio',
	options:['CH', 'CB', 'BS'],
	nextQuestionMap:['CH', 'CB', 'BS'],
	scoreMap:[1, 1, 1]
},{
	id:'VISl2-3',
	category:CATEGORIES.PERSONAL,
	text:'Lateral visual area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LH',
	category:CATEGORIES.PERSONAL,
	text:'Lateral habenula',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PMd',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal premammillary nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MEApd',
	category:CATEGORIES.PERSONAL,
	text:'Medial amygdalar nucleus, posterodorsal part',
	inputType:'radio',
	options:['MEApd-c', 'MEApd-a', 'MEApd-b'],
	nextQuestionMap:['MEApd-c', 'MEApd-a', 'MEApd-b'],
	scoreMap:[0, 0, 0]
},{
	id:'AUDv5',
	category:CATEGORIES.PERSONAL,
	text:'Ventral auditory area, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'IV',
	category:CATEGORIES.PERSONAL,
	text:'Trochlear nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LC',
	category:CATEGORIES.PERSONAL,
	text:'Locus ceruleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LGvl',
	category:CATEGORIES.PERSONAL,
	text:'Ventral part of the lateral geniculate complex, lateral zone',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACA6b',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ILM',
	category:CATEGORIES.PERSONAL,
	text:'Intralaminar nuclei of the dorsal thalamus',
	inputType:'radio',
	options:['PF', 'CM', 'CL', 'RH', 'PCN'],
	nextQuestionMap:['PF', 'CM', 'CL', 'RH', 'PCN'],
	scoreMap:[0, 0, 0, 0, 0]
},{
	id:'VISp6a',
	category:CATEGORIES.PERSONAL,
	text:'Primary visual area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ENTl2',
	category:CATEGORIES.PERSONAL,
	text:'Entorhinal area, lateral part, layer 2',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LM',
	category:CATEGORIES.PERSONAL,
	text:'Lateral mammillary nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AUDd2-3',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal auditory area, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TTv',
	category:CATEGORIES.PERSONAL,
	text:'Taenia tecta, ventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'P',
	category:CATEGORIES.PERSONAL,
	text:'Pons',
	inputType:'radio',
	options:['P-sat', 'P-sen', 'P-mot'],
	nextQuestionMap:['P-sat', 'P-sen', 'P-mot'],
	scoreMap:[1, 1, 1]
},{
	id:'COApl1',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, posterior part, lateral zone, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'COAa',
	category:CATEGORIES.PERSONAL,
	text:'Cortical amygdalar area, anterior part',
	inputType:'radio',
	options:['COAa2', 'COAa3', 'COAa1'],
	nextQuestionMap:['COAa2', 'COAa3', 'COAa1'],
	scoreMap:[0, 0, 0]
},{
	id:'MGv',
	category:CATEGORIES.PERSONAL,
	text:'Medial geniculate complex, ventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ILA',
	category:CATEGORIES.PERSONAL,
	text:'Infralimbic area',
	inputType:'radio',
	options:['ILA2-3', 'ILA6a', 'ILA5', 'ILA6b', 'ILA1', 'ILA2'],
	nextQuestionMap:['ILA2-3', 'ILA6a', 'ILA5', 'ILA6b', 'ILA1', 'ILA2'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'ORB',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area',
	inputType:'radio',
	options:['ORBvl', 'ORB5', 'ORB1', 'ORB6b', 'ORBv', 'ORBl', 'ORBm', 'ORB2-3', 'ORB6a'],
	nextQuestionMap:['ORBvl', 'ORB5', 'ORB1', 'ORB6b', 'ORBv', 'ORBl', 'ORBm', 'ORB2-3', 'ORB6a'],
	scoreMap:[1, 0, 0, 0, 0, 1, 1, 0, 0]
},{
	id:'SUBd-sr',
	category:CATEGORIES.PERSONAL,
	text:'Subiculum, dorsal part, stratum radiatum',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ACAd6b',
	category:CATEGORIES.PERSONAL,
	text:'Anterior cingulate area, dorsal part, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SFO',
	category:CATEGORIES.PERSONAL,
	text:'Subfornical organ',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DTN',
	category:CATEGORIES.PERSONAL,
	text:'Dorsal tegmental nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CUL4',
	category:CATEGORIES.PERSONAL,
	text:'Lobule IV',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSP-n6a',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, nose, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DEC',
	category:CATEGORIES.PERSONAL,
	text:'Declive (VI)',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PTLp6a',
	category:CATEGORIES.PERSONAL,
	text:'Posterior parietal association areas, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VLPO',
	category:CATEGORIES.PERSONAL,
	text:'Ventrolateral preoptic nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DGcr-po',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus crest, polymorph layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MBmot',
	category:CATEGORIES.PERSONAL,
	text:'Midbrain, motor related',
	inputType:'radio',
	options:['PAG', 'LT', 'AT', 'EW', 'MT', 'SNr', 'RN', 'VTN', 'PRT', 'VTA', 'DT', 'RR', 'SNI', 'MRN', 'CUN', 'III', 'IV', 'SCm'],
	nextQuestionMap:['PAG', 'LT', 'AT', 'EW', 'MT', 'SNr', 'RN', 'VTN', 'PRT', 'VTA', 'DT', 'RR', 'SNI', 'MRN', 'CUN', 'III', 'IV', 'SCm'],
	scoreMap:[1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1]
},{
	id:'AHNc',
	category:CATEGORIES.PERSONAL,
	text:'Anterior hypothalamic nucleus, central part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ORB6a',
	category:CATEGORIES.PERSONAL,
	text:'Orbital area, layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PIR1',
	category:CATEGORIES.PERSONAL,
	text:'Piriform area, molecular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'MOp6a',
	category:CATEGORIES.PERSONAL,
	text:'Primary motor area, Layer 6a',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSp-n',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, nose',
	inputType:'radio',
	options:['SSp-n6b', 'SSp-n4', 'SSp-n2-3', 'SSp-n5', 'SSp-n1', 'SSP-n6a'],
	nextQuestionMap:['SSp-n6b', 'SSp-n4', 'SSp-n2-3', 'SSp-n5', 'SSp-n1', 'SSP-n6a'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},{
	id:'NTSl',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the solitary tract, lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CBXgr',
	category:CATEGORIES.PERSONAL,
	text:'Cerebellar cortex, granular layer',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSs6b',
	category:CATEGORIES.PERSONAL,
	text:'Supplemental somatosensory area, layer 6b',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'AIv2-3',
	category:CATEGORIES.PERSONAL,
	text:'Agranular insular area, ventral part, layer 2/3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'A13',
	category:CATEGORIES.PERSONAL,
	text:'Dopaminergic A13 group',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'TR3',
	category:CATEGORIES.PERSONAL,
	text:'Postpiriform transition area, layers 3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'LS',
	category:CATEGORIES.PERSONAL,
	text:'Lateral septal nucleus',
	inputType:'radio',
	options:['LSc', 'LSr', 'LSv'],
	nextQuestionMap:['LSc', 'LSr', 'LSv'],
	scoreMap:[0, 0, 0]
},{
	id:'SSp-tr5',
	category:CATEGORIES.PERSONAL,
	text:'Primary somatosensory area, trunk, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VII',
	category:CATEGORIES.PERSONAL,
	text:'Facial motor nucleus',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'HY',
	category:CATEGORIES.PERSONAL,
	text:'Hypothalamus',
	inputType:'radio',
	options:['PVZ', 'LZ', 'MEZ', 'PVR'],
	nextQuestionMap:['PVZ', 'LZ', 'MEZ', 'PVR'],
	scoreMap:[1, 1, 1, 1]
},{
	id:'BMAa',
	category:CATEGORIES.PERSONAL,
	text:'Basomedial amygdalar nucleus, anterior part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'VISal1',
	category:CATEGORIES.PERSONAL,
	text:'Anterolateral visual area, layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'ECT1',
	category:CATEGORIES.PERSONAL,
	text:'Ectorhinal area/Layer 1',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SUBv',
	category:CATEGORIES.PERSONAL,
	text:'Subiculum, ventral part',
	inputType:'radio',
	options:['SUBv-m', 'SUBv-sr', 'SUBv-sp'],
	nextQuestionMap:['SUBv-m', 'SUBv-sr', 'SUBv-sp'],
	scoreMap:[0, 0, 0]
},{
	id:'LGvm',
	category:CATEGORIES.PERSONAL,
	text:'Ventral part of the lateral geniculate complex, medial zone',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SNc',
	category:CATEGORIES.PERSONAL,
	text:'Substantia nigra, compact part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'DGmb',
	category:CATEGORIES.PERSONAL,
	text:'Dentate gyrus medial blade',
	inputType:'radio',
	options:['DGmb-mo', 'DGmb-po', 'DGmb-sg'],
	nextQuestionMap:['DGmb-mo', 'DGmb-po', 'DGmb-sg'],
	scoreMap:[0, 0, 0]
},{
	id:'PBlv',
	category:CATEGORIES.PERSONAL,
	text:'Parabrachial nucleus, lateral division, ventral lateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'CTX',
	category:CATEGORIES.PERSONAL,
	text:'Cerebral cortex',
	inputType:'radio',
	options:['CTX5', 'CTX6', 'CTX1', 'CTXsp', 'CTX2', 'CTX3', 'CTXpl', 'CTX1-6b', 'CTX2-3', 'CTX6a', 'CTX4'],
	nextQuestionMap:['CTX5', 'CTX6', 'CTX1', 'CTXsp', 'CTX2', 'CTX3', 'CTXpl', 'CTX1-6b', 'CTX2-3', 'CTX6a', 'CTX4'],
	scoreMap:[0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0]
},{
	id:'LSv',
	category:CATEGORIES.PERSONAL,
	text:'Lateral septal nucleus, ventral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SSs4',
	category:CATEGORIES.PERSONAL,
	text:'Supplemental somatosensory area, layer 4',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'PTLp5',
	category:CATEGORIES.PERSONAL,
	text:'Posterior parietal association areas, layer 5',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'SCm',
	category:CATEGORIES.PERSONAL,
	text:'Superior colliculus, motor related',
	inputType:'radio',
	options:['SCiw', 'SCdg', 'SCdw', 'SCig'],
	nextQuestionMap:['SCiw', 'SCdg', 'SCdw', 'SCig'],
	scoreMap:[0, 0, 0, 1]
},{
	id:'SPVOvl',
	category:CATEGORIES.PERSONAL,
	text:'Spinal nucleus of the trigeminal, oral part, ventrolateral part',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},{
	id:'NLOT3',
	category:CATEGORIES.PERSONAL,
	text:'Nucleus of the lateral olfactory tract, layer 3',
	inputType:'radio',
	options:['ARBO'],
	nextQuestionMap:['ARBO'],
	scoreMap:[0]
},]