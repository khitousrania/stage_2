import {
	Guard 

	}, from './guard';

export type Question = {
	id: string;
	category: string;
	comment?: string;
	text: string;
	inputType: 'radio' | 'date' | 'checkbox' | 'postal';
	options?: string[] | CheckboxOption[];
	nextQuestionMap?: string | string[];
	scoreMap?: number[];
	guard?: Guard;
 },;
export type CheckboxOption = {
	label: string;
	id: string;
},;
export const CATEGORIES = {
	PERSONAL: 'personalInfo',

	SYMPTOMS: 'symptoms',

},;
export const NO_XML = 'X';
export const QUESTION = {
	POSTAL_CODE: 'V1',
	AGE: 'P0',
	 ABOVE_65: 'P1',
	 LIVING_SITUATION: 'P2',
	 CARING: 'P3',
 	WORKSPACE: 'P4',
 	CONTACT_DATE: 'CZ',
	OUT_OF_BREATH: 'SB',
	SYMPTOM_DATE: 'SZ',
	 DATA_DONATION: `${NO_XML},1`,
},;
export const XML_ORDER = ['V', 'P', 'C', 'S', 'D', 'M'];
export const QUESTIONS: Question[] = [
{
    category: CATEGORIES.PERSONAL,
    id: 'ARBO',
    inputType: 'radio',
    nextQuestionMap: ['Class','Data object',
    'DIRECTED-BINARY-RELATION',
    'Measurement_units',
    'PAL-Constraint',
    'Taxon account section',
    'Sex'
    
],
options: ['Class','Data object',
'DIRECTED-BINARY-RELATION',
'Measurement_units',
'PAL-Constraint',
'Taxon account section',
'Sex'
],
    scoreMap: [],
    text: 'ARBO'


},
{
    category: CATEGORIES.PERSONAL,
    id: 'Planktivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Planktivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Polygynandrous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Polygynandrous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Eggs',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Eggs'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Natatorial',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Natatorial'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Soil_aeration',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Soil aeration'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Other_foraging_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Filter-feeding',
        'Stores_or_caches_food'
    ],
    options: [
        'Filter-feeding',
        'Stores_or_caches_food'
    ],
    scoreMap: [
        0,
        0
    ],
    text: 'Other foraging keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Inherits_maternal_paternal_territory',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Inherits maternal paternal territory'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Length',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Length'
},{
    category: CATEGORIES.PERSONAL,
    id: 'm_2',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'm^2'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Economic_importance_positive_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Economic importance positive text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Duets',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Duets'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Tactile_signals',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Tactile signals'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Measurement_units',
    inputType: 'radio',
    nextQuestionMap: [
        'Length_unit',
        'Mass_unit',
        'Time_unit',
        'Area_unit'
    ],
    options: [
        'Length_unit',
        'Mass_unit',
        'Time_unit',
        'Area_unit'
    ],
    scoreMap: [
        1,
        1,
        1,
        1
    ],
    text: 'Measurement units'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Photic_Bioluminescent',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Photic/Bioluminescent'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Delayed_implementation',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Delayed implementation'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Body_Symmetry',
    inputType: 'radio',
    'nextQuestionMap': [
        'Bilateral_symmetry',
        'Radial_symmetry'
    ],
    options: [
        'Bilateral_symmetry',
        'Radial_symmetry'
    ],
    scoreMap: [
        0,
        0
    ],
    text: 'Body Symmetry'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Data_object',
    inputType: 'radio',
    'nextQuestionMap': [
        'Interacting_taxon',
        'Keyword',
        'Data_range',
        'Field_text',
        'Paragraph_text'
    ],
    options: [
        'Interacting_taxon',
        'Keyword',
        'Data_range',
        'Field_text',
        'Paragraph_text'
    ],
    scoreMap: [
        1,
        1,
        1,
        1,
        1
    ],
    text: 'Data object'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Primary__Other',
    inputType: 'radio',
    'nextQuestionMap': [
        'Planktivore',
        'Coprophage',
        'Mycophage',
        'Omnivore'
    ],
    options: [
        'Planktivore',
        'Coprophage',
        'Mycophage',
        'Omnivore'
    ],
    scoreMap: [
        0,
        0,
        0,
        0
    ],
    text: 'Primary: Other'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Colonial',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Colonial'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Saltatorial',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Saltatorial'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Viviparous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Viviparous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Induced_ovulation',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Induced ovulation'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Other_geographic_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Island_endemic',
        'Holarctic',
        'Cosmopolitan'
    ],
    options: [
        'Island_endemic',
        'Holarctic',
        'Cosmopolitan'
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: 'Other geographic keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Weeks',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Weeks'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Ecosystem_role_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Ecosystem roles text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Leaves',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Leaves'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Chaparral',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Chaparral'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Suburban',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Suburban'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Primary_diet_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Primary__Other',
        'Primary__Herbivore',
        'Primary__Carnivore'
    ],
    options: [
        'Primary__Other',
        'Primary__Herbivore',
        'Primary__Carnivore'
    ],
    scoreMap: [
        1,
        1,
        1
    ],
    text: 'Primary diet keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Dung',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Dung'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Metabolism',
    inputType: 'radio',
    'nextQuestionMap': [
        'Endothermic',
        'Heterothermic',
        'Ectothermic',
        'Homoiothermic'
    ],
    options: [
        'Endothermic',
        'Heterothermic',
        'Ectothermic',
        'Homoiothermic'
    ],
    scoreMap: [
        0,
        0,
        0,
        0
    ],
    text: 'Metabolism'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Electric',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Electric'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Behavior_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Colonial',
        'Motile',
        'Daily_torpor',
        'Migratory',
        'Dominance_hierarchies',
        'Territorial',
        'Nomadic',
        'Sessile',
        'Social',
        'Nocturnal',
        'Crepuscular',
        'Hibernation',
        'Sedentary',
        'Troglophic',
        'Diurnal',
        'Locomotion',
        'Aestivation'
    ],
    options: [
        'Colonial',
        'Motile',
        'Daily_torpor',
        'Migratory',
        'Dominance_hierarchies',
        'Territorial',
        'Nomadic',
        'Sessile',
        'Social',
        'Nocturnal',
        'Crepuscular',
        'Hibernation',
        'Sedentary',
        'Troglophic',
        'Diurnal',
        'Locomotion',
        'Aestivation'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        1,
        0
    ],
    text: 'Behavior keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Neotenic_or_paedomorphic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Neotenic or paedomorphic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Produces_fertilizer',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Produces fertilizer'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Causes_or_carries_domestic_animal_disease',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Causes or carries domestic animal disease'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Echinoderms',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Echinoderms'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Geographic_range',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Geographic range'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Endothermic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Endothermic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Expected_wild_lifespan',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Expected wild lifespan'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Extended_period_of_juvenile_learning',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Extended period of juvenile learning'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Australian',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Australian'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Research_and_education',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Research and education'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Communication_other_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Duets',
        'Photic_Bioluminescent',
        'Mimicry',
        'Choruses',
        'Pheromones',
        'Scent_marks'
    ],
    options: [
        'Duets',
        'Photic_Bioluminescent',
        'Mimicry',
        'Choruses',
        'Pheromones',
        'Scent_marks'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Communication other keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Communication_channel',
    inputType: 'radio',
    'nextQuestionMap': [
        'Tactile_signals',
        'Visual_signals',
        'Electric_signals',
        'Chemical_signals',
        'Acoustic_signals'
    ],
    options: [
        'Tactile_signals',
        'Visual_signals',
        'Electric_signals',
        'Chemical_signals',
        'Acoustic_signals'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Communication channel'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mutualist',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Mutualist'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Longest_captive_lifespan',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Longest captive lifespan'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Eats_marine_invertebrates',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Eats marine invertebrates'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Bilateral_symmetry',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Bilateral_symmetry'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Semelparous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Semelparous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'mm',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'mm'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Pollinates',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Pollinates'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Body_fluids',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Body fluids'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Coprophage',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Coprophage'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Motile',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Motile'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Controls_pest_population',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Controls pest population'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Amphibians',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Amphibians'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Provisioning',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Provisioning'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Length_unit',
    inputType: 'radio',
    'nextQuestionMap': [
        'mm',
        'cm',
        'm'
    ],
    options: [
        'mm',
        'cm',
        'm'
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: 'Length unit'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Fruit',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Fruit'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Arboreal',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Arboreal'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Daily_torpor',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'daily torpor'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Swamp',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Swamp'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Primary__Herbivore',
    inputType: 'radio',
    'nextQuestionMap': [
        'Lignivore',
        'Eats_sap_or_other_plant_foods',
        'Granivore',
        'Nectarivore',
        'Algivore',
        'Folivore',
        'Frugivore'
    ],
    options: [
        'Lignivore',
        'Eats_sap_or_other_plant_foods',
        'Granivore',
        'Nectarivore',
        'Algivore',
        'Folivore',
        'Frugivore'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Primary: Herbivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Aposematic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Aposematic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Habitat',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Habitat'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Delayed_fertilization',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Delayed fertilization'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Saltwater_or_marine',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Saltwater_or_marine'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Nearctic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Nearctic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Monogamous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Monogamous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Rainforest',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Rainforest'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Protecting',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Protecting'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Eats_terrestrial_vertebrates',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Eats terrestrial vertebrates'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Polymorphism',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Polymorphism'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Terrestrial',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Terrestrial'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Echolocation',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Echolocation'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Embryonic_diapause',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Embryonic diapause'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Biodegradation',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Biodegradation'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Lifespan_range',
    inputType: 'radio',
    'nextQuestionMap': [
        'Longest_wild_lifespan',
        'Expected_wild_lifespan',
        'Longest_captive_lifespan',
        'Expected_captive_lifespan'
    ],
    options: [
        'Longest_wild_lifespan',
        'Expected_wild_lifespan',
        'Longest_captive_lifespan',
        'Expected_captive_lifespan'
    ],
    scoreMap: [
        0,
        0,
        0,
        0
    ],
    text: 'Lifespan range'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Migratory',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Migratory'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Asexual',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Asexual'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Economic_importance_positive',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Economic importance positive'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Interacting_taxon',
    inputType: 'radio',
    'nextQuestionMap': [
        'Known_predator',
        'Commensal_species',
        'Food_item',
        'Mutualist_species',
        'Host_species'
    ],
    options: [
        'Known_predator',
        'Commensal_species',
        'Food_item',
        'Mutualist_species',
        'Host_species'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Interacting taxon'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Food_habits_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Food habits text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Filter-feeding',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Filter-feeding'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Years',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Years'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Visual_signals',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Visual signals'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Broadcast__group__spawning',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Broadcast (group) spawning'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Piscivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Piscivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Protogynous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Protogynous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'External_fertilization',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'External fertilization'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Indian_Ocean',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Indian Ocean'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Rivers_and_streams',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'rivers and stream'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Terrestrial_worms',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Terrestrial worms'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Male',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Male'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Dominance_hierarchies',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'dominance hierarchies'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Electric_signals',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Electric signals'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Carrion',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Carrion'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Radial_symmetry',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Radial_symmetry'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Algae',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Algae'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Habitat_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Habitat text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Temporary_pools',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Temporary_pools'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Key_reproductive_feature',
    inputType: 'radio',
    'nextQuestionMap': [
        'Delayed_implementation',
        'Viviparous',
        'Induced_ovulation',
        'Semelparous',
        'Delayed_fertilization',
        'Embryonic_diapause',
        'Asexual',
        'Broadcast__group__spawning',
        'Sequential_hermaphrodite',
        'Iteroparous',
        'Simultaneous_hermaphrodite',
        'Sexual',
        'Year-round_breeding',
        'Ovoviviparous',
        'Parthenogenic',
        'Seasonal_breeding',
        'Fertilization',
        'Oviparous',
        'Sperm-storing',
        'Post-partum_estrous',
        'Gonochoric'
    ],
    options: [
        'Delayed_implementation',
        'Viviparous',
        'Induced_ovulation',
        'Semelparous',
        'Delayed_fertilization',
        'Embryonic_diapause',
        'Asexual',
        'Broadcast__group__spawning',
        'Sequential_hermaphrodite',
        'Iteroparous',
        'Simultaneous_hermaphrodite',
        'Sexual',
        'Year-round_breeding',
        'Ovoviviparous',
        'Parthenogenic',
        'Seasonal_breeding',
        'Fertilization',
        'Oviparous',
        'Sperm-storing',
        'Post-partum_estrous',
        'Gonochoric'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        1,
        0,
        0,
        0,
        0
    ],
    text: 'Key reproductive feature'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Antarctica',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Antarctica'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Reef',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Reef'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Scavenger',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Scavenger'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Lignivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Lignivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Commensal',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Commensal'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mass_unit',
    inputType: 'radio',
    'nextQuestionMap': [
        'g',
        'kg'
    ],
    options: [
        'g',
        'kg'
    ],
    scoreMap: [
        0,
        0
    ],
    text: 'Mass unit'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Bog',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Bog'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Forest',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Forest'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Development',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Development'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Size_dimorphism',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Size_dimorphism'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Oceanic_Islands',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Oceanic Islands'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Reproduction_general',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Reproduction general'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Flowers',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Flowers'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Female',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Female'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Temperature_sex_determination',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Temperature sex determination'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Plant_foods',
    inputType: 'radio',
    'nextQuestionMap': [
        'Leaves',
        'Fruit',
        'Algae',
        'Flowers',
        'Wood__bark__or_stems',
        'Nectar',
        'Sap_or_other_plant_fluids',
        'Roots_and_tubers',
        'Lichens',
        'Bryophytes',
        'Phytoplankton',
        'Macroalgae',
        'Pollen',
        'Seeds__grains__and_nuts'
    ],
    options: [
        'Leaves',
        'Fruit',
        'Algae',
        'Flowers',
        'Wood__bark__or_stems',
        'Nectar',
        'Sap_or_other_plant_fluids',
        'Roots_and_tubers',
        'Lichens',
        'Bryophytes',
        'Phytoplankton',
        'Macroalgae',
        'Pollen',
        'Seeds__grains__and_nuts'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Plant foods'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Wood__bark__or_stems',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Wood, bark, or stems'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Fish',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Fish'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Parasite',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Parasite'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Other_comments_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Other comments text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Eats_non_insect_arthopods',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Eats non_insect arthopods'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Terrestrial_biomes',
    inputType: 'radio',
    'nextQuestionMap': [
        'Chaparral',
        'Rainforest',
        'Forest',
        'Tundra',
        'Taiga',
        'Mountains',
        'Scrub_forest',
        'Desert_or_dune',
        'Ice_cap'
    ],
    options: [
        'Chaparral',
        'Rainforest',
        'Forest',
        'Tundra',
        'Taiga',
        'Mountains',
        'Scrub_forest',
        'Desert_or_dune',
        'Ice_cap'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'terrestrial habitat'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Territorial',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Territorial'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Territory_size',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Territory size'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Nectar',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Nectar'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Protandrous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Protandrous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Acoustic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Acoustic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Reptiles',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Reptiles'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Economic_importance_negative_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Causes_or_carries_domestic_animal_disease',
        'Injures_humans',
        'Crop_pest'
    ],
    options: [
        'Causes_or_carries_domestic_animal_disease',
        'Injures_humans',
        'Crop_pest'
    ],
    scoreMap: [
        0,
        1,
        0
    ],
    text: 'Economic importance negative keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Known_predator',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Known predators'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Parental_investment_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Inherits_maternal_paternal_territory',
        'Extended_period_of_juvenile_learning',
        'Altricial',
        'Precocial',
        'No_parental_involvement',
        'Kind_of_care',
        'Maternal_position_affects_young'
    ],
    options: [
        'Inherits_maternal_paternal_territory',
        'Extended_period_of_juvenile_learning',
        'Altricial',
        'Precocial',
        'No_parental_involvement',
        'Kind_of_care',
        'Maternal_position_affects_young'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        1,
        0
    ],
    text: 'Parental care keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Ornamentation',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Ornamentation'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Lakes_and_ponds',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Lakes_and_ponds'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Venomous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Venomous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Palearctic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Palearctic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Sap_or_other_plant_fluids',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Sap or other plant fluids'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Coastal',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Coastal'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Aquatic_or_marine_worms',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Aquatic or marine worms'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mating_system',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Mating system'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Scansorial',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Scansorial'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Tundra',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Tundra'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mediterranean',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Mediterranean'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Altricial',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Altricial'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Sequential_hermaphrodite',
    inputType: 'radio',
    'nextQuestionMap': [
        'Protogynous',
        'Protandrous'
    ],
    options: [
        'Protogynous',
        'Protandrous'
    ],
    scoreMap: [
        0,
        0
    ],
    text: 'Sequential hermaphrodite'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Conservation_status_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Conservation status text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Iteroparous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Iteroparous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Zooplankton',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Zooplankton'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Blood',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Blood'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mimicry',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Mimicry'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Brackish_water',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Brackish_water'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Precocial',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Precocial'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Atlantic_Ocean',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Atlantic Ocean'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Predation',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Predation'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Poisonous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Poisonous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Nomadic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Nomadic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Injures_humans',
    inputType: 'radio',
    'nextQuestionMap': [
        'Causes_disease_in_humans',
        'Carries_human_disease',
        'Bites_or_stings'
    ],
    options: [
        'Causes_disease_in_humans',
        'Carries_human_disease',
        'Bites_or_stings'
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: 'Injures humans'
},{
    category: CATEGORIES.PERSONAL,
    id: 'cm',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'cm'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Communication_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Communication text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Sessile',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'sessile or motile'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Causes_disease_in_humans',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Causes disease in humans'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Volant',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Volant'
},{
    category: CATEGORIES.PERSONAL,
    id: 'BreedingInterval',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'BreedingInterval'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Polyandrous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Polyandrous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Vermivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Vermivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Chemical',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Chemical'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Island_endemic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Island endemic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Cnidarians',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Cnidarians'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Simultaneous_hermaphrodite',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Simultaneous hermaphrodite'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Eats_sap_or_other_plant_foods',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Eats sap or other plant foods'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Eats_body_fluids',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Eats body fluids'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Granivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Granivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Pollinates_crops',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Pollinates crops'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Sexes_shaped_or_patterned_differently',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Sexes_shaped_or_patterned_differently'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Marsh',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Marsh'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Ecosystem_role',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Ecosystem role'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Roots_and_tubers',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Roots_and_tubers'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Ecosystem_impact_keywords',
    inputType: 'radio',
    'nextQuestionMap': [
        'Soil_aeration',
        'Mutualist',
        'Pollinates',
        'Biodegradation',
        'Commensal',
        'Parasite',
        'Disperses_seeds',
        'Creates_habitat',
        'Keystone_species'
    ],
    options: [
        'Soil_aeration',
        'Mutualist',
        'Pollinates',
        'Biodegradation',
        'Commensal',
        'Parasite',
        'Disperses_seeds',
        'Creates_habitat',
        'Keystone_species'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Ecosystem impact keywords'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Economic_importance_negative',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Economic importance negative'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Tropical',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Tropical'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Social',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Social'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Molluscs',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Molluscs'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mycophage',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Mycophage'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Nectarivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Nectarivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Aquatic_biomes',
    inputType: 'radio',
    'nextQuestionMap': [
        'Rivers_and_streams',
        'Temporary_pools',
        'Reef',
        'Lakes_and_ponds',
        'Coastal',
        'Brackish_water',
        'Abyssal',
        'Pelagic',
        'Oceanic_vent',
        'Benthic'
    ],
    options: [
        'Rivers_and_streams',
        'Temporary_pools',
        'Reef',
        'Lakes_and_ponds',
        'Coastal',
        'Brackish_water',
        'Abyssal',
        'Pelagic',
        'Oceanic_vent',
        'Benthic'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Aquatic_biomes'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Algivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Algivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Chemical_signals',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Chemical signals'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mammals',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Mammals'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Colonial_growth',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Colonial growth'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Development_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Neotenic_or_paedomorphic',
        'Temperature_sex_determination',
        'Colonial_growth',
        'Indeterminate_growth',
        'Metamorphosis'
    ],
    options: [
        'Neotenic_or_paedomorphic',
        'Temperature_sex_determination',
        'Colonial_growth',
        'Indeterminate_growth',
        'Metamorphosis'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Development keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Internal_fertilization',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Internal fertilization'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mimic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Mimic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Cryptic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Cryptic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Visual',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Visual'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Lifespan',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Lifespan'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Parental_investment',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Parental Care'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mating_system_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Polygynandrous',
        'Monogamous',
        'Polyandrous',
        'Polygynous',
        'Cooperative_breeding',
        'Eusocial'
    ],
    options: [
        'Polygynandrous',
        'Monogamous',
        'Polyandrous',
        'Polygynous',
        'Cooperative_breeding',
        'Eusocial'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Mating system keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Communication_and_Perception',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Communication_and_Perception'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Lichens',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Lichens'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Sexual',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Sexual'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Taxon_account_section',
    inputType: 'radio',
    'nextQuestionMap': [
        'Geographic_range',
        'Habitat',
        'Development',
        'Predation',
        'Ecosystem_role',
        'Lifespan',
        'Communication_and_Perception',
        'Reproduction',
        'Other_comments',
        'Economic_importance',
        'Conservation_status',
        'Physical_description',
        'Behavior',
        'Food_habits'
    ],
    options: [
        'Geographic_range',
        'Habitat',
        'Development',
        'Predation',
        'Ecosystem_role',
        'Lifespan',
        'Communication_and_Perception',
        'Reproduction',
        'Other_comments',
        'Economic_importance',
        'Conservation_status',
        'Physical_description',
        'Behavior',
        'Food_habits'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        1,
        0,
        1,
        0,
        0,
        0,
        0
    ],
    text: 'Taxon account section'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Reproduction',
    inputType: 'radio',
    'nextQuestionMap': [
        'Reproduction_general',
        'Mating_system',
        'Parental_investment'
    ],
    options: [
        'Reproduction_general',
        'Mating_system',
        'Parental_investment'
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: 'Reproduction'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Ultrasound',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Ultrasound'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Abyssal',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Abyssal'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Parental_investment_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Parental care text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'BreedingSeason',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'BreedingSeason'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Polygynous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Polygynous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'PAL-Constraint',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'PAL-Constraint'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Indeterminate_growth',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Indeterminate growth'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Year-round_breeding',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Year-round breeding'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Anti-predator_adaptations_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Anti-predator adaptations text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Ovoviviparous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Ovoviviparous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Perception_channel',
    inputType: 'radio',
    'nextQuestionMap': [
        'Electric',
        'Echolocation',
        'Acoustic',
        'Chemical',
        'Visual',
        'Ultrasound',
        'Ultraviolet',
        'Magnetic',
        'Polarized_light',
        'Vibrations',
        'Tactile'
    ],
    options: [
        'Electric',
        'Echolocation',
        'Acoustic',
        'Chemical',
        'Visual',
        'Ultrasound',
        'Ultraviolet',
        'Magnetic',
        'Polarized_light',
        'Vibrations',
        'Tactile'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Perception channel'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Choruses',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Choruses'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Disperses_seeds',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Disperses seeds'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Number_of_offspring',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Number of offspring'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Bryophytes',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Bryophytes'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Parthenogenic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Parthenogenic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Food',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Food'
},{
    category: CATEGORIES.PERSONAL,
    id: 'http://www.w3.org/2002/07/owl#Class',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Class'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Taiga',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Taiga'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Phytoplankton',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Phytoplankton'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Other_comments',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Other comments'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mountains',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Mountains'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Male_sexual_maturity',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Male sexual maturity'
},{
    category: CATEGORIES.PERSONAL,
    id: 'g',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'g'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Heterothermic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Heterothermic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Color_dimorphism',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Color_dimorphism'
},{
    category: CATEGORIES.PERSONAL,
    id: 'hectares',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'hectares'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Acoustic_signals',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Acoustic signals'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Molluscivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Molluscivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Female_sexual_maturity',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Female sexual maturity'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Temperate',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Temperate'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Economic_importance',
    inputType: 'radio',
    'nextQuestionMap': [
        'Economic_importance_positive',
        'Economic_importance_negative'
    ],
    options: [
        'Economic_importance_positive',
        'Economic_importance_negative'
    ],
    scoreMap: [
        0,
        0
    ],
    text: 'Economic importance'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Scrub_forest',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'scrub forest'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Macroalgae',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Macroalgae'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Seasonal_breeding',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Seasonal breeding'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Arctic_Ocean',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Arctic Ocean'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Oriental',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Oriental'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Carries_human_disease',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Carries human disease'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Pet_trade',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Pet trade'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Ultraviolet',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Ultraviolet'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Holarctic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Holarctic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Stores_or_caches_food',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Stores or caches food'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Dimorphism',
    inputType: 'radio',
    'nextQuestionMap': [
        'Size_dimorphism',
        'Sexes_shaped_or_patterned_differently',
        'Color_dimorphism'
    ],
    options: [
        'Size_dimorphism',
        'Sexes_shaped_or_patterned_differently',
        'Color_dimorphism'
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: 'Dimorphism'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Geographic_range_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Geographic range text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Commensal_species',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Commensal species'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Months',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Months'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Food_item',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Food items'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Metabolic_rate',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Metabolic rate'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Physical_description_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Body_Symmetry',
        'Metabolism',
        'Polymorphism',
        'Ornamentation',
        'Dimorphism'
    ],
    options: [
        'Body_Symmetry',
        'Metabolism',
        'Polymorphism',
        'Ornamentation',
        'Dimorphism'
    ],
    scoreMap: [
        1,
        1,
        0,
        0,
        1
    ],
    text: 'Physical description keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Nocturnal',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Nocturnal'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Metamorphosis',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Metamorphosis'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Economic_importance_positive_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Produces_fertilizer',
        'Research_and_education',
        'Controls_pest_population',
        'Pollinates_crops',
        'Food',
        'Pet_trade',
        'Ecotourism',
        'Body_parts_are_source_of_valuable_material'
    ],
    options: [
        'Produces_fertilizer',
        'Research_and_education',
        'Controls_pest_population',
        'Pollinates_crops',
        'Food',
        'Pet_trade',
        'Ecotourism',
        'Body_parts_are_source_of_valuable_material'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Economic importance positive keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Pollen',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Pollen'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Magnetic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Magnetic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Terrestrial_non-insect_arthropods',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Terrestrial non-insect arthropods'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Insectivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Insectivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Other_foraging_keyword',
        'Other_geographic_keyword',
        'Primary_diet_keyword',
        'Behavior_keyword',
        'Communication_other_keyword',
        'Communication_channel',
        'Key_reproductive_feature',
        'Economic_importance_negative_keyword',
        'Parental_investment_keyword',
        'Ecosystem_impact_keywords',
        'Development_keyword',
        'Mating_system_keyword',
        'Perception_channel',
        'Physical_description_keyword',
        'Economic_importance_positive_keyword',
        'Other_food',
        'Biogeographic_region',
        'Known_food',
        'Anti-predator_adaptations_keyword',
        'Habitat_keyword'
    ],
    options: [
        'Other_foraging_keyword',
        'Other_geographic_keyword',
        'Primary_diet_keyword',
        'Behavior_keyword',
        'Communication_other_keyword',
        'Communication_channel',
        'Key_reproductive_feature',
        'Economic_importance_negative_keyword',
        'Parental_investment_keyword',
        'Ecosystem_impact_keywords',
        'Development_keyword',
        'Mating_system_keyword',
        'Perception_channel',
        'Physical_description_keyword',
        'Economic_importance_positive_keyword',
        'Other_food',
        'Biogeographic_region',
        'Known_food',
        'Anti-predator_adaptations_keyword',
        'Habitat_keyword'
    ],
    scoreMap: [
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1
    ],
    text: 'Keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Cosmopolitan',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Cosmopolitan'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Conservation_status',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Conservation status'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Gestation_Period',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Gestation Period'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Days',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Days'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Home_range',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Home range'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Expected_captive_lifespan',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Expected captive lifespan'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Physical_description',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Physical description'
},{
    category: CATEGORIES.PERSONAL,
    id: 'No_parental_involvement',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'No_parental_involvement'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Detritus',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Detritus'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mutualist_species',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Mutualist species'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Aquatic_crustaceans',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Aquatic crustaceans'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Ecotourism',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Ecotourism'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Glides',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Glides'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Wetlands_habitat',
    inputType: 'radio',
    'nextQuestionMap': [
        'Swamp',
        'Bog',
        'Marsh'
    ],
    options: [
        'Swamp',
        'Bog',
        'Marsh'
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: 'Wetlands_habitat'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Reproduction_general_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Reproduction general text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Terricolous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Terricolous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Cooperative_breeding',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Cooperative breeding'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Other_food',
    inputType: 'radio',
    'nextQuestionMap': [
        'Dung',
        'Detritus',
        'Fungus',
        'Microbes'
    ],
    options: [
        'Dung',
        'Detritus',
        'Fungus',
        'Microbes'
    ],
    scoreMap: [
        0,
        0,
        0,
        0
    ],
    text: 'Other food'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Animal_foods',
    inputType: 'radio',
    'nextQuestionMap': [
        'Eggs',
        'Echinoderms',
        'Body_fluids',
        'Amphibians',
        'Terrestrial_worms',
        'Carrion',
        'Fish',
        'Reptiles',
        'Aquatic_or_marine_worms',
        'Zooplankton',
        'Blood',
        'Cnidarians',
        'Molluscs',
        'Mammals',
        'Terrestrial_non-insect_arthropods',
        'Aquatic_crustaceans',
        'Insects',
        'Birds'
    ],
    options: [
        'Eggs',
        'Echinoderms',
        'Body_fluids',
        'Amphibians',
        'Terrestrial_worms',
        'Carrion',
        'Fish',
        'Reptiles',
        'Aquatic_or_marine_worms',
        'Zooplankton',
        'Blood',
        'Cnidarians',
        'Molluscs',
        'Mammals',
        'Terrestrial_non-insect_arthropods',
        'Aquatic_crustaceans',
        'Insects',
        'Birds'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Animal foods'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Fertilization',
    inputType: 'radio',
    'nextQuestionMap': [
        'External_fertilization',
        'Internal_fertilization'
    ],
    options: [
        'External_fertilization',
        'Internal_fertilization'
    ],
    scoreMap: [
        0,
        0
    ],
    text: 'Fertilization'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Pheromones',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Pheromones'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Polarized_light',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Polarized light'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Behavior',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Behavior'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Crepuscular',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Crepuscular'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Biogeographic_region',
    inputType: 'radio',
    'nextQuestionMap': [
        'Australian',
        'Nearctic',
        'Indian_Ocean',
        'Antarctica',
        'Oceanic_Islands',
        'Palearctic',
        'Mediterranean',
        'Atlantic_Ocean',
        'Arctic_Ocean',
        'Oriental',
        'Pacific_Ocean',
        'Neotropical',
        'Ethiopian'
    ],
    options: [
        'Australian',
        'Nearctic',
        'Indian_Ocean',
        'Antarctica',
        'Oceanic_Islands',
        'Palearctic',
        'Mediterranean',
        'Atlantic_Ocean',
        'Arctic_Ocean',
        'Oriental',
        'Pacific_Ocean',
        'Neotropical',
        'Ethiopian'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Biogeographic region'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Polar',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Polar'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Data_range',
    inputType: 'radio',
    'nextQuestionMap': [
        'Length',
        'Lifespan_range',
        'Territory_size',
        'Number_of_offspring',
        'Male_sexual_maturity',
        'Female_sexual_maturity',
        'Metabolic_rate',
        'Gestation_Period',
        'Wingspan',
        'Mass',
        'Time_to_weaning_fledging',
        'Time_to_independence'
    ],
    options: [
        'Length',
        'Lifespan_range',
        'Territory_size',
        'Number_of_offspring',
        'Male_sexual_maturity',
        'Female_sexual_maturity',
        'Metabolic_rate',
        'Gestation_Period',
        'Wingspan',
        'Mass',
        'Time_to_weaning_fledging',
        'Time_to_independence'
    ],
    scoreMap: [
        0,
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Data range'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Hibernation',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Hibernation'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Oviparous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Oviparous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Kind_of_care',
    inputType: 'radio',
    'nextQuestionMap': [
        'Provisioning',
        'Protecting'
    ],
    options: [
        'Provisioning',
        'Protecting'
    ],
    scoreMap: [
        0,
        0
    ],
    text: 'Kind of care'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Desert_or_dune',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'desert or dune'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Folivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Folivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Scent_marks',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Scent marks'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Creates_habitat',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Creates habitat'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Crop_pest',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Crop pest'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Sanguivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Sanguivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Insects',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Insects'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Sedentary',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Sedentary'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Troglophic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Troglophic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'PAL-CONSTRAINT',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'PAL-CONSTRAINT'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Behavior_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Behavior text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Field_text',
    inputType: 'radio',
    'nextQuestionMap': [
        'BreedingInterval',
        'BreedingSeason',
        'Home_range'
    ],
    options: [
        'BreedingInterval',
        'BreedingSeason',
        'Home_range'
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: 'Field text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'DIRECTED-BINARY-RELATION',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'DIRECTED-BINARY-RELATION'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Wingspan',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Wingspan'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Sperm-storing',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Sperm-storing'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Sex',
    inputType: 'radio',
    'nextQuestionMap': [
        'Male',
        'Female'
    ],
    options: [
        'Male',
        'Female'
    ],
    scoreMap: [
        0,
        0
    ],
    text: 'Sex'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Other_habitat',
    inputType: 'radio',
    'nextQuestionMap': [
        'Suburban',
        'Intertidal_or_littoral',
        'Urban',
        'Agricultural',
        'Riparian',
        'Estuarine',
        'Caves'
    ],
    options: [
        'Suburban',
        'Intertidal_or_littoral',
        'Urban',
        'Agricultural',
        'Riparian',
        'Estuarine',
        'Caves'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Other_habitat'
},{
    category: CATEGORIES.PERSONAL,
    id: 'kg',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'kg'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Diurnal',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: '\'Diurnal'
},{
    category: CATEGORIES.PERSONAL,
    id: 'm',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: '\''
},{
    category: CATEGORIES.PERSONAL,
    id: 'Pacific_Ocean',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'm'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Neotropical',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Pacific Ocean'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Lifespan_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Neotropical'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Economic_importance_negative_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Lifespan text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Body_parts_are_source_of_valuable_material',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Economic importance negative text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Time_unit',
    inputType: 'radio',
    'nextQuestionMap': [
        'Weeks',
        'Years',
        'Months',
        'Days'
    ],
    options: [
        'Weeks',
        'Years',
        'Months',
        'Days'
    ],
    scoreMap: [
        0,
        0,
        0,
        0
    ],
    text: 'Body parts are source of valuable material'
},{
    category: CATEGORIES.PERSONAL,
    id: 'mm_2',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Time unit'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Habitat_type',
    inputType: 'radio',
    'nextQuestionMap': [
        'Saltwater_or_marine',
        'Terrestrial',
        'Freshwater'
    ],
    options: [
        'Saltwater_or_marine',
        'Terrestrial',
        'Freshwater'
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: 'mm^2'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Intertidal_or_littoral',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Habitat_type'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Ice_cap',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Intertidal_or_littoral'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Pelagic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'ice cap'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mass',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Pelagic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Known_food',
    inputType: 'radio',
    'nextQuestionMap': [
        'Plant_foods',
        'Animal_foods'
    ],
    options: [
        'Plant_foods',
        'Animal_foods'
    ],
    scoreMap: [
        1,
        1
    ],
    text: 'Mass'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Eusocial',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Known food'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Time_to_weaning_fledging',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Eusocial'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Keystone_species',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Time to weaning fledging'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Post-partum_estrous',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Keystone species'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Seeds__grains__and_nuts',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Post-partum estrous'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Urban',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Seeds, grains, and nuts'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Vibrations',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Urban'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Maternal_position_affects_young',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Vibrations'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Ethiopian',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Maternal position affects young'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Anti-predator_adaptations_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Aposematic',
        'Mimic',
        'Cryptic'
    ],
    options: [
        'Aposematic',
        'Mimic',
        'Cryptic'
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: 'Ethiopian'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Paragraph_text',
    inputType: 'radio',
    'nextQuestionMap': [
        'Economic_importance_positive_text',
        'Ecosystem_role_text',
        'Food_habits_text',
        'Habitat_text',
        'Other_comments_text',
        'Conservation_status_text',
        'Communication_text',
        'Parental_investment_text',
        'Anti-predator_adaptations_text',
        'Geographic_range_text',
        'Reproduction_general_text',
        'Behavior_text',
        'Lifespan_text',
        'Economic_importance_negative_text',
        'Mating_system_text'
    ],
    options: [
        'Economic_importance_positive_text',
        'Ecosystem_role_text',
        'Food_habits_text',
        'Habitat_text',
        'Other_comments_text',
        'Conservation_status_text',
        'Communication_text',
        'Parental_investment_text',
        'Anti-predator_adaptations_text',
        'Geographic_range_text',
        'Reproduction_general_text',
        'Behavior_text',
        'Lifespan_text',
        'Economic_importance_negative_text',
        'Mating_system_text'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Anti-predation adaptations keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Omnivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Paragraph text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Fungus',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Omnivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Area_unit',
    inputType: 'radio',
    'nextQuestionMap': [
        'm_2',
        'hectares',
        'mm_2'
    ],
    options: [
        'm_2',
        'hectares',
        'mm_2'
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: 'Fungus'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Locomotion',
    inputType: 'radio',
    'nextQuestionMap': [
        'Natatorial',
        'Saltatorial',
        'Arboreal',
        'Scansorial',
        'Volant',
        'Glides',
        'Terricolous',
        'Fossorial'
    ],
    options: [
        'Natatorial',
        'Saltatorial',
        'Arboreal',
        'Scansorial',
        'Volant',
        'Glides',
        'Terricolous',
        'Fossorial'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Area unit'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Aestivation',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Locomotion'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Primary__Carnivore',
    inputType: 'radio',
    'nextQuestionMap': [
        'Eats_marine_invertebrates',
        'Eats_terrestrial_vertebrates',
        'Piscivore',
        'Scavenger',
        'Eats_non_insect_arthopods',
        'Vermivore',
        'Eats_body_fluids',
        'Molluscivore',
        'Insectivore',
        'Sanguivore'
    ],
    options: [
        'Eats_marine_invertebrates',
        'Eats_terrestrial_vertebrates',
        'Piscivore',
        'Scavenger',
        'Eats_non_insect_arthopods',
        'Vermivore',
        'Eats_body_fluids',
        'Molluscivore',
        'Insectivore',
        'Sanguivore'
    ],
    scoreMap: [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    text: 'Aestivation'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Microbes',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Primary: Carnivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Agricultural',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Microbes'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Time_to_independence',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Agricultural'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Oceanic_vent',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Time to independence'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Mating_system_text',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'oceanic vent'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Riparian',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Mating system text'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Frugivore',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Riparian'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Birds',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Frugivore'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Freshwater',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Birds'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Host_species',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Freshwater'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Fossorial',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Host species'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Ectothermic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Fossorial'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Homoiothermic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Ectothermic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Food_habits',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Homoiothermic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Tactile',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Food habits'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Benthic',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Tactile'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Habitat_keyword',
    inputType: 'radio',
    'nextQuestionMap': [
        'Terrestrial_biomes',
        'Aquatic_biomes',
        'Wetlands_habitat',
        'Other_habitat',
        'Habitat_type',
        'Latitude_information'
    ],
    options: [
        'Terrestrial_biomes',
        'Aquatic_biomes',
        'Wetlands_habitat',
        'Other_habitat',
        'Habitat_type',
        'Latitude_information'
    ],
    scoreMap: [
        1,
        1,
        1,
        1,
        1,
        1
    ],
    text: 'Benthic'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Latitude_information',
    inputType: 'radio',
    'nextQuestionMap': [
        'Tropical',
        'Temperate',
        'Polar'
    ],
    options: [
        'Tropical',
        'Temperate',
        'Polar'
    ],
    scoreMap: [
        0,
        0,
        0
    ],
    text: 'Habitat keyword'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Bites_or_stings',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Latitude information'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Estuarine',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Bites or stings'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Caves',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Estuarine'
},{
    category: CATEGORIES.PERSONAL,
    id: 'Gonochoric',
    inputType: 'radio',
    nextQuestionMap: ['home'],
    options: ['home'],
    scoreMap: [],
    text: 'Caves'
},];