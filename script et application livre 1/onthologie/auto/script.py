

import shutil
import json 

l =['ABA-ABM','ADW']

for i in range(0,len(l)):
    shutil.copytree('rep/DIAGNOSTIC Application','rep/automatisation/'+str(l[i]))
    
for i in range(0,len(l)):
    with open(str(l[i])+'/en.json', 'r+',errors='ignore',encoding='utf-8') as f:
        en = json.load(f)
    fileJs1 =open('rep/automatisation/'+str(l[i])+'/src/global/i18n/en.json','r+',encoding='utf-8')
    fileJs2 =open('rep/automatisation/'+str(l[i])+'/src/custom/translations/en.json','r+',encoding='utf-8')
    fileJs1.truncate(0)
    fileJs1.seek(0,0)
    fileJs2.truncate(0)
    fileJs2.seek(0,0)
    
    with open('rep/automatisation/'+str(l[i])+'/src/global/i18n/en.json', 'r+',encoding="utf-8") as fp:
        json.dump(en, fp, indent=4)
    with open('rep/automatisation/'+str(l[i])+'/src/custom/translations/en.json', 'r+',encoding="utf-8") as fp:
        json.dump(en, fp, indent=4)
    fileTsx =open('rep/automatisation/'+l[i]+'/src/global/questions.ts','r+',encoding='utf-8')
    fileTsx.truncate(0)
    fileTsx.seek(0,0)
    q =open(l[i]+'/questions.ts','r+',encoding='utf-8')
    fileTsx.write(q.read())
    
