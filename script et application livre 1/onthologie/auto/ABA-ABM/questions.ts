import {
  Guard
  /*ScoreCondition,
  Disjunction,
  RadioAnswerCondition,
  Conjunction,
  BoolCondition,*/
} from './guard';
//import { PANDEMIC_TRACKING_IS_ENABLED } from './custom';

export type Question = {
  id: string;
  category: string;
  comment?: string;
  text: string;
  inputType: 'radio' | 'date' | 'checkbox' | 'postal';
  options?: string[] | CheckboxOption[];
  nextQuestionMap?: string | string[];
  scoreMap?: number[];
  guard?: Guard;
};

export type CheckboxOption = {
  label: string;
  id: string;
};

export const CATEGORIES = {
  PERSONAL: 'personalInfo',
  SYMPTOMS: 'symptoms',

};

export const NO_XML = 'X';
export const QUESTION = {
  POSTAL_CODE: 'V1',
  AGE: 'P0',
  ABOVE_65: 'P1',
  LIVING_SITUATION: 'P2',
  CARING: 'P3',
  WORKSPACE: 'P4',
  CONTACT_DATE: 'CZ',
  OUT_OF_BREATH: 'SB',
  SYMPTOM_DATE: 'SZ',
  DATA_DONATION: `${NO_XML}1`,
};

export const XML_ORDER = ['V', 'P', 'C', 'S', 'D', 'M'];
export const QUESTIONS: Question[] = [

  {
      category: CATEGORIES.PERSONAL,
      id: 'ARBO',
      text: 'ARBO',
      inputType: 'radio',
      options: [
        'structure_graphs',
        'structures'
    ],
      nextQuestionMap: [
          'structure_graphs',
          'structures'
      ],
     
      scoreMap: [
          1,
          1
      ],
      
  },{
  category: CATEGORIES.PERSONAL,
  id: 'PVi',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Periventricular hypothalamic nucleus, intermediate part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GU5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Gustatory areas, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: '',
  inputType: 'radio',
  nextQuestionMap: [
      'SI',
      'MA'
  ],
  options: [
      'SI',
      'MA'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Pallidum, ventral region'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CN',
  inputType: 'radio',
  nextQuestionMap: [
      'CNspg',
      'VCO',
      'DCO',
      'CNlam'
  ],
  options: [
      'CNspg',
      'VCO',
      'DCO',
      'CNlam'
  ],
  scoreMap: [
      0,
      0,
      0,
      0
  ],
  text: 'Cochlear nuclei'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISal4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterolateral visual area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AIp6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, posterior part, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BA',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nucleus of the accessory olfactory tract'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MM',
  inputType: 'radio',
  nextQuestionMap: [
      'Mmme'
  ],
  options: [
      'Mmme'
  ],
  scoreMap: [
      0
  ],
  text: 'Medial mammillary nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TU',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Tuberal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MEV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Midbrain trigeminal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MH',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial habenula'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RHP',
  inputType: 'radio',
  nextQuestionMap: [
      'SUB',
      'PRE',
      'PAR',
      'ENT',
      'POST'
  ],
  options: [
      'SUB',
      'PRE',
      'PAR',
      'ENT',
      'POST'
  ],
  scoreMap: [
      1,
      0,
      0,
      1,
      0
  ],
  text: 'Retrohippocampal region'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VIS6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Visual areas, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'KF',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Kolliker-Fuse subnucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBl6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, lateral part, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'isl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Islands of Calleja'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpm6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'posteromedial visual area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TMd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Tuberomammillary nucleus, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPd4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, dorsal part, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NTSm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the solitary tract, medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVZ',
  inputType: 'radio',
  nextQuestionMap: [
      'PVi',
      'ARH',
      'PVH',
      'PVa',
      'SO',
      'ASO'
  ],
  options: [
      'PVi',
      'ARH',
      'PVH',
      'PVa',
      'SO',
      'ASO'
  ],
  scoreMap: [
      0,
      0,
      1,
      0,
      0,
      1
  ],
  text: 'Periventricular zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ARH',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Arcuate hypothalamic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VM',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral medial nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial vestibular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TTd1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Taenia tecta, dorsal part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPagl',
  inputType: 'radio',
  nextQuestionMap: [
      'RSPagl6a',
      'RSPagl2-3',
      'RSPagl6b',
      'RSPagl5',
      'RSPagl1'
  ],
  options: [
      'RSPagl6a',
      'RSPagl2-3',
      'RSPagl6b',
      'RSPagl5',
      'RSPagl1'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Retrosplenial area, lateral agranular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISal2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterolateral visual area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACA1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MDRNd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medullary reticular nucleus, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COAa2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, anterior part, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSs5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Supplemental somatosensory area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp',
  inputType: 'radio',
  nextQuestionMap: [
      'SSp2-3',
      'SSp-bfd',
      'SSp6a',
      'SSp-ul',
      'SSp4',
      'SSp6b',
      'SSp5',
      'SSp-ll',
      'SSp1',
      'SSp-m',
      'SSp-tr',
      'SSp-n'
  ],
  options: [
      'SSp2-3',
      'SSp-bfd',
      'SSp6a',
      'SSp-ul',
      'SSp4',
      'SSp6b',
      'SSp5',
      'SSp-ll',
      'SSp1',
      'SSp-m',
      'SSp-tr',
      'SSp-n'
  ],
  scoreMap: [
      0,
      1,
      0,
      1,
      0,
      0,
      0,
      1,
      0,
      1,
      1,
      1
  ],
  text: 'Primary somatosensory area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LAT',
  inputType: 'radio',
  nextQuestionMap: [
      'SGN',
      'PO',
      'LP',
      'POL'
  ],
  options: [
      'SGN',
      'PO',
      'LP',
      'POL'
  ],
  scoreMap: [
      0,
      0,
      0,
      0
  ],
  text: 'Lateral group of the dorsal thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUM',
  inputType: 'radio',
  nextQuestionMap: [
      'SUMl',
      'SUMm'
  ],
  options: [
      'SUMl',
      'SUMm'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Supramammillary nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUBv-m',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subiculum, ventral part, molecular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DG-sg',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dentate gyrus, granule cell layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUD',
  inputType: 'radio',
  nextQuestionMap: [
      'AUDd',
      'AUDpo',
      'AUDp',
      'AUDv'
  ],
  options: [
      'AUDd',
      'AUDpo',
      'AUDp',
      'AUDv'
  ],
  scoreMap: [
      1,
      1,
      1,
      1
  ],
  text: 'Auditory areas'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VERM',
  inputType: 'radio',
  nextQuestionMap: [
      'NOD',
      'UVU',
      'PYR',
      'LING',
      'CUL',
      'FOTU',
      'CENT',
      'DEC'
  ],
  options: [
      'NOD',
      'UVU',
      'PYR',
      'LING',
      'CUL',
      'FOTU',
      'CENT',
      'DEC'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      1,
      0,
      1,
      0
  ],
  text: 'Vermal regions'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA2',
  inputType: 'radio',
  nextQuestionMap: [
      'CA2sr',
      'CA2so',
      'CA2slm',
      'CA2sp'
  ],
  options: [
      'CA2sr',
      'CA2so',
      'CA2slm',
      'CA2sp'
  ],
  scoreMap: [
      0,
      0,
      0,
      0
  ],
  text: 'Field CA2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PIR2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Piriform area, pyramidal layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LD',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral dorsal nucleus of thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DMHv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsomedial nucleus of the hypothalamus, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AIp5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, posterior part, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PCG',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Pontine central gray'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TT',
  inputType: 'radio',
  nextQuestionMap: [
      'TTd1',
      'TTd2',
      'TTd',
      'TTv1',
      'TTv2',
      'TTv3',
      'TTv1-3',
      'TTd3',
      'TTd1-4',
      'TTd4',
      'TTv'
  ],
  options: [
      'TTd1',
      'TTd2',
      'TTd',
      'TTv1',
      'TTv2',
      'TTv3',
      'TTv1-3',
      'TTd3',
      'TTd1-4',
      'TTd4',
      'TTv'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Taenia tecta'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTX5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cerebral cortex, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-m4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, mouth, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TR2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Postpiriform transition area, layers 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CH',
  inputType: 'radio',
  nextQuestionMap: [
      'CNU',
      'CTX'
  ],
  options: [
      'CNU',
      'CTX'
  ],
  scoreMap: [
      1,
      1
  ],
  text: 'Cerebrum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACAv6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, ventral part, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA1sr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA1, stratum radiatum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COApm2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, posterior part, medial zone, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHmpv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paraventricular hypothalamic nucleus, descending division, medial parvicellular part, ventral zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MG',
  inputType: 'radio',
  nextQuestionMap: [
      'MGd',
      'MGm',
      'MGv'
  ],
  options: [
      'MGd',
      'MGm',
      'MGv'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Medial geniculate complex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DMHp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsomedial nucleus of the hypothalamus, posterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AId5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, dorsal part, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VMHdm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventromedial hypothalamic nucleus, dorsomedial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AMBd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus ambiguus, dorsal division'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCiw',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior colliculus, motor related, intermediate white layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ll6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, lower limb, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSP',
  inputType: 'radio',
  nextQuestionMap: [
      'RSPagl',
      'RSPv',
      'RSPd'
  ],
  options: [
      'RSPagl',
      'RSPv',
      'RSPd'
  ],
  scoreMap: [
      1,
      1,
      1
  ],
  text: 'Retrosplenial area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTm6',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, dorsal zone, layer 6'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-n6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, nose, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TTd2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Taenia tecta, dorsal part, layers 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CEAc',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Central amygdalar nucleus, capsular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PERI1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Perirhinal area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SBPV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subparaventricular zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ll5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, lower limb, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPv',
  inputType: 'radio',
  nextQuestionMap: [
      'RSPv6a',
      'RSPv6b',
      'RSPv1',
      'RSPv5',
      'RSPv2-3',
      'RSPv2'
  ],
  options: [
      'RSPv6a',
      'RSPv6b',
      'RSPv1',
      'RSPv5',
      'RSPv2-3',
      'RSPv2'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Retrosplenial area, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DR',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal nucleus raphé'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MDm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Mediodorsal nucleus of the thalamus, medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISC4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Visceral area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AIv6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, ventral part, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDpo4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior auditory area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHlp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paraventricular hypothalamic nucleus, descending division, lateral parvicellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GU4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Gustatory areas, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-tr2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, trunk, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPA',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subparafascicular area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'FRP',
  inputType: 'radio',
  nextQuestionMap: [
      'FRP2-3',
      'FRP1'
  ],
  options: [
      'FRP2-3',
      'FRP1'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Frontal pole, cerebral cortex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISam6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anteromedial visual area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDp4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary auditory area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHmpd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paraventricular hypothalamic nucleus, parvicellular division, medial parvicellular part, dorsal zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IRN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Intermediate reticular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBvl',
  inputType: 'radio',
  nextQuestionMap: [
      'ORBvl6a',
      'ORBvl5',
      'ORBvl2-3',
      'ORBvl6b',
      'ORBvl1'
  ],
  options: [
      'ORBvl6a',
      'ORBvl5',
      'ORBvl2-3',
      'ORBvl6b',
      'ORBvl1'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Orbital area, ventrolateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA1',
  inputType: 'radio',
  nextQuestionMap: [
      'CA1sr',
      'CA1slm',
      'CA1so',
      'CA1sp'
  ],
  options: [
      'CA1sr',
      'CA1slm',
      'CA1so',
      'CA1sp'
  ],
  scoreMap: [
      0,
      0,
      0,
      0
  ],
  text: 'Field CA1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISl6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral visual area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DGmb-mo',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dentate gyrus medial blade, molecular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MB',
  inputType: 'radio',
  nextQuestionMap: [
      'MBsta',
      'MBsen',
      'MBmot'
  ],
  options: [
      'MBsta',
      'MBsen',
      'MBmot'
  ],
  scoreMap: [
      1,
      1,
      1
  ],
  text: 'Midbrain'
},{
  category: CATEGORIES.PERSONAL,
  id: 'structures',
  inputType: 'radio',
  nextQuestionMap: [
      'Brain'
  ],
  options: [
      'Brain'
  ],
  scoreMap: [
      1
  ],
  text: 'structures'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUMl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Supramammillary nucleus, lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IGL',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Intergeniculate leaflet of the lateral geniculate complex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-m2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, mouth, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPv6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, ventral part, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACAd1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, dorsal part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MBsta',
  inputType: 'radio',
  nextQuestionMap: [
      'PPN',
      'RAmb',
      'SNc'
  ],
  options: [
      'PPN',
      'RAmb',
      'SNc'
  ],
  scoreMap: [
      0,
      1,
      0
  ],
  text: 'Midbrain, behavioral state related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MEApd-c',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial amygdalar nucleus, posterodorsal part, sublayer c'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NTSco',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the solitary tract, commissural part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NOD',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nodulus (X)'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anteroventral nucleus of thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTm2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, dorsal zone, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOs5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Secondary motor area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MDRN',
  inputType: 'radio',
  nextQuestionMap: [
      'MDRNd',
      'MDRNv'
  ],
  options: [
      'MDRNd',
      'MDRNv'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Medullary reticular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NB',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the brachium of the inferior colliculus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ICB',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Infracerebellar nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PPN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Pedunculopontine nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MO6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Somatomotor areas, Layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-tr6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, trunk, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DORpm',
  inputType: 'radio',
  nextQuestionMap: [
      'LAT',
      'EPI',
      'MTN',
      'GENv',
      'ATN',
      'RT',
      'MED',
      'ILM'
  ],
  options: [
      'LAT',
      'EPI',
      'MTN',
      'GENv',
      'ATN',
      'RT',
      'MED',
      'ILM'
  ],
  scoreMap: [
      1,
      1,
      1,
      1,
      1,
      0,
      1,
      1
  ],
  text: 'Thalamus, polymodal association cortex related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SGN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Suprageniculate nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ul4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, upper limb, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VMHvl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventromedial hypothalamic nucleus, ventrolateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SOCm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior olivary complex, medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AMv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anteromedial nucleus, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ICc',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Inferior colliculus, central nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISl5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral visual area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSs1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Supplemental somatosensory area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDpo2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior auditory area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MD',
  inputType: 'radio',
  nextQuestionMap: [
      'MDm',
      'MDl',
      'MDc'
  ],
  options: [
      'MDm',
      'MDl',
      'MDc'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Mediodorsal nucleus of thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VPL',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral posterolateral nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PRE',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Presubiculum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA1slm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA1, stratum lacunosum-moleculare'
},{
  category: CATEGORIES.PERSONAL,
  id: 'INC',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Interstitial nucleus of Cajal'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GRN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Gigantocellular reticular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISal5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterolateral visual area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GPe',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Globus pallidus, external segment'
},{
  category: CATEGORIES.PERSONAL,
  id: 'OT',
  inputType: 'radio',
  nextQuestionMap: [
      'isl',
      'islm',
      'OT1-3',
      'OT3',
      'OT2',
      'OT1'
  ],
  options: [
      'isl',
      'islm',
      'OT1-3',
      'OT3',
      'OT2',
      'OT1'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Olfactory tubercle'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AON1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior olfactory nucleus, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHpm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paraventricular hypothalamic nucleus, magnocellular division, posterior magnocellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CS',
  inputType: 'radio',
  nextQuestionMap: [
      'CSl',
      'CSm'
  ],
  options: [
      'CSl',
      'CSm'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Superior central nucleus raphé'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VIS4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Visual areas, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AId1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, dorsal part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ECT',
  inputType: 'radio',
  nextQuestionMap: [
      'ECT2-3',
      'ECT6a',
      'ECT6b',
      'ECT5',
      'ECT1'
  ],
  options: [
      'ECT2-3',
      'ECT6a',
      'ECT6b',
      'ECT5',
      'ECT1'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Ectorhinal area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTp',
  inputType: 'radio',
  nextQuestionMap: [
      'BSTtr',
      'BSTpr',
      'BSTif',
      'BSTd',
      'BSTse'
  ],
  options: [
      'BSTtr',
      'BSTpr',
      'BSTif',
      'BSTd',
      'BSTse'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Bed nuclei of the stria terminalis, posterior division'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AIp1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, posterior part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PERI5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Perirhinal area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ICe',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Inferior colliculus, external nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPagl6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, lateral agranular part, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACAv5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, ventral part, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PPY',
  inputType: 'radio',
  nextQuestionMap: [
      'PPYd',
      'PPYs'
  ],
  options: [
      'PPYd',
      'PPYs'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Parapyramidal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISam1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anteromedial visual area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MY-mot',
  inputType: 'radio',
  nextQuestionMap: [
      'IRN',
      'MDRN',
      'ICB',
      'GRN',
      'PPY',
      'MARN',
      'PAS',
      'ISN',
      'PMR',
      'VNC',
      'PGRN',
      'XII',
      'x',
      'LRN',
      'ACVII',
      'IO',
      'VI',
      'EV',
      'PARN',
      'y',
      'LIN',
      'PHY',
      'AMB',
      'ECO',
      'ACVI',
      'DMX',
      'INV',
      'VII'
  ],
  options: [
      'IRN',
      'MDRN',
      'ICB',
      'GRN',
      'PPY',
      'MARN',
      'PAS',
      'ISN',
      'PMR',
      'VNC',
      'PGRN',
      'XII',
      'x',
      'LRN',
      'ACVII',
      'IO',
      'VI',
      'EV',
      'PARN',
      'y',
      'LIN',
      'PHY',
      'AMB',
      'ECO',
      'ACVI',
      'DMX',
      'INV',
      'VII'
  ],
  scoreMap: [
      0,
      1,
      0,
      0,
      1,
      0,
      0,
      0,
      0,
      1,
      1,
      0,
      0,
      1,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      1,
      1,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Medulla, motor related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPd6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, dorsal part, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AHNp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior hypothalamic nucleus, posterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTX6',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cerebral cortex, layer 6'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PD',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterodorsal preoptic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MARN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Magnocellular reticular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACAd6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, dorsal part, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MBsen',
  inputType: 'radio',
  nextQuestionMap: [
      'MEV',
      'NB',
      'SCs',
      'SAG',
      'PBG',
      'IC'
  ],
  options: [
      'MEV',
      'NB',
      'SCs',
      'SAG',
      'PBG',
      'IC'
  ],
  scoreMap: [
      0,
      0,
      1,
      0,
      0,
      1
  ],
  text: 'Midbrain, sensory related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MO6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Somatomotor areas, Layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DGlb-mo',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dentate gyrus lateral blade, molecular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TEa6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Temporal association areas, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'HEM',
  inputType: 'radio',
  nextQuestionMap: [
      'AN',
      'FL',
      'COPY',
      'PFL',
      'PRM',
      'SIM'
  ],
  options: [
      'AN',
      'FL',
      'COPY',
      'PFL',
      'PRM',
      'SIM'
  ],
  scoreMap: [
      1,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Hemispheric regions'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PRP',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus prepositus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDv2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral auditory area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISC6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Visceral area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MDl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Mediodorsal nucleus of the thalamus, lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'UVU',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Uvula (IX)'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VAL',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral anterior-lateral complex of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PL6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Prelimbic area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISp5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary visual area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PL2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Prelimbic area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PAS',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parasolitary nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ILA2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Infralimbic area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUBv-sr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subiculum, ventral part, stratum radiatum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'STRd',
  inputType: 'radio',
  nextQuestionMap: [
      'CP'
  ],
  options: [
      'CP'
  ],
  scoreMap: [
      0
  ],
  text: 'Striatum dorsal region'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ISN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Inferior salivatory nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACAd5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, dorsal part, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CUL5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lobule V'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MO1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Somatomotor areas, Layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ll2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, lower limb, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'islm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Major island of Calleja'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PMv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral premammillary nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PBls',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parabrachial nucleus, lateral division, superior lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AHNd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior hypothalamic nucleus, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AI',
  inputType: 'radio',
  nextQuestionMap: [
      'AIp',
      'AId',
      'AIv'
  ],
  options: [
      'AIp',
      'AId',
      'AIv'
  ],
  scoreMap: [
      1,
      1,
      1
  ],
  text: 'Agranular insular area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SLC',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subceruleus nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOp5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary motor area, Layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AId6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, dorsal part, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COApm3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, posterior part, medial zone, layer 3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PMR',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paramedian reticular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORB5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DGlb-po',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dentate gyrus lateral blade, polymorph layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AN',
  inputType: 'radio',
  nextQuestionMap: [
      'ANcr1',
      'ANcr2'
  ],
  options: [
      'ANcr1',
      'ANcr2'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Ansiform lobule'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPVOmdmd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Spinal nucleus of the trigeminal, oral part, middle dorsomedial part, dorsal zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA1so',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA1, stratum oriens'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LZ',
  inputType: 'radio',
  nextQuestionMap: [
      'TU',
      'ZI',
      'STN',
      'RCH',
      'PST',
      'LHA',
      'LPO',
      'PSTN'
  ],
  options: [
      'TU',
      'ZI',
      'STN',
      'RCH',
      'PST',
      'LHA',
      'LPO',
      'PSTN'
  ],
  scoreMap: [
      0,
      1,
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Hypothalamic lateral zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VIS6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Visual areas, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DP6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal peduncular area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NIS',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus intercalates'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTX1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cerebral cortex, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TTd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Taenia tecta, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior complex of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LSc',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral septal nucleus, caudal (caudodorsal) part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VIS5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Visual areas, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTm5-6',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, dorsal zone, layer 5/6'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISal',
  inputType: 'radio',
  nextQuestionMap: [
      'VISal4',
      'VISal2-3',
      'VISal5',
      'VISal6a',
      'VISal6b',
      'VISal1'
  ],
  options: [
      'VISal4',
      'VISal2-3',
      'VISal5',
      'VISal6a',
      'VISal6b',
      'VISal1'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Anterolateral visual area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUMm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Supramammillary nucleus, medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpl6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterolateral visual area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-m5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, mouth, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBm1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, medial part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PL6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Prelimbic area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PAG',
  inputType: 'radio',
  nextQuestionMap: [
      'INC',
      'PRC',
      'ND'
  ],
  options: [
      'INC',
      'PRC',
      'ND'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Periaqueductal gray'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AHA',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior hypothalamic area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUBd-sp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subiculum, dorsal part, pyramidal layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOBgr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Main olfactory bulb, granule layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBvl6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, ventrolateral part, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA',
  inputType: 'radio',
  nextQuestionMap: [
      'CA2',
      'CA1',
      'CA3'
  ],
  options: [
      'CA2',
      'CA1',
      'CA3'
  ],
  scoreMap: [
      1,
      1,
      1
  ],
  text: 'Ammons Horn'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBvl5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, ventrolateral part, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dentate nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'EMTmv1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, ventral zone, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MPT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial pretectal area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTXsp',
  inputType: 'radio',
  nextQuestionMap: [
      'BLA',
      'PA',
      'BMA',
      'EP',
      'CLA',
      'LA',
      'CTXsp6b'
  ],
  options: [
      'BLA',
      'PA',
      'BMA',
      'EP',
      'CLA',
      'LA',
      'CTXsp6b'
  ],
  scoreMap: [
      1,
      0,
      1,
      1,
      0,
      0,
      0
  ],
  text: 'Cortical subplate'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PIR1-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Piriform area, layers 1-3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DP2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal peduncular area, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MS',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial septal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-n4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, nose, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VNC',
  inputType: 'radio',
  nextQuestionMap: [
      'MV',
      'SUV',
      'SPIV',
      'LAV'
  ],
  options: [
      'MV',
      'SUV',
      'SPIV',
      'LAV'
  ],
  scoreMap: [
      0,
      0,
      0,
      0
  ],
  text: 'Vestibular nuclei'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AD',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterodorsal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ICd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Inferior colliculus, dorsal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MEAav',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial amygdalar nucleus, anteroventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-bfd5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, barrel field, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpm1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'posteromedial visual area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'FF',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Fields of Forel'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COApm1-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, posterior part, medial zone, layer 1-3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RAmb',
  inputType: 'radio',
  nextQuestionMap: [
      'DR',
      'CLI',
      'IPN',
      'IF',
      'RL'
  ],
  options: [
      'DR',
      'CLI',
      'IPN',
      'IF',
      'RL'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Midbrain raphe nuclei'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PAA1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Piriform-amygdalar area, molecular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral terminal nucleus of the accessory optic tract'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PL2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Prelimbic area, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACAv6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, ventral part, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTX2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cerebral cortex, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COApm1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, posterior part, medial zone, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCs',
  inputType: 'radio',
  nextQuestionMap: [
      'SCsg',
      'SCzo',
      'SCop'
  ],
  options: [
      'SCsg',
      'SCzo',
      'SCop'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Superior colliculus, sensory related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPVO',
  inputType: 'radio',
  nextQuestionMap: [
      'SPVOmdmd',
      'SPVOcdm',
      'SPVOrdm',
      'SPVOmdmv',
      'SPVOvl'
  ],
  options: [
      'SPVOmdmd',
      'SPVOcdm',
      'SPVOrdm',
      'SPVOmdmv',
      'SPVOvl'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Spinal nucleus of the trigeminal, oral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MEApd-a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial amygdalar nucleus, posterodorsal part, sublayer a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'OT1-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Olfactory tubercle, layers 1-3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AM',
  inputType: 'radio',
  nextQuestionMap: [
      'AMv',
      'AMd'
  ],
  options: [
      'AMv',
      'AMd'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Anteromedial nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PRC',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Precommissural nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PS',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parastrial nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'POR',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior olivary complex, periolivary region'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISam2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anteromedial visual area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOp2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary motor area, Layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PGRN',
  inputType: 'radio',
  nextQuestionMap: [
      'PGRNd',
      'PGRNl'
  ],
  options: [
      'PGRNd',
      'PGRNl'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Paragigantocellular reticular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COAa3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, anterior part, layer 3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'OP',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Olivary pretectal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCig-a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior colliculus, motor related, intermediate gray layer, sublayer a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NC',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus circularis'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BMAp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Basomedial amygdalar nucleus, posterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACA',
  inputType: 'radio',
  nextQuestionMap: [
      'ACA1',
      'ACA2-3',
      'ACA6a',
      'ACAd',
      'ACAv',
      'ACA5',
      'ACA6b'
  ],
  options: [
      'ACA1',
      'ACA2-3',
      'ACA6a',
      'ACAd',
      'ACAv',
      'ACA5',
      'ACA6b'
  ],
  scoreMap: [
      0,
      0,
      0,
      1,
      1,
      0,
      0
  ],
  text: 'Anterior cingulate area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'STRv',
  inputType: 'radio',
  nextQuestionMap: [
      'OT',
      'ACB',
      'FS'
  ],
  options: [
      'OT',
      'ACB',
      'FS'
  ],
  scoreMap: [
      1,
      0,
      0
  ],
  text: 'Striatum ventral region'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOp1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary motor area, Layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORB1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PL1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Prelimbic area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTju',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, anterior division, juxtacapsular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AId2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, dorsal part, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TTv1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Taenia tecta, ventral part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-n2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, nose, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTtr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, posterior division, transverse nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'OLF',
  inputType: 'radio',
  nextQuestionMap: [
      'TT',
      'PIR',
      'MOB',
      'PAA',
      'AOB',
      'AON',
      'DP',
      'NLOT',
      'TR',
      'COA'
  ],
  options: [
      'TT',
      'PIR',
      'MOB',
      'PAA',
      'AOB',
      'AON',
      'DP',
      'NLOT',
      'TR',
      'COA'
  ],
  scoreMap: [
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1
  ],
  text: 'Olfactory bulb'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-bfd',
  inputType: 'radio',
  nextQuestionMap: [
      'SSp-bfd5',
      'SSp-bfd1',
      'SSp-bfd2-3',
      'SSp-bfd6b',
      'SSp-bfd6a',
      'SSp-bfd4'
  ],
  options: [
      'SSp-bfd5',
      'SSp-bfd1',
      'SSp-bfd2-3',
      'SSp-bfd6b',
      'SSp-bfd6a',
      'SSp-bfd4'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Primary somatosensory area, barrel field'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AIp2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, posterior part, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior tegmental nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA3slu',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA3, stratum lucidum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHp',
  inputType: 'radio',
  nextQuestionMap: [
      'PVHmpd',
      'PVHpv',
      'PVHap'
  ],
  options: [
      'PVHmpd',
      'PVHpv',
      'PVHap'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Paraventricular hypothalamic nucleus, parvicellular division'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MRNmg',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Midbrain reticular nucleus, magnocellular part, general'
},{
  category: CATEGORIES.PERSONAL,
  id: 'EW',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Edinger-Westphal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBl6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, lateral part, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SOC',
  inputType: 'radio',
  nextQuestionMap: [
      'SOCm',
      'POR',
      'SOCl'
  ],
  options: [
      'SOCm',
      'POR',
      'SOCl'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Superior olivary complex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTX3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cerebral cortex, layer 3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AMBv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus ambiguus, ventral division'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPd1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, dorsal part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'OV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Vascular organ of the lamina terminalis'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, lateral part, layer 3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NTSge',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the solitary tract, gelatinous part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IAD',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Interanterodorsal nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBm5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, medial part, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DGmb-po',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dentate gyrus medial blade, polymorph layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MPNc',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial preoptic nucleus, central part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LSX',
  inputType: 'radio',
  nextQuestionMap: [
      'SF',
      'SH',
      'LS'
  ],
  options: [
      'SF',
      'SH',
      'LS'
  ],
  scoreMap: [
      0,
      0,
      1
  ],
  text: 'Lateral septal complex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COApl',
  inputType: 'radio',
  nextQuestionMap: [
      'COApl1-2',
      'COApl2',
      'COApl3',
      'COApl1-3',
      'COApl1'
  ],
  options: [
      'COApl1-2',
      'COApl2',
      'COApl3',
      'COApl1-3',
      'COApl1'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Cortical amygdalar area, posterior part, lateral zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CB',
  inputType: 'radio',
  nextQuestionMap: [
      'CBN',
      'CBX'
  ],
  options: [
      'CBN',
      'CBX'
  ],
  scoreMap: [
      1,
      1
  ],
  text: 'Cerebellum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOp6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary motor area, Layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MO',
  inputType: 'radio',
  nextQuestionMap: [
      'MO6a',
      'MO6b',
      'MO1',
      'MO5',
      'MOp',
      'MOs',
      'MO2-3'
  ],
  options: [
      'MO6a',
      'MO6b',
      'MO1',
      'MO5',
      'MOp',
      'MOs',
      'MO2-3'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      1,
      1,
      0
  ],
  text: 'Somatomotor areas'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VMHc',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventromedial hypothalamic nucleus, central part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CLI',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Central linear nucleus raphe'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-tr1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, trunk, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AIv5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, ventral part, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BLAp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Basolateral amygdalar nucleus, posterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACB',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus accumbens'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PIR',
  inputType: 'radio',
  nextQuestionMap: [
      'PIR2',
      'PIR1-3',
      'PIR3',
      'PIR1'
  ],
  options: [
      'PIR2',
      'PIR1-3',
      'PIR3',
      'PIR1'
  ],
  scoreMap: [
      0,
      0,
      0,
      0
  ],
  text: 'Piriform area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CU',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cuneate nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RPO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus raphe pontis'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial terminal nucleus of the accessory optic tract'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BLAv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Basolateral amygdalar nucleus, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AON2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior olfactory nucleus, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'FRP2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Frontal pole, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISC1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Visceral area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CP',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Caudoputamen'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BST',
  inputType: 'radio',
  nextQuestionMap: [
      'BSTp',
      'BSTa'
  ],
  options: [
      'BSTp',
      'BSTa'
  ],
  scoreMap: [
      1,
      1
  ],
  text: 'Bed nuclei of the stria terminalis'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AOBgr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Accessory olfactory bulb, granular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'P-sat',
  inputType: 'radio',
  nextQuestionMap: [
      'CS',
      'SLC',
      'RPO',
      'NI',
      'SLD',
      'PRNr',
      'LDT',
      'LC'
  ],
  options: [
      'CS',
      'SLC',
      'RPO',
      'NI',
      'SLD',
      'PRNr',
      'LDT',
      'LC'
  ],
  scoreMap: [
      1,
      0,
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Pons, behavioral state related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUBv-sp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subiculum, ventral part, pyramidal layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORB6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AIp',
  inputType: 'radio',
  nextQuestionMap: [
      'AIp6b',
      'AIp5',
      'AIp1',
      'AIp2-3',
      'AIp6a'
  ],
  options: [
      'AIp6b',
      'AIp5',
      'AIp1',
      'AIp2-3',
      'AIp6a'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Agranular insular area, posterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'EPI',
  inputType: 'radio',
  nextQuestionMap: [
      'MH',
      'PIN',
      'LH'
  ],
  options: [
      'MH',
      'PIN',
      'LH'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Epithalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHdp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paraventricular hypothalamic nucleus, descending division, dorsal parvicellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TTv2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Taenia tecta, ventral part, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paraventricular nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-bfd1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, barrel field, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MY',
  inputType: 'radio',
  nextQuestionMap: [
      'MY-mot',
      'My-sen',
      'MY-sat'
  ],
  options: [
      'MY-mot',
      'My-sen',
      'MY-sat'
  ],
  scoreMap: [
      1,
      1,
      1
  ],
  text: 'Medulla'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PF',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parafascicular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPVOcdm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Spinal nucleus of the trigeminal, oral part, caudal dorsomedial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DGcr',
  inputType: 'radio',
  nextQuestionMap: [
      'DGcr-mo',
      'DGcr-sg',
      'DGcr-po'
  ],
  options: [
      'DGcr-mo',
      'DGcr-sg',
      'DGcr-po'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Dentate gyrus crest'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AId',
  inputType: 'radio',
  nextQuestionMap: [
      'AId5',
      'AId1',
      'AId6b',
      'AId2-3',
      'AId6a'
  ],
  options: [
      'AId5',
      'AId1',
      'AId6b',
      'AId2-3',
      'AId6a'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Agranular insular area, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVH',
  inputType: 'radio',
  nextQuestionMap: [
      'PVHp',
      'PVHm'
  ],
  options: [
      'PVHp',
      'PVHm'
  ],
  scoreMap: [
      1,
      1
  ],
  text: 'Paraventricular hypothalamic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MEA',
  inputType: 'radio',
  nextQuestionMap: [
      'MEAav',
      'MEApv',
      'MEAad',
      'MEApd'
  ],
  options: [
      'MEAav',
      'MEApv',
      'MEAad',
      'MEApd'
  ],
  scoreMap: [
      0,
      0,
      0,
      1
  ],
  text: 'Medial amygdalar nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'FL',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Flocculus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CNU',
  inputType: 'radio',
  nextQuestionMap: [
      'PAL',
      'STR'
  ],
  options: [
      'PAL',
      'STR'
  ],
  scoreMap: [
      1,
      1
  ],
  text: 'Cerebral nuclei'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NOT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the optic tract'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BLA',
  inputType: 'radio',
  nextQuestionMap: [
      'BLAp',
      'BLAv',
      'BLAa'
  ],
  options: [
      'BLAp',
      'BLAv',
      'BLAa'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Basolateral amygdalar nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTm3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, dorsal zone, layer 3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpl1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterolateral visual area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TEa6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Temporal association areas, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VIS1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Visual areas, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parataenial nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPVOrdm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Spinal nucleus of the trigeminal, oral part, rostral dorsomedial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NI',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus incertus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PAA2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Piriform-amygdalar area, pyramidal layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPFp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subparafascicular nucleus, parvicellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ND',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of Darkschewitsch'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDd',
  inputType: 'radio',
  nextQuestionMap: [
      'AUDd6a',
      'AUDd6b',
      'AUDd4',
      'AUDd1',
      'AUDd5',
      'AUDd2-3'
  ],
  options: [
      'AUDd6a',
      'AUDd6b',
      'AUDd4',
      'AUDd1',
      'AUDd5',
      'AUDd2-3'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Dorsal auditory area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-m6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, mouth, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SF',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Septofimbrial nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COAp',
  inputType: 'radio',
  nextQuestionMap: [
      'COApl',
      'COApm'
  ],
  options: [
      'COApl',
      'COApm'
  ],
  scoreMap: [
      1,
      1
  ],
  text: 'Cortical amygdalar area, posterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDd6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal auditory area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDp1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary auditory area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PGRNd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paragigantocellular reticular nucleus, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISam4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anteromedial visual area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCsg',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior colliculus, superficial gray layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NR',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of Roller'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PA',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior amygdalar nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PYR',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Pyramus (VIII)'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDpo5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior auditory area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VENT',
  inputType: 'radio',
  nextQuestionMap: [
      'VM',
      'VAL',
      'VP'
  ],
  options: [
      'VM',
      'VAL',
      'VP'
  ],
  scoreMap: [
      0,
      0,
      1
  ],
  text: 'Ventral group of the dorsal thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CENT2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lobule II'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ZI',
  inputType: 'radio',
  nextQuestionMap: [
      'FF',
      'A13'
  ],
  options: [
      'FF',
      'A13'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Zona incerta'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MGd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial geniculate complex, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CEA',
  inputType: 'radio',
  nextQuestionMap: [
      'CEAc',
      'CEAl',
      'CEAm'
  ],
  options: [
      'CEAc',
      'CEAl',
      'CEAm'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Central amygdalar nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PPT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior pretectal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PAR',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parasubiculum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVa',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Periventricular hypothalamic nucleus, anterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENT',
  inputType: 'radio',
  nextQuestionMap: [
      'ENTl',
      'ENTm',
      'ENTmv'
  ],
  options: [
      'ENTl',
      'ENTm',
      'ENTmv'
  ],
  scoreMap: [
      1,
      1,
      1
  ],
  text: 'Entorhinal area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SNr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Substantia nigra, reticular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TRN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Tegmental reticular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IPN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Interpeduncular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPv6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, ventral part, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPd5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, dorsal part, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTfu',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, anterior division, fusiform nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SAG',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus sagulum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NTS',
  inputType: 'radio',
  nextQuestionMap: [
      'NTSm',
      'NTSco',
      'NTSge',
      'NTSce',
      'NTSl'
  ],
  options: [
      'NTSm',
      'NTSco',
      'NTSge',
      'NTSce',
      'NTSl'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Nucleus of the solitary tract'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LP',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral posterior nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CNspg',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cochlear nucleus, subpedunclular granular region'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AMd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anteromedial nucleus, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PGRNl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paragigantocellular reticular nucleus, lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPv1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, ventral part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PAA3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Piriform-amygdalar area, polymorph layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CM',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Central medial nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NTSce',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the solitary tract, central part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LING',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lingula (I)'
},{
  category: CATEGORIES.PERSONAL,
  id: 'STN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subthalamic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Red Nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOB',
  inputType: 'radio',
  nextQuestionMap: [
      'MOBgr',
      'MOBmi',
      'MOBgl',
      'MOBopl',
      'MOBipl'
  ],
  options: [
      'MOBgr',
      'MOBmi',
      'MOBgl',
      'MOBopl',
      'MOBipl'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Main olfactory bulb'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MGm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial geniculate complex, medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MTN',
  inputType: 'radio',
  nextQuestionMap: [
      'PVT',
      'PT',
      'RE'
  ],
  options: [
      'PVT',
      'PT',
      'RE'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Midline group of the dorsal thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BS',
  inputType: 'radio',
  nextQuestionMap: [
      'MB',
      'HB',
      'IB'
  ],
  options: [
      'MB',
      'HB',
      'IB'
  ],
  scoreMap: [
      1,
      1,
      1
  ],
  text: 'Brain stem'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISal6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterolateral visual area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NLL',
  inputType: 'radio',
  nextQuestionMap: [
      'NLLv',
      'NLLh',
      'NLLd'
  ],
  options: [
      'NLLv',
      'NLLh',
      'NLLd'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Nucleus of the lateral lemniscus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PL',
  inputType: 'radio',
  nextQuestionMap: [
      'PL6a',
      'PL2-3',
      'PL6b',
      'PL2',
      'PL1',
      'PL5'
  ],
  options: [
      'PL6a',
      'PL2-3',
      'PL6b',
      'PL2',
      'PL1',
      'PL5'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Prelimbic area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GU',
  inputType: 'radio',
  nextQuestionMap: [
      'GU5',
      'GU4',
      'GU2-3',
      'GU6a',
      'GU6b',
      'GU1'
  ],
  options: [
      'GU5',
      'GU4',
      'GU2-3',
      'GU6a',
      'GU6b',
      'GU1'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Gustatory areas'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-m1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, mouth, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VTN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral tegmental nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'OT3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Olfactory tubercle, polymorph layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOs6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Secondary motor area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA3sr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA3, stratum radiatum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SOCl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior olivary complex, lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PG',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Pontine gray'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AIv1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, ventral part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHmm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paraventricular hypothalamic nucleus, magnocellular division, medial magnocellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PBm',
  inputType: 'radio',
  nextQuestionMap: [
      'PBmm',
      'PBme',
      'PBmv'
  ],
  options: [
      'PBmm',
      'PBme',
      'PBmv'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Parabrachial nucleus, medial division'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTov',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, anterior division, oval nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpl2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterolateral visual area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ul6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, upper limb, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PRT',
  inputType: 'radio',
  nextQuestionMap: [
      'MPT',
      'OP',
      'NOT',
      'PPT',
      'APN',
      'NPC'
  ],
  options: [
      'MPT',
      'OP',
      'NOT',
      'PPT',
      'APN',
      'NPC'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Pretectal region'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GENv',
  inputType: 'radio',
  nextQuestionMap: [
      'IGL',
      'SubG',
      'LGv'
  ],
  options: [
      'IGL',
      'SubG',
      'LGv'
  ],
  scoreMap: [
      0,
      0,
      1
  ],
  text: 'Geniculate group, ventral thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPF',
  inputType: 'radio',
  nextQuestionMap: [
      'SPFp',
      'SPFm'
  ],
  options: [
      'SPFp',
      'SPFm'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Subparafascicular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCdg',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior colliculus, motor related, deep gray layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDpo',
  inputType: 'radio',
  nextQuestionMap: [
      'AUDpo4',
      'AUDpo2-3',
      'AUDpo5',
      'AUDpo6a',
      'AUDpo6b',
      'AUDpo1'
  ],
  options: [
      'AUDpo4',
      'AUDpo2-3',
      'AUDpo5',
      'AUDpo6a',
      'AUDpo6b',
      'AUDpo1'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Posterior auditory area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ll1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, lower limb, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IMD',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Intermediodorsal nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BMA',
  inputType: 'radio',
  nextQuestionMap: [
      'BMAp',
      'BMAa'
  ],
  options: [
      'BMAp',
      'BMAa'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Basomedial amygdalar nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTm2a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, dorsal zone, layer 2a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PTLp1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior parietal association areas, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MEZ',
  inputType: 'radio',
  nextQuestionMap: [
      'PMv',
      'MPN',
      'PH',
      'AHN',
      'MBO',
      'PVHd',
      'VMH',
      'PMd'
  ],
  options: [
      'PMv',
      'MPN',
      'PH',
      'AHN',
      'MBO',
      'PVHd',
      'VMH',
      'PMd'
  ],
  scoreMap: [
      0,
      1,
      0,
      1,
      1,
      1,
      1,
      0
  ],
  text: 'Hypothalamic medial zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTpr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, posterior division, principal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NLLv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the lateral lemniscus, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCdw',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior colliculus, motor related, deep white layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ECT2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ectorhinal area/Layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ILA6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Infralimbic area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DP1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal peduncular area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VTA',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral tegmental area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl',
  inputType: 'radio',
  nextQuestionMap: [
      'ENTl3',
      'ENTl6b',
      'ENTl2b',
      'ENTl2-3',
      'ENTl4',
      'ENTl5-6',
      'ENTl2a',
      'ENTl1',
      'ENTl6a',
      'ENTl5',
      'ENTl4-5',
      'ENTl2'
  ],
  options: [
      'ENTl3',
      'ENTl6b',
      'ENTl2b',
      'ENTl2-3',
      'ENTl4',
      'ENTl5-6',
      'ENTl2a',
      'ENTl1',
      'ENTl6a',
      'ENTl5',
      'ENTl4-5',
      'ENTl2'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Entorhinal area, lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSs',
  inputType: 'radio',
  nextQuestionMap: [
      'SSs5',
      'SSs1',
      'SSs2-3',
      'SSs6a',
      'SSs6b',
      'SSs4'
  ],
  options: [
      'SSs5',
      'SSs1',
      'SSs2-3',
      'SSs6a',
      'SSs6b',
      'SSs4'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Supplemental somatosensory area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TEa4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Temporal association areas, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ATN',
  inputType: 'radio',
  nextQuestionMap: [
      'LD',
      'AV',
      'AD',
      'AM',
      'IAD',
      'IAM'
  ],
  options: [
      'LD',
      'AV',
      'AD',
      'AM',
      'IAD',
      'IAM'
  ],
  scoreMap: [
      0,
      0,
      0,
      1,
      0,
      0
  ],
  text: 'Anterior group of the dorsal thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBvl2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, ventrolateral part, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUBd',
  inputType: 'radio',
  nextQuestionMap: [
      'SUBd-sp',
      'SUBd-m',
      'SUBd-sr'
  ],
  options: [
      'SUBd-sp',
      'SUBd-m',
      'SUBd-sr'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Subiculum, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GENd',
  inputType: 'radio',
  nextQuestionMap: [
      'MG',
      'LGd'
  ],
  options: [
      'MG',
      'LGd'
  ],
  scoreMap: [
      1,
      0
  ],
  text: 'Geniculate group, dorsal thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CL',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Central lateral nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PBG',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parabigeminal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AIv6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, ventral part, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior salivatory nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal terminal nucleus of the accessory optic tract'
},{
  category: CATEGORIES.PERSONAL,
  id: 'OT2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Olfactory tubercle, pyramidal layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RCH',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrochiasmatic area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RM',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus raphe magnus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'XII',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Hypoglossal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'V',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Motor nucleus of trigeminal'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDp5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary auditory area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDv6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral auditory area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ANcr1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Crus 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'EP',
  inputType: 'radio',
  nextQuestionMap: [
      'EPv',
      'EPd'
  ],
  options: [
      'EPv',
      'EPd'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Endopiriform nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PSCH',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Suprachiasmatic preoptic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISC5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Visceral area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSs2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Supplemental somatosensory area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RR',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Midbrain reticular nucleus, retrorubral area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, lateral part, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpm',
  inputType: 'radio',
  nextQuestionMap: [
      'VISpm6a',
      'VISpm1',
      'VISpm6b',
      'VISpm4',
      'VISpm2-3',
      'VISpm5'
  ],
  options: [
      'VISpm6a',
      'VISpm1',
      'VISpm6b',
      'VISpm4',
      'VISpm2-3',
      'VISpm5'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'posteromedial visual area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-bfd2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, barrel field, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'FC',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Fasciola cinerea'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CBXpu',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cerebellar cortex, Purkinje layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CUL',
  inputType: 'radio',
  nextQuestionMap: [
      'CUL5',
      'CUL4-5',
      'CUL4'
  ],
  options: [
      'CUL5',
      'CUL4-5',
      'CUL4'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Culmen'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PPYd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parapyramidal nucleus, deep part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PB',
  inputType: 'radio',
  nextQuestionMap: [
      'KF',
      'PBm',
      'PBl'
  ],
  options: [
      'KF',
      'PBm',
      'PBl'
  ],
  scoreMap: [
      0,
      1,
      1
  ],
  text: 'Parabrachial nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LRNp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral reticular nucleus, parvicellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SG',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Supragenual nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTXpl',
  inputType: 'radio',
  nextQuestionMap: [
      'OLF',
      'HPF',
      'Isocortex'
  ],
  options: [
      'OLF',
      'HPF',
      'Isocortex'
  ],
  scoreMap: [
      1,
      1,
      1
  ],
  text: 'Cortical plate'
},{
  category: CATEGORIES.PERSONAL,
  id: 'EPv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Endopiriform nucleus, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ul5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, upper limb, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'OT1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Olfactory tubercle, molecular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MPN',
  inputType: 'radio',
  nextQuestionMap: [
      'MPNc',
      'MPNl',
      'MPNm'
  ],
  options: [
      'MPNc',
      'MPNl',
      'MPNm'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Medial preoptic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTX1-6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cerebral cortex, layers 1-6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DP2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal peduncular area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MO5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Somatomotor areas, Layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MRNm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Midbrain reticular nucleus, magnocellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SMT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Submedial nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PAA',
  inputType: 'radio',
  nextQuestionMap: [
      'PAA1',
      'PAA2',
      'PAA3',
      'PAA1-3'
  ],
  options: [
      'PAA1',
      'PAA2',
      'PAA3',
      'PAA1-3'
  ],
  scoreMap: [
      0,
      0,
      0,
      0
  ],
  text: 'Piriform-amygdalar area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPv5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, ventral part, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PP',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Peripeduncular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'HIP',
  inputType: 'radio',
  nextQuestionMap: [
      'CA',
      'FC',
      'DG',
      'IG'
  ],
  options: [
      'CA',
      'FC',
      'DG',
      'IG'
  ],
  scoreMap: [
      1,
      0,
      1,
      0
  ],
  text: 'Hippocampal region'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MDc',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Mediodorsal nucleus of the thalamus, central part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA3',
  inputType: 'radio',
  nextQuestionMap: [
      'CA3slu',
      'CA3sr',
      'CA3slm',
      'CA3so',
      'CA3sp'
  ],
  options: [
      'CA3slu',
      'CA3sr',
      'CA3slm',
      'CA3so',
      'CA3sp'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Field CA3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RH',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Rhomboid nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ECT6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ectorhinal area/Layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AOB',
  inputType: 'radio',
  nextQuestionMap: [
      'AOBgr',
      'AOBmi',
      'AOBgl'
  ],
  options: [
      'AOBgr',
      'AOBmi',
      'AOBgl'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Accessory olfactory bulb'
},{
  category: CATEGORIES.PERSONAL,
  id: 'EMTmv2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, ventral zone, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DGcr-mo',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dentate gyrus crest, molecular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AON',
  inputType: 'radio',
  nextQuestionMap: [
      'AON1',
      'AON2',
      'AONl',
      'AONm',
      'AONpv',
      'AONe',
      'AONd'
  ],
  options: [
      'AON1',
      'AON2',
      'AONl',
      'AONm',
      'AONpv',
      'AONe',
      'AONd'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Anterior olfactory nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AOBmi',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Accessory olfactory bulb, mitral layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDd6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal auditory area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NLOT1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the lateral olfactory tract, molecular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GU2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Gustatory areas, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PRNc',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Pontine reticular nucleus, caudal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTm',
  inputType: 'radio',
  nextQuestionMap: [
      'ENTm6',
      'ENTm2',
      'ENTm5-6',
      'ENTm3',
      'ENTm2a',
      'ENTm2b',
      'ENTm4',
      'ENTm1',
      'ENTm5'
  ],
  options: [
      'ENTm6',
      'ENTm2',
      'ENTm5-6',
      'ENTm3',
      'ENTm2a',
      'ENTm2b',
      'ENTm4',
      'ENTm1',
      'ENTm5'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Entorhinal area, medial part, dorsal zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACA2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISam5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anteromedial visual area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CEAl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Central amygdalar nucleus, lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA2sr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA2, stratum radiatum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PBl',
  inputType: 'radio',
  nextQuestionMap: [
      'PBls',
      'PBlc',
      'PBld',
      'PBle',
      'PBlv'
  ],
  options: [
      'PBls',
      'PBlc',
      'PBld',
      'PBle',
      'PBlv'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Parabrachial nucleus, lateral division'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PST',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Preparasubthalamic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ul',
  inputType: 'radio',
  nextQuestionMap: [
      'SSp-ul4',
      'SSp-ul6b',
      'SSp-ul5',
      'SSp-ul2-3',
      'SSp-ul1',
      'SSP-ul6a'
  ],
  options: [
      'SSp-ul4',
      'SSp-ul6b',
      'SSp-ul5',
      'SSp-ul2-3',
      'SSp-ul1',
      'SSP-ul6a'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Primary somatosensory area, upper limb'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-n5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, nose, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl2b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, lateral part, layer 2b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpm6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'posteromedial visual area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CBN',
  inputType: 'radio',
  nextQuestionMap: [
      'DN',
      'FN',
      'IP'
  ],
  options: [
      'DN',
      'FN',
      'IP'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Cerebellar nuclei'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA1sp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA1, pyramidal layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PSV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Principal sensory nucleus of the trigeminal'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GPi',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Globus pallidus, internal segment'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, lateral part, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TTv3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Taenia tecta, ventral part, layer 3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCH',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Suprachiasmatic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DG',
  inputType: 'radio',
  nextQuestionMap: [
      'DG-sg',
      'DGcr',
      'DGlb',
      'DGmb'
  ],
  options: [
      'DG-sg',
      'DGcr',
      'DGlb',
      'DGmb'
  ],
  scoreMap: [
      0,
      1,
      1,
      1
  ],
  text: 'Dentate gyrus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDpo6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior auditory area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPd',
  inputType: 'radio',
  nextQuestionMap: [
      'RSPd4',
      'RSPd6b',
      'RSPd1',
      'RSPd5',
      'RSPd2-3',
      'RSPd6a'
  ],
  options: [
      'RSPd4',
      'RSPd6b',
      'RSPd1',
      'RSPd5',
      'RSPd2-3',
      'RSPd6a'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Retrosplenial area, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISl1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral visual area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHpv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paraventricular hypothalamic nucleus, parvicellular division, periventricular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SNI',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Substantia nigra, lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ABAMouse',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Allen Brain Atlas Adult Mouse Ontology'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COApm',
  inputType: 'radio',
  nextQuestionMap: [
      'COApm2',
      'COApm3',
      'COApm1-3',
      'COApm1',
      'COApm1-2'
  ],
  options: [
      'COApm2',
      'COApm3',
      'COApm1-3',
      'COApm1',
      'COApm1-2'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Cortical amygdalar area, posterior part, medial zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MEApv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial amygdalar nucleus, posteroventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTrh',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, anterior division, rhomboid nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CSl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior central nucleus raphe, lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, lateral part, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTX2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cerebral cortex, layer 2-3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'My-sen',
  inputType: 'radio',
  nextQuestionMap: [
      'CN',
      'SPVO',
      'NTS',
      'ECU',
      'SPVI',
      'NTB',
      'SPVC',
      'DCN',
      'z',
      'AP'
  ],
  options: [
      'CN',
      'SPVO',
      'NTS',
      'ECU',
      'SPVI',
      'NTB',
      'SPVC',
      'DCN',
      'z',
      'AP'
  ],
  scoreMap: [
      1,
      1,
      1,
      0,
      0,
      0,
      0,
      1,
      0,
      0
  ],
  text: 'Medulla, sensory related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RE',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of reunions'
},{
  category: CATEGORIES.PERSONAL,
  id: 'x',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus x'
},{
  category: CATEGORIES.PERSONAL,
  id: 'POL',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior limiting nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-tr4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, trunk, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ILA5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Infralimbic area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VIS',
  inputType: 'radio',
  nextQuestionMap: [
      'VIS6a',
      'VIS4',
      'VIS6b',
      'VIS5',
      'VISal',
      'VIS1',
      'VISpm',
      'VISam',
      'VISl',
      'VISp',
      'VISpl',
      'VIS2-3'
  ],
  options: [
      'VIS6a',
      'VIS4',
      'VIS6b',
      'VIS5',
      'VISal',
      'VIS1',
      'VISpm',
      'VISam',
      'VISl',
      'VISp',
      'VISpl',
      'VIS2-3'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      1,
      0,
      1,
      1,
      1,
      1,
      1,
      0
  ],
  text: 'Visual areas'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPFm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subparafascicular nucleus, magnocellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CEAm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Central amygdalar nucleus, medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MEAad',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial amygdalar nucleus, anterodorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DGlb-sg',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dentate gyrus lateral blade, granule cell layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PH',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior hypothalamic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TEa5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Temporal association areas, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCig-b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior colliculus, motor related, intermediate gray layer, sublayer b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MPNl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial preoptic nucleus, lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LHA',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral hypothalamic area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISam',
  inputType: 'radio',
  nextQuestionMap: [
      'VISam6b',
      'VISam1',
      'VISam2-3',
      'VISam4',
      'VISam5',
      'VISam6a'
  ],
  options: [
      'VISam6b',
      'VISam1',
      'VISam2-3',
      'VISam4',
      'VISam5',
      'VISam6a'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Anteromedial visual area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpl4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterolateral visual area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DMH',
  inputType: 'radio',
  nextQuestionMap: [
      'DMHv',
      'DMHp',
      'DMHa'
  ],
  options: [
      'DMHv',
      'DMHp',
      'DMHa'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Dorsomedial nucleus of the hypothalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AONl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior olfactory nucleus, lateral part'
},{
  id: 'structure_graphs',
  category: CATEGORIES.PERSONAL,
  text: 'structure_graphs',
  inputType: 'radio',
  options: [
    'ABAMouse'
],
  nextQuestionMap: [
      'ABAMouse'
  ],
 
  scoreMap: [
      0
  ],
  
},{
  category: CATEGORIES.PERSONAL,
  id: 'CLA',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Claustrum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTal',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, anterior division, anterolateral area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NDB',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Diagonal band nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AAA',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior amygdalar area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDd4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal auditory area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'HPF',
  inputType: 'radio',
  nextQuestionMap: [
      'RHP',
      'HIP'
  ],
  options: [
      'RHP',
      'HIP'
  ],
  scoreMap: [
      1,
      1
  ],
  text: 'Hippocampal formation'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTm2b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, dorsal zone, layer 2b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COApl1-2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, posterior part, lateral zone, layer 1-2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VMHa',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventromedial hypothalamic nucleus, anterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'FOTU',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Folium-tuber vermis (VII)'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DP',
  inputType: 'radio',
  nextQuestionMap: [
      'DP6a',
      'DP2',
      'DP1',
      'DP2-3',
      'DP5'
  ],
  options: [
      'DP6a',
      'DP2',
      'DP1',
      'DP2-3',
      'DP5'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Dorsal peduncular area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CSm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior central nucleus raphe, medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VPM',
  inputType: 'radio',
  nextQuestionMap: [
      'VPMpc'
  ],
  options: [
      'VPMpc'
  ],
  scoreMap: [
      0
  ],
  text: 'Ventral posteromedial nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOBmi',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Main olfactory bulb, mitral layer]'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MPO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial preoptic area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBm2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, medial part, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVR',
  inputType: 'radio',
  nextQuestionMap: [
      'SBPV',
      'PD',
      'AHA',
      'PS',
      'OV',
      'PSCH',
      'SCH',
      'DMH',
      'MPO',
      'PVpo',
      'AVP',
      'AVPV',
      'ADP',
      'MEPO',
      'PVp',
      'SFO',
      'VLPO'
  ],
  options: [
      'SBPV',
      'PD',
      'AHA',
      'PS',
      'OV',
      'PSCH',
      'SCH',
      'DMH',
      'MPO',
      'PVpo',
      'AVP',
      'AVPV',
      'ADP',
      'MEPO',
      'PVp',
      'SFO',
      'VLPO'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      1,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Periventricular region'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISC',
  inputType: 'radio',
  nextQuestionMap: [
      'VISC4',
      'VISC6b',
      'VISC1',
      'VISC5',
      'VISC2-3',
      'VISC6a'
  ],
  options: [
      'VISC4',
      'VISC6b',
      'VISC1',
      'VISC5',
      'VISC2-3',
      'VISC6a'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Visceral area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PBmm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parabrachial nucleus, medial division, medial medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ul2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, upper limb, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LRN',
  inputType: 'radio',
  nextQuestionMap: [
      'LRNp',
      'LRNm'
  ],
  options: [
      'LRNp',
      'LRNm'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Lateral reticular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'APN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior pretectal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDp',
  inputType: 'radio',
  nextQuestionMap: [
      'AUDp4',
      'AUDp1',
      'AUDp5',
      'AUDp6b',
      'AUDp2-3',
      'AUDp6a'
  ],
  options: [
      'AUDp4',
      'AUDp1',
      'AUDp5',
      'AUDp6b',
      'AUDp2-3',
      'AUDp6a'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Primary auditory area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDv',
  inputType: 'radio',
  nextQuestionMap: [
      'AUDv2-3',
      'AUDv6a',
      'AUDv6b',
      'AUDv4',
      'AUDv1',
      'AUDv5'
  ],
  options: [
      'AUDv2-3',
      'AUDv6a',
      'AUDv6b',
      'AUDv4',
      'AUDv1',
      'AUDv5'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Ventral auditory area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHm',
  inputType: 'radio',
  nextQuestionMap: [
      'PVHpm',
      'PVHmm',
      'PVHam'
  ],
  options: [
      'PVHpm',
      'PVHmm',
      'PVHam'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Paraventricular hypothalamic nucleus, magnocellular division'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ECU',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'External cuneate nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBl5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, lateral part, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTdm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, anterior division, dorsomedial nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACVII',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Accessory facial motor nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NLOT1-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the lateral olfactory tract, layers 1-3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA2so',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA2, stratum oriens'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-n1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, nose, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOs2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Secondary motor area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDp6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary auditory area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDv6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral auditory area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISl',
  inputType: 'radio',
  nextQuestionMap: [
      'VISl6a',
      'VISl5',
      'VISl1',
      'VISl4',
      'VISl6b',
      'VISl2-3'
  ],
  options: [
      'VISl6a',
      'VISl5',
      'VISl1',
      'VISl4',
      'VISl6b',
      'VISl2-3'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Lateral visual area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COPY',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Copula pyramidis'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOs6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Secondary motor area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACAv1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, ventral part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPVI',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Spinal nucleus of the trigeminal, interpolar part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CENT3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lobule III'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TRS',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Triangular nucleus of septum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHap',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paraventricular hypothalamic nucleus, parvicellular division, anterior parvicellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'B',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Barringtons nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MEApd-b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial amygdalar nucleus, posterodorsal part, sublayer b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AONm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior olfactory nucleus, medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NTB',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the trapezoid body'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACA6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TR1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Postpiriform transition area, layers 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPagl2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, lateral agranular part, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NLLh',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the lateral lemniscus, horizontal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'EMTmv5-6',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, ventral zone, layer 5/6'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl5-6',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, lateral part, layer 5/6'
},{
  category: CATEGORIES.PERSONAL,
  id: 'POST',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Postsubiculum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPd2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, dorsal part, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CBX',
  inputType: 'radio',
  nextQuestionMap: [
      'VERM',
      'HEM',
      'CBXpu',
      'CBXgr'
  ],
  options: [
      'VERM',
      'HEM',
      'CBXpu',
      'CBXgr'
  ],
  scoreMap: [
      1,
      1,
      0,
      0
  ],
  text: 'Cerebellar cortex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SLD',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Sublaterodorsal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISp',
  inputType: 'radio',
  nextQuestionMap: [
      'VISp5',
      'VISp2-3',
      'VISp6b',
      'VISp4',
      'VISp1',
      'VISp6a'
  ],
  options: [
      'VISp5',
      'VISp2-3',
      'VISp6b',
      'VISp4',
      'VISp1',
      'VISp6a'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Primary visual area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Reticular nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DGlb',
  inputType: 'radio',
  nextQuestionMap: [
      'DGlb-mo',
      'DGlb-po',
      'DGlb-sg'
  ],
  options: [
      'DGlb-mo',
      'DGlb-po',
      'DGlb-sg'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Dentate gyrus lateral blade'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TEa2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Temporal association areas, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVpo',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Periventricular hypothalamic nucleus, preoptic part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GR',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Gracile nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PR',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Perireunensis nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LGd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal part of the lateral geniculate complex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PAL',
  inputType: 'radio',
  nextQuestionMap: [
      '',
      'PALm',
      'PALc',
      'PALd'
  ],
  options: [
      '',
      'PALm',
      'PALc',
      'PALd'
  ],
  scoreMap: [
      1,
      1,
      1,
      1
  ],
  text: 'Pallidum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DGmb-sg',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dentate gyrus medial blade, granule cell layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ll',
  inputType: 'radio',
  nextQuestionMap: [
      'SSp-ll6b',
      'SSp-ll5',
      'SSp-ll2-3',
      'SSp-ll1',
      'SSp-ll6a',
      'SSp-ll4'
  ],
  options: [
      'SSp-ll6b',
      'SSp-ll5',
      'SSp-ll2-3',
      'SSp-ll1',
      'SSp-ll6a',
      'SSp-ll4'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Primary somatosensory area, lower limb'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPagl6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, lateral agranular part, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDp2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary auditory area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpl5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterolateral visual area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TEa1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Temporal association areas, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOp',
  inputType: 'radio',
  nextQuestionMap: [
      'MOp5',
      'MOp2-3',
      'MOp1',
      'MOp6b',
      'MOp6a'
  ],
  options: [
      'MOp5',
      'MOp2-3',
      'MOp1',
      'MOp6b',
      'MOp6a'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Primary motor area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBl1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, lateral part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISC2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Visceral area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'P-sen',
  inputType: 'radio',
  nextQuestionMap: [
      'SOC',
      'NLL',
      'PB',
      'PSV'
  ],
  options: [
      'SOC',
      'NLL',
      'PB',
      'PSV'
  ],
  scoreMap: [
      1,
      1,
      1,
      0
  ],
  text: 'Pons, sensory related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PTLp',
  inputType: 'radio',
  nextQuestionMap: [
      'PTLp1',
      'PTLp6b',
      'PTLp2-3',
      'PTLp4',
      'PTLp6a',
      'PTLp5'
  ],
  options: [
      'PTLp1',
      'PTLp6b',
      'PTLp2-3',
      'PTLp4',
      'PTLp6a',
      'PTLp5'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Posterior parietal association areas'
},{
  category: CATEGORIES.PERSONAL,
  id: 'FS',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Fundus of striatum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSs6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Supplemental somatosensory area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'HB',
  inputType: 'radio',
  nextQuestionMap: [
      'MY',
      'P'
  ],
  options: [
      'MY',
      'P'
  ],
  scoreMap: [
      1,
      1
  ],
  text: 'Hindbrain'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ILA6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Infralimbic area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NPC',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the posterior commissure'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDd1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal auditory area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACAd',
  inputType: 'radio',
  nextQuestionMap: [
      'ACAd1',
      'ACAd6a',
      'ACAd5',
      'ACAd2-3',
      'ACAd6b'
  ],
  options: [
      'ACAd1',
      'ACAd6a',
      'ACAd5',
      'ACAd2-3',
      'ACAd6b'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Anterior cingulate area, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BAC',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nucleus of the anterior commissure'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PTLp6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior parietal association areas, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LSr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral septal nucleus, rostral (rostroventral) part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SI',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Substantia innominata'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDv4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral auditory area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LA',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral amygdalar nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AHN',
  inputType: 'radio',
  nextQuestionMap: [
      'AHNp',
      'AHNd',
      'AHNa',
      'AHNc'
  ],
  options: [
      'AHNp',
      'AHNd',
      'AHNa',
      'AHNc'
  ],
  scoreMap: [
      0,
      0,
      0,
      0
  ],
  text: 'Anterior hypothalamic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PFL',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paraflocculus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Inferior olivary complex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus raphe obscurus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPVC',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Spinal nucleus of the trigeminal, caudal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MRNp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Midbrain reticular nucleus, parvicellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCig-c',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior colliculus, motor related, intermediate gray layer, sublayer c'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PIR3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Piriform area, polymorph layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA3slm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA3, stratum lacunosum-moleculare'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PALm',
  inputType: 'radio',
  nextQuestionMap: [
      'TRS',
      'MSC'
  ],
  options: [
      'TRS',
      'MSC'
  ],
  scoreMap: [
      0,
      1
  ],
  text: 'Pallidum, medial region'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DMHa',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsomedial nucleus of the hypothalamus, anterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VCO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral cochlear nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VPMpc',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral posteromedial nucleus of the thalamus, parvicellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VI',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Abducens nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA3so',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA3, stratum oriens'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AVP',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anteroventral preoptic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'EV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Efferent vestibular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'FN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Fastigial nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISp2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary visual area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TTv1-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Taenia tecta, ventral part, layers 1-3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TTd3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Taenia tecta, dorsal part, layers 3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PARN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parvicellular reticular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AONpv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior olfactory nucleus, posteroventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTif',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, posterior division, interfascicular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DP5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal peduncular area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GU6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Gustatory areas, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PRNv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Pontine reticular nucleus, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBl',
  inputType: 'radio',
  nextQuestionMap: [
      'ORBl6a',
      'ORBl6b',
      'ORBl5',
      'ORBl1',
      'ORBl2-3'
  ],
  options: [
      'ORBl6a',
      'ORBl6b',
      'ORBl5',
      'ORBl1',
      'ORBl2-3'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Orbital area, lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'Isocortex',
  inputType: 'radio',
  nextQuestionMap: [
      'AUD',
      'RSP',
      'FRP',
      'ECT',
      'AI',
      'ACA',
      'MO',
      'PL',
      'GU',
      'VIS',
      'VISC',
      'PTLp',
      'PERI',
      'SS',
      'TEa',
      'ILA',
      'ORB'
  ],
  options: [
      'AUD',
      'RSP',
      'FRP',
      'ECT',
      'AI',
      'ACA',
      'MO',
      'PL',
      'GU',
      'VIS',
      'VISC',
      'PTLp',
      'PERI',
      'SS',
      'TEa',
      'ILA',
      'ORB'
  ],
  scoreMap: [
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1
  ],
  text: 'Isocortex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBvl6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, ventrolateral part, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NLOT',
  inputType: 'radio',
  nextQuestionMap: [
      'NLOT1',
      'NLOT1-3',
      'NLOT2',
      'NLOT3'
  ],
  options: [
      'NLOT1',
      'NLOT1-3',
      'NLOT2',
      'NLOT3'
  ],
  scoreMap: [
      0,
      0,
      0,
      0
  ],
  text: 'Nucleus of the lateral olfactory tract'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SH',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Septohippocampal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TTd1-4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Taenia tecta, dorsal part, layers 1-4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTm4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, dorsal zone, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPVOmdmv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Spinal nucleus of the trigeminal, oral part, middle dorsomedial part, ventral zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MBO',
  inputType: 'radio',
  nextQuestionMap: [
      'MM',
      'SUM',
      'TM',
      'LM'
  ],
  options: [
      'MM',
      'SUM',
      'TM',
      'LM'
  ],
  scoreMap: [
      1,
      1,
      1,
      0
  ],
  text: 'Mammillary body'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA2slm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA2, stratum lacunosum-moleculare'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PTLp2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior parietal association areas, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpl',
  inputType: 'radio',
  nextQuestionMap: [
      'VISpl6b',
      'VISpl1',
      'VISpl2-3',
      'VISpl4',
      'VISpl5',
      'VISpl6a'
  ],
  options: [
      'VISpl6b',
      'VISpl1',
      'VISpl2-3',
      'VISpl4',
      'VISpl5',
      'VISpl6a'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Posterolateral visual area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPagl5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, lateral agranular part, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PL5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Prelimbic area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTX6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cerebral cortex, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl2a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, lateral part, layer 2a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NLLd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the lateral lemniscus, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PAA1-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Piriform-amygdalar area, layers 1-3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ECT6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ectorhinal area/Layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PTLp4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior parietal association areas, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPv2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, ventral part, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DCN',
  inputType: 'radio',
  nextQuestionMap: [
      'CU',
      'GR'
  ],
  options: [
      'CU',
      'GR'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Dorsal column nuclei'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTmv',
  inputType: 'radio',
  nextQuestionMap: [
      'EMTmv1',
      'EMTmv2',
      'EMTmv5-6',
      'EMTmv3',
      'EMTmv4'
  ],
  options: [
      'EMTmv1',
      'EMTmv2',
      'EMTmv5-6',
      'EMTmv3',
      'EMTmv4'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Entorhinal area, medial part, ventral zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NLOT2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the lateral olfactory tract, pyramidal layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TR1-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Postpiriform transition area, layers 1-3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, anterior division, ventral nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IP',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Interposed nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AOBgl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Accessory olfactory bulb, glomerular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'y',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus y'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpm4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'posteromedial visual area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TTd4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Taenia tecta, dorsal part, layers 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Supraoptic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ECT5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ectorhinal area/Layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOs1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Secondary motor area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CENT',
  inputType: 'radio',
  nextQuestionMap: [
      'CENT2',
      'CENT3'
  ],
  options: [
      'CENT2',
      'CENT3'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Central lobule'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCzo',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior colliculus, zonal layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COApl2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, posterior part, lateral zone, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GU6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Gustatory areas, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTam',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, anterior division, anteromedial area]'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDd5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal auditory area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LRNm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral reticular nucleus, magnocellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'Mmme',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial mammillary nucleus, median part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TR',
  inputType: 'radio',
  nextQuestionMap: [
      'TR2',
      'TR1',
      'TR1-3',
      'TR3'
  ],
  options: [
      'TR2',
      'TR1',
      'TR1-3',
      'TR3'
  ],
  scoreMap: [
      0,
      0,
      0,
      0
  ],
  text: 'Postpiriform transition area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Supratrigeminal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ILA1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Infralimbic area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, lateral part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior vestibular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PBlc',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parabrachial nucleus, lateral division, central lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PCN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paracentral nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPv2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, ventral part, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, posterior division, dorsal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LIN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Linear nucleus of the medulla'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PERI',
  inputType: 'radio',
  nextQuestionMap: [
      'PERI1',
      'PERI5',
      'PERI6a',
      'PERI2-3',
      'PERI6b'
  ],
  options: [
      'PERI1',
      'PERI5',
      'PERI6a',
      'PERI2-3',
      'PERI6b'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Perirhinal area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBm',
  inputType: 'radio',
  nextQuestionMap: [
      'ORBm1',
      'ORBm5',
      'ORBm2',
      'ORBm6a',
      'ORBm2-3'
  ],
  options: [
      'ORBm1',
      'ORBm5',
      'ORBm2',
      'ORBm6a',
      'ORBm2-3'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Orbital area, medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ll6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, lower limb, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PRM',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paramedian lobule'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AVPV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anteroventral periventricular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCop',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Superior colliculus, optic layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ADP',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterodorsal preoptic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHd',
  inputType: 'radio',
  nextQuestionMap: [
      'PVHmpv',
      'PVHlp',
      'PVHdp',
      'PVHf'
  ],
  options: [
      'PVHmpv',
      'PVHlp',
      'PVHdp',
      'PVHf'
  ],
  scoreMap: [
      0,
      0,
      0,
      0
  ],
  text: 'Paraventricular hypothalamic nucleus, descending division'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpm2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'posteromedial visual area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IA',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Intercalated amygdalar nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISam6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anteromedial visual area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AIv',
  inputType: 'radio',
  nextQuestionMap: [
      'AIv6a',
      'AIv5',
      'AIv1',
      'AIv6b',
      'AIv2-3'
  ],
  options: [
      'AIv6a',
      'AIv5',
      'AIv1',
      'AIv6b',
      'AIv2-3'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Agranular insular area, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DCO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal cochlear nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCig',
  inputType: 'radio',
  nextQuestionMap: [
      'SCig-a',
      'SCig-b',
      'SCig-c'
  ],
  options: [
      'SCig-a',
      'SCig-b',
      'SCig-c'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Superior colliculus, motor related, intermediate gray layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MPNm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial preoptic nucleus, medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TH',
  inputType: 'radio',
  nextQuestionMap: [
      'DORpm',
      'DORsm'
  ],
  options: [
      'DORpm',
      'DORsm'
  ],
  scoreMap: [
      1,
      1
  ],
  text: 'Thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'EMTmv3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, ventral zone, layer 3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBl2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, lateral part, layer 2-3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOBgl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Main olfactory bulb, glomerular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ll4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, lower limb, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-m',
  inputType: 'radio',
  nextQuestionMap: [
      'SSp-m4',
      'SSp-m2-3',
      'SSp-m5',
      'SSp-m6a',
      'SSp-m1',
      'SSp-m6b'
  ],
  options: [
      'SSp-m4',
      'SSp-m2-3',
      'SSp-m5',
      'SSp-m6a',
      'SSp-m1',
      'SSp-m6b'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Primary somatosensory area, mouth'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PERI6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Perirhinal area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBm6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, medial part, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PRNr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Pontine reticular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MED',
  inputType: 'radio',
  nextQuestionMap: [
      'MD',
      'IMD',
      'SMT',
      'PR'
  ],
  options: [
      'MD',
      'IMD',
      'SMT',
      'PR'
  ],
  scoreMap: [
      1,
      0,
      0,
      0
  ],
  text: 'Medial group of the dorsal thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PHY',
  inputType: 'radio',
  nextQuestionMap: [
      'PRP',
      'NIS',
      'NR'
  ],
  options: [
      'PRP',
      'NIS',
      'NR'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Perihypoglossal nuclei'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CUL4-5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lobules IV-V'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISp6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary visual area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TM',
  inputType: 'radio',
  nextQuestionMap: [
      'TMd',
      'TMv'
  ],
  options: [
      'TMd',
      'TMv'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Tuberomammillary nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'EPd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Endopiriform nucleus, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IF',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Interfascicular nucleus raphe'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COApm1-2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, posterior part, medial zone, layer 1-2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ASO',
  inputType: 'radio',
  nextQuestionMap: [
      'NC'
  ],
  options: [
      'NC'
  ],
  scoreMap: [
      0
  ],
  text: 'Accessory supraoptic group'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LTN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral tegmental nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'z',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus z'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PBld',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parabrachial nucleus, lateral division, dorsal lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AMB',
  inputType: 'radio',
  nextQuestionMap: [
      'AMBd',
      'AMBv'
  ],
  options: [
      'AMBd',
      'AMBv'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Nucleus ambiguus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MEPO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Median preoptic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TMv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Tuberomammillary nucleus, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'GU1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Gustatory areas, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTse',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, posterior division, strial extension'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, lateral part, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTm1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, dorsal zone, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ILA2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Infralimbic area, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISl4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral visual area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACAv',
  inputType: 'radio',
  nextQuestionMap: [
      'ACAv6b',
      'ACAv5',
      'ACAv6a',
      'ACAv1',
      'ACAv2-3'
  ],
  options: [
      'ACAv6b',
      'ACAv5',
      'ACAv6a',
      'ACAv1',
      'ACAv2-3'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Anterior cingulate area, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SS',
  inputType: 'radio',
  nextQuestionMap: [
      'SSp',
      'SSs'
  ],
  options: [
      'SSp',
      'SSs'
  ],
  scoreMap: [
      1,
      1
  ],
  text: 'Somatosensory areas'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VIS2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Visual areas, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'P-mot',
  inputType: 'radio',
  nextQuestionMap: [
      'PCG',
      'TRN',
      'PG',
      'SSN',
      'V',
      'SG',
      'PRNc',
      'B',
      'PRNv',
      'SUT',
      'LTN',
      'DTN'
  ],
  options: [
      'PCG',
      'TRN',
      'PG',
      'SSN',
      'V',
      'SG',
      'PRNc',
      'B',
      'PRNv',
      'SUT',
      'LTN',
      'DTN'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Pons, motor related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-ul1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, upper limb, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ANcr2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Crus 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SubG',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subgeniculate nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MDRNv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medullary reticular nucleus, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'sAMY',
  inputType: 'radio',
  nextQuestionMap: [
      'BA',
      'MEA',
      'CEA',
      'AAA',
      'IA'
  ],
  options: [
      'BA',
      'MEA',
      'CEA',
      'AAA',
      'IA'
  ],
  scoreMap: [
      0,
      1,
      1,
      0,
      0
  ],
  text: 'Striatum-like amygdalar nuclei'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTmg',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Bed nuclei of the stria terminalis, anterior division, magnocellular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MSC',
  inputType: 'radio',
  nextQuestionMap: [
      'MS',
      'NDB'
  ],
  options: [
      'MS',
      'NDB'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Medial septal complex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISp4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary visual area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Periventricular hypothalamic nucleus, posterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpl6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterolateral visual area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, lateral part, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MRN',
  inputType: 'radio',
  nextQuestionMap: [
      'MRNmg',
      'MRNm',
      'MRNp'
  ],
  options: [
      'MRNmg',
      'MRNm',
      'MRNp'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Midbrain reticular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VP',
  inputType: 'radio',
  nextQuestionMap: [
      'VPL',
      'VPM',
      'VPLpc'
  ],
  options: [
      'VPL',
      'VPM',
      'VPLpc'
  ],
  scoreMap: [
      0,
      1,
      0
  ],
  text: 'Ventral posterior complex of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-bfd6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, barrel field, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PIN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Pineal body'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LPO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral preoptic area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AONe',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior olfactory nucleus, external part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTXsp6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Layer 6b, isocortex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ECO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Efferent vestibular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PBle',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parabrachial nucleus, lateral division, external lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACA5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUBd-m',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subiculum, dorsal part, molecular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COA',
  inputType: 'radio',
  nextQuestionMap: [
      'COAp',
      'COAa'
  ],
  options: [
      'COAp',
      'COAa'
  ],
  scoreMap: [
      1,
      1
  ],
  text: 'Cortical amygdalar area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CUN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cuneiform nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TEa',
  inputType: 'radio',
  nextQuestionMap: [
      'TEa6a',
      'TEa6b',
      'TEa4',
      'TEa5',
      'TEa2-3',
      'TEa1'
  ],
  options: [
      'TEa6a',
      'TEa6b',
      'TEa4',
      'TEa5',
      'TEa2-3',
      'TEa1'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Temporal association areas'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHf',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paraventricular hypothalamic nucleus, descending division, forniceal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IB',
  inputType: 'radio',
  nextQuestionMap: [
      'TH',
      'HY'
  ],
  options: [
      'TH',
      'HY'
  ],
  scoreMap: [
      1,
      1
  ],
  text: 'Interbrain'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PERI2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Perirhinal area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBm2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, medial part, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AIp6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, posterior part, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RPA',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus raphe pallidus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACAv2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, ventral part, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISpm5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'posteromedial visual area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DGcr-sg',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dentate gyrus crest, granule cell layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COApl3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, posterior part, lateral zone, layer 3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MA',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Magnocellular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LDT',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Laterodorsal tegmental nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AId6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, dorsal part, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IG',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Induseum griseum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PPYs',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parapyramidal nucleus, superficial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDv1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral auditory area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AONd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior olfactory nucleus, dorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTm5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, dorsal zone, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-m6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, mouth, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VPLpc',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral posterolateral nucleus of the thalamus, parvicellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PVHam',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Paraventricular hypothalamic nucleus, magnocellular division, anterior magnocellular part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORB2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IAM',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Interanteromedial nucleus of the thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RL',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Rostral linear nucleus raphe'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DORsm',
  inputType: 'radio',
  nextQuestionMap: [
      'SPA',
      'VENT',
      'SPF',
      'GENd',
      'PP'
  ],
  options: [
      'SPA',
      'VENT',
      'SPF',
      'GENd',
      'PP'
  ],
  scoreMap: [
      0,
      1,
      1,
      1,
      0
  ],
  text: 'Thalamus, sensory-motor cortex related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'FRP1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Frontal pole, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPIV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Spinal vestibular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SIM',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Simple lobule'
},{
  category: CATEGORIES.PERSONAL,
  id: 'EMTmv4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, medial part, ventral zone, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'III',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Oculomotor nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COAa1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, anterior part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PERI6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Perirhinal area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORBvl1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, ventrolateral part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AHNa',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior hypothalamic nucleus, anterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PALc',
  inputType: 'radio',
  nextQuestionMap: [
      'BST',
      'BAC'
  ],
  options: [
      'BST',
      'BAC'
  ],
  scoreMap: [
      1,
      0
  ],
  text: 'Pallidum, caudal region'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PBme',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parabrachial nucleus, medial division, external medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LGv',
  inputType: 'radio',
  nextQuestionMap: [
      'LGvl',
      'LGvm'
  ],
  options: [
      'LGvl',
      'LGvm'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Ventral part of the lateral geniculate complex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BSTa',
  inputType: 'radio',
  nextQuestionMap: [
      'BSTju',
      'BSTfu',
      'BSTov',
      'BSTrh',
      'BSTal',
      'BSTdm',
      'BSTv',
      'BSTam',
      'BSTmg'
  ],
  options: [
      'BSTju',
      'BSTfu',
      'BSTov',
      'BSTrh',
      'BSTal',
      'BSTdm',
      'BSTv',
      'BSTam',
      'BSTmg'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Bed nuclei of the stria terminalis, anterior division'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACVI',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Accessory abducens nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'STR',
  inputType: 'radio',
  nextQuestionMap: [
      'STRd',
      'STRv',
      'LSX',
      'sAMY'
  ],
  options: [
      'STRd',
      'STRv',
      'LSX',
      'sAMY'
  ],
  scoreMap: [
      1,
      1,
      1,
      1
  ],
  text: 'Striatum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACAd2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, dorsal part, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-tr',
  inputType: 'radio',
  nextQuestionMap: [
      'SSp-tr2-3',
      'SSp-tr6a',
      'SSp-tr1',
      'SSp-tr4',
      'SSp-tr6b',
      'SSp-tr5'
  ],
  options: [
      'SSp-tr2-3',
      'SSp-tr6a',
      'SSp-tr1',
      'SSp-tr4',
      'SSp-tr6b',
      'SSp-tr5'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Primary somatosensory area, trunk'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPagl1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, lateral agranular part, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA3sp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA3, pyramidal layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-bfd6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, barrel field, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOs',
  inputType: 'radio',
  nextQuestionMap: [
      'MOs5',
      'MOs6a',
      'MOs2-3',
      'MOs6b',
      'MOs1'
  ],
  options: [
      'MOs5',
      'MOs6a',
      'MOs2-3',
      'MOs6b',
      'MOs1'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Secondary motor area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDpo6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior auditory area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-bfd4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, barrel field, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'RSPd6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Retrosplenial area, dorsal part, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDpo1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior auditory area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTX4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cerebral cortex, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COApl1-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, posterior part, lateral zone, layer 1-3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MY-sat',
  inputType: 'radio',
  nextQuestionMap: [
      'RM',
      'RO',
      'RPA'
  ],
  options: [
      'RM',
      'RO',
      'RPA'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Medulla, behavioral state related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSP-ul6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, upper limb, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LAV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral vestibular nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BLAa',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Basolateral amygdalar nucleus, anterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISl6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral visual area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AP',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Area postrema'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDp6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary auditory area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CNlam',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Granular lamina of the cochlear nuclei'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DMX',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal motor nucleus of the vagus nerve'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISC6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Visceral area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PSTN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parasubthalamic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISp1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary visual area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PALd',
  inputType: 'radio',
  nextQuestionMap: [
      'GPe',
      'GPi'
  ],
  options: [
      'GPe',
      'GPi'
  ],
  scoreMap: [
      0,
      0
  ],
  text: 'Pallidum, dorsal region'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOBopl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Main olfactory bulb, outer plexiform layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CA2sp',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Field CA2, pyramidal layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IC',
  inputType: 'radio',
  nextQuestionMap: [
      'ICc',
      'ICe',
      'ICd'
  ],
  options: [
      'ICc',
      'ICe',
      'ICd'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Inferior colliculus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISal6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterolateral visual area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PBmv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parabrachial nucleus, medial division, ventral medial part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-tr6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, trunk, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOBipl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Main olfactory bulb, inner plexiform layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl4-5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, lateral part, layer 4/5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VMH',
  inputType: 'radio',
  nextQuestionMap: [
      'VMHdm',
      'VMHvl',
      'VMHc',
      'VMHa'
  ],
  options: [
      'VMHdm',
      'VMHvl',
      'VMHc',
      'VMHa'
  ],
  scoreMap: [
      0,
      0,
      0,
      0
  ],
  text: 'Ventromedial hypothalamic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MO2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Somatomotor areas, Layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'INV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Interstitial nucleus of the vestibular nerve'
},{
  category: CATEGORIES.PERSONAL,
  id: 'Brain',
  inputType: 'radio',
  nextQuestionMap: [
      'CH',
      'CB',
      'BS'
  ],
  options: [
      'CH',
      'CB',
      'BS'
  ],
  scoreMap: [
      1,
      1,
      1
  ],
  text: 'Basic cell groups and regions'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISl2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral visual area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LH',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral habenula'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PMd',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal premammillary nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MEApd',
  inputType: 'radio',
  nextQuestionMap: [
      'MEApd-c',
      'MEApd-a',
      'MEApd-b'
  ],
  options: [
      'MEApd-c',
      'MEApd-a',
      'MEApd-b'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Medial amygdalar nucleus, posterodorsal part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDv5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral auditory area, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'IV',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Trochlear nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LC',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Locus ceruleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LGvl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral part of the lateral geniculate complex, lateral zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACA6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ILM',
  inputType: 'radio',
  nextQuestionMap: [
      'PF',
      'CM',
      'CL',
      'RH',
      'PCN'
  ],
  options: [
      'PF',
      'CM',
      'CL',
      'RH',
      'PCN'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Intralaminar nuclei of the dorsal thalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISp6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary visual area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ENTl2',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Entorhinal area, lateral part, layer 2'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LM',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral mammillary nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AUDd2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal auditory area, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TTv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Taenia tecta, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'P',
  inputType: 'radio',
  nextQuestionMap: [
      'P-sat',
      'P-sen',
      'P-mot'
  ],
  options: [
      'P-sat',
      'P-sen',
      'P-mot'
  ],
  scoreMap: [
      1,
      1,
      1
  ],
  text: 'Pons'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COApl1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cortical amygdalar area, posterior part, lateral zone, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'COAa',
  inputType: 'radio',
  nextQuestionMap: [
      'COAa2',
      'COAa3',
      'COAa1'
  ],
  options: [
      'COAa2',
      'COAa3',
      'COAa1'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Cortical amygdalar area, anterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MGv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Medial geniculate complex, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ILA',
  inputType: 'radio',
  nextQuestionMap: [
      'ILA2-3',
      'ILA6a',
      'ILA5',
      'ILA6b',
      'ILA1',
      'ILA2'
  ],
  options: [
      'ILA2-3',
      'ILA6a',
      'ILA5',
      'ILA6b',
      'ILA1',
      'ILA2'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Infralimbic area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORB',
  inputType: 'radio',
  nextQuestionMap: [
      'ORBvl',
      'ORB5',
      'ORB1',
      'ORB6b',
      'ORBv',
      'ORBl',
      'ORBm',
      'ORB2-3',
      'ORB6a'
  ],
  options: [
      'ORBvl',
      'ORB5',
      'ORB1',
      'ORB6b',
      'ORBv',
      'ORBl',
      'ORBm',
      'ORB2-3',
      'ORB6a'
  ],
  scoreMap: [
      1,
      0,
      0,
      0,
      0,
      1,
      1,
      0,
      0
  ],
  text: 'Orbital area'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUBd-sr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subiculum, dorsal part, stratum radiatum'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ACAd6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior cingulate area, dorsal part, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SFO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Subfornical organ'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DTN',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dorsal tegmental nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CUL4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lobule IV'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSP-n6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, nose, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DEC',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Declive (VI)'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PTLp6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior parietal association areas, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VLPO',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventrolateral preoptic nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DGcr-po',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dentate gyrus crest, polymorph layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MBmot',
  inputType: 'radio',
  nextQuestionMap: [
      'PAG',
      'LT',
      'AT',
      'EW',
      'MT',
      'SNr',
      'RN',
      'VTN',
      'PRT',
      'VTA',
      'DT',
      'RR',
      'SNI',
      'MRN',
      'CUN',
      'III',
      'IV',
      'SCm'
  ],
  options: [
      'PAG',
      'LT',
      'AT',
      'EW',
      'MT',
      'SNr',
      'RN',
      'VTN',
      'PRT',
      'VTA',
      'DT',
      'RR',
      'SNI',
      'MRN',
      'CUN',
      'III',
      'IV',
      'SCm'
  ],
  scoreMap: [
      1,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      1,
      0,
      0,
      0,
      0,
      1,
      0,
      0,
      0,
      1
  ],
  text: 'Midbrain, motor related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AHNc',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterior hypothalamic nucleus, central part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ORB6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Orbital area, layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PIR1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Piriform area, molecular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'MOp6a',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary motor area, Layer 6a'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-n',
  inputType: 'radio',
  nextQuestionMap: [
      'SSp-n6b',
      'SSp-n4',
      'SSp-n2-3',
      'SSp-n5',
      'SSp-n1',
      'SSP-n6a'
  ],
  options: [
      'SSp-n6b',
      'SSp-n4',
      'SSp-n2-3',
      'SSp-n5',
      'SSp-n1',
      'SSP-n6a'
  ],
  scoreMap: [
      0,
      0,
      0,
      0,
      0,
      0
  ],
  text: 'Primary somatosensory area, nose'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NTSl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the solitary tract, lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CBXgr',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Cerebellar cortex, granular layer'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSs6b',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Supplemental somatosensory area, layer 6b'
},{
  category: CATEGORIES.PERSONAL,
  id: 'AIv2-3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Agranular insular area, ventral part, layer 2/3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'A13',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Dopaminergic A13 group'
},{
  category: CATEGORIES.PERSONAL,
  id: 'TR3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Postpiriform transition area, layers 3'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LS',
  inputType: 'radio',
  nextQuestionMap: [
      'LSc',
      'LSr',
      'LSv'
  ],
  options: [
      'LSc',
      'LSr',
      'LSv'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Lateral septal nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSp-tr5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Primary somatosensory area, trunk, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VII',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Facial motor nucleus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'HY',
  inputType: 'radio',
  nextQuestionMap: [
      'PVZ',
      'LZ',
      'MEZ',
      'PVR'
  ],
  options: [
      'PVZ',
      'LZ',
      'MEZ',
      'PVR'
  ],
  scoreMap: [
      1,
      1,
      1,
      1
  ],
  text: 'Hypothalamus'
},{
  category: CATEGORIES.PERSONAL,
  id: 'BMAa',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Basomedial amygdalar nucleus, anterior part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'VISal1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Anterolateral visual area, layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'ECT1',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ectorhinal area/Layer 1'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SUBv',
  inputType: 'radio',
  nextQuestionMap: [
      'SUBv-m',
      'SUBv-sr',
      'SUBv-sp'
  ],
  options: [
      'SUBv-m',
      'SUBv-sr',
      'SUBv-sp'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Subiculum, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LGvm',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Ventral part of the lateral geniculate complex, medial zone'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SNc',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Substantia nigra, compact part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'DGmb',
  inputType: 'radio',
  nextQuestionMap: [
      'DGmb-mo',
      'DGmb-po',
      'DGmb-sg'
  ],
  options: [
      'DGmb-mo',
      'DGmb-po',
      'DGmb-sg'
  ],
  scoreMap: [
      0,
      0,
      0
  ],
  text: 'Dentate gyrus medial blade'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PBlv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Parabrachial nucleus, lateral division, ventral lateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'CTX',
  inputType: 'radio',
  nextQuestionMap: [
      'CTX5',
      'CTX6',
      'CTX1',
      'CTXsp',
      'CTX2',
      'CTX3',
      'CTXpl',
      'CTX1-6b',
      'CTX2-3',
      'CTX6a',
      'CTX4'
  ],
  options: [
      'CTX5',
      'CTX6',
      'CTX1',
      'CTXsp',
      'CTX2',
      'CTX3',
      'CTXpl',
      'CTX1-6b',
      'CTX2-3',
      'CTX6a',
      'CTX4'
  ],
  scoreMap: [
      0,
      0,
      0,
      1,
      0,
      0,
      1,
      0,
      0,
      0,
      0
  ],
  text: 'Cerebral cortex'
},{
  category: CATEGORIES.PERSONAL,
  id: 'LSv',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Lateral septal nucleus, ventral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SSs4',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Supplemental somatosensory area, layer 4'
},{
  category: CATEGORIES.PERSONAL,
  id: 'PTLp5',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Posterior parietal association areas, layer 5'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SCm',
  inputType: 'radio',
  nextQuestionMap: [
      'SCiw',
      'SCdg',
      'SCdw',
      'SCig'
  ],
  options: [
      'SCiw',
      'SCdg',
      'SCdw',
      'SCig'
  ],
  scoreMap: [
      0,
      0,
      0,
      1
  ],
  text: 'Superior colliculus, motor related'
},{
  category: CATEGORIES.PERSONAL,
  id: 'SPVOvl',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Spinal nucleus of the trigeminal, oral part, ventrolateral part'
},{
  category: CATEGORIES.PERSONAL,
  id: 'NLOT3',
  inputType: 'radio',
  nextQuestionMap :['ARBO'],
  options: ['ARBO'],
  scoreMap: [],
  text: 'Nucleus of the lateral olfactory tract, layer 3'
},];