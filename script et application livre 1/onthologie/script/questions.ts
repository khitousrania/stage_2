import {
	Guard 
	/*ScoreCondition
	Disjunction
	RadioAnswerCondition
	Conjunction
	BoolCondition,*/
	} from './guard';
	/*import { PANDEMIC_TRACKING_IS_ENABLED } from './custom';*/
export type Question = {
	id: string;
	category: string;
	comment?: string;
	text: string;
	inputType: 'radio' | 'date' | 'checkbox' | 'postal';
	options?: string[] | CheckboxOption[];
	nextQuestionMap?: string | string[];
	scoreMap?: number[];
	guard?: Guard;
 };
export type CheckboxOption = {
	label: string;
	id: string;
};
export const CATEGORIES = {
	PERSONAL: 'personalInfo',
	// CONTACT: 'contact',
	SYMPTOMS: 'symptoms',
	// RESPIRATORY_SYMPTOMS: 'respiratorySymptoms',
	// ILLNESS: 'illnesses',
	// MEDICATION: 'medication',
};
export const NO_XML = 'X';
export const QUESTION = {
	POSTAL_CODE: 'V1',
	AGE: 'P0',
	 ABOVE_65: 'P1',
	 LIVING_SITUATION: 'P2',
	 CARING: 'P3',
 	WORKSPACE: 'P4',
 	CONTACT_DATE: 'CZ',
	OUT_OF_BREATH: 'SB',
	SYMPTOM_DATE: 'SZ',
	 DATA_DONATION: `${NO_XML}1`,
};
export const XML_ORDER = ['V', 'P', 'C', 'S', 'D', 'M'];
export const QUESTIONS: Question[] = [
{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000057",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "fibroblast"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000525",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "syncytiotrophoblast cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000205",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "protuberance"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000066",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000216",
        "http://purl.obolibrary.org/obo/AEO_0000119f",
        "http://purl.obolibrary.org/obo/AEO_0000206",
        "http://purl.obolibrary.org/obo/CARO_0000071",
        "http://purl.obolibrary.org/obo/CARO_0000073",
        "http://purl.obolibrary.org/obo/CARO_0000069",
        "http://purl.obolibrary.org/obo/AEO_0000115",
        "http://purl.obolibrary.org/obo/AEO_0000114"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000216",
        "http://purl.obolibrary.org/obo/AEO_0000119f",
        "http://purl.obolibrary.org/obo/AEO_0000206",
        "http://purl.obolibrary.org/obo/CARO_0000071",
        "http://purl.obolibrary.org/obo/CARO_0000073",
        "http://purl.obolibrary.org/obo/CARO_0000069",
        "http://purl.obolibrary.org/obo/AEO_0000115",
        "http://purl.obolibrary.org/obo/AEO_0000114"
    ],
    "scoremap": [
        0,
        1,
        0,
        0,
        1,
        1,
        1,
        1
    ],
    "text": "epithelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000327",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000667"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000667"
    ],
    "scoremap": [
        1
    ],
    "text": "extracellular matrix secreting cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000065",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "basal lamina"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000048",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000028",
        "http://purl.obolibrary.org/obo/CARO_0000027"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000028",
        "http://purl.obolibrary.org/obo/CARO_0000027"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "gonochoristic organism"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000215",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "open cavity"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000204",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "pit"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000015",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "male germ cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000100",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "chitin-based structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000123",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000135",
        "http://purl.obolibrary.org/obo/AEO_0001012",
        "http://purl.obolibrary.org/obo/AEO_0001011",
        "http://purl.obolibrary.org/obo/AEO_0000137",
        "http://purl.obolibrary.org/obo/AEO_0000138"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000135",
        "http://purl.obolibrary.org/obo/AEO_0001012",
        "http://purl.obolibrary.org/obo/AEO_0001011",
        "http://purl.obolibrary.org/obo/AEO_0000137",
        "http://purl.obolibrary.org/obo/AEO_0000138"
    ],
    "scoremap": [
        1,
        1,
        1,
        1,
        0
    ],
    "text": "neural tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000181",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "synovial membrane"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000188",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "skeletal muscle cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000216",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "epithelial cord"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000119f",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0001015"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0001015"
    ],
    "scoremap": [
        0
    ],
    "text": "epithelial vesicle"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000067",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "simple cuboidal epithelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000170",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "anlage"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000092",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "aponeurosis"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000081",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000092",
        "http://purl.obolibrary.org/obo/AEO_0000091",
        "http://purl.obolibrary.org/obo/AEO_0000185",
        "http://purl.obolibrary.org/obo/AEO_0000087",
        "http://purl.obolibrary.org/obo/AEO_0000090",
        "http://purl.obolibrary.org/obo/AEO_0000178"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000092",
        "http://purl.obolibrary.org/obo/AEO_0000091",
        "http://purl.obolibrary.org/obo/AEO_0000185",
        "http://purl.obolibrary.org/obo/AEO_0000087",
        "http://purl.obolibrary.org/obo/AEO_0000090",
        "http://purl.obolibrary.org/obo/AEO_0000178"
    ],
    "scoremap": [
        0,
        0,
        0,
        1,
        0,
        0
    ],
    "text": "matrix-based tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000126",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000128"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000128"
    ],
    "scoremap": [
        1
    ],
    "text": "macroglial cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000135",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0001001",
        "http://purl.obolibrary.org/obo/AEO_0001000"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0001001",
        "http://purl.obolibrary.org/obo/AEO_0001000"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "ganglion"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000146",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "dense mesenchyme tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000111",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "mesothelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000074",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "solid compound organ"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000080",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "foramen"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000091",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "tendon"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000127",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "astrocyte"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000145",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000146",
        "http://purl.obolibrary.org/obo/AEO_0000152",
        "http://purl.obolibrary.org/obo/AEO_0000151"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000146",
        "http://purl.obolibrary.org/obo/AEO_0000152",
        "http://purl.obolibrary.org/obo/AEO_0000151"
    ],
    "scoremap": [
        0,
        0,
        0
    ],
    "text": "mesenchymal tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000079",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "lumen of epithelial sac"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000192",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "anatomical surface feature"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000670",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "primordial germ cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000243",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000126",
        "http://purl.obolibrary.org/obo/CL_0000129",
        "http://purl.obolibrary.org/obo/CL_0002573"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000126",
        "http://purl.obolibrary.org/obo/CL_0000129",
        "http://purl.obolibrary.org/obo/CL_0002573"
    ],
    "scoremap": [
        1,
        0,
        0
    ],
    "text": "glial cell (sensu Vertebrata)"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001009",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "proliferating neuroepithelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000179",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "joint-associated cartilage"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000078",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "lumen of tube"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000667",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000138"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000138"
    ],
    "scoremap": [
        0
    ],
    "text": "collagen secreting cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000080",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "circulating cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000185",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "exoskeletal tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000196",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "larval anatomical structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000211",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000393"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000393"
    ],
    "scoremap": [
        1
    ],
    "text": "electrically active cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000206",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "migrating epithelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000217",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "migrating developing tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000186",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "body cavity"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000011",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000168"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000168"
    ],
    "scoremap": [
        0
    ],
    "text": "anatomical system"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000445",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "apoptosis fated cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000139",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "nerve fiber tract"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000039",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000670",
        "http://purl.obolibrary.org/obo/CL_0000586"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000670",
        "http://purl.obolibrary.org/obo/CL_0000586"
    ],
    "scoremap": [
        0,
        1
    ],
    "text": "germ line cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001001",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "autonomic ganglion"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001012",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0001013",
        "http://purl.obolibrary.org/obo/AEO_0000136"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0001013",
        "http://purl.obolibrary.org/obo/AEO_0000136"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "neuronal grey matter"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000737",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000444",
        "http://purl.obolibrary.org/obo/CL_0000746"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000444",
        "http://purl.obolibrary.org/obo/CL_0000746"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "striated muscle cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001000",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "sensory ganglion"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0002319",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000095"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000095"
    ],
    "scoremap": [
        1
    ],
    "text": "neural cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001011",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000139"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000139"
    ],
    "scoremap": [
        0
    ],
    "text": "neuronal white matter"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000082",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000083",
        "http://purl.obolibrary.org/obo/AEO_0000085"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000083",
        "http://purl.obolibrary.org/obo/AEO_0000085"
    ],
    "scoremap": [
        1,
        0
    ],
    "text": "bone"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000093",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "non-connected functional system"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000013",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "single-cell tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000125",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000243"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000243"
    ],
    "scoremap": [
        1
    ],
    "text": "glial cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001002",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "capillary bed"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001013",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "neuronal column"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000055",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000031",
        "http://purl.obolibrary.org/obo/CL_0000468"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000031",
        "http://purl.obolibrary.org/obo/CL_0000468"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "non-terminally differentiated cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000153",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "mesenchymal tissue with stem cells"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000106",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "head"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000012",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000048",
        "http://purl.obolibrary.org/obo/CARO_0000063",
        "http://purl.obolibrary.org/obo/AEO_0000169",
        "http://purl.obolibrary.org/obo/AEO_0000194",
        "http://purl.obolibrary.org/obo/AEO_0000126",
        "http://purl.obolibrary.org/obo/CARO_0000029"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000048",
        "http://purl.obolibrary.org/obo/CARO_0000063",
        "http://purl.obolibrary.org/obo/AEO_0000169",
        "http://purl.obolibrary.org/obo/AEO_0000194",
        "http://purl.obolibrary.org/obo/AEO_0000126",
        "http://purl.obolibrary.org/obo/CARO_0000029"
    ],
    "scoremap": [
        1,
        0,
        0,
        0,
        0,
        1
    ],
    "text": "multi-cellular organism"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000187",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000153",
        "http://purl.obolibrary.org/obo/AEO_0000189"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000153",
        "http://purl.obolibrary.org/obo/AEO_0000189"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "multi-tissue structure with stem cells"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000076",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000071"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000071"
    ],
    "scoremap": [
        0
    ],
    "text": "squamous epithelial cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000152",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "migrating mesenchyme population"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000174",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000176",
        "http://purl.obolibrary.org/obo/AEO_0000175",
        "http://purl.obolibrary.org/obo/AEO_0000177"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000176",
        "http://purl.obolibrary.org/obo/AEO_0000175",
        "http://purl.obolibrary.org/obo/AEO_0000177"
    ],
    "scoremap": [
        0,
        0,
        0
    ],
    "text": "gender-specific anatomical structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000444",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "obliquely striated muscle cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000031",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "neuroblast"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000003",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000012",
        "http://purl.obolibrary.org/obo/AEO_0000174",
        "http://purl.obolibrary.org/obo/CARO_0000040",
        "http://purl.obolibrary.org/obo/CARO_0000013",
        "http://purl.obolibrary.org/obo/CARO_0000054",
        "http://purl.obolibrary.org/obo/CARO_0000055",
        "http://purl.obolibrary.org/obo/AEO_0000125",
        "http://purl.obolibrary.org/obo/CARO_0000032",
        "http://purl.obolibrary.org/obo/CARO_0000024",
        "http://purl.obolibrary.org/obo/CARO_0000014",
        "http://purl.obolibrary.org/obo/CARO_0000042",
        "http://purl.obolibrary.org/obo/AEO_0000200",
        "http://purl.obolibrary.org/obo/CARO_0000043"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000012",
        "http://purl.obolibrary.org/obo/AEO_0000174",
        "http://purl.obolibrary.org/obo/CARO_0000040",
        "http://purl.obolibrary.org/obo/CARO_0000013",
        "http://purl.obolibrary.org/obo/CARO_0000054",
        "http://purl.obolibrary.org/obo/CARO_0000055",
        "http://purl.obolibrary.org/obo/AEO_0000125",
        "http://purl.obolibrary.org/obo/CARO_0000032",
        "http://purl.obolibrary.org/obo/CARO_0000024",
        "http://purl.obolibrary.org/obo/CARO_0000014",
        "http://purl.obolibrary.org/obo/CARO_0000042",
        "http://purl.obolibrary.org/obo/AEO_0000200",
        "http://purl.obolibrary.org/obo/CARO_0000043"
    ],
    "scoremap": [
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        0,
        0,
        0,
        1
    ],
    "text": "anatomical structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000129",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "functioning and developing structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000118",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0001002",
        "http://purl.obolibrary.org/obo/AEO_0000219"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0001002",
        "http://purl.obolibrary.org/obo/AEO_0000219"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "epithelial plexus"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000154",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "organism surfacef"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000176",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "female anatomical structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000141",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000143"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000143"
    ],
    "scoremap": [
        0
    ],
    "text": "smooth muscle tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000040",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000065",
        "http://purl.obolibrary.org/obo/AEO_0000100",
        "http://purl.obolibrary.org/obo/AEO_0000099",
        "http://purl.obolibrary.org/obo/AEO_0000188",
        "http://purl.obolibrary.org/obo/AEO_0001003"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000065",
        "http://purl.obolibrary.org/obo/AEO_0000100",
        "http://purl.obolibrary.org/obo/AEO_0000099",
        "http://purl.obolibrary.org/obo/AEO_0000188",
        "http://purl.obolibrary.org/obo/AEO_0001003"
    ],
    "scoremap": [
        0,
        0,
        0,
        0,
        0
    ],
    "text": "acellular anatomical structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000129",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "microglial cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000117",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "arborising epithelial duct system"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000175",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "male anatomical structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000087",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000179",
        "http://purl.obolibrary.org/obo/AEO_0000180"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000179",
        "http://purl.obolibrary.org/obo/AEO_0000180"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "cartilage"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000098",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "ductless gland"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000013",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000064",
        "http://purl.obolibrary.org/obo/CARO_0000077"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000064",
        "http://purl.obolibrary.org/obo/CARO_0000077"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000198",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "antenna"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000021",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "simple organ"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000004",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0001006",
        "http://purl.obolibrary.org/obo/AEO_0001005"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0001006",
        "http://purl.obolibrary.org/obo/AEO_0001005"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "non-tissue substance"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000097",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "ducted gland"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000086",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "sesamoid bone"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000062",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "osteoblast"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000003",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000012"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000012"
    ],
    "scoremap": [
        1
    ],
    "text": "cell in vivo"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000008",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "anatomical line"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000062",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "cell space"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000028",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "female organism"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000045",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000049",
        "http://purl.obolibrary.org/obo/CARO_0000050"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000049",
        "http://purl.obolibrary.org/obo/CARO_0000050"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "sequential hermaphroditic organism"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000586",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000021",
        "http://purl.obolibrary.org/obo/CL_0000015"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000021",
        "http://purl.obolibrary.org/obo/CL_0000015"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "germ cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000202",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "efferent nerve"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000527",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "efferent neuron"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000009",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "anatomical point"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000201",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "afferent nerve"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000212",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "sclerotome"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000131",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "developing structure with stem cells"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0002242",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000228"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000228"
    ],
    "scoremap": [
        1
    ],
    "text": "nucleate cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000213",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "bone condensation"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000054",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000011",
        "http://purl.obolibrary.org/obo/CARO_0000041",
        "http://purl.obolibrary.org/obo/AEO_0000210"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000011",
        "http://purl.obolibrary.org/obo/CARO_0000041",
        "http://purl.obolibrary.org/obo/AEO_0000210"
    ],
    "scoremap": [
        1,
        1,
        0
    ],
    "text": "anatomical group"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000071",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "atypical epithelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000746",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "cardiac muscle cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000066",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000076"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000076"
    ],
    "scoremap": [
        1
    ],
    "text": "epithelial cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000132",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "transitional anatomical structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000143",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "smooth muscle sphincter"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000027",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "male organism"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000107",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "autonomic neuron"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000108",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "neck"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000075",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "columnar/cuboidal epithelial cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000142",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "cardiac muscle tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001007",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0001008",
        "http://purl.obolibrary.org/obo/AEO_0000218"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0001008",
        "http://purl.obolibrary.org/obo/AEO_0000218"
    ],
    "scoremap": [
        1,
        0
    ],
    "text": "developing epithelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001018",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "myotome"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000099",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "keratin-based structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001006",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "excreta"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001017",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "dermatome"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000165",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "neuroendocrine cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001008",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0001009",
        "http://purl.obolibrary.org/obo/AEO_0001010"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0001009",
        "http://purl.obolibrary.org/obo/AEO_0001010"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "neuroepithelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001019",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "developing germ cell population"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000499",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "stromal cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000081",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "blood cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000049",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "protandrous hermaphroditic organism"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000058",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "chondroblast"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000187",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000188",
        "http://purl.obolibrary.org/obo/CL_0000737",
        "http://purl.obolibrary.org/obo/CL_0000192",
        "http://purl.obolibrary.org/obo/CL_0000196"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000188",
        "http://purl.obolibrary.org/obo/CL_0000737",
        "http://purl.obolibrary.org/obo/CL_0000192",
        "http://purl.obolibrary.org/obo/CL_0000196"
    ],
    "scoremap": [
        0,
        1,
        0,
        0
    ],
    "text": "muscle cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000151",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "secretory cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000055",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000082",
        "http://purl.obolibrary.org/obo/AEO_0000187",
        "http://purl.obolibrary.org/obo/CARO_0000021",
        "http://purl.obolibrary.org/obo/AEO_0000183",
        "http://purl.obolibrary.org/obo/AEO_0000095",
        "http://purl.obolibrary.org/obo/AEO_0000195",
        "http://purl.obolibrary.org/obo/AEO_0000199",
        "http://purl.obolibrary.org/obo/AEO_0000220",
        "http://purl.obolibrary.org/obo/AEO_0000096",
        "http://purl.obolibrary.org/obo/CARO_0000019"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000082",
        "http://purl.obolibrary.org/obo/AEO_0000187",
        "http://purl.obolibrary.org/obo/CARO_0000021",
        "http://purl.obolibrary.org/obo/AEO_0000183",
        "http://purl.obolibrary.org/obo/AEO_0000095",
        "http://purl.obolibrary.org/obo/AEO_0000195",
        "http://purl.obolibrary.org/obo/AEO_0000199",
        "http://purl.obolibrary.org/obo/AEO_0000220",
        "http://purl.obolibrary.org/obo/AEO_0000096",
        "http://purl.obolibrary.org/obo/CARO_0000019"
    ],
    "scoremap": [
        1,
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        1,
        0
    ],
    "text": "multi-tissue structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000072",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "cavitated compound organ"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000079",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "stratified epithelial cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000193",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000172",
        "http://purl.obolibrary.org/obo/AEO_0001004",
        "http://purl.obolibrary.org/obo/AEO_0000109"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000172",
        "http://purl.obolibrary.org/obo/AEO_0001004",
        "http://purl.obolibrary.org/obo/AEO_0000109"
    ],
    "scoremap": [
        0,
        0,
        0
    ],
    "text": "appendage"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000214",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "dermomyotome"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000203",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "syncytium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000046",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "synchronous hermaphroditic organism"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000063",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "asexual organism"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000169",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "embryo"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000540",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000527",
        "http://purl.obolibrary.org/obo/CL_0000107",
        "http://purl.obolibrary.org/obo/CL_0000099",
        "http://purl.obolibrary.org/obo/CL_0000526",
        "http://purl.obolibrary.org/obo/CL_0000029"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000527",
        "http://purl.obolibrary.org/obo/CL_0000107",
        "http://purl.obolibrary.org/obo/CL_0000099",
        "http://purl.obolibrary.org/obo/CL_0000526",
        "http://purl.obolibrary.org/obo/CL_0000029"
    ],
    "scoremap": [
        0,
        0,
        0,
        1,
        0
    ],
    "text": "neuron"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000125",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000170",
        "http://purl.obolibrary.org/obo/AEO_0000196",
        "http://purl.obolibrary.org/obo/AEO_0000217",
        "http://purl.obolibrary.org/obo/AEO_0000129",
        "http://purl.obolibrary.org/obo/AEO_0000131",
        "http://purl.obolibrary.org/obo/AEO_0000132",
        "http://purl.obolibrary.org/obo/AEO_0001019",
        "http://purl.obolibrary.org/obo/AEO_0000171",
        "http://purl.obolibrary.org/obo/AEO_0000127",
        "http://purl.obolibrary.org/obo/AEO_0001014",
        "http://purl.obolibrary.org/obo/AEO_0001016"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000170",
        "http://purl.obolibrary.org/obo/AEO_0000196",
        "http://purl.obolibrary.org/obo/AEO_0000217",
        "http://purl.obolibrary.org/obo/AEO_0000129",
        "http://purl.obolibrary.org/obo/AEO_0000131",
        "http://purl.obolibrary.org/obo/AEO_0000132",
        "http://purl.obolibrary.org/obo/AEO_0001019",
        "http://purl.obolibrary.org/obo/AEO_0000171",
        "http://purl.obolibrary.org/obo/AEO_0000127",
        "http://purl.obolibrary.org/obo/AEO_0001014",
        "http://purl.obolibrary.org/obo/AEO_0001016"
    ],
    "scoremap": [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ],
    "text": "developing anatomical structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000136",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "neuronal nucleus"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000147",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "cartilagenous condensation"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000183",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "synovial joint"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000194",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "conceptus"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000073",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000067",
        "http://purl.obolibrary.org/obo/CARO_0000068",
        "http://purl.obolibrary.org/obo/CARO_0000070"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000067",
        "http://purl.obolibrary.org/obo/CARO_0000068",
        "http://purl.obolibrary.org/obo/CARO_0000070"
    ],
    "scoremap": [
        0,
        0,
        1
    ],
    "text": "unilaminar epithelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0002573",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "Schwann cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000034",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "stem cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000134",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000335"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000335"
    ],
    "scoremap": [
        0
    ],
    "text": "mesenchymal cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000064",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "single cell organism"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000128",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000127"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000127"
    ],
    "scoremap": [
        0
    ],
    "text": "oligodendrocyte"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000493",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "fat tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000068",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "columnar epithelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000090",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "ligament"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001021",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "stem cell population"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001010",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "differentiating neuroepithelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000012",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000055",
        "http://purl.obolibrary.org/obo/CL_0000224",
        "http://purl.obolibrary.org/obo/CL_0000414",
        "http://purl.obolibrary.org/obo/CL_0000063",
        "http://purl.obolibrary.org/obo/CL_0000144"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000055",
        "http://purl.obolibrary.org/obo/CL_0000224",
        "http://purl.obolibrary.org/obo/CL_0000414",
        "http://purl.obolibrary.org/obo/CL_0000063",
        "http://purl.obolibrary.org/obo/CL_0000144"
    ],
    "scoremap": [
        1,
        1,
        0,
        1,
        1
    ],
    "text": "cell by class"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001020",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "reproductive cell population"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000208",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "artery"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000150",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "pre-cartilage condensation"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000095",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000125"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000125"
    ],
    "scoremap": [
        1
    ],
    "text": "neuron associated cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000207",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000208",
        "http://purl.obolibrary.org/obo/AEO_0000209"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000208",
        "http://purl.obolibrary.org/obo/AEO_0000209"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "blood vessel"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000218",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "developing epithelial placode"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000000",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000006",
        "http://purl.obolibrary.org/obo/CARO_0000007"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000006",
        "http://purl.obolibrary.org/obo/CARO_0000007"
    ],
    "scoremap": [
        1,
        1
    ],
    "text": "anatomical entity"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000069",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000181"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000181"
    ],
    "scoremap": [
        0
    ],
    "text": "multilaminar epithelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000103",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "body"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000171",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "primordium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000077",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "epithelial cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000126",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "larva"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000137",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000202",
        "http://purl.obolibrary.org/obo/AEO_0000201"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000202",
        "http://purl.obolibrary.org/obo/AEO_0000201"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "nerve"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000071",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "blood vessel endothelial cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000393",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000165",
        "http://purl.obolibrary.org/obo/CL_0000540"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000165",
        "http://purl.obolibrary.org/obo/CL_0000540"
    ],
    "scoremap": [
        0,
        1
    ],
    "text": "electrically responsive cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000010",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000205",
        "http://purl.obolibrary.org/obo/AEO_0000204",
        "http://purl.obolibrary.org/obo/AEO_0000080",
        "http://purl.obolibrary.org/obo/AEO_0000162",
        "http://purl.obolibrary.org/obo/AEO_0000161"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000205",
        "http://purl.obolibrary.org/obo/AEO_0000204",
        "http://purl.obolibrary.org/obo/AEO_0000080",
        "http://purl.obolibrary.org/obo/AEO_0000162",
        "http://purl.obolibrary.org/obo/AEO_0000161"
    ],
    "scoremap": [
        0,
        0,
        0,
        0,
        0
    ],
    "text": "anatomical surface"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000219",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "vascular plexus"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000151",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "loose mesenchyme tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000162",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "ridge"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000173",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000198"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000198"
    ],
    "scoremap": [
        0
    ],
    "text": "surface sense organ"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000115",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000117"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000117"
    ],
    "scoremap": [
        0
    ],
    "text": "epithelial sac"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000161",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "groove"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000172",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "limb"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000095",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "fluid-based anatomical entity"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000138",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "nerve plexus"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000149",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "pre-muscle condensation"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000127",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "apoptosing developing anatomical entity"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000114",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000118",
        "http://purl.obolibrary.org/obo/AEO_0000207"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000118",
        "http://purl.obolibrary.org/obo/AEO_0000207"
    ],
    "scoremap": [
        1,
        1
    ],
    "text": "epithelial tube"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000099",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "interneuron"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000083",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000086"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000086"
    ],
    "scoremap": [
        0
    ],
    "text": "endochondral bone"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000094",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000173"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000173"
    ],
    "scoremap": [
        1
    ],
    "text": "sense organ"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000988",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000081"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000081"
    ],
    "scoremap": [
        0
    ],
    "text": "hematopoietic cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000005",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000186",
        "http://purl.obolibrary.org/obo/CARO_0000062",
        "http://purl.obolibrary.org/obo/AEO_0000222",
        "http://purl.obolibrary.org/obo/AEO_0000221"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000186",
        "http://purl.obolibrary.org/obo/CARO_0000062",
        "http://purl.obolibrary.org/obo/AEO_0000222",
        "http://purl.obolibrary.org/obo/AEO_0000221"
    ],
    "scoremap": [
        0,
        0,
        1,
        1
    ],
    "text": "anatomical space"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000148",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000213",
        "http://purl.obolibrary.org/obo/AEO_0000147",
        "http://purl.obolibrary.org/obo/AEO_0000150",
        "http://purl.obolibrary.org/obo/AEO_0000149"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000213",
        "http://purl.obolibrary.org/obo/AEO_0000147",
        "http://purl.obolibrary.org/obo/AEO_0000150",
        "http://purl.obolibrary.org/obo/AEO_0000149"
    ],
    "scoremap": [
        0,
        0,
        0,
        0
    ],
    "text": "developing mesenchymal condensation"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000195",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "extraembryonic component"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000468",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "neuroglioblast"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000224",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0002242"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0002242"
    ],
    "scoremap": [
        1
    ],
    "text": "cell by nuclear number"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000032",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000106",
        "http://purl.obolibrary.org/obo/AEO_0000154",
        "http://purl.obolibrary.org/obo/AEO_0000108",
        "http://purl.obolibrary.org/obo/AEO_0000193",
        "http://purl.obolibrary.org/obo/AEO_0000103"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000106",
        "http://purl.obolibrary.org/obo/AEO_0000154",
        "http://purl.obolibrary.org/obo/AEO_0000108",
        "http://purl.obolibrary.org/obo/AEO_0000193",
        "http://purl.obolibrary.org/obo/AEO_0000103"
    ],
    "scoremap": [
        0,
        0,
        0,
        1,
        0
    ],
    "text": "organism subdivision"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000192",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "smooth muscle cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000006",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000003",
        "http://purl.obolibrary.org/obo/CARO_0000004"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000003",
        "http://purl.obolibrary.org/obo/CARO_0000004"
    ],
    "scoremap": [
        1,
        1
    ],
    "text": "material anatomical entity"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000414",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "cell by ploidy"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000188",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "acellular fluid anatomical entity"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000199",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "mucous membrane"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000222",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000079"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000079"
    ],
    "scoremap": [
        0
    ],
    "text": "enclosed anatomical space"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000220",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "tooth"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000209",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "vein"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000189",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "epithelial tissue with stem cells"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000041",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000093"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000093"
    ],
    "scoremap": [
        0
    ],
    "text": "anatomical cluster"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000024",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000074",
        "http://purl.obolibrary.org/obo/CARO_0000072",
        "http://purl.obolibrary.org/obo/AEO_0000094"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000074",
        "http://purl.obolibrary.org/obo/CARO_0000072",
        "http://purl.obolibrary.org/obo/AEO_0000094"
    ],
    "scoremap": [
        0,
        0,
        1
    ],
    "text": "compound organ"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000007",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000008",
        "http://purl.obolibrary.org/obo/CARO_0000009",
        "http://purl.obolibrary.org/obo/CARO_0000010",
        "http://purl.obolibrary.org/obo/CARO_0000005"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000008",
        "http://purl.obolibrary.org/obo/CARO_0000009",
        "http://purl.obolibrary.org/obo/CARO_0000010",
        "http://purl.obolibrary.org/obo/CARO_0000005"
    ],
    "scoremap": [
        0,
        0,
        1,
        1
    ],
    "text": "immaterial anatomical entity"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000101",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "sensory neuron"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000658",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "cuticle secreting cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0002371",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0002319",
        "http://purl.obolibrary.org/obo/CL_0000988",
        "http://purl.obolibrary.org/obo/CL_0002320"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0002319",
        "http://purl.obolibrary.org/obo/CL_0000988",
        "http://purl.obolibrary.org/obo/CL_0002320"
    ],
    "scoremap": [
        1,
        1,
        1
    ],
    "text": "somatic cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000210",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "row"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000221",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000215",
        "http://purl.obolibrary.org/obo/AEO_0000078"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000215",
        "http://purl.obolibrary.org/obo/AEO_0000078"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "open anatomical space"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000140",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000144"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000144"
    ],
    "scoremap": [
        0
    ],
    "text": "striated muscle tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000138",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "chondrocyte"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000014",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "cell component"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000526",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000101"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000101"
    ],
    "scoremap": [
        0
    ],
    "text": "afferent neuron"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0002372",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "myotube"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0002320",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000057",
        "http://purl.obolibrary.org/obo/CL_0000499",
        "http://purl.obolibrary.org/obo/CL_0000136"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000057",
        "http://purl.obolibrary.org/obo/CL_0000499",
        "http://purl.obolibrary.org/obo/CL_0000136"
    ],
    "scoremap": [
        0,
        0,
        0
    ],
    "text": "connective tissue cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001015",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "somite"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001004",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "fin"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000085",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "membrane bone"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000096",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000098",
        "http://purl.obolibrary.org/obo/AEO_0000097"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000098",
        "http://purl.obolibrary.org/obo/AEO_0000097"
    ],
    "scoremap": [
        0,
        0
    ],
    "text": "gland"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000063",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000066",
        "http://purl.obolibrary.org/obo/CL_0000134"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000066",
        "http://purl.obolibrary.org/obo/CL_0000134"
    ],
    "scoremap": [
        1,
        1
    ],
    "text": "cell by histology"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001003",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "acellular extracellular matrix"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001014",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "differentiating neuronal condensation"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000029",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000045",
        "http://purl.obolibrary.org/obo/CARO_0000046"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000045",
        "http://purl.obolibrary.org/obo/CARO_0000046"
    ],
    "scoremap": [
        1,
        0
    ],
    "text": "hermaphroditic organism"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001005",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "secretion"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0001016",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "developing mesenchymal structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000019",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "compound organ component"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000109",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "tail"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000042",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "extraembryonic structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000144",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000080",
        "http://purl.obolibrary.org/obo/CL_0000211",
        "http://purl.obolibrary.org/obo/CL_0000445",
        "http://purl.obolibrary.org/obo/CL_0000039",
        "http://purl.obolibrary.org/obo/CL_0000151",
        "http://purl.obolibrary.org/obo/CL_0000034",
        "http://purl.obolibrary.org/obo/CL_0002371"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000080",
        "http://purl.obolibrary.org/obo/CL_0000211",
        "http://purl.obolibrary.org/obo/CL_0000445",
        "http://purl.obolibrary.org/obo/CL_0000039",
        "http://purl.obolibrary.org/obo/CL_0000151",
        "http://purl.obolibrary.org/obo/CL_0000034",
        "http://purl.obolibrary.org/obo/CL_0002371"
    ],
    "scoremap": [
        0,
        1,
        0,
        1,
        0,
        0,
        1
    ],
    "text": "cell by function"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000177",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "hermaphrodite anatomical structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000050",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "protogynous hermaphroditic organism"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000200",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "regenerating anatomical structure"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000211",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "non-connected developing functional system"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000000",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000003"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000003"
    ],
    "scoremap": [
        1
    ],
    "text": "cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000168",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "skeletal system"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000144",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "striated muscle sphincter"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000122",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000141",
        "http://purl.obolibrary.org/obo/AEO_0000142",
        "http://purl.obolibrary.org/obo/AEO_0000140"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000141",
        "http://purl.obolibrary.org/obo/AEO_0000142",
        "http://purl.obolibrary.org/obo/AEO_0000140"
    ],
    "scoremap": [
        1,
        0,
        1
    ],
    "text": "muscle tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000180",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "non-joint-associated cartilage"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000070",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/AEO_0000111"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/AEO_0000111"
    ],
    "scoremap": [
        0
    ],
    "text": "simple squamous epithelium"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/AEO_0000178",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "fibrous joint"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CARO_0000043",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CARO_0000066",
        "http://purl.obolibrary.org/obo/AEO_0000123",
        "http://purl.obolibrary.org/obo/AEO_0000081",
        "http://purl.obolibrary.org/obo/AEO_0000145",
        "http://purl.obolibrary.org/obo/AEO_0000013",
        "http://purl.obolibrary.org/obo/AEO_0000203",
        "http://purl.obolibrary.org/obo/AEO_0000493",
        "http://purl.obolibrary.org/obo/AEO_0001021",
        "http://purl.obolibrary.org/obo/AEO_0001020",
        "http://purl.obolibrary.org/obo/AEO_0000122"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CARO_0000066",
        "http://purl.obolibrary.org/obo/AEO_0000123",
        "http://purl.obolibrary.org/obo/AEO_0000081",
        "http://purl.obolibrary.org/obo/AEO_0000145",
        "http://purl.obolibrary.org/obo/AEO_0000013",
        "http://purl.obolibrary.org/obo/AEO_0000203",
        "http://purl.obolibrary.org/obo/AEO_0000493",
        "http://purl.obolibrary.org/obo/AEO_0001021",
        "http://purl.obolibrary.org/obo/AEO_0001020",
        "http://purl.obolibrary.org/obo/AEO_0000122"
    ],
    "scoremap": [
        1,
        1,
        1,
        1,
        0,
        0,
        0,
        0,
        0,
        1
    ],
    "text": "simple tissue"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000136",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "fat cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000335",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "mesenchyme condensation cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000680",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "muscle precursor cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000196",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "flight muscle cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000029",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "neuron neural crest derived"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000228",
    "inputType": "radio",
    "nextQuestionMap": [
        "http://purl.obolibrary.org/obo/CL_0000525"
    ],
    "options": [
        "http://purl.obolibrary.org/obo/CL_0000525"
    ],
    "scoremap": [
        0
    ],
    "text": "multinucleate cell"
}{
    "category": "CATEGORIES.PERSONAL",
    "id": "http://purl.obolibrary.org/obo/CL_0000068",
    "inputType": "radio",
    "nextQuestionMap": "",
    "options": "",
    "scoremap": [],
    "text": "duct epithelial cell"
}