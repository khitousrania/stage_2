ASK THE FOLLOWING QUESTIONS
1. Is it acute or chronic? Acute loss of smell would certainly suggest an
acute upper respiratory infection. It would also suggest recent
exposure to toxic fumes or recent head injury. If the anosmia or
unusual odor is intermittent, then one should consider psychomotor
epilepsy.
2. Is there a history of trauma? A skull fracture, particularly if it
involves the cribriform plate, may interrupt the olfactory nerves and
cause anosmia.
3. Is there a history of drug use or overuse of nasal sprays? Captopril
and penicillamine may cause anosmia. Overuse of alcohol or tobacco
may also be the problem. Antirheumatic and antiproliferative drugs
are also known to cause anosmia.
4. Is the anosmia unilateral or bilateral? If there is unilateral anosmia,
one should consider an olfactory groove meningioma.
5. Are there other neurologic signs? Multifocal neurologic signs
should suggest multiple sclerosis, and additional neurologic signs such
as memory loss should suggest an olfactory groove meningioma or
parietal lobe tumor.
6. Are there signs of a systemic disease? Many systemic diseases may
cause anosmia, including hypothyroidism, diabetes, renal failure,
hepatic failure, and pernicious anemia.
DIAGNOSTIC WORKUP
If the disorder is acute and associated with an upper respiratory infection,
nothing needs to be done. However, if the condition has been of gradual
onset, the nasopharyngeal examination is negative, and the history of drugs
is negative, then a CT scan of the brain should be done. If this is negative,
a workup for systemic disease should be done, and that should include a
CBC and chemistry panel, thyroid profile, serum B12 and folic acid,
glucose tolerance test, and liver profile. If the anosmia or unusual odors
are intermittent, a wake-and-sleep EEG should be done. This symptom is
often delusional, so a psychiatrist may need to be consulted at the outset.
