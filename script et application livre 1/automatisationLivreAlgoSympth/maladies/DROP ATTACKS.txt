ASK THE FOLLOWING QUESTIONS
1. Is there loss of consciousness? If there is loss of consciousness, the
differential diagnosis for syncope should be considered (page 493).
2. Are there other neurologic signs and symptoms? Focal neurologic
signs and symptoms should make one think of basilar artery
insufficiency, cerebral arteriosclerosis, Ménière’s disease, and
cerebellar atrophy. A brain tumor should also be considered if there
are focal signs.
3. Is there hypotension, cardiomegaly, or a heart murmur? These
findings should make one think of orthostatic hypotension, aortic
stenosis and insufficiency, and cardiac arrhythmia.
DIAGNOSTIC WORKUP
Basic studies for the workup of drop attacks are CBC, sedimentation rate,
chemistry panel, VDRL test, chest x-ray, tilt table test, and EKG. These
will help identify anemia, hypoglycemia, and cardiovascular diseases. An
EEG should also be done to rule out epilepsy. If there are focal neurologic
signs, a CT scan or MRI should be done. Remember, the MRI is double
the cost of a CT scan and the diagnostic yield is only slightly higher. A
neurologist should be consulted to help decide which study is appropriate.
A 5-hour glucose tolerance test can be done to help diagnose
hypoglycemia. Four-vessel angiography or MRA is necessary to diagnose
vertebral–basilar disease. Holter monitoring will be useful to diagnose
complete heart block and other cardiac arrhythmias. If the chest x-ray or
EKG has revealed possible cardiac findings, a referral to a cardiologist
would be wise.
