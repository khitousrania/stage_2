ASK THE FOLLOWING QUESTIONS
1. Is it in the right upper quadrant? A mass in the right upper quadrant
is most often an enlarged liver. However, the liver may be pushed
down by a subphrenic abscess, and there may be an enlarged
gallbladder due to cholecystitis or bile duct obstruction. There may be
perinephric abscesses, tumors of the colon, renal tumors, adrenal
tumors, hydrops of the gallbladder, fecal impaction, or an abdominal
wall hematoma.
2. Is it in the epigastrium? A mass in the epigastrium also may be an
enlarged liver, but other types of masses must be considered, including
an omental hernia, pancreatic tumor, pancreatic cyst, gastric
carcinoma, pyloric stenosis, aortic aneurysm, and retroperitoneal
sarcoma.
3. Is it in the left upper quadrant? Left upper quadrant masses are
often a splenomegaly, but abdominal wall hematomas occur in this
area, as well as pancreatic tumors, pancreatic cysts, gastric tumors,
colon tumors, kidney tumors or enlargement, and fecal impaction.
4. Is it in the right lower quadrant? A mass in the right lower quadrant
is frequently a carcinoma of the colon, appendiceal abscess, psoas
abscess, pyosalpinx, regional ileitis, intussusception, or an ovarian
tumor.
5. Is it in the hypogastrium? A mass in the hypogastrium may be
bladder, pregnant uterus, uterine fibroids, regional ileitis, urachal cyst,
omental cyst, and, rarely, endometrial carcinoma.
6. Is it in the LLQ? An LLQ mass is most often a palpable sigmoid
colon, but pathologic conditions such as diverticulitis with abscess,
carcinoma of the colon, and ovarian tumors may be present.
7. Is the mass tender? The presence of a tender mass in the right upper
quadrant often means congestive heart failure, a tender liver from
hepatitis, or a tender gallbladder from cholecystitis, subphrenic
abscess, perinephric abscess, or an abdominal wall hematoma. A
tender mass in the epigastrium may be a pancreatic cyst. A tender
mass in the left upper quadrant may be an abdominal wall hematoma
or a perinephric abscess. A tender mass in the right lower quadrant
may be appendiceal abscess, psoas abscess, pyosalpinx, regional
ileitis, or intussusception. A tender mass in the LLQ may be a
diverticulitis or pyosalpinx.
8. Is there blood in the urine or stool? The presence of blood in the
urine, of course, would suggest a tumor of the kidney such as
hypernephroma or Wilms’ tumor. The presence of blood in the GI
tract would suggest either a gastric carcinoma or colon carcinoma but
may also be seen in intussusception, diverticulitis, and regional ileitis.
Occasionally, it is seen in carcinoma of the ampulla of Vater.
9. Is there fever? The presence of fever would suggest that the mass is
an abscess such as subphrenic abscess, perinephric abscess,
diverticular abscess, appendiceal abscess, or pyosalpinx. Fever also
suggests hepatitis, cholecystitis, or cholangitis.
DIAGNOSTIC WORKUP
Routine diagnostic tests include a CBC, sedimentation rate, urinalysis,
chemistry panel with electrolytes, amylase and lipase, stool for occult
blood, EKG, chest x-rays, and a CT scan of the abdomen and pelvis with
and without contrast. A carcinoembryonic antigen test may diagnose colon
cancer. Alpha-1-fetoprotein may diagnose carcinoma of the liver. If there
are chills and fever, blood cultures ought to be done. Next in line are
contrast radiographic studies such as upper GI series, barium enema, small
bowel series, IVP, or cholecystogram.
At this point, before ordering more expensive tests, a surgeon or
gastroenterologist should be consulted. An abdominal ultrasound will be
helpful in differentiating cholecystitis and other cystic masses of the
pancreas, kidneys, and reproductive organs.
Endoscopic procedures will help diagnose carcinoma of the stomach
and colon and diverticulitis. ERCP is useful in diagnosing carcinoma of
the pancreas and bile ducts.
Lymphangiography will help differentiate retroperitoneal tumors. An
MRI may provide better visualization of the biliary and pancreatic ductal
systems. Gallium scans will help uncover subdiaphragmatic, perinephric,
diverticular, and pelvic abscesses. Peritoneal taps will help differentiate
ascites, pancreatitis, and peritoneal bleeding. Needle biopsy of the liver or
any mass lesion under laparoscopic guidance may be diagnostic. A
laparoscopy is useful in differentiating many types of masses also.
Ultimately, exploratory laparotomy is still an excellent way of establishing
a diagnosis.
