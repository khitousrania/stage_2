CASE HISTORY
A 32-year-old white male presents at your clinic with a 3-week history of
scrotal swelling. Following the algorithm, you note that it is only his left
scrotum, so systemic diseases, such as congestive heart failure, cirrhosis
and nephrosis can be ruled out. It is painless, so it is unlikely that he has
torsion of the testicle, or an incarcerated, or strangulated inguinal hernia.
On examination, you find that the mass fails to transilluminate ruling
out a hydrocele. However, it is reducible, so you suspect a sliding inguinal
hernia or varicocele. You would be correct.
ASK THE FOLLOWING QUESTIONS
1. Is it diffuse or focal? Diffuse scrotal swelling would suggest
congestive heart failure, nephrosis, uremia, and cirrhosis, as well as
focal diseases such as filariasis or bilateral hydrocele. Focal scrotal
swelling would suggest a hernia, hydrocele, torsion of the testicle,
abscesses, epididymitis, orchitis, varicoceles, and testicular tumors.
2. If it is diffuse, is there ascites or generalized edema? The presence
of diffuse edema of the scrotum with ascites or generalized edema
would suggest congestive heart failure, nephrosis, uremia, or cirrhosis.
3. If it is focal, is it painful? The presence of painful scrotal swelling
would suggest an incarcerated or strangulated inguinal hernia, torsion
of the testicle, a hematoma, orchitis, epididymitis, furuncle, or abscess.
4. Does it transilluminate? If the mass transilluminates, it is very likely
a hydrocele of the testicle or a spermatocele.
5. Is it reducible? If the mass is reducible, it is most likely an inguinal
hernia or a varicocele.
DIAGNOSTIC WORKUP
Routine laboratory tests include a CBC, sedimentation rate, urinalysis,
urine culture, and urethral smear. If prostatic disease is suspected, a PSA
should be ordered. If intestinal obstruction is suspected, a flat plate of the
abdomen and lateral decubitus should be ordered. Ultrasonography or a
radionuclide testicular scan with technetium-99m are useful in
differentiating between testicular torsion and epididymitis. Scrotal
ultrasound may be done to evaluate any kind of testicular or scrotal mass.
However, it is much less costly to refer the patient to an urologist.
