ASK THE FOLLOWING QUESTIONS
1. Are there diminished or absent peripheral pulses? The finding of
poor peripheral pulses would suggest that the lesion is secondary to
ischemia from arteriosclerosis, Buerger’s disease, diabetic arteriolar
sclerosis, familial hyperlipidemia, and cryoproteinemia.
2. Are there abnormalities on neurologic examination? The presence
of good peripheral pulses should make one look for a neurologic
explanation for the ulcer, and if there is diminished sensation to touch
and pain in the periphery, peripheral neuropathy is very likely. Ulcers
may also form in paraplegia of any cause, leprosy, and tabes dorsalis.
3. Is there a history of diabetes? A history of diabetes makes the
diagnosis of diabetic arteriolar sclerosis very likely. Remember, the
pulses may be normal in this condition.
4. Is there a positive smear or culture? The presence of good
peripheral pulses should prompt one to do a smear and culture of
material from the lesion, and if this is positive, then the diagnosis is
made. We would consider, in addition to the normal bacteria,
blastomycosis, sporotrichosis, maduromycosis, and syphilis.
DIAGNOSTIC WORKUP
Diminished pulses are a clear indication for Doppler ultrasound studies.
Routine tests include a CBC, sedimentation rate, urinalysis, chemistry
panel, VDRL test, and glucose tolerance test. An x-ray of the involved foot
should be done to rule out osteomyelitis. A bone scan or CT scan is even
more sensitive to osteomyelitis and other disorders of the bone that may be
causing the ulcer. A smear should be made of the ulcer material and a
culture done also, not just for the common pathogens, but for AFB and
fungi. A dark field preparation may be necessary. Skin testing for
blastomycosis and other fungi should be done. A nerve conduction
velocity study of the lower extremities will be helpful in differentiating
neurologic causes. Femoral angiography may be valuable in determining
the exact level of the lesion and whether it can be approached surgically.
