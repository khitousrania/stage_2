ASK THE FOLLOWING QUESTIONS
1. Are there hyperactive or pathologic reflexes of the involved
extremity? These findings suggest spinal cord tumor, parasagittal
tumor, amyotrophic lateral sclerosis, anterior cerebral artery occlusion,
spinal cord injury, transverse myelitis, and multiple sclerosis.
2. Are there decreased or absent reflexes of the involved extremity?
These findings suggest a herniated disk, a cauda equina tumor or early
cervical cord tumor, progressive muscular atrophy, brachial plexus
neuropathy, sciatic neuritis, or peripheral neuropathy.
3. Is the onset acute or gradual? An acute onset would suggest a
vascular lesion such as anterior cerebral artery occlusion, a spinal cord
injury, transverse myelitis, and multiple sclerosis. A gradual onset
suggests a space-occupying lesion such as spinal cord tumor,
parasagittal tumor, and degenerative diseases such as amyotrophic
lateral sclerosis.
4. Are there exacerbations or remissions? The presence of
exacerbations or remissions should suggest multiple sclerosis,
transient ischemic attack, and migraine.
DIAGNOSTIC WORKUP
Monoplegia of the upper extremities with hyperactive reflexes should
suggest the need to order a CT scan or an MRI of the brain and/or an MRI
of the cervical spine.
Monoplegia of the lower extremities with hyperactive reflexes or
pathologic reflexes would suggest the need to order MRI of the thoracic
spine. However, because an anterior cerebral artery occlusion or
parasagittal tumor may cause similar findings, a CT scan of the brain may
be necessary. Rather than make this difficult choice yourself, a neurologist
should be consulted. He/she may want to do a spinal fluid analysis or
evoked potential studies as well. If he/she believes a vascular lesion is
possible, then he/she may want to do a four-vessel angiography, MRA or
simply a carotid scan.
The findings of monoplegia with hypoactive reflexes, especially of
gradual onset, would suggest a radiculopathy, peripheral neuropathy, or
plexopathy. In the lower extremities, these findings would indicate the
need for a CT scan or an MRI of the lumbosacral spine. In the upper
extremities, these findings would suggest the need for an MRI of the
cervical spine.
A neuropathy workup is also indicated in monoplegia of the upper or
lower extremity (page 378). Nerve conduction velocity studies and EMG
studies of the involved extremities are also extremely valuable. The most
cost-effective approach is to refer the patient to a neurologist at the outset.
