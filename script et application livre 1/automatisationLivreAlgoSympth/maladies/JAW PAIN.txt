ASK THE FOLLOWING QUESTIONS
1. Are there abnormalities on examination of the teeth or gums? A
thorough examination of the teeth and gums may disclose dental
caries, gingivitis, oral tumors, or alveolar abscess.
2. Is the pain intermittent? Intermittent pain should suggest a
trigeminal neuralgia or glossopharyngeal neuralgia.
3. Is there a rash? The presence of a rash would suggest herpes zoster.
Be sure to examine the eardrum for Ramsay Hunt’s syndrome.
DIAGNOSTIC WORKUP
Routine diagnostic studies include a CBC, sedimentation rate, chemistry
panel, arthritis panel, and an x-ray of the teeth and jaw. An x-ray of the
sinuses may be helpful. At this point of time, referral to a dentist or oral
surgeon should be made if there is still diagnostic difficulty. He may order
an MRI of the temporomandibular joint, which is the procedure of choice
in evaluating this joint. If all tests are negative or equivocal, perhaps a
psychiatric referral is in order.
