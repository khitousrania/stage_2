ASK THE FOLLOWING QUESTIONS
1. Is there a history of trauma? Trauma, of course, may cause fractures
and subperiosteal hematomas.
2. Is the patient a child or an adult? Children are more likely to have
Ewing’s tumors, scurvy, rickets, syphilis, battered baby syndrome,
osteosarcoma, osteomas, and osteochondromas. Adults are more likely
to have giant cell tumor, metastasis, osteomyelitis, osteogenic
sarcoma, fibrosarcoma, multiple myeloma, generalized fibrocystic
disease, Paget’s disease, acromegaly, and chondromas.
3. Are the lesions single or focal or are they multiple or diffuse?
Multiple and diffuse lesions in children are often because of scurvy,
rickets, syphilis, and battered baby syndrome. Multiple lesions or
diffuse lesions in adults are often because of metastasis, multiple
myeloma, generalized fibrocystic disease, Paget’s disease,
acromegaly, and chondroma. Single lesions in children are more likely
to be fracture, osteomyelitis, hematoma, Ewing’s tumor,
osteosarcoma, osteomas, and osteochondromas. Single lesions in
adults are often because of a giant cell tumor, osteomyelitis, fracture,
hematoma, osteogenic sarcoma, and fibrosarcoma, but may be because
of a metastasis.
4. Are the lesions usually painful? Painful lesions in children are more
likely to be because of fracture, osteomyelitis, hematoma, Ewing’s
tumors, scurvy, syphilis, battered baby syndrome, and rickets. Painful
lesions in adults may be because of a giant cell tumor, metastasis,
osteomyelitis,     fracture,    hematomas,     osteogenic     sarcoma,
fibrosarcomas, and multiple myeloma.
DIAGNOSTIC WORKUP
Routine diagnostic studies include a CBC, sedimentation rate, urinalysis,
chemistry panel, arthritis panel, serum protein electrophoresis, and plain
films of the involved bones. A skeletal survey may be necessary. If the
diagnosis remains in doubt, consult an orthopedic surgeon at this point.
Bone scans are often useful. A search for a primary tumor may require
chest x-ray, upper GI series, barium enema, IVP, CT scan of the abdomen
mammography, prostatic examination, PSA titer, thyroid scans, lymph
node biopsy, and bone marrow examinations.
CT scans of the area may help differentiate the mass or swelling.
Needle biopsy or exploratory surgery and bone biopsy may be necessary
before deciding what surgical approach should be undertaken.
