ASK THE FOLLOWING QUESTIONS
1. What is the chloride level? Hypernatremia with an elevated chloride
is almost certainly due to dehydration, but renal and hypothalamic
diabetes insipidus, heat exhaustion, and hypertonic fluid
administration may also be responsible. A low chloride level with
hypernatremia may be seen in aldosteronism or Cushing’s syndrome.
2. What is the serum antidiuretic hormone (ADH) assay? If this is
low or absent, hypothalamic diabetes insipidus must be considered. If
this is normal, one should consider dehydration, heat exhaustion,
prolonged vomiting, renal diabetes insipidus, and hypertonic saline
administration as likely causes.
DIAGNOSTIC WORKUP
The workup should include a CBC, urinalysis, chemistry panel, serum and
urine osmolality, plasma cortisol, serum ADH, plasma volume studies,
serial electrolytes, and consultation with an endocrinologist or
nephrologist.
