ASK THE FOLLOWING QUESTIONS
1. Are the findings intermittent? If the pathologic reflexes come and
go, transient ischemic attacks, multiple sclerosis, migraine, epilepsy,
and hypoglycemia should be considered in the differential diagnosis.
2. Are they unilateral or bilateral? Unilateral pathologic reflexes
should signify either a brain tumor or vascular lesion. Bilateral
pathologic reflexes should suggest an inflammatory or degenerative
disease. However, multiple sclerosis may present with either unilateral
or bilateral pathologic reflexes. Vascular lesions in the basilar
circulation may also present with bilateral pathologic reflexes. It
should be pointed out that there is no hard-and-fast rule.
3. Is there associated facial palsy or other cranial nerve signs? The
presence of facial palsy or other cranial nerve signs should make one
look for a lesion in the brain or brain stem.
4. Is there headache or papilledema? The presence of headache or
papilledema should prompt the investigation for a space-occupying
lesion of the brain or brain stem.
5. Is there hypertension or a possible source for an embolism? These
findings would suggest a cerebral vascular accident such as cerebral
hemorrhage or embolism.
6. Is the sensory examination normal? The findings of bilateral
pathologic reflexes or unilateral pathologic reflexes with a normal
sensory exam and no cranial nerve signs would suggest amyotrophic
lateral sclerosis or primary lateral sclerosis.
DIAGNOSTIC WORKUP
Routine studies include a CBC, sedimentation rate, urinalysis, chemistry
panel, ANA assay, serum B12 and folic acid, VDRL test, chest x-ray, and
EKG. If there are cranial nerve signs, a CT scan or an MRI of the brain
will usually be necessary. However, it is wise to get a neurology
consultation before undertaking these expensive tests. A spinal tap may be
done if the imaging study is negative.
If vascular disease is suspected, carotid scans to rule out carotid
stenosis or plaque and a search for an embolic source using
echocardiography and blood culture should be done. A cardiologist can
assist in this search. Four-vessel cerebral angiography or MRA may be
necessary. In fact, if a cerebral hemorrhage has been ruled out and there is
no significant hypertension, a four-vessel cerebral angiographic study
should probably be done. Evoked potential studies and HIV antibody titers
should also be done. If there are no cranial nerve signs, an MRI of the
cervical spine or thoracic spine should be done, depending on the level of
the lesion. Myelography may also be helpful. Serum protein
electrophoresis and immunoelectrophoresis all may be necessary in the
workup.
