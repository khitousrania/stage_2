ASK THE FOLLOWING QUESTIONS
1. Are there significant RBCs in the urine? If so, one should consider
glomerulonephritis, collagen disease, tuberculosis, neoplasm, trauma,
renal calculus, and polycystic kidney disease. If not, the most likely
etiology is a UTI.
2. Are there WBC casts in the urine? This would make pyelonephritis
or interstitial nephritis more likely.
3. Are there RBC casts in the urine? This would help differentiate
glomerulonephritis and collagen diseases from the other conditions
associated with hematuria.
DIAGNOSTIC WORKUP
First, look at the urinary sediment under a microscope. Further workup
should include a urine culture and colony count, AFB smear and culture,
CBC, sedimentation rate, ANA test, chemistry panel, serum protein
electrophoresis, IVP, and a urology consultation. In cases of sterile pyuria
don’t forget to consider renal tuberculosis. A chest x-ray and sputum
culture for AFB should be done. An urologist may do cystoscopy and
retrograde pyelography. He may order a CT scan of the abdomen to rule
out renal carcinomas and other kidney diseases. A nephrologist may need
to be consulted in difficult cases. An urologist should be consulted in all
cases of recurrent or persistent pyuria.
