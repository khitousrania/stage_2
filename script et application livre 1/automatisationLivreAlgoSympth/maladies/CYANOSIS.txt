ASK THE FOLLOWING QUESTIONS
1. Is there a history of drug ingestion? Potassium chlorate,
sulfanilamide, and coal tar products are only a few of the drugs that
may cause methemoglobinemia and sulfhemoglobinemia.
2. Is the cyanosis limited to one extremity? If the cyanosis is limited to
one extremity, one should suspect an arterial embolism or
phlebothrombosis.
3. Is the cyanosis limited to the extremities or is it generalized?
Cyanosis that is limited to the extremities only suggests Raynaud’s
disease, Raynaud’s phenomena, shock, and acrocyanosis.
4. Is there associated dyspnea? If there is significant dyspnea, one
should consider a pulmonary or cardiac origin for the cyanosis such as
cyanotic congenital heart disease, pulmonary emphysema, pulmonary
fibrosis, or pulmonary embolism.
5. Is the patient a child or an adult? Certain causes of cyanosis are
limited to children, such as laryngismus stridulus, laryngotracheitis,
and acute subglottic laryngitis.
6. Is there a heart murmur or cardiomegaly? A heart murmur or
cardiomegaly suggests rheumatic carditis, congenital heart disease, or
congestive heart failure.
DIAGNOSTIC WORKUP
It is wise to order a cardiology consult at the outset. Arterial blood gases,
EKG, chest x-ray, and pulmonary function studies will diagnose most
cases that are because of pulmonary or cardiac causes. If there is a history
of drug ingestion, the blood should be drawn for methemoglobin and
sulfhemoglobin testing. If a pulmonary embolism is suspected, a
ventilation–perfusion scan or spiral CT scan and pulmonary arteriography
may need to be done. If a peripheral embolism is suspected, angiography
of the vessel involved will be diagnostic. Sputum or nose and throat
cultures will be useful in diagnosing the infectious diseases associated with
cyanosis.
