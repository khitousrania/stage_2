ASK THE FOLLOWING QUESTIONS
1. What is the serum albumin? A decreased serum albumin associated
with an increased cholesterol strongly suggests the nephrotic
syndrome or liver disease. These two conditions may be further
differentiated by the finding of proteinuria in the nephrotic syndrome.
2. What is the free T4? Hypercholesterolemia associated with a low T4
suggests myxedema.
3. What is the triglyceride level? An elevated cholesterol coupled with
a marked increase in the triglyceride is typical of type I and type II
lipoproteinemia. Type IIB, type III, and type IV lipoproteinemias are
associated with a high cholesterol but only mild increase of
triglyceride. Oral contraceptive use, atherosclerosis, and
xanthomatosis may present the same picture. These may be further
differentiated by lipoprotein electrophoresis.
DIAGNOSTIC WORKUP
First repeat the lipid panel after a 14-hour fast. If the total cholesterol is
still elevated, proceed with a workup of hypercholesterolemia. This should
include a CBC, CRP, urinalysis, chemistry panel, overnight refrigeration
of plasma, free T4, TSH, apolipoprotein, and lipoprotein electrophoresis.
An elevated CRP is especially helpful in evaluating the risk for stroke or
myocardial infarction. If these tests suggest liver disease, liver function
tests and liver biopsy may be indicated. If these tests suggest kidney
disease, a nephrologist should be consulted for consideration of renal
function tests or renal biopsy. A metabolic disease specialist or an
endocrinologist may need to be consulted for further diagnostic evaluation
and management.
