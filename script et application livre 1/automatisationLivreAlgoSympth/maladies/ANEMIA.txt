CASE HISTORY
A 59-year-old white man complains of difficulty with his memory and
tingling of his hands and feet for the past year. His physical examination
reveals a slight decrease in vibratory sense below the knees and a smooth
tongue. His hemoglobin in your office is 13.1 g. Although this is not
striking, you know that this is below the normal of 14 to 17 for males.
Following the algorithm, you find he denies alcohol abuse and has not
noted bloody or tarry stools in the past year. He is not jaundiced. You
order a CBC and find, among other things, that his white count is low. You
suspect pernicious anemia, and further studies confirm your diagnosis.
ASK THE FOLLOWING QUESTIONS
1. Is there a history of chronic blood loss? A history of peptic ulcer,
ulcerative colitis, or other causes of chronic GI bleeding would
indicate the anemia is most likely caused by iron deficiency. Likewise,
chronic hypermenorrhea or metrorrhagia in women of child-bearing
age may lead to iron deficiency anemia.
2. Are there neurologic signs? Paresthesias, loss of vibratory sense,
ataxia, and mild dementia should lead one to suspect pernicious
anemia.
3. Is there jaundice? Clinical jaundice should arouse the suspicion of a
hemolytic anemia, but mild clinical jaundice may be seen in
pernicious anemia also. Chronic liver disease, such as alcoholic
cirrhosis, is often associated with folate deficiency or sideroblastic
anemia.
4. What is the white count? A decreased white count should alert one to
the possibility of aplastic anemia, myelofibrosis, or infiltrative
diseases of the bone marrow, such as carcinomatosis, Gaucher’s
disease, or lymphoma. It may also be related to pernicious anemia. An
increased white count would suggest leukemia, chronic infections,
anemia, or bacteremia.
5. Is the tourniquet test positive? This would suggest disseminated
intravascular coagulation or thrombocytopenia purpura of various
etiologies.
DIAGNOSTIC WORKUP
The most important thing to do initially is to examine a blood smear for
red cell morphology. If the anemia is microcytic, one would consider iron
deficiency or chronic blood loss. If it is macrocytic, consideration should
be given to pernicious anemia or folate deficiency. If there are
schistocytes, look for disseminated intravascular coagulation. Further
workup should include a CBC, red cell indices, chemistry panel, sickle cell
prep, Hb electrophoresis, stool for occult blood, liver function tests,
reticulocyte count, serum homocysteine, serum ferritin, serum iron and
iron-binding capacity, serum B12 and folic acid, serum haptoglobin, and
platelet count. A urine test for methylmalonic acid will help diagnose
pernicious anemia. A positive stool for occult blood would prompt a GI
workup. If these studies are inconclusive, a hematologist should be
consulted. A hematologist will perform a bone marrow examination for a
more definitive diagnosis. Perhaps a liver–spleen scan, CT scan of the
abdomen, or therapeutic trial of iron, B12, or folic acid is indicated.
