ASK THE FOLLOWING QUESTIONS
1. Are the pulses diminished? The presence of diminished pulses
should suggest peripheral arteriosclerosis or Leriche’s syndrome.
2. Is there associated pain in the involved extremity? The presence of
pain in the involved extremity should suggest lumbar spondylosis,
spinal stenosis, cauda equina tumor, spondylolisthesis, herniated disk,
and pelvic tumors.
3. Is there a positive straight-leg raising test and/or decreased
Achilles reflex? These findings suggest a herniated disk of L4 to 5 or
L5 to S1, lumbar spondylosis, spinal stenosis, a cauda equina tumor,
or spondylolisthesis.
4. Is there a positive femoral stretch test or decreased knee jerk?
These findings suggest a herniated disk of L3 to 4 or L2 to 3 or lumbar
spondylosis.
5. Are there diffuse hyperactive reflexes? These findings suggest
multiple sclerosis, pernicious anemia, degenerative diseases of the
spinal cord, such as syringomyelia, spinal cord tumor, or other space-
occupying lesions. It may also suggest anterior spinal artery occlusion.
6. Are there diffuse hypoactive reflexes? The presence of diffuse
hypoactive reflexes would suggest poliomyelitis, Guillain–Barré
syndrome, cauda equina tumor, metastatic tumor of the lumbar spine,
and, occasionally, pernicious anemia or peroneal neuropathy. Also,
peripheral neuropathy will present with diffuse hypoactive reflexes.
7. Is there incontinence associated with the hypoactive reflexes? The
presence of incontinence with the hypoactive reflexes may indicate
poliomyelitis, cauda equina tumor, or metastatic tumors to the lumbar
spine.
8. Paresthesias limited to the foot and toes may indicate Morton’s
neuroma or tarsal tunnel syndrome. If there is a positive Tinel’s sign
over the tibial nerve or a positive cuff test a tarsal tunnel syndrome is
even more likely.
DIAGNOSTIC WORKUP
The basic diagnostic workup includes a CBC, CRP, sedimentation rate,
urinalysis, chemistry panel, arthritis panel, VDRL test, and x-ray of the
lumbosacral spine. Serum B12 and folic acid tests should be done if
pernicious anemia is suspected. If these tests are negative, an orthopedic or
neurologic specialist should be consulted. A CT scan of the lumbosacral
spine, a nerve conduction velocity study, and an EMG may all be
necessary in the workup. An MRI is more expensive and often
unnecessary.
Combined myelography and a CT scan is often useful in evaluating the
need for surgery. A bone scan may be helpful in diagnosing occult
fractures, metastases, or osteomyelitis.
If multiple sclerosis, Guillain–Barré syndrome, or central nervous
system lues is suspected, a spinal tap may be done. Somatosensory evoked
potential (SSEP) studies are useful in diagnosing multiple sclerosis.
A neuropathy workup may be necessary. This involves a HbA1c or a
glucose tolerance test to rule out diabetes; urine tests for porphyrins and
porphobilinogen to rule out porphyria; quantitative urine niacin, thiamine,
pyridoxine, and other B vitamins after loading, an ANA and anti-dsDNA
test to rule out collagen disease; serum protein electrophoresis and
immunoelectrophoresis to diagnose various collagen diseases and
macroglobulinemia; a lymph node biopsy and Kveim test for sarcoidosis;
nerve conduction velocity studies and EMG to establish the presence of a
neuropathy; thyroid profile to rule out hypothyroidism or hyperthyroidism;
HIV antibody titers; blood levels for heavy metals such as lead to rule out
lead or arsenic neuropathy; and skin and muscle biopsies to rule out
various collagen diseases. Blood tests are now available to rule out all the
various vitamin deficiencies that may cause paresthesias. Nevertheless, a
trial of therapy is often necessary to rule out the nutritional neuropathies.
Lumbar puncture, as already mentioned, is useful in diagnosing
Guillain–Barré syndrome. Remember that the cerebrospinal fluid (CSF)
protein is also elevated in diabetic neuropathy and hypothyroidism. Nerve
biopsy may be necessary when all the above procedures are negative.
RBC transketolase activity is decreased in beriberi and the serum
pyruvate and lactate levels are elevated.
