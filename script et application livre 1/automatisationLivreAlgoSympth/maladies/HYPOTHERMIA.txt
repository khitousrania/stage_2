ASK THE FOLLOWING QUESTIONS
1. Is there a history of drug or alcohol ingestion? Alcoholic
intoxication, opium poisoning, tricyclic antidepressants, and
phenothiazine may cause hypothermia.
2. Is there a history of severe vomiting or diarrhea? Severe vomiting
or diarrhea may induce dehydration and electrolyte disturbances,
which will induce hypothermia. Intestinal obstruction, cholera, and
peritonitis are among the many disorders that may lead to severe
vomiting or diarrhea.
3. Are there endocrine abnormalities? Signs of hypothyroidism and
Addison’s disease may be obvious, but hypopituitarism,
hypoglycemia, and diabetes mellitus may also be the cause of
hypothermia.
4. Are there abnormalities on the neurologic examination? Focal
neurologic findings may be seen in a cerebral vascular accident or
epidural or subdural hematoma. However, thiamine deficiency may
also result in hypothermia.
DIAGNOSTIC WORKUP
Routine laboratory tests include a CBC and differential count, a
sedimentation rate and chemistry panel, electrolytes, thyroid profile, blood
cultures, urinalysis, and a urine drug screen. An EKG and chest x-ray
should also be done. A CT scan of the brain is done if there are focal
neurologic abnormalities or disorders of consciousness.
An infusion of dextrose intravenously and thiamine are given as soon
as blood studies are drawn in case there is hypoglycemia or thiamine
deficiency.
If the above studies are normal, a thorough endocrine workup is
indicated, including tests for serum cortisol, FSH, lutein-stimulating
hormone, and growth hormone. A cardiologist, neurologist, or
endocrinologist may need to be consulted to help solve the diagnostic
dilemma.
