ASK THE FOLLOWING QUESTIONS
The cause of scalp tenderness is usually obvious when there is dermatitis
or inflammatory disorders of the skin, such as herpes zoster, pediculosis,
tinea capitis, infected sebaceous cyst, and impetigo.
The tenderness is more subtle in temporal arteritis unless there is
associated homolateral blindness or obvious enlargement of the superficial
temporal artery. Tenderness in the occipital area is usually due to occipital
major or minor entrapment by the posterior cervical muscles. This is
common after flexion–extension injuries of the cervical spine or cervical
spondylosis. Referred tenderness from trigeminal neuralgia, sinusitis, otitis
media, mastoiditis, and disorders of the teeth may occur. When a patient
presents with scalp tenderness, especially at the top of the head, and the
physical examination is normal, the diagnosis of psychoneurosis should be
entertained.
DIAGNOSTIC WORKUP
When there are obvious skin lesions, cultures, potassium hydroxide
preparations, or biopsies will usually establish the diagnosis. A skull x-ray
should be done to exclude fracture, rickets, syphilitic periostitis, and
primary and secondary tumors of the cranium. A sedimentation rate should
be done to exclude temporal arteritis, especially in the elderly. If the
physical examination and diagnostic workup are normal and the patient
persists with the complaint, a referral to a psychiatrist is in order.
