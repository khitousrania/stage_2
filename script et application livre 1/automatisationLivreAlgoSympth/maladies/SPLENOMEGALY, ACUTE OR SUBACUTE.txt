ASK THE FOLLOWING QUESTIONS
1. Is there fever? The presence of fever should suggest infectious
mononucleosis, infectious hepatitis, leptospirosis, acute leukemia,
lymphoma, malaria, and bacterial endocarditis, among other things.
2. Is there a rash? The presence of a rash should suggest
thrombocytopenic purpura, acute leukemia, typhoid fever, septicemia,
and lupus erythematosus.
3. Is there jaundice? The presence of jaundice should suggest infectious
hepatitis, leptospirosis, malaria, hereditary spherocytosis and other
hemolytic anemias, and portal vein thrombosis secondary to chronic
liver disease.
4. Is there lymphadenopathy? The presence of lymphadenopathy
should suggest infectious mononucleosis, acute lymphatic leukemia,
lymphoma, brucellosis, and reticuloendotheliosis.
5. Is there a history of trauma? The presence of a history of trauma
would suggest a traumatic rupture of the spleen.
DIAGNOSTIC WORKUP
Routine tests include a CBC, platelet count, sedimentation rate, chemistry
panel, febrile agglutinins, serum haptoglobins, ANA test, monospot test,
serum protein electrophoresis, tuberculin test, a chest x-ray, EKG, and flat
plate of the abdomen.
If there is jaundice, a hepatitis profile, red cell fragility test, and blood
smear for parasites should be done. If there is fever, serial blood cultures,
leptospirosis antibody titer, and smear for malarial parasites should be
done. If there is a petechial rash, a coagulation profile should be done.
Lymph node biopsies and bone marrow examinations may be necessary. A
CT scan of the abdomen and radionuclide scan for liver and spleen size
and ratio should be done. The assistance of a hematologist or infectious
disease expert should be sought. A surgeon may need to be consulted for
an exploratory laparotomy.
