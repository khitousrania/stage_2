ASK THE FOLLOWING QUESTIONS
1. What is the BUN/creatinine ratio? If this ratio is 20:1 or greater, one
should look for prerenal azotemia. Confirmation with a serum and
urine osmolality will be helpful. If this ratio is 10:1 or less, one should
look for renal diseases or obstructive uropathy.
2. Is the bladder enlarged, or is there significant residual urine?
These findings point to obstructive uropathy, particularly bladder neck
obstruction. If these findings are absent, the cause of the uremia is
most likely a renal disease in cases in which the BUN/creatinine ratio
is 10:1 or less.
3. What drugs is the patient on? ACE inhibitors, diuretics, and
nonsteroidal anti-inflammatory drugs (NSAIDs) can cause renal
failure.
DIAGNOSTIC WORKUP
Two things to do at the outset are a microscopic examination of the urine
and renal and bladder ultrasound. Further workup should include a CBC,
urinalysis, urine culture and colony count, serum and urine osmolality,
chemistry panel, sedimentation rate, arterial blood gas analysis, blood
volume, cystoscopy and retrograde pyelography, a nephrology consult, and
a urology consult. Additional studies include abdominal CT scan and a
renal biopsy.
