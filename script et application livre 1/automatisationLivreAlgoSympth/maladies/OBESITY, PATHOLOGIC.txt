ASK THE FOLLOWING QUESTIONS
1. Is there associated hyperphagia? If the patient recognizes that he or
she has a ravenous appetite or eats more than is necessary, the
possibility of an insulinoma or Fröhlich’s syndrome should be
considered.
2. Is the obesity centripetal? The presence of centripetal obesity,
especially with moon facies, should suggest Cushing’s syndrome.
3. Is the obesity mainly of the lower extremities? This finding would
suggest lipodystrophy.
4. Is there mental retardation? The presence of mental retardation
should suggest Laurence–Moon–Bardet–Biedl syndrome.
5. What is the sex of the patient? In male patients, one should consider
Klinefelter’s syndrome, and in female patients, one should consider
polycystic ovary.
6. What drugs is the patient on?
Many drugs may cause obesity, most notably the tricyclic
antidepressants and corticosteroids.
DIAGNOSTIC WORKUP
Routine laboratory tests include a CBC, urinalysis, chemistry panel, 2-hour
postprandial blood sugar, and thyroid profile. If an insulinoma is strongly
suspected, plasma insulin, C-peptide, a 24- to 36-hour fast, a 5-hour
glucose tolerance test, and tolbutamide tolerance test may be done. If
Cushing’s syndrome is suspected, a 24-hour urine free cortisol and cortisol
suppression test should be done. Pelvic ultrasound will help diagnose
polycystic ovaries. Chromosomal analysis will help diagnose Klinefelter’s
syndrome. Perhaps a psychiatrist should be consulted.
