ASK THE FOLLOWING QUESTIONS
1. Is it true vertigo? True vertigo is characterized by the fact that the
person feels he/she or his/her environment is turning. One other form
of true vertigo is lateral pulsion, in which the person feels as if he/she
is moving to the left or right or may be moving forward or backward.
True vertigo is a sign of neurologic or otologic disease, whereas
dizziness that is not true vertigo is more likely a sign of cardiovascular
disease, drug toxicity, or hypoglycemia.
2. Is there associated tinnitus or deafness? The presence of tinnitus or
deafness, especially if the ear examination is negative, is a sign of a
more serious otologic or neurologic condition. Disorders such as
cholesteatoma, acoustic neuroma, and Ménière’s disease must be
considered. On the contrary, vertigo without tinnitus or deafness
should prompt consideration of benign positional vertigo and
vestibular neuronitis.
3. Are there other neurologic findings? The finding of abnormalities of
other cranial nerves or the long tracts, such as the pyramidal tracts,
would suggest multiple sclerosis, an advanced brain stem tumor,
acoustic neuroma, or basilar artery insufficiency.
4. Are there findings on otoscopic examination? A normal neurologic
examination with an abnormal ear examination would suggest otitis
media, cholesteatoma, or petrositis.
5. Is there tachypnea during the attack? If there is hyperventilation
during the attack, then hyperventilation syndrome should be
considered.
6. Is there a history of trauma? A history of trauma would suggest a
postconcussion syndrome.
7. Are there abnormalities of the blood pressure? If the dizziness is
really light-headedness, hypertension may be present, but hypertension
may also cause true vertigo. Hypotension is more likely to cause light-
headedness, which is not true vertigo. Be sure to take the blood
pressures while the patient is lying down and again after rapidly rising
to the standing position.
8. Are there abnormal cardiac findings? A thorough cardiovascular
examination should be done. Irregularities of the heartbeat, heart
murmurs, or cardiac enlargement will suggest cardiac arrhythmia,
aortic stenosis and insufficiency, mitral stenosis, prolapse of the mitral
valve, and congestive heart failure. A slow pulse may indicate heart
block or a sick sinus syndrome.
9. Is there pallor? Moderate to severe anemia will cause light-
headedness and dizziness, but usually not true vertigo.
DIAGNOSTIC WORKUP
If there is true vertigo, an audiogram and a caloric test or
electronystagmography should be done. Hallpike maneuver should also be
done to exclude benign positional vertigo. If these are abnormal, an x-ray
of the mastoids, petrous bones, and internal auditory canals should be
done. At this point, a neurologist should be consulted. If an acoustic
neuroma is strongly suspected, an MRI of the brain stem and auditory
canals should be done. If the MRI of the brain is negative, a spinal fluid
examination can be done to exclude such disorders as central nervous
system lues and multiple sclerosis. An MRI of the brain needs to be done
to distinguish multiple sclerosis. BSEPs, VEPs, and SSEPs will also be
helpful in making the diagnosis of multiple sclerosis, along with the spinal
fluid analysis mentioned earlier. A wake-and-sleep EEG needs to be done
to exclude temporal lobe epilepsy. If migraine or migraine equivalents are
suspected, perhaps a trial of beta-blockers would help make this diagnosis.
If vertebral–basilar artery ischemia is suspected, MRA may be indicated.
If the dizziness is not true vertigo, a CBC and chemistry panel, thyroid
profile, and 5-hour glucose tolerance test should be done at the outset.
Additional studies in the form of 24-hour blood pressure monitoring,
Holter monitoring, and echocardiography all have a valuable place in the
diagnostic workup of dizziness without true vertigo. Perhaps a tilt table
test should be ordered to rule out orthostatic hypotension. However,
referral to a cardiologist is wise before undertaking these expensive
studies. If all studies are negative, perhaps a psychiatrist should be
consulted.
