ASK THE FOLLOWING QUESTIONS
1. Is there a positive drug or alcohol history? It is well known that
alcohol can cause diarrhea, as do drugs in common use, such as
digitalis, diuretics, beta-blockers, aspirin, colchicine, and other
nonsteroidal anti-inflammatory drugs. Perhaps there is overuse of
laxatives. Remember, patients may lie about the use of laxatives.
2. Is there blood in the stool? Blood in the stool certainly is significant
for ulcerative colitis, Crohn’s disease, carcinoma, and diverticulitis,
but it is also found in amebiasis and Zollinger–Ellison syndrome.
3. Is there a lot of mucus in the stool? Mucus is often found in
ulcerative colitis, Crohn’s disease, and irritable bowel syndrome.
4. Is there evidence of steatorrhea? Large volumes of stools that are
partially formed or formed and float in the commode suggest
steatorrhea. Stool analysis can be done, as is discussed later.
5. Is there an abdominal mass? A mass in the right lower quadrant
would suggest carcinoma or diverticulitis. Tenderness in the left lower
quadrant with or without a significant mass would be suggestive of
ulcerative colitis, diverticulitis, and irritable bowel syndrome. A mass
in the area of the ascending or descending colon or the transverse
colon should also be looked for, as these would suggest carcinoma.
6. Are there signs of systemic disease? Many systemic diseases may
cause diarrhea. Among them are thyrotoxicosis, in which case one
would be looking for a thyroid tumor and a tremor and tachycardia;
carcinoid syndrome, which would cause considerable flushing;
Addison’s disease, which would cause hyperpigmentation of the skin;
and pellagra, which may cause dermatitis and dementia.
7. Does significant diarrhea persist on fasting? Diarrhea that persists
after fasting suggests a secretory diarrhea from a polypeptide-secreting
tumor, such as villous adenoma, a gastrinoma, or a carcinoid tumor.
DIAGNOSTIC WORKUP
Most patients will be diagnosed by a stool culture, stool for occult blood,
and stool for ovum and parasites, along with sigmoidoscopy and barium
enema. Colonoscopy has largely replaced the latter two procedures. Serum
lactoferrin and calprotectin will distinguish inflammatory bowel disease
from irritable bowel syndrome. Giardiasis may be best diagnosed by the
finding of Giardia antigen in the stool. Perform a hydrogen breath test if
lactose intolerance is suspected. In patients who have been on antibiotics,
the stool should be tested for C. difficile toxin B. If a systemic disease is
suspected, CBC, sedimentation rate, chemistry panel, and thyroid profile
should be done. An HIV antibody test may be indicated depending on the
history. A urine test for 5-HIAA will uncover a carcinoid syndrome. A
serum gastrin will usually reveal a gastrinoma. Perinuclear staining
antineutrophil cytoplasmic antibodies (p-ANCA) are found in 70% of
patients with ulcerative colitis. If these tests do not provide a diagnosis, the
most cost-effective approach at this point is to refer the patient to a
gastroenterologist who will undoubtedly perform a colonoscopy as part of
the workup. Small bowel aspiration and biopsy will be useful in
diagnosing Giardia infection or celiac sprue; angiography will confirm
mesenteric ischemia or infarcts. A swallowed string test may pick up
Giardia, but when all else fails, a trial of metronidazole will be diagnostic.
A course of metronidazole will also help diagnose C. difficile colitis.
If a gastroenterologist is not available, the clinician may proceed with a
quantitative 24-hour stool analysis for fat. If there is 10 g or more of fat in
the stool in a day, then steatorrhea can be diagnosed and one can proceed
with the workup of steatorrhea (page 482). If there is less than 7 g of fat
per day in the stool, the stool volume after fasting should be done. If it is
large and we have ruled out surreptitious laxative abuse, a polypeptide-
secreting tumor should be considered. Here again, it would be best to refer
the patient to a gastroenterologist. If the volume after a fast is small, the
problem is most likely lactose or other food intolerance or an irritable
bowel syndrome. Occasionally, the problem is dysfunction of the anal
sphincter. Once again, a GI specialist is probably best consulted for
workup of a dysfunctional anal sphincter.
