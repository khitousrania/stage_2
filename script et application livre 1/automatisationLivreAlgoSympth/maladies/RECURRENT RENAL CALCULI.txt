ASK THE FOLLOWING QUESTIONS
1. Is there a history of joint pain? This would suggest primary or
secondary gout (polycythemia, leukemia).
2. Is there hypercalcemia? This finding would suggest
hyperparathyroidism, hypervitaminosis-D, or milk-alkali syndrome.
3. Is there a fine tremor associated with tachycardia? Look for
hyperthyroidism.
4. Is there a flank mass? Look for hypernephroma, obstructive uropathy
or a congenital anomaly of the kidney, horse-shoe kidney, etc.
DIAGNOSTIC WORKUP
Routine laboratory including a CBC, sedimentation rate, urinalysis and
culture, CMP, thyroid profile, and vitamin D3 levels should be done. The
uric acid is not usually included in the CMP so it must be ordered
separately. A CT scan of the abdomen and neck with contrast should be
done in any case with recurrent calculi. Analysis of the stone and a urology
consult are usually necessary.
