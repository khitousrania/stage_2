ASK THE FOLLOWING QUESTIONS
1. What is the level of creatine phosphokinase isoenzyme containing
M and B subunits (MB-CPK)? An elevated MB-CPK combined
with elevated LDH is most likely because of a myocardial infarction,
although myocarditis and pericarditis can produce a similar picture.
2. What is the AST level? If the MB-CPK or CPK are not also
increased, an elevation of both the LDH and AST would point to liver
disease.
3. What are the serum and urine creatine levels? If these are elevated,
muscle disease must be considered likely.
4. What does the lung scan show? If positive, a lung scan separates
pulmonary infarction from the other conditions in this group.
DIAGNOSTIC WORKUP
The diagnostic workup should include a CBC, urinalysis, chemistry panel,
sedimentation rate, CRP, ANA, urine and serum creatine, urine
myoglobin, serial EKGs, blood gas analysis, LDH isoenzymes, troponin C,
a chest x-ray, lung scan, EMG, and cardiology and neurology consults. A
liver scan, CT scan of the abdomen and liver, and muscle biopsy may be
necessary.
