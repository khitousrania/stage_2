ASK THE FOLLOWING QUESTIONS
1. Are there abnormalities on the vaginal examination? Imperforated
hymen, a mass in the cul-de-sac, a retroverted uterus, pregnancy, PID,
and cystitis are just a few of the conditions that might be found.
2. Are there abnormalities on the rectal examination? The rectal
examination may disclose anal fissures, hemorrhoids, or perirectal
abscess.
3. Are there abnormalities of the secondary sexual characteristics?
Turner’s syndrome and testicular feminization are two of the
conditions that may be associated with these abnormalities.
4. Is there a history of emotional trauma? Childhood sexual
molestations and marital difficulties are among the conditions that
may be found on a careful history.
DIAGNOSTIC WORKUP
First, one should do a good pelvic and rectal examination. If abnormalities
are found on these examinations, referral to a gynecologist or a
proctologist can be made. If the pelvic and rectal examinations are normal,
the patient should probably be referred to a psychiatrist or psychologist for
treatment. If there are abnormalities of the secondary sexual
characteristics, the clinician may undertake studies of these disorders, but
referral to an endocrinologist is probably more cost-effective.
