ASK THE FOLLOWING QUESTIONS
1. Is the rate normal, rapid, or slow? A rapid rate would suggest
supraventricular tachycardia, ventricular tachycardia, atrial flutter, and
fibrillation. A slow rate would suggest atrioventricular (AV) nodal
rhythm, third-degree AV block, sinoatrial block, Wenckebach
phenomena, and sick sinus syndrome. A normal rate would suggest
atrial and ventricular premature contractions, AV dissociation,
bigeminal rhythm, and controlled flutter or fibrillation.
2. If the rate is rapid, is it regular or irregular? A rapid regular rate
would suggest supraventricular tachycardia or ventricular tachycardia,
whereas a rapid irregular rate would suggest atrial flutter or
fibrillation.
3. If the rate is slow, is it regular or irregular? A slow regular rhythm
would suggest AV nodal rhythm, third-degree AV block, or sinoatrial
block. A slow irregular rhythm would suggest Wenckebach
phenomena or sick sinus syndrome.
DIAGNOSTIC WORKUP
Some physicians will want to refer the patient with a pulse irregularity to a
cardiologist at the outset. Other clinicians would rather investigate the
patient further.
Routine tests ordered include CBC, sedimentation rate, urinalysis,
chemistry panel, serial cardiac enzymes, thyroid profile, ANA test, EKG,
and chest x-ray with lateral and anterior oblique views. If there is fever, an
ASO titer or streptozyme test should be done to rule out rheumatic fever,
and blood cultures should be done to rule out bacterial endocarditis. If the
EKG is normal and the symptoms are intermittent, 24-hour Holter
monitoring should be done. The patient also can be hospitalized for a few
days for telemetry. Newer technology includes using a continuous-loop
event recorder to allow monitoring for 2 weeks at a time.
Echocardiography will disclose valvular diseases and myocardiopathies.
His bundle studies may be necessary in some cases. Also, angiography and
catheterization studies should be considered in difficult cases. Before
expensive tests are ordered, it is wise to consult a cardiologist.
