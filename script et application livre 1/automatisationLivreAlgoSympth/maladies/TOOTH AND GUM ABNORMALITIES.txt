GENERAL INFORMATION
As physicians, we often neglect inspection of the teeth. We seem to expect
the dentist to do this part of the examination for us. Multiple cavities are
found in diabetes mellitus, pernicious anemia, and multiparous women.
Separation of the teeth may be a clue to hypopituitarism, whereas teeth
that taper to a thin edge are typical of the screwdriver appearance of
Hutchinson teeth in congenital syphilis. Who has not heard of the dramatic
gum hypertrophy associated with phenytoin use in epileptic children? In
scurvy, the gums become swollen, and the teeth get loose, drop out, or
become misaligned. In dental ectodermal dysplasia, the teeth may be
partially or completely absent, and, at times, there is no evidence of
enamel formation. The dark blue line positioned where the gums meet the
teeth is a sign of lead intoxication.
Mothers frequently complain that their children grind their teeth, but
this is rarely of pathologic significance. Of course, it may be a sign of
malocclusion and temporomandibular joint syndrome, especially in adults.
Yellow teeth is a sign that a child has been affected by tetracyclines
either in utero or early childhood.
