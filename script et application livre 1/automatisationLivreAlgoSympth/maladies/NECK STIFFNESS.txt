ASK THE FOLLOWING QUESTIONS
1. Is it acute or chronic? Acute stiffness of the neck should make one
look for nuchal rigidity. If there is nuchal rigidity, meningitis or
subarachnoid hemorrhage would be high on the list of possibilities. If
there is chronic neck stiffness, one should consider rheumatoid
arthritis, cervical spondylosis, and idiopathic torticollis. With a history
of trauma, the possibility of flexion–extension injury and fracture is
more likely.
2. Is there nuchal rigidity or fever? The presence of nuchal rigidity or
fever should make one think of meningitis, subarachnoid hemorrhage,
or meningismus due to some systemic infectious disease.
Retropharyngeal abscess must also be considered.
3. Is it congenital or acquired? The presence of congenital stiffness of
the neck should make one think of congenital torticollis or Klippel–
Feil syndrome. Chronic acquired neck stiffness should make one think
of cervical spondylosis, Parkinson’s disease, idiopathic torticollis,
rheumatoid arthritis, tuberculosis, fractures of the spine, flexion–
extension injuries, and inflammation of the lymph nodes.
4. Are there x-ray changes? Plain films of the cervical spine will often
reveal cervical spondylosis, fractures, and tuberculosis. However, one
should not jump to the conclusion that this is the cause of the
condition.
DIAGNOSTIC WORKUP
If there is nuchal rigidity and fever, a CT scan of the brain should be done
to rule out a space-occupying lesion, and, following that, a spinal tap for
analysis, smear, and culture should be done.
If there is no nuchal rigidity or fever, plain films of the cervical spine
are a good place to start the diagnostic workup. A CBC, sedimentation
rate, urinalysis, chemistry panel, and arthritis profile may also be helpful.
If the stiffness is associated with pain radiating into the upper extremities,
EMG and nerve conduction velocity studies may be useful. If the stiffness
persists, an MRI of the cervical spine may be necessary. A bone scan may
identify a subtle fracture or osteomyelitis. A neurologic specialist should
be consulted before ordering expensive diagnostic tests.
