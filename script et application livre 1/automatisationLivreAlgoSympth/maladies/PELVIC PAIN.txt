CASE HISTORY
A 39-year-old female, gravid 3, para 2, presents to your office with a
history of recurrent hypogastric pain. Following the algorithm, you look
for a vaginal discharge and fever. These are absent. There is a history of
heavy periods and on examination, she had an enlarged asymmetrical
uterus. The pregnancy test is negative so, you suspect either uterine
fibroids or endometriosis. Pelvic ultrasound confirms the diagnosis of
uterine fibroids.
ASK THE FOLLOWING QUESTIONS
1. Is there a pelvic mass? The presence of a pelvic mass would suggest
salpingo-oophoritis, ectopic pregnancy, endometriosis, uterine fibroid,
or an ovarian tumor that is twisting on its pedicle. Be sure to do a
rectovaginal examination as there may be a mass or fluid in the cul-de-
sac.
2. Is there fever or purulent vaginal discharge? The presence of fever
or purulent vaginal discharge would suggest PID, diverticulitis, and
appendicitis.
3. Is there a history of metrorrhagia or menorrhagia? The history of
metrorrhagia or menorrhagia would suggest ectopic pregnancy,
threatened abortion, retained secundinae, uterine fibroids, and
endometriosis.
4. Is there a positive pregnancy test? The presence of a positive
pregnancy test would suggest an ectopic pregnancy or threatened
abortion.
5. Is the pain related to the menstrual cycle? If the pain is related to
the menstrual cycle, mittelschmerz should be considered.
DIAGNOSTIC WORKUP
Routine studies include a CBC, sedimentation rate, pregnancy test,
urinalysis, urine culture, chemistry panel, VDRL test, and Pap smear. A
vaginal smear and culture should also be done routinely.
The next step would logically be a pelvic or transvaginal ultrasound,
but it is wise to consult a gynecologist before ordering expensive tests. The
gynecologist may proceed with laparoscopy, culdocentesis, and,
ultimately, an exploratory laparotomy. A CT scan of the abdomen and
pelvis may also be necessary. If there is fever, a trial of antibiotics may be
appropriate even if the workup is negative.
