import csv
import json 
import copy 

maladie_dict_helper ={}

listingMaladie =[]

listeMaladie=open('listeMaladie.txt','r+',encoding='utf-8')

csV = open('liste.csv','w+',encoding='utf-8')

#Cette fonction sert a stocker la liste de maladies dans listingMaladie et a  remplir le csv avec la liste des maladies dans le fichier listeMaladienew

def remlirCSV():
    for ligne in listeMaladie:
        ligneLu = ligne.strip()
        if(ligneLu =='\n' or ligneLu == '' ):
            pass
        else:
            csV.write(ligneLu.upper()+'\n')
            listingMaladie.append(ligneLu.upper()+'\n') 
remlirCSV()

#Keywords ici stocke la liste des maladie obtenu aprés l'execution de remplirCSV()
keyWords = listingMaladie    

fileExample =open('maladieDetails.txt','r+',encoding="utf-8")


def lectureMaladie(i):
    f = open("maladies/"+keyWords[i-1][:-1]+'.txt','r+',encoding="utf-8")
    file1 = f.read();
    sentences =file1.split(keyWords[i])
    f.truncate(0)
    f.seek(0,0)
    maladie_dict_helper[keyWords[i-1][:-1]]=sentences[0]
    f.write(sentences[0])
    file2 = open("maladies/"+keyWords[i][:-1]+'.txt','w+',encoding="utf-8")
    maladie_dict_helper[keyWords[i][:-1]]=sentences[1]
    file2.write(sentences[1])

def lectureFichier(): 
    fileMaladie1=open("maladies/"+keyWords[0][:-1]+'.txt','w+',encoding="utf-8")
    fileMaladie1.write(fileExample.read())
    fileMaladie1.seek(0,0)
    for i in range(1,len(keyWords)):
        lectureMaladie(i)
        if(i ==len(keyWords)):
            break
            
lectureFichier()
#Decoupage avec ['CASE HISTORY','ASK THE FOLLOWING QUESTIONS','DIAGNOSTIC WORKUP','GENERAL INFORMATION']

fileMaladie = open('healpersDocDontDelete/maladie.txt','w+',encoding='utf-8',errors='ignore')

keywords =['CASE HISTORY','ASK THE FOLLOWING QUESTIONS','DIAGNOSTIC WORKUP','GENERAL INFORMATION']


dict_final_globalJson_Maladie ={}  #stocker la clé et son contenu 

def lecture(key):
    dict_globalJson_Maladie ={}        #stocker pour une seule maladie les clé 
    fileMaladie.write(maladie_dict_helper[key])
    fileMaladie.seek(0,0)
    s = fileMaladie.read()
    fileMaladie.truncate()
    fileMaladie.seek(0,0)
    if(keywords[len(keywords)-1] in s ):  #test si general 
        sentences = s.split(keywords[len(keywords)-1])
        dict_globalJson_Maladie[keywords[len(keywords)-1]]=sentences[1]
    elif(keywords[len(keywords)-2] in s): #test pour diagnostic
        sentences =s.split(keywords[len(keywords)-2])
        dict_globalJson_Maladie[keywords[len(keywords)-2]]=sentences[1]
        s= sentences[0]  
        if(keywords[len(keywords)-3] in s): #test questions 
            sentences = s.split(keywords[len(keywords)-3])
            if(len(sentences) == 2): 
                dict_globalJson_Maladie[keywords[len(keywords)-3]] = sentences[1]
                dict_globalJson_Maladie[keywords[len(keywords)-4]] =sentences[0]
            else:
                dict_globalJson_Maladie[keywords[len(keywords)-3]] =s
    elif(keywords[len(keywords)-3] in s):
        sentences =s.split(keywords[len(keywords)-3])
        dict_globalJson_Maladie[keywords[len(keywords)-1]] = sentences[1]
    dict_final_globalJson_Maladie[key]=dict_globalJson_Maladie
    fileMaladie.truncate()
    fileMaladie.seek(0,0)
    

# lectureGeneral s'occupe de passer clé par clé(maladie par maladie) a lecture qui elle va découper le detail de la maladie
def lectureGeneral():
    for key in maladie_dict_helper:
        lecture(key)
            
lectureGeneral()

with open('visualiserDicoJson/dict_globalJson_Maladie.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_final_globalJson_Maladie, fp, indent=4)

#part 2: diagnostic partage 
dict_decoupeQuestComm ={}

# PART 3 :dévision questions et commentaires
keywordSpec =['INCREASED','DECREASED']

tab_summary_id =[]

def complexQuestionCommentaire(value,s,dict_helper,file,alpha,b,key,k):
    key+=1
    file.seek(0,0)
    file.truncate()
    i=2
    if('?' in s):
        file.write(s)
        file.seek(0,0)
        s =file.read() 
        sentences =s.split('1.') 
        while (sentences[0]!=s) :
            sentences2=sentences[1].split('?')   
            if(len(sentences2) < 2):
                dict_helper[value[0]+str(key)+'_'+str(alpha)]=sentences2[0]
            else:
                sentencescommet = sentences2[1].split(str(i+1)+'.')
                dict_helper[value[0]+str(key)+'_'+str(alpha)]=sentences2[0]+'?'
                dict_helper['comment_'+value[0]+str(key)+'_'+str(alpha)] =sentencescommet[0].split(str(i)+'.')[0]
            alpha+=1
            sentences =s.split(str(i)+'.')  
            i+=1
    elif(b==True):
        dict_helper[value[0]+str(key)+'_'+str(alpha)]=s  #decr_00
    else:
        dict_helper[value[0]+str(key)+'_'+str(alpha)]=s
    dict_decoupeQuestComm[value[0]+str(k)] =dict_helper
    #appel a celle qui va découper et remplir les diagnostic
    dict_decoupeQuestComm[value[0]+str(k)]['CASE HISTORY_'+value[0]+str(key)] =dict_final_globalJson_Maladie[value]['CASE HISTORY']
    # decoupeDiagnostic(key,value,dict_final_globalJson_Maladie[value]['DIAGNOSTIC WORKUP'],k)
    dict_decoupeQuestComm[value[0]+str(k)]['summary_comment_'+str(key)]=dict_final_globalJson_Maladie[value]['DIAGNOSTIC WORKUP']
    tab_summary_id.append(str(key))
    file.seek(0,0)
    file.truncate()
    return alpha


def deviserQuestionCommentaire(key,value,k): 
    b =False
    alpha=1
    dict_helper ={}
    file = open('healpersDocDontDelete/questionsReponseAide.txt','w+',encoding='utf-8',errors='ignore')
    if('ASK THE FOLLOWING QUESTIONS' in dict_final_globalJson_Maladie[value]):
        file.write(dict_final_globalJson_Maladie[value]['ASK THE FOLLOWING QUESTIONS'])
        file.seek(0,0)
        s =file.read() 
        if(keywordSpec[0] in s or keywordSpec[1] in s):
            b=True
            sentences = s.split(keywordSpec[1])
            file.seek(0,0)
            file.truncate()
            alpha =complexQuestionCommentaire(value,sentences[0],dict_helper,file,alpha-1,b,key,k)
            complexQuestionCommentaire(value,sentences[1],dict_helper,file,alpha,b,key,k)
        else:
            complexQuestionCommentaire(value,s,dict_helper,file,0,b,key,k)
    elif('GENERAL INFORMATION' in dict_final_globalJson_Maladie[value]):
        dict_decoupeQuestComm[value[0]+str(k)] ={}
        dict_decoupeQuestComm[value[0]+str(k)][value[0]+str(key+1)+'_'+str(alpha-1)] =dict_final_globalJson_Maladie[value]['GENERAL INFORMATION']
        dict_decoupeQuestComm[value[0]+str(k)]['summary_comment_'+str(key+1)]='This disease has no diagnosis'
    file.seek(0,0)
    file.truncate()

def generalDeviserQuestionCommentaire():
    k=0
    for key,value in enumerate(dict_final_globalJson_Maladie):
        k+=1
        deviserQuestionCommentaire(key,value,k)
        

generalDeviserQuestionCommentaire()

#PART 4 je vais travailler avec dict_decoupeQuestComm qui stocke les questions, commentaires et summary

with open('healpersDocDontDelete/basicJsonSansQstCom.json', 'r+',errors='ignore',encoding='utf-8') as f:
    dict_basicSansQstCom = json.load(f)

dict_keyMaladie ={}
def ajoutListeMaladies():
    j = 0
    for key in listingMaladie:
        j+=1
        dict_basicSansQstCom['keys'][key[0]+str(j)]=key[:-1]
        dict_keyMaladie[key[0]+str(j)]=key[:-1]

ajoutListeMaladies()


def remplissageFinal(key):
    for keyMaladie in dict_decoupeQuestComm[key]:
        dict_basicSansQstCom['keys'][keyMaladie]=dict_decoupeQuestComm[key][keyMaladie]

alphabets =['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',  'R', 'S', 'T', 'U', 'V', 'W']

def remlissageFinalGeneral():
    dict_basicSansQstCom['keys']['question']="What's the first letter of the disease that you are looking for?"
    for key ,alphabet in enumerate(alphabets):
        dict_basicSansQstCom['keys'][str(alphabet.upper())]= " "+alphabet.upper()
    for key in dict_decoupeQuestComm:
        remplissageFinal(key)
    
    
remlissageFinalGeneral()

dict_decoupagekeyParAlphab={}
listingFourchetteNomMaladies=[]

def remplissageFourchette(keyAlphabet):
    s =[]
    for key in dict_keyMaladie:
        if(key[0]==alphabets[keyAlphabet].upper()):
            s.append(key)
    dict_decoupagekeyParAlphab[str(s[0])+'-'+s[len(s)-1]]='Diseases that begin with the letter '+str(alphabets[keyAlphabet].upper())
    listingFourchetteNomMaladies.append(str(s[0])+'-'+s[len(s)-1])

def remplissageFourchetteGenral():
    for keyAlphabet ,alphabet in enumerate(alphabets):
        remplissageFourchette(keyAlphabet)

remplissageFourchetteGenral()

tab =[]
def ajoutFourchette():
    for key in dict_decoupagekeyParAlphab:
        dict_basicSansQstCom['keys'][key]= dict_decoupagekeyParAlphab[key]
        tab.append(key)

ajoutFourchette()

with open('en.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_basicSansQstCom, fp, indent=4)

with open('visualiserDicoJson/decoupeQuestComm.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupeQuestComm, fp, indent=4)

with open('visualiserDicoJson/keyMaladie.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_keyMaladie, fp, indent=4)

with open('visualiserDicoJson/decoupe.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupagekeyParAlphab, fp, indent=4)



    

print('en.json pret !')

#-----------------questions.ts--------------------------------------------------------

def healperWrite_DictKey(dicto_copy):
    s='{\n'
    for key in dicto_copy:
        if(key !='options' and key!='category' and key!='nextQuestionMap'and key!='scoreMap'):
            s =s +'\t'+ str(key) +':'+"'"+str(dicto_copy[key])+"'"+',\n'
        else:
            s =s  +'\t'+ str(key) +':'+str(dicto_copy[key])+',\n'
    fileTs.write(s[:-2]+'\n},')
    

#ouvrir le fichier ou se trouve la premiére partie du question.ts
fileTsBasique = open('healpersDocDontDelete/basicTSdebut.txt','r+',encoding='utf-8')

fileTs = open('questions.ts','w+',encoding='utf-8')

tab_decoupeFourchette=[]
with open('healpersDocDontDelete/basicTShealper.json', 'r+',errors='ignore',encoding='utf-8') as f:
    dict_basicTsHealper = json.load(f) 

def debutQuestion_Ts():
    fileTs.truncate()
    fileTs.seek(0,0)
    fileTs.write(fileTsBasique.read())
    fileTs.write('\n//*******************PARTIE QUESTOIN :PREMIERE LETTERE DE LA MALADIE************\n')
    dict_basicTsHealper['id']='question'
    dict_basicTsHealper['category']='CATEGORIES.PERSONAL'
    dict_basicTsHealper['text']= dict_basicTsHealper['id']
    dict_basicTsHealper['options']=alphabets
    for key in dict_decoupagekeyParAlphab:
        dict_basicTsHealper['nextQuestionMap'].append(key)
        tab_decoupeFourchette.append(key)
    for i in range(0,len(dict_basicTsHealper['options'])):
        dict_basicTsHealper['scoreMap'].append(0)
    healperWrite_DictKey(dict_basicTsHealper) 
    
def vidage_dict_basicTsHealper():
    for key in dict_basicTsHealper:
        if(key !="options" and key !="nextQuestionMap" and key !="scoreMap"):
            dict_basicTsHealper[key]=''
        else:
            dict_basicTsHealper[key]=[]
            
tab_question1Formu=[]
tab_question1Formu_copieAll=[]
def recupToutePremiereKey(maladie):
    tab_question1Formu_copieAll=[]
    tab_question1Formu_copieAll=[next(iter(dict_decoupeQuestComm[maladie].keys()))]
    return tab_question1Formu_copieAll

def recupPremiereKeyQuestion(maladie):
    tab_question1Formu=[]
    tab_question1Formu_copieAll =recupToutePremiereKey(maladie)
    for key in tab_question1Formu_copieAll:
        if(key[0]==maladie[0]):
            tab_question1Formu.append(key)
        else:
            pass
    return tab_question1Formu

def ajoutKeyDetailFourchette(id,nb1,nb2):
    vidage_dict_basicTsHealper()
    fileTs.write('\n//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE'+str(id)+'\n')
    dict_basicTsHealper['id']=id
    dict_basicTsHealper['category']='CATEGORIES.PERSONAL'
    dict_basicTsHealper['text']= dict_basicTsHealper['id']
    for i in range(nb1,nb2+1):
        dict_basicTsHealper['options'].append(id[0]+str(i))
    for maladie in dict_basicTsHealper['options']:
        tab_question1Formu =recupPremiereKeyQuestion(maladie)
        dict_basicTsHealper['nextQuestionMap'].append(tab_question1Formu[0])  
    for j in range(0,len(dict_basicTsHealper['options'])):#------------------------------aaaaaaaaaaaaa
        dict_basicTsHealper['scoreMap'].append(0) 
    dict_basicTsHealper['inputType']= 'radio'
    healperWrite_DictKey(dict_basicTsHealper)


# Ces deux fonctions ont pour but d'ajouter le detail la premiere qst du formu a afficher aprés le choix de maladie
def fourchetteEnumeration(id):
    sentences=id.split('-')
    s1=sentences[0].split(id[0])
    s2=sentences[1].split(id[0])
    nb1 =s1[1]
    nb2=s2[1]
    tab_question1Formu=[]
    ajoutKeyDetailFourchette(id,int(nb1),int(nb2))

def generalFoucrchetteEnumeration():
    fileTs.write('\n//**********AJOUT DELIMITATION MALADIE ALPHABETS DE A.. a A..****************\n')
    for id in tab_decoupeFourchette:
        fourchetteEnumeration(id)
    

#----Partie question et son commentaire
tab_sauvegardeQuestComm=[]



with open('healpersDocDontDelete/dictMaladieTs.json', 'r+',errors='ignore',encoding='utf-8') as f:
    dictMaladieTs = json.load(f)

def maladieQuestionSuiv(key,maladie):
    for i in dict_decoupeQuestComm[maladie]:
        dict_copy =dictMaladieTs.copy()
        if('CASE HISTORY' in i or 'summary_comment' in i):
            pass
        else:
            sentencesAlpha =i.split('_')
            if(sentencesAlpha[0] == maladie):
                dict_copy['id']=i
                dict_copy['text']= i
                if('comment_'+str(i) in dict_decoupeQuestComm[maladie]):
                    dict_copy['comment']= 'comment_'+str(i)
                sentences = str(i).split('_')
                if((maladie+'_'+ str(int(sentences[1])+1)) in dict_decoupeQuestComm[maladie] ):
                    dict_copy['nextQuestionMap']=[]
                    dict_copy['nextQuestionMap'].append(maladie+'_'+ str(int(sentences[1])+1)) #oui il renvoie a la méme 
                    dict_copy['nextQuestionMap'].append(maladie+'_'+ str(int(sentences[1])+1)) #non il renvoie a la méme 
                else:
                    dict_copy['nextQuestionMap']=[]
                    #///////////////////////////////////////
                    dict_copy['nextQuestionMap'].append(str(key))
                dict_copy['scoreMap']=[1,0]
                healperWrite_DictKey(dict_copy)
        dict_copy={}

def generalMaladieQuestionSuiv():
    fileTs.write('\n//**********DETAIL QUESTION ET SA SUIVANTE DANS LE FORUM****************\n')
    for key,maladie in enumerate(dict_decoupeQuestComm):    
        maladieQuestionSuiv(key+1,maladie)
    fileTs.write('];')


    
   
print('questions.ts pret !')
if __name__ == '__main__':
    debutQuestion_Ts()
    generalFoucrchetteEnumeration()
    generalMaladieQuestionSuiv()
        


def automateFonctionApp():
    s='if('
    for k in range(0,len(tab_summary_id)):
        s = s +'nextQuestionId == '+ "'" +str(tab_summary_id[k]) + "'" +' ||'
    return s[:-2] +'){'

print(automateFonctionApp())

