import {
  Guard

} from './guard';


export type Question = {
  id: string;
  category: string;
  comment?: string;
  text: string;
  inputType: 'radio' | 'date' | 'checkbox' | 'postal';
  options?: string[] | CheckboxOption[];
  nextQuestionMap?: string | string[];
  scoreMap?: number[];
  guard?: Guard;
};

export type CheckboxOption = {
  label: string;
  id: string;
};

export const CATEGORIES = {
  PERSONAL: 'personalInfo',
  SYMPTOMS: 'symptoms',

};
export const NO_XML = 'X';
export const QUESTION = {
  POSTAL_CODE: 'V1',
  AGE: 'P0',
  ABOVE_65: 'P1',
  LIVING_SITUATION: 'P2',
  CARING: 'P3',
  WORKSPACE: 'P4',
  CONTACT_DATE: 'CZ',
  OUT_OF_BREATH: 'SB',
  SYMPTOM_DATE: 'SZ',
  DATA_DONATION: `${NO_XML}1`,
};

export const XML_ORDER = ['V', 'P', 'C', 'S', 'D', 'M'];
export const QUESTIONS: Question[] = [


//*******************PARTIE QUESTOIN :PREMIERE LETTERE DE LA MALADIE************
{
	id:'question',
	category:CATEGORIES.PERSONAL,
	text:'question',
	inputType:'radio',
	comment:'',
	options:['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'V', 'W'],
	nextQuestionMap:['A1-A30', 'B31-B41', 'C42-C63', 'D64-D83', 'E84-E96', 'F97-F117', 'G118-G123', 'H124-H163', 'I164-I174', 'J175-J179', 'K180-K182', 'L183-L190', 'M191-M200', 'N201-N212', 'O213-O216', 'P217-P257', 'R258-R272', 'S273-S300', 'T301-T316', 'U317-U320', 'V321-V326', 'W327-W328'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//**********AJOUT DELIMITATION MALADIE ALPHABETS DE A.. a A..****************

//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREA1-A30
{
	id:'A1-A30',
	category:CATEGORIES.PERSONAL,
	text:'A1-A30',
	inputType:'radio',
	comment:'',
	options:['A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9', 'A10', 'A11', 'A12', 'A13', 'A14', 'A15', 'A16', 'A17', 'A18', 'A19', 'A20', 'A21', 'A22', 'A23', 'A24', 'A25', 'A26', 'A27', 'A28', 'A29', 'A30'],
	nextQuestionMap:['A1_0', 'A2_0', 'A3_0', 'A4_0', 'A5_0', 'A6_0', 'A7_0', 'A8_0', 'A9_0', 'A10_0', 'A11_0', 'A12_0', 'A13_0', 'A14_0', 'A15_0', 'A16_0', 'A17_0', 'A18_0', 'A19_0', 'A20_0', 'A21_0', 'A22_0', 'A23_0', 'A24_0', 'A25_0', 'A26_0', 'A27_0', 'A28_0', 'A29_0', 'A30_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREB31-B41
{
	id:'B31-B41',
	category:CATEGORIES.PERSONAL,
	text:'B31-B41',
	inputType:'radio',
	comment:'',
	options:['B31', 'B32', 'B33', 'B34', 'B35', 'B36', 'B37', 'B38', 'B39', 'B40', 'B41'],
	nextQuestionMap:['B31_0', 'B32_0', 'B33_0', 'B34_0', 'B35_0', 'B36_0', 'B37_0', 'B38_0', 'B39_0', 'B40_0', 'B41_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREC42-C63
{
	id:'C42-C63',
	category:CATEGORIES.PERSONAL,
	text:'C42-C63',
	inputType:'radio',
	comment:'',
	options:['C42', 'C43', 'C44', 'C45', 'C46', 'C47', 'C48', 'C49', 'C50', 'C51', 'C52', 'C53', 'C54', 'C55', 'C56', 'C57', 'C58', 'C59', 'C60', 'C61', 'C62', 'C63'],
	nextQuestionMap:['C42_0', 'C43_0', 'C44_0', 'C45_0', 'C46_0', 'C47_0', 'C48_0', 'C49_0', 'C50_0', 'C51_0', 'C52_0', 'C53_0', 'C54_0', 'C55_0', 'C56_0', 'C57_0', 'C58_0', 'C59_0', 'C60_0', 'C61_0', 'C62_0', 'C63_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRED64-D83
{
	id:'D64-D83',
	category:CATEGORIES.PERSONAL,
	text:'D64-D83',
	inputType:'radio',
	comment:'',
	options:['D64', 'D65', 'D66', 'D67', 'D68', 'D69', 'D70', 'D71', 'D72', 'D73', 'D74', 'D75', 'D76', 'D77', 'D78', 'D79', 'D80', 'D81', 'D82', 'D83'],
	nextQuestionMap:['D64_0', 'D65_0', 'D66_0', 'D67_0', 'D68_0', 'D69_0', 'D70_0', 'D71_0', 'D72_0', 'D73_0', 'D74_0', 'D75_0', 'D76_0', 'D77_0', 'D78_0', 'D79_0', 'D80_0', 'D81_0', 'D82_0', 'D83_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREE84-E96
{
	id:'E84-E96',
	category:CATEGORIES.PERSONAL,
	text:'E84-E96',
	inputType:'radio',
	comment:'',
	options:['E84', 'E85', 'E86', 'E87', 'E88', 'E89', 'E90', 'E91', 'E92', 'E93', 'E94', 'E95', 'E96'],
	nextQuestionMap:['E84_0', 'E85_0', 'E86_0', 'E87_0', 'E88_0', 'E89_0', 'E90_0', 'E91_0', 'E92_0', 'E93_0', 'E94_0', 'E95_0', 'E96_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREF97-F117
{
	id:'F97-F117',
	category:CATEGORIES.PERSONAL,
	text:'F97-F117',
	inputType:'radio',
	comment:'',
	options:['F97', 'F98', 'F99', 'F100', 'F101', 'F102', 'F103', 'F104', 'F105', 'F106', 'F107', 'F108', 'F109', 'F110', 'F111', 'F112', 'F113', 'F114', 'F115', 'F116', 'F117'],
	nextQuestionMap:['F97_0', 'F98_0', 'F99_0', 'F100_0', 'F101_0', 'F102_0', 'F103_0', 'F104_0', 'F105_0', 'F106_0', 'F107_0', 'F108_0', 'F109_0', 'F110_0', 'F111_0', 'F112_0', 'F113_0', 'F114_0', 'F115_0', 'F116_0', 'F117_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREG118-G123
{
	id:'G118-G123',
	category:CATEGORIES.PERSONAL,
	text:'G118-G123',
	inputType:'radio',
	comment:'',
	options:['G118', 'G119', 'G120', 'G121', 'G122', 'G123'],
	nextQuestionMap:['G118_0', 'G119_0', 'G120_0', 'G121_0', 'G122_0', 'G123_0'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREH124-H163
{
	id:'H124-H163',
	category:CATEGORIES.PERSONAL,
	text:'H124-H163',
	inputType:'radio',
	comment:'',
	options:['H124', 'H125', 'H126', 'H127', 'H128', 'H129', 'H130', 'H131', 'H132', 'H133', 'H134', 'H135', 'H136', 'H137', 'H138', 'H139', 'H140', 'H141', 'H142', 'H143', 'H144', 'H145', 'H146', 'H147', 'H148', 'H149', 'H150', 'H151', 'H152', 'H153', 'H154', 'H155', 'H156', 'H157', 'H158', 'H159', 'H160', 'H161', 'H162', 'H163'],
	nextQuestionMap:['H124_0', 'H125_0', 'H126_0', 'H127_0', 'H128_0', 'H129_0', 'H130_0', 'H131_0', 'H132_0', 'H133_0', 'H134_0', 'H135_0', 'H136_0', 'H137_0', 'H138_0', 'H139_0', 'H140_0', 'H141_0', 'H142_0', 'H143_0', 'H144_0', 'H145_0', 'H146_0', 'H147_0', 'H148_0', 'H149_0', 'H150_0', 'H151_0', 'H152_0', 'H153_0', 'H154_0', 'H155_0', 'H156_0', 'H157_0', 'H158_0', 'H159_0', 'H160_0', 'H161_0', 'H162_0', 'H163_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREI164-I174
{
	id:'I164-I174',
	category:CATEGORIES.PERSONAL,
	text:'I164-I174',
	inputType:'radio',
	comment:'',
	options:['I164', 'I165', 'I166', 'I167', 'I168', 'I169', 'I170', 'I171', 'I172', 'I173', 'I174'],
	nextQuestionMap:['I164_0', 'I165_0', 'I166_0', 'I167_0', 'I168_0', 'I169_0', 'I170_0', 'I171_0', 'I172_0', 'I173_0', 'I174_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREJ175-J179
{
	id:'J175-J179',
	category:CATEGORIES.PERSONAL,
	text:'J175-J179',
	inputType:'radio',
	comment:'',
	options:['J175', 'J176', 'J177', 'J178', 'J179'],
	nextQuestionMap:['J175_0', 'J176_0', 'J177_0', 'J178_0', 'J179_0'],
	scoreMap:[0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREK180-K182
{
	id:'K180-K182',
	category:CATEGORIES.PERSONAL,
	text:'K180-K182',
	inputType:'radio',
	comment:'',
	options:['K180', 'K181', 'K182'],
	nextQuestionMap:['K180_0', 'K181_0', 'K182_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREL183-L190
{
	id:'L183-L190',
	category:CATEGORIES.PERSONAL,
	text:'L183-L190',
	inputType:'radio',
	comment:'',
	options:['L183', 'L184', 'L185', 'L186', 'L187', 'L188', 'L189', 'L190'],
	nextQuestionMap:['L183_0', 'L184_0', 'L185_0', 'L186_0', 'L187_0', 'L188_0', 'L189_0', 'L190_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREM191-M200
{
	id:'M191-M200',
	category:CATEGORIES.PERSONAL,
	text:'M191-M200',
	inputType:'radio',
	comment:'',
	options:['M191', 'M192', 'M193', 'M194', 'M195', 'M196', 'M197', 'M198', 'M199', 'M200'],
	nextQuestionMap:['M191_0', 'M192_0', 'M193_0', 'M194_0', 'M195_0', 'M196_0', 'M197_0', 'M198_0', 'M199_0', 'M200_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREN201-N212
{
	id:'N201-N212',
	category:CATEGORIES.PERSONAL,
	text:'N201-N212',
	inputType:'radio',
	comment:'',
	options:['N201', 'N202', 'N203', 'N204', 'N205', 'N206', 'N207', 'N208', 'N209', 'N210', 'N211', 'N212'],
	nextQuestionMap:['N201_0', 'N202_0', 'N203_0', 'N204_0', 'N205_0', 'N206_0', 'N207_0', 'N208_0', 'N209_0', 'N210_0', 'N211_0', 'N212_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREO213-O216
{
	id:'O213-O216',
	category:CATEGORIES.PERSONAL,
	text:'O213-O216',
	inputType:'radio',
	comment:'',
	options:['O213', 'O214', 'O215', 'O216'],
	nextQuestionMap:['O213_0', 'O214_0', 'O215_0', 'O216_0'],
	scoreMap:[0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREP217-P257
{
	id:'P217-P257',
	category:CATEGORIES.PERSONAL,
	text:'P217-P257',
	inputType:'radio',
	comment:'',
	options:['P217', 'P218', 'P219', 'P220', 'P221', 'P222', 'P223', 'P224', 'P225', 'P226', 'P227', 'P228', 'P229', 'P230', 'P231', 'P232', 'P233', 'P234', 'P235', 'P236', 'P237', 'P238', 'P239', 'P240', 'P241', 'P242', 'P243', 'P244', 'P245', 'P246', 'P247', 'P248', 'P249', 'P250', 'P251', 'P252', 'P253', 'P254', 'P255', 'P256', 'P257'],
	nextQuestionMap:['P217_0', 'P218_0', 'P219_0', 'P220_0', 'P221_0', 'P222_0', 'P223_0', 'P224_0', 'P225_0', 'P226_0', 'P227_0', 'P228_0', 'P229_0', 'P230_0', 'P231_0', 'P232_0', 'P233_0', 'P234_0', 'P235_0', 'P236_0', 'P237_0', 'P238_0', 'P239_0', 'P240_0', 'P241_0', 'P242_0', 'P243_0', 'P244_0', 'P245_0', 'P246_0', 'P247_0', 'P248_0', 'P249_0', 'P250_0', 'P251_0', 'P252_0', 'P253_0', 'P254_0', 'P255_0', 'P256_0', 'P257_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRER258-R272
{
	id:'R258-R272',
	category:CATEGORIES.PERSONAL,
	text:'R258-R272',
	inputType:'radio',
	comment:'',
	options:['R258', 'R259', 'R260', 'R261', 'R262', 'R263', 'R264', 'R265', 'R266', 'R267', 'R268', 'R269', 'R270', 'R271', 'R272'],
	nextQuestionMap:['R258_0', 'R259_0', 'R260_0', 'R261_0', 'R262_0', 'R263_0', 'R264_0', 'R265_0', 'R266_0', 'R267_0', 'R268_0', 'R269_0', 'R270_0', 'R271_0', 'R272_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRES273-S300
{
	id:'S273-S300',
	category:CATEGORIES.PERSONAL,
	text:'S273-S300',
	inputType:'radio',
	comment:'',
	options:['S273', 'S274', 'S275', 'S276', 'S277', 'S278', 'S279', 'S280', 'S281', 'S282', 'S283', 'S284', 'S285', 'S286', 'S287', 'S288', 'S289', 'S290', 'S291', 'S292', 'S293', 'S294', 'S295', 'S296', 'S297', 'S298', 'S299', 'S300'],
	nextQuestionMap:['S273_0', 'S274_0', 'S275_0', 'S276_0', 'S277_0', 'S278_0', 'S279_0', 'S280_0', 'S281_0', 'S282_0', 'S283_0', 'S284_0', 'S285_0', 'S286_0', 'S287_0', 'S288_0', 'S289_0', 'S290_0', 'S291_0', 'S292_0', 'S293_0', 'S294_0', 'S295_0', 'S296_0', 'S297_0', 'S298_0', 'S299_0', 'S300_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRET301-T316
{
	id:'T301-T316',
	category:CATEGORIES.PERSONAL,
	text:'T301-T316',
	inputType:'radio',
	comment:'',
	options:['T301', 'T302', 'T303', 'T304', 'T305', 'T306', 'T307', 'T308', 'T309', 'T310', 'T311', 'T312', 'T313', 'T314', 'T315', 'T316'],
	nextQuestionMap:['T301_0', 'T302_0', 'T303_0', 'T304_0', 'T305_0', 'T306_0', 'T307_0', 'T308_0', 'T309_0', 'T310_0', 'T311_0', 'T312_0', 'T313_0', 'T314_0', 'T315_0', 'T316_0'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREU317-U320
{
	id:'U317-U320',
	category:CATEGORIES.PERSONAL,
	text:'U317-U320',
	inputType:'radio',
	comment:'',
	options:['U317', 'U318', 'U319', 'U320'],
	nextQuestionMap:['U317_0', 'U318_0', 'U319_0', 'U320_0'],
	scoreMap:[0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREV321-V326
{
	id:'V321-V326',
	category:CATEGORIES.PERSONAL,
	text:'V321-V326',
	inputType:'radio',
	comment:'',
	options:['V321', 'V322', 'V323', 'V324', 'V325', 'V326'],
	nextQuestionMap:['V321_0', 'V322_0', 'V323_0', 'V324_0', 'V325_0', 'V326_0'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIREW327-W328
{
	id:'W327-W328',
	category:CATEGORIES.PERSONAL,
	text:'W327-W328',
	inputType:'radio',
	comment:'',
	options:['W327', 'W328'],
	nextQuestionMap:['W327_0', 'W328_0'],
	scoreMap:[0, 0]
},
//**********DETAIL QUESTION ET SA SUIVANTE DANS LE FORUM****************
{
	id:'A1_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A1_0',
	inputType:'radio',
	comment:'comment_A1_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A1_1', 'A1_1'],
	scoreMap:[1, 0]
},{
	id:'A1_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A1_1',
	inputType:'radio',
	comment:'comment_A1_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A1_2', 'A1_2'],
	scoreMap:[1, 0]
},{
	id:'A1_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A1_2',
	inputType:'radio',
	comment:'comment_A1_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A1_3', 'A1_3'],
	scoreMap:[1, 0]
},{
	id:'A1_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A1_3',
	inputType:'radio',
	comment:'comment_A1_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A1_4', 'A1_4'],
	scoreMap:[1, 0]
},{
	id:'A1_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A1_4',
	inputType:'radio',
	comment:'comment_A1_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A1_5', 'A1_5'],
	scoreMap:[1, 0]
},{
	id:'A1_5',
	category:CATEGORIES.SYMPTOMS,
	text:'A1_5',
	inputType:'radio',
	comment:'comment_A1_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['1'],
	scoreMap:[1, 0]
},{
	id:'A2_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A2_0',
	inputType:'radio',
	comment:'comment_A2_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A2_1', 'A2_1'],
	scoreMap:[1, 0]
},{
	id:'A2_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A2_1',
	inputType:'radio',
	comment:'comment_A2_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A2_2', 'A2_2'],
	scoreMap:[1, 0]
},{
	id:'A2_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A2_2',
	inputType:'radio',
	comment:'comment_A2_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A2_3', 'A2_3'],
	scoreMap:[1, 0]
},{
	id:'A2_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A2_3',
	inputType:'radio',
	comment:'comment_A2_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A2_4', 'A2_4'],
	scoreMap:[1, 0]
},{
	id:'A2_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A2_4',
	inputType:'radio',
	comment:'comment_A2_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A2_5', 'A2_5'],
	scoreMap:[1, 0]
},{
	id:'A2_5',
	category:CATEGORIES.SYMPTOMS,
	text:'A2_5',
	inputType:'radio',
	comment:'comment_A2_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A2_6', 'A2_6'],
	scoreMap:[1, 0]
},{
	id:'A2_6',
	category:CATEGORIES.SYMPTOMS,
	text:'A2_6',
	inputType:'radio',
	comment:'comment_A2_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A2_7', 'A2_7'],
	scoreMap:[1, 0]
},{
	id:'A2_7',
	category:CATEGORIES.SYMPTOMS,
	text:'A2_7',
	inputType:'radio',
	comment:'comment_A2_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['2'],
	scoreMap:[1, 0]
},{
	id:'A3_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A3_0',
	inputType:'radio',
	comment:'comment_A3_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A3_1', 'A3_1'],
	scoreMap:[1, 0]
},{
	id:'A3_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A3_1',
	inputType:'radio',
	comment:'comment_A3_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A3_2', 'A3_2'],
	scoreMap:[1, 0]
},{
	id:'A3_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A3_2',
	inputType:'radio',
	comment:'comment_A3_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['3'],
	scoreMap:[1, 0]
},{
	id:'A4_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A4_0',
	inputType:'radio',
	comment:'comment_A4_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A4_1', 'A4_1'],
	scoreMap:[1, 0]
},{
	id:'A4_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A4_1',
	inputType:'radio',
	comment:'comment_A4_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A4_2', 'A4_2'],
	scoreMap:[1, 0]
},{
	id:'A4_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A4_2',
	inputType:'radio',
	comment:'comment_A4_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A4_3', 'A4_3'],
	scoreMap:[1, 0]
},{
	id:'A4_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A4_3',
	inputType:'radio',
	comment:'comment_A4_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A4_4', 'A4_4'],
	scoreMap:[1, 0]
},{
	id:'A4_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A4_4',
	inputType:'radio',
	comment:'comment_A4_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A4_5', 'A4_5'],
	scoreMap:[1, 0]
},{
	id:'A4_5',
	category:CATEGORIES.SYMPTOMS,
	text:'A4_5',
	inputType:'radio',
	comment:'comment_A4_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A4_6', 'A4_6'],
	scoreMap:[1, 0]
},{
	id:'A4_6',
	category:CATEGORIES.SYMPTOMS,
	text:'A4_6',
	inputType:'radio',
	comment:'comment_A4_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A4_7', 'A4_7'],
	scoreMap:[1, 0]
},{
	id:'A4_7',
	category:CATEGORIES.SYMPTOMS,
	text:'A4_7',
	inputType:'radio',
	comment:'comment_A4_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A4_8', 'A4_8'],
	scoreMap:[1, 0]
},{
	id:'A4_8',
	category:CATEGORIES.SYMPTOMS,
	text:'A4_8',
	inputType:'radio',
	comment:'comment_A4_8',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['4'],
	scoreMap:[1, 0]
},{
	id:'A5_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A5_0',
	inputType:'radio',
	comment:'comment_A5_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A5_1', 'A5_1'],
	scoreMap:[1, 0]
},{
	id:'A5_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A5_1',
	inputType:'radio',
	comment:'comment_A5_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A5_2', 'A5_2'],
	scoreMap:[1, 0]
},{
	id:'A5_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A5_2',
	inputType:'radio',
	comment:'comment_A5_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A5_3', 'A5_3'],
	scoreMap:[1, 0]
},{
	id:'A5_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A5_3',
	inputType:'radio',
	comment:'comment_A5_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['5'],
	scoreMap:[1, 0]
},{
	id:'A6_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A6_0',
	inputType:'radio',
	comment:'comment_A6_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A6_1', 'A6_1'],
	scoreMap:[1, 0]
},{
	id:'A6_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A6_1',
	inputType:'radio',
	comment:'comment_A6_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A6_2', 'A6_2'],
	scoreMap:[1, 0]
},{
	id:'A6_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A6_2',
	inputType:'radio',
	comment:'comment_A6_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['6'],
	scoreMap:[1, 0]
},{
	id:'A7_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A7_0',
	inputType:'radio',
	comment:'comment_A7_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A7_1', 'A7_1'],
	scoreMap:[1, 0]
},{
	id:'A7_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A7_1',
	inputType:'radio',
	comment:'comment_A7_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A7_2', 'A7_2'],
	scoreMap:[1, 0]
},{
	id:'A7_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A7_2',
	inputType:'radio',
	comment:'comment_A7_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['7'],
	scoreMap:[1, 0]
},{
	id:'A8_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A8_0',
	inputType:'radio',
	comment:'comment_A8_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A8_1', 'A8_1'],
	scoreMap:[1, 0]
},{
	id:'A8_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A8_1',
	inputType:'radio',
	comment:'comment_A8_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['8'],
	scoreMap:[1, 0]
},{
	id:'A9_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A9_0',
	inputType:'radio',
	comment:'comment_A9_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A9_1', 'A9_1'],
	scoreMap:[1, 0]
},{
	id:'A9_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A9_1',
	inputType:'radio',
	comment:'comment_A9_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A9_2', 'A9_2'],
	scoreMap:[1, 0]
},{
	id:'A9_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A9_2',
	inputType:'radio',
	comment:'comment_A9_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A9_3', 'A9_3'],
	scoreMap:[1, 0]
},{
	id:'A9_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A9_3',
	inputType:'radio',
	comment:'comment_A9_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['9'],
	scoreMap:[1, 0]
},{
	id:'A10_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A10_0',
	inputType:'radio',
	comment:'comment_A10_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A10_1', 'A10_1'],
	scoreMap:[1, 0]
},{
	id:'A10_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A10_1',
	inputType:'radio',
	comment:'comment_A10_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['10'],
	scoreMap:[1, 0]
},{
	id:'A11_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A11_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['11'],
	scoreMap:[1, 0]
},{
	id:'A12_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A12_0',
	inputType:'radio',
	comment:'comment_A12_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A12_1', 'A12_1'],
	scoreMap:[1, 0]
},{
	id:'A12_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A12_1',
	inputType:'radio',
	comment:'comment_A12_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['12'],
	scoreMap:[1, 0]
},{
	id:'A13_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A13_0',
	inputType:'radio',
	comment:'comment_A13_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A13_1', 'A13_1'],
	scoreMap:[1, 0]
},{
	id:'A13_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A13_1',
	inputType:'radio',
	comment:'comment_A13_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A13_2', 'A13_2'],
	scoreMap:[1, 0]
},{
	id:'A13_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A13_2',
	inputType:'radio',
	comment:'comment_A13_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A13_3', 'A13_3'],
	scoreMap:[1, 0]
},{
	id:'A13_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A13_3',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A13_4', 'A13_4'],
	scoreMap:[1, 0]
},{
	id:'A13_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A13_4',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['13'],
	scoreMap:[1, 0]
},{
	id:'A14_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A14_0',
	inputType:'radio',
	comment:'comment_A14_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A14_1', 'A14_1'],
	scoreMap:[1, 0]
},{
	id:'A14_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A14_1',
	inputType:'radio',
	comment:'comment_A14_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A14_2', 'A14_2'],
	scoreMap:[1, 0]
},{
	id:'A14_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A14_2',
	inputType:'radio',
	comment:'comment_A14_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['14'],
	scoreMap:[1, 0]
},{
	id:'A15_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A15_0',
	inputType:'radio',
	comment:'comment_A15_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A15_1', 'A15_1'],
	scoreMap:[1, 0]
},{
	id:'A15_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A15_1',
	inputType:'radio',
	comment:'comment_A15_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['15'],
	scoreMap:[1, 0]
},{
	id:'A16_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A16_0',
	inputType:'radio',
	comment:'comment_A16_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A16_1', 'A16_1'],
	scoreMap:[1, 0]
},{
	id:'A16_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A16_1',
	inputType:'radio',
	comment:'comment_A16_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A16_2', 'A16_2'],
	scoreMap:[1, 0]
},{
	id:'A16_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A16_2',
	inputType:'radio',
	comment:'comment_A16_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A16_3', 'A16_3'],
	scoreMap:[1, 0]
},{
	id:'A16_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A16_3',
	inputType:'radio',
	comment:'comment_A16_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A16_4', 'A16_4'],
	scoreMap:[1, 0]
},{
	id:'A16_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A16_4',
	inputType:'radio',
	comment:'comment_A16_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['16'],
	scoreMap:[1, 0]
},{
	id:'A17_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A17_0',
	inputType:'radio',
	comment:'comment_A17_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A17_1', 'A17_1'],
	scoreMap:[1, 0]
},{
	id:'A17_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A17_1',
	inputType:'radio',
	comment:'comment_A17_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['17'],
	scoreMap:[1, 0]
},{
	id:'A18_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A18_0',
	inputType:'radio',
	comment:'comment_A18_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A18_1', 'A18_1'],
	scoreMap:[1, 0]
},{
	id:'A18_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A18_1',
	inputType:'radio',
	comment:'comment_A18_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A18_2', 'A18_2'],
	scoreMap:[1, 0]
},{
	id:'A18_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A18_2',
	inputType:'radio',
	comment:'comment_A18_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A18_3', 'A18_3'],
	scoreMap:[1, 0]
},{
	id:'A18_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A18_3',
	inputType:'radio',
	comment:'comment_A18_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A18_4', 'A18_4'],
	scoreMap:[1, 0]
},{
	id:'A18_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A18_4',
	inputType:'radio',
	comment:'comment_A18_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['18'],
	scoreMap:[1, 0]
},{
	id:'A19_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A19_0',
	inputType:'radio',
	comment:'comment_A19_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A19_1', 'A19_1'],
	scoreMap:[1, 0]
},{
	id:'A19_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A19_1',
	inputType:'radio',
	comment:'comment_A19_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A19_2', 'A19_2'],
	scoreMap:[1, 0]
},{
	id:'A19_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A19_2',
	inputType:'radio',
	comment:'comment_A19_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A19_3', 'A19_3'],
	scoreMap:[1, 0]
},{
	id:'A19_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A19_3',
	inputType:'radio',
	comment:'comment_A19_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A19_4', 'A19_4'],
	scoreMap:[1, 0]
},{
	id:'A19_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A19_4',
	inputType:'radio',
	comment:'comment_A19_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A19_5', 'A19_5'],
	scoreMap:[1, 0]
},{
	id:'A19_5',
	category:CATEGORIES.SYMPTOMS,
	text:'A19_5',
	inputType:'radio',
	comment:'comment_A19_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['19'],
	scoreMap:[1, 0]
},{
	id:'A20_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A20_0',
	inputType:'radio',
	comment:'comment_A20_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A20_1', 'A20_1'],
	scoreMap:[1, 0]
},{
	id:'A20_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A20_1',
	inputType:'radio',
	comment:'comment_A20_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A20_2', 'A20_2'],
	scoreMap:[1, 0]
},{
	id:'A20_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A20_2',
	inputType:'radio',
	comment:'comment_A20_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A20_3', 'A20_3'],
	scoreMap:[1, 0]
},{
	id:'A20_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A20_3',
	inputType:'radio',
	comment:'comment_A20_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A20_4', 'A20_4'],
	scoreMap:[1, 0]
},{
	id:'A20_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A20_4',
	inputType:'radio',
	comment:'comment_A20_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A20_5', 'A20_5'],
	scoreMap:[1, 0]
},{
	id:'A20_5',
	category:CATEGORIES.SYMPTOMS,
	text:'A20_5',
	inputType:'radio',
	comment:'comment_A20_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A20_6', 'A20_6'],
	scoreMap:[1, 0]
},{
	id:'A20_6',
	category:CATEGORIES.SYMPTOMS,
	text:'A20_6',
	inputType:'radio',
	comment:'comment_A20_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['20'],
	scoreMap:[1, 0]
},{
	id:'A21_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A21_0',
	inputType:'radio',
	comment:'comment_A21_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A21_1', 'A21_1'],
	scoreMap:[1, 0]
},{
	id:'A21_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A21_1',
	inputType:'radio',
	comment:'comment_A21_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A21_2', 'A21_2'],
	scoreMap:[1, 0]
},{
	id:'A21_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A21_2',
	inputType:'radio',
	comment:'comment_A21_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A21_3', 'A21_3'],
	scoreMap:[1, 0]
},{
	id:'A21_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A21_3',
	inputType:'radio',
	comment:'comment_A21_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A21_4', 'A21_4'],
	scoreMap:[1, 0]
},{
	id:'A21_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A21_4',
	inputType:'radio',
	comment:'comment_A21_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['21'],
	scoreMap:[1, 0]
},{
	id:'A22_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A22_0',
	inputType:'radio',
	comment:'comment_A22_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A22_1', 'A22_1'],
	scoreMap:[1, 0]
},{
	id:'A22_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A22_1',
	inputType:'radio',
	comment:'comment_A22_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A22_2', 'A22_2'],
	scoreMap:[1, 0]
},{
	id:'A22_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A22_2',
	inputType:'radio',
	comment:'comment_A22_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A22_3', 'A22_3'],
	scoreMap:[1, 0]
},{
	id:'A22_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A22_3',
	inputType:'radio',
	comment:'comment_A22_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['22'],
	scoreMap:[1, 0]
},{
	id:'A23_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A23_0',
	inputType:'radio',
	comment:'comment_A23_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A23_1', 'A23_1'],
	scoreMap:[1, 0]
},{
	id:'A23_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A23_1',
	inputType:'radio',
	comment:'comment_A23_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A23_2', 'A23_2'],
	scoreMap:[1, 0]
},{
	id:'A23_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A23_2',
	inputType:'radio',
	comment:'comment_A23_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A23_3', 'A23_3'],
	scoreMap:[1, 0]
},{
	id:'A23_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A23_3',
	inputType:'radio',
	comment:'comment_A23_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['23'],
	scoreMap:[1, 0]
},{
	id:'A24_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A24_0',
	inputType:'radio',
	comment:'comment_A24_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A24_1', 'A24_1'],
	scoreMap:[1, 0]
},{
	id:'A24_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A24_1',
	inputType:'radio',
	comment:'comment_A24_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A24_2', 'A24_2'],
	scoreMap:[1, 0]
},{
	id:'A24_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A24_2',
	inputType:'radio',
	comment:'comment_A24_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A24_3', 'A24_3'],
	scoreMap:[1, 0]
},{
	id:'A24_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A24_3',
	inputType:'radio',
	comment:'comment_A24_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A24_4', 'A24_4'],
	scoreMap:[1, 0]
},{
	id:'A24_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A24_4',
	inputType:'radio',
	comment:'comment_A24_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A24_5', 'A24_5'],
	scoreMap:[1, 0]
},{
	id:'A24_5',
	category:CATEGORIES.SYMPTOMS,
	text:'A24_5',
	inputType:'radio',
	comment:'comment_A24_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A24_6', 'A24_6'],
	scoreMap:[1, 0]
},{
	id:'A24_6',
	category:CATEGORIES.SYMPTOMS,
	text:'A24_6',
	inputType:'radio',
	comment:'comment_A24_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['24'],
	scoreMap:[1, 0]
},{
	id:'A25_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A25_0',
	inputType:'radio',
	comment:'comment_A25_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A25_1', 'A25_1'],
	scoreMap:[1, 0]
},{
	id:'A25_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A25_1',
	inputType:'radio',
	comment:'comment_A25_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A25_2', 'A25_2'],
	scoreMap:[1, 0]
},{
	id:'A25_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A25_2',
	inputType:'radio',
	comment:'comment_A25_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A25_3', 'A25_3'],
	scoreMap:[1, 0]
},{
	id:'A25_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A25_3',
	inputType:'radio',
	comment:'comment_A25_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A25_4', 'A25_4'],
	scoreMap:[1, 0]
},{
	id:'A25_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A25_4',
	inputType:'radio',
	comment:'comment_A25_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['25'],
	scoreMap:[1, 0]
},{
	id:'A26_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A26_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['26'],
	scoreMap:[1, 0]
},{
	id:'A27_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A27_0',
	inputType:'radio',
	comment:'comment_A27_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A27_1', 'A27_1'],
	scoreMap:[1, 0]
},{
	id:'A27_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A27_1',
	inputType:'radio',
	comment:'comment_A27_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A27_2', 'A27_2'],
	scoreMap:[1, 0]
},{
	id:'A27_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A27_2',
	inputType:'radio',
	comment:'comment_A27_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A27_3', 'A27_3'],
	scoreMap:[1, 0]
},{
	id:'A27_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A27_3',
	inputType:'radio',
	comment:'comment_A27_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A27_4', 'A27_4'],
	scoreMap:[1, 0]
},{
	id:'A27_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A27_4',
	inputType:'radio',
	comment:'comment_A27_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A27_5', 'A27_5'],
	scoreMap:[1, 0]
},{
	id:'A27_5',
	category:CATEGORIES.SYMPTOMS,
	text:'A27_5',
	inputType:'radio',
	comment:'comment_A27_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['27'],
	scoreMap:[1, 0]
},{
	id:'A28_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A28_0',
	inputType:'radio',
	comment:'comment_A28_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A28_1', 'A28_1'],
	scoreMap:[1, 0]
},{
	id:'A28_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A28_1',
	inputType:'radio',
	comment:'comment_A28_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A28_2', 'A28_2'],
	scoreMap:[1, 0]
},{
	id:'A28_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A28_2',
	inputType:'radio',
	comment:'comment_A28_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A28_3', 'A28_3'],
	scoreMap:[1, 0]
},{
	id:'A28_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A28_3',
	inputType:'radio',
	comment:'comment_A28_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['28'],
	scoreMap:[1, 0]
},{
	id:'A29_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A29_0',
	inputType:'radio',
	comment:'comment_A29_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A29_1', 'A29_1'],
	scoreMap:[1, 0]
},{
	id:'A29_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A29_1',
	inputType:'radio',
	comment:'comment_A29_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A29_2', 'A29_2'],
	scoreMap:[1, 0]
},{
	id:'A29_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A29_2',
	inputType:'radio',
	comment:'comment_A29_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A29_3', 'A29_3'],
	scoreMap:[1, 0]
},{
	id:'A29_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A29_3',
	inputType:'radio',
	comment:'comment_A29_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A29_4', 'A29_4'],
	scoreMap:[1, 0]
},{
	id:'A29_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A29_4',
	inputType:'radio',
	comment:'comment_A29_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['29'],
	scoreMap:[1, 0]
},{
	id:'A30_0',
	category:CATEGORIES.SYMPTOMS,
	text:'A30_0',
	inputType:'radio',
	comment:'comment_A30_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A30_1', 'A30_1'],
	scoreMap:[1, 0]
},{
	id:'A30_1',
	category:CATEGORIES.SYMPTOMS,
	text:'A30_1',
	inputType:'radio',
	comment:'comment_A30_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A30_2', 'A30_2'],
	scoreMap:[1, 0]
},{
	id:'A30_2',
	category:CATEGORIES.SYMPTOMS,
	text:'A30_2',
	inputType:'radio',
	comment:'comment_A30_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A30_3', 'A30_3'],
	scoreMap:[1, 0]
},{
	id:'A30_3',
	category:CATEGORIES.SYMPTOMS,
	text:'A30_3',
	inputType:'radio',
	comment:'comment_A30_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A30_4', 'A30_4'],
	scoreMap:[1, 0]
},{
	id:'A30_4',
	category:CATEGORIES.SYMPTOMS,
	text:'A30_4',
	inputType:'radio',
	comment:'comment_A30_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['A30_5', 'A30_5'],
	scoreMap:[1, 0]
},{
	id:'A30_5',
	category:CATEGORIES.SYMPTOMS,
	text:'A30_5',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['30'],
	scoreMap:[1, 0]
},{
	id:'B31_0',
	category:CATEGORIES.SYMPTOMS,
	text:'B31_0',
	inputType:'radio',
	comment:'comment_B31_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B31_1', 'B31_1'],
	scoreMap:[1, 0]
},{
	id:'B31_1',
	category:CATEGORIES.SYMPTOMS,
	text:'B31_1',
	inputType:'radio',
	comment:'comment_B31_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B31_2', 'B31_2'],
	scoreMap:[1, 0]
},{
	id:'B31_2',
	category:CATEGORIES.SYMPTOMS,
	text:'B31_2',
	inputType:'radio',
	comment:'comment_B31_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B31_3', 'B31_3'],
	scoreMap:[1, 0]
},{
	id:'B31_3',
	category:CATEGORIES.SYMPTOMS,
	text:'B31_3',
	inputType:'radio',
	comment:'comment_B31_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B31_4', 'B31_4'],
	scoreMap:[1, 0]
},{
	id:'B31_4',
	category:CATEGORIES.SYMPTOMS,
	text:'B31_4',
	inputType:'radio',
	comment:'comment_B31_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B31_5', 'B31_5'],
	scoreMap:[1, 0]
},{
	id:'B31_5',
	category:CATEGORIES.SYMPTOMS,
	text:'B31_5',
	inputType:'radio',
	comment:'comment_B31_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B31_6', 'B31_6'],
	scoreMap:[1, 0]
},{
	id:'B31_6',
	category:CATEGORIES.SYMPTOMS,
	text:'B31_6',
	inputType:'radio',
	comment:'comment_B31_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B31_7', 'B31_7'],
	scoreMap:[1, 0]
},{
	id:'B31_7',
	category:CATEGORIES.SYMPTOMS,
	text:'B31_7',
	inputType:'radio',
	comment:'comment_B31_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B31_8', 'B31_8'],
	scoreMap:[1, 0]
},{
	id:'B31_8',
	category:CATEGORIES.SYMPTOMS,
	text:'B31_8',
	inputType:'radio',
	comment:'comment_B31_8',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['31'],
	scoreMap:[1, 0]
},{
	id:'B32_0',
	category:CATEGORIES.SYMPTOMS,
	text:'B32_0',
	inputType:'radio',
	comment:'comment_B32_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B32_1', 'B32_1'],
	scoreMap:[1, 0]
},{
	id:'B32_1',
	category:CATEGORIES.SYMPTOMS,
	text:'B32_1',
	inputType:'radio',
	comment:'comment_B32_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B32_2', 'B32_2'],
	scoreMap:[1, 0]
},{
	id:'B32_2',
	category:CATEGORIES.SYMPTOMS,
	text:'B32_2',
	inputType:'radio',
	comment:'comment_B32_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B32_3', 'B32_3'],
	scoreMap:[1, 0]
},{
	id:'B32_3',
	category:CATEGORIES.SYMPTOMS,
	text:'B32_3',
	inputType:'radio',
	comment:'comment_B32_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['32'],
	scoreMap:[1, 0]
},{
	id:'B33_0',
	category:CATEGORIES.SYMPTOMS,
	text:'B33_0',
	inputType:'radio',
	comment:'comment_B33_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B33_1', 'B33_1'],
	scoreMap:[1, 0]
},{
	id:'B33_1',
	category:CATEGORIES.SYMPTOMS,
	text:'B33_1',
	inputType:'radio',
	comment:'comment_B33_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B33_2', 'B33_2'],
	scoreMap:[1, 0]
},{
	id:'B33_2',
	category:CATEGORIES.SYMPTOMS,
	text:'B33_2',
	inputType:'radio',
	comment:'comment_B33_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['33'],
	scoreMap:[1, 0]
},{
	id:'B34_0',
	category:CATEGORIES.SYMPTOMS,
	text:'B34_0',
	inputType:'radio',
	comment:'comment_B34_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B34_1', 'B34_1'],
	scoreMap:[1, 0]
},{
	id:'B34_1',
	category:CATEGORIES.SYMPTOMS,
	text:'B34_1',
	inputType:'radio',
	comment:'comment_B34_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B34_2', 'B34_2'],
	scoreMap:[1, 0]
},{
	id:'B34_2',
	category:CATEGORIES.SYMPTOMS,
	text:'B34_2',
	inputType:'radio',
	comment:'comment_B34_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B34_3', 'B34_3'],
	scoreMap:[1, 0]
},{
	id:'B34_3',
	category:CATEGORIES.SYMPTOMS,
	text:'B34_3',
	inputType:'radio',
	comment:'comment_B34_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B34_4', 'B34_4'],
	scoreMap:[1, 0]
},{
	id:'B34_4',
	category:CATEGORIES.SYMPTOMS,
	text:'B34_4',
	inputType:'radio',
	comment:'comment_B34_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['34'],
	scoreMap:[1, 0]
},{
	id:'B35_0',
	category:CATEGORIES.SYMPTOMS,
	text:'B35_0',
	inputType:'radio',
	comment:'comment_B35_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B35_1', 'B35_1'],
	scoreMap:[1, 0]
},{
	id:'B35_1',
	category:CATEGORIES.SYMPTOMS,
	text:'B35_1',
	inputType:'radio',
	comment:'comment_B35_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B35_2', 'B35_2'],
	scoreMap:[1, 0]
},{
	id:'B35_2',
	category:CATEGORIES.SYMPTOMS,
	text:'B35_2',
	inputType:'radio',
	comment:'comment_B35_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B35_3', 'B35_3'],
	scoreMap:[1, 0]
},{
	id:'B35_3',
	category:CATEGORIES.SYMPTOMS,
	text:'B35_3',
	inputType:'radio',
	comment:'comment_B35_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B35_4', 'B35_4'],
	scoreMap:[1, 0]
},{
	id:'B35_4',
	category:CATEGORIES.SYMPTOMS,
	text:'B35_4',
	inputType:'radio',
	comment:'comment_B35_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['35'],
	scoreMap:[1, 0]
},{
	id:'B36_0',
	category:CATEGORIES.SYMPTOMS,
	text:'B36_0',
	inputType:'radio',
	comment:'comment_B36_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B36_1', 'B36_1'],
	scoreMap:[1, 0]
},{
	id:'B36_1',
	category:CATEGORIES.SYMPTOMS,
	text:'B36_1',
	inputType:'radio',
	comment:'comment_B36_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B36_2', 'B36_2'],
	scoreMap:[1, 0]
},{
	id:'B36_2',
	category:CATEGORIES.SYMPTOMS,
	text:'B36_2',
	inputType:'radio',
	comment:'comment_B36_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B36_3', 'B36_3'],
	scoreMap:[1, 0]
},{
	id:'B36_3',
	category:CATEGORIES.SYMPTOMS,
	text:'B36_3',
	inputType:'radio',
	comment:'comment_B36_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['36'],
	scoreMap:[1, 0]
},{
	id:'B37_0',
	category:CATEGORIES.SYMPTOMS,
	text:'B37_0',
	inputType:'radio',
	comment:'comment_B37_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B37_1', 'B37_1'],
	scoreMap:[1, 0]
},{
	id:'B37_1',
	category:CATEGORIES.SYMPTOMS,
	text:'B37_1',
	inputType:'radio',
	comment:'comment_B37_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B37_2', 'B37_2'],
	scoreMap:[1, 0]
},{
	id:'B37_2',
	category:CATEGORIES.SYMPTOMS,
	text:'B37_2',
	inputType:'radio',
	comment:'comment_B37_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B37_3', 'B37_3'],
	scoreMap:[1, 0]
},{
	id:'B37_3',
	category:CATEGORIES.SYMPTOMS,
	text:'B37_3',
	inputType:'radio',
	comment:'comment_B37_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['37'],
	scoreMap:[1, 0]
},{
	id:'B38_0',
	category:CATEGORIES.SYMPTOMS,
	text:'B38_0',
	inputType:'radio',
	comment:'comment_B38_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B38_1', 'B38_1'],
	scoreMap:[1, 0]
},{
	id:'B38_1',
	category:CATEGORIES.SYMPTOMS,
	text:'B38_1',
	inputType:'radio',
	comment:'comment_B38_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B38_2', 'B38_2'],
	scoreMap:[1, 0]
},{
	id:'B38_2',
	category:CATEGORIES.SYMPTOMS,
	text:'B38_2',
	inputType:'radio',
	comment:'comment_B38_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B38_3', 'B38_3'],
	scoreMap:[1, 0]
},{
	id:'B38_3',
	category:CATEGORIES.SYMPTOMS,
	text:'B38_3',
	inputType:'radio',
	comment:'comment_B38_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B38_4', 'B38_4'],
	scoreMap:[1, 0]
},{
	id:'B38_4',
	category:CATEGORIES.SYMPTOMS,
	text:'B38_4',
	inputType:'radio',
	comment:'comment_B38_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B38_5', 'B38_5'],
	scoreMap:[1, 0]
},{
	id:'B38_5',
	category:CATEGORIES.SYMPTOMS,
	text:'B38_5',
	inputType:'radio',
	comment:'comment_B38_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['38'],
	scoreMap:[1, 0]
},{
	id:'B39_0',
	category:CATEGORIES.SYMPTOMS,
	text:'B39_0',
	inputType:'radio',
	comment:'comment_B39_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B39_1', 'B39_1'],
	scoreMap:[1, 0]
},{
	id:'B39_1',
	category:CATEGORIES.SYMPTOMS,
	text:'B39_1',
	inputType:'radio',
	comment:'comment_B39_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B39_2', 'B39_2'],
	scoreMap:[1, 0]
},{
	id:'B39_2',
	category:CATEGORIES.SYMPTOMS,
	text:'B39_2',
	inputType:'radio',
	comment:'comment_B39_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B39_3', 'B39_3'],
	scoreMap:[1, 0]
},{
	id:'B39_3',
	category:CATEGORIES.SYMPTOMS,
	text:'B39_3',
	inputType:'radio',
	comment:'comment_B39_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['39'],
	scoreMap:[1, 0]
},{
	id:'B40_0',
	category:CATEGORIES.SYMPTOMS,
	text:'B40_0',
	inputType:'radio',
	comment:'comment_B40_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B40_1', 'B40_1'],
	scoreMap:[1, 0]
},{
	id:'B40_1',
	category:CATEGORIES.SYMPTOMS,
	text:'B40_1',
	inputType:'radio',
	comment:'comment_B40_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B40_2', 'B40_2'],
	scoreMap:[1, 0]
},{
	id:'B40_2',
	category:CATEGORIES.SYMPTOMS,
	text:'B40_2',
	inputType:'radio',
	comment:'comment_B40_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B40_3', 'B40_3'],
	scoreMap:[1, 0]
},{
	id:'B40_3',
	category:CATEGORIES.SYMPTOMS,
	text:'B40_3',
	inputType:'radio',
	comment:'comment_B40_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B40_4', 'B40_4'],
	scoreMap:[1, 0]
},{
	id:'B40_4',
	category:CATEGORIES.SYMPTOMS,
	text:'B40_4',
	inputType:'radio',
	comment:'comment_B40_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['40'],
	scoreMap:[1, 0]
},{
	id:'B41_0',
	category:CATEGORIES.SYMPTOMS,
	text:'B41_0',
	inputType:'radio',
	comment:'comment_B41_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B41_1', 'B41_1'],
	scoreMap:[1, 0]
},{
	id:'B41_1',
	category:CATEGORIES.SYMPTOMS,
	text:'B41_1',
	inputType:'radio',
	comment:'comment_B41_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B41_2', 'B41_2'],
	scoreMap:[1, 0]
},{
	id:'B41_2',
	category:CATEGORIES.SYMPTOMS,
	text:'B41_2',
	inputType:'radio',
	comment:'comment_B41_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['B41_3', 'B41_3'],
	scoreMap:[1, 0]
},{
	id:'B41_3',
	category:CATEGORIES.SYMPTOMS,
	text:'B41_3',
	inputType:'radio',
	comment:'comment_B41_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['41'],
	scoreMap:[1, 0]
},{
	id:'C42_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C42_0',
	inputType:'radio',
	comment:'comment_C42_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C42_1', 'C42_1'],
	scoreMap:[1, 0]
},{
	id:'C42_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C42_1',
	inputType:'radio',
	comment:'comment_C42_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C42_2', 'C42_2'],
	scoreMap:[1, 0]
},{
	id:'C42_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C42_2',
	inputType:'radio',
	comment:'comment_C42_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C42_3', 'C42_3'],
	scoreMap:[1, 0]
},{
	id:'C42_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C42_3',
	inputType:'radio',
	comment:'comment_C42_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C42_4', 'C42_4'],
	scoreMap:[1, 0]
},{
	id:'C42_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C42_4',
	inputType:'radio',
	comment:'comment_C42_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C42_5', 'C42_5'],
	scoreMap:[1, 0]
},{
	id:'C42_5',
	category:CATEGORIES.SYMPTOMS,
	text:'C42_5',
	inputType:'radio',
	comment:'comment_C42_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C42_6', 'C42_6'],
	scoreMap:[1, 0]
},{
	id:'C42_6',
	category:CATEGORIES.SYMPTOMS,
	text:'C42_6',
	inputType:'radio',
	comment:'comment_C42_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C42_7', 'C42_7'],
	scoreMap:[1, 0]
},{
	id:'C42_7',
	category:CATEGORIES.SYMPTOMS,
	text:'C42_7',
	inputType:'radio',
	comment:'comment_C42_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C42_8', 'C42_8'],
	scoreMap:[1, 0]
},{
	id:'C42_8',
	category:CATEGORIES.SYMPTOMS,
	text:'C42_8',
	inputType:'radio',
	comment:'comment_C42_8',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['42'],
	scoreMap:[1, 0]
},{
	id:'C43_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C43_0',
	inputType:'radio',
	comment:'comment_C43_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C43_1', 'C43_1'],
	scoreMap:[1, 0]
},{
	id:'C43_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C43_1',
	inputType:'radio',
	comment:'comment_C43_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C43_2', 'C43_2'],
	scoreMap:[1, 0]
},{
	id:'C43_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C43_2',
	inputType:'radio',
	comment:'comment_C43_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C43_3', 'C43_3'],
	scoreMap:[1, 0]
},{
	id:'C43_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C43_3',
	inputType:'radio',
	comment:'comment_C43_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C43_4', 'C43_4'],
	scoreMap:[1, 0]
},{
	id:'C43_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C43_4',
	inputType:'radio',
	comment:'comment_C43_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C43_5', 'C43_5'],
	scoreMap:[1, 0]
},{
	id:'C43_5',
	category:CATEGORIES.SYMPTOMS,
	text:'C43_5',
	inputType:'radio',
	comment:'comment_C43_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C43_6', 'C43_6'],
	scoreMap:[1, 0]
},{
	id:'C43_6',
	category:CATEGORIES.SYMPTOMS,
	text:'C43_6',
	inputType:'radio',
	comment:'comment_C43_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C43_7', 'C43_7'],
	scoreMap:[1, 0]
},{
	id:'C43_7',
	category:CATEGORIES.SYMPTOMS,
	text:'C43_7',
	inputType:'radio',
	comment:'comment_C43_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C43_8', 'C43_8'],
	scoreMap:[1, 0]
},{
	id:'C43_8',
	category:CATEGORIES.SYMPTOMS,
	text:'C43_8',
	inputType:'radio',
	comment:'comment_C43_8',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C43_9', 'C43_9'],
	scoreMap:[1, 0]
},{
	id:'C43_9',
	category:CATEGORIES.SYMPTOMS,
	text:'C43_9',
	inputType:'radio',
	comment:'comment_C43_9',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['43'],
	scoreMap:[1, 0]
},{
	id:'C44_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C44_0',
	inputType:'radio',
	comment:'comment_C44_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C44_1', 'C44_1'],
	scoreMap:[1, 0]
},{
	id:'C44_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C44_1',
	inputType:'radio',
	comment:'comment_C44_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C44_2', 'C44_2'],
	scoreMap:[1, 0]
},{
	id:'C44_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C44_2',
	inputType:'radio',
	comment:'comment_C44_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C44_3', 'C44_3'],
	scoreMap:[1, 0]
},{
	id:'C44_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C44_3',
	inputType:'radio',
	comment:'comment_C44_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C44_4', 'C44_4'],
	scoreMap:[1, 0]
},{
	id:'C44_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C44_4',
	inputType:'radio',
	comment:'comment_C44_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C44_5', 'C44_5'],
	scoreMap:[1, 0]
},{
	id:'C44_5',
	category:CATEGORIES.SYMPTOMS,
	text:'C44_5',
	inputType:'radio',
	comment:'comment_C44_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C44_6', 'C44_6'],
	scoreMap:[1, 0]
},{
	id:'C44_6',
	category:CATEGORIES.SYMPTOMS,
	text:'C44_6',
	inputType:'radio',
	comment:'comment_C44_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['44'],
	scoreMap:[1, 0]
},{
	id:'C45_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C45_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['45'],
	scoreMap:[1, 0]
},{
	id:'C46_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C46_0',
	inputType:'radio',
	comment:'comment_C46_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C46_1', 'C46_1'],
	scoreMap:[1, 0]
},{
	id:'C46_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C46_1',
	inputType:'radio',
	comment:'comment_C46_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C46_2', 'C46_2'],
	scoreMap:[1, 0]
},{
	id:'C46_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C46_2',
	inputType:'radio',
	comment:'comment_C46_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C46_3', 'C46_3'],
	scoreMap:[1, 0]
},{
	id:'C46_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C46_3',
	inputType:'radio',
	comment:'comment_C46_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C46_4', 'C46_4'],
	scoreMap:[1, 0]
},{
	id:'C46_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C46_4',
	inputType:'radio',
	comment:'comment_C46_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C46_5', 'C46_5'],
	scoreMap:[1, 0]
},{
	id:'C46_5',
	category:CATEGORIES.SYMPTOMS,
	text:'C46_5',
	inputType:'radio',
	comment:'comment_C46_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C46_6', 'C46_6'],
	scoreMap:[1, 0]
},{
	id:'C46_6',
	category:CATEGORIES.SYMPTOMS,
	text:'C46_6',
	inputType:'radio',
	comment:'comment_C46_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C46_7', 'C46_7'],
	scoreMap:[1, 0]
},{
	id:'C46_7',
	category:CATEGORIES.SYMPTOMS,
	text:'C46_7',
	inputType:'radio',
	comment:'comment_C46_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C46_8', 'C46_8'],
	scoreMap:[1, 0]
},{
	id:'C46_8',
	category:CATEGORIES.SYMPTOMS,
	text:'C46_8',
	inputType:'radio',
	comment:'comment_C46_8',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C46_9', 'C46_9'],
	scoreMap:[1, 0]
},{
	id:'C46_9',
	category:CATEGORIES.SYMPTOMS,
	text:'C46_9',
	inputType:'radio',
	comment:'comment_C46_9',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['46'],
	scoreMap:[1, 0]
},{
	id:'C47_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C47_0',
	inputType:'radio',
	comment:'comment_C47_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C47_1', 'C47_1'],
	scoreMap:[1, 0]
},{
	id:'C47_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C47_1',
	inputType:'radio',
	comment:'comment_C47_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C47_2', 'C47_2'],
	scoreMap:[1, 0]
},{
	id:'C47_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C47_2',
	inputType:'radio',
	comment:'comment_C47_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C47_3', 'C47_3'],
	scoreMap:[1, 0]
},{
	id:'C47_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C47_3',
	inputType:'radio',
	comment:'comment_C47_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['47'],
	scoreMap:[1, 0]
},{
	id:'C48_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C48_0',
	inputType:'radio',
	comment:'comment_C48_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C48_1', 'C48_1'],
	scoreMap:[1, 0]
},{
	id:'C48_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C48_1',
	inputType:'radio',
	comment:'comment_C48_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C48_2', 'C48_2'],
	scoreMap:[1, 0]
},{
	id:'C48_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C48_2',
	inputType:'radio',
	comment:'comment_C48_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C48_3', 'C48_3'],
	scoreMap:[1, 0]
},{
	id:'C48_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C48_3',
	inputType:'radio',
	comment:'comment_C48_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C48_4', 'C48_4'],
	scoreMap:[1, 0]
},{
	id:'C48_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C48_4',
	inputType:'radio',
	comment:'comment_C48_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C48_5', 'C48_5'],
	scoreMap:[1, 0]
},{
	id:'C48_5',
	category:CATEGORIES.SYMPTOMS,
	text:'C48_5',
	inputType:'radio',
	comment:'comment_C48_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C48_6', 'C48_6'],
	scoreMap:[1, 0]
},{
	id:'C48_6',
	category:CATEGORIES.SYMPTOMS,
	text:'C48_6',
	inputType:'radio',
	comment:'comment_C48_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C48_7', 'C48_7'],
	scoreMap:[1, 0]
},{
	id:'C48_7',
	category:CATEGORIES.SYMPTOMS,
	text:'C48_7',
	inputType:'radio',
	comment:'comment_C48_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['48'],
	scoreMap:[1, 0]
},{
	id:'C49_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C49_0',
	inputType:'radio',
	comment:'comment_C49_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C49_1', 'C49_1'],
	scoreMap:[1, 0]
},{
	id:'C49_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C49_1',
	inputType:'radio',
	comment:'comment_C49_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C49_2', 'C49_2'],
	scoreMap:[1, 0]
},{
	id:'C49_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C49_2',
	inputType:'radio',
	comment:'comment_C49_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C49_3', 'C49_3'],
	scoreMap:[1, 0]
},{
	id:'C49_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C49_3',
	inputType:'radio',
	comment:'comment_C49_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['49'],
	scoreMap:[1, 0]
},{
	id:'C50_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C50_0',
	inputType:'radio',
	comment:'comment_C50_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C50_1', 'C50_1'],
	scoreMap:[1, 0]
},{
	id:'C50_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C50_1',
	inputType:'radio',
	comment:'comment_C50_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C50_2', 'C50_2'],
	scoreMap:[1, 0]
},{
	id:'C50_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C50_2',
	inputType:'radio',
	comment:'comment_C50_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['50'],
	scoreMap:[1, 0]
},{
	id:'C51_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C51_0',
	inputType:'radio',
	comment:'comment_C51_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C51_1', 'C51_1'],
	scoreMap:[1, 0]
},{
	id:'C51_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C51_1',
	inputType:'radio',
	comment:'comment_C51_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C51_2', 'C51_2'],
	scoreMap:[1, 0]
},{
	id:'C51_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C51_2',
	inputType:'radio',
	comment:'comment_C51_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C51_3', 'C51_3'],
	scoreMap:[1, 0]
},{
	id:'C51_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C51_3',
	inputType:'radio',
	comment:'comment_C51_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C51_4', 'C51_4'],
	scoreMap:[1, 0]
},{
	id:'C51_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C51_4',
	inputType:'radio',
	comment:'comment_C51_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C51_5', 'C51_5'],
	scoreMap:[1, 0]
},{
	id:'C51_5',
	category:CATEGORIES.SYMPTOMS,
	text:'C51_5',
	inputType:'radio',
	comment:'comment_C51_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C51_6', 'C51_6'],
	scoreMap:[1, 0]
},{
	id:'C51_6',
	category:CATEGORIES.SYMPTOMS,
	text:'C51_6',
	inputType:'radio',
	comment:'comment_C51_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C51_7', 'C51_7'],
	scoreMap:[1, 0]
},{
	id:'C51_7',
	category:CATEGORIES.SYMPTOMS,
	text:'C51_7',
	inputType:'radio',
	comment:'comment_C51_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['51'],
	scoreMap:[1, 0]
},{
	id:'C52_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C52_0',
	inputType:'radio',
	comment:'comment_C52_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C52_1', 'C52_1'],
	scoreMap:[1, 0]
},{
	id:'C52_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C52_1',
	inputType:'radio',
	comment:'comment_C52_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C52_2', 'C52_2'],
	scoreMap:[1, 0]
},{
	id:'C52_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C52_2',
	inputType:'radio',
	comment:'comment_C52_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C52_3', 'C52_3'],
	scoreMap:[1, 0]
},{
	id:'C52_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C52_3',
	inputType:'radio',
	comment:'comment_C52_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C52_4', 'C52_4'],
	scoreMap:[1, 0]
},{
	id:'C52_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C52_4',
	inputType:'radio',
	comment:'comment_C52_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['52'],
	scoreMap:[1, 0]
},{
	id:'C53_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C53_0',
	inputType:'radio',
	comment:'comment_C53_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C53_1', 'C53_1'],
	scoreMap:[1, 0]
},{
	id:'C53_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C53_1',
	inputType:'radio',
	comment:'comment_C53_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C53_2', 'C53_2'],
	scoreMap:[1, 0]
},{
	id:'C53_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C53_2',
	inputType:'radio',
	comment:'comment_C53_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C53_3', 'C53_3'],
	scoreMap:[1, 0]
},{
	id:'C53_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C53_3',
	inputType:'radio',
	comment:'comment_C53_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C53_4', 'C53_4'],
	scoreMap:[1, 0]
},{
	id:'C53_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C53_4',
	inputType:'radio',
	comment:'comment_C53_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['53'],
	scoreMap:[1, 0]
},{
	id:'C54_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C54_0',
	inputType:'radio',
	comment:'comment_C54_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C54_1', 'C54_1'],
	scoreMap:[1, 0]
},{
	id:'C54_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C54_1',
	inputType:'radio',
	comment:'comment_C54_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C54_2', 'C54_2'],
	scoreMap:[1, 0]
},{
	id:'C54_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C54_2',
	inputType:'radio',
	comment:'comment_C54_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C54_3', 'C54_3'],
	scoreMap:[1, 0]
},{
	id:'C54_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C54_3',
	inputType:'radio',
	comment:'comment_C54_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C54_4', 'C54_4'],
	scoreMap:[1, 0]
},{
	id:'C54_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C54_4',
	inputType:'radio',
	comment:'comment_C54_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C54_5', 'C54_5'],
	scoreMap:[1, 0]
},{
	id:'C54_5',
	category:CATEGORIES.SYMPTOMS,
	text:'C54_5',
	inputType:'radio',
	comment:'comment_C54_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['54'],
	scoreMap:[1, 0]
},{
	id:'C55_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C55_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['55'],
	scoreMap:[1, 0]
},{
	id:'C56_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C56_0',
	inputType:'radio',
	comment:'comment_C56_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C56_1', 'C56_1'],
	scoreMap:[1, 0]
},{
	id:'C56_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C56_1',
	inputType:'radio',
	comment:'comment_C56_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C56_2', 'C56_2'],
	scoreMap:[1, 0]
},{
	id:'C56_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C56_2',
	inputType:'radio',
	comment:'comment_C56_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C56_3', 'C56_3'],
	scoreMap:[1, 0]
},{
	id:'C56_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C56_3',
	inputType:'radio',
	comment:'comment_C56_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C56_4', 'C56_4'],
	scoreMap:[1, 0]
},{
	id:'C56_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C56_4',
	inputType:'radio',
	comment:'comment_C56_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C56_5', 'C56_5'],
	scoreMap:[1, 0]
},{
	id:'C56_5',
	category:CATEGORIES.SYMPTOMS,
	text:'C56_5',
	inputType:'radio',
	comment:'comment_C56_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C56_6', 'C56_6'],
	scoreMap:[1, 0]
},{
	id:'C56_6',
	category:CATEGORIES.SYMPTOMS,
	text:'C56_6',
	inputType:'radio',
	comment:'comment_C56_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C56_7', 'C56_7'],
	scoreMap:[1, 0]
},{
	id:'C56_7',
	category:CATEGORIES.SYMPTOMS,
	text:'C56_7',
	inputType:'radio',
	comment:'comment_C56_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['56'],
	scoreMap:[1, 0]
},{
	id:'C57_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C57_0',
	inputType:'radio',
	comment:'comment_C57_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C57_1', 'C57_1'],
	scoreMap:[1, 0]
},{
	id:'C57_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C57_1',
	inputType:'radio',
	comment:'comment_C57_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C57_2', 'C57_2'],
	scoreMap:[1, 0]
},{
	id:'C57_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C57_2',
	inputType:'radio',
	comment:'comment_C57_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C57_3', 'C57_3'],
	scoreMap:[1, 0]
},{
	id:'C57_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C57_3',
	inputType:'radio',
	comment:'comment_C57_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['57'],
	scoreMap:[1, 0]
},{
	id:'C58_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C58_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['58'],
	scoreMap:[1, 0]
},{
	id:'C59_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C59_0',
	inputType:'radio',
	comment:'comment_C59_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C59_1', 'C59_1'],
	scoreMap:[1, 0]
},{
	id:'C59_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C59_1',
	inputType:'radio',
	comment:'comment_C59_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C59_2', 'C59_2'],
	scoreMap:[1, 0]
},{
	id:'C59_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C59_2',
	inputType:'radio',
	comment:'comment_C59_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C59_3', 'C59_3'],
	scoreMap:[1, 0]
},{
	id:'C59_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C59_3',
	inputType:'radio',
	comment:'comment_C59_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C59_4', 'C59_4'],
	scoreMap:[1, 0]
},{
	id:'C59_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C59_4',
	inputType:'radio',
	comment:'comment_C59_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C59_5', 'C59_5'],
	scoreMap:[1, 0]
},{
	id:'C59_5',
	category:CATEGORIES.SYMPTOMS,
	text:'C59_5',
	inputType:'radio',
	comment:'comment_C59_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['59'],
	scoreMap:[1, 0]
},{
	id:'C60_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C60_0',
	inputType:'radio',
	comment:'comment_C60_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C60_1', 'C60_1'],
	scoreMap:[1, 0]
},{
	id:'C60_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C60_1',
	inputType:'radio',
	comment:'comment_C60_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C60_2', 'C60_2'],
	scoreMap:[1, 0]
},{
	id:'C60_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C60_2',
	inputType:'radio',
	comment:'comment_C60_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C60_3', 'C60_3'],
	scoreMap:[1, 0]
},{
	id:'C60_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C60_3',
	inputType:'radio',
	comment:'comment_C60_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C60_4', 'C60_4'],
	scoreMap:[1, 0]
},{
	id:'C60_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C60_4',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['60'],
	scoreMap:[1, 0]
},{
	id:'C61_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C61_0',
	inputType:'radio',
	comment:'comment_C61_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C61_1', 'C61_1'],
	scoreMap:[1, 0]
},{
	id:'C61_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C61_1',
	inputType:'radio',
	comment:'comment_C61_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C61_2', 'C61_2'],
	scoreMap:[1, 0]
},{
	id:'C61_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C61_2',
	inputType:'radio',
	comment:'comment_C61_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C61_3', 'C61_3'],
	scoreMap:[1, 0]
},{
	id:'C61_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C61_3',
	inputType:'radio',
	comment:'comment_C61_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['61'],
	scoreMap:[1, 0]
},{
	id:'C62_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C62_0',
	inputType:'radio',
	comment:'comment_C62_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C62_1', 'C62_1'],
	scoreMap:[1, 0]
},{
	id:'C62_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C62_1',
	inputType:'radio',
	comment:'comment_C62_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C62_2', 'C62_2'],
	scoreMap:[1, 0]
},{
	id:'C62_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C62_2',
	inputType:'radio',
	comment:'comment_C62_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C62_3', 'C62_3'],
	scoreMap:[1, 0]
},{
	id:'C62_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C62_3',
	inputType:'radio',
	comment:'comment_C62_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['62'],
	scoreMap:[1, 0]
},{
	id:'C63_0',
	category:CATEGORIES.SYMPTOMS,
	text:'C63_0',
	inputType:'radio',
	comment:'comment_C63_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C63_1', 'C63_1'],
	scoreMap:[1, 0]
},{
	id:'C63_1',
	category:CATEGORIES.SYMPTOMS,
	text:'C63_1',
	inputType:'radio',
	comment:'comment_C63_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C63_2', 'C63_2'],
	scoreMap:[1, 0]
},{
	id:'C63_2',
	category:CATEGORIES.SYMPTOMS,
	text:'C63_2',
	inputType:'radio',
	comment:'comment_C63_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C63_3', 'C63_3'],
	scoreMap:[1, 0]
},{
	id:'C63_3',
	category:CATEGORIES.SYMPTOMS,
	text:'C63_3',
	inputType:'radio',
	comment:'comment_C63_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C63_4', 'C63_4'],
	scoreMap:[1, 0]
},{
	id:'C63_4',
	category:CATEGORIES.SYMPTOMS,
	text:'C63_4',
	inputType:'radio',
	comment:'comment_C63_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['C63_5', 'C63_5'],
	scoreMap:[1, 0]
},{
	id:'C63_5',
	category:CATEGORIES.SYMPTOMS,
	text:'C63_5',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['63'],
	scoreMap:[1, 0]
},{
	id:'D64_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D64_0',
	inputType:'radio',
	comment:'comment_D64_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D64_1', 'D64_1'],
	scoreMap:[1, 0]
},{
	id:'D64_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D64_1',
	inputType:'radio',
	comment:'comment_D64_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D64_2', 'D64_2'],
	scoreMap:[1, 0]
},{
	id:'D64_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D64_2',
	inputType:'radio',
	comment:'comment_D64_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D64_3', 'D64_3'],
	scoreMap:[1, 0]
},{
	id:'D64_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D64_3',
	inputType:'radio',
	comment:'comment_D64_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['64'],
	scoreMap:[1, 0]
},{
	id:'D65_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D65_0',
	inputType:'radio',
	comment:'comment_D65_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D65_1', 'D65_1'],
	scoreMap:[1, 0]
},{
	id:'D65_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D65_1',
	inputType:'radio',
	comment:'comment_D65_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D65_2', 'D65_2'],
	scoreMap:[1, 0]
},{
	id:'D65_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D65_2',
	inputType:'radio',
	comment:'comment_D65_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D65_3', 'D65_3'],
	scoreMap:[1, 0]
},{
	id:'D65_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D65_3',
	inputType:'radio',
	comment:'comment_D65_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['65'],
	scoreMap:[1, 0]
},{
	id:'D66_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D66_0',
	inputType:'radio',
	comment:'comment_D66_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D66_1', 'D66_1'],
	scoreMap:[1, 0]
},{
	id:'D66_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D66_1',
	inputType:'radio',
	comment:'comment_D66_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D66_2', 'D66_2'],
	scoreMap:[1, 0]
},{
	id:'D66_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D66_2',
	inputType:'radio',
	comment:'comment_D66_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D66_3', 'D66_3'],
	scoreMap:[1, 0]
},{
	id:'D66_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D66_3',
	inputType:'radio',
	comment:'comment_D66_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D66_4', 'D66_4'],
	scoreMap:[1, 0]
},{
	id:'D66_4',
	category:CATEGORIES.SYMPTOMS,
	text:'D66_4',
	inputType:'radio',
	comment:'comment_D66_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D66_5', 'D66_5'],
	scoreMap:[1, 0]
},{
	id:'D66_5',
	category:CATEGORIES.SYMPTOMS,
	text:'D66_5',
	inputType:'radio',
	comment:'comment_D66_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D66_6', 'D66_6'],
	scoreMap:[1, 0]
},{
	id:'D66_6',
	category:CATEGORIES.SYMPTOMS,
	text:'D66_6',
	inputType:'radio',
	comment:'comment_D66_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D66_7', 'D66_7'],
	scoreMap:[1, 0]
},{
	id:'D66_7',
	category:CATEGORIES.SYMPTOMS,
	text:'D66_7',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['66'],
	scoreMap:[1, 0]
},{
	id:'D67_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D67_0',
	inputType:'radio',
	comment:'comment_D67_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D67_1', 'D67_1'],
	scoreMap:[1, 0]
},{
	id:'D67_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D67_1',
	inputType:'radio',
	comment:'comment_D67_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['67'],
	scoreMap:[1, 0]
},{
	id:'D68_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D68_0',
	inputType:'radio',
	comment:'comment_D68_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D68_1', 'D68_1'],
	scoreMap:[1, 0]
},{
	id:'D68_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D68_1',
	inputType:'radio',
	comment:'comment_D68_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D68_2', 'D68_2'],
	scoreMap:[1, 0]
},{
	id:'D68_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D68_2',
	inputType:'radio',
	comment:'comment_D68_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D68_3', 'D68_3'],
	scoreMap:[1, 0]
},{
	id:'D68_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D68_3',
	inputType:'radio',
	comment:'comment_D68_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D68_4', 'D68_4'],
	scoreMap:[1, 0]
},{
	id:'D68_4',
	category:CATEGORIES.SYMPTOMS,
	text:'D68_4',
	inputType:'radio',
	comment:'comment_D68_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D68_5', 'D68_5'],
	scoreMap:[1, 0]
},{
	id:'D68_5',
	category:CATEGORIES.SYMPTOMS,
	text:'D68_5',
	inputType:'radio',
	comment:'comment_D68_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['68'],
	scoreMap:[1, 0]
},{
	id:'D69_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D69_0',
	inputType:'radio',
	comment:'comment_D69_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D69_1', 'D69_1'],
	scoreMap:[1, 0]
},{
	id:'D69_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D69_1',
	inputType:'radio',
	comment:'comment_D69_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D69_2', 'D69_2'],
	scoreMap:[1, 0]
},{
	id:'D69_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D69_2',
	inputType:'radio',
	comment:'comment_D69_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D69_3', 'D69_3'],
	scoreMap:[1, 0]
},{
	id:'D69_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D69_3',
	inputType:'radio',
	comment:'comment_D69_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['69'],
	scoreMap:[1, 0]
},{
	id:'D70_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D70_0',
	inputType:'radio',
	comment:'comment_D70_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D70_1', 'D70_1'],
	scoreMap:[1, 0]
},{
	id:'D70_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D70_1',
	inputType:'radio',
	comment:'comment_D70_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D70_2', 'D70_2'],
	scoreMap:[1, 0]
},{
	id:'D70_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D70_2',
	inputType:'radio',
	comment:'comment_D70_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D70_3', 'D70_3'],
	scoreMap:[1, 0]
},{
	id:'D70_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D70_3',
	inputType:'radio',
	comment:'comment_D70_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D70_4', 'D70_4'],
	scoreMap:[1, 0]
},{
	id:'D70_4',
	category:CATEGORIES.SYMPTOMS,
	text:'D70_4',
	inputType:'radio',
	comment:'comment_D70_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D70_5', 'D70_5'],
	scoreMap:[1, 0]
},{
	id:'D70_5',
	category:CATEGORIES.SYMPTOMS,
	text:'D70_5',
	inputType:'radio',
	comment:'comment_D70_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['70'],
	scoreMap:[1, 0]
},{
	id:'D71_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D71_0',
	inputType:'radio',
	comment:'comment_D71_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D71_1', 'D71_1'],
	scoreMap:[1, 0]
},{
	id:'D71_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D71_1',
	inputType:'radio',
	comment:'comment_D71_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D71_2', 'D71_2'],
	scoreMap:[1, 0]
},{
	id:'D71_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D71_2',
	inputType:'radio',
	comment:'comment_D71_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D71_3', 'D71_3'],
	scoreMap:[1, 0]
},{
	id:'D71_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D71_3',
	inputType:'radio',
	comment:'comment_D71_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D71_4', 'D71_4'],
	scoreMap:[1, 0]
},{
	id:'D71_4',
	category:CATEGORIES.SYMPTOMS,
	text:'D71_4',
	inputType:'radio',
	comment:'comment_D71_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D71_5', 'D71_5'],
	scoreMap:[1, 0]
},{
	id:'D71_5',
	category:CATEGORIES.SYMPTOMS,
	text:'D71_5',
	inputType:'radio',
	comment:'comment_D71_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['71'],
	scoreMap:[1, 0]
},{
	id:'D72_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D72_0',
	inputType:'radio',
	comment:'comment_D72_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D72_1', 'D72_1'],
	scoreMap:[1, 0]
},{
	id:'D72_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D72_1',
	inputType:'radio',
	comment:'comment_D72_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D72_2', 'D72_2'],
	scoreMap:[1, 0]
},{
	id:'D72_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D72_2',
	inputType:'radio',
	comment:'comment_D72_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D72_3', 'D72_3'],
	scoreMap:[1, 0]
},{
	id:'D72_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D72_3',
	inputType:'radio',
	comment:'comment_D72_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D72_4', 'D72_4'],
	scoreMap:[1, 0]
},{
	id:'D72_4',
	category:CATEGORIES.SYMPTOMS,
	text:'D72_4',
	inputType:'radio',
	comment:'comment_D72_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D72_5', 'D72_5'],
	scoreMap:[1, 0]
},{
	id:'D72_5',
	category:CATEGORIES.SYMPTOMS,
	text:'D72_5',
	inputType:'radio',
	comment:'comment_D72_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D72_6', 'D72_6'],
	scoreMap:[1, 0]
},{
	id:'D72_6',
	category:CATEGORIES.SYMPTOMS,
	text:'D72_6',
	inputType:'radio',
	comment:'comment_D72_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['72'],
	scoreMap:[1, 0]
},{
	id:'D73_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D73_0',
	inputType:'radio',
	comment:'comment_D73_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D73_1', 'D73_1'],
	scoreMap:[1, 0]
},{
	id:'D73_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D73_1',
	inputType:'radio',
	comment:'comment_D73_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D73_2', 'D73_2'],
	scoreMap:[1, 0]
},{
	id:'D73_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D73_2',
	inputType:'radio',
	comment:'comment_D73_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['73'],
	scoreMap:[1, 0]
},{
	id:'D74_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D74_0',
	inputType:'radio',
	comment:'comment_D74_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D74_1', 'D74_1'],
	scoreMap:[1, 0]
},{
	id:'D74_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D74_1',
	inputType:'radio',
	comment:'comment_D74_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D74_2', 'D74_2'],
	scoreMap:[1, 0]
},{
	id:'D74_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D74_2',
	inputType:'radio',
	comment:'comment_D74_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D74_3', 'D74_3'],
	scoreMap:[1, 0]
},{
	id:'D74_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D74_3',
	inputType:'radio',
	comment:'comment_D74_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D74_4', 'D74_4'],
	scoreMap:[1, 0]
},{
	id:'D74_4',
	category:CATEGORIES.SYMPTOMS,
	text:'D74_4',
	inputType:'radio',
	comment:'comment_D74_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D74_5', 'D74_5'],
	scoreMap:[1, 0]
},{
	id:'D74_5',
	category:CATEGORIES.SYMPTOMS,
	text:'D74_5',
	inputType:'radio',
	comment:'comment_D74_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['74'],
	scoreMap:[1, 0]
},{
	id:'D75_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D75_0',
	inputType:'radio',
	comment:'comment_D75_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D75_1', 'D75_1'],
	scoreMap:[1, 0]
},{
	id:'D75_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D75_1',
	inputType:'radio',
	comment:'comment_D75_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D75_2', 'D75_2'],
	scoreMap:[1, 0]
},{
	id:'D75_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D75_2',
	inputType:'radio',
	comment:'comment_D75_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D75_3', 'D75_3'],
	scoreMap:[1, 0]
},{
	id:'D75_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D75_3',
	inputType:'radio',
	comment:'comment_D75_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D75_4', 'D75_4'],
	scoreMap:[1, 0]
},{
	id:'D75_4',
	category:CATEGORIES.SYMPTOMS,
	text:'D75_4',
	inputType:'radio',
	comment:'comment_D75_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D75_5', 'D75_5'],
	scoreMap:[1, 0]
},{
	id:'D75_5',
	category:CATEGORIES.SYMPTOMS,
	text:'D75_5',
	inputType:'radio',
	comment:'comment_D75_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D75_6', 'D75_6'],
	scoreMap:[1, 0]
},{
	id:'D75_6',
	category:CATEGORIES.SYMPTOMS,
	text:'D75_6',
	inputType:'radio',
	comment:'comment_D75_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D75_7', 'D75_7'],
	scoreMap:[1, 0]
},{
	id:'D75_7',
	category:CATEGORIES.SYMPTOMS,
	text:'D75_7',
	inputType:'radio',
	comment:'comment_D75_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D75_8', 'D75_8'],
	scoreMap:[1, 0]
},{
	id:'D75_8',
	category:CATEGORIES.SYMPTOMS,
	text:'D75_8',
	inputType:'radio',
	comment:'comment_D75_8',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['75'],
	scoreMap:[1, 0]
},{
	id:'D76_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D76_0',
	inputType:'radio',
	comment:'comment_D76_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D76_1', 'D76_1'],
	scoreMap:[1, 0]
},{
	id:'D76_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D76_1',
	inputType:'radio',
	comment:'comment_D76_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D76_2', 'D76_2'],
	scoreMap:[1, 0]
},{
	id:'D76_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D76_2',
	inputType:'radio',
	comment:'comment_D76_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['76'],
	scoreMap:[1, 0]
},{
	id:'D77_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D77_0',
	inputType:'radio',
	comment:'comment_D77_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D77_1', 'D77_1'],
	scoreMap:[1, 0]
},{
	id:'D77_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D77_1',
	inputType:'radio',
	comment:'comment_D77_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D77_2', 'D77_2'],
	scoreMap:[1, 0]
},{
	id:'D77_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D77_2',
	inputType:'radio',
	comment:'comment_D77_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D77_3', 'D77_3'],
	scoreMap:[1, 0]
},{
	id:'D77_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D77_3',
	inputType:'radio',
	comment:'comment_D77_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D77_4', 'D77_4'],
	scoreMap:[1, 0]
},{
	id:'D77_4',
	category:CATEGORIES.SYMPTOMS,
	text:'D77_4',
	inputType:'radio',
	comment:'comment_D77_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['77'],
	scoreMap:[1, 0]
},{
	id:'D78_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D78_0',
	inputType:'radio',
	comment:'comment_D78_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D78_1', 'D78_1'],
	scoreMap:[1, 0]
},{
	id:'D78_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D78_1',
	inputType:'radio',
	comment:'comment_D78_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D78_2', 'D78_2'],
	scoreMap:[1, 0]
},{
	id:'D78_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D78_2',
	inputType:'radio',
	comment:'comment_D78_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D78_3', 'D78_3'],
	scoreMap:[1, 0]
},{
	id:'D78_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D78_3',
	inputType:'radio',
	comment:'comment_D78_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['78'],
	scoreMap:[1, 0]
},{
	id:'D79_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D79_0',
	inputType:'radio',
	comment:'comment_D79_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D79_1', 'D79_1'],
	scoreMap:[1, 0]
},{
	id:'D79_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D79_1',
	inputType:'radio',
	comment:'comment_D79_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D79_2', 'D79_2'],
	scoreMap:[1, 0]
},{
	id:'D79_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D79_2',
	inputType:'radio',
	comment:'comment_D79_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['79'],
	scoreMap:[1, 0]
},{
	id:'D80_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D80_0',
	inputType:'radio',
	comment:'comment_D80_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D80_1', 'D80_1'],
	scoreMap:[1, 0]
},{
	id:'D80_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D80_1',
	inputType:'radio',
	comment:'comment_D80_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D80_2', 'D80_2'],
	scoreMap:[1, 0]
},{
	id:'D80_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D80_2',
	inputType:'radio',
	comment:'comment_D80_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D80_3', 'D80_3'],
	scoreMap:[1, 0]
},{
	id:'D80_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D80_3',
	inputType:'radio',
	comment:'comment_D80_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['80'],
	scoreMap:[1, 0]
},{
	id:'D81_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D81_0',
	inputType:'radio',
	comment:'comment_D81_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D81_1', 'D81_1'],
	scoreMap:[1, 0]
},{
	id:'D81_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D81_1',
	inputType:'radio',
	comment:'comment_D81_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D81_2', 'D81_2'],
	scoreMap:[1, 0]
},{
	id:'D81_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D81_2',
	inputType:'radio',
	comment:'comment_D81_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D81_3', 'D81_3'],
	scoreMap:[1, 0]
},{
	id:'D81_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D81_3',
	inputType:'radio',
	comment:'comment_D81_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D81_4', 'D81_4'],
	scoreMap:[1, 0]
},{
	id:'D81_4',
	category:CATEGORIES.SYMPTOMS,
	text:'D81_4',
	inputType:'radio',
	comment:'comment_D81_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D81_5', 'D81_5'],
	scoreMap:[1, 0]
},{
	id:'D81_5',
	category:CATEGORIES.SYMPTOMS,
	text:'D81_5',
	inputType:'radio',
	comment:'comment_D81_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D81_6', 'D81_6'],
	scoreMap:[1, 0]
},{
	id:'D81_6',
	category:CATEGORIES.SYMPTOMS,
	text:'D81_6',
	inputType:'radio',
	comment:'comment_D81_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D81_7', 'D81_7'],
	scoreMap:[1, 0]
},{
	id:'D81_7',
	category:CATEGORIES.SYMPTOMS,
	text:'D81_7',
	inputType:'radio',
	comment:'comment_D81_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['81'],
	scoreMap:[1, 0]
},{
	id:'D82_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D82_0',
	inputType:'radio',
	comment:'comment_D82_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D82_1', 'D82_1'],
	scoreMap:[1, 0]
},{
	id:'D82_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D82_1',
	inputType:'radio',
	comment:'comment_D82_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D82_2', 'D82_2'],
	scoreMap:[1, 0]
},{
	id:'D82_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D82_2',
	inputType:'radio',
	comment:'comment_D82_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D82_3', 'D82_3'],
	scoreMap:[1, 0]
},{
	id:'D82_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D82_3',
	inputType:'radio',
	comment:'comment_D82_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D82_4', 'D82_4'],
	scoreMap:[1, 0]
},{
	id:'D82_4',
	category:CATEGORIES.SYMPTOMS,
	text:'D82_4',
	inputType:'radio',
	comment:'comment_D82_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['82'],
	scoreMap:[1, 0]
},{
	id:'D83_0',
	category:CATEGORIES.SYMPTOMS,
	text:'D83_0',
	inputType:'radio',
	comment:'comment_D83_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D83_1', 'D83_1'],
	scoreMap:[1, 0]
},{
	id:'D83_1',
	category:CATEGORIES.SYMPTOMS,
	text:'D83_1',
	inputType:'radio',
	comment:'comment_D83_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D83_2', 'D83_2'],
	scoreMap:[1, 0]
},{
	id:'D83_2',
	category:CATEGORIES.SYMPTOMS,
	text:'D83_2',
	inputType:'radio',
	comment:'comment_D83_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D83_3', 'D83_3'],
	scoreMap:[1, 0]
},{
	id:'D83_3',
	category:CATEGORIES.SYMPTOMS,
	text:'D83_3',
	inputType:'radio',
	comment:'comment_D83_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['D83_4', 'D83_4'],
	scoreMap:[1, 0]
},{
	id:'D83_4',
	category:CATEGORIES.SYMPTOMS,
	text:'D83_4',
	inputType:'radio',
	comment:'comment_D83_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['83'],
	scoreMap:[1, 0]
},{
	id:'E84_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E84_0',
	inputType:'radio',
	comment:'comment_E84_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E84_1', 'E84_1'],
	scoreMap:[1, 0]
},{
	id:'E84_1',
	category:CATEGORIES.SYMPTOMS,
	text:'E84_1',
	inputType:'radio',
	comment:'comment_E84_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E84_2', 'E84_2'],
	scoreMap:[1, 0]
},{
	id:'E84_2',
	category:CATEGORIES.SYMPTOMS,
	text:'E84_2',
	inputType:'radio',
	comment:'comment_E84_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E84_3', 'E84_3'],
	scoreMap:[1, 0]
},{
	id:'E84_3',
	category:CATEGORIES.SYMPTOMS,
	text:'E84_3',
	inputType:'radio',
	comment:'comment_E84_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['84'],
	scoreMap:[1, 0]
},{
	id:'E85_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E85_0',
	inputType:'radio',
	comment:'comment_E85_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E85_1', 'E85_1'],
	scoreMap:[1, 0]
},{
	id:'E85_1',
	category:CATEGORIES.SYMPTOMS,
	text:'E85_1',
	inputType:'radio',
	comment:'comment_E85_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E85_2', 'E85_2'],
	scoreMap:[1, 0]
},{
	id:'E85_2',
	category:CATEGORIES.SYMPTOMS,
	text:'E85_2',
	inputType:'radio',
	comment:'comment_E85_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E85_3', 'E85_3'],
	scoreMap:[1, 0]
},{
	id:'E85_3',
	category:CATEGORIES.SYMPTOMS,
	text:'E85_3',
	inputType:'radio',
	comment:'comment_E85_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['85'],
	scoreMap:[1, 0]
},{
	id:'E86_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E86_0',
	inputType:'radio',
	comment:'comment_E86_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E86_1', 'E86_1'],
	scoreMap:[1, 0]
},{
	id:'E86_1',
	category:CATEGORIES.SYMPTOMS,
	text:'E86_1',
	inputType:'radio',
	comment:'comment_E86_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E86_2', 'E86_2'],
	scoreMap:[1, 0]
},{
	id:'E86_2',
	category:CATEGORIES.SYMPTOMS,
	text:'E86_2',
	inputType:'radio',
	comment:'comment_E86_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E86_3', 'E86_3'],
	scoreMap:[1, 0]
},{
	id:'E86_3',
	category:CATEGORIES.SYMPTOMS,
	text:'E86_3',
	inputType:'radio',
	comment:'comment_E86_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E86_4', 'E86_4'],
	scoreMap:[1, 0]
},{
	id:'E86_4',
	category:CATEGORIES.SYMPTOMS,
	text:'E86_4',
	inputType:'radio',
	comment:'comment_E86_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E86_5', 'E86_5'],
	scoreMap:[1, 0]
},{
	id:'E86_5',
	category:CATEGORIES.SYMPTOMS,
	text:'E86_5',
	inputType:'radio',
	comment:'comment_E86_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['86'],
	scoreMap:[1, 0]
},{
	id:'E87_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E87_0',
	inputType:'radio',
	comment:'comment_E87_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E87_1', 'E87_1'],
	scoreMap:[1, 0]
},{
	id:'E87_1',
	category:CATEGORIES.SYMPTOMS,
	text:'E87_1',
	inputType:'radio',
	comment:'comment_E87_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E87_2', 'E87_2'],
	scoreMap:[1, 0]
},{
	id:'E87_2',
	category:CATEGORIES.SYMPTOMS,
	text:'E87_2',
	inputType:'radio',
	comment:'comment_E87_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E87_3', 'E87_3'],
	scoreMap:[1, 0]
},{
	id:'E87_3',
	category:CATEGORIES.SYMPTOMS,
	text:'E87_3',
	inputType:'radio',
	comment:'comment_E87_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['87'],
	scoreMap:[1, 0]
},{
	id:'E88_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E88_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['88'],
	scoreMap:[1, 0]
},{
	id:'E89_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E89_0',
	inputType:'radio',
	comment:'comment_E89_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E89_1', 'E89_1'],
	scoreMap:[1, 0]
},{
	id:'E89_1',
	category:CATEGORIES.SYMPTOMS,
	text:'E89_1',
	inputType:'radio',
	comment:'comment_E89_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E89_2', 'E89_2'],
	scoreMap:[1, 0]
},{
	id:'E89_2',
	category:CATEGORIES.SYMPTOMS,
	text:'E89_2',
	inputType:'radio',
	comment:'comment_E89_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E89_3', 'E89_3'],
	scoreMap:[1, 0]
},{
	id:'E89_3',
	category:CATEGORIES.SYMPTOMS,
	text:'E89_3',
	inputType:'radio',
	comment:'comment_E89_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E89_4', 'E89_4'],
	scoreMap:[1, 0]
},{
	id:'E89_4',
	category:CATEGORIES.SYMPTOMS,
	text:'E89_4',
	inputType:'radio',
	comment:'comment_E89_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['89'],
	scoreMap:[1, 0]
},{
	id:'E90_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E90_0',
	inputType:'radio',
	comment:'comment_E90_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E90_1', 'E90_1'],
	scoreMap:[1, 0]
},{
	id:'E90_1',
	category:CATEGORIES.SYMPTOMS,
	text:'E90_1',
	inputType:'radio',
	comment:'comment_E90_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['90'],
	scoreMap:[1, 0]
},{
	id:'E91_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E91_0',
	inputType:'radio',
	comment:'comment_E91_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E91_1', 'E91_1'],
	scoreMap:[1, 0]
},{
	id:'E91_1',
	category:CATEGORIES.SYMPTOMS,
	text:'E91_1',
	inputType:'radio',
	comment:'comment_E91_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E91_2', 'E91_2'],
	scoreMap:[1, 0]
},{
	id:'E91_2',
	category:CATEGORIES.SYMPTOMS,
	text:'E91_2',
	inputType:'radio',
	comment:'comment_E91_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E91_3', 'E91_3'],
	scoreMap:[1, 0]
},{
	id:'E91_3',
	category:CATEGORIES.SYMPTOMS,
	text:'E91_3',
	inputType:'radio',
	comment:'comment_E91_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['91'],
	scoreMap:[1, 0]
},{
	id:'E92_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E92_0',
	inputType:'radio',
	comment:'comment_E92_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E92_1', 'E92_1'],
	scoreMap:[1, 0]
},{
	id:'E92_1',
	category:CATEGORIES.SYMPTOMS,
	text:'E92_1',
	inputType:'radio',
	comment:'comment_E92_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E92_2', 'E92_2'],
	scoreMap:[1, 0]
},{
	id:'E92_2',
	category:CATEGORIES.SYMPTOMS,
	text:'E92_2',
	inputType:'radio',
	comment:'comment_E92_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E92_3', 'E92_3'],
	scoreMap:[1, 0]
},{
	id:'E92_3',
	category:CATEGORIES.SYMPTOMS,
	text:'E92_3',
	inputType:'radio',
	comment:'comment_E92_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['92'],
	scoreMap:[1, 0]
},{
	id:'E93_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E93_0',
	inputType:'radio',
	comment:'comment_E93_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E93_1', 'E93_1'],
	scoreMap:[1, 0]
},{
	id:'E93_1',
	category:CATEGORIES.SYMPTOMS,
	text:'E93_1',
	inputType:'radio',
	comment:'comment_E93_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E93_2', 'E93_2'],
	scoreMap:[1, 0]
},{
	id:'E93_2',
	category:CATEGORIES.SYMPTOMS,
	text:'E93_2',
	inputType:'radio',
	comment:'comment_E93_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E93_3', 'E93_3'],
	scoreMap:[1, 0]
},{
	id:'E93_3',
	category:CATEGORIES.SYMPTOMS,
	text:'E93_3',
	inputType:'radio',
	comment:'comment_E93_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E93_4', 'E93_4'],
	scoreMap:[1, 0]
},{
	id:'E93_4',
	category:CATEGORIES.SYMPTOMS,
	text:'E93_4',
	inputType:'radio',
	comment:'comment_E93_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['93'],
	scoreMap:[1, 0]
},{
	id:'E94_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E94_0',
	inputType:'radio',
	comment:'comment_E94_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E94_1', 'E94_1'],
	scoreMap:[1, 0]
},{
	id:'E94_1',
	category:CATEGORIES.SYMPTOMS,
	text:'E94_1',
	inputType:'radio',
	comment:'comment_E94_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E94_2', 'E94_2'],
	scoreMap:[1, 0]
},{
	id:'E94_2',
	category:CATEGORIES.SYMPTOMS,
	text:'E94_2',
	inputType:'radio',
	comment:'comment_E94_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E94_3', 'E94_3'],
	scoreMap:[1, 0]
},{
	id:'E94_3',
	category:CATEGORIES.SYMPTOMS,
	text:'E94_3',
	inputType:'radio',
	comment:'comment_E94_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E94_4', 'E94_4'],
	scoreMap:[1, 0]
},{
	id:'E94_4',
	category:CATEGORIES.SYMPTOMS,
	text:'E94_4',
	inputType:'radio',
	comment:'comment_E94_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E94_5', 'E94_5'],
	scoreMap:[1, 0]
},{
	id:'E94_5',
	category:CATEGORIES.SYMPTOMS,
	text:'E94_5',
	inputType:'radio',
	comment:'comment_E94_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['94'],
	scoreMap:[1, 0]
},{
	id:'E95_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E95_0',
	inputType:'radio',
	comment:'comment_E95_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E95_1', 'E95_1'],
	scoreMap:[1, 0]
},{
	id:'E95_1',
	category:CATEGORIES.SYMPTOMS,
	text:'E95_1',
	inputType:'radio',
	comment:'comment_E95_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E95_2', 'E95_2'],
	scoreMap:[1, 0]
},{
	id:'E95_2',
	category:CATEGORIES.SYMPTOMS,
	text:'E95_2',
	inputType:'radio',
	comment:'comment_E95_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E95_3', 'E95_3'],
	scoreMap:[1, 0]
},{
	id:'E95_3',
	category:CATEGORIES.SYMPTOMS,
	text:'E95_3',
	inputType:'radio',
	comment:'comment_E95_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E95_4', 'E95_4'],
	scoreMap:[1, 0]
},{
	id:'E95_4',
	category:CATEGORIES.SYMPTOMS,
	text:'E95_4',
	inputType:'radio',
	comment:'comment_E95_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E95_5', 'E95_5'],
	scoreMap:[1, 0]
},{
	id:'E95_5',
	category:CATEGORIES.SYMPTOMS,
	text:'E95_5',
	inputType:'radio',
	comment:'comment_E95_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E95_6', 'E95_6'],
	scoreMap:[1, 0]
},{
	id:'E95_6',
	category:CATEGORIES.SYMPTOMS,
	text:'E95_6',
	inputType:'radio',
	comment:'comment_E95_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['95'],
	scoreMap:[1, 0]
},{
	id:'E96_0',
	category:CATEGORIES.SYMPTOMS,
	text:'E96_0',
	inputType:'radio',
	comment:'comment_E96_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E96_1', 'E96_1'],
	scoreMap:[1, 0]
},{
	id:'E96_1',
	category:CATEGORIES.SYMPTOMS,
	text:'E96_1',
	inputType:'radio',
	comment:'comment_E96_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E96_2', 'E96_2'],
	scoreMap:[1, 0]
},{
	id:'E96_2',
	category:CATEGORIES.SYMPTOMS,
	text:'E96_2',
	inputType:'radio',
	comment:'comment_E96_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E96_3', 'E96_3'],
	scoreMap:[1, 0]
},{
	id:'E96_3',
	category:CATEGORIES.SYMPTOMS,
	text:'E96_3',
	inputType:'radio',
	comment:'comment_E96_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E96_4', 'E96_4'],
	scoreMap:[1, 0]
},{
	id:'E96_4',
	category:CATEGORIES.SYMPTOMS,
	text:'E96_4',
	inputType:'radio',
	comment:'comment_E96_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['E96_5', 'E96_5'],
	scoreMap:[1, 0]
},{
	id:'E96_5',
	category:CATEGORIES.SYMPTOMS,
	text:'E96_5',
	inputType:'radio',
	comment:'comment_E96_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['96'],
	scoreMap:[1, 0]
},{
	id:'F97_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F97_0',
	inputType:'radio',
	comment:'comment_F97_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F97_1', 'F97_1'],
	scoreMap:[1, 0]
},{
	id:'F97_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F97_1',
	inputType:'radio',
	comment:'comment_F97_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F97_2', 'F97_2'],
	scoreMap:[1, 0]
},{
	id:'F97_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F97_2',
	inputType:'radio',
	comment:'comment_F97_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F97_3', 'F97_3'],
	scoreMap:[1, 0]
},{
	id:'F97_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F97_3',
	inputType:'radio',
	comment:'comment_F97_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['97'],
	scoreMap:[1, 0]
},{
	id:'F98_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F98_0',
	inputType:'radio',
	comment:'comment_F98_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F98_1', 'F98_1'],
	scoreMap:[1, 0]
},{
	id:'F98_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F98_1',
	inputType:'radio',
	comment:'comment_F98_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F98_2', 'F98_2'],
	scoreMap:[1, 0]
},{
	id:'F98_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F98_2',
	inputType:'radio',
	comment:'comment_F98_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F98_3', 'F98_3'],
	scoreMap:[1, 0]
},{
	id:'F98_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F98_3',
	inputType:'radio',
	comment:'comment_F98_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['98'],
	scoreMap:[1, 0]
},{
	id:'F99_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F99_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['99'],
	scoreMap:[1, 0]
},{
	id:'F100_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F100_0',
	inputType:'radio',
	comment:'comment_F100_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F100_1', 'F100_1'],
	scoreMap:[1, 0]
},{
	id:'F100_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F100_1',
	inputType:'radio',
	comment:'comment_F100_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F100_2', 'F100_2'],
	scoreMap:[1, 0]
},{
	id:'F100_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F100_2',
	inputType:'radio',
	comment:'comment_F100_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['100'],
	scoreMap:[1, 0]
},{
	id:'F101_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F101_0',
	inputType:'radio',
	comment:'comment_F101_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F101_1', 'F101_1'],
	scoreMap:[1, 0]
},{
	id:'F101_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F101_1',
	inputType:'radio',
	comment:'comment_F101_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F101_2', 'F101_2'],
	scoreMap:[1, 0]
},{
	id:'F101_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F101_2',
	inputType:'radio',
	comment:'comment_F101_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F101_3', 'F101_3'],
	scoreMap:[1, 0]
},{
	id:'F101_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F101_3',
	inputType:'radio',
	comment:'comment_F101_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F101_4', 'F101_4'],
	scoreMap:[1, 0]
},{
	id:'F101_4',
	category:CATEGORIES.SYMPTOMS,
	text:'F101_4',
	inputType:'radio',
	comment:'comment_F101_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['101'],
	scoreMap:[1, 0]
},{
	id:'F102_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F102_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['102'],
	scoreMap:[1, 0]
},{
	id:'F103_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F103_0',
	inputType:'radio',
	comment:'comment_F103_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F103_1', 'F103_1'],
	scoreMap:[1, 0]
},{
	id:'F103_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F103_1',
	inputType:'radio',
	comment:'comment_F103_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F103_2', 'F103_2'],
	scoreMap:[1, 0]
},{
	id:'F103_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F103_2',
	inputType:'radio',
	comment:'comment_F103_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F103_3', 'F103_3'],
	scoreMap:[1, 0]
},{
	id:'F103_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F103_3',
	inputType:'radio',
	comment:'comment_F103_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F103_4', 'F103_4'],
	scoreMap:[1, 0]
},{
	id:'F103_4',
	category:CATEGORIES.SYMPTOMS,
	text:'F103_4',
	inputType:'radio',
	comment:'comment_F103_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['103'],
	scoreMap:[1, 0]
},{
	id:'F104_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F104_0',
	inputType:'radio',
	comment:'comment_F104_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F104_1', 'F104_1'],
	scoreMap:[1, 0]
},{
	id:'F104_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F104_1',
	inputType:'radio',
	comment:'comment_F104_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F104_2', 'F104_2'],
	scoreMap:[1, 0]
},{
	id:'F104_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F104_2',
	inputType:'radio',
	comment:'comment_F104_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F104_3', 'F104_3'],
	scoreMap:[1, 0]
},{
	id:'F104_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F104_3',
	inputType:'radio',
	comment:'comment_F104_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F104_4', 'F104_4'],
	scoreMap:[1, 0]
},{
	id:'F104_4',
	category:CATEGORIES.SYMPTOMS,
	text:'F104_4',
	inputType:'radio',
	comment:'comment_F104_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F104_5', 'F104_5'],
	scoreMap:[1, 0]
},{
	id:'F104_5',
	category:CATEGORIES.SYMPTOMS,
	text:'F104_5',
	inputType:'radio',
	comment:'comment_F104_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F104_6', 'F104_6'],
	scoreMap:[1, 0]
},{
	id:'F104_6',
	category:CATEGORIES.SYMPTOMS,
	text:'F104_6',
	inputType:'radio',
	comment:'comment_F104_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['104'],
	scoreMap:[1, 0]
},{
	id:'F105_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F105_0',
	inputType:'radio',
	comment:'comment_F105_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F105_1', 'F105_1'],
	scoreMap:[1, 0]
},{
	id:'F105_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F105_1',
	inputType:'radio',
	comment:'comment_F105_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F105_2', 'F105_2'],
	scoreMap:[1, 0]
},{
	id:'F105_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F105_2',
	inputType:'radio',
	comment:'comment_F105_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F105_3', 'F105_3'],
	scoreMap:[1, 0]
},{
	id:'F105_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F105_3',
	inputType:'radio',
	comment:'comment_F105_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F105_4', 'F105_4'],
	scoreMap:[1, 0]
},{
	id:'F105_4',
	category:CATEGORIES.SYMPTOMS,
	text:'F105_4',
	inputType:'radio',
	comment:'comment_F105_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['105'],
	scoreMap:[1, 0]
},{
	id:'F106_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F106_0',
	inputType:'radio',
	comment:'comment_F106_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F106_1', 'F106_1'],
	scoreMap:[1, 0]
},{
	id:'F106_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F106_1',
	inputType:'radio',
	comment:'comment_F106_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F106_2', 'F106_2'],
	scoreMap:[1, 0]
},{
	id:'F106_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F106_2',
	inputType:'radio',
	comment:'comment_F106_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F106_3', 'F106_3'],
	scoreMap:[1, 0]
},{
	id:'F106_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F106_3',
	inputType:'radio',
	comment:'comment_F106_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F106_4', 'F106_4'],
	scoreMap:[1, 0]
},{
	id:'F106_4',
	category:CATEGORIES.SYMPTOMS,
	text:'F106_4',
	inputType:'radio',
	comment:'comment_F106_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['106'],
	scoreMap:[1, 0]
},{
	id:'F107_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F107_0',
	inputType:'radio',
	comment:'comment_F107_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F107_1', 'F107_1'],
	scoreMap:[1, 0]
},{
	id:'F107_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F107_1',
	inputType:'radio',
	comment:'comment_F107_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F107_2', 'F107_2'],
	scoreMap:[1, 0]
},{
	id:'F107_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F107_2',
	inputType:'radio',
	comment:'comment_F107_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F107_3', 'F107_3'],
	scoreMap:[1, 0]
},{
	id:'F107_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F107_3',
	inputType:'radio',
	comment:'comment_F107_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F107_4', 'F107_4'],
	scoreMap:[1, 0]
},{
	id:'F107_4',
	category:CATEGORIES.SYMPTOMS,
	text:'F107_4',
	inputType:'radio',
	comment:'comment_F107_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F107_5', 'F107_5'],
	scoreMap:[1, 0]
},{
	id:'F107_5',
	category:CATEGORIES.SYMPTOMS,
	text:'F107_5',
	inputType:'radio',
	comment:'comment_F107_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['107'],
	scoreMap:[1, 0]
},{
	id:'F108_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F108_0',
	inputType:'radio',
	comment:'comment_F108_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F108_1', 'F108_1'],
	scoreMap:[1, 0]
},{
	id:'F108_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F108_1',
	inputType:'radio',
	comment:'comment_F108_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F108_2', 'F108_2'],
	scoreMap:[1, 0]
},{
	id:'F108_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F108_2',
	inputType:'radio',
	comment:'comment_F108_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F108_3', 'F108_3'],
	scoreMap:[1, 0]
},{
	id:'F108_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F108_3',
	inputType:'radio',
	comment:'comment_F108_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['108'],
	scoreMap:[1, 0]
},{
	id:'F109_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F109_0',
	inputType:'radio',
	comment:'comment_F109_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F109_1', 'F109_1'],
	scoreMap:[1, 0]
},{
	id:'F109_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F109_1',
	inputType:'radio',
	comment:'comment_F109_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F109_2', 'F109_2'],
	scoreMap:[1, 0]
},{
	id:'F109_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F109_2',
	inputType:'radio',
	comment:'comment_F109_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['109'],
	scoreMap:[1, 0]
},{
	id:'F110_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F110_0',
	inputType:'radio',
	comment:'comment_F110_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F110_1', 'F110_1'],
	scoreMap:[1, 0]
},{
	id:'F110_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F110_1',
	inputType:'radio',
	comment:'comment_F110_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F110_2', 'F110_2'],
	scoreMap:[1, 0]
},{
	id:'F110_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F110_2',
	inputType:'radio',
	comment:'comment_F110_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F110_3', 'F110_3'],
	scoreMap:[1, 0]
},{
	id:'F110_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F110_3',
	inputType:'radio',
	comment:'comment_F110_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['110'],
	scoreMap:[1, 0]
},{
	id:'F111_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F111_0',
	inputType:'radio',
	comment:'comment_F111_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F111_1', 'F111_1'],
	scoreMap:[1, 0]
},{
	id:'F111_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F111_1',
	inputType:'radio',
	comment:'comment_F111_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F111_2', 'F111_2'],
	scoreMap:[1, 0]
},{
	id:'F111_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F111_2',
	inputType:'radio',
	comment:'comment_F111_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F111_3', 'F111_3'],
	scoreMap:[1, 0]
},{
	id:'F111_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F111_3',
	inputType:'radio',
	comment:'comment_F111_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['111'],
	scoreMap:[1, 0]
},{
	id:'F112_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F112_0',
	inputType:'radio',
	comment:'comment_F112_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['112'],
	scoreMap:[1, 0]
},{
	id:'F113_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F113_0',
	inputType:'radio',
	comment:'comment_F113_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F113_1', 'F113_1'],
	scoreMap:[1, 0]
},{
	id:'F113_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F113_1',
	inputType:'radio',
	comment:'comment_F113_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F113_2', 'F113_2'],
	scoreMap:[1, 0]
},{
	id:'F113_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F113_2',
	inputType:'radio',
	comment:'comment_F113_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F113_3', 'F113_3'],
	scoreMap:[1, 0]
},{
	id:'F113_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F113_3',
	inputType:'radio',
	comment:'comment_F113_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['113'],
	scoreMap:[1, 0]
},{
	id:'F114_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F114_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['114'],
	scoreMap:[1, 0]
},{
	id:'F115_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F115_0',
	inputType:'radio',
	comment:'comment_F115_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F115_1', 'F115_1'],
	scoreMap:[1, 0]
},{
	id:'F115_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F115_1',
	inputType:'radio',
	comment:'comment_F115_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F115_2', 'F115_2'],
	scoreMap:[1, 0]
},{
	id:'F115_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F115_2',
	inputType:'radio',
	comment:'comment_F115_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F115_3', 'F115_3'],
	scoreMap:[1, 0]
},{
	id:'F115_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F115_3',
	inputType:'radio',
	comment:'comment_F115_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['115'],
	scoreMap:[1, 0]
},{
	id:'F116_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F116_0',
	inputType:'radio',
	comment:'comment_F116_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F116_1', 'F116_1'],
	scoreMap:[1, 0]
},{
	id:'F116_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F116_1',
	inputType:'radio',
	comment:'comment_F116_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F116_2', 'F116_2'],
	scoreMap:[1, 0]
},{
	id:'F116_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F116_2',
	inputType:'radio',
	comment:'comment_F116_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['116'],
	scoreMap:[1, 0]
},{
	id:'F117_0',
	category:CATEGORIES.SYMPTOMS,
	text:'F117_0',
	inputType:'radio',
	comment:'comment_F117_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F117_1', 'F117_1'],
	scoreMap:[1, 0]
},{
	id:'F117_1',
	category:CATEGORIES.SYMPTOMS,
	text:'F117_1',
	inputType:'radio',
	comment:'comment_F117_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F117_2', 'F117_2'],
	scoreMap:[1, 0]
},{
	id:'F117_2',
	category:CATEGORIES.SYMPTOMS,
	text:'F117_2',
	inputType:'radio',
	comment:'comment_F117_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['F117_3', 'F117_3'],
	scoreMap:[1, 0]
},{
	id:'F117_3',
	category:CATEGORIES.SYMPTOMS,
	text:'F117_3',
	inputType:'radio',
	comment:'comment_F117_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['117'],
	scoreMap:[1, 0]
},{
	id:'G118_0',
	category:CATEGORIES.SYMPTOMS,
	text:'G118_0',
	inputType:'radio',
	comment:'comment_G118_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G118_1', 'G118_1'],
	scoreMap:[1, 0]
},{
	id:'G118_1',
	category:CATEGORIES.SYMPTOMS,
	text:'G118_1',
	inputType:'radio',
	comment:'comment_G118_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G118_2', 'G118_2'],
	scoreMap:[1, 0]
},{
	id:'G118_2',
	category:CATEGORIES.SYMPTOMS,
	text:'G118_2',
	inputType:'radio',
	comment:'comment_G118_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G118_3', 'G118_3'],
	scoreMap:[1, 0]
},{
	id:'G118_3',
	category:CATEGORIES.SYMPTOMS,
	text:'G118_3',
	inputType:'radio',
	comment:'comment_G118_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['118'],
	scoreMap:[1, 0]
},{
	id:'G119_0',
	category:CATEGORIES.SYMPTOMS,
	text:'G119_0',
	inputType:'radio',
	comment:'comment_G119_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G119_1', 'G119_1'],
	scoreMap:[1, 0]
},{
	id:'G119_1',
	category:CATEGORIES.SYMPTOMS,
	text:'G119_1',
	inputType:'radio',
	comment:'comment_G119_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G119_2', 'G119_2'],
	scoreMap:[1, 0]
},{
	id:'G119_2',
	category:CATEGORIES.SYMPTOMS,
	text:'G119_2',
	inputType:'radio',
	comment:'comment_G119_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G119_3', 'G119_3'],
	scoreMap:[1, 0]
},{
	id:'G119_3',
	category:CATEGORIES.SYMPTOMS,
	text:'G119_3',
	inputType:'radio',
	comment:'comment_G119_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G119_4', 'G119_4'],
	scoreMap:[1, 0]
},{
	id:'G119_4',
	category:CATEGORIES.SYMPTOMS,
	text:'G119_4',
	inputType:'radio',
	comment:'comment_G119_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['119'],
	scoreMap:[1, 0]
},{
	id:'G120_0',
	category:CATEGORIES.SYMPTOMS,
	text:'G120_0',
	inputType:'radio',
	comment:'comment_G120_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G120_1', 'G120_1'],
	scoreMap:[1, 0]
},{
	id:'G120_1',
	category:CATEGORIES.SYMPTOMS,
	text:'G120_1',
	inputType:'radio',
	comment:'comment_G120_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G120_2', 'G120_2'],
	scoreMap:[1, 0]
},{
	id:'G120_2',
	category:CATEGORIES.SYMPTOMS,
	text:'G120_2',
	inputType:'radio',
	comment:'comment_G120_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['120'],
	scoreMap:[1, 0]
},{
	id:'G121_0',
	category:CATEGORIES.SYMPTOMS,
	text:'G121_0',
	inputType:'radio',
	comment:'comment_G121_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G121_1', 'G121_1'],
	scoreMap:[1, 0]
},{
	id:'G121_1',
	category:CATEGORIES.SYMPTOMS,
	text:'G121_1',
	inputType:'radio',
	comment:'comment_G121_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['121'],
	scoreMap:[1, 0]
},{
	id:'G122_0',
	category:CATEGORIES.SYMPTOMS,
	text:'G122_0',
	inputType:'radio',
	comment:'comment_G122_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G122_1', 'G122_1'],
	scoreMap:[1, 0]
},{
	id:'G122_1',
	category:CATEGORIES.SYMPTOMS,
	text:'G122_1',
	inputType:'radio',
	comment:'comment_G122_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['122'],
	scoreMap:[1, 0]
},{
	id:'G123_0',
	category:CATEGORIES.SYMPTOMS,
	text:'G123_0',
	inputType:'radio',
	comment:'comment_G123_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G123_1', 'G123_1'],
	scoreMap:[1, 0]
},{
	id:'G123_1',
	category:CATEGORIES.SYMPTOMS,
	text:'G123_1',
	inputType:'radio',
	comment:'comment_G123_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G123_2', 'G123_2'],
	scoreMap:[1, 0]
},{
	id:'G123_2',
	category:CATEGORIES.SYMPTOMS,
	text:'G123_2',
	inputType:'radio',
	comment:'comment_G123_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G123_3', 'G123_3'],
	scoreMap:[1, 0]
},{
	id:'G123_3',
	category:CATEGORIES.SYMPTOMS,
	text:'G123_3',
	inputType:'radio',
	comment:'comment_G123_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G123_4', 'G123_4'],
	scoreMap:[1, 0]
},{
	id:'G123_4',
	category:CATEGORIES.SYMPTOMS,
	text:'G123_4',
	inputType:'radio',
	comment:'comment_G123_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['G123_5', 'G123_5'],
	scoreMap:[1, 0]
},{
	id:'G123_5',
	category:CATEGORIES.SYMPTOMS,
	text:'G123_5',
	inputType:'radio',
	comment:'comment_G123_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['123'],
	scoreMap:[1, 0]
},{
	id:'H124_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H124_0',
	inputType:'radio',
	comment:'comment_H124_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H124_1', 'H124_1'],
	scoreMap:[1, 0]
},{
	id:'H124_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H124_1',
	inputType:'radio',
	comment:'comment_H124_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H124_2', 'H124_2'],
	scoreMap:[1, 0]
},{
	id:'H124_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H124_2',
	inputType:'radio',
	comment:'comment_H124_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H124_3', 'H124_3'],
	scoreMap:[1, 0]
},{
	id:'H124_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H124_3',
	inputType:'radio',
	comment:'comment_H124_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['124'],
	scoreMap:[1, 0]
},{
	id:'H125_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H125_0',
	inputType:'radio',
	comment:'comment_H125_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H125_1', 'H125_1'],
	scoreMap:[1, 0]
},{
	id:'H125_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H125_1',
	inputType:'radio',
	comment:'comment_H125_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H125_2', 'H125_2'],
	scoreMap:[1, 0]
},{
	id:'H125_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H125_2',
	inputType:'radio',
	comment:'comment_H125_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H125_3', 'H125_3'],
	scoreMap:[1, 0]
},{
	id:'H125_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H125_3',
	inputType:'radio',
	comment:'comment_H125_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H125_4', 'H125_4'],
	scoreMap:[1, 0]
},{
	id:'H125_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H125_4',
	inputType:'radio',
	comment:'comment_H125_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['125'],
	scoreMap:[1, 0]
},{
	id:'H126_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H126_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['126'],
	scoreMap:[1, 0]
},{
	id:'H127_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H127_0',
	inputType:'radio',
	comment:'comment_H127_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H127_1', 'H127_1'],
	scoreMap:[1, 0]
},{
	id:'H127_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H127_1',
	inputType:'radio',
	comment:'comment_H127_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H127_2', 'H127_2'],
	scoreMap:[1, 0]
},{
	id:'H127_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H127_2',
	inputType:'radio',
	comment:'comment_H127_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H127_3', 'H127_3'],
	scoreMap:[1, 0]
},{
	id:'H127_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H127_3',
	inputType:'radio',
	comment:'comment_H127_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H127_4', 'H127_4'],
	scoreMap:[1, 0]
},{
	id:'H127_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H127_4',
	inputType:'radio',
	comment:'comment_H127_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H127_5', 'H127_5'],
	scoreMap:[1, 0]
},{
	id:'H127_5',
	category:CATEGORIES.SYMPTOMS,
	text:'H127_5',
	inputType:'radio',
	comment:'comment_H127_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H127_6', 'H127_6'],
	scoreMap:[1, 0]
},{
	id:'H127_6',
	category:CATEGORIES.SYMPTOMS,
	text:'H127_6',
	inputType:'radio',
	comment:'comment_H127_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H127_7', 'H127_7'],
	scoreMap:[1, 0]
},{
	id:'H127_7',
	category:CATEGORIES.SYMPTOMS,
	text:'H127_7',
	inputType:'radio',
	comment:'comment_H127_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H127_8', 'H127_8'],
	scoreMap:[1, 0]
},{
	id:'H127_8',
	category:CATEGORIES.SYMPTOMS,
	text:'H127_8',
	inputType:'radio',
	comment:'comment_H127_8',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['127'],
	scoreMap:[1, 0]
},{
	id:'H128_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H128_0',
	inputType:'radio',
	comment:'comment_H128_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H128_1', 'H128_1'],
	scoreMap:[1, 0]
},{
	id:'H128_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H128_1',
	inputType:'radio',
	comment:'comment_H128_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H128_2', 'H128_2'],
	scoreMap:[1, 0]
},{
	id:'H128_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H128_2',
	inputType:'radio',
	comment:'comment_H128_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H128_3', 'H128_3'],
	scoreMap:[1, 0]
},{
	id:'H128_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H128_3',
	inputType:'radio',
	comment:'comment_H128_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H128_4', 'H128_4'],
	scoreMap:[1, 0]
},{
	id:'H128_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H128_4',
	inputType:'radio',
	comment:'comment_H128_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['128'],
	scoreMap:[1, 0]
},{
	id:'H129_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H129_0',
	inputType:'radio',
	comment:'comment_H129_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H129_1', 'H129_1'],
	scoreMap:[1, 0]
},{
	id:'H129_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H129_1',
	inputType:'radio',
	comment:'comment_H129_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H129_2', 'H129_2'],
	scoreMap:[1, 0]
},{
	id:'H129_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H129_2',
	inputType:'radio',
	comment:'comment_H129_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H129_3', 'H129_3'],
	scoreMap:[1, 0]
},{
	id:'H129_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H129_3',
	inputType:'radio',
	comment:'comment_H129_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['129'],
	scoreMap:[1, 0]
},{
	id:'H130_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H130_0',
	inputType:'radio',
	comment:'comment_H130_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H130_1', 'H130_1'],
	scoreMap:[1, 0]
},{
	id:'H130_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H130_1',
	inputType:'radio',
	comment:'comment_H130_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H130_2', 'H130_2'],
	scoreMap:[1, 0]
},{
	id:'H130_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H130_2',
	inputType:'radio',
	comment:'comment_H130_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H130_3', 'H130_3'],
	scoreMap:[1, 0]
},{
	id:'H130_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H130_3',
	inputType:'radio',
	comment:'comment_H130_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H130_4', 'H130_4'],
	scoreMap:[1, 0]
},{
	id:'H130_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H130_4',
	inputType:'radio',
	comment:'comment_H130_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H130_5', 'H130_5'],
	scoreMap:[1, 0]
},{
	id:'H130_5',
	category:CATEGORIES.SYMPTOMS,
	text:'H130_5',
	inputType:'radio',
	comment:'comment_H130_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['130'],
	scoreMap:[1, 0]
},{
	id:'H131_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H131_0',
	inputType:'radio',
	comment:'comment_H131_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H131_1', 'H131_1'],
	scoreMap:[1, 0]
},{
	id:'H131_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H131_1',
	inputType:'radio',
	comment:'comment_H131_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H131_2', 'H131_2'],
	scoreMap:[1, 0]
},{
	id:'H131_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H131_2',
	inputType:'radio',
	comment:'comment_H131_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H131_3', 'H131_3'],
	scoreMap:[1, 0]
},{
	id:'H131_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H131_3',
	inputType:'radio',
	comment:'comment_H131_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H131_4', 'H131_4'],
	scoreMap:[1, 0]
},{
	id:'H131_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H131_4',
	inputType:'radio',
	comment:'comment_H131_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H131_5', 'H131_5'],
	scoreMap:[1, 0]
},{
	id:'H131_5',
	category:CATEGORIES.SYMPTOMS,
	text:'H131_5',
	inputType:'radio',
	comment:'comment_H131_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['131'],
	scoreMap:[1, 0]
},{
	id:'H132_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H132_0',
	inputType:'radio',
	comment:'comment_H132_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H132_1', 'H132_1'],
	scoreMap:[1, 0]
},{
	id:'H132_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H132_1',
	inputType:'radio',
	comment:'comment_H132_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H132_2', 'H132_2'],
	scoreMap:[1, 0]
},{
	id:'H132_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H132_2',
	inputType:'radio',
	comment:'comment_H132_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H132_3', 'H132_3'],
	scoreMap:[1, 0]
},{
	id:'H132_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H132_3',
	inputType:'radio',
	comment:'comment_H132_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H132_4', 'H132_4'],
	scoreMap:[1, 0]
},{
	id:'H132_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H132_4',
	inputType:'radio',
	comment:'comment_H132_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H132_5', 'H132_5'],
	scoreMap:[1, 0]
},{
	id:'H132_5',
	category:CATEGORIES.SYMPTOMS,
	text:'H132_5',
	inputType:'radio',
	comment:'comment_H132_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['132'],
	scoreMap:[1, 0]
},{
	id:'H133_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H133_0',
	inputType:'radio',
	comment:'comment_H133_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H133_1', 'H133_1'],
	scoreMap:[1, 0]
},{
	id:'H133_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H133_1',
	inputType:'radio',
	comment:'comment_H133_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H133_2', 'H133_2'],
	scoreMap:[1, 0]
},{
	id:'H133_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H133_2',
	inputType:'radio',
	comment:'comment_H133_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H133_3', 'H133_3'],
	scoreMap:[1, 0]
},{
	id:'H133_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H133_3',
	inputType:'radio',
	comment:'comment_H133_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H133_4', 'H133_4'],
	scoreMap:[1, 0]
},{
	id:'H133_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H133_4',
	inputType:'radio',
	comment:'comment_H133_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H133_5', 'H133_5'],
	scoreMap:[1, 0]
},{
	id:'H133_5',
	category:CATEGORIES.SYMPTOMS,
	text:'H133_5',
	inputType:'radio',
	comment:'comment_H133_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H133_6', 'H133_6'],
	scoreMap:[1, 0]
},{
	id:'H133_6',
	category:CATEGORIES.SYMPTOMS,
	text:'H133_6',
	inputType:'radio',
	comment:'comment_H133_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['133'],
	scoreMap:[1, 0]
},{
	id:'H134_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H134_0',
	inputType:'radio',
	comment:'comment_H134_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H134_1', 'H134_1'],
	scoreMap:[1, 0]
},{
	id:'H134_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H134_1',
	inputType:'radio',
	comment:'comment_H134_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H134_2', 'H134_2'],
	scoreMap:[1, 0]
},{
	id:'H134_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H134_2',
	inputType:'radio',
	comment:'comment_H134_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H134_3', 'H134_3'],
	scoreMap:[1, 0]
},{
	id:'H134_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H134_3',
	inputType:'radio',
	comment:'comment_H134_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['134'],
	scoreMap:[1, 0]
},{
	id:'H135_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H135_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['135'],
	scoreMap:[1, 0]
},{
	id:'H136_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H136_0',
	inputType:'radio',
	comment:'comment_H136_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H136_1', 'H136_1'],
	scoreMap:[1, 0]
},{
	id:'H136_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H136_1',
	inputType:'radio',
	comment:'comment_H136_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H136_2', 'H136_2'],
	scoreMap:[1, 0]
},{
	id:'H136_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H136_2',
	inputType:'radio',
	comment:'comment_H136_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H136_3', 'H136_3'],
	scoreMap:[1, 0]
},{
	id:'H136_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H136_3',
	inputType:'radio',
	comment:'comment_H136_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H136_4', 'H136_4'],
	scoreMap:[1, 0]
},{
	id:'H136_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H136_4',
	inputType:'radio',
	comment:'comment_H136_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H136_5', 'H136_5'],
	scoreMap:[1, 0]
},{
	id:'H136_5',
	category:CATEGORIES.SYMPTOMS,
	text:'H136_5',
	inputType:'radio',
	comment:'comment_H136_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H136_6', 'H136_6'],
	scoreMap:[1, 0]
},{
	id:'H136_6',
	category:CATEGORIES.SYMPTOMS,
	text:'H136_6',
	inputType:'radio',
	comment:'comment_H136_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['136'],
	scoreMap:[1, 0]
},{
	id:'H137_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H137_0',
	inputType:'radio',
	comment:'comment_H137_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H137_1', 'H137_1'],
	scoreMap:[1, 0]
},{
	id:'H137_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H137_1',
	inputType:'radio',
	comment:'comment_H137_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H137_2', 'H137_2'],
	scoreMap:[1, 0]
},{
	id:'H137_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H137_2',
	inputType:'radio',
	comment:'comment_H137_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H137_3', 'H137_3'],
	scoreMap:[1, 0]
},{
	id:'H137_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H137_3',
	inputType:'radio',
	comment:'comment_H137_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H137_4', 'H137_4'],
	scoreMap:[1, 0]
},{
	id:'H137_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H137_4',
	inputType:'radio',
	comment:'comment_H137_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['137'],
	scoreMap:[1, 0]
},{
	id:'H138_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H138_0',
	inputType:'radio',
	comment:'comment_H138_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H138_1', 'H138_1'],
	scoreMap:[1, 0]
},{
	id:'H138_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H138_1',
	inputType:'radio',
	comment:'comment_H138_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H138_2', 'H138_2'],
	scoreMap:[1, 0]
},{
	id:'H138_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H138_2',
	inputType:'radio',
	comment:'comment_H138_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H138_3', 'H138_3'],
	scoreMap:[1, 0]
},{
	id:'H138_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H138_3',
	inputType:'radio',
	comment:'comment_H138_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H138_4', 'H138_4'],
	scoreMap:[1, 0]
},{
	id:'H138_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H138_4',
	inputType:'radio',
	comment:'comment_H138_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['138'],
	scoreMap:[1, 0]
},{
	id:'H139_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H139_0',
	inputType:'radio',
	comment:'comment_H139_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H139_1', 'H139_1'],
	scoreMap:[1, 0]
},{
	id:'H139_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H139_1',
	inputType:'radio',
	comment:'comment_H139_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H139_2', 'H139_2'],
	scoreMap:[1, 0]
},{
	id:'H139_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H139_2',
	inputType:'radio',
	comment:'comment_H139_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H139_3', 'H139_3'],
	scoreMap:[1, 0]
},{
	id:'H139_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H139_3',
	inputType:'radio',
	comment:'comment_H139_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['139'],
	scoreMap:[1, 0]
},{
	id:'H140_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H140_0',
	inputType:'radio',
	comment:'comment_H140_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H140_1', 'H140_1'],
	scoreMap:[1, 0]
},{
	id:'H140_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H140_1',
	inputType:'radio',
	comment:'comment_H140_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H140_2', 'H140_2'],
	scoreMap:[1, 0]
},{
	id:'H140_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H140_2',
	inputType:'radio',
	comment:'comment_H140_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H140_3', 'H140_3'],
	scoreMap:[1, 0]
},{
	id:'H140_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H140_3',
	inputType:'radio',
	comment:'comment_H140_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['140'],
	scoreMap:[1, 0]
},{
	id:'H141_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H141_0',
	inputType:'radio',
	comment:'comment_H141_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H141_1', 'H141_1'],
	scoreMap:[1, 0]
},{
	id:'H141_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H141_1',
	inputType:'radio',
	comment:'comment_H141_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H141_2', 'H141_2'],
	scoreMap:[1, 0]
},{
	id:'H141_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H141_2',
	inputType:'radio',
	comment:'comment_H141_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['141'],
	scoreMap:[1, 0]
},{
	id:'H142_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H142_0',
	inputType:'radio',
	comment:'comment_H142_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H142_1', 'H142_1'],
	scoreMap:[1, 0]
},{
	id:'H142_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H142_1',
	inputType:'radio',
	comment:'comment_H142_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H142_2', 'H142_2'],
	scoreMap:[1, 0]
},{
	id:'H142_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H142_2',
	inputType:'radio',
	comment:'comment_H142_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['142'],
	scoreMap:[1, 0]
},{
	id:'H143_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H143_0',
	inputType:'radio',
	comment:'comment_H143_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H143_1', 'H143_1'],
	scoreMap:[1, 0]
},{
	id:'H143_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H143_1',
	inputType:'radio',
	comment:'comment_H143_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H143_2', 'H143_2'],
	scoreMap:[1, 0]
},{
	id:'H143_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H143_2',
	inputType:'radio',
	comment:'comment_H143_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H143_3', 'H143_3'],
	scoreMap:[1, 0]
},{
	id:'H143_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H143_3',
	inputType:'radio',
	comment:'comment_H143_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H143_4', 'H143_4'],
	scoreMap:[1, 0]
},{
	id:'H143_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H143_4',
	inputType:'radio',
	comment:'comment_H143_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H143_5', 'H143_5'],
	scoreMap:[1, 0]
},{
	id:'H143_5',
	category:CATEGORIES.SYMPTOMS,
	text:'H143_5',
	inputType:'radio',
	comment:'comment_H143_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['143'],
	scoreMap:[1, 0]
},{
	id:'H144_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H144_0',
	inputType:'radio',
	comment:'comment_H144_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H144_1', 'H144_1'],
	scoreMap:[1, 0]
},{
	id:'H144_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H144_1',
	inputType:'radio',
	comment:'comment_H144_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['144'],
	scoreMap:[1, 0]
},{
	id:'H145_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H145_0',
	inputType:'radio',
	comment:'comment_H145_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H145_1', 'H145_1'],
	scoreMap:[1, 0]
},{
	id:'H145_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H145_1',
	inputType:'radio',
	comment:'comment_H145_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H145_2', 'H145_2'],
	scoreMap:[1, 0]
},{
	id:'H145_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H145_2',
	inputType:'radio',
	comment:'comment_H145_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['145'],
	scoreMap:[1, 0]
},{
	id:'H146_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H146_0',
	inputType:'radio',
	comment:'comment_H146_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H146_1', 'H146_1'],
	scoreMap:[1, 0]
},{
	id:'H146_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H146_1',
	inputType:'radio',
	comment:'comment_H146_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H146_2', 'H146_2'],
	scoreMap:[1, 0]
},{
	id:'H146_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H146_2',
	inputType:'radio',
	comment:'comment_H146_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H146_3', 'H146_3'],
	scoreMap:[1, 0]
},{
	id:'H146_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H146_3',
	inputType:'radio',
	comment:'comment_H146_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['146'],
	scoreMap:[1, 0]
},{
	id:'H147_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H147_0',
	inputType:'radio',
	comment:'comment_H147_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H147_1', 'H147_1'],
	scoreMap:[1, 0]
},{
	id:'H147_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H147_1',
	inputType:'radio',
	comment:'comment_H147_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H147_2', 'H147_2'],
	scoreMap:[1, 0]
},{
	id:'H147_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H147_2',
	inputType:'radio',
	comment:'comment_H147_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H147_3', 'H147_3'],
	scoreMap:[1, 0]
},{
	id:'H147_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H147_3',
	inputType:'radio',
	comment:'comment_H147_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['147'],
	scoreMap:[1, 0]
},{
	id:'H148_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H148_0',
	inputType:'radio',
	comment:'comment_H148_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H148_1', 'H148_1'],
	scoreMap:[1, 0]
},{
	id:'H148_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H148_1',
	inputType:'radio',
	comment:'comment_H148_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H148_2', 'H148_2'],
	scoreMap:[1, 0]
},{
	id:'H148_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H148_2',
	inputType:'radio',
	comment:'comment_H148_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['148'],
	scoreMap:[1, 0]
},{
	id:'H149_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H149_0',
	inputType:'radio',
	comment:'comment_H149_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H149_1', 'H149_1'],
	scoreMap:[1, 0]
},{
	id:'H149_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H149_1',
	inputType:'radio',
	comment:'comment_H149_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['149'],
	scoreMap:[1, 0]
},{
	id:'H150_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H150_0',
	inputType:'radio',
	comment:'comment_H150_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H150_1', 'H150_1'],
	scoreMap:[1, 0]
},{
	id:'H150_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H150_1',
	inputType:'radio',
	comment:'comment_H150_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H150_2', 'H150_2'],
	scoreMap:[1, 0]
},{
	id:'H150_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H150_2',
	inputType:'radio',
	comment:'comment_H150_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H150_3', 'H150_3'],
	scoreMap:[1, 0]
},{
	id:'H150_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H150_3',
	inputType:'radio',
	comment:'comment_H150_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H150_4', 'H150_4'],
	scoreMap:[1, 0]
},{
	id:'H150_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H150_4',
	inputType:'radio',
	comment:'comment_H150_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['150'],
	scoreMap:[1, 0]
},{
	id:'H151_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H151_0',
	inputType:'radio',
	comment:'comment_H151_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H151_1', 'H151_1'],
	scoreMap:[1, 0]
},{
	id:'H151_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H151_1',
	inputType:'radio',
	comment:'comment_H151_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H151_2', 'H151_2'],
	scoreMap:[1, 0]
},{
	id:'H151_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H151_2',
	inputType:'radio',
	comment:'comment_H151_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H151_3', 'H151_3'],
	scoreMap:[1, 0]
},{
	id:'H151_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H151_3',
	inputType:'radio',
	comment:'comment_H151_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['151'],
	scoreMap:[1, 0]
},{
	id:'H152_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H152_0',
	inputType:'radio',
	comment:'comment_H152_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H152_1', 'H152_1'],
	scoreMap:[1, 0]
},{
	id:'H152_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H152_1',
	inputType:'radio',
	comment:'comment_H152_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H152_2', 'H152_2'],
	scoreMap:[1, 0]
},{
	id:'H152_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H152_2',
	inputType:'radio',
	comment:'comment_H152_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H152_3', 'H152_3'],
	scoreMap:[1, 0]
},{
	id:'H152_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H152_3',
	inputType:'radio',
	comment:'comment_H152_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H152_4', 'H152_4'],
	scoreMap:[1, 0]
},{
	id:'H152_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H152_4',
	inputType:'radio',
	comment:'comment_H152_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['152'],
	scoreMap:[1, 0]
},{
	id:'H153_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H153_0',
	inputType:'radio',
	comment:'comment_H153_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H153_1', 'H153_1'],
	scoreMap:[1, 0]
},{
	id:'H153_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H153_1',
	inputType:'radio',
	comment:'comment_H153_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H153_2', 'H153_2'],
	scoreMap:[1, 0]
},{
	id:'H153_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H153_2',
	inputType:'radio',
	comment:'comment_H153_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H153_3', 'H153_3'],
	scoreMap:[1, 0]
},{
	id:'H153_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H153_3',
	inputType:'radio',
	comment:'comment_H153_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H153_4', 'H153_4'],
	scoreMap:[1, 0]
},{
	id:'H153_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H153_4',
	inputType:'radio',
	comment:'comment_H153_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H153_5', 'H153_5'],
	scoreMap:[1, 0]
},{
	id:'H153_5',
	category:CATEGORIES.SYMPTOMS,
	text:'H153_5',
	inputType:'radio',
	comment:'comment_H153_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H153_6', 'H153_6'],
	scoreMap:[1, 0]
},{
	id:'H153_6',
	category:CATEGORIES.SYMPTOMS,
	text:'H153_6',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['153'],
	scoreMap:[1, 0]
},{
	id:'H154_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H154_0',
	inputType:'radio',
	comment:'comment_H154_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H154_1', 'H154_1'],
	scoreMap:[1, 0]
},{
	id:'H154_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H154_1',
	inputType:'radio',
	comment:'comment_H154_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H154_2', 'H154_2'],
	scoreMap:[1, 0]
},{
	id:'H154_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H154_2',
	inputType:'radio',
	comment:'comment_H154_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H154_3', 'H154_3'],
	scoreMap:[1, 0]
},{
	id:'H154_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H154_3',
	inputType:'radio',
	comment:'comment_H154_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['154'],
	scoreMap:[1, 0]
},{
	id:'H155_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H155_0',
	inputType:'radio',
	comment:'comment_H155_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H155_1', 'H155_1'],
	scoreMap:[1, 0]
},{
	id:'H155_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H155_1',
	inputType:'radio',
	comment:'comment_H155_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H155_2', 'H155_2'],
	scoreMap:[1, 0]
},{
	id:'H155_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H155_2',
	inputType:'radio',
	comment:'comment_H155_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H155_3', 'H155_3'],
	scoreMap:[1, 0]
},{
	id:'H155_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H155_3',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['155'],
	scoreMap:[1, 0]
},{
	id:'H156_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H156_0',
	inputType:'radio',
	comment:'comment_H156_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H156_1', 'H156_1'],
	scoreMap:[1, 0]
},{
	id:'H156_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H156_1',
	inputType:'radio',
	comment:'comment_H156_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H156_2', 'H156_2'],
	scoreMap:[1, 0]
},{
	id:'H156_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H156_2',
	inputType:'radio',
	comment:'comment_H156_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['156'],
	scoreMap:[1, 0]
},{
	id:'H157_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H157_0',
	inputType:'radio',
	comment:'comment_H157_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H157_1', 'H157_1'],
	scoreMap:[1, 0]
},{
	id:'H157_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H157_1',
	inputType:'radio',
	comment:'comment_H157_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H157_2', 'H157_2'],
	scoreMap:[1, 0]
},{
	id:'H157_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H157_2',
	inputType:'radio',
	comment:'comment_H157_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['157'],
	scoreMap:[1, 0]
},{
	id:'H158_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H158_0',
	inputType:'radio',
	comment:'comment_H158_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H158_1', 'H158_1'],
	scoreMap:[1, 0]
},{
	id:'H158_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H158_1',
	inputType:'radio',
	comment:'comment_H158_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H158_2', 'H158_2'],
	scoreMap:[1, 0]
},{
	id:'H158_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H158_2',
	inputType:'radio',
	comment:'comment_H158_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H158_3', 'H158_3'],
	scoreMap:[1, 0]
},{
	id:'H158_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H158_3',
	inputType:'radio',
	comment:'comment_H158_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H158_4', 'H158_4'],
	scoreMap:[1, 0]
},{
	id:'H158_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H158_4',
	inputType:'radio',
	comment:'comment_H158_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['158'],
	scoreMap:[1, 0]
},{
	id:'H159_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H159_0',
	inputType:'radio',
	comment:'comment_H159_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H159_1', 'H159_1'],
	scoreMap:[1, 0]
},{
	id:'H159_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H159_1',
	inputType:'radio',
	comment:'comment_H159_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H159_2', 'H159_2'],
	scoreMap:[1, 0]
},{
	id:'H159_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H159_2',
	inputType:'radio',
	comment:'comment_H159_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['159'],
	scoreMap:[1, 0]
},{
	id:'H160_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H160_0',
	inputType:'radio',
	comment:'comment_H160_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H160_1', 'H160_1'],
	scoreMap:[1, 0]
},{
	id:'H160_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H160_1',
	inputType:'radio',
	comment:'comment_H160_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H160_2', 'H160_2'],
	scoreMap:[1, 0]
},{
	id:'H160_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H160_2',
	inputType:'radio',
	comment:'comment_H160_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H160_3', 'H160_3'],
	scoreMap:[1, 0]
},{
	id:'H160_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H160_3',
	inputType:'radio',
	comment:'comment_H160_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['160'],
	scoreMap:[1, 0]
},{
	id:'H161_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H161_0',
	inputType:'radio',
	comment:'comment_H161_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H161_1', 'H161_1'],
	scoreMap:[1, 0]
},{
	id:'H161_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H161_1',
	inputType:'radio',
	comment:'comment_H161_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H161_2', 'H161_2'],
	scoreMap:[1, 0]
},{
	id:'H161_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H161_2',
	inputType:'radio',
	comment:'comment_H161_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H161_3', 'H161_3'],
	scoreMap:[1, 0]
},{
	id:'H161_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H161_3',
	inputType:'radio',
	comment:'comment_H161_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H161_4', 'H161_4'],
	scoreMap:[1, 0]
},{
	id:'H161_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H161_4',
	inputType:'radio',
	comment:'comment_H161_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['161'],
	scoreMap:[1, 0]
},{
	id:'H162_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H162_0',
	inputType:'radio',
	comment:'comment_H162_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H162_1', 'H162_1'],
	scoreMap:[1, 0]
},{
	id:'H162_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H162_1',
	inputType:'radio',
	comment:'comment_H162_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H162_2', 'H162_2'],
	scoreMap:[1, 0]
},{
	id:'H162_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H162_2',
	inputType:'radio',
	comment:'comment_H162_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H162_3', 'H162_3'],
	scoreMap:[1, 0]
},{
	id:'H162_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H162_3',
	inputType:'radio',
	comment:'comment_H162_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['162'],
	scoreMap:[1, 0]
},{
	id:'H163_0',
	category:CATEGORIES.SYMPTOMS,
	text:'H163_0',
	inputType:'radio',
	comment:'comment_H163_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H163_1', 'H163_1'],
	scoreMap:[1, 0]
},{
	id:'H163_1',
	category:CATEGORIES.SYMPTOMS,
	text:'H163_1',
	inputType:'radio',
	comment:'comment_H163_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H163_2', 'H163_2'],
	scoreMap:[1, 0]
},{
	id:'H163_2',
	category:CATEGORIES.SYMPTOMS,
	text:'H163_2',
	inputType:'radio',
	comment:'comment_H163_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H163_3', 'H163_3'],
	scoreMap:[1, 0]
},{
	id:'H163_3',
	category:CATEGORIES.SYMPTOMS,
	text:'H163_3',
	inputType:'radio',
	comment:'comment_H163_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['H163_4', 'H163_4'],
	scoreMap:[1, 0]
},{
	id:'H163_4',
	category:CATEGORIES.SYMPTOMS,
	text:'H163_4',
	inputType:'radio',
	comment:'comment_H163_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['163'],
	scoreMap:[1, 0]
},{
	id:'I164_0',
	category:CATEGORIES.SYMPTOMS,
	text:'I164_0',
	inputType:'radio',
	comment:'comment_I164_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I164_1', 'I164_1'],
	scoreMap:[1, 0]
},{
	id:'I164_1',
	category:CATEGORIES.SYMPTOMS,
	text:'I164_1',
	inputType:'radio',
	comment:'comment_I164_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I164_2', 'I164_2'],
	scoreMap:[1, 0]
},{
	id:'I164_2',
	category:CATEGORIES.SYMPTOMS,
	text:'I164_2',
	inputType:'radio',
	comment:'comment_I164_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I164_3', 'I164_3'],
	scoreMap:[1, 0]
},{
	id:'I164_3',
	category:CATEGORIES.SYMPTOMS,
	text:'I164_3',
	inputType:'radio',
	comment:'comment_I164_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['164'],
	scoreMap:[1, 0]
},{
	id:'I165_0',
	category:CATEGORIES.SYMPTOMS,
	text:'I165_0',
	inputType:'radio',
	comment:'comment_I165_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I165_1', 'I165_1'],
	scoreMap:[1, 0]
},{
	id:'I165_1',
	category:CATEGORIES.SYMPTOMS,
	text:'I165_1',
	inputType:'radio',
	comment:'comment_I165_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I165_2', 'I165_2'],
	scoreMap:[1, 0]
},{
	id:'I165_2',
	category:CATEGORIES.SYMPTOMS,
	text:'I165_2',
	inputType:'radio',
	comment:'comment_I165_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I165_3', 'I165_3'],
	scoreMap:[1, 0]
},{
	id:'I165_3',
	category:CATEGORIES.SYMPTOMS,
	text:'I165_3',
	inputType:'radio',
	comment:'comment_I165_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['165'],
	scoreMap:[1, 0]
},{
	id:'I166_0',
	category:CATEGORIES.SYMPTOMS,
	text:'I166_0',
	inputType:'radio',
	comment:'comment_I166_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I166_1', 'I166_1'],
	scoreMap:[1, 0]
},{
	id:'I166_1',
	category:CATEGORIES.SYMPTOMS,
	text:'I166_1',
	inputType:'radio',
	comment:'comment_I166_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I166_2', 'I166_2'],
	scoreMap:[1, 0]
},{
	id:'I166_2',
	category:CATEGORIES.SYMPTOMS,
	text:'I166_2',
	inputType:'radio',
	comment:'comment_I166_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I166_3', 'I166_3'],
	scoreMap:[1, 0]
},{
	id:'I166_3',
	category:CATEGORIES.SYMPTOMS,
	text:'I166_3',
	inputType:'radio',
	comment:'comment_I166_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I166_4', 'I166_4'],
	scoreMap:[1, 0]
},{
	id:'I166_4',
	category:CATEGORIES.SYMPTOMS,
	text:'I166_4',
	inputType:'radio',
	comment:'comment_I166_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['166'],
	scoreMap:[1, 0]
},{
	id:'I167_0',
	category:CATEGORIES.SYMPTOMS,
	text:'I167_0',
	inputType:'radio',
	comment:'comment_I167_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I167_1', 'I167_1'],
	scoreMap:[1, 0]
},{
	id:'I167_1',
	category:CATEGORIES.SYMPTOMS,
	text:'I167_1',
	inputType:'radio',
	comment:'comment_I167_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I167_2', 'I167_2'],
	scoreMap:[1, 0]
},{
	id:'I167_2',
	category:CATEGORIES.SYMPTOMS,
	text:'I167_2',
	inputType:'radio',
	comment:'comment_I167_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['167'],
	scoreMap:[1, 0]
},{
	id:'I168_0',
	category:CATEGORIES.SYMPTOMS,
	text:'I168_0',
	inputType:'radio',
	comment:'comment_I168_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I168_1', 'I168_1'],
	scoreMap:[1, 0]
},{
	id:'I168_1',
	category:CATEGORIES.SYMPTOMS,
	text:'I168_1',
	inputType:'radio',
	comment:'comment_I168_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I168_2', 'I168_2'],
	scoreMap:[1, 0]
},{
	id:'I168_2',
	category:CATEGORIES.SYMPTOMS,
	text:'I168_2',
	inputType:'radio',
	comment:'comment_I168_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I168_3', 'I168_3'],
	scoreMap:[1, 0]
},{
	id:'I168_3',
	category:CATEGORIES.SYMPTOMS,
	text:'I168_3',
	inputType:'radio',
	comment:'comment_I168_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I168_4', 'I168_4'],
	scoreMap:[1, 0]
},{
	id:'I168_4',
	category:CATEGORIES.SYMPTOMS,
	text:'I168_4',
	inputType:'radio',
	comment:'comment_I168_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I168_5', 'I168_5'],
	scoreMap:[1, 0]
},{
	id:'I168_5',
	category:CATEGORIES.SYMPTOMS,
	text:'I168_5',
	inputType:'radio',
	comment:'comment_I168_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I168_6', 'I168_6'],
	scoreMap:[1, 0]
},{
	id:'I168_6',
	category:CATEGORIES.SYMPTOMS,
	text:'I168_6',
	inputType:'radio',
	comment:'comment_I168_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['168'],
	scoreMap:[1, 0]
},{
	id:'I169_0',
	category:CATEGORIES.SYMPTOMS,
	text:'I169_0',
	inputType:'radio',
	comment:'comment_I169_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I169_1', 'I169_1'],
	scoreMap:[1, 0]
},{
	id:'I169_1',
	category:CATEGORIES.SYMPTOMS,
	text:'I169_1',
	inputType:'radio',
	comment:'comment_I169_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I169_2', 'I169_2'],
	scoreMap:[1, 0]
},{
	id:'I169_2',
	category:CATEGORIES.SYMPTOMS,
	text:'I169_2',
	inputType:'radio',
	comment:'comment_I169_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['169'],
	scoreMap:[1, 0]
},{
	id:'I170_0',
	category:CATEGORIES.SYMPTOMS,
	text:'I170_0',
	inputType:'radio',
	comment:'comment_I170_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I170_1', 'I170_1'],
	scoreMap:[1, 0]
},{
	id:'I170_1',
	category:CATEGORIES.SYMPTOMS,
	text:'I170_1',
	inputType:'radio',
	comment:'comment_I170_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I170_2', 'I170_2'],
	scoreMap:[1, 0]
},{
	id:'I170_2',
	category:CATEGORIES.SYMPTOMS,
	text:'I170_2',
	inputType:'radio',
	comment:'comment_I170_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['170'],
	scoreMap:[1, 0]
},{
	id:'I171_0',
	category:CATEGORIES.SYMPTOMS,
	text:'I171_0',
	inputType:'radio',
	comment:'comment_I171_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I171_1', 'I171_1'],
	scoreMap:[1, 0]
},{
	id:'I171_1',
	category:CATEGORIES.SYMPTOMS,
	text:'I171_1',
	inputType:'radio',
	comment:'comment_I171_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I171_2', 'I171_2'],
	scoreMap:[1, 0]
},{
	id:'I171_2',
	category:CATEGORIES.SYMPTOMS,
	text:'I171_2',
	inputType:'radio',
	comment:'comment_I171_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['171'],
	scoreMap:[1, 0]
},{
	id:'I172_0',
	category:CATEGORIES.SYMPTOMS,
	text:'I172_0',
	inputType:'radio',
	comment:'comment_I172_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I172_1', 'I172_1'],
	scoreMap:[1, 0]
},{
	id:'I172_1',
	category:CATEGORIES.SYMPTOMS,
	text:'I172_1',
	inputType:'radio',
	comment:'comment_I172_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I172_2', 'I172_2'],
	scoreMap:[1, 0]
},{
	id:'I172_2',
	category:CATEGORIES.SYMPTOMS,
	text:'I172_2',
	inputType:'radio',
	comment:'comment_I172_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I172_3', 'I172_3'],
	scoreMap:[1, 0]
},{
	id:'I172_3',
	category:CATEGORIES.SYMPTOMS,
	text:'I172_3',
	inputType:'radio',
	comment:'comment_I172_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['172'],
	scoreMap:[1, 0]
},{
	id:'I173_0',
	category:CATEGORIES.SYMPTOMS,
	text:'I173_0',
	inputType:'radio',
	comment:'comment_I173_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['I173_1', 'I173_1'],
	scoreMap:[1, 0]
},{
	id:'I173_1',
	category:CATEGORIES.SYMPTOMS,
	text:'I173_1',
	inputType:'radio',
	comment:'comment_I173_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['173'],
	scoreMap:[1, 0]
},{
	id:'I174_0',
	category:CATEGORIES.SYMPTOMS,
	text:'I174_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['174'],
	scoreMap:[1, 0]
},{
	id:'J175_0',
	category:CATEGORIES.SYMPTOMS,
	text:'J175_0',
	inputType:'radio',
	comment:'comment_J175_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J175_1', 'J175_1'],
	scoreMap:[1, 0]
},{
	id:'J175_1',
	category:CATEGORIES.SYMPTOMS,
	text:'J175_1',
	inputType:'radio',
	comment:'comment_J175_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J175_2', 'J175_2'],
	scoreMap:[1, 0]
},{
	id:'J175_2',
	category:CATEGORIES.SYMPTOMS,
	text:'J175_2',
	inputType:'radio',
	comment:'comment_J175_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J175_3', 'J175_3'],
	scoreMap:[1, 0]
},{
	id:'J175_3',
	category:CATEGORIES.SYMPTOMS,
	text:'J175_3',
	inputType:'radio',
	comment:'comment_J175_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J175_4', 'J175_4'],
	scoreMap:[1, 0]
},{
	id:'J175_4',
	category:CATEGORIES.SYMPTOMS,
	text:'J175_4',
	inputType:'radio',
	comment:'comment_J175_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J175_5', 'J175_5'],
	scoreMap:[1, 0]
},{
	id:'J175_5',
	category:CATEGORIES.SYMPTOMS,
	text:'J175_5',
	inputType:'radio',
	comment:'comment_J175_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J175_6', 'J175_6'],
	scoreMap:[1, 0]
},{
	id:'J175_6',
	category:CATEGORIES.SYMPTOMS,
	text:'J175_6',
	inputType:'radio',
	comment:'comment_J175_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['175'],
	scoreMap:[1, 0]
},{
	id:'J176_0',
	category:CATEGORIES.SYMPTOMS,
	text:'J176_0',
	inputType:'radio',
	comment:'comment_J176_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J176_1', 'J176_1'],
	scoreMap:[1, 0]
},{
	id:'J176_1',
	category:CATEGORIES.SYMPTOMS,
	text:'J176_1',
	inputType:'radio',
	comment:'comment_J176_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J176_2', 'J176_2'],
	scoreMap:[1, 0]
},{
	id:'J176_2',
	category:CATEGORIES.SYMPTOMS,
	text:'J176_2',
	inputType:'radio',
	comment:'comment_J176_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['176'],
	scoreMap:[1, 0]
},{
	id:'J177_0',
	category:CATEGORIES.SYMPTOMS,
	text:'J177_0',
	inputType:'radio',
	comment:'comment_J177_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J177_1', 'J177_1'],
	scoreMap:[1, 0]
},{
	id:'J177_1',
	category:CATEGORIES.SYMPTOMS,
	text:'J177_1',
	inputType:'radio',
	comment:'comment_J177_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J177_2', 'J177_2'],
	scoreMap:[1, 0]
},{
	id:'J177_2',
	category:CATEGORIES.SYMPTOMS,
	text:'J177_2',
	inputType:'radio',
	comment:'comment_J177_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J177_3', 'J177_3'],
	scoreMap:[1, 0]
},{
	id:'J177_3',
	category:CATEGORIES.SYMPTOMS,
	text:'J177_3',
	inputType:'radio',
	comment:'comment_J177_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['177'],
	scoreMap:[1, 0]
},{
	id:'J178_0',
	category:CATEGORIES.SYMPTOMS,
	text:'J178_0',
	inputType:'radio',
	comment:'comment_J178_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J178_1', 'J178_1'],
	scoreMap:[1, 0]
},{
	id:'J178_1',
	category:CATEGORIES.SYMPTOMS,
	text:'J178_1',
	inputType:'radio',
	comment:'comment_J178_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J178_2', 'J178_2'],
	scoreMap:[1, 0]
},{
	id:'J178_2',
	category:CATEGORIES.SYMPTOMS,
	text:'J178_2',
	inputType:'radio',
	comment:'comment_J178_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J178_3', 'J178_3'],
	scoreMap:[1, 0]
},{
	id:'J178_3',
	category:CATEGORIES.SYMPTOMS,
	text:'J178_3',
	inputType:'radio',
	comment:'comment_J178_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J178_4', 'J178_4'],
	scoreMap:[1, 0]
},{
	id:'J178_4',
	category:CATEGORIES.SYMPTOMS,
	text:'J178_4',
	inputType:'radio',
	comment:'comment_J178_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J178_5', 'J178_5'],
	scoreMap:[1, 0]
},{
	id:'J178_5',
	category:CATEGORIES.SYMPTOMS,
	text:'J178_5',
	inputType:'radio',
	comment:'comment_J178_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['178'],
	scoreMap:[1, 0]
},{
	id:'J179_0',
	category:CATEGORIES.SYMPTOMS,
	text:'J179_0',
	inputType:'radio',
	comment:'comment_J179_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J179_1', 'J179_1'],
	scoreMap:[1, 0]
},{
	id:'J179_1',
	category:CATEGORIES.SYMPTOMS,
	text:'J179_1',
	inputType:'radio',
	comment:'comment_J179_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J179_2', 'J179_2'],
	scoreMap:[1, 0]
},{
	id:'J179_2',
	category:CATEGORIES.SYMPTOMS,
	text:'J179_2',
	inputType:'radio',
	comment:'comment_J179_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J179_3', 'J179_3'],
	scoreMap:[1, 0]
},{
	id:'J179_3',
	category:CATEGORIES.SYMPTOMS,
	text:'J179_3',
	inputType:'radio',
	comment:'comment_J179_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['J179_4', 'J179_4'],
	scoreMap:[1, 0]
},{
	id:'J179_4',
	category:CATEGORIES.SYMPTOMS,
	text:'J179_4',
	inputType:'radio',
	comment:'comment_J179_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['179'],
	scoreMap:[1, 0]
},{
	id:'K180_0',
	category:CATEGORIES.SYMPTOMS,
	text:'K180_0',
	inputType:'radio',
	comment:'comment_K180_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['K180_1', 'K180_1'],
	scoreMap:[1, 0]
},{
	id:'K180_1',
	category:CATEGORIES.SYMPTOMS,
	text:'K180_1',
	inputType:'radio',
	comment:'comment_K180_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['K180_2', 'K180_2'],
	scoreMap:[1, 0]
},{
	id:'K180_2',
	category:CATEGORIES.SYMPTOMS,
	text:'K180_2',
	inputType:'radio',
	comment:'comment_K180_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['K180_3', 'K180_3'],
	scoreMap:[1, 0]
},{
	id:'K180_3',
	category:CATEGORIES.SYMPTOMS,
	text:'K180_3',
	inputType:'radio',
	comment:'comment_K180_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['K180_4', 'K180_4'],
	scoreMap:[1, 0]
},{
	id:'K180_4',
	category:CATEGORIES.SYMPTOMS,
	text:'K180_4',
	inputType:'radio',
	comment:'comment_K180_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['180'],
	scoreMap:[1, 0]
},{
	id:'K181_0',
	category:CATEGORIES.SYMPTOMS,
	text:'K181_0',
	inputType:'radio',
	comment:'comment_K181_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['K181_1', 'K181_1'],
	scoreMap:[1, 0]
},{
	id:'K181_1',
	category:CATEGORIES.SYMPTOMS,
	text:'K181_1',
	inputType:'radio',
	comment:'comment_K181_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['K181_2', 'K181_2'],
	scoreMap:[1, 0]
},{
	id:'K181_2',
	category:CATEGORIES.SYMPTOMS,
	text:'K181_2',
	inputType:'radio',
	comment:'comment_K181_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['K181_3', 'K181_3'],
	scoreMap:[1, 0]
},{
	id:'K181_3',
	category:CATEGORIES.SYMPTOMS,
	text:'K181_3',
	inputType:'radio',
	comment:'comment_K181_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['K181_4', 'K181_4'],
	scoreMap:[1, 0]
},{
	id:'K181_4',
	category:CATEGORIES.SYMPTOMS,
	text:'K181_4',
	inputType:'radio',
	comment:'comment_K181_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['181'],
	scoreMap:[1, 0]
},{
	id:'K182_0',
	category:CATEGORIES.SYMPTOMS,
	text:'K182_0',
	inputType:'radio',
	comment:'comment_K182_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['K182_1', 'K182_1'],
	scoreMap:[1, 0]
},{
	id:'K182_1',
	category:CATEGORIES.SYMPTOMS,
	text:'K182_1',
	inputType:'radio',
	comment:'comment_K182_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['K182_2', 'K182_2'],
	scoreMap:[1, 0]
},{
	id:'K182_2',
	category:CATEGORIES.SYMPTOMS,
	text:'K182_2',
	inputType:'radio',
	comment:'comment_K182_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['182'],
	scoreMap:[1, 0]
},{
	id:'L183_0',
	category:CATEGORIES.SYMPTOMS,
	text:'L183_0',
	inputType:'radio',
	comment:'comment_L183_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L183_1', 'L183_1'],
	scoreMap:[1, 0]
},{
	id:'L183_1',
	category:CATEGORIES.SYMPTOMS,
	text:'L183_1',
	inputType:'radio',
	comment:'comment_L183_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L183_2', 'L183_2'],
	scoreMap:[1, 0]
},{
	id:'L183_2',
	category:CATEGORIES.SYMPTOMS,
	text:'L183_2',
	inputType:'radio',
	comment:'comment_L183_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L183_3', 'L183_3'],
	scoreMap:[1, 0]
},{
	id:'L183_3',
	category:CATEGORIES.SYMPTOMS,
	text:'L183_3',
	inputType:'radio',
	comment:'comment_L183_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['183'],
	scoreMap:[1, 0]
},{
	id:'L184_0',
	category:CATEGORIES.SYMPTOMS,
	text:'L184_0',
	inputType:'radio',
	comment:'comment_L184_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L184_1', 'L184_1'],
	scoreMap:[1, 0]
},{
	id:'L184_1',
	category:CATEGORIES.SYMPTOMS,
	text:'L184_1',
	inputType:'radio',
	comment:'comment_L184_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L184_2', 'L184_2'],
	scoreMap:[1, 0]
},{
	id:'L184_2',
	category:CATEGORIES.SYMPTOMS,
	text:'L184_2',
	inputType:'radio',
	comment:'comment_L184_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['184'],
	scoreMap:[1, 0]
},{
	id:'L185_0',
	category:CATEGORIES.SYMPTOMS,
	text:'L185_0',
	inputType:'radio',
	comment:'comment_L185_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L185_1', 'L185_1'],
	scoreMap:[1, 0]
},{
	id:'L185_1',
	category:CATEGORIES.SYMPTOMS,
	text:'L185_1',
	inputType:'radio',
	comment:'comment_L185_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L185_2', 'L185_2'],
	scoreMap:[1, 0]
},{
	id:'L185_2',
	category:CATEGORIES.SYMPTOMS,
	text:'L185_2',
	inputType:'radio',
	comment:'comment_L185_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L185_3', 'L185_3'],
	scoreMap:[1, 0]
},{
	id:'L185_3',
	category:CATEGORIES.SYMPTOMS,
	text:'L185_3',
	inputType:'radio',
	comment:'comment_L185_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L185_4', 'L185_4'],
	scoreMap:[1, 0]
},{
	id:'L185_4',
	category:CATEGORIES.SYMPTOMS,
	text:'L185_4',
	inputType:'radio',
	comment:'comment_L185_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L185_5', 'L185_5'],
	scoreMap:[1, 0]
},{
	id:'L185_5',
	category:CATEGORIES.SYMPTOMS,
	text:'L185_5',
	inputType:'radio',
	comment:'comment_L185_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['185'],
	scoreMap:[1, 0]
},{
	id:'L186_0',
	category:CATEGORIES.SYMPTOMS,
	text:'L186_0',
	inputType:'radio',
	comment:'comment_L186_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L186_1', 'L186_1'],
	scoreMap:[1, 0]
},{
	id:'L186_1',
	category:CATEGORIES.SYMPTOMS,
	text:'L186_1',
	inputType:'radio',
	comment:'comment_L186_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L186_2', 'L186_2'],
	scoreMap:[1, 0]
},{
	id:'L186_2',
	category:CATEGORIES.SYMPTOMS,
	text:'L186_2',
	inputType:'radio',
	comment:'comment_L186_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['186'],
	scoreMap:[1, 0]
},{
	id:'L187_0',
	category:CATEGORIES.SYMPTOMS,
	text:'L187_0',
	inputType:'radio',
	comment:'comment_L187_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L187_1', 'L187_1'],
	scoreMap:[1, 0]
},{
	id:'L187_1',
	category:CATEGORIES.SYMPTOMS,
	text:'L187_1',
	inputType:'radio',
	comment:'comment_L187_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L187_2', 'L187_2'],
	scoreMap:[1, 0]
},{
	id:'L187_2',
	category:CATEGORIES.SYMPTOMS,
	text:'L187_2',
	inputType:'radio',
	comment:'comment_L187_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['187'],
	scoreMap:[1, 0]
},{
	id:'L188_0',
	category:CATEGORIES.SYMPTOMS,
	text:'L188_0',
	inputType:'radio',
	comment:'comment_L188_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['188'],
	scoreMap:[1, 0]
},{
	id:'L189_0',
	category:CATEGORIES.SYMPTOMS,
	text:'L189_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['189'],
	scoreMap:[1, 0]
},{
	id:'L190_0',
	category:CATEGORIES.SYMPTOMS,
	text:'L190_0',
	inputType:'radio',
	comment:'comment_L190_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L190_1', 'L190_1'],
	scoreMap:[1, 0]
},{
	id:'L190_1',
	category:CATEGORIES.SYMPTOMS,
	text:'L190_1',
	inputType:'radio',
	comment:'comment_L190_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['L190_2', 'L190_2'],
	scoreMap:[1, 0]
},{
	id:'L190_2',
	category:CATEGORIES.SYMPTOMS,
	text:'L190_2',
	inputType:'radio',
	comment:'comment_L190_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['190'],
	scoreMap:[1, 0]
},{
	id:'M191_0',
	category:CATEGORIES.SYMPTOMS,
	text:'M191_0',
	inputType:'radio',
	comment:'comment_M191_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M191_1', 'M191_1'],
	scoreMap:[1, 0]
},{
	id:'M191_1',
	category:CATEGORIES.SYMPTOMS,
	text:'M191_1',
	inputType:'radio',
	comment:'comment_M191_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M191_2', 'M191_2'],
	scoreMap:[1, 0]
},{
	id:'M191_2',
	category:CATEGORIES.SYMPTOMS,
	text:'M191_2',
	inputType:'radio',
	comment:'comment_M191_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M191_3', 'M191_3'],
	scoreMap:[1, 0]
},{
	id:'M191_3',
	category:CATEGORIES.SYMPTOMS,
	text:'M191_3',
	inputType:'radio',
	comment:'comment_M191_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['191'],
	scoreMap:[1, 0]
},{
	id:'M192_0',
	category:CATEGORIES.SYMPTOMS,
	text:'M192_0',
	inputType:'radio',
	comment:'comment_M192_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M192_1', 'M192_1'],
	scoreMap:[1, 0]
},{
	id:'M192_1',
	category:CATEGORIES.SYMPTOMS,
	text:'M192_1',
	inputType:'radio',
	comment:'comment_M192_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M192_2', 'M192_2'],
	scoreMap:[1, 0]
},{
	id:'M192_2',
	category:CATEGORIES.SYMPTOMS,
	text:'M192_2',
	inputType:'radio',
	comment:'comment_M192_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M192_3', 'M192_3'],
	scoreMap:[1, 0]
},{
	id:'M192_3',
	category:CATEGORIES.SYMPTOMS,
	text:'M192_3',
	inputType:'radio',
	comment:'comment_M192_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['192'],
	scoreMap:[1, 0]
},{
	id:'M193_0',
	category:CATEGORIES.SYMPTOMS,
	text:'M193_0',
	inputType:'radio',
	comment:'comment_M193_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M193_1', 'M193_1'],
	scoreMap:[1, 0]
},{
	id:'M193_1',
	category:CATEGORIES.SYMPTOMS,
	text:'M193_1',
	inputType:'radio',
	comment:'comment_M193_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M193_2', 'M193_2'],
	scoreMap:[1, 0]
},{
	id:'M193_2',
	category:CATEGORIES.SYMPTOMS,
	text:'M193_2',
	inputType:'radio',
	comment:'comment_M193_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['193'],
	scoreMap:[1, 0]
},{
	id:'M194_0',
	category:CATEGORIES.SYMPTOMS,
	text:'M194_0',
	inputType:'radio',
	comment:'comment_M194_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M194_1', 'M194_1'],
	scoreMap:[1, 0]
},{
	id:'M194_1',
	category:CATEGORIES.SYMPTOMS,
	text:'M194_1',
	inputType:'radio',
	comment:'comment_M194_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M194_2', 'M194_2'],
	scoreMap:[1, 0]
},{
	id:'M194_2',
	category:CATEGORIES.SYMPTOMS,
	text:'M194_2',
	inputType:'radio',
	comment:'comment_M194_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M194_3', 'M194_3'],
	scoreMap:[1, 0]
},{
	id:'M194_3',
	category:CATEGORIES.SYMPTOMS,
	text:'M194_3',
	inputType:'radio',
	comment:'comment_M194_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M194_4', 'M194_4'],
	scoreMap:[1, 0]
},{
	id:'M194_4',
	category:CATEGORIES.SYMPTOMS,
	text:'M194_4',
	inputType:'radio',
	comment:'comment_M194_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M194_5', 'M194_5'],
	scoreMap:[1, 0]
},{
	id:'M194_5',
	category:CATEGORIES.SYMPTOMS,
	text:'M194_5',
	inputType:'radio',
	comment:'comment_M194_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['194'],
	scoreMap:[1, 0]
},{
	id:'M195_0',
	category:CATEGORIES.SYMPTOMS,
	text:'M195_0',
	inputType:'radio',
	comment:'comment_M195_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M195_1', 'M195_1'],
	scoreMap:[1, 0]
},{
	id:'M195_1',
	category:CATEGORIES.SYMPTOMS,
	text:'M195_1',
	inputType:'radio',
	comment:'comment_M195_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M195_2', 'M195_2'],
	scoreMap:[1, 0]
},{
	id:'M195_2',
	category:CATEGORIES.SYMPTOMS,
	text:'M195_2',
	inputType:'radio',
	comment:'comment_M195_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M195_3', 'M195_3'],
	scoreMap:[1, 0]
},{
	id:'M195_3',
	category:CATEGORIES.SYMPTOMS,
	text:'M195_3',
	inputType:'radio',
	comment:'comment_M195_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['195'],
	scoreMap:[1, 0]
},{
	id:'M196_0',
	category:CATEGORIES.SYMPTOMS,
	text:'M196_0',
	inputType:'radio',
	comment:'comment_M196_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M196_1', 'M196_1'],
	scoreMap:[1, 0]
},{
	id:'M196_1',
	category:CATEGORIES.SYMPTOMS,
	text:'M196_1',
	inputType:'radio',
	comment:'comment_M196_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M196_2', 'M196_2'],
	scoreMap:[1, 0]
},{
	id:'M196_2',
	category:CATEGORIES.SYMPTOMS,
	text:'M196_2',
	inputType:'radio',
	comment:'comment_M196_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M196_3', 'M196_3'],
	scoreMap:[1, 0]
},{
	id:'M196_3',
	category:CATEGORIES.SYMPTOMS,
	text:'M196_3',
	inputType:'radio',
	comment:'comment_M196_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M196_4', 'M196_4'],
	scoreMap:[1, 0]
},{
	id:'M196_4',
	category:CATEGORIES.SYMPTOMS,
	text:'M196_4',
	inputType:'radio',
	comment:'comment_M196_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M196_5', 'M196_5'],
	scoreMap:[1, 0]
},{
	id:'M196_5',
	category:CATEGORIES.SYMPTOMS,
	text:'M196_5',
	inputType:'radio',
	comment:'comment_M196_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M196_6', 'M196_6'],
	scoreMap:[1, 0]
},{
	id:'M196_6',
	category:CATEGORIES.SYMPTOMS,
	text:'M196_6',
	inputType:'radio',
	comment:'comment_M196_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['196'],
	scoreMap:[1, 0]
},{
	id:'M197_0',
	category:CATEGORIES.SYMPTOMS,
	text:'M197_0',
	inputType:'radio',
	comment:'comment_M197_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M197_1', 'M197_1'],
	scoreMap:[1, 0]
},{
	id:'M197_1',
	category:CATEGORIES.SYMPTOMS,
	text:'M197_1',
	inputType:'radio',
	comment:'comment_M197_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M197_2', 'M197_2'],
	scoreMap:[1, 0]
},{
	id:'M197_2',
	category:CATEGORIES.SYMPTOMS,
	text:'M197_2',
	inputType:'radio',
	comment:'comment_M197_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M197_3', 'M197_3'],
	scoreMap:[1, 0]
},{
	id:'M197_3',
	category:CATEGORIES.SYMPTOMS,
	text:'M197_3',
	inputType:'radio',
	comment:'comment_M197_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['197'],
	scoreMap:[1, 0]
},{
	id:'M198_0',
	category:CATEGORIES.SYMPTOMS,
	text:'M198_0',
	inputType:'radio',
	comment:'comment_M198_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M198_1', 'M198_1'],
	scoreMap:[1, 0]
},{
	id:'M198_1',
	category:CATEGORIES.SYMPTOMS,
	text:'M198_1',
	inputType:'radio',
	comment:'comment_M198_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['198'],
	scoreMap:[1, 0]
},{
	id:'M199_0',
	category:CATEGORIES.SYMPTOMS,
	text:'M199_0',
	inputType:'radio',
	comment:'comment_M199_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M199_1', 'M199_1'],
	scoreMap:[1, 0]
},{
	id:'M199_1',
	category:CATEGORIES.SYMPTOMS,
	text:'M199_1',
	inputType:'radio',
	comment:'comment_M199_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M199_2', 'M199_2'],
	scoreMap:[1, 0]
},{
	id:'M199_2',
	category:CATEGORIES.SYMPTOMS,
	text:'M199_2',
	inputType:'radio',
	comment:'comment_M199_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M199_3', 'M199_3'],
	scoreMap:[1, 0]
},{
	id:'M199_3',
	category:CATEGORIES.SYMPTOMS,
	text:'M199_3',
	inputType:'radio',
	comment:'comment_M199_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['199'],
	scoreMap:[1, 0]
},{
	id:'M200_0',
	category:CATEGORIES.SYMPTOMS,
	text:'M200_0',
	inputType:'radio',
	comment:'comment_M200_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M200_1', 'M200_1'],
	scoreMap:[1, 0]
},{
	id:'M200_1',
	category:CATEGORIES.SYMPTOMS,
	text:'M200_1',
	inputType:'radio',
	comment:'comment_M200_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M200_2', 'M200_2'],
	scoreMap:[1, 0]
},{
	id:'M200_2',
	category:CATEGORIES.SYMPTOMS,
	text:'M200_2',
	inputType:'radio',
	comment:'comment_M200_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M200_3', 'M200_3'],
	scoreMap:[1, 0]
},{
	id:'M200_3',
	category:CATEGORIES.SYMPTOMS,
	text:'M200_3',
	inputType:'radio',
	comment:'comment_M200_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['M200_4', 'M200_4'],
	scoreMap:[1, 0]
},{
	id:'M200_4',
	category:CATEGORIES.SYMPTOMS,
	text:'M200_4',
	inputType:'radio',
	comment:'comment_M200_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['200'],
	scoreMap:[1, 0]
},{
	id:'N201_0',
	category:CATEGORIES.SYMPTOMS,
	text:'N201_0',
	inputType:'radio',
	comment:'comment_N201_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['201'],
	scoreMap:[1, 0]
},{
	id:'N202_0',
	category:CATEGORIES.SYMPTOMS,
	text:'N202_0',
	inputType:'radio',
	comment:'comment_N202_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N202_1', 'N202_1'],
	scoreMap:[1, 0]
},{
	id:'N202_1',
	category:CATEGORIES.SYMPTOMS,
	text:'N202_1',
	inputType:'radio',
	comment:'comment_N202_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N202_2', 'N202_2'],
	scoreMap:[1, 0]
},{
	id:'N202_2',
	category:CATEGORIES.SYMPTOMS,
	text:'N202_2',
	inputType:'radio',
	comment:'comment_N202_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N202_3', 'N202_3'],
	scoreMap:[1, 0]
},{
	id:'N202_3',
	category:CATEGORIES.SYMPTOMS,
	text:'N202_3',
	inputType:'radio',
	comment:'comment_N202_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N202_4', 'N202_4'],
	scoreMap:[1, 0]
},{
	id:'N202_4',
	category:CATEGORIES.SYMPTOMS,
	text:'N202_4',
	inputType:'radio',
	comment:'comment_N202_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N202_5', 'N202_5'],
	scoreMap:[1, 0]
},{
	id:'N202_5',
	category:CATEGORIES.SYMPTOMS,
	text:'N202_5',
	inputType:'radio',
	comment:'comment_N202_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['202'],
	scoreMap:[1, 0]
},{
	id:'N203_0',
	category:CATEGORIES.SYMPTOMS,
	text:'N203_0',
	inputType:'radio',
	comment:'comment_N203_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N203_1', 'N203_1'],
	scoreMap:[1, 0]
},{
	id:'N203_1',
	category:CATEGORIES.SYMPTOMS,
	text:'N203_1',
	inputType:'radio',
	comment:'comment_N203_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N203_2', 'N203_2'],
	scoreMap:[1, 0]
},{
	id:'N203_2',
	category:CATEGORIES.SYMPTOMS,
	text:'N203_2',
	inputType:'radio',
	comment:'comment_N203_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['203'],
	scoreMap:[1, 0]
},{
	id:'N204_0',
	category:CATEGORIES.SYMPTOMS,
	text:'N204_0',
	inputType:'radio',
	comment:'comment_N204_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N204_1', 'N204_1'],
	scoreMap:[1, 0]
},{
	id:'N204_1',
	category:CATEGORIES.SYMPTOMS,
	text:'N204_1',
	inputType:'radio',
	comment:'comment_N204_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N204_2', 'N204_2'],
	scoreMap:[1, 0]
},{
	id:'N204_2',
	category:CATEGORIES.SYMPTOMS,
	text:'N204_2',
	inputType:'radio',
	comment:'comment_N204_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N204_3', 'N204_3'],
	scoreMap:[1, 0]
},{
	id:'N204_3',
	category:CATEGORIES.SYMPTOMS,
	text:'N204_3',
	inputType:'radio',
	comment:'comment_N204_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N204_4', 'N204_4'],
	scoreMap:[1, 0]
},{
	id:'N204_4',
	category:CATEGORIES.SYMPTOMS,
	text:'N204_4',
	inputType:'radio',
	comment:'comment_N204_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N204_5', 'N204_5'],
	scoreMap:[1, 0]
},{
	id:'N204_5',
	category:CATEGORIES.SYMPTOMS,
	text:'N204_5',
	inputType:'radio',
	comment:'comment_N204_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['204'],
	scoreMap:[1, 0]
},{
	id:'N205_0',
	category:CATEGORIES.SYMPTOMS,
	text:'N205_0',
	inputType:'radio',
	comment:'comment_N205_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N205_1', 'N205_1'],
	scoreMap:[1, 0]
},{
	id:'N205_1',
	category:CATEGORIES.SYMPTOMS,
	text:'N205_1',
	inputType:'radio',
	comment:'comment_N205_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N205_2', 'N205_2'],
	scoreMap:[1, 0]
},{
	id:'N205_2',
	category:CATEGORIES.SYMPTOMS,
	text:'N205_2',
	inputType:'radio',
	comment:'comment_N205_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['205'],
	scoreMap:[1, 0]
},{
	id:'N206_0',
	category:CATEGORIES.SYMPTOMS,
	text:'N206_0',
	inputType:'radio',
	comment:'comment_N206_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N206_1', 'N206_1'],
	scoreMap:[1, 0]
},{
	id:'N206_1',
	category:CATEGORIES.SYMPTOMS,
	text:'N206_1',
	inputType:'radio',
	comment:'comment_N206_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N206_2', 'N206_2'],
	scoreMap:[1, 0]
},{
	id:'N206_2',
	category:CATEGORIES.SYMPTOMS,
	text:'N206_2',
	inputType:'radio',
	comment:'comment_N206_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N206_3', 'N206_3'],
	scoreMap:[1, 0]
},{
	id:'N206_3',
	category:CATEGORIES.SYMPTOMS,
	text:'N206_3',
	inputType:'radio',
	comment:'comment_N206_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['206'],
	scoreMap:[1, 0]
},{
	id:'N207_0',
	category:CATEGORIES.SYMPTOMS,
	text:'N207_0',
	inputType:'radio',
	comment:'comment_N207_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N207_1', 'N207_1'],
	scoreMap:[1, 0]
},{
	id:'N207_1',
	category:CATEGORIES.SYMPTOMS,
	text:'N207_1',
	inputType:'radio',
	comment:'comment_N207_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N207_2', 'N207_2'],
	scoreMap:[1, 0]
},{
	id:'N207_2',
	category:CATEGORIES.SYMPTOMS,
	text:'N207_2',
	inputType:'radio',
	comment:'comment_N207_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N207_3', 'N207_3'],
	scoreMap:[1, 0]
},{
	id:'N207_3',
	category:CATEGORIES.SYMPTOMS,
	text:'N207_3',
	inputType:'radio',
	comment:'comment_N207_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N207_4', 'N207_4'],
	scoreMap:[1, 0]
},{
	id:'N207_4',
	category:CATEGORIES.SYMPTOMS,
	text:'N207_4',
	inputType:'radio',
	comment:'comment_N207_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['207'],
	scoreMap:[1, 0]
},{
	id:'N208_0',
	category:CATEGORIES.SYMPTOMS,
	text:'N208_0',
	inputType:'radio',
	comment:'comment_N208_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N208_1', 'N208_1'],
	scoreMap:[1, 0]
},{
	id:'N208_1',
	category:CATEGORIES.SYMPTOMS,
	text:'N208_1',
	inputType:'radio',
	comment:'comment_N208_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N208_2', 'N208_2'],
	scoreMap:[1, 0]
},{
	id:'N208_2',
	category:CATEGORIES.SYMPTOMS,
	text:'N208_2',
	inputType:'radio',
	comment:'comment_N208_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N208_3', 'N208_3'],
	scoreMap:[1, 0]
},{
	id:'N208_3',
	category:CATEGORIES.SYMPTOMS,
	text:'N208_3',
	inputType:'radio',
	comment:'comment_N208_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['208'],
	scoreMap:[1, 0]
},{
	id:'N209_0',
	category:CATEGORIES.SYMPTOMS,
	text:'N209_0',
	inputType:'radio',
	comment:'comment_N209_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N209_1', 'N209_1'],
	scoreMap:[1, 0]
},{
	id:'N209_1',
	category:CATEGORIES.SYMPTOMS,
	text:'N209_1',
	inputType:'radio',
	comment:'comment_N209_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N209_2', 'N209_2'],
	scoreMap:[1, 0]
},{
	id:'N209_2',
	category:CATEGORIES.SYMPTOMS,
	text:'N209_2',
	inputType:'radio',
	comment:'comment_N209_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['209'],
	scoreMap:[1, 0]
},{
	id:'N210_0',
	category:CATEGORIES.SYMPTOMS,
	text:'N210_0',
	inputType:'radio',
	comment:'comment_N210_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N210_1', 'N210_1'],
	scoreMap:[1, 0]
},{
	id:'N210_1',
	category:CATEGORIES.SYMPTOMS,
	text:'N210_1',
	inputType:'radio',
	comment:'comment_N210_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N210_2', 'N210_2'],
	scoreMap:[1, 0]
},{
	id:'N210_2',
	category:CATEGORIES.SYMPTOMS,
	text:'N210_2',
	inputType:'radio',
	comment:'comment_N210_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N210_3', 'N210_3'],
	scoreMap:[1, 0]
},{
	id:'N210_3',
	category:CATEGORIES.SYMPTOMS,
	text:'N210_3',
	inputType:'radio',
	comment:'comment_N210_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N210_4', 'N210_4'],
	scoreMap:[1, 0]
},{
	id:'N210_4',
	category:CATEGORIES.SYMPTOMS,
	text:'N210_4',
	inputType:'radio',
	comment:'comment_N210_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['210'],
	scoreMap:[1, 0]
},{
	id:'N211_0',
	category:CATEGORIES.SYMPTOMS,
	text:'N211_0',
	inputType:'radio',
	comment:'comment_N211_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N211_1', 'N211_1'],
	scoreMap:[1, 0]
},{
	id:'N211_1',
	category:CATEGORIES.SYMPTOMS,
	text:'N211_1',
	inputType:'radio',
	comment:'comment_N211_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N211_2', 'N211_2'],
	scoreMap:[1, 0]
},{
	id:'N211_2',
	category:CATEGORIES.SYMPTOMS,
	text:'N211_2',
	inputType:'radio',
	comment:'comment_N211_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N211_3', 'N211_3'],
	scoreMap:[1, 0]
},{
	id:'N211_3',
	category:CATEGORIES.SYMPTOMS,
	text:'N211_3',
	inputType:'radio',
	comment:'comment_N211_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N211_4', 'N211_4'],
	scoreMap:[1, 0]
},{
	id:'N211_4',
	category:CATEGORIES.SYMPTOMS,
	text:'N211_4',
	inputType:'radio',
	comment:'comment_N211_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['211'],
	scoreMap:[1, 0]
},{
	id:'N212_0',
	category:CATEGORIES.SYMPTOMS,
	text:'N212_0',
	inputType:'radio',
	comment:'comment_N212_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N212_1', 'N212_1'],
	scoreMap:[1, 0]
},{
	id:'N212_1',
	category:CATEGORIES.SYMPTOMS,
	text:'N212_1',
	inputType:'radio',
	comment:'comment_N212_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N212_2', 'N212_2'],
	scoreMap:[1, 0]
},{
	id:'N212_2',
	category:CATEGORIES.SYMPTOMS,
	text:'N212_2',
	inputType:'radio',
	comment:'comment_N212_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N212_3', 'N212_3'],
	scoreMap:[1, 0]
},{
	id:'N212_3',
	category:CATEGORIES.SYMPTOMS,
	text:'N212_3',
	inputType:'radio',
	comment:'comment_N212_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N212_4', 'N212_4'],
	scoreMap:[1, 0]
},{
	id:'N212_4',
	category:CATEGORIES.SYMPTOMS,
	text:'N212_4',
	inputType:'radio',
	comment:'comment_N212_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['N212_5', 'N212_5'],
	scoreMap:[1, 0]
},{
	id:'N212_5',
	category:CATEGORIES.SYMPTOMS,
	text:'N212_5',
	inputType:'radio',
	comment:'comment_N212_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['212'],
	scoreMap:[1, 0]
},{
	id:'O213_0',
	category:CATEGORIES.SYMPTOMS,
	text:'O213_0',
	inputType:'radio',
	comment:'comment_O213_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['O213_1', 'O213_1'],
	scoreMap:[1, 0]
},{
	id:'O213_1',
	category:CATEGORIES.SYMPTOMS,
	text:'O213_1',
	inputType:'radio',
	comment:'comment_O213_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['O213_2', 'O213_2'],
	scoreMap:[1, 0]
},{
	id:'O213_2',
	category:CATEGORIES.SYMPTOMS,
	text:'O213_2',
	inputType:'radio',
	comment:'comment_O213_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['O213_3', 'O213_3'],
	scoreMap:[1, 0]
},{
	id:'O213_3',
	category:CATEGORIES.SYMPTOMS,
	text:'O213_3',
	inputType:'radio',
	comment:'comment_O213_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['O213_4', 'O213_4'],
	scoreMap:[1, 0]
},{
	id:'O213_4',
	category:CATEGORIES.SYMPTOMS,
	text:'O213_4',
	inputType:'radio',
	comment:'comment_O213_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['O213_5', 'O213_5'],
	scoreMap:[1, 0]
},{
	id:'O213_5',
	category:CATEGORIES.SYMPTOMS,
	text:'O213_5',
	inputType:'radio',
	comment:'comment_O213_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['213'],
	scoreMap:[1, 0]
},{
	id:'O214_0',
	category:CATEGORIES.SYMPTOMS,
	text:'O214_0',
	inputType:'radio',
	comment:'comment_O214_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['O214_1', 'O214_1'],
	scoreMap:[1, 0]
},{
	id:'O214_1',
	category:CATEGORIES.SYMPTOMS,
	text:'O214_1',
	inputType:'radio',
	comment:'comment_O214_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['O214_2', 'O214_2'],
	scoreMap:[1, 0]
},{
	id:'O214_2',
	category:CATEGORIES.SYMPTOMS,
	text:'O214_2',
	inputType:'radio',
	comment:'comment_O214_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['214'],
	scoreMap:[1, 0]
},{
	id:'O215_0',
	category:CATEGORIES.SYMPTOMS,
	text:'O215_0',
	inputType:'radio',
	comment:'comment_O215_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['O215_1', 'O215_1'],
	scoreMap:[1, 0]
},{
	id:'O215_1',
	category:CATEGORIES.SYMPTOMS,
	text:'O215_1',
	inputType:'radio',
	comment:'comment_O215_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['O215_2', 'O215_2'],
	scoreMap:[1, 0]
},{
	id:'O215_2',
	category:CATEGORIES.SYMPTOMS,
	text:'O215_2',
	inputType:'radio',
	comment:'comment_O215_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['O215_3', 'O215_3'],
	scoreMap:[1, 0]
},{
	id:'O215_3',
	category:CATEGORIES.SYMPTOMS,
	text:'O215_3',
	inputType:'radio',
	comment:'comment_O215_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['O215_4', 'O215_4'],
	scoreMap:[1, 0]
},{
	id:'O215_4',
	category:CATEGORIES.SYMPTOMS,
	text:'O215_4',
	inputType:'radio',
	comment:'comment_O215_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['215'],
	scoreMap:[1, 0]
},{
	id:'O216_0',
	category:CATEGORIES.SYMPTOMS,
	text:'O216_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['216'],
	scoreMap:[1, 0]
},{
	id:'P217_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P217_0',
	inputType:'radio',
	comment:'comment_P217_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P217_1', 'P217_1'],
	scoreMap:[1, 0]
},{
	id:'P217_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P217_1',
	inputType:'radio',
	comment:'comment_P217_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P217_2', 'P217_2'],
	scoreMap:[1, 0]
},{
	id:'P217_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P217_2',
	inputType:'radio',
	comment:'comment_P217_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P217_3', 'P217_3'],
	scoreMap:[1, 0]
},{
	id:'P217_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P217_3',
	inputType:'radio',
	comment:'comment_P217_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P217_4', 'P217_4'],
	scoreMap:[1, 0]
},{
	id:'P217_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P217_4',
	inputType:'radio',
	comment:'comment_P217_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['217'],
	scoreMap:[1, 0]
},{
	id:'P218_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P218_0',
	inputType:'radio',
	comment:'comment_P218_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P218_1', 'P218_1'],
	scoreMap:[1, 0]
},{
	id:'P218_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P218_1',
	inputType:'radio',
	comment:'comment_P218_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['218'],
	scoreMap:[1, 0]
},{
	id:'P219_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P219_0',
	inputType:'radio',
	comment:'comment_P219_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P219_1', 'P219_1'],
	scoreMap:[1, 0]
},{
	id:'P219_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P219_1',
	inputType:'radio',
	comment:'comment_P219_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P219_2', 'P219_2'],
	scoreMap:[1, 0]
},{
	id:'P219_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P219_2',
	inputType:'radio',
	comment:'comment_P219_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['219'],
	scoreMap:[1, 0]
},{
	id:'P220_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P220_0',
	inputType:'radio',
	comment:'comment_P220_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P220_1', 'P220_1'],
	scoreMap:[1, 0]
},{
	id:'P220_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P220_1',
	inputType:'radio',
	comment:'comment_P220_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P220_2', 'P220_2'],
	scoreMap:[1, 0]
},{
	id:'P220_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P220_2',
	inputType:'radio',
	comment:'comment_P220_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P220_3', 'P220_3'],
	scoreMap:[1, 0]
},{
	id:'P220_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P220_3',
	inputType:'radio',
	comment:'comment_P220_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['220'],
	scoreMap:[1, 0]
},{
	id:'P221_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P221_0',
	inputType:'radio',
	comment:'comment_P221_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P221_1', 'P221_1'],
	scoreMap:[1, 0]
},{
	id:'P221_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P221_1',
	inputType:'radio',
	comment:'comment_P221_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P221_2', 'P221_2'],
	scoreMap:[1, 0]
},{
	id:'P221_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P221_2',
	inputType:'radio',
	comment:'comment_P221_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P221_3', 'P221_3'],
	scoreMap:[1, 0]
},{
	id:'P221_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P221_3',
	inputType:'radio',
	comment:'comment_P221_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P221_4', 'P221_4'],
	scoreMap:[1, 0]
},{
	id:'P221_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P221_4',
	inputType:'radio',
	comment:'comment_P221_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P221_5', 'P221_5'],
	scoreMap:[1, 0]
},{
	id:'P221_5',
	category:CATEGORIES.SYMPTOMS,
	text:'P221_5',
	inputType:'radio',
	comment:'comment_P221_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P221_6', 'P221_6'],
	scoreMap:[1, 0]
},{
	id:'P221_6',
	category:CATEGORIES.SYMPTOMS,
	text:'P221_6',
	inputType:'radio',
	comment:'comment_P221_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P221_7', 'P221_7'],
	scoreMap:[1, 0]
},{
	id:'P221_7',
	category:CATEGORIES.SYMPTOMS,
	text:'P221_7',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['221'],
	scoreMap:[1, 0]
},{
	id:'P222_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P222_0',
	inputType:'radio',
	comment:'comment_P222_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P222_1', 'P222_1'],
	scoreMap:[1, 0]
},{
	id:'P222_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P222_1',
	inputType:'radio',
	comment:'comment_P222_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P222_2', 'P222_2'],
	scoreMap:[1, 0]
},{
	id:'P222_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P222_2',
	inputType:'radio',
	comment:'comment_P222_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P222_3', 'P222_3'],
	scoreMap:[1, 0]
},{
	id:'P222_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P222_3',
	inputType:'radio',
	comment:'comment_P222_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P222_4', 'P222_4'],
	scoreMap:[1, 0]
},{
	id:'P222_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P222_4',
	inputType:'radio',
	comment:'comment_P222_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P222_5', 'P222_5'],
	scoreMap:[1, 0]
},{
	id:'P222_5',
	category:CATEGORIES.SYMPTOMS,
	text:'P222_5',
	inputType:'radio',
	comment:'comment_P222_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P222_6', 'P222_6'],
	scoreMap:[1, 0]
},{
	id:'P222_6',
	category:CATEGORIES.SYMPTOMS,
	text:'P222_6',
	inputType:'radio',
	comment:'comment_P222_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['222'],
	scoreMap:[1, 0]
},{
	id:'P223_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P223_0',
	inputType:'radio',
	comment:'comment_P223_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P223_1', 'P223_1'],
	scoreMap:[1, 0]
},{
	id:'P223_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P223_1',
	inputType:'radio',
	comment:'comment_P223_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P223_2', 'P223_2'],
	scoreMap:[1, 0]
},{
	id:'P223_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P223_2',
	inputType:'radio',
	comment:'comment_P223_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P223_3', 'P223_3'],
	scoreMap:[1, 0]
},{
	id:'P223_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P223_3',
	inputType:'radio',
	comment:'comment_P223_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P223_4', 'P223_4'],
	scoreMap:[1, 0]
},{
	id:'P223_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P223_4',
	inputType:'radio',
	comment:'comment_P223_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P223_5', 'P223_5'],
	scoreMap:[1, 0]
},{
	id:'P223_5',
	category:CATEGORIES.SYMPTOMS,
	text:'P223_5',
	inputType:'radio',
	comment:'comment_P223_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['223'],
	scoreMap:[1, 0]
},{
	id:'P224_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P224_0',
	inputType:'radio',
	comment:'comment_P224_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P224_1', 'P224_1'],
	scoreMap:[1, 0]
},{
	id:'P224_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P224_1',
	inputType:'radio',
	comment:'comment_P224_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P224_2', 'P224_2'],
	scoreMap:[1, 0]
},{
	id:'P224_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P224_2',
	inputType:'radio',
	comment:'comment_P224_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P224_3', 'P224_3'],
	scoreMap:[1, 0]
},{
	id:'P224_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P224_3',
	inputType:'radio',
	comment:'comment_P224_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['224'],
	scoreMap:[1, 0]
},{
	id:'P225_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P225_0',
	inputType:'radio',
	comment:'comment_P225_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P225_1', 'P225_1'],
	scoreMap:[1, 0]
},{
	id:'P225_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P225_1',
	inputType:'radio',
	comment:'comment_P225_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P225_2', 'P225_2'],
	scoreMap:[1, 0]
},{
	id:'P225_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P225_2',
	inputType:'radio',
	comment:'comment_P225_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P225_3', 'P225_3'],
	scoreMap:[1, 0]
},{
	id:'P225_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P225_3',
	inputType:'radio',
	comment:'comment_P225_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P225_4', 'P225_4'],
	scoreMap:[1, 0]
},{
	id:'P225_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P225_4',
	inputType:'radio',
	comment:'comment_P225_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['225'],
	scoreMap:[1, 0]
},{
	id:'P226_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P226_0',
	inputType:'radio',
	comment:'comment_P226_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P226_1', 'P226_1'],
	scoreMap:[1, 0]
},{
	id:'P226_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P226_1',
	inputType:'radio',
	comment:'comment_P226_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['226'],
	scoreMap:[1, 0]
},{
	id:'P227_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P227_0',
	inputType:'radio',
	comment:'comment_P227_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P227_1', 'P227_1'],
	scoreMap:[1, 0]
},{
	id:'P227_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P227_1',
	inputType:'radio',
	comment:'comment_P227_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['227'],
	scoreMap:[1, 0]
},{
	id:'P228_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P228_0',
	inputType:'radio',
	comment:'comment_P228_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P228_1', 'P228_1'],
	scoreMap:[1, 0]
},{
	id:'P228_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P228_1',
	inputType:'radio',
	comment:'comment_P228_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P228_2', 'P228_2'],
	scoreMap:[1, 0]
},{
	id:'P228_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P228_2',
	inputType:'radio',
	comment:'comment_P228_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P228_3', 'P228_3'],
	scoreMap:[1, 0]
},{
	id:'P228_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P228_3',
	inputType:'radio',
	comment:'comment_P228_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['228'],
	scoreMap:[1, 0]
},{
	id:'P229_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P229_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['229'],
	scoreMap:[1, 0]
},{
	id:'P230_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P230_0',
	inputType:'radio',
	comment:'comment_P230_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P230_1', 'P230_1'],
	scoreMap:[1, 0]
},{
	id:'P230_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P230_1',
	inputType:'radio',
	comment:'comment_P230_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P230_2', 'P230_2'],
	scoreMap:[1, 0]
},{
	id:'P230_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P230_2',
	inputType:'radio',
	comment:'comment_P230_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P230_3', 'P230_3'],
	scoreMap:[1, 0]
},{
	id:'P230_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P230_3',
	inputType:'radio',
	comment:'comment_P230_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['230'],
	scoreMap:[1, 0]
},{
	id:'P231_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P231_0',
	inputType:'radio',
	comment:'comment_P231_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P231_1', 'P231_1'],
	scoreMap:[1, 0]
},{
	id:'P231_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P231_1',
	inputType:'radio',
	comment:'comment_P231_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P231_2', 'P231_2'],
	scoreMap:[1, 0]
},{
	id:'P231_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P231_2',
	inputType:'radio',
	comment:'comment_P231_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['231'],
	scoreMap:[1, 0]
},{
	id:'P232_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P232_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['232'],
	scoreMap:[1, 0]
},{
	id:'P233_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P233_0',
	inputType:'radio',
	comment:'comment_P233_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P233_1', 'P233_1'],
	scoreMap:[1, 0]
},{
	id:'P233_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P233_1',
	inputType:'radio',
	comment:'comment_P233_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P233_2', 'P233_2'],
	scoreMap:[1, 0]
},{
	id:'P233_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P233_2',
	inputType:'radio',
	comment:'comment_P233_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['233'],
	scoreMap:[1, 0]
},{
	id:'P234_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P234_0',
	inputType:'radio',
	comment:'comment_P234_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P234_1', 'P234_1'],
	scoreMap:[1, 0]
},{
	id:'P234_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P234_1',
	inputType:'radio',
	comment:'comment_P234_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P234_2', 'P234_2'],
	scoreMap:[1, 0]
},{
	id:'P234_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P234_2',
	inputType:'radio',
	comment:'comment_P234_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P234_3', 'P234_3'],
	scoreMap:[1, 0]
},{
	id:'P234_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P234_3',
	inputType:'radio',
	comment:'comment_P234_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['234'],
	scoreMap:[1, 0]
},{
	id:'P235_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P235_0',
	inputType:'radio',
	comment:'comment_P235_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P235_1', 'P235_1'],
	scoreMap:[1, 0]
},{
	id:'P235_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P235_1',
	inputType:'radio',
	comment:'comment_P235_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P235_2', 'P235_2'],
	scoreMap:[1, 0]
},{
	id:'P235_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P235_2',
	inputType:'radio',
	comment:'comment_P235_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P235_3', 'P235_3'],
	scoreMap:[1, 0]
},{
	id:'P235_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P235_3',
	inputType:'radio',
	comment:'comment_P235_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['235'],
	scoreMap:[1, 0]
},{
	id:'P236_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P236_0',
	inputType:'radio',
	comment:'comment_P236_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P236_1', 'P236_1'],
	scoreMap:[1, 0]
},{
	id:'P236_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P236_1',
	inputType:'radio',
	comment:'comment_P236_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P236_2', 'P236_2'],
	scoreMap:[1, 0]
},{
	id:'P236_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P236_2',
	inputType:'radio',
	comment:'comment_P236_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P236_3', 'P236_3'],
	scoreMap:[1, 0]
},{
	id:'P236_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P236_3',
	inputType:'radio',
	comment:'comment_P236_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P236_4', 'P236_4'],
	scoreMap:[1, 0]
},{
	id:'P236_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P236_4',
	inputType:'radio',
	comment:'comment_P236_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['236'],
	scoreMap:[1, 0]
},{
	id:'P237_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P237_0',
	inputType:'radio',
	comment:'comment_P237_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P237_1', 'P237_1'],
	scoreMap:[1, 0]
},{
	id:'P237_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P237_1',
	inputType:'radio',
	comment:'comment_P237_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P237_2', 'P237_2'],
	scoreMap:[1, 0]
},{
	id:'P237_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P237_2',
	inputType:'radio',
	comment:'comment_P237_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P237_3', 'P237_3'],
	scoreMap:[1, 0]
},{
	id:'P237_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P237_3',
	inputType:'radio',
	comment:'comment_P237_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P237_4', 'P237_4'],
	scoreMap:[1, 0]
},{
	id:'P237_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P237_4',
	inputType:'radio',
	comment:'comment_P237_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P237_5', 'P237_5'],
	scoreMap:[1, 0]
},{
	id:'P237_5',
	category:CATEGORIES.SYMPTOMS,
	text:'P237_5',
	inputType:'radio',
	comment:'comment_P237_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['237'],
	scoreMap:[1, 0]
},{
	id:'P238_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P238_0',
	inputType:'radio',
	comment:'comment_P238_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P238_1', 'P238_1'],
	scoreMap:[1, 0]
},{
	id:'P238_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P238_1',
	inputType:'radio',
	comment:'comment_P238_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P238_2', 'P238_2'],
	scoreMap:[1, 0]
},{
	id:'P238_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P238_2',
	inputType:'radio',
	comment:'comment_P238_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P238_3', 'P238_3'],
	scoreMap:[1, 0]
},{
	id:'P238_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P238_3',
	inputType:'radio',
	comment:'comment_P238_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P238_4', 'P238_4'],
	scoreMap:[1, 0]
},{
	id:'P238_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P238_4',
	inputType:'radio',
	comment:'comment_P238_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P238_5', 'P238_5'],
	scoreMap:[1, 0]
},{
	id:'P238_5',
	category:CATEGORIES.SYMPTOMS,
	text:'P238_5',
	inputType:'radio',
	comment:'comment_P238_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P238_6', 'P238_6'],
	scoreMap:[1, 0]
},{
	id:'P238_6',
	category:CATEGORIES.SYMPTOMS,
	text:'P238_6',
	inputType:'radio',
	comment:'comment_P238_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P238_7', 'P238_7'],
	scoreMap:[1, 0]
},{
	id:'P238_7',
	category:CATEGORIES.SYMPTOMS,
	text:'P238_7',
	inputType:'radio',
	comment:'comment_P238_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['238'],
	scoreMap:[1, 0]
},{
	id:'P239_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P239_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['239'],
	scoreMap:[1, 0]
},{
	id:'P240_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P240_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['240'],
	scoreMap:[1, 0]
},{
	id:'P241_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P241_0',
	inputType:'radio',
	comment:'comment_P241_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P241_1', 'P241_1'],
	scoreMap:[1, 0]
},{
	id:'P241_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P241_1',
	inputType:'radio',
	comment:'comment_P241_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P241_2', 'P241_2'],
	scoreMap:[1, 0]
},{
	id:'P241_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P241_2',
	inputType:'radio',
	comment:'comment_P241_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P241_3', 'P241_3'],
	scoreMap:[1, 0]
},{
	id:'P241_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P241_3',
	inputType:'radio',
	comment:'comment_P241_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['241'],
	scoreMap:[1, 0]
},{
	id:'P242_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P242_0',
	inputType:'radio',
	comment:'comment_P242_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P242_1', 'P242_1'],
	scoreMap:[1, 0]
},{
	id:'P242_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P242_1',
	inputType:'radio',
	comment:'comment_P242_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P242_2', 'P242_2'],
	scoreMap:[1, 0]
},{
	id:'P242_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P242_2',
	inputType:'radio',
	comment:'comment_P242_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['242'],
	scoreMap:[1, 0]
},{
	id:'P243_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P243_0',
	inputType:'radio',
	comment:'comment_P243_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P243_1', 'P243_1'],
	scoreMap:[1, 0]
},{
	id:'P243_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P243_1',
	inputType:'radio',
	comment:'comment_P243_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P243_2', 'P243_2'],
	scoreMap:[1, 0]
},{
	id:'P243_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P243_2',
	inputType:'radio',
	comment:'comment_P243_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P243_3', 'P243_3'],
	scoreMap:[1, 0]
},{
	id:'P243_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P243_3',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['243'],
	scoreMap:[1, 0]
},{
	id:'P244_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P244_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['244'],
	scoreMap:[1, 0]
},{
	id:'P245_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P245_0',
	inputType:'radio',
	comment:'comment_P245_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P245_1', 'P245_1'],
	scoreMap:[1, 0]
},{
	id:'P245_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P245_1',
	inputType:'radio',
	comment:'comment_P245_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P245_2', 'P245_2'],
	scoreMap:[1, 0]
},{
	id:'P245_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P245_2',
	inputType:'radio',
	comment:'comment_P245_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P245_3', 'P245_3'],
	scoreMap:[1, 0]
},{
	id:'P245_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P245_3',
	inputType:'radio',
	comment:'comment_P245_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P245_4', 'P245_4'],
	scoreMap:[1, 0]
},{
	id:'P245_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P245_4',
	inputType:'radio',
	comment:'comment_P245_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['245'],
	scoreMap:[1, 0]
},{
	id:'P246_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P246_0',
	inputType:'radio',
	comment:'comment_P246_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P246_1', 'P246_1'],
	scoreMap:[1, 0]
},{
	id:'P246_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P246_1',
	inputType:'radio',
	comment:'comment_P246_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P246_2', 'P246_2'],
	scoreMap:[1, 0]
},{
	id:'P246_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P246_2',
	inputType:'radio',
	comment:'comment_P246_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['246'],
	scoreMap:[1, 0]
},{
	id:'P247_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P247_0',
	inputType:'radio',
	comment:'comment_P247_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P247_1', 'P247_1'],
	scoreMap:[1, 0]
},{
	id:'P247_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P247_1',
	inputType:'radio',
	comment:'comment_P247_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P247_2', 'P247_2'],
	scoreMap:[1, 0]
},{
	id:'P247_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P247_2',
	inputType:'radio',
	comment:'comment_P247_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P247_3', 'P247_3'],
	scoreMap:[1, 0]
},{
	id:'P247_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P247_3',
	inputType:'radio',
	comment:'comment_P247_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P247_4', 'P247_4'],
	scoreMap:[1, 0]
},{
	id:'P247_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P247_4',
	inputType:'radio',
	comment:'comment_P247_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P247_5', 'P247_5'],
	scoreMap:[1, 0]
},{
	id:'P247_5',
	category:CATEGORIES.SYMPTOMS,
	text:'P247_5',
	inputType:'radio',
	comment:'comment_P247_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P247_6', 'P247_6'],
	scoreMap:[1, 0]
},{
	id:'P247_6',
	category:CATEGORIES.SYMPTOMS,
	text:'P247_6',
	inputType:'radio',
	comment:'comment_P247_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P247_7', 'P247_7'],
	scoreMap:[1, 0]
},{
	id:'P247_7',
	category:CATEGORIES.SYMPTOMS,
	text:'P247_7',
	inputType:'radio',
	comment:'comment_P247_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['247'],
	scoreMap:[1, 0]
},{
	id:'P248_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P248_0',
	inputType:'radio',
	comment:'comment_P248_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P248_1', 'P248_1'],
	scoreMap:[1, 0]
},{
	id:'P248_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P248_1',
	inputType:'radio',
	comment:'comment_P248_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P248_2', 'P248_2'],
	scoreMap:[1, 0]
},{
	id:'P248_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P248_2',
	inputType:'radio',
	comment:'comment_P248_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P248_3', 'P248_3'],
	scoreMap:[1, 0]
},{
	id:'P248_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P248_3',
	inputType:'radio',
	comment:'comment_P248_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P248_4', 'P248_4'],
	scoreMap:[1, 0]
},{
	id:'P248_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P248_4',
	inputType:'radio',
	comment:'comment_P248_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['248'],
	scoreMap:[1, 0]
},{
	id:'P249_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P249_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['249'],
	scoreMap:[1, 0]
},{
	id:'P250_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P250_0',
	inputType:'radio',
	comment:'comment_P250_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P250_1', 'P250_1'],
	scoreMap:[1, 0]
},{
	id:'P250_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P250_1',
	inputType:'radio',
	comment:'comment_P250_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P250_2', 'P250_2'],
	scoreMap:[1, 0]
},{
	id:'P250_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P250_2',
	inputType:'radio',
	comment:'comment_P250_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['250'],
	scoreMap:[1, 0]
},{
	id:'P251_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P251_0',
	inputType:'radio',
	comment:'comment_P251_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P251_1', 'P251_1'],
	scoreMap:[1, 0]
},{
	id:'P251_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P251_1',
	inputType:'radio',
	comment:'comment_P251_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P251_2', 'P251_2'],
	scoreMap:[1, 0]
},{
	id:'P251_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P251_2',
	inputType:'radio',
	comment:'comment_P251_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P251_3', 'P251_3'],
	scoreMap:[1, 0]
},{
	id:'P251_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P251_3',
	inputType:'radio',
	comment:'comment_P251_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P251_4', 'P251_4'],
	scoreMap:[1, 0]
},{
	id:'P251_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P251_4',
	inputType:'radio',
	comment:'comment_P251_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P251_5', 'P251_5'],
	scoreMap:[1, 0]
},{
	id:'P251_5',
	category:CATEGORIES.SYMPTOMS,
	text:'P251_5',
	inputType:'radio',
	comment:'comment_P251_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P251_6', 'P251_6'],
	scoreMap:[1, 0]
},{
	id:'P251_6',
	category:CATEGORIES.SYMPTOMS,
	text:'P251_6',
	inputType:'radio',
	comment:'comment_P251_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['251'],
	scoreMap:[1, 0]
},{
	id:'P252_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P252_0',
	inputType:'radio',
	comment:'comment_P252_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P252_1', 'P252_1'],
	scoreMap:[1, 0]
},{
	id:'P252_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P252_1',
	inputType:'radio',
	comment:'comment_P252_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P252_2', 'P252_2'],
	scoreMap:[1, 0]
},{
	id:'P252_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P252_2',
	inputType:'radio',
	comment:'comment_P252_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P252_3', 'P252_3'],
	scoreMap:[1, 0]
},{
	id:'P252_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P252_3',
	inputType:'radio',
	comment:'comment_P252_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P252_4', 'P252_4'],
	scoreMap:[1, 0]
},{
	id:'P252_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P252_4',
	inputType:'radio',
	comment:'comment_P252_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P252_5', 'P252_5'],
	scoreMap:[1, 0]
},{
	id:'P252_5',
	category:CATEGORIES.SYMPTOMS,
	text:'P252_5',
	inputType:'radio',
	comment:'comment_P252_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P252_6', 'P252_6'],
	scoreMap:[1, 0]
},{
	id:'P252_6',
	category:CATEGORIES.SYMPTOMS,
	text:'P252_6',
	inputType:'radio',
	comment:'comment_P252_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['252'],
	scoreMap:[1, 0]
},{
	id:'P253_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P253_0',
	inputType:'radio',
	comment:'comment_P253_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P253_1', 'P253_1'],
	scoreMap:[1, 0]
},{
	id:'P253_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P253_1',
	inputType:'radio',
	comment:'comment_P253_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P253_2', 'P253_2'],
	scoreMap:[1, 0]
},{
	id:'P253_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P253_2',
	inputType:'radio',
	comment:'comment_P253_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P253_3', 'P253_3'],
	scoreMap:[1, 0]
},{
	id:'P253_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P253_3',
	inputType:'radio',
	comment:'comment_P253_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['253'],
	scoreMap:[1, 0]
},{
	id:'P254_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P254_0',
	inputType:'radio',
	comment:'comment_P254_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P254_1', 'P254_1'],
	scoreMap:[1, 0]
},{
	id:'P254_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P254_1',
	inputType:'radio',
	comment:'comment_P254_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P254_2', 'P254_2'],
	scoreMap:[1, 0]
},{
	id:'P254_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P254_2',
	inputType:'radio',
	comment:'comment_P254_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['254'],
	scoreMap:[1, 0]
},{
	id:'P255_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P255_0',
	inputType:'radio',
	comment:'comment_P255_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P255_1', 'P255_1'],
	scoreMap:[1, 0]
},{
	id:'P255_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P255_1',
	inputType:'radio',
	comment:'comment_P255_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P255_2', 'P255_2'],
	scoreMap:[1, 0]
},{
	id:'P255_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P255_2',
	inputType:'radio',
	comment:'comment_P255_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P255_3', 'P255_3'],
	scoreMap:[1, 0]
},{
	id:'P255_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P255_3',
	inputType:'radio',
	comment:'comment_P255_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['255'],
	scoreMap:[1, 0]
},{
	id:'P256_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P256_0',
	inputType:'radio',
	comment:'comment_P256_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P256_1', 'P256_1'],
	scoreMap:[1, 0]
},{
	id:'P256_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P256_1',
	inputType:'radio',
	comment:'comment_P256_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P256_2', 'P256_2'],
	scoreMap:[1, 0]
},{
	id:'P256_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P256_2',
	inputType:'radio',
	comment:'comment_P256_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['256'],
	scoreMap:[1, 0]
},{
	id:'P257_0',
	category:CATEGORIES.SYMPTOMS,
	text:'P257_0',
	inputType:'radio',
	comment:'comment_P257_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P257_1', 'P257_1'],
	scoreMap:[1, 0]
},{
	id:'P257_1',
	category:CATEGORIES.SYMPTOMS,
	text:'P257_1',
	inputType:'radio',
	comment:'comment_P257_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P257_2', 'P257_2'],
	scoreMap:[1, 0]
},{
	id:'P257_2',
	category:CATEGORIES.SYMPTOMS,
	text:'P257_2',
	inputType:'radio',
	comment:'comment_P257_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P257_3', 'P257_3'],
	scoreMap:[1, 0]
},{
	id:'P257_3',
	category:CATEGORIES.SYMPTOMS,
	text:'P257_3',
	inputType:'radio',
	comment:'comment_P257_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['P257_4', 'P257_4'],
	scoreMap:[1, 0]
},{
	id:'P257_4',
	category:CATEGORIES.SYMPTOMS,
	text:'P257_4',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['257'],
	scoreMap:[1, 0]
},{
	id:'R258_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R258_0',
	inputType:'radio',
	comment:'comment_R258_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R258_1', 'R258_1'],
	scoreMap:[1, 0]
},{
	id:'R258_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R258_1',
	inputType:'radio',
	comment:'comment_R258_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R258_2', 'R258_2'],
	scoreMap:[1, 0]
},{
	id:'R258_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R258_2',
	inputType:'radio',
	comment:'comment_R258_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R258_3', 'R258_3'],
	scoreMap:[1, 0]
},{
	id:'R258_3',
	category:CATEGORIES.SYMPTOMS,
	text:'R258_3',
	inputType:'radio',
	comment:'comment_R258_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['258'],
	scoreMap:[1, 0]
},{
	id:'R259_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R259_0',
	inputType:'radio',
	comment:'comment_R259_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R259_1', 'R259_1'],
	scoreMap:[1, 0]
},{
	id:'R259_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R259_1',
	inputType:'radio',
	comment:'comment_R259_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R259_2', 'R259_2'],
	scoreMap:[1, 0]
},{
	id:'R259_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R259_2',
	inputType:'radio',
	comment:'comment_R259_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R259_3', 'R259_3'],
	scoreMap:[1, 0]
},{
	id:'R259_3',
	category:CATEGORIES.SYMPTOMS,
	text:'R259_3',
	inputType:'radio',
	comment:'comment_R259_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R259_4', 'R259_4'],
	scoreMap:[1, 0]
},{
	id:'R259_4',
	category:CATEGORIES.SYMPTOMS,
	text:'R259_4',
	inputType:'radio',
	comment:'comment_R259_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R259_5', 'R259_5'],
	scoreMap:[1, 0]
},{
	id:'R259_5',
	category:CATEGORIES.SYMPTOMS,
	text:'R259_5',
	inputType:'radio',
	comment:'comment_R259_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['259'],
	scoreMap:[1, 0]
},{
	id:'R260_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R260_0',
	inputType:'radio',
	comment:'comment_R260_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R260_1', 'R260_1'],
	scoreMap:[1, 0]
},{
	id:'R260_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R260_1',
	inputType:'radio',
	comment:'comment_R260_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R260_2', 'R260_2'],
	scoreMap:[1, 0]
},{
	id:'R260_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R260_2',
	inputType:'radio',
	comment:'comment_R260_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R260_3', 'R260_3'],
	scoreMap:[1, 0]
},{
	id:'R260_3',
	category:CATEGORIES.SYMPTOMS,
	text:'R260_3',
	inputType:'radio',
	comment:'comment_R260_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R260_4', 'R260_4'],
	scoreMap:[1, 0]
},{
	id:'R260_4',
	category:CATEGORIES.SYMPTOMS,
	text:'R260_4',
	inputType:'radio',
	comment:'comment_R260_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R260_5', 'R260_5'],
	scoreMap:[1, 0]
},{
	id:'R260_5',
	category:CATEGORIES.SYMPTOMS,
	text:'R260_5',
	inputType:'radio',
	comment:'comment_R260_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R260_6', 'R260_6'],
	scoreMap:[1, 0]
},{
	id:'R260_6',
	category:CATEGORIES.SYMPTOMS,
	text:'R260_6',
	inputType:'radio',
	comment:'comment_R260_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R260_7', 'R260_7'],
	scoreMap:[1, 0]
},{
	id:'R260_7',
	category:CATEGORIES.SYMPTOMS,
	text:'R260_7',
	inputType:'radio',
	comment:'comment_R260_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['260'],
	scoreMap:[1, 0]
},{
	id:'R261_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R261_0',
	inputType:'radio',
	comment:'comment_R261_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R261_1', 'R261_1'],
	scoreMap:[1, 0]
},{
	id:'R261_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R261_1',
	inputType:'radio',
	comment:'comment_R261_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R261_2', 'R261_2'],
	scoreMap:[1, 0]
},{
	id:'R261_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R261_2',
	inputType:'radio',
	comment:'comment_R261_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R261_3', 'R261_3'],
	scoreMap:[1, 0]
},{
	id:'R261_3',
	category:CATEGORIES.SYMPTOMS,
	text:'R261_3',
	inputType:'radio',
	comment:'comment_R261_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R261_4', 'R261_4'],
	scoreMap:[1, 0]
},{
	id:'R261_4',
	category:CATEGORIES.SYMPTOMS,
	text:'R261_4',
	inputType:'radio',
	comment:'comment_R261_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['261'],
	scoreMap:[1, 0]
},{
	id:'R262_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R262_0',
	inputType:'radio',
	comment:'comment_R262_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R262_1', 'R262_1'],
	scoreMap:[1, 0]
},{
	id:'R262_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R262_1',
	inputType:'radio',
	comment:'comment_R262_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R262_2', 'R262_2'],
	scoreMap:[1, 0]
},{
	id:'R262_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R262_2',
	inputType:'radio',
	comment:'comment_R262_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R262_3', 'R262_3'],
	scoreMap:[1, 0]
},{
	id:'R262_3',
	category:CATEGORIES.SYMPTOMS,
	text:'R262_3',
	inputType:'radio',
	comment:'comment_R262_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R262_4', 'R262_4'],
	scoreMap:[1, 0]
},{
	id:'R262_4',
	category:CATEGORIES.SYMPTOMS,
	text:'R262_4',
	inputType:'radio',
	comment:'comment_R262_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R262_5', 'R262_5'],
	scoreMap:[1, 0]
},{
	id:'R262_5',
	category:CATEGORIES.SYMPTOMS,
	text:'R262_5',
	inputType:'radio',
	comment:'comment_R262_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['262'],
	scoreMap:[1, 0]
},{
	id:'R263_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R263_0',
	inputType:'radio',
	comment:'comment_R263_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R263_1', 'R263_1'],
	scoreMap:[1, 0]
},{
	id:'R263_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R263_1',
	inputType:'radio',
	comment:'comment_R263_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R263_2', 'R263_2'],
	scoreMap:[1, 0]
},{
	id:'R263_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R263_2',
	inputType:'radio',
	comment:'comment_R263_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['263'],
	scoreMap:[1, 0]
},{
	id:'R264_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R264_0',
	inputType:'radio',
	comment:'comment_R264_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R264_1', 'R264_1'],
	scoreMap:[1, 0]
},{
	id:'R264_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R264_1',
	inputType:'radio',
	comment:'comment_R264_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R264_2', 'R264_2'],
	scoreMap:[1, 0]
},{
	id:'R264_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R264_2',
	inputType:'radio',
	comment:'comment_R264_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R264_3', 'R264_3'],
	scoreMap:[1, 0]
},{
	id:'R264_3',
	category:CATEGORIES.SYMPTOMS,
	text:'R264_3',
	inputType:'radio',
	comment:'comment_R264_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['264'],
	scoreMap:[1, 0]
},{
	id:'R265_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R265_0',
	inputType:'radio',
	comment:'comment_R265_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R265_1', 'R265_1'],
	scoreMap:[1, 0]
},{
	id:'R265_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R265_1',
	inputType:'radio',
	comment:'comment_R265_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R265_2', 'R265_2'],
	scoreMap:[1, 0]
},{
	id:'R265_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R265_2',
	inputType:'radio',
	comment:'comment_R265_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['265'],
	scoreMap:[1, 0]
},{
	id:'R266_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R266_0',
	inputType:'radio',
	comment:'comment_R266_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R266_1', 'R266_1'],
	scoreMap:[1, 0]
},{
	id:'R266_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R266_1',
	inputType:'radio',
	comment:'comment_R266_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R266_2', 'R266_2'],
	scoreMap:[1, 0]
},{
	id:'R266_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R266_2',
	inputType:'radio',
	comment:'comment_R266_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R266_3', 'R266_3'],
	scoreMap:[1, 0]
},{
	id:'R266_3',
	category:CATEGORIES.SYMPTOMS,
	text:'R266_3',
	inputType:'radio',
	comment:'comment_R266_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['266'],
	scoreMap:[1, 0]
},{
	id:'R267_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R267_0',
	inputType:'radio',
	comment:'comment_R267_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R267_1', 'R267_1'],
	scoreMap:[1, 0]
},{
	id:'R267_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R267_1',
	inputType:'radio',
	comment:'comment_R267_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R267_2', 'R267_2'],
	scoreMap:[1, 0]
},{
	id:'R267_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R267_2',
	inputType:'radio',
	comment:'comment_R267_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['267'],
	scoreMap:[1, 0]
},{
	id:'R268_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R268_0',
	inputType:'radio',
	comment:'comment_R268_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R268_1', 'R268_1'],
	scoreMap:[1, 0]
},{
	id:'R268_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R268_1',
	inputType:'radio',
	comment:'comment_R268_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R268_2', 'R268_2'],
	scoreMap:[1, 0]
},{
	id:'R268_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R268_2',
	inputType:'radio',
	comment:'comment_R268_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R268_3', 'R268_3'],
	scoreMap:[1, 0]
},{
	id:'R268_3',
	category:CATEGORIES.SYMPTOMS,
	text:'R268_3',
	inputType:'radio',
	comment:'comment_R268_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['268'],
	scoreMap:[1, 0]
},{
	id:'R269_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R269_0',
	inputType:'radio',
	comment:'comment_R269_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R269_1', 'R269_1'],
	scoreMap:[1, 0]
},{
	id:'R269_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R269_1',
	inputType:'radio',
	comment:'comment_R269_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R269_2', 'R269_2'],
	scoreMap:[1, 0]
},{
	id:'R269_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R269_2',
	inputType:'radio',
	comment:'comment_R269_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['269'],
	scoreMap:[1, 0]
},{
	id:'R270_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R270_0',
	inputType:'radio',
	comment:'comment_R270_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R270_1', 'R270_1'],
	scoreMap:[1, 0]
},{
	id:'R270_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R270_1',
	inputType:'radio',
	comment:'comment_R270_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['270'],
	scoreMap:[1, 0]
},{
	id:'R271_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R271_0',
	inputType:'radio',
	comment:'comment_R271_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R271_1', 'R271_1'],
	scoreMap:[1, 0]
},{
	id:'R271_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R271_1',
	inputType:'radio',
	comment:'comment_R271_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R271_2', 'R271_2'],
	scoreMap:[1, 0]
},{
	id:'R271_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R271_2',
	inputType:'radio',
	comment:'comment_R271_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R271_3', 'R271_3'],
	scoreMap:[1, 0]
},{
	id:'R271_3',
	category:CATEGORIES.SYMPTOMS,
	text:'R271_3',
	inputType:'radio',
	comment:'comment_R271_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['271'],
	scoreMap:[1, 0]
},{
	id:'R272_0',
	category:CATEGORIES.SYMPTOMS,
	text:'R272_0',
	inputType:'radio',
	comment:'comment_R272_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R272_1', 'R272_1'],
	scoreMap:[1, 0]
},{
	id:'R272_1',
	category:CATEGORIES.SYMPTOMS,
	text:'R272_1',
	inputType:'radio',
	comment:'comment_R272_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['R272_2', 'R272_2'],
	scoreMap:[1, 0]
},{
	id:'R272_2',
	category:CATEGORIES.SYMPTOMS,
	text:'R272_2',
	inputType:'radio',
	comment:'comment_R272_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['272'],
	scoreMap:[1, 0]
},{
	id:'S273_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S273_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['273'],
	scoreMap:[1, 0]
},{
	id:'S274_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S274_0',
	inputType:'radio',
	comment:'comment_S274_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S274_1', 'S274_1'],
	scoreMap:[1, 0]
},{
	id:'S274_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S274_1',
	inputType:'radio',
	comment:'comment_S274_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S274_2', 'S274_2'],
	scoreMap:[1, 0]
},{
	id:'S274_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S274_2',
	inputType:'radio',
	comment:'comment_S274_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S274_3', 'S274_3'],
	scoreMap:[1, 0]
},{
	id:'S274_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S274_3',
	inputType:'radio',
	comment:'comment_S274_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S274_4', 'S274_4'],
	scoreMap:[1, 0]
},{
	id:'S274_4',
	category:CATEGORIES.SYMPTOMS,
	text:'S274_4',
	inputType:'radio',
	comment:'comment_S274_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S274_5', 'S274_5'],
	scoreMap:[1, 0]
},{
	id:'S274_5',
	category:CATEGORIES.SYMPTOMS,
	text:'S274_5',
	inputType:'radio',
	comment:'comment_S274_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['274'],
	scoreMap:[1, 0]
},{
	id:'S275_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S275_0',
	inputType:'radio',
	comment:'comment_S275_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S275_1', 'S275_1'],
	scoreMap:[1, 0]
},{
	id:'S275_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S275_1',
	inputType:'radio',
	comment:'comment_S275_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S275_2', 'S275_2'],
	scoreMap:[1, 0]
},{
	id:'S275_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S275_2',
	inputType:'radio',
	comment:'comment_S275_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['275'],
	scoreMap:[1, 0]
},{
	id:'S276_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S276_0',
	inputType:'radio',
	comment:'comment_S276_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S276_1', 'S276_1'],
	scoreMap:[1, 0]
},{
	id:'S276_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S276_1',
	inputType:'radio',
	comment:'comment_S276_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S276_2', 'S276_2'],
	scoreMap:[1, 0]
},{
	id:'S276_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S276_2',
	inputType:'radio',
	comment:'comment_S276_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S276_3', 'S276_3'],
	scoreMap:[1, 0]
},{
	id:'S276_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S276_3',
	inputType:'radio',
	comment:'comment_S276_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S276_4', 'S276_4'],
	scoreMap:[1, 0]
},{
	id:'S276_4',
	category:CATEGORIES.SYMPTOMS,
	text:'S276_4',
	inputType:'radio',
	comment:'comment_S276_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['276'],
	scoreMap:[1, 0]
},{
	id:'S277_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S277_0',
	inputType:'radio',
	comment:'comment_S277_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S277_1', 'S277_1'],
	scoreMap:[1, 0]
},{
	id:'S277_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S277_1',
	inputType:'radio',
	comment:'comment_S277_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S277_2', 'S277_2'],
	scoreMap:[1, 0]
},{
	id:'S277_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S277_2',
	inputType:'radio',
	comment:'comment_S277_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S277_3', 'S277_3'],
	scoreMap:[1, 0]
},{
	id:'S277_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S277_3',
	inputType:'radio',
	comment:'comment_S277_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S277_4', 'S277_4'],
	scoreMap:[1, 0]
},{
	id:'S277_4',
	category:CATEGORIES.SYMPTOMS,
	text:'S277_4',
	inputType:'radio',
	comment:'comment_S277_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S277_5', 'S277_5'],
	scoreMap:[1, 0]
},{
	id:'S277_5',
	category:CATEGORIES.SYMPTOMS,
	text:'S277_5',
	inputType:'radio',
	comment:'comment_S277_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['277'],
	scoreMap:[1, 0]
},{
	id:'S278_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S278_0',
	inputType:'radio',
	comment:'comment_S278_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S278_1', 'S278_1'],
	scoreMap:[1, 0]
},{
	id:'S278_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S278_1',
	inputType:'radio',
	comment:'comment_S278_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S278_2', 'S278_2'],
	scoreMap:[1, 0]
},{
	id:'S278_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S278_2',
	inputType:'radio',
	comment:'comment_S278_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S278_3', 'S278_3'],
	scoreMap:[1, 0]
},{
	id:'S278_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S278_3',
	inputType:'radio',
	comment:'comment_S278_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['278'],
	scoreMap:[1, 0]
},{
	id:'S279_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S279_0',
	inputType:'radio',
	comment:'comment_S279_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S279_1', 'S279_1'],
	scoreMap:[1, 0]
},{
	id:'S279_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S279_1',
	inputType:'radio',
	comment:'comment_S279_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S279_2', 'S279_2'],
	scoreMap:[1, 0]
},{
	id:'S279_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S279_2',
	inputType:'radio',
	comment:'comment_S279_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S279_3', 'S279_3'],
	scoreMap:[1, 0]
},{
	id:'S279_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S279_3',
	inputType:'radio',
	comment:'comment_S279_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['279'],
	scoreMap:[1, 0]
},{
	id:'S280_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S280_0',
	inputType:'radio',
	comment:'comment_S280_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S280_1', 'S280_1'],
	scoreMap:[1, 0]
},{
	id:'S280_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S280_1',
	inputType:'radio',
	comment:'comment_S280_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S280_2', 'S280_2'],
	scoreMap:[1, 0]
},{
	id:'S280_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S280_2',
	inputType:'radio',
	comment:'comment_S280_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S280_3', 'S280_3'],
	scoreMap:[1, 0]
},{
	id:'S280_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S280_3',
	inputType:'radio',
	comment:'comment_S280_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S280_4', 'S280_4'],
	scoreMap:[1, 0]
},{
	id:'S280_4',
	category:CATEGORIES.SYMPTOMS,
	text:'S280_4',
	inputType:'radio',
	comment:'comment_S280_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S280_5', 'S280_5'],
	scoreMap:[1, 0]
},{
	id:'S280_5',
	category:CATEGORIES.SYMPTOMS,
	text:'S280_5',
	inputType:'radio',
	comment:'comment_S280_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S280_6', 'S280_6'],
	scoreMap:[1, 0]
},{
	id:'S280_6',
	category:CATEGORIES.SYMPTOMS,
	text:'S280_6',
	inputType:'radio',
	comment:'comment_S280_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['280'],
	scoreMap:[1, 0]
},{
	id:'S281_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S281_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['281'],
	scoreMap:[1, 0]
},{
	id:'S282_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S282_0',
	inputType:'radio',
	comment:'comment_S282_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S282_1', 'S282_1'],
	scoreMap:[1, 0]
},{
	id:'S282_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S282_1',
	inputType:'radio',
	comment:'comment_S282_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S282_2', 'S282_2'],
	scoreMap:[1, 0]
},{
	id:'S282_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S282_2',
	inputType:'radio',
	comment:'comment_S282_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['282'],
	scoreMap:[1, 0]
},{
	id:'S283_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S283_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['283'],
	scoreMap:[1, 0]
},{
	id:'S284_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S284_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['284'],
	scoreMap:[1, 0]
},{
	id:'S285_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S285_0',
	inputType:'radio',
	comment:'comment_S285_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S285_1', 'S285_1'],
	scoreMap:[1, 0]
},{
	id:'S285_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S285_1',
	inputType:'radio',
	comment:'comment_S285_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['285'],
	scoreMap:[1, 0]
},{
	id:'S286_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S286_0',
	inputType:'radio',
	comment:'comment_S286_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S286_1', 'S286_1'],
	scoreMap:[1, 0]
},{
	id:'S286_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S286_1',
	inputType:'radio',
	comment:'comment_S286_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S286_2', 'S286_2'],
	scoreMap:[1, 0]
},{
	id:'S286_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S286_2',
	inputType:'radio',
	comment:'comment_S286_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S286_3', 'S286_3'],
	scoreMap:[1, 0]
},{
	id:'S286_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S286_3',
	inputType:'radio',
	comment:'comment_S286_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['286'],
	scoreMap:[1, 0]
},{
	id:'S287_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S287_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['287'],
	scoreMap:[1, 0]
},{
	id:'S288_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S288_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['288'],
	scoreMap:[1, 0]
},{
	id:'S289_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S289_0',
	inputType:'radio',
	comment:'comment_S289_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S289_1', 'S289_1'],
	scoreMap:[1, 0]
},{
	id:'S289_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S289_1',
	inputType:'radio',
	comment:'comment_S289_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S289_2', 'S289_2'],
	scoreMap:[1, 0]
},{
	id:'S289_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S289_2',
	inputType:'radio',
	comment:'comment_S289_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S289_3', 'S289_3'],
	scoreMap:[1, 0]
},{
	id:'S289_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S289_3',
	inputType:'radio',
	comment:'comment_S289_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S289_4', 'S289_4'],
	scoreMap:[1, 0]
},{
	id:'S289_4',
	category:CATEGORIES.SYMPTOMS,
	text:'S289_4',
	inputType:'radio',
	comment:'comment_S289_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['289'],
	scoreMap:[1, 0]
},{
	id:'S290_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S290_0',
	inputType:'radio',
	comment:'comment_S290_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S290_1', 'S290_1'],
	scoreMap:[1, 0]
},{
	id:'S290_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S290_1',
	inputType:'radio',
	comment:'comment_S290_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S290_2', 'S290_2'],
	scoreMap:[1, 0]
},{
	id:'S290_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S290_2',
	inputType:'radio',
	comment:'comment_S290_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S290_3', 'S290_3'],
	scoreMap:[1, 0]
},{
	id:'S290_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S290_3',
	inputType:'radio',
	comment:'comment_S290_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S290_4', 'S290_4'],
	scoreMap:[1, 0]
},{
	id:'S290_4',
	category:CATEGORIES.SYMPTOMS,
	text:'S290_4',
	inputType:'radio',
	comment:'comment_S290_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['290'],
	scoreMap:[1, 0]
},{
	id:'S291_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S291_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['291'],
	scoreMap:[1, 0]
},{
	id:'S292_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S292_0',
	inputType:'radio',
	comment:'comment_S292_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S292_1', 'S292_1'],
	scoreMap:[1, 0]
},{
	id:'S292_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S292_1',
	inputType:'radio',
	comment:'comment_S292_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S292_2', 'S292_2'],
	scoreMap:[1, 0]
},{
	id:'S292_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S292_2',
	inputType:'radio',
	comment:'comment_S292_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S292_3', 'S292_3'],
	scoreMap:[1, 0]
},{
	id:'S292_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S292_3',
	inputType:'radio',
	comment:'comment_S292_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S292_4', 'S292_4'],
	scoreMap:[1, 0]
},{
	id:'S292_4',
	category:CATEGORIES.SYMPTOMS,
	text:'S292_4',
	inputType:'radio',
	comment:'comment_S292_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S292_5', 'S292_5'],
	scoreMap:[1, 0]
},{
	id:'S292_5',
	category:CATEGORIES.SYMPTOMS,
	text:'S292_5',
	inputType:'radio',
	comment:'comment_S292_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['292'],
	scoreMap:[1, 0]
},{
	id:'S293_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S293_0',
	inputType:'radio',
	comment:'comment_S293_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S293_1', 'S293_1'],
	scoreMap:[1, 0]
},{
	id:'S293_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S293_1',
	inputType:'radio',
	comment:'comment_S293_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S293_2', 'S293_2'],
	scoreMap:[1, 0]
},{
	id:'S293_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S293_2',
	inputType:'radio',
	comment:'comment_S293_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S293_3', 'S293_3'],
	scoreMap:[1, 0]
},{
	id:'S293_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S293_3',
	inputType:'radio',
	comment:'comment_S293_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S293_4', 'S293_4'],
	scoreMap:[1, 0]
},{
	id:'S293_4',
	category:CATEGORIES.SYMPTOMS,
	text:'S293_4',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['293'],
	scoreMap:[1, 0]
},{
	id:'S294_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S294_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['294'],
	scoreMap:[1, 0]
},{
	id:'S295_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S295_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['295'],
	scoreMap:[1, 0]
},{
	id:'S296_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S296_0',
	inputType:'radio',
	comment:'comment_S296_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S296_1', 'S296_1'],
	scoreMap:[1, 0]
},{
	id:'S296_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S296_1',
	inputType:'radio',
	comment:'comment_S296_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S296_2', 'S296_2'],
	scoreMap:[1, 0]
},{
	id:'S296_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S296_2',
	inputType:'radio',
	comment:'comment_S296_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S296_3', 'S296_3'],
	scoreMap:[1, 0]
},{
	id:'S296_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S296_3',
	inputType:'radio',
	comment:'comment_S296_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S296_4', 'S296_4'],
	scoreMap:[1, 0]
},{
	id:'S296_4',
	category:CATEGORIES.SYMPTOMS,
	text:'S296_4',
	inputType:'radio',
	comment:'comment_S296_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['296'],
	scoreMap:[1, 0]
},{
	id:'S297_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S297_0',
	inputType:'radio',
	comment:'comment_S297_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S297_1', 'S297_1'],
	scoreMap:[1, 0]
},{
	id:'S297_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S297_1',
	inputType:'radio',
	comment:'comment_S297_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S297_2', 'S297_2'],
	scoreMap:[1, 0]
},{
	id:'S297_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S297_2',
	inputType:'radio',
	comment:'comment_S297_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S297_3', 'S297_3'],
	scoreMap:[1, 0]
},{
	id:'S297_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S297_3',
	inputType:'radio',
	comment:'comment_S297_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S297_4', 'S297_4'],
	scoreMap:[1, 0]
},{
	id:'S297_4',
	category:CATEGORIES.SYMPTOMS,
	text:'S297_4',
	inputType:'radio',
	comment:'comment_S297_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['297'],
	scoreMap:[1, 0]
},{
	id:'S298_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S298_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['298'],
	scoreMap:[1, 0]
},{
	id:'S299_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S299_0',
	inputType:'radio',
	comment:'comment_S299_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S299_1', 'S299_1'],
	scoreMap:[1, 0]
},{
	id:'S299_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S299_1',
	inputType:'radio',
	comment:'comment_S299_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S299_2', 'S299_2'],
	scoreMap:[1, 0]
},{
	id:'S299_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S299_2',
	inputType:'radio',
	comment:'comment_S299_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S299_3', 'S299_3'],
	scoreMap:[1, 0]
},{
	id:'S299_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S299_3',
	inputType:'radio',
	comment:'comment_S299_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S299_4', 'S299_4'],
	scoreMap:[1, 0]
},{
	id:'S299_4',
	category:CATEGORIES.SYMPTOMS,
	text:'S299_4',
	inputType:'radio',
	comment:'comment_S299_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S299_5', 'S299_5'],
	scoreMap:[1, 0]
},{
	id:'S299_5',
	category:CATEGORIES.SYMPTOMS,
	text:'S299_5',
	inputType:'radio',
	comment:'comment_S299_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S299_6', 'S299_6'],
	scoreMap:[1, 0]
},{
	id:'S299_6',
	category:CATEGORIES.SYMPTOMS,
	text:'S299_6',
	inputType:'radio',
	comment:'comment_S299_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S299_7', 'S299_7'],
	scoreMap:[1, 0]
},{
	id:'S299_7',
	category:CATEGORIES.SYMPTOMS,
	text:'S299_7',
	inputType:'radio',
	comment:'comment_S299_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S299_8', 'S299_8'],
	scoreMap:[1, 0]
},{
	id:'S299_8',
	category:CATEGORIES.SYMPTOMS,
	text:'S299_8',
	inputType:'radio',
	comment:'comment_S299_8',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['299'],
	scoreMap:[1, 0]
},{
	id:'S300_0',
	category:CATEGORIES.SYMPTOMS,
	text:'S300_0',
	inputType:'radio',
	comment:'comment_S300_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S300_1', 'S300_1'],
	scoreMap:[1, 0]
},{
	id:'S300_1',
	category:CATEGORIES.SYMPTOMS,
	text:'S300_1',
	inputType:'radio',
	comment:'comment_S300_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S300_2', 'S300_2'],
	scoreMap:[1, 0]
},{
	id:'S300_2',
	category:CATEGORIES.SYMPTOMS,
	text:'S300_2',
	inputType:'radio',
	comment:'comment_S300_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S300_3', 'S300_3'],
	scoreMap:[1, 0]
},{
	id:'S300_3',
	category:CATEGORIES.SYMPTOMS,
	text:'S300_3',
	inputType:'radio',
	comment:'comment_S300_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['S300_4', 'S300_4'],
	scoreMap:[1, 0]
},{
	id:'S300_4',
	category:CATEGORIES.SYMPTOMS,
	text:'S300_4',
	inputType:'radio',
	comment:'comment_S300_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['300'],
	scoreMap:[1, 0]
},{
	id:'T301_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T301_0',
	inputType:'radio',
	comment:'comment_T301_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T301_1', 'T301_1'],
	scoreMap:[1, 0]
},{
	id:'T301_1',
	category:CATEGORIES.SYMPTOMS,
	text:'T301_1',
	inputType:'radio',
	comment:'comment_T301_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T301_2', 'T301_2'],
	scoreMap:[1, 0]
},{
	id:'T301_2',
	category:CATEGORIES.SYMPTOMS,
	text:'T301_2',
	inputType:'radio',
	comment:'comment_T301_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T301_3', 'T301_3'],
	scoreMap:[1, 0]
},{
	id:'T301_3',
	category:CATEGORIES.SYMPTOMS,
	text:'T301_3',
	inputType:'radio',
	comment:'comment_T301_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T301_4', 'T301_4'],
	scoreMap:[1, 0]
},{
	id:'T301_4',
	category:CATEGORIES.SYMPTOMS,
	text:'T301_4',
	inputType:'radio',
	comment:'comment_T301_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T301_5', 'T301_5'],
	scoreMap:[1, 0]
},{
	id:'T301_5',
	category:CATEGORIES.SYMPTOMS,
	text:'T301_5',
	inputType:'radio',
	comment:'comment_T301_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T301_6', 'T301_6'],
	scoreMap:[1, 0]
},{
	id:'T301_6',
	category:CATEGORIES.SYMPTOMS,
	text:'T301_6',
	inputType:'radio',
	comment:'comment_T301_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T301_7', 'T301_7'],
	scoreMap:[1, 0]
},{
	id:'T301_7',
	category:CATEGORIES.SYMPTOMS,
	text:'T301_7',
	inputType:'radio',
	comment:'comment_T301_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T301_8', 'T301_8'],
	scoreMap:[1, 0]
},{
	id:'T301_8',
	category:CATEGORIES.SYMPTOMS,
	text:'T301_8',
	inputType:'radio',
	comment:'comment_T301_8',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['301'],
	scoreMap:[1, 0]
},{
	id:'T302_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T302_0',
	inputType:'radio',
	comment:'comment_T302_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T302_1', 'T302_1'],
	scoreMap:[1, 0]
},{
	id:'T302_1',
	category:CATEGORIES.SYMPTOMS,
	text:'T302_1',
	inputType:'radio',
	comment:'comment_T302_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T302_2', 'T302_2'],
	scoreMap:[1, 0]
},{
	id:'T302_2',
	category:CATEGORIES.SYMPTOMS,
	text:'T302_2',
	inputType:'radio',
	comment:'comment_T302_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T302_3', 'T302_3'],
	scoreMap:[1, 0]
},{
	id:'T302_3',
	category:CATEGORIES.SYMPTOMS,
	text:'T302_3',
	inputType:'radio',
	comment:'comment_T302_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T302_4', 'T302_4'],
	scoreMap:[1, 0]
},{
	id:'T302_4',
	category:CATEGORIES.SYMPTOMS,
	text:'T302_4',
	inputType:'radio',
	comment:'comment_T302_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['302'],
	scoreMap:[1, 0]
},{
	id:'T303_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T303_0',
	inputType:'radio',
	comment:'comment_T303_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T303_1', 'T303_1'],
	scoreMap:[1, 0]
},{
	id:'T303_1',
	category:CATEGORIES.SYMPTOMS,
	text:'T303_1',
	inputType:'radio',
	comment:'comment_T303_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T303_2', 'T303_2'],
	scoreMap:[1, 0]
},{
	id:'T303_2',
	category:CATEGORIES.SYMPTOMS,
	text:'T303_2',
	inputType:'radio',
	comment:'comment_T303_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T303_3', 'T303_3'],
	scoreMap:[1, 0]
},{
	id:'T303_3',
	category:CATEGORIES.SYMPTOMS,
	text:'T303_3',
	inputType:'radio',
	comment:'comment_T303_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T303_4', 'T303_4'],
	scoreMap:[1, 0]
},{
	id:'T303_4',
	category:CATEGORIES.SYMPTOMS,
	text:'T303_4',
	inputType:'radio',
	comment:'comment_T303_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T303_5', 'T303_5'],
	scoreMap:[1, 0]
},{
	id:'T303_5',
	category:CATEGORIES.SYMPTOMS,
	text:'T303_5',
	inputType:'radio',
	comment:'comment_T303_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['303'],
	scoreMap:[1, 0]
},{
	id:'T304_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T304_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['304'],
	scoreMap:[1, 0]
},{
	id:'T305_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T305_0',
	inputType:'radio',
	comment:'comment_T305_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T305_1', 'T305_1'],
	scoreMap:[1, 0]
},{
	id:'T305_1',
	category:CATEGORIES.SYMPTOMS,
	text:'T305_1',
	inputType:'radio',
	comment:'comment_T305_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T305_2', 'T305_2'],
	scoreMap:[1, 0]
},{
	id:'T305_2',
	category:CATEGORIES.SYMPTOMS,
	text:'T305_2',
	inputType:'radio',
	comment:'comment_T305_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T305_3', 'T305_3'],
	scoreMap:[1, 0]
},{
	id:'T305_3',
	category:CATEGORIES.SYMPTOMS,
	text:'T305_3',
	inputType:'radio',
	comment:'comment_T305_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['305'],
	scoreMap:[1, 0]
},{
	id:'T306_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T306_0',
	inputType:'radio',
	comment:'comment_T306_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T306_1', 'T306_1'],
	scoreMap:[1, 0]
},{
	id:'T306_1',
	category:CATEGORIES.SYMPTOMS,
	text:'T306_1',
	inputType:'radio',
	comment:'comment_T306_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T306_2', 'T306_2'],
	scoreMap:[1, 0]
},{
	id:'T306_2',
	category:CATEGORIES.SYMPTOMS,
	text:'T306_2',
	inputType:'radio',
	comment:'comment_T306_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T306_3', 'T306_3'],
	scoreMap:[1, 0]
},{
	id:'T306_3',
	category:CATEGORIES.SYMPTOMS,
	text:'T306_3',
	inputType:'radio',
	comment:'comment_T306_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['306'],
	scoreMap:[1, 0]
},{
	id:'T307_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T307_0',
	inputType:'radio',
	comment:'comment_T307_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T307_1', 'T307_1'],
	scoreMap:[1, 0]
},{
	id:'T307_1',
	category:CATEGORIES.SYMPTOMS,
	text:'T307_1',
	inputType:'radio',
	comment:'comment_T307_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['307'],
	scoreMap:[1, 0]
},{
	id:'T308_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T308_0',
	inputType:'radio',
	comment:'comment_T308_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T308_1', 'T308_1'],
	scoreMap:[1, 0]
},{
	id:'T308_1',
	category:CATEGORIES.SYMPTOMS,
	text:'T308_1',
	inputType:'radio',
	comment:'comment_T308_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T308_2', 'T308_2'],
	scoreMap:[1, 0]
},{
	id:'T308_2',
	category:CATEGORIES.SYMPTOMS,
	text:'T308_2',
	inputType:'radio',
	comment:'comment_T308_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T308_3', 'T308_3'],
	scoreMap:[1, 0]
},{
	id:'T308_3',
	category:CATEGORIES.SYMPTOMS,
	text:'T308_3',
	inputType:'radio',
	comment:'comment_T308_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T308_4', 'T308_4'],
	scoreMap:[1, 0]
},{
	id:'T308_4',
	category:CATEGORIES.SYMPTOMS,
	text:'T308_4',
	inputType:'radio',
	comment:'comment_T308_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['308'],
	scoreMap:[1, 0]
},{
	id:'T309_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T309_0',
	inputType:'radio',
	comment:'comment_T309_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T309_1', 'T309_1'],
	scoreMap:[1, 0]
},{
	id:'T309_1',
	category:CATEGORIES.SYMPTOMS,
	text:'T309_1',
	inputType:'radio',
	comment:'comment_T309_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T309_2', 'T309_2'],
	scoreMap:[1, 0]
},{
	id:'T309_2',
	category:CATEGORIES.SYMPTOMS,
	text:'T309_2',
	inputType:'radio',
	comment:'comment_T309_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T309_3', 'T309_3'],
	scoreMap:[1, 0]
},{
	id:'T309_3',
	category:CATEGORIES.SYMPTOMS,
	text:'T309_3',
	inputType:'radio',
	comment:'comment_T309_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T309_4', 'T309_4'],
	scoreMap:[1, 0]
},{
	id:'T309_4',
	category:CATEGORIES.SYMPTOMS,
	text:'T309_4',
	inputType:'radio',
	comment:'comment_T309_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T309_5', 'T309_5'],
	scoreMap:[1, 0]
},{
	id:'T309_5',
	category:CATEGORIES.SYMPTOMS,
	text:'T309_5',
	inputType:'radio',
	comment:'comment_T309_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T309_6', 'T309_6'],
	scoreMap:[1, 0]
},{
	id:'T309_6',
	category:CATEGORIES.SYMPTOMS,
	text:'T309_6',
	inputType:'radio',
	comment:'comment_T309_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['309'],
	scoreMap:[1, 0]
},{
	id:'T310_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T310_0',
	inputType:'radio',
	comment:'comment_T310_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T310_1', 'T310_1'],
	scoreMap:[1, 0]
},{
	id:'T310_1',
	category:CATEGORIES.SYMPTOMS,
	text:'T310_1',
	inputType:'radio',
	comment:'comment_T310_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['310'],
	scoreMap:[1, 0]
},{
	id:'T311_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T311_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['311'],
	scoreMap:[1, 0]
},{
	id:'T312_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T312_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['312'],
	scoreMap:[1, 0]
},{
	id:'T313_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T313_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['313'],
	scoreMap:[1, 0]
},{
	id:'T314_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T314_0',
	inputType:'radio',
	comment:'comment_T314_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T314_1', 'T314_1'],
	scoreMap:[1, 0]
},{
	id:'T314_1',
	category:CATEGORIES.SYMPTOMS,
	text:'T314_1',
	inputType:'radio',
	comment:'comment_T314_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T314_2', 'T314_2'],
	scoreMap:[1, 0]
},{
	id:'T314_2',
	category:CATEGORIES.SYMPTOMS,
	text:'T314_2',
	inputType:'radio',
	comment:'comment_T314_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T314_3', 'T314_3'],
	scoreMap:[1, 0]
},{
	id:'T314_3',
	category:CATEGORIES.SYMPTOMS,
	text:'T314_3',
	inputType:'radio',
	comment:'comment_T314_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['314'],
	scoreMap:[1, 0]
},{
	id:'T315_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T315_0',
	inputType:'radio',
	comment:'comment_T315_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T315_1', 'T315_1'],
	scoreMap:[1, 0]
},{
	id:'T315_1',
	category:CATEGORIES.SYMPTOMS,
	text:'T315_1',
	inputType:'radio',
	comment:'comment_T315_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['T315_2', 'T315_2'],
	scoreMap:[1, 0]
},{
	id:'T315_2',
	category:CATEGORIES.SYMPTOMS,
	text:'T315_2',
	inputType:'radio',
	comment:'comment_T315_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['315'],
	scoreMap:[1, 0]
},{
	id:'T316_0',
	category:CATEGORIES.SYMPTOMS,
	text:'T316_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['316'],
	scoreMap:[1, 0]
},{
	id:'U317_0',
	category:CATEGORIES.SYMPTOMS,
	text:'U317_0',
	inputType:'radio',
	comment:'comment_U317_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['U317_1', 'U317_1'],
	scoreMap:[1, 0]
},{
	id:'U317_1',
	category:CATEGORIES.SYMPTOMS,
	text:'U317_1',
	inputType:'radio',
	comment:'comment_U317_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['U317_2', 'U317_2'],
	scoreMap:[1, 0]
},{
	id:'U317_2',
	category:CATEGORIES.SYMPTOMS,
	text:'U317_2',
	inputType:'radio',
	comment:'comment_U317_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['317'],
	scoreMap:[1, 0]
},{
	id:'U318_0',
	category:CATEGORIES.SYMPTOMS,
	text:'U318_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['318'],
	scoreMap:[1, 0]
},{
	id:'U319_0',
	category:CATEGORIES.SYMPTOMS,
	text:'U319_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['319'],
	scoreMap:[1, 0]
},{
	id:'U320_0',
	category:CATEGORIES.SYMPTOMS,
	text:'U320_0',
	inputType:'radio',
	comment:'comment_U320_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['U320_1', 'U320_1'],
	scoreMap:[1, 0]
},{
	id:'U320_1',
	category:CATEGORIES.SYMPTOMS,
	text:'U320_1',
	inputType:'radio',
	comment:'comment_U320_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['U320_2', 'U320_2'],
	scoreMap:[1, 0]
},{
	id:'U320_2',
	category:CATEGORIES.SYMPTOMS,
	text:'U320_2',
	inputType:'radio',
	comment:'comment_U320_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['U320_3', 'U320_3'],
	scoreMap:[1, 0]
},{
	id:'U320_3',
	category:CATEGORIES.SYMPTOMS,
	text:'U320_3',
	inputType:'radio',
	comment:'comment_U320_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['U320_4', 'U320_4'],
	scoreMap:[1, 0]
},{
	id:'U320_4',
	category:CATEGORIES.SYMPTOMS,
	text:'U320_4',
	inputType:'radio',
	comment:'comment_U320_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['320'],
	scoreMap:[1, 0]
},{
	id:'V321_0',
	category:CATEGORIES.SYMPTOMS,
	text:'V321_0',
	inputType:'radio',
	comment:'comment_V321_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V321_1', 'V321_1'],
	scoreMap:[1, 0]
},{
	id:'V321_1',
	category:CATEGORIES.SYMPTOMS,
	text:'V321_1',
	inputType:'radio',
	comment:'comment_V321_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V321_2', 'V321_2'],
	scoreMap:[1, 0]
},{
	id:'V321_2',
	category:CATEGORIES.SYMPTOMS,
	text:'V321_2',
	inputType:'radio',
	comment:'comment_V321_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V321_3', 'V321_3'],
	scoreMap:[1, 0]
},{
	id:'V321_3',
	category:CATEGORIES.SYMPTOMS,
	text:'V321_3',
	inputType:'radio',
	comment:'comment_V321_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V321_4', 'V321_4'],
	scoreMap:[1, 0]
},{
	id:'V321_4',
	category:CATEGORIES.SYMPTOMS,
	text:'V321_4',
	inputType:'radio',
	comment:'comment_V321_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['321'],
	scoreMap:[1, 0]
},{
	id:'V322_0',
	category:CATEGORIES.SYMPTOMS,
	text:'V322_0',
	inputType:'radio',
	comment:'comment_V322_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V322_1', 'V322_1'],
	scoreMap:[1, 0]
},{
	id:'V322_1',
	category:CATEGORIES.SYMPTOMS,
	text:'V322_1',
	inputType:'radio',
	comment:'comment_V322_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V322_2', 'V322_2'],
	scoreMap:[1, 0]
},{
	id:'V322_2',
	category:CATEGORIES.SYMPTOMS,
	text:'V322_2',
	inputType:'radio',
	comment:'comment_V322_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V322_3', 'V322_3'],
	scoreMap:[1, 0]
},{
	id:'V322_3',
	category:CATEGORIES.SYMPTOMS,
	text:'V322_3',
	inputType:'radio',
	comment:'comment_V322_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V322_4', 'V322_4'],
	scoreMap:[1, 0]
},{
	id:'V322_4',
	category:CATEGORIES.SYMPTOMS,
	text:'V322_4',
	inputType:'radio',
	comment:'comment_V322_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V322_5', 'V322_5'],
	scoreMap:[1, 0]
},{
	id:'V322_5',
	category:CATEGORIES.SYMPTOMS,
	text:'V322_5',
	inputType:'radio',
	comment:'comment_V322_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V322_6', 'V322_6'],
	scoreMap:[1, 0]
},{
	id:'V322_6',
	category:CATEGORIES.SYMPTOMS,
	text:'V322_6',
	inputType:'radio',
	comment:'comment_V322_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['322'],
	scoreMap:[1, 0]
},{
	id:'V323_0',
	category:CATEGORIES.SYMPTOMS,
	text:'V323_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['323'],
	scoreMap:[1, 0]
},{
	id:'V324_0',
	category:CATEGORIES.SYMPTOMS,
	text:'V324_0',
	inputType:'radio',
	comment:'comment_V324_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V324_1', 'V324_1'],
	scoreMap:[1, 0]
},{
	id:'V324_1',
	category:CATEGORIES.SYMPTOMS,
	text:'V324_1',
	inputType:'radio',
	comment:'comment_V324_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['324'],
	scoreMap:[1, 0]
},{
	id:'V325_0',
	category:CATEGORIES.SYMPTOMS,
	text:'V325_0',
	inputType:'radio',
	comment:'comment_V325_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V325_1', 'V325_1'],
	scoreMap:[1, 0]
},{
	id:'V325_1',
	category:CATEGORIES.SYMPTOMS,
	text:'V325_1',
	inputType:'radio',
	comment:'comment_V325_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V325_2', 'V325_2'],
	scoreMap:[1, 0]
},{
	id:'V325_2',
	category:CATEGORIES.SYMPTOMS,
	text:'V325_2',
	inputType:'radio',
	comment:'comment_V325_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['325'],
	scoreMap:[1, 0]
},{
	id:'V326_0',
	category:CATEGORIES.SYMPTOMS,
	text:'V326_0',
	inputType:'radio',
	comment:'comment_V326_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['V326_1', 'V326_1'],
	scoreMap:[1, 0]
},{
	id:'V326_1',
	category:CATEGORIES.SYMPTOMS,
	text:'V326_1',
	inputType:'radio',
	comment:'comment_V326_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['326'],
	scoreMap:[1, 0]
},{
	id:'W327_0',
	category:CATEGORIES.SYMPTOMS,
	text:'W327_0',
	inputType:'radio',
	comment:'comment_W327_0',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['W327_1', 'W327_1'],
	scoreMap:[1, 0]
},{
	id:'W327_1',
	category:CATEGORIES.SYMPTOMS,
	text:'W327_1',
	inputType:'radio',
	comment:'comment_W327_1',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['W327_2', 'W327_2'],
	scoreMap:[1, 0]
},{
	id:'W327_2',
	category:CATEGORIES.SYMPTOMS,
	text:'W327_2',
	inputType:'radio',
	comment:'comment_W327_2',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['W327_3', 'W327_3'],
	scoreMap:[1, 0]
},{
	id:'W327_3',
	category:CATEGORIES.SYMPTOMS,
	text:'W327_3',
	inputType:'radio',
	comment:'comment_W327_3',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['W327_4', 'W327_4'],
	scoreMap:[1, 0]
},{
	id:'W327_4',
	category:CATEGORIES.SYMPTOMS,
	text:'W327_4',
	inputType:'radio',
	comment:'comment_W327_4',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['W327_5', 'W327_5'],
	scoreMap:[1, 0]
},{
	id:'W327_5',
	category:CATEGORIES.SYMPTOMS,
	text:'W327_5',
	inputType:'radio',
	comment:'comment_W327_5',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['W327_6', 'W327_6'],
	scoreMap:[1, 0]
},{
	id:'W327_6',
	category:CATEGORIES.SYMPTOMS,
	text:'W327_6',
	inputType:'radio',
	comment:'comment_W327_6',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['W327_7', 'W327_7'],
	scoreMap:[1, 0]
},{
	id:'W327_7',
	category:CATEGORIES.SYMPTOMS,
	text:'W327_7',
	inputType:'radio',
	comment:'comment_W327_7',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['W327_8', 'W327_8'],
	scoreMap:[1, 0]
},{
	id:'W327_8',
	category:CATEGORIES.SYMPTOMS,
	text:'W327_8',
	inputType:'radio',
	comment:'comment_W327_8',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['327'],
	scoreMap:[1, 0]
},{
	id:'W328_0',
	category:CATEGORIES.SYMPTOMS,
	text:'W328_0',
	inputType:'radio',
	comment:'',
	options:['answer_yes', 'answer_no'],
	nextQuestionMap:['328'],
	scoreMap:[1, 0]
},];