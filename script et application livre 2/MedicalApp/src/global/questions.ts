import {
  Guard,

} from './guard';


export type Question = {
  id: string;
  category: string;
  comment?: string;
  text?: string;
  inputType: 'radio' | 'date' | 'checkbox' | 'postal' | 'decimal' | 'hidden';
  options?: string[] | CheckboxOption[];
  nextQuestionMap?: string | string[];
  scoreMap?: number[];
  guard?: Guard;
  xmlValueMapping?: number[];
  inputMin?: number;
  inputMax?: number;
  inputStep?: number;
  xmlValue?: (answers: any) => string | number;
  optional?: boolean;
};

export type CheckboxOption = {
  label: string;
  id: string;
};

export const CATEGORIES = {
  PERSONAL: 'personalInfo',
  CONTACT: 'contact',
  SYMPTOMS: 'symptoms', // Box 2 symptoms
  SYMPTOMS_HIGH: 'symptomsHigh', // Box 1 symptoms
  ILLNESS: 'illnesses',
  MEDICATION: 'medication',
};

export const NO_XML = 'X';
export const QUESTION = {
  POSTAL_CODE: 'V1',
  ABOVE_65: 'P1',
  HOUSING: 'P2',
  CARING: 'P3',
  WORKSPACE: 'P4',
  CONTACT_DATE: 'CZ',
  OUT_OF_BREATH: 'SB',
  SYMPTOM_DATE: 'SZ',
  DATA_DONATION: 'X1',
};

export const XML_ORDER = ['V', 'P', 'C', 'S', 'D', 'M'];

export const QUESTIONS: Question[] = [
//*******************PARTIE QUESTOIN :CHOIX PREMIERE LETTERE DE LA MALADIE************
  {
    id:'question',
    category:CATEGORIES.PERSONAL,
    text:'question',
    inputType:'radio',
    comment:'',
    options:['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'L', 'M', 'N', 'O', 'P', 'S', 'T', 'U', 'V', 'W', 'Y'],
    nextQuestionMap:['A1-A4', 'B5-B14', 'C15-C21', 'D22-D26', 'E27-E29', 'F30-F33', 'G34-G35', 'H36-H45', 'I46-I47', 'J48-J48', 'L49-L49', 'M50-M53', 'N54-N58', 'O59-O60', 'P61-P62', 'S63-S69', 'T70-T70', 'U71-U71', 'V72-V73', 'W74-W74', 'Y75-Y75'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//**********AJOUT DELIMITATION MALADIE ALPHABETS DE A.. a A..****************

//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE A1-A4
  {
    id:'A1-A4',
    category:CATEGORIES.PERSONAL,
    text:'A1-A4',
    inputType:'radio',
    comment:'',
    options:['A1', 'A2', 'A3', 'A4'],
    nextQuestionMap:['A1', 'A2', 'A3', 'A4'],
    scoreMap:[0, 0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE B5-B14
  {
    id:'B5-B14',
    category:CATEGORIES.PERSONAL,
    text:'B5-B14',
    inputType:'radio',
    comment:'',
    options:['B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'B12', 'B13', 'B14'],
    nextQuestionMap:['B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'B12', 'B13', 'B14'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE C15-C21
  {
    id:'C15-C21',
    category:CATEGORIES.PERSONAL,
    text:'C15-C21',
    inputType:'radio',
    comment:'',
    options:['C15', 'C16', 'C17', 'C18', 'C19', 'C20', 'C21'],
    nextQuestionMap:['C15', 'C16', 'C17', 'C18', 'C19', 'C20', 'C21'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE D22-D26
  {
    id:'D22-D26',
    category:CATEGORIES.PERSONAL,
    text:'D22-D26',
    inputType:'radio',
    comment:'',
    options:['D22', 'D23', 'D24', 'D25', 'D26'],
    nextQuestionMap:['D22', 'D23', 'D24', 'D25', 'D26'],
    scoreMap:[0, 0, 0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE E27-E29
  {
    id:'E27-E29',
    category:CATEGORIES.PERSONAL,
    text:'E27-E29',
    inputType:'radio',
    comment:'',
    options:['E27', 'E28', 'E29'],
    nextQuestionMap:['E27', 'E28', 'E29'],
    scoreMap:[0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE F30-F33
  {
    id:'F30-F33',
    category:CATEGORIES.PERSONAL,
    text:'F30-F33',
    inputType:'radio',
    comment:'',
    options:['F30', 'F31', 'F32', 'F33'],
    nextQuestionMap:['F30', 'F31', 'F32', 'F33'],
    scoreMap:[0, 0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE G34-G35
  {
    id:'G34-G35',
    category:CATEGORIES.PERSONAL,
    text:'G34-G35',
    inputType:'radio',
    comment:'',
    options:['G34', 'G35'],
    nextQuestionMap:['G34', 'G35'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE H36-H45
  {
    id:'H36-H45',
    category:CATEGORIES.PERSONAL,
    text:'H36-H45',
    inputType:'radio',
    comment:'',
    options:['H36', 'H37', 'H38', 'H39', 'H40', 'H41', 'H42', 'H43', 'H44', 'H45'],
    nextQuestionMap:['H36', 'H37', 'H38', 'H39', 'H40', 'H41', 'H42', 'H43', 'H44', 'H45'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE I46-I47
  {
    id:'I46-I47',
    category:CATEGORIES.PERSONAL,
    text:'I46-I47',
    inputType:'radio',
    comment:'',
    options:['I46', 'I47'],
    nextQuestionMap:['I46', 'I47'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE J48-J48
  {
    id:'J48-J48',
    category:CATEGORIES.PERSONAL,
    text:'J48-J48',
    inputType:'radio',
    comment:'',
    options:['J48'],
    nextQuestionMap:['J48'],
    scoreMap:[0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE L49-L49
  {
    id:'L49-L49',
    category:CATEGORIES.PERSONAL,
    text:'L49-L49',
    inputType:'radio',
    comment:'',
    options:['L49'],
    nextQuestionMap:['L49'],
    scoreMap:[0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE M50-M53
  {
    id:'M50-M53',
    category:CATEGORIES.PERSONAL,
    text:'M50-M53',
    inputType:'radio',
    comment:'',
    options:['M50', 'M51', 'M52', 'M53'],
    nextQuestionMap:['M50', 'M51', 'M52', 'M53'],
    scoreMap:[0, 0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE N54-N58
  {
    id:'N54-N58',
    category:CATEGORIES.PERSONAL,
    text:'N54-N58',
    inputType:'radio',
    comment:'',
    options:['N54', 'N55', 'N56', 'N57', 'N58'],
    nextQuestionMap:['N54', 'N55', 'N56', 'N57', 'N58'],
    scoreMap:[0, 0, 0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE O59-O60
  {
    id:'O59-O60',
    category:CATEGORIES.PERSONAL,
    text:'O59-O60',
    inputType:'radio',
    comment:'',
    options:['O59', 'O60'],
    nextQuestionMap:['O59', 'O60'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE P61-P62
  {
    id:'P61-P62',
    category:CATEGORIES.PERSONAL,
    text:'P61-P62',
    inputType:'radio',
    comment:'',
    options:['P61', 'P62'],
    nextQuestionMap:['P61', 'P62'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE S63-S69
  {
    id:'S63-S69',
    category:CATEGORIES.PERSONAL,
    text:'S63-S69',
    inputType:'radio',
    comment:'',
    options:['S63', 'S64', 'S65', 'S66', 'S67', 'S68', 'S69'],
    nextQuestionMap:['S63', 'S64', 'S65', 'S66', 'S67', 'S68', 'S69'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE T70-T70
  {
    id:'T70-T70',
    category:CATEGORIES.PERSONAL,
    text:'T70-T70',
    inputType:'radio',
    comment:'',
    options:['T70'],
    nextQuestionMap:['T70'],
    scoreMap:[0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE U71-U71
  {
    id:'U71-U71',
    category:CATEGORIES.PERSONAL,
    text:'U71-U71',
    inputType:'radio',
    comment:'',
    options:['U71'],
    nextQuestionMap:['U71'],
    scoreMap:[0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE V72-V73
  {
    id:'V72-V73',
    category:CATEGORIES.PERSONAL,
    text:'V72-V73',
    inputType:'radio',
    comment:'',
    options:['V72', 'V73'],
    nextQuestionMap:['V72', 'V73'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE W74-W74
  {
    id:'W74-W74',
    category:CATEGORIES.PERSONAL,
    text:'W74-W74',
    inputType:'radio',
    comment:'',
    options:['W74'],
    nextQuestionMap:['W74'],
    scoreMap:[0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE Y75-Y75
  {
    id:'Y75-Y75',
    category:CATEGORIES.PERSONAL,
    text:'Y75-Y75',
    inputType:'radio',
    comment:'',
    options:['Y75'],
    nextQuestionMap:['Y75'],
    scoreMap:[0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE A1
  {
    id:'A1',
    category:CATEGORIES.PERSONAL,
    text:'A1',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_1', 'your_doctor_visit_1', 'question_1_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE A2
  {
    id:'A2',
    category:CATEGORIES.PERSONAL,
    text:'A2',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_2', 'your_doctor_visit_2', 'question_2_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE A3
  {
    id:'A3',
    category:CATEGORIES.PERSONAL,
    text:'A3',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_3', 'your_doctor_visit_3', 'question_3_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE A4
  {
    id:'A4',
    category:CATEGORIES.PERSONAL,
    text:'A4',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_4', 'your_doctor_visit_4', 'question_4_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B5
  {
    id:'B5',
    category:CATEGORIES.PERSONAL,
    text:'B5',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_5', 'your_doctor_visit_5', 'question_5_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B6
  {
    id:'B6',
    category:CATEGORIES.PERSONAL,
    text:'B6',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_6', 'your_doctor_visit_6', 'question_6_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B7
  {
    id:'B7',
    category:CATEGORIES.PERSONAL,
    text:'B7',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_7', 'your_doctor_visit_7', 'question_7_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B8
  {
    id:'B8',
    category:CATEGORIES.PERSONAL,
    text:'B8',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_8', 'your_doctor_visit_8', 'question_8_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B9
  {
    id:'B9',
    category:CATEGORIES.PERSONAL,
    text:'B9',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_9', 'your_doctor_visit_9', 'option_9_0-option_9_2'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B10
  {
    id:'B10',
    category:CATEGORIES.PERSONAL,
    text:'B10',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_10', 'your_doctor_visit_10', 'option_10_0-option_10_3'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B11
  {
    id:'B11',
    category:CATEGORIES.PERSONAL,
    text:'B11',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_11', 'your_doctor_visit_11', 'question_11_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B12
  {
    id:'B12',
    category:CATEGORIES.PERSONAL,
    text:'B12',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_12', 'your_doctor_visit_12', 'question_12_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B13
  {
    id:'B13',
    category:CATEGORIES.PERSONAL,
    text:'B13',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_13', 'your_doctor_visit_13', 'question_13_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B14
  {
    id:'B14',
    category:CATEGORIES.PERSONAL,
    text:'B14',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_14', 'your_doctor_visit_14', 'question_14_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C15
  {
    id:'C15',
    category:CATEGORIES.PERSONAL,
    text:'C15',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_15', 'your_doctor_visit_15', 'question_15_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C16
  {
    id:'C16',
    category:CATEGORIES.PERSONAL,
    text:'C16',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_16', 'your_doctor_visit_16', 'question_16_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C17
  {
    id:'C17',
    category:CATEGORIES.PERSONAL,
    text:'C17',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_17', 'your_doctor_visit_17', 'question_17_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C18
  {
    id:'C18',
    category:CATEGORIES.PERSONAL,
    text:'C18',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_18', 'your_doctor_visit_18', 'question_18_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C19
  {
    id:'C19',
    category:CATEGORIES.PERSONAL,
    text:'C19',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_19', 'your_doctor_visit_19', 'question_19_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C20
  {
    id:'C20',
    category:CATEGORIES.PERSONAL,
    text:'C20',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_20', 'your_doctor_visit_20', 'question_20_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C21
  {
    id:'C21',
    category:CATEGORIES.PERSONAL,
    text:'C21',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit'],
    nextQuestionMap:['introduction_21', 'your_doctor_visit_21'],
    scoreMap:[0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE D22
  {
    id:'D22',
    category:CATEGORIES.PERSONAL,
    text:'D22',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit'],
    nextQuestionMap:['introduction_22', 'your_doctor_visit_22'],
    scoreMap:[0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE D23
  {
    id:'D23',
    category:CATEGORIES.PERSONAL,
    text:'D23',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_23', 'your_doctor_visit_23', 'question_23_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE D24
  {
    id:'D24',
    category:CATEGORIES.PERSONAL,
    text:'D24',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_24', 'your_doctor_visit_24', 'option_24_0-option_24_2'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE D25
  {
    id:'D25',
    category:CATEGORIES.PERSONAL,
    text:'D25',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_25', 'your_doctor_visit_25', 'question_25_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE D26
  {
    id:'D26',
    category:CATEGORIES.PERSONAL,
    text:'D26',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_26', 'your_doctor_visit_26', 'question_26_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE E27
  {
    id:'E27',
    category:CATEGORIES.PERSONAL,
    text:'E27',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_27', 'your_doctor_visit_27', 'option_27_0-option_27_1'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE E28
  {
    id:'E28',
    category:CATEGORIES.PERSONAL,
    text:'E28',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_28', 'your_doctor_visit_28', 'question_28_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE E29
  {
    id:'E29',
    category:CATEGORIES.PERSONAL,
    text:'E29',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_29', 'your_doctor_visit_29', 'option_29_0-option_29_6'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE F30
  {
    id:'F30',
    category:CATEGORIES.PERSONAL,
    text:'F30',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_30', 'your_doctor_visit_30', 'question_30_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE F31
  {
    id:'F31',
    category:CATEGORIES.PERSONAL,
    text:'F31',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_31', 'your_doctor_visit_31', 'option_31_0-option_31_2'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE F32
  {
    id:'F32',
    category:CATEGORIES.PERSONAL,
    text:'F32',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_32', 'your_doctor_visit_32', 'question_32_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE F33
  {
    id:'F33',
    category:CATEGORIES.PERSONAL,
    text:'F33',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit'],
    nextQuestionMap:['introduction_33', 'your_doctor_visit_33'],
    scoreMap:[0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE G34
  {
    id:'G34',
    category:CATEGORIES.PERSONAL,
    text:'G34',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_34', 'your_doctor_visit_34', 'question_34_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE G35
  {
    id:'G35',
    category:CATEGORIES.PERSONAL,
    text:'G35',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_35', 'your_doctor_visit_35', 'question_35_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H36
  {
    id:'H36',
    category:CATEGORIES.PERSONAL,
    text:'H36',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_36', 'your_doctor_visit_36', 'question_36_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H37
  {
    id:'H37',
    category:CATEGORIES.PERSONAL,
    text:'H37',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_37', 'your_doctor_visit_37', 'question_37_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H38
  {
    id:'H38',
    category:CATEGORIES.PERSONAL,
    text:'H38',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_38', 'your_doctor_visit_38', 'question_38_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H39
  {
    id:'H39',
    category:CATEGORIES.PERSONAL,
    text:'H39',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_39', 'your_doctor_visit_39', 'option_39_0-option_39_1'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H40
  {
    id:'H40',
    category:CATEGORIES.PERSONAL,
    text:'H40',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_40', 'your_doctor_visit_40', 'question_40_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H41
  {
    id:'H41',
    category:CATEGORIES.PERSONAL,
    text:'H41',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit'],
    nextQuestionMap:['introduction_41', 'your_doctor_visit_41'],
    scoreMap:[0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H42
  {
    id:'H42',
    category:CATEGORIES.PERSONAL,
    text:'H42',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_42', 'your_doctor_visit_42', 'question_42_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H43
  {
    id:'H43',
    category:CATEGORIES.PERSONAL,
    text:'H43',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_43', 'your_doctor_visit_43', 'question_43_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H44
  {
    id:'H44',
    category:CATEGORIES.PERSONAL,
    text:'H44',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_44', 'your_doctor_visit_44', 'question_44_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H45
  {
    id:'H45',
    category:CATEGORIES.PERSONAL,
    text:'H45',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_45', 'your_doctor_visit_45', 'question_45_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE I46
  {
    id:'I46',
    category:CATEGORIES.PERSONAL,
    text:'I46',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_46', 'your_doctor_visit_46', 'option_46_0-option_46_5'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE I47
  {
    id:'I47',
    category:CATEGORIES.PERSONAL,
    text:'I47',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_47', 'your_doctor_visit_47', 'question_47_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE J48
  {
    id:'J48',
    category:CATEGORIES.PERSONAL,
    text:'J48',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_48', 'your_doctor_visit_48', 'option_48_0-option_48_7'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE L49
  {
    id:'L49',
    category:CATEGORIES.PERSONAL,
    text:'L49',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_49', 'your_doctor_visit_49', 'question_49_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE M50
  {
    id:'M50',
    category:CATEGORIES.PERSONAL,
    text:'M50',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_50', 'your_doctor_visit_50', 'question_50_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE M51
  {
    id:'M51',
    category:CATEGORIES.PERSONAL,
    text:'M51',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit'],
    nextQuestionMap:['introduction_51', 'your_doctor_visit_51'],
    scoreMap:[0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE M52
  {
    id:'M52',
    category:CATEGORIES.PERSONAL,
    text:'M52',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_52', 'your_doctor_visit_52', 'option_52_0-option_52_4'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE M53
  {
    id:'M53',
    category:CATEGORIES.PERSONAL,
    text:'M53',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_53', 'your_doctor_visit_53', 'question_53_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE N54
  {
    id:'N54',
    category:CATEGORIES.PERSONAL,
    text:'N54',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_54', 'your_doctor_visit_54', 'option_54_0-option_54_2'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE N55
  {
    id:'N55',
    category:CATEGORIES.PERSONAL,
    text:'N55',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_55', 'your_doctor_visit_55', 'option_55_0-option_55_1'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE N56
  {
    id:'N56',
    category:CATEGORIES.PERSONAL,
    text:'N56',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_56', 'your_doctor_visit_56', 'option_56_0-option_56_1'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE N57
  {
    id:'N57',
    category:CATEGORIES.PERSONAL,
    text:'N57',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit'],
    nextQuestionMap:['introduction_57', 'your_doctor_visit_57'],
    scoreMap:[0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE N58
  {
    id:'N58',
    category:CATEGORIES.PERSONAL,
    text:'N58',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_58', 'your_doctor_visit_58', 'question_58_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE O59
  {
    id:'O59',
    category:CATEGORIES.PERSONAL,
    text:'O59',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit'],
    nextQuestionMap:['introduction_59', 'your_doctor_visit_59'],
    scoreMap:[0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE O60
  {
    id:'O60',
    category:CATEGORIES.PERSONAL,
    text:'O60',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_60', 'your_doctor_visit_60', 'question_60_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE P61
  {
    id:'P61',
    category:CATEGORIES.PERSONAL,
    text:'P61',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_61', 'your_doctor_visit_61', 'question_61_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE P62
  {
    id:'P62',
    category:CATEGORIES.PERSONAL,
    text:'P62',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit'],
    nextQuestionMap:['introduction_62', 'your_doctor_visit_62'],
    scoreMap:[0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S63
  {
    id:'S63',
    category:CATEGORIES.PERSONAL,
    text:'S63',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_63', 'your_doctor_visit_63', 'option_63_0-option_63_1'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S64
  {
    id:'S64',
    category:CATEGORIES.PERSONAL,
    text:'S64',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_64', 'your_doctor_visit_64', 'option_64_0-option_64_1'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S65
  {
    id:'S65',
    category:CATEGORIES.PERSONAL,
    text:'S65',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_65', 'your_doctor_visit_65', 'option_65_0-option_65_1'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S66
  {
    id:'S66',
    category:CATEGORIES.PERSONAL,
    text:'S66',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_66', 'your_doctor_visit_66', 'option_66_0-option_66_1'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S67
  {
    id:'S67',
    category:CATEGORIES.PERSONAL,
    text:'S67',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_67', 'your_doctor_visit_67', 'question_67_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S68
  {
    id:'S68',
    category:CATEGORIES.PERSONAL,
    text:'S68',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_68', 'your_doctor_visit_68', 'question_68_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S69
  {
    id:'S69',
    category:CATEGORIES.PERSONAL,
    text:'S69',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_69', 'your_doctor_visit_69', 'option_69_0-option_69_1'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE T70
  {
    id:'T70',
    category:CATEGORIES.PERSONAL,
    text:'T70',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_70', 'your_doctor_visit_70', 'option_70_0-option_70_1'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE U71
  {
    id:'U71',
    category:CATEGORIES.PERSONAL,
    text:'U71',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_71', 'your_doctor_visit_71', 'option_71_0-option_71_4'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE V72
  {
    id:'V72',
    category:CATEGORIES.PERSONAL,
    text:'V72',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_72', 'your_doctor_visit_72', 'question_72_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE V73
  {
    id:'V73',
    category:CATEGORIES.PERSONAL,
    text:'V73',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_73', 'your_doctor_visit_73', 'question_73_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE W74
  {
    id:'W74',
    category:CATEGORIES.PERSONAL,
    text:'W74',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_74', 'your_doctor_visit_74', 'question_74_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE Y75
  {
    id:'Y75',
    category:CATEGORIES.PERSONAL,
    text:'Y75',
    inputType:'radio',
    comment:'',
    options:['introduction', 'your doctor visit', 'questionnaire'],
    nextQuestionMap:['introduction_75', 'your_doctor_visit_75', 'question_75_0'],
    scoreMap:[0, 0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE B9
  {
    id:'option_9_0-option_9_2',
    category:CATEGORIES.PERSONAL,
    text:'option_9_0-option_9_2',
    inputType:'radio',
    comment:'',
    options:['option_9_0', 'option_9_1', 'option_9_2'],
    nextQuestionMap:['question_9_0', 'question_9_1', 'question_9_2'],
    scoreMap:[0, 0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE B10
  {
    id:'option_10_0-option_10_3',
    category:CATEGORIES.PERSONAL,
    text:'option_10_0-option_10_3',
    inputType:'radio',
    comment:'',
    options:['option_10_0', 'option_10_1', 'option_10_2', 'option_10_3'],
    nextQuestionMap:['question_10_0', 'question_10_1', 'question_10_2', 'question_10_3'],
    scoreMap:[0, 0, 0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE D24
  {
    id:'option_24_0-option_24_2',
    category:CATEGORIES.PERSONAL,
    text:'option_24_0-option_24_2',
    inputType:'radio',
    comment:'',
    options:['option_24_0', 'option_24_1', 'option_24_2'],
    nextQuestionMap:['question_24_0', 'question_24_1', 'question_24_2'],
    scoreMap:[0, 0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE E27
  {
    id:'option_27_0-option_27_1',
    category:CATEGORIES.PERSONAL,
    text:'option_27_0-option_27_1',
    inputType:'radio',
    comment:'',
    options:['option_27_0', 'option_27_1'],
    nextQuestionMap:['question_27_0', 'question_27_1'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE E29
  {
    id:'option_29_0-option_29_6',
    category:CATEGORIES.PERSONAL,
    text:'option_29_0-option_29_6',
    inputType:'radio',
    comment:'',
    options:['option_29_0', 'option_29_1', 'option_29_2', 'option_29_3', 'option_29_4', 'option_29_5', 'option_29_6'],
    nextQuestionMap:['question_29_0', 'question_29_1', 'question_29_2', 'question_29_3', 'question_29_4', 'question_29_5', 'question_29_6'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE F31
  {
    id:'option_31_0-option_31_2',
    category:CATEGORIES.PERSONAL,
    text:'option_31_0-option_31_2',
    inputType:'radio',
    comment:'',
    options:['option_31_0', 'option_31_1', 'option_31_2'],
    nextQuestionMap:['question_31_0', 'question_31_1', 'question_31_2'],
    scoreMap:[0, 0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE H39
  {
    id:'option_39_0-option_39_1',
    category:CATEGORIES.PERSONAL,
    text:'option_39_0-option_39_1',
    inputType:'radio',
    comment:'',
    options:['option_39_0', 'option_39_1'],
    nextQuestionMap:['question_39_0', 'question_39_1'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE I46
  {
    id:'option_46_0-option_46_5',
    category:CATEGORIES.PERSONAL,
    text:'option_46_0-option_46_5',
    inputType:'radio',
    comment:'',
    options:['option_46_0', 'option_46_1', 'option_46_2', 'option_46_3', 'option_46_4', 'option_46_5'],
    nextQuestionMap:['question_46_0', 'question_46_1', 'question_46_2', 'question_46_3', 'question_46_4', 'question_46_5'],
    scoreMap:[0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE J48
  {
    id:'option_48_0-option_48_7',
    category:CATEGORIES.PERSONAL,
    text:'option_48_0-option_48_7',
    inputType:'radio',
    comment:'',
    options:['option_48_0', 'option_48_1', 'option_48_2', 'option_48_3', 'option_48_4', 'option_48_5', 'option_48_6', 'option_48_7'],
    nextQuestionMap:['question_48_0', 'question_48_1', 'question_48_2', 'question_48_3', 'question_48_4', 'question_48_5', 'question_48_6', 'question_48_7'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE M52
  {
    id:'option_52_0-option_52_4',
    category:CATEGORIES.PERSONAL,
    text:'option_52_0-option_52_4',
    inputType:'radio',
    comment:'',
    options:['option_52_0', 'option_52_1', 'option_52_2', 'option_52_3', 'option_52_4'],
    nextQuestionMap:['question_52_0', 'question_52_1', 'question_52_2', 'question_52_3', 'question_52_4'],
    scoreMap:[0, 0, 0, 0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE N54
  {
    id:'option_54_0-option_54_2',
    category:CATEGORIES.PERSONAL,
    text:'option_54_0-option_54_2',
    inputType:'radio',
    comment:'',
    options:['option_54_0', 'option_54_1', 'option_54_2'],
    nextQuestionMap:['question_54_0', 'question_54_1', 'question_54_2'],
    scoreMap:[0, 0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE N55
  {
    id:'option_55_0-option_55_1',
    category:CATEGORIES.PERSONAL,
    text:'option_55_0-option_55_1',
    inputType:'radio',
    comment:'',
    options:['option_55_0', 'option_55_1'],
    nextQuestionMap:['question_55_0', 'question_55_1'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE N56
  {
    id:'option_56_0-option_56_1',
    category:CATEGORIES.PERSONAL,
    text:'option_56_0-option_56_1',
    inputType:'radio',
    comment:'',
    options:['option_56_0', 'option_56_1'],
    nextQuestionMap:['question_56_0', 'question_56_1'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE S63
  {
    id:'option_63_0-option_63_1',
    category:CATEGORIES.PERSONAL,
    text:'option_63_0-option_63_1',
    inputType:'radio',
    comment:'',
    options:['option_63_0', 'option_63_1'],
    nextQuestionMap:['question_63_0', 'question_63_1'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE S64
  {
    id:'option_64_0-option_64_1',
    category:CATEGORIES.PERSONAL,
    text:'option_64_0-option_64_1',
    inputType:'radio',
    comment:'',
    options:['option_64_0', 'option_64_1'],
    nextQuestionMap:['question_64_0', 'question_64_1'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE S65
  {
    id:'option_65_0-option_65_1',
    category:CATEGORIES.PERSONAL,
    text:'option_65_0-option_65_1',
    inputType:'radio',
    comment:'',
    options:['option_65_0', 'option_65_1'],
    nextQuestionMap:['question_65_0', 'question_65_1'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE S66
  {
    id:'option_66_0-option_66_1',
    category:CATEGORIES.PERSONAL,
    text:'option_66_0-option_66_1',
    inputType:'radio',
    comment:'',
    options:['option_66_0', 'option_66_1'],
    nextQuestionMap:['question_66_0', 'question_66_1'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE S69
  {
    id:'option_69_0-option_69_1',
    category:CATEGORIES.PERSONAL,
    text:'option_69_0-option_69_1',
    inputType:'radio',
    comment:'',
    options:['option_69_0', 'option_69_1'],
    nextQuestionMap:['question_69_0', 'question_69_1'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE T70
  {
    id:'option_70_0-option_70_1',
    category:CATEGORIES.PERSONAL,
    text:'option_70_0-option_70_1',
    inputType:'radio',
    comment:'',
    options:['option_70_0', 'option_70_1'],
    nextQuestionMap:['question_70_0', 'question_70_1'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE OPTION MALADIE U71
  {
    id:'option_71_0-option_71_4',
    category:CATEGORIES.PERSONAL,
    text:'option_71_0-option_71_4',
    inputType:'radio',
    comment:'',
    options:['option_71_0', 'option_71_1', 'option_71_2', 'option_71_3', 'option_71_4'],
    nextQuestionMap:['question_71_0', 'question_71_1', 'question_71_2', 'question_71_3', 'question_71_4'],
    scoreMap:[0, 0, 0, 0, 0]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['100'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['101'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['102'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['103'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['104'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['105'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['106'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['107'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['108'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['109'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1011'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_12',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1012'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_13',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_13',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1013'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_14',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_14',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1014'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_15',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_15',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1015'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_16',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_16',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1016'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_17',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_17',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1017'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 1
  {
    id:'reponse_1_0_18',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_1_0_18',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1018'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 1
  {
    id:'question_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_1_0',
    inputType:'radio',
    comment:'',
    options:['reponse_1_0_0', 'reponse_1_0_1', 'reponse_1_0_2', 'reponse_1_0_3', 'reponse_1_0_4', 'reponse_1_0_5', 'reponse_1_0_6', 'reponse_1_0_7', 'reponse_1_0_8', 'reponse_1_0_9', 'reponse_1_0_10', 'reponse_1_0_11', 'reponse_1_0_12', 'reponse_1_0_13', 'reponse_1_0_14', 'reponse_1_0_15', 'reponse_1_0_16', 'reponse_1_0_17', 'reponse_1_0_18'],
    nextQuestionMap:['reponse_1_0_0', 'reponse_1_0_1', 'reponse_1_0_2', 'reponse_1_0_3', 'reponse_1_0_4', 'reponse_1_0_5', 'reponse_1_0_6', 'reponse_1_0_7', 'reponse_1_0_8', 'reponse_1_0_9', 'reponse_1_0_10', 'reponse_1_0_11', 'reponse_1_0_12', 'reponse_1_0_13', 'reponse_1_0_14', 'reponse_1_0_15', 'reponse_1_0_16', 'reponse_1_0_17', 'reponse_1_0_18'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['200'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['201'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['202'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['203'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['204'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['205'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['206'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['207'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['208'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['209'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2011'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_12',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2012'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_13',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_13',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2013'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_14',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_14',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2014'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_15',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_15',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2015'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_16',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_16',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2016'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_17',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_17',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2017'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_18',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_18',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2018'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_19',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_19',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2019'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_20',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_20',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2020'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 2
  {
    id:'reponse_2_0_21',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_2_0_21',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2021'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 2
  {
    id:'question_2_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_2_0',
    inputType:'radio',
    comment:'',
    options:['reponse_2_0_0', 'reponse_2_0_1', 'reponse_2_0_2', 'reponse_2_0_3', 'reponse_2_0_4', 'reponse_2_0_5', 'reponse_2_0_6', 'reponse_2_0_7', 'reponse_2_0_8', 'reponse_2_0_9', 'reponse_2_0_10', 'reponse_2_0_11', 'reponse_2_0_12', 'reponse_2_0_13', 'reponse_2_0_14', 'reponse_2_0_15', 'reponse_2_0_16', 'reponse_2_0_17', 'reponse_2_0_18', 'reponse_2_0_19', 'reponse_2_0_20', 'reponse_2_0_21'],
    nextQuestionMap:['reponse_2_0_0', 'reponse_2_0_1', 'reponse_2_0_2', 'reponse_2_0_3', 'reponse_2_0_4', 'reponse_2_0_5', 'reponse_2_0_6', 'reponse_2_0_7', 'reponse_2_0_8', 'reponse_2_0_9', 'reponse_2_0_10', 'reponse_2_0_11', 'reponse_2_0_12', 'reponse_2_0_13', 'reponse_2_0_14', 'reponse_2_0_15', 'reponse_2_0_16', 'reponse_2_0_17', 'reponse_2_0_18', 'reponse_2_0_19', 'reponse_2_0_20', 'reponse_2_0_21'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 3
  {
    id:'reponse_3_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_3_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['300'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 3
  {
    id:'reponse_3_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_3_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['301'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 3
  {
    id:'reponse_3_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_3_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['302'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 3
  {
    id:'reponse_3_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_3_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['303'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 3
  {
    id:'reponse_3_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_3_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['304'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 3
  {
    id:'question_3_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_3_0',
    inputType:'radio',
    comment:'',
    options:['reponse_3_0_0', 'reponse_3_0_1', 'reponse_3_0_2', 'reponse_3_0_3', 'reponse_3_0_4'],
    nextQuestionMap:['reponse_3_0_0', 'reponse_3_0_1', 'reponse_3_0_2', 'reponse_3_0_3', 'reponse_3_0_4'],
    scoreMap:[1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 4
  {
    id:'reponse_4_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_4_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['400'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 4
  {
    id:'reponse_4_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_4_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['401'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 4
  {
    id:'reponse_4_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_4_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['402'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 4
  {
    id:'reponse_4_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_4_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['403'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 4
  {
    id:'reponse_4_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_4_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['404'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 4
  {
    id:'reponse_4_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_4_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['405'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 4
  {
    id:'reponse_4_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_4_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['406'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 4
  {
    id:'reponse_4_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_4_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['407'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 4
  {
    id:'question_4_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_4_0',
    inputType:'radio',
    comment:'',
    options:['reponse_4_0_0', 'reponse_4_0_1', 'reponse_4_0_2', 'reponse_4_0_3', 'reponse_4_0_4', 'reponse_4_0_5', 'reponse_4_0_6', 'reponse_4_0_7'],
    nextQuestionMap:['reponse_4_0_0', 'reponse_4_0_1', 'reponse_4_0_2', 'reponse_4_0_3', 'reponse_4_0_4', 'reponse_4_0_5', 'reponse_4_0_6', 'reponse_4_0_7'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['500'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['501'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['502'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['503'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['504'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['505'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['506'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['507'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['508'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['509'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5011'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_12',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5012'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_13',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_13',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5013'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_14',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_14',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5014'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_15',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_15',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5015'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 5
  {
    id:'reponse_5_0_16',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_5_0_16',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5016'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 5
  {
    id:'question_5_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_5_0',
    inputType:'radio',
    comment:'',
    options:['reponse_5_0_0', 'reponse_5_0_1', 'reponse_5_0_2', 'reponse_5_0_3', 'reponse_5_0_4', 'reponse_5_0_5', 'reponse_5_0_6', 'reponse_5_0_7', 'reponse_5_0_8', 'reponse_5_0_9', 'reponse_5_0_10', 'reponse_5_0_11', 'reponse_5_0_12', 'reponse_5_0_13', 'reponse_5_0_14', 'reponse_5_0_15', 'reponse_5_0_16'],
    nextQuestionMap:['reponse_5_0_0', 'reponse_5_0_1', 'reponse_5_0_2', 'reponse_5_0_3', 'reponse_5_0_4', 'reponse_5_0_5', 'reponse_5_0_6', 'reponse_5_0_7', 'reponse_5_0_8', 'reponse_5_0_9', 'reponse_5_0_10', 'reponse_5_0_11', 'reponse_5_0_12', 'reponse_5_0_13', 'reponse_5_0_14', 'reponse_5_0_15', 'reponse_5_0_16'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 6
  {
    id:'reponse_6_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_6_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['600'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 6
  {
    id:'reponse_6_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_6_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['601'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 6
  {
    id:'reponse_6_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_6_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['602'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 6
  {
    id:'reponse_6_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_6_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['603'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 6
  {
    id:'question_6_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_6_0',
    inputType:'radio',
    comment:'',
    options:['reponse_6_0_0', 'reponse_6_0_1', 'reponse_6_0_2', 'reponse_6_0_3'],
    nextQuestionMap:['reponse_6_0_0', 'reponse_6_0_1', 'reponse_6_0_2', 'reponse_6_0_3'],
    scoreMap:[1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 7
  {
    id:'reponse_7_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_7_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['700'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 7
  {
    id:'reponse_7_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_7_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['701'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 7
  {
    id:'reponse_7_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_7_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['702'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 7
  {
    id:'reponse_7_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_7_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['703'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 7
  {
    id:'reponse_7_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_7_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['704'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 7
  {
    id:'reponse_7_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_7_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['705'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 7
  {
    id:'reponse_7_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_7_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['706'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 7
  {
    id:'question_7_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_7_0',
    inputType:'radio',
    comment:'',
    options:['reponse_7_0_0', 'reponse_7_0_1', 'reponse_7_0_2', 'reponse_7_0_3', 'reponse_7_0_4', 'reponse_7_0_5', 'reponse_7_0_6'],
    nextQuestionMap:['reponse_7_0_0', 'reponse_7_0_1', 'reponse_7_0_2', 'reponse_7_0_3', 'reponse_7_0_4', 'reponse_7_0_5', 'reponse_7_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 8
  {
    id:'reponse_8_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_8_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['800'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 8
  {
    id:'reponse_8_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_8_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['801'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 8
  {
    id:'reponse_8_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_8_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['802'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 8
  {
    id:'reponse_8_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_8_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['803'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 8
  {
    id:'reponse_8_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_8_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['804'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 8
  {
    id:'reponse_8_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_8_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['805'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 8
  {
    id:'reponse_8_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_8_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['806'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 8
  {
    id:'question_8_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_8_0',
    inputType:'radio',
    comment:'',
    options:['reponse_8_0_0', 'reponse_8_0_1', 'reponse_8_0_2', 'reponse_8_0_3', 'reponse_8_0_4', 'reponse_8_0_5', 'reponse_8_0_6'],
    nextQuestionMap:['reponse_8_0_0', 'reponse_8_0_1', 'reponse_8_0_2', 'reponse_8_0_3', 'reponse_8_0_4', 'reponse_8_0_5', 'reponse_8_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['900'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['901'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['902'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['903'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['904'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['905'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['906'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 9
  {
    id:'question_9_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_9_0',
    inputType:'radio',
    comment:'',
    options:['reponse_9_0_0', 'reponse_9_0_1', 'reponse_9_0_2', 'reponse_9_0_3', 'reponse_9_0_4', 'reponse_9_0_5', 'reponse_9_0_6'],
    nextQuestionMap:['reponse_9_0_0', 'reponse_9_0_1', 'reponse_9_0_2', 'reponse_9_0_3', 'reponse_9_0_4', 'reponse_9_0_5', 'reponse_9_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['910'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['911'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 9
  {
    id:'question_9_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_9_1',
    inputType:'radio',
    comment:'',
    options:['reponse_9_1_0', 'reponse_9_1_1'],
    nextQuestionMap:['reponse_9_1_0', 'reponse_9_1_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_2_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_2_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['920'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_2_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_2_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['921'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_2_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_2_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['922'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 9
  {
    id:'reponse_9_2_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_9_2_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['923'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 9
  {
    id:'question_9_2',
    category:CATEGORIES.SYMPTOMS,
    text:'question_9_2',
    inputType:'radio',
    comment:'',
    options:['reponse_9_2_0', 'reponse_9_2_1', 'reponse_9_2_2', 'reponse_9_2_3'],
    nextQuestionMap:['reponse_9_2_0', 'reponse_9_2_1', 'reponse_9_2_2', 'reponse_9_2_3'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 10
  {
    id:'reponse_10_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_10_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1000'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 10
  {
    id:'reponse_10_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_10_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1001'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 10
  {
    id:'reponse_10_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_10_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1002'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 10
  {
    id:'reponse_10_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_10_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1003'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 10
  {
    id:'question_10_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_10_0',
    inputType:'radio',
    comment:'',
    options:['reponse_10_0_0', 'reponse_10_0_1', 'reponse_10_0_2', 'reponse_10_0_3'],
    nextQuestionMap:['reponse_10_0_0', 'reponse_10_0_1', 'reponse_10_0_2', 'reponse_10_0_3'],
    scoreMap:[1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 10
  {
    id:'reponse_10_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_10_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 10
  {
    id:'reponse_10_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_10_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1011'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 10
  {
    id:'question_10_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_10_1',
    inputType:'radio',
    comment:'',
    options:['reponse_10_1_0', 'reponse_10_1_1'],
    nextQuestionMap:['reponse_10_1_0', 'reponse_10_1_1'],
    scoreMap:[1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 10
  {
    id:'reponse_10_2_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_10_2_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1020'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 10
  {
    id:'reponse_10_2_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_10_2_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1021'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 10
  {
    id:'question_10_2',
    category:CATEGORIES.SYMPTOMS,
    text:'question_10_2',
    inputType:'radio',
    comment:'',
    options:['reponse_10_2_0', 'reponse_10_2_1'],
    nextQuestionMap:['reponse_10_2_0', 'reponse_10_2_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 10
  {
    id:'reponse_10_3_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_10_3_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1030'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 10
  {
    id:'reponse_10_3_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_10_3_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1031'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 10
  {
    id:'reponse_10_3_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_10_3_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1032'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 10
  {
    id:'question_10_3',
    category:CATEGORIES.SYMPTOMS,
    text:'question_10_3',
    inputType:'radio',
    comment:'',
    options:['reponse_10_3_0', 'reponse_10_3_1', 'reponse_10_3_2'],
    nextQuestionMap:['reponse_10_3_0', 'reponse_10_3_1', 'reponse_10_3_2'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1100'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1101'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1102'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1103'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1104'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1105'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1106'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1107'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1108'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1109'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['11010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['11011'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_12',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['11012'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 11
  {
    id:'reponse_11_0_13',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_11_0_13',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['11013'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 11
  {
    id:'question_11_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_11_0',
    inputType:'radio',
    comment:'',
    options:['reponse_11_0_0', 'reponse_11_0_1', 'reponse_11_0_2', 'reponse_11_0_3', 'reponse_11_0_4', 'reponse_11_0_5', 'reponse_11_0_6', 'reponse_11_0_7', 'reponse_11_0_8', 'reponse_11_0_9', 'reponse_11_0_10', 'reponse_11_0_11', 'reponse_11_0_12', 'reponse_11_0_13'],
    nextQuestionMap:['reponse_11_0_0', 'reponse_11_0_1', 'reponse_11_0_2', 'reponse_11_0_3', 'reponse_11_0_4', 'reponse_11_0_5', 'reponse_11_0_6', 'reponse_11_0_7', 'reponse_11_0_8', 'reponse_11_0_9', 'reponse_11_0_10', 'reponse_11_0_11', 'reponse_11_0_12', 'reponse_11_0_13'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 12
  {
    id:'reponse_12_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_12_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1200'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 12
  {
    id:'reponse_12_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_12_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1201'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 12
  {
    id:'reponse_12_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_12_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1202'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 12
  {
    id:'reponse_12_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_12_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1203'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 12
  {
    id:'reponse_12_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_12_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1204'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 12
  {
    id:'reponse_12_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_12_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1205'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 12
  {
    id:'reponse_12_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_12_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1206'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 12
  {
    id:'reponse_12_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_12_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1207'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 12
  {
    id:'reponse_12_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_12_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1208'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 12
  {
    id:'question_12_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_12_0',
    inputType:'radio',
    comment:'comment_12_0',
    options:['reponse_12_0_0', 'reponse_12_0_1', 'reponse_12_0_2', 'reponse_12_0_3', 'reponse_12_0_4', 'reponse_12_0_5', 'reponse_12_0_6', 'reponse_12_0_7', 'reponse_12_0_8'],
    nextQuestionMap:['reponse_12_0_0', 'reponse_12_0_1', 'reponse_12_0_2', 'reponse_12_0_3', 'reponse_12_0_4', 'reponse_12_0_5', 'reponse_12_0_6', 'reponse_12_0_7', 'reponse_12_0_8'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 13
  {
    id:'reponse_13_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_13_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1300'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 13
  {
    id:'reponse_13_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_13_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1301'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 13
  {
    id:'question_13_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_13_0',
    inputType:'radio',
    comment:'',
    options:['reponse_13_0_0', 'reponse_13_0_1'],
    nextQuestionMap:['reponse_13_0_0', 'reponse_13_0_1'],
    scoreMap:[1, 1]
  },
//PARTIE REPONSE SUMMARY 14
  {
    id:'reponse_14_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_14_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1400'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 14
  {
    id:'reponse_14_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_14_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1401'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 14
  {
    id:'reponse_14_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_14_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1402'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 14
  {
    id:'question_14_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_14_0',
    inputType:'radio',
    comment:'comment_14_0',
    options:['reponse_14_0_0', 'reponse_14_0_1', 'reponse_14_0_2'],
    nextQuestionMap:['reponse_14_0_0', 'reponse_14_0_1', 'reponse_14_0_2'],
    scoreMap:[1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1500'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1501'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1502'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1503'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1504'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1505'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1506'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1507'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1508'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1509'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['15010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['15011'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_12',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['15012'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_13',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_13',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['15013'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_14',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_14',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['15014'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_15',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_15',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['15015'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_16',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_16',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['15016'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_17',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_17',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['15017'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_18',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_18',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['15018'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_19',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_19',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['15019'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 15
  {
    id:'reponse_15_0_20',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_15_0_20',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['15020'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 15
  {
    id:'question_15_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_15_0',
    inputType:'radio',
    comment:'',
    options:['reponse_15_0_0', 'reponse_15_0_1', 'reponse_15_0_2', 'reponse_15_0_3', 'reponse_15_0_4', 'reponse_15_0_5', 'reponse_15_0_6', 'reponse_15_0_7', 'reponse_15_0_8', 'reponse_15_0_9', 'reponse_15_0_10', 'reponse_15_0_11', 'reponse_15_0_12', 'reponse_15_0_13', 'reponse_15_0_14', 'reponse_15_0_15', 'reponse_15_0_16', 'reponse_15_0_17', 'reponse_15_0_18', 'reponse_15_0_19', 'reponse_15_0_20'],
    nextQuestionMap:['reponse_15_0_0', 'reponse_15_0_1', 'reponse_15_0_2', 'reponse_15_0_3', 'reponse_15_0_4', 'reponse_15_0_5', 'reponse_15_0_6', 'reponse_15_0_7', 'reponse_15_0_8', 'reponse_15_0_9', 'reponse_15_0_10', 'reponse_15_0_11', 'reponse_15_0_12', 'reponse_15_0_13', 'reponse_15_0_14', 'reponse_15_0_15', 'reponse_15_0_16', 'reponse_15_0_17', 'reponse_15_0_18', 'reponse_15_0_19', 'reponse_15_0_20'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 16
  {
    id:'reponse_16_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_16_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1600'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 16
  {
    id:'reponse_16_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_16_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1601'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 16
  {
    id:'reponse_16_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_16_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1602'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 16
  {
    id:'question_16_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_16_0',
    inputType:'radio',
    comment:'',
    options:['reponse_16_0_0', 'reponse_16_0_1', 'reponse_16_0_2'],
    nextQuestionMap:['reponse_16_0_0', 'reponse_16_0_1', 'reponse_16_0_2'],
    scoreMap:[1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 17
  {
    id:'reponse_17_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_17_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1700'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 17
  {
    id:'reponse_17_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_17_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1701'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 17
  {
    id:'reponse_17_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_17_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1702'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 17
  {
    id:'reponse_17_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_17_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1703'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 17
  {
    id:'reponse_17_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_17_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1704'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 17
  {
    id:'reponse_17_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_17_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1705'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 17
  {
    id:'reponse_17_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_17_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1706'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 17
  {
    id:'question_17_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_17_0',
    inputType:'radio',
    comment:'',
    options:['reponse_17_0_0', 'reponse_17_0_1', 'reponse_17_0_2', 'reponse_17_0_3', 'reponse_17_0_4', 'reponse_17_0_5', 'reponse_17_0_6'],
    nextQuestionMap:['reponse_17_0_0', 'reponse_17_0_1', 'reponse_17_0_2', 'reponse_17_0_3', 'reponse_17_0_4', 'reponse_17_0_5', 'reponse_17_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 18
  {
    id:'reponse_18_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_18_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1800'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 18
  {
    id:'reponse_18_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_18_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1801'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 18
  {
    id:'reponse_18_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_18_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1802'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 18
  {
    id:'reponse_18_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_18_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1803'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 18
  {
    id:'question_18_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_18_0',
    inputType:'radio',
    comment:'',
    options:['reponse_18_0_0', 'reponse_18_0_1', 'reponse_18_0_2', 'reponse_18_0_3'],
    nextQuestionMap:['reponse_18_0_0', 'reponse_18_0_1', 'reponse_18_0_2', 'reponse_18_0_3'],
    scoreMap:[1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 19
  {
    id:'reponse_19_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_19_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1900'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 19
  {
    id:'reponse_19_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_19_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1901'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 19
  {
    id:'reponse_19_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_19_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1902'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 19
  {
    id:'reponse_19_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_19_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1903'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 19
  {
    id:'reponse_19_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_19_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1904'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 19
  {
    id:'reponse_19_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_19_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1905'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 19
  {
    id:'reponse_19_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_19_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1906'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 19
  {
    id:'reponse_19_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_19_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1907'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 19
  {
    id:'reponse_19_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_19_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1908'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 19
  {
    id:'reponse_19_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_19_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1909'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 19
  {
    id:'reponse_19_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_19_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['19010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 19
  {
    id:'reponse_19_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_19_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['19011'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 19
  {
    id:'question_19_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_19_0',
    inputType:'radio',
    comment:'',
    options:['reponse_19_0_0', 'reponse_19_0_1', 'reponse_19_0_2', 'reponse_19_0_3', 'reponse_19_0_4', 'reponse_19_0_5', 'reponse_19_0_6', 'reponse_19_0_7', 'reponse_19_0_8', 'reponse_19_0_9', 'reponse_19_0_10', 'reponse_19_0_11'],
    nextQuestionMap:['reponse_19_0_0', 'reponse_19_0_1', 'reponse_19_0_2', 'reponse_19_0_3', 'reponse_19_0_4', 'reponse_19_0_5', 'reponse_19_0_6', 'reponse_19_0_7', 'reponse_19_0_8', 'reponse_19_0_9', 'reponse_19_0_10', 'reponse_19_0_11'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2000'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2001'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2002'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2003'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2004'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2005'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2006'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2007'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2008'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2009'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['20010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['20011'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_12',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['20012'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_13',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_13',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['20013'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 20
  {
    id:'reponse_20_0_14',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_20_0_14',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['20014'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 20
  {
    id:'question_20_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_20_0',
    inputType:'radio',
    comment:'',
    options:['reponse_20_0_0', 'reponse_20_0_1', 'reponse_20_0_2', 'reponse_20_0_3', 'reponse_20_0_4', 'reponse_20_0_5', 'reponse_20_0_6', 'reponse_20_0_7', 'reponse_20_0_8', 'reponse_20_0_9', 'reponse_20_0_10', 'reponse_20_0_11', 'reponse_20_0_12', 'reponse_20_0_13', 'reponse_20_0_14'],
    nextQuestionMap:['reponse_20_0_0', 'reponse_20_0_1', 'reponse_20_0_2', 'reponse_20_0_3', 'reponse_20_0_4', 'reponse_20_0_5', 'reponse_20_0_6', 'reponse_20_0_7', 'reponse_20_0_8', 'reponse_20_0_9', 'reponse_20_0_10', 'reponse_20_0_11', 'reponse_20_0_12', 'reponse_20_0_13', 'reponse_20_0_14'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 23
  {
    id:'reponse_23_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_23_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2300'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 23
  {
    id:'reponse_23_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_23_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2301'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 23
  {
    id:'reponse_23_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_23_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2302'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 23
  {
    id:'reponse_23_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_23_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2303'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 23
  {
    id:'reponse_23_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_23_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2304'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 23
  {
    id:'reponse_23_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_23_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2305'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 23
  {
    id:'question_23_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_23_0',
    inputType:'radio',
    comment:'',
    options:['reponse_23_0_0', 'reponse_23_0_1', 'reponse_23_0_2', 'reponse_23_0_3', 'reponse_23_0_4', 'reponse_23_0_5'],
    nextQuestionMap:['reponse_23_0_0', 'reponse_23_0_1', 'reponse_23_0_2', 'reponse_23_0_3', 'reponse_23_0_4', 'reponse_23_0_5'],
    scoreMap:[1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2400'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2401'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2402'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2403'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2404'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2405'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2406'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2407'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2408'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 24
  {
    id:'question_24_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_24_0',
    inputType:'radio',
    comment:'',
    options:['reponse_24_0_0', 'reponse_24_0_1', 'reponse_24_0_2', 'reponse_24_0_3', 'reponse_24_0_4', 'reponse_24_0_5', 'reponse_24_0_6', 'reponse_24_0_7', 'reponse_24_0_8'],
    nextQuestionMap:['reponse_24_0_0', 'reponse_24_0_1', 'reponse_24_0_2', 'reponse_24_0_3', 'reponse_24_0_4', 'reponse_24_0_5', 'reponse_24_0_6', 'reponse_24_0_7', 'reponse_24_0_8'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2410'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2411'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_1_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_1_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2412'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_1_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_1_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2413'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_1_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_1_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2414'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_1_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_1_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2415'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_1_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_1_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2416'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_1_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_1_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2417'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 24
  {
    id:'question_24_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_24_1',
    inputType:'radio',
    comment:'',
    options:['reponse_24_1_0', 'reponse_24_1_1', 'reponse_24_1_2', 'reponse_24_1_3', 'reponse_24_1_4', 'reponse_24_1_5', 'reponse_24_1_6', 'reponse_24_1_7'],
    nextQuestionMap:['reponse_24_1_0', 'reponse_24_1_1', 'reponse_24_1_2', 'reponse_24_1_3', 'reponse_24_1_4', 'reponse_24_1_5', 'reponse_24_1_6', 'reponse_24_1_7'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_2_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_2_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2420'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_2_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_2_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2421'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_2_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_2_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2422'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 24
  {
    id:'reponse_24_2_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_24_2_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2423'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 24
  {
    id:'question_24_2',
    category:CATEGORIES.SYMPTOMS,
    text:'question_24_2',
    inputType:'radio',
    comment:'comment_24_2',
    options:['reponse_24_2_0', 'reponse_24_2_1', 'reponse_24_2_2', 'reponse_24_2_3'],
    nextQuestionMap:['reponse_24_2_0', 'reponse_24_2_1', 'reponse_24_2_2', 'reponse_24_2_3'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 25
  {
    id:'reponse_25_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_25_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2500'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 25
  {
    id:'reponse_25_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_25_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2501'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 25
  {
    id:'reponse_25_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_25_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2502'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 25
  {
    id:'reponse_25_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_25_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2503'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 25
  {
    id:'reponse_25_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_25_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2504'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 25
  {
    id:'reponse_25_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_25_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2505'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 25
  {
    id:'reponse_25_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_25_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2506'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 25
  {
    id:'reponse_25_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_25_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2507'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 25
  {
    id:'reponse_25_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_25_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2508'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 25
  {
    id:'reponse_25_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_25_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2509'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 25
  {
    id:'question_25_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_25_0',
    inputType:'radio',
    comment:'',
    options:['reponse_25_0_0', 'reponse_25_0_1', 'reponse_25_0_2', 'reponse_25_0_3', 'reponse_25_0_4', 'reponse_25_0_5', 'reponse_25_0_6', 'reponse_25_0_7', 'reponse_25_0_8', 'reponse_25_0_9'],
    nextQuestionMap:['reponse_25_0_0', 'reponse_25_0_1', 'reponse_25_0_2', 'reponse_25_0_3', 'reponse_25_0_4', 'reponse_25_0_5', 'reponse_25_0_6', 'reponse_25_0_7', 'reponse_25_0_8', 'reponse_25_0_9'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2600'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2601'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2602'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2603'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2604'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2605'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2606'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2607'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2608'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2609'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['26010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['26011'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 26
  {
    id:'reponse_26_0_12',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_26_0_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['26012'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 26
  {
    id:'question_26_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_26_0',
    inputType:'radio',
    comment:'',
    options:['reponse_26_0_0', 'reponse_26_0_1', 'reponse_26_0_2', 'reponse_26_0_3', 'reponse_26_0_4', 'reponse_26_0_5', 'reponse_26_0_6', 'reponse_26_0_7', 'reponse_26_0_8', 'reponse_26_0_9', 'reponse_26_0_10', 'reponse_26_0_11', 'reponse_26_0_12'],
    nextQuestionMap:['reponse_26_0_0', 'reponse_26_0_1', 'reponse_26_0_2', 'reponse_26_0_3', 'reponse_26_0_4', 'reponse_26_0_5', 'reponse_26_0_6', 'reponse_26_0_7', 'reponse_26_0_8', 'reponse_26_0_9', 'reponse_26_0_10', 'reponse_26_0_11', 'reponse_26_0_12'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2700'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2701'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2702'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2703'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2704'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2705'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2706'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2707'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2708'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 27
  {
    id:'question_27_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_27_0',
    inputType:'radio',
    comment:'comment_27_0',
    options:['reponse_27_0_0', 'reponse_27_0_1', 'reponse_27_0_2', 'reponse_27_0_3', 'reponse_27_0_4', 'reponse_27_0_5', 'reponse_27_0_6', 'reponse_27_0_7', 'reponse_27_0_8'],
    nextQuestionMap:['reponse_27_0_0', 'reponse_27_0_1', 'reponse_27_0_2', 'reponse_27_0_3', 'reponse_27_0_4', 'reponse_27_0_5', 'reponse_27_0_6', 'reponse_27_0_7', 'reponse_27_0_8'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2710'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2711'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_1_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_1_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2712'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 27
  {
    id:'reponse_27_1_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_27_1_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2713'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 27
  {
    id:'question_27_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_27_1',
    inputType:'radio',
    comment:'comment_27_0',
    options:['reponse_27_1_0', 'reponse_27_1_1', 'reponse_27_1_2', 'reponse_27_1_3'],
    nextQuestionMap:['reponse_27_1_0', 'reponse_27_1_1', 'reponse_27_1_2', 'reponse_27_1_3'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 28
  {
    id:'reponse_28_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_28_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2800'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 28
  {
    id:'reponse_28_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_28_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2801'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 28
  {
    id:'reponse_28_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_28_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2802'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 28
  {
    id:'reponse_28_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_28_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2803'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 28
  {
    id:'reponse_28_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_28_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2804'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 28
  {
    id:'reponse_28_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_28_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2805'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 28
  {
    id:'reponse_28_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_28_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2806'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 28
  {
    id:'question_28_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_28_0',
    inputType:'radio',
    comment:'comment_28_0',
    options:['reponse_28_0_0', 'reponse_28_0_1', 'reponse_28_0_2', 'reponse_28_0_3', 'reponse_28_0_4', 'reponse_28_0_5', 'reponse_28_0_6'],
    nextQuestionMap:['reponse_28_0_0', 'reponse_28_0_1', 'reponse_28_0_2', 'reponse_28_0_3', 'reponse_28_0_4', 'reponse_28_0_5', 'reponse_28_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2900'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2901'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2902'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2903'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2904'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2905'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2906'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 29
  {
    id:'question_29_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_29_0',
    inputType:'radio',
    comment:'',
    options:['reponse_29_0_0', 'reponse_29_0_1', 'reponse_29_0_2', 'reponse_29_0_3', 'reponse_29_0_4', 'reponse_29_0_5', 'reponse_29_0_6'],
    nextQuestionMap:['reponse_29_0_0', 'reponse_29_0_1', 'reponse_29_0_2', 'reponse_29_0_3', 'reponse_29_0_4', 'reponse_29_0_5', 'reponse_29_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2910'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2911'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_1_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_1_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2912'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_1_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_1_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2913'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_1_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_1_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2914'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 29
  {
    id:'question_29_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_29_1',
    inputType:'radio',
    comment:'',
    options:['reponse_29_1_0', 'reponse_29_1_1', 'reponse_29_1_2', 'reponse_29_1_3', 'reponse_29_1_4'],
    nextQuestionMap:['reponse_29_1_0', 'reponse_29_1_1', 'reponse_29_1_2', 'reponse_29_1_3', 'reponse_29_1_4'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_2_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_2_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2920'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_2_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_2_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2921'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_2_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_2_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2922'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_2_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_2_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2923'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_2_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_2_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2924'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_2_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_2_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2925'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_2_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_2_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2926'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 29
  {
    id:'question_29_2',
    category:CATEGORIES.SYMPTOMS,
    text:'question_29_2',
    inputType:'radio',
    comment:'',
    options:['reponse_29_2_0', 'reponse_29_2_1', 'reponse_29_2_2', 'reponse_29_2_3', 'reponse_29_2_4', 'reponse_29_2_5', 'reponse_29_2_6'],
    nextQuestionMap:['reponse_29_2_0', 'reponse_29_2_1', 'reponse_29_2_2', 'reponse_29_2_3', 'reponse_29_2_4', 'reponse_29_2_5', 'reponse_29_2_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_3_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_3_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2930'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_3_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_3_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2931'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_3_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_3_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2932'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_3_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_3_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2933'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_3_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_3_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2934'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 29
  {
    id:'question_29_3',
    category:CATEGORIES.SYMPTOMS,
    text:'question_29_3',
    inputType:'radio',
    comment:'',
    options:['reponse_29_3_0', 'reponse_29_3_1', 'reponse_29_3_2', 'reponse_29_3_3', 'reponse_29_3_4'],
    nextQuestionMap:['reponse_29_3_0', 'reponse_29_3_1', 'reponse_29_3_2', 'reponse_29_3_3', 'reponse_29_3_4'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_4_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_4_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2940'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_4_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_4_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2941'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_4_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_4_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2942'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 29
  {
    id:'question_29_4',
    category:CATEGORIES.SYMPTOMS,
    text:'question_29_4',
    inputType:'radio',
    comment:'',
    options:['reponse_29_4_0', 'reponse_29_4_1', 'reponse_29_4_2'],
    nextQuestionMap:['reponse_29_4_0', 'reponse_29_4_1', 'reponse_29_4_2'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_5_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_5_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2950'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_5_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_5_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2951'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_5_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_5_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2952'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_5_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_5_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2953'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_5_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_5_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2954'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 29
  {
    id:'question_29_5',
    category:CATEGORIES.SYMPTOMS,
    text:'question_29_5',
    inputType:'radio',
    comment:'',
    options:['reponse_29_5_0', 'reponse_29_5_1', 'reponse_29_5_2', 'reponse_29_5_3', 'reponse_29_5_4'],
    nextQuestionMap:['reponse_29_5_0', 'reponse_29_5_1', 'reponse_29_5_2', 'reponse_29_5_3', 'reponse_29_5_4'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_6_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_6_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2960'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_6_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_6_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2961'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 29
  {
    id:'reponse_29_6_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_29_6_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2962'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 29
  {
    id:'question_29_6',
    category:CATEGORIES.SYMPTOMS,
    text:'question_29_6',
    inputType:'radio',
    comment:'',
    options:['reponse_29_6_0', 'reponse_29_6_1', 'reponse_29_6_2'],
    nextQuestionMap:['reponse_29_6_0', 'reponse_29_6_1', 'reponse_29_6_2'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 30
  {
    id:'reponse_30_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_30_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3000'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 30
  {
    id:'reponse_30_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_30_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3001'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 30
  {
    id:'reponse_30_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_30_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3002'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 30
  {
    id:'reponse_30_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_30_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3003'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 30
  {
    id:'reponse_30_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_30_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3004'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 30
  {
    id:'reponse_30_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_30_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3005'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 30
  {
    id:'reponse_30_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_30_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3006'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 30
  {
    id:'reponse_30_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_30_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3007'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 30
  {
    id:'question_30_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_30_0',
    inputType:'radio',
    comment:'',
    options:['reponse_30_0_0', 'reponse_30_0_1', 'reponse_30_0_2', 'reponse_30_0_3', 'reponse_30_0_4', 'reponse_30_0_5', 'reponse_30_0_6', 'reponse_30_0_7'],
    nextQuestionMap:['reponse_30_0_0', 'reponse_30_0_1', 'reponse_30_0_2', 'reponse_30_0_3', 'reponse_30_0_4', 'reponse_30_0_5', 'reponse_30_0_6', 'reponse_30_0_7'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3100'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3101'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3102'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3103'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3104'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 31
  {
    id:'question_31_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_31_0',
    inputType:'radio',
    comment:'',
    options:['reponse_31_0_0', 'reponse_31_0_1', 'reponse_31_0_2', 'reponse_31_0_3', 'reponse_31_0_4'],
    nextQuestionMap:['reponse_31_0_0', 'reponse_31_0_1', 'reponse_31_0_2', 'reponse_31_0_3', 'reponse_31_0_4'],
    scoreMap:[1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3110'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3111'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3112'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3113'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3114'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3115'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3116'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3117'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3118'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3119'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['31110'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['31111'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_1_12',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_1_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['31112'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 31
  {
    id:'question_31_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_31_1',
    inputType:'radio',
    comment:'',
    options:['reponse_31_1_0', 'reponse_31_1_1', 'reponse_31_1_2', 'reponse_31_1_3', 'reponse_31_1_4', 'reponse_31_1_5', 'reponse_31_1_6', 'reponse_31_1_7', 'reponse_31_1_8', 'reponse_31_1_9', 'reponse_31_1_10', 'reponse_31_1_11', 'reponse_31_1_12'],
    nextQuestionMap:['reponse_31_1_0', 'reponse_31_1_1', 'reponse_31_1_2', 'reponse_31_1_3', 'reponse_31_1_4', 'reponse_31_1_5', 'reponse_31_1_6', 'reponse_31_1_7', 'reponse_31_1_8', 'reponse_31_1_9', 'reponse_31_1_10', 'reponse_31_1_11', 'reponse_31_1_12'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_2_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_2_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3120'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_2_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_2_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3121'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_2_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_2_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3122'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_2_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_2_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3123'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_2_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_2_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3124'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 31
  {
    id:'reponse_31_2_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_31_2_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3125'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 31
  {
    id:'question_31_2',
    category:CATEGORIES.SYMPTOMS,
    text:'question_31_2',
    inputType:'radio',
    comment:'',
    options:['reponse_31_2_0', 'reponse_31_2_1', 'reponse_31_2_2', 'reponse_31_2_3', 'reponse_31_2_4', 'reponse_31_2_5'],
    nextQuestionMap:['reponse_31_2_0', 'reponse_31_2_1', 'reponse_31_2_2', 'reponse_31_2_3', 'reponse_31_2_4', 'reponse_31_2_5'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3200'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3201'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3202'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3203'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3204'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3205'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3206'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3207'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3208'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3209'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['32010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['32011'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 32
  {
    id:'reponse_32_0_12',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_32_0_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['32012'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 32
  {
    id:'question_32_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_32_0',
    inputType:'radio',
    comment:'',
    options:['reponse_32_0_0', 'reponse_32_0_1', 'reponse_32_0_2', 'reponse_32_0_3', 'reponse_32_0_4', 'reponse_32_0_5', 'reponse_32_0_6', 'reponse_32_0_7', 'reponse_32_0_8', 'reponse_32_0_9', 'reponse_32_0_10', 'reponse_32_0_11', 'reponse_32_0_12'],
    nextQuestionMap:['reponse_32_0_0', 'reponse_32_0_1', 'reponse_32_0_2', 'reponse_32_0_3', 'reponse_32_0_4', 'reponse_32_0_5', 'reponse_32_0_6', 'reponse_32_0_7', 'reponse_32_0_8', 'reponse_32_0_9', 'reponse_32_0_10', 'reponse_32_0_11', 'reponse_32_0_12'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 34
  {
    id:'reponse_34_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_34_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3400'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 34
  {
    id:'reponse_34_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_34_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3401'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 34
  {
    id:'reponse_34_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_34_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3402'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 34
  {
    id:'reponse_34_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_34_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3403'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 34
  {
    id:'reponse_34_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_34_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3404'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 34
  {
    id:'reponse_34_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_34_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3405'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 34
  {
    id:'reponse_34_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_34_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3406'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 34
  {
    id:'reponse_34_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_34_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3407'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 34
  {
    id:'reponse_34_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_34_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3408'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 34
  {
    id:'reponse_34_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_34_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3409'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 34
  {
    id:'reponse_34_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_34_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['34010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 34
  {
    id:'reponse_34_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_34_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['34011'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 34
  {
    id:'question_34_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_34_0',
    inputType:'radio',
    comment:'',
    options:['reponse_34_0_0', 'reponse_34_0_1', 'reponse_34_0_2', 'reponse_34_0_3', 'reponse_34_0_4', 'reponse_34_0_5', 'reponse_34_0_6', 'reponse_34_0_7', 'reponse_34_0_8', 'reponse_34_0_9', 'reponse_34_0_10', 'reponse_34_0_11'],
    nextQuestionMap:['reponse_34_0_0', 'reponse_34_0_1', 'reponse_34_0_2', 'reponse_34_0_3', 'reponse_34_0_4', 'reponse_34_0_5', 'reponse_34_0_6', 'reponse_34_0_7', 'reponse_34_0_8', 'reponse_34_0_9', 'reponse_34_0_10', 'reponse_34_0_11'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 35
  {
    id:'reponse_35_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_35_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3500'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 35
  {
    id:'reponse_35_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_35_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3501'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 35
  {
    id:'reponse_35_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_35_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3502'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 35
  {
    id:'question_35_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_35_0',
    inputType:'radio',
    comment:'',
    options:['reponse_35_0_0', 'reponse_35_0_1', 'reponse_35_0_2'],
    nextQuestionMap:['reponse_35_0_0', 'reponse_35_0_1', 'reponse_35_0_2'],
    scoreMap:[1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 36
  {
    id:'reponse_36_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_36_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3600'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 36
  {
    id:'reponse_36_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_36_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3601'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 36
  {
    id:'reponse_36_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_36_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3602'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 36
  {
    id:'reponse_36_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_36_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3603'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 36
  {
    id:'reponse_36_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_36_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3604'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 36
  {
    id:'reponse_36_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_36_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3605'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 36
  {
    id:'reponse_36_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_36_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3606'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 36
  {
    id:'reponse_36_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_36_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3607'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 36
  {
    id:'reponse_36_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_36_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3608'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 36
  {
    id:'question_36_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_36_0',
    inputType:'radio',
    comment:'comment_36_0',
    options:['reponse_36_0_0', 'reponse_36_0_1', 'reponse_36_0_2', 'reponse_36_0_3', 'reponse_36_0_4', 'reponse_36_0_5', 'reponse_36_0_6', 'reponse_36_0_7', 'reponse_36_0_8'],
    nextQuestionMap:['reponse_36_0_0', 'reponse_36_0_1', 'reponse_36_0_2', 'reponse_36_0_3', 'reponse_36_0_4', 'reponse_36_0_5', 'reponse_36_0_6', 'reponse_36_0_7', 'reponse_36_0_8'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3700'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3701'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3702'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3703'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3704'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3705'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3706'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3707'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3708'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3709'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['37010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['37011'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_12',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['37012'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_13',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_13',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['37013'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_14',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_14',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['37014'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_15',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_15',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['37015'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 37
  {
    id:'reponse_37_0_16',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_37_0_16',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['37016'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 37
  {
    id:'question_37_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_37_0',
    inputType:'radio',
    comment:'',
    options:['reponse_37_0_0', 'reponse_37_0_1', 'reponse_37_0_2', 'reponse_37_0_3', 'reponse_37_0_4', 'reponse_37_0_5', 'reponse_37_0_6', 'reponse_37_0_7', 'reponse_37_0_8', 'reponse_37_0_9', 'reponse_37_0_10', 'reponse_37_0_11', 'reponse_37_0_12', 'reponse_37_0_13', 'reponse_37_0_14', 'reponse_37_0_15', 'reponse_37_0_16'],
    nextQuestionMap:['reponse_37_0_0', 'reponse_37_0_1', 'reponse_37_0_2', 'reponse_37_0_3', 'reponse_37_0_4', 'reponse_37_0_5', 'reponse_37_0_6', 'reponse_37_0_7', 'reponse_37_0_8', 'reponse_37_0_9', 'reponse_37_0_10', 'reponse_37_0_11', 'reponse_37_0_12', 'reponse_37_0_13', 'reponse_37_0_14', 'reponse_37_0_15', 'reponse_37_0_16'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 38
  {
    id:'reponse_38_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_38_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3800'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 38
  {
    id:'reponse_38_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_38_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3801'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 38
  {
    id:'reponse_38_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_38_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3802'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 38
  {
    id:'reponse_38_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_38_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3803'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 38
  {
    id:'question_38_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_38_0',
    inputType:'radio',
    comment:'',
    options:['reponse_38_0_0', 'reponse_38_0_1', 'reponse_38_0_2', 'reponse_38_0_3'],
    nextQuestionMap:['reponse_38_0_0', 'reponse_38_0_1', 'reponse_38_0_2', 'reponse_38_0_3'],
    scoreMap:[1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3900'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3901'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3902'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3903'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3904'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3905'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3906'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3907'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3908'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3909'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 39
  {
    id:'question_39_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_39_0',
    inputType:'radio',
    comment:'',
    options:['reponse_39_0_0', 'reponse_39_0_1', 'reponse_39_0_2', 'reponse_39_0_3', 'reponse_39_0_4', 'reponse_39_0_5', 'reponse_39_0_6', 'reponse_39_0_7', 'reponse_39_0_8', 'reponse_39_0_9'],
    nextQuestionMap:['reponse_39_0_0', 'reponse_39_0_1', 'reponse_39_0_2', 'reponse_39_0_3', 'reponse_39_0_4', 'reponse_39_0_5', 'reponse_39_0_6', 'reponse_39_0_7', 'reponse_39_0_8', 'reponse_39_0_9'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3910'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3911'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 39
  {
    id:'reponse_39_1_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_39_1_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3912'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 39
  {
    id:'question_39_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_39_1',
    inputType:'radio',
    comment:'',
    options:['reponse_39_1_0', 'reponse_39_1_1', 'reponse_39_1_2'],
    nextQuestionMap:['reponse_39_1_0', 'reponse_39_1_1', 'reponse_39_1_2'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 40
  {
    id:'reponse_40_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_40_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4000'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 40
  {
    id:'reponse_40_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_40_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4001'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 40
  {
    id:'reponse_40_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_40_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4002'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 40
  {
    id:'reponse_40_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_40_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4003'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 40
  {
    id:'reponse_40_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_40_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4004'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 40
  {
    id:'reponse_40_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_40_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4005'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 40
  {
    id:'question_40_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_40_0',
    inputType:'radio',
    comment:'',
    options:['reponse_40_0_0', 'reponse_40_0_1', 'reponse_40_0_2', 'reponse_40_0_3', 'reponse_40_0_4', 'reponse_40_0_5'],
    nextQuestionMap:['reponse_40_0_0', 'reponse_40_0_1', 'reponse_40_0_2', 'reponse_40_0_3', 'reponse_40_0_4', 'reponse_40_0_5'],
    scoreMap:[1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 42
  {
    id:'reponse_42_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_42_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4200'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 42
  {
    id:'reponse_42_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_42_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4201'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 42
  {
    id:'question_42_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_42_0',
    inputType:'radio',
    comment:'',
    options:['reponse_42_0_0', 'reponse_42_0_1'],
    nextQuestionMap:['reponse_42_0_0', 'reponse_42_0_1'],
    scoreMap:[1, 1]
  },
//PARTIE REPONSE SUMMARY 43
  {
    id:'reponse_43_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_43_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4300'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 43
  {
    id:'reponse_43_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_43_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4301'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 43
  {
    id:'reponse_43_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_43_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4302'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 43
  {
    id:'reponse_43_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_43_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4303'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 43
  {
    id:'reponse_43_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_43_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4304'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 43
  {
    id:'reponse_43_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_43_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4305'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 43
  {
    id:'question_43_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_43_0',
    inputType:'radio',
    comment:'',
    options:['reponse_43_0_0', 'reponse_43_0_1', 'reponse_43_0_2', 'reponse_43_0_3', 'reponse_43_0_4', 'reponse_43_0_5'],
    nextQuestionMap:['reponse_43_0_0', 'reponse_43_0_1', 'reponse_43_0_2', 'reponse_43_0_3', 'reponse_43_0_4', 'reponse_43_0_5'],
    scoreMap:[1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 44
  {
    id:'reponse_44_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_44_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4400'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 44
  {
    id:'reponse_44_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_44_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4401'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 44
  {
    id:'reponse_44_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_44_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4402'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 44
  {
    id:'reponse_44_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_44_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4403'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 44
  {
    id:'reponse_44_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_44_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4404'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 44
  {
    id:'question_44_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_44_0',
    inputType:'radio',
    comment:'',
    options:['reponse_44_0_0', 'reponse_44_0_1', 'reponse_44_0_2', 'reponse_44_0_3', 'reponse_44_0_4'],
    nextQuestionMap:['reponse_44_0_0', 'reponse_44_0_1', 'reponse_44_0_2', 'reponse_44_0_3', 'reponse_44_0_4'],
    scoreMap:[1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 45
  {
    id:'reponse_45_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_45_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4500'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 45
  {
    id:'reponse_45_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_45_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4501'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 45
  {
    id:'reponse_45_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_45_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4502'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 45
  {
    id:'reponse_45_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_45_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4503'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 45
  {
    id:'reponse_45_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_45_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4504'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 45
  {
    id:'reponse_45_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_45_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4505'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 45
  {
    id:'reponse_45_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_45_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4506'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 45
  {
    id:'question_45_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_45_0',
    inputType:'radio',
    comment:'',
    options:['reponse_45_0_0', 'reponse_45_0_1', 'reponse_45_0_2', 'reponse_45_0_3', 'reponse_45_0_4', 'reponse_45_0_5', 'reponse_45_0_6'],
    nextQuestionMap:['reponse_45_0_0', 'reponse_45_0_1', 'reponse_45_0_2', 'reponse_45_0_3', 'reponse_45_0_4', 'reponse_45_0_5', 'reponse_45_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4600'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4601'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 46
  {
    id:'question_46_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_46_0',
    inputType:'radio',
    comment:'',
    options:['reponse_46_0_0', 'reponse_46_0_1'],
    nextQuestionMap:['reponse_46_0_0', 'reponse_46_0_1'],
    scoreMap:[1, 1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4610'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4611'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_1_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_1_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4612'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_1_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_1_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4613'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_1_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_1_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4614'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_1_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_1_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4615'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 46
  {
    id:'question_46_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_46_1',
    inputType:'radio',
    comment:'',
    options:['reponse_46_1_0', 'reponse_46_1_1', 'reponse_46_1_2', 'reponse_46_1_3', 'reponse_46_1_4', 'reponse_46_1_5'],
    nextQuestionMap:['reponse_46_1_0', 'reponse_46_1_1', 'reponse_46_1_2', 'reponse_46_1_3', 'reponse_46_1_4', 'reponse_46_1_5'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_2_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_2_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4620'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 46
  {
    id:'question_46_2',
    category:CATEGORIES.SYMPTOMS,
    text:'question_46_2',
    inputType:'radio',
    comment:'',
    options:['reponse_46_2_0'],
    nextQuestionMap:['reponse_46_2_0'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_3_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_3_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4630'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_3_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_3_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4631'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 46
  {
    id:'question_46_3',
    category:CATEGORIES.SYMPTOMS,
    text:'question_46_3',
    inputType:'radio',
    comment:'',
    options:['reponse_46_3_0', 'reponse_46_3_1'],
    nextQuestionMap:['reponse_46_3_0', 'reponse_46_3_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_4_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_4_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4640'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_4_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_4_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4641'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_4_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_4_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4642'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 46
  {
    id:'question_46_4',
    category:CATEGORIES.SYMPTOMS,
    text:'question_46_4',
    inputType:'radio',
    comment:'',
    options:['reponse_46_4_0', 'reponse_46_4_1', 'reponse_46_4_2'],
    nextQuestionMap:['reponse_46_4_0', 'reponse_46_4_1', 'reponse_46_4_2'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_5_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_5_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4650'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_5_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_5_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4651'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_5_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_5_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4652'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_5_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_5_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4653'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_5_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_5_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4654'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_5_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_5_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4655'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_5_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_5_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4656'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_5_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_5_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4657'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_5_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_5_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4658'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_5_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_5_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4659'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 46
  {
    id:'reponse_46_5_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_46_5_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['46510'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 46
  {
    id:'question_46_5',
    category:CATEGORIES.SYMPTOMS,
    text:'question_46_5',
    inputType:'radio',
    comment:'',
    options:['reponse_46_5_0', 'reponse_46_5_1', 'reponse_46_5_2', 'reponse_46_5_3', 'reponse_46_5_4', 'reponse_46_5_5', 'reponse_46_5_6', 'reponse_46_5_7', 'reponse_46_5_8', 'reponse_46_5_9', 'reponse_46_5_10'],
    nextQuestionMap:['reponse_46_5_0', 'reponse_46_5_1', 'reponse_46_5_2', 'reponse_46_5_3', 'reponse_46_5_4', 'reponse_46_5_5', 'reponse_46_5_6', 'reponse_46_5_7', 'reponse_46_5_8', 'reponse_46_5_9', 'reponse_46_5_10'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 47
  {
    id:'reponse_47_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_47_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4700'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 47
  {
    id:'reponse_47_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_47_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4701'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 47
  {
    id:'reponse_47_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_47_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4702'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 47
  {
    id:'reponse_47_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_47_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4703'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 47
  {
    id:'reponse_47_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_47_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4704'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 47
  {
    id:'reponse_47_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_47_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4705'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 47
  {
    id:'reponse_47_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_47_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4706'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 47
  {
    id:'reponse_47_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_47_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4707'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 47
  {
    id:'reponse_47_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_47_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4708'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 47
  {
    id:'reponse_47_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_47_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4709'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 47
  {
    id:'reponse_47_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_47_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['47010'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 47
  {
    id:'question_47_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_47_0',
    inputType:'radio',
    comment:'',
    options:['reponse_47_0_0', 'reponse_47_0_1', 'reponse_47_0_2', 'reponse_47_0_3', 'reponse_47_0_4', 'reponse_47_0_5', 'reponse_47_0_6', 'reponse_47_0_7', 'reponse_47_0_8', 'reponse_47_0_9', 'reponse_47_0_10'],
    nextQuestionMap:['reponse_47_0_0', 'reponse_47_0_1', 'reponse_47_0_2', 'reponse_47_0_3', 'reponse_47_0_4', 'reponse_47_0_5', 'reponse_47_0_6', 'reponse_47_0_7', 'reponse_47_0_8', 'reponse_47_0_9', 'reponse_47_0_10'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4800'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4801'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4802'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4803'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4804'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4805'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4806'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 48
  {
    id:'question_48_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_48_0',
    inputType:'radio',
    comment:'',
    options:['reponse_48_0_0', 'reponse_48_0_1', 'reponse_48_0_2', 'reponse_48_0_3', 'reponse_48_0_4', 'reponse_48_0_5', 'reponse_48_0_6'],
    nextQuestionMap:['reponse_48_0_0', 'reponse_48_0_1', 'reponse_48_0_2', 'reponse_48_0_3', 'reponse_48_0_4', 'reponse_48_0_5', 'reponse_48_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4810'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4811'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 48
  {
    id:'question_48_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_48_1',
    inputType:'radio',
    comment:'',
    options:['reponse_48_1_0', 'reponse_48_1_1'],
    nextQuestionMap:['reponse_48_1_0', 'reponse_48_1_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_2_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_2_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4820'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_2_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_2_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4821'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_2_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_2_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4822'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 48
  {
    id:'question_48_2',
    category:CATEGORIES.SYMPTOMS,
    text:'question_48_2',
    inputType:'radio',
    comment:'',
    options:['reponse_48_2_0', 'reponse_48_2_1', 'reponse_48_2_2'],
    nextQuestionMap:['reponse_48_2_0', 'reponse_48_2_1', 'reponse_48_2_2'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_3_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_3_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4830'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_3_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_3_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4831'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_3_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_3_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4832'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 48
  {
    id:'question_48_3',
    category:CATEGORIES.SYMPTOMS,
    text:'question_48_3',
    inputType:'radio',
    comment:'',
    options:['reponse_48_3_0', 'reponse_48_3_1', 'reponse_48_3_2'],
    nextQuestionMap:['reponse_48_3_0', 'reponse_48_3_1', 'reponse_48_3_2'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_4_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_4_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4840'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_4_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_4_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4841'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_4_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_4_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4842'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 48
  {
    id:'question_48_4',
    category:CATEGORIES.SYMPTOMS,
    text:'question_48_4',
    inputType:'radio',
    comment:'',
    options:['reponse_48_4_0', 'reponse_48_4_1', 'reponse_48_4_2'],
    nextQuestionMap:['reponse_48_4_0', 'reponse_48_4_1', 'reponse_48_4_2'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_5_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_5_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4850'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_5_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_5_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4851'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_5_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_5_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4852'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 48
  {
    id:'question_48_5',
    category:CATEGORIES.SYMPTOMS,
    text:'question_48_5',
    inputType:'radio',
    comment:'',
    options:['reponse_48_5_0', 'reponse_48_5_1', 'reponse_48_5_2'],
    nextQuestionMap:['reponse_48_5_0', 'reponse_48_5_1', 'reponse_48_5_2'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_6_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_6_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4860'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_6_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_6_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4861'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_6_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_6_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4862'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 48
  {
    id:'question_48_6',
    category:CATEGORIES.SYMPTOMS,
    text:'question_48_6',
    inputType:'radio',
    comment:'',
    options:['reponse_48_6_0', 'reponse_48_6_1', 'reponse_48_6_2'],
    nextQuestionMap:['reponse_48_6_0', 'reponse_48_6_1', 'reponse_48_6_2'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_7_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_7_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4870'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 48
  {
    id:'reponse_48_7_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_48_7_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4871'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 48
  {
    id:'question_48_7',
    category:CATEGORIES.SYMPTOMS,
    text:'question_48_7',
    inputType:'radio',
    comment:'',
    options:['reponse_48_7_0', 'reponse_48_7_1'],
    nextQuestionMap:['reponse_48_7_0', 'reponse_48_7_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4900'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4901'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4902'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4903'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4904'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4905'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4906'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4907'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4908'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['4909'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['49010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['49011'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_12',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['49012'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_13',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_13',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['49013'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_14',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_14',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['49014'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_15',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_15',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['49015'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_16',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_16',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['49016'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_17',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_17',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['49017'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 49
  {
    id:'reponse_49_0_18',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_49_0_18',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['49018'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 49
  {
    id:'question_49_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_49_0',
    inputType:'radio',
    comment:'comment_49_0',
    options:['reponse_49_0_0', 'reponse_49_0_1', 'reponse_49_0_2', 'reponse_49_0_3', 'reponse_49_0_4', 'reponse_49_0_5', 'reponse_49_0_6', 'reponse_49_0_7', 'reponse_49_0_8', 'reponse_49_0_9', 'reponse_49_0_10', 'reponse_49_0_11', 'reponse_49_0_12', 'reponse_49_0_13', 'reponse_49_0_14', 'reponse_49_0_15', 'reponse_49_0_16', 'reponse_49_0_17', 'reponse_49_0_18'],
    nextQuestionMap:['reponse_49_0_0', 'reponse_49_0_1', 'reponse_49_0_2', 'reponse_49_0_3', 'reponse_49_0_4', 'reponse_49_0_5', 'reponse_49_0_6', 'reponse_49_0_7', 'reponse_49_0_8', 'reponse_49_0_9', 'reponse_49_0_10', 'reponse_49_0_11', 'reponse_49_0_12', 'reponse_49_0_13', 'reponse_49_0_14', 'reponse_49_0_15', 'reponse_49_0_16', 'reponse_49_0_17', 'reponse_49_0_18'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 50
  {
    id:'reponse_50_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_50_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5000'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 50
  {
    id:'reponse_50_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_50_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5001'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 50
  {
    id:'reponse_50_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_50_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5002'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 50
  {
    id:'question_50_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_50_0',
    inputType:'radio',
    comment:'',
    options:['reponse_50_0_0', 'reponse_50_0_1', 'reponse_50_0_2'],
    nextQuestionMap:['reponse_50_0_0', 'reponse_50_0_1', 'reponse_50_0_2'],
    scoreMap:[1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5200'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5201'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5202'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5203'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5204'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5205'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5206'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5207'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 52
  {
    id:'question_52_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_52_0',
    inputType:'radio',
    comment:'',
    options:['reponse_52_0_0', 'reponse_52_0_1', 'reponse_52_0_2', 'reponse_52_0_3', 'reponse_52_0_4', 'reponse_52_0_5', 'reponse_52_0_6', 'reponse_52_0_7'],
    nextQuestionMap:['reponse_52_0_0', 'reponse_52_0_1', 'reponse_52_0_2', 'reponse_52_0_3', 'reponse_52_0_4', 'reponse_52_0_5', 'reponse_52_0_6', 'reponse_52_0_7'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5210'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5211'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 52
  {
    id:'question_52_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_52_1',
    inputType:'radio',
    comment:'',
    options:['reponse_52_1_0', 'reponse_52_1_1'],
    nextQuestionMap:['reponse_52_1_0', 'reponse_52_1_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_2_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_2_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5220'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_2_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_2_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5221'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 52
  {
    id:'question_52_2',
    category:CATEGORIES.SYMPTOMS,
    text:'question_52_2',
    inputType:'radio',
    comment:'',
    options:['reponse_52_2_0', 'reponse_52_2_1'],
    nextQuestionMap:['reponse_52_2_0', 'reponse_52_2_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_3_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_3_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5230'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_3_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_3_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5231'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 52
  {
    id:'question_52_3',
    category:CATEGORIES.SYMPTOMS,
    text:'question_52_3',
    inputType:'radio',
    comment:'',
    options:['reponse_52_3_0', 'reponse_52_3_1'],
    nextQuestionMap:['reponse_52_3_0', 'reponse_52_3_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_4_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_4_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5240'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 52
  {
    id:'reponse_52_4_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_52_4_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5241'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 52
  {
    id:'question_52_4',
    category:CATEGORIES.SYMPTOMS,
    text:'question_52_4',
    inputType:'radio',
    comment:'',
    options:['reponse_52_4_0', 'reponse_52_4_1'],
    nextQuestionMap:['reponse_52_4_0', 'reponse_52_4_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 53
  {
    id:'reponse_53_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_53_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5300'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 53
  {
    id:'reponse_53_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_53_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5301'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 53
  {
    id:'reponse_53_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_53_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5302'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 53
  {
    id:'reponse_53_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_53_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5303'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 53
  {
    id:'reponse_53_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_53_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5304'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 53
  {
    id:'reponse_53_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_53_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5305'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 53
  {
    id:'reponse_53_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_53_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5306'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 53
  {
    id:'reponse_53_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_53_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5307'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 53
  {
    id:'reponse_53_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_53_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5308'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 53
  {
    id:'reponse_53_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_53_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5309'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 53
  {
    id:'reponse_53_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_53_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['53010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 53
  {
    id:'reponse_53_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_53_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['53011'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 53
  {
    id:'question_53_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_53_0',
    inputType:'radio',
    comment:'',
    options:['reponse_53_0_0', 'reponse_53_0_1', 'reponse_53_0_2', 'reponse_53_0_3', 'reponse_53_0_4', 'reponse_53_0_5', 'reponse_53_0_6', 'reponse_53_0_7', 'reponse_53_0_8', 'reponse_53_0_9', 'reponse_53_0_10', 'reponse_53_0_11'],
    nextQuestionMap:['reponse_53_0_0', 'reponse_53_0_1', 'reponse_53_0_2', 'reponse_53_0_3', 'reponse_53_0_4', 'reponse_53_0_5', 'reponse_53_0_6', 'reponse_53_0_7', 'reponse_53_0_8', 'reponse_53_0_9', 'reponse_53_0_10', 'reponse_53_0_11'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 54
  {
    id:'reponse_54_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_54_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5400'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 54
  {
    id:'reponse_54_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_54_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5401'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 54
  {
    id:'reponse_54_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_54_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5402'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 54
  {
    id:'reponse_54_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_54_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5403'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 54
  {
    id:'question_54_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_54_0',
    inputType:'radio',
    comment:'',
    options:['reponse_54_0_0', 'reponse_54_0_1', 'reponse_54_0_2', 'reponse_54_0_3'],
    nextQuestionMap:['reponse_54_0_0', 'reponse_54_0_1', 'reponse_54_0_2', 'reponse_54_0_3'],
    scoreMap:[1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 54
  {
    id:'reponse_54_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_54_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5410'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 54
  {
    id:'reponse_54_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_54_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5411'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 54
  {
    id:'question_54_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_54_1',
    inputType:'radio',
    comment:'',
    options:['reponse_54_1_0', 'reponse_54_1_1'],
    nextQuestionMap:['reponse_54_1_0', 'reponse_54_1_1'],
    scoreMap:[1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 54
  {
    id:'reponse_54_2_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_54_2_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5420'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 54
  {
    id:'reponse_54_2_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_54_2_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5421'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 54
  {
    id:'reponse_54_2_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_54_2_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5422'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 54
  {
    id:'question_54_2',
    category:CATEGORIES.SYMPTOMS,
    text:'question_54_2',
    inputType:'radio',
    comment:'',
    options:['reponse_54_2_0', 'reponse_54_2_1', 'reponse_54_2_2'],
    nextQuestionMap:['reponse_54_2_0', 'reponse_54_2_1', 'reponse_54_2_2'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5500'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5501'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5502'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5503'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5504'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5505'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5506'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5507'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5508'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5509'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 55
  {
    id:'question_55_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_55_0',
    inputType:'radio',
    comment:'',
    options:['reponse_55_0_0', 'reponse_55_0_1', 'reponse_55_0_2', 'reponse_55_0_3', 'reponse_55_0_4', 'reponse_55_0_5', 'reponse_55_0_6', 'reponse_55_0_7', 'reponse_55_0_8', 'reponse_55_0_9'],
    nextQuestionMap:['reponse_55_0_0', 'reponse_55_0_1', 'reponse_55_0_2', 'reponse_55_0_3', 'reponse_55_0_4', 'reponse_55_0_5', 'reponse_55_0_6', 'reponse_55_0_7', 'reponse_55_0_8', 'reponse_55_0_9'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5510'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5511'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_1_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_1_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5512'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_1_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_1_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5513'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 55
  {
    id:'reponse_55_1_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_55_1_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5514'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 55
  {
    id:'question_55_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_55_1',
    inputType:'radio',
    comment:'',
    options:['reponse_55_1_0', 'reponse_55_1_1', 'reponse_55_1_2', 'reponse_55_1_3', 'reponse_55_1_4'],
    nextQuestionMap:['reponse_55_1_0', 'reponse_55_1_1', 'reponse_55_1_2', 'reponse_55_1_3', 'reponse_55_1_4'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 56
  {
    id:'reponse_56_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_56_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5600'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 56
  {
    id:'reponse_56_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_56_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5601'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 56
  {
    id:'reponse_56_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_56_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5602'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 56
  {
    id:'reponse_56_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_56_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5603'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 56
  {
    id:'reponse_56_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_56_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5604'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 56
  {
    id:'reponse_56_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_56_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5605'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 56
  {
    id:'reponse_56_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_56_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5606'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 56
  {
    id:'reponse_56_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_56_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5607'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 56
  {
    id:'reponse_56_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_56_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5608'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 56
  {
    id:'reponse_56_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_56_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5609'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 56
  {
    id:'question_56_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_56_0',
    inputType:'radio',
    comment:'',
    options:['reponse_56_0_0', 'reponse_56_0_1', 'reponse_56_0_2', 'reponse_56_0_3', 'reponse_56_0_4', 'reponse_56_0_5', 'reponse_56_0_6', 'reponse_56_0_7', 'reponse_56_0_8', 'reponse_56_0_9'],
    nextQuestionMap:['reponse_56_0_0', 'reponse_56_0_1', 'reponse_56_0_2', 'reponse_56_0_3', 'reponse_56_0_4', 'reponse_56_0_5', 'reponse_56_0_6', 'reponse_56_0_7', 'reponse_56_0_8', 'reponse_56_0_9'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 56
  {
    id:'reponse_56_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_56_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5610'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 56
  {
    id:'reponse_56_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_56_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5611'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 56
  {
    id:'question_56_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_56_1',
    inputType:'radio',
    comment:'',
    options:['reponse_56_1_0', 'reponse_56_1_1'],
    nextQuestionMap:['reponse_56_1_0', 'reponse_56_1_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 58
  {
    id:'reponse_58_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_58_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5800'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 58
  {
    id:'reponse_58_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_58_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5801'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 58
  {
    id:'reponse_58_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_58_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5802'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 58
  {
    id:'reponse_58_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_58_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5803'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 58
  {
    id:'reponse_58_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_58_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5804'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 58
  {
    id:'reponse_58_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_58_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5805'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 58
  {
    id:'reponse_58_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_58_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['5806'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 58
  {
    id:'question_58_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_58_0',
    inputType:'radio',
    comment:'comment_58_0',
    options:['reponse_58_0_0', 'reponse_58_0_1', 'reponse_58_0_2', 'reponse_58_0_3', 'reponse_58_0_4', 'reponse_58_0_5', 'reponse_58_0_6'],
    nextQuestionMap:['reponse_58_0_0', 'reponse_58_0_1', 'reponse_58_0_2', 'reponse_58_0_3', 'reponse_58_0_4', 'reponse_58_0_5', 'reponse_58_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 60
  {
    id:'reponse_60_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_60_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6000'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 60
  {
    id:'reponse_60_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_60_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6001'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 60
  {
    id:'reponse_60_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_60_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6002'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 60
  {
    id:'reponse_60_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_60_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6003'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 60
  {
    id:'reponse_60_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_60_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6004'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 60
  {
    id:'reponse_60_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_60_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6005'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 60
  {
    id:'question_60_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_60_0',
    inputType:'radio',
    comment:'',
    options:['reponse_60_0_0', 'reponse_60_0_1', 'reponse_60_0_2', 'reponse_60_0_3', 'reponse_60_0_4', 'reponse_60_0_5'],
    nextQuestionMap:['reponse_60_0_0', 'reponse_60_0_1', 'reponse_60_0_2', 'reponse_60_0_3', 'reponse_60_0_4', 'reponse_60_0_5'],
    scoreMap:[1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 61
  {
    id:'reponse_61_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_61_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6100'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 61
  {
    id:'reponse_61_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_61_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6101'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 61
  {
    id:'reponse_61_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_61_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6102'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 61
  {
    id:'reponse_61_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_61_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6103'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 61
  {
    id:'reponse_61_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_61_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6104'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 61
  {
    id:'reponse_61_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_61_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6105'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 61
  {
    id:'reponse_61_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_61_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6106'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 61
  {
    id:'reponse_61_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_61_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6107'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 61
  {
    id:'question_61_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_61_0',
    inputType:'radio',
    comment:'',
    options:['reponse_61_0_0', 'reponse_61_0_1', 'reponse_61_0_2', 'reponse_61_0_3', 'reponse_61_0_4', 'reponse_61_0_5', 'reponse_61_0_6', 'reponse_61_0_7'],
    nextQuestionMap:['reponse_61_0_0', 'reponse_61_0_1', 'reponse_61_0_2', 'reponse_61_0_3', 'reponse_61_0_4', 'reponse_61_0_5', 'reponse_61_0_6', 'reponse_61_0_7'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 63
  {
    id:'reponse_63_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_63_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6300'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 63
  {
    id:'reponse_63_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_63_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6301'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 63
  {
    id:'reponse_63_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_63_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6302'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 63
  {
    id:'question_63_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_63_0',
    inputType:'radio',
    comment:'',
    options:['reponse_63_0_0', 'reponse_63_0_1', 'reponse_63_0_2'],
    nextQuestionMap:['reponse_63_0_0', 'reponse_63_0_1', 'reponse_63_0_2'],
    scoreMap:[1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 63
  {
    id:'reponse_63_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_63_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6310'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 63
  {
    id:'reponse_63_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_63_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6311'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 63
  {
    id:'reponse_63_1_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_63_1_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6312'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 63
  {
    id:'reponse_63_1_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_63_1_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6313'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 63
  {
    id:'reponse_63_1_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_63_1_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6314'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 63
  {
    id:'reponse_63_1_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_63_1_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6315'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 63
  {
    id:'question_63_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_63_1',
    inputType:'radio',
    comment:'',
    options:['reponse_63_1_0', 'reponse_63_1_1', 'reponse_63_1_2', 'reponse_63_1_3', 'reponse_63_1_4', 'reponse_63_1_5'],
    nextQuestionMap:['reponse_63_1_0', 'reponse_63_1_1', 'reponse_63_1_2', 'reponse_63_1_3', 'reponse_63_1_4', 'reponse_63_1_5'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 64
  {
    id:'reponse_64_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_64_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6400'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 64
  {
    id:'reponse_64_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_64_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6401'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 64
  {
    id:'reponse_64_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_64_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6402'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 64
  {
    id:'reponse_64_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_64_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6403'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 64
  {
    id:'reponse_64_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_64_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6404'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 64
  {
    id:'reponse_64_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_64_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6405'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 64
  {
    id:'question_64_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_64_0',
    inputType:'radio',
    comment:'',
    options:['reponse_64_0_0', 'reponse_64_0_1', 'reponse_64_0_2', 'reponse_64_0_3', 'reponse_64_0_4', 'reponse_64_0_5'],
    nextQuestionMap:['reponse_64_0_0', 'reponse_64_0_1', 'reponse_64_0_2', 'reponse_64_0_3', 'reponse_64_0_4', 'reponse_64_0_5'],
    scoreMap:[1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 64
  {
    id:'reponse_64_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_64_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6410'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 64
  {
    id:'reponse_64_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_64_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6411'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 64
  {
    id:'question_64_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_64_1',
    inputType:'radio',
    comment:'',
    options:['reponse_64_1_0', 'reponse_64_1_1'],
    nextQuestionMap:['reponse_64_1_0', 'reponse_64_1_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 65
  {
    id:'reponse_65_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_65_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6500'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 65
  {
    id:'reponse_65_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_65_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6501'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 65
  {
    id:'reponse_65_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_65_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6502'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 65
  {
    id:'reponse_65_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_65_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6503'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 65
  {
    id:'reponse_65_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_65_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6504'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 65
  {
    id:'reponse_65_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_65_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6505'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 65
  {
    id:'reponse_65_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_65_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6506'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 65
  {
    id:'question_65_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_65_0',
    inputType:'radio',
    comment:'',
    options:['reponse_65_0_0', 'reponse_65_0_1', 'reponse_65_0_2', 'reponse_65_0_3', 'reponse_65_0_4', 'reponse_65_0_5', 'reponse_65_0_6'],
    nextQuestionMap:['reponse_65_0_0', 'reponse_65_0_1', 'reponse_65_0_2', 'reponse_65_0_3', 'reponse_65_0_4', 'reponse_65_0_5', 'reponse_65_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 65
  {
    id:'reponse_65_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_65_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6510'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 65
  {
    id:'reponse_65_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_65_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6511'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 65
  {
    id:'reponse_65_1_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_65_1_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6512'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 65
  {
    id:'reponse_65_1_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_65_1_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6513'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 65
  {
    id:'question_65_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_65_1',
    inputType:'radio',
    comment:'',
    options:['reponse_65_1_0', 'reponse_65_1_1', 'reponse_65_1_2', 'reponse_65_1_3'],
    nextQuestionMap:['reponse_65_1_0', 'reponse_65_1_1', 'reponse_65_1_2', 'reponse_65_1_3'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6600'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6601'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6602'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6603'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6604'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6605'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6606'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6607'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6608'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6609'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['66010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_11',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['66011'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_12',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['66012'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_13',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_13',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['66013'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_14',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_14',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['66014'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_0_15',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_0_15',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['66015'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 66
  {
    id:'question_66_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_66_0',
    inputType:'radio',
    comment:'',
    options:['reponse_66_0_0', 'reponse_66_0_1', 'reponse_66_0_2', 'reponse_66_0_3', 'reponse_66_0_4', 'reponse_66_0_5', 'reponse_66_0_6', 'reponse_66_0_7', 'reponse_66_0_8', 'reponse_66_0_9', 'reponse_66_0_10', 'reponse_66_0_11', 'reponse_66_0_12', 'reponse_66_0_13', 'reponse_66_0_14', 'reponse_66_0_15'],
    nextQuestionMap:['reponse_66_0_0', 'reponse_66_0_1', 'reponse_66_0_2', 'reponse_66_0_3', 'reponse_66_0_4', 'reponse_66_0_5', 'reponse_66_0_6', 'reponse_66_0_7', 'reponse_66_0_8', 'reponse_66_0_9', 'reponse_66_0_10', 'reponse_66_0_11', 'reponse_66_0_12', 'reponse_66_0_13', 'reponse_66_0_14', 'reponse_66_0_15'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6610'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6611'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_1_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_1_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6612'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 66
  {
    id:'reponse_66_1_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_66_1_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6613'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 66
  {
    id:'question_66_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_66_1',
    inputType:'radio',
    comment:'',
    options:['reponse_66_1_0', 'reponse_66_1_1', 'reponse_66_1_2', 'reponse_66_1_3'],
    nextQuestionMap:['reponse_66_1_0', 'reponse_66_1_1', 'reponse_66_1_2', 'reponse_66_1_3'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 67
  {
    id:'reponse_67_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_67_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6700'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 67
  {
    id:'reponse_67_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_67_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6701'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 67
  {
    id:'reponse_67_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_67_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6702'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 67
  {
    id:'reponse_67_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_67_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6703'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 67
  {
    id:'reponse_67_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_67_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6704'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 67
  {
    id:'reponse_67_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_67_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6705'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 67
  {
    id:'reponse_67_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_67_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6706'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 67
  {
    id:'question_67_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_67_0',
    inputType:'radio',
    comment:'',
    options:['reponse_67_0_0', 'reponse_67_0_1', 'reponse_67_0_2', 'reponse_67_0_3', 'reponse_67_0_4', 'reponse_67_0_5', 'reponse_67_0_6'],
    nextQuestionMap:['reponse_67_0_0', 'reponse_67_0_1', 'reponse_67_0_2', 'reponse_67_0_3', 'reponse_67_0_4', 'reponse_67_0_5', 'reponse_67_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 68
  {
    id:'reponse_68_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_68_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6800'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 68
  {
    id:'reponse_68_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_68_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6801'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 68
  {
    id:'reponse_68_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_68_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6802'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 68
  {
    id:'reponse_68_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_68_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6803'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 68
  {
    id:'reponse_68_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_68_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6804'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 68
  {
    id:'reponse_68_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_68_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6805'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 68
  {
    id:'reponse_68_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_68_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6806'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 68
  {
    id:'question_68_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_68_0',
    inputType:'radio',
    comment:'',
    options:['reponse_68_0_0', 'reponse_68_0_1', 'reponse_68_0_2', 'reponse_68_0_3', 'reponse_68_0_4', 'reponse_68_0_5', 'reponse_68_0_6'],
    nextQuestionMap:['reponse_68_0_0', 'reponse_68_0_1', 'reponse_68_0_2', 'reponse_68_0_3', 'reponse_68_0_4', 'reponse_68_0_5', 'reponse_68_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6900'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6901'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6902'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6903'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6904'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6905'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6906'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_0_7',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_0_7',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6907'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_0_8',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_0_8',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6908'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_0_9',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_0_9',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6909'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_0_10',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_0_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['69010'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 69
  {
    id:'question_69_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_69_0',
    inputType:'radio',
    comment:'',
    options:['reponse_69_0_0', 'reponse_69_0_1', 'reponse_69_0_2', 'reponse_69_0_3', 'reponse_69_0_4', 'reponse_69_0_5', 'reponse_69_0_6', 'reponse_69_0_7', 'reponse_69_0_8', 'reponse_69_0_9', 'reponse_69_0_10'],
    nextQuestionMap:['reponse_69_0_0', 'reponse_69_0_1', 'reponse_69_0_2', 'reponse_69_0_3', 'reponse_69_0_4', 'reponse_69_0_5', 'reponse_69_0_6', 'reponse_69_0_7', 'reponse_69_0_8', 'reponse_69_0_9', 'reponse_69_0_10'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6910'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 69
  {
    id:'reponse_69_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_69_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['6911'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 69
  {
    id:'question_69_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_69_1',
    inputType:'radio',
    comment:'comment_69_1',
    options:['reponse_69_1_0', 'reponse_69_1_1'],
    nextQuestionMap:['reponse_69_1_0', 'reponse_69_1_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 70
  {
    id:'reponse_70_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_70_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7000'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 70
  {
    id:'reponse_70_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_70_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7001'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 70
  {
    id:'reponse_70_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_70_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7002'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 70
  {
    id:'question_70_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_70_0',
    inputType:'radio',
    comment:'',
    options:['reponse_70_0_0', 'reponse_70_0_1', 'reponse_70_0_2'],
    nextQuestionMap:['reponse_70_0_0', 'reponse_70_0_1', 'reponse_70_0_2'],
    scoreMap:[1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 70
  {
    id:'reponse_70_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_70_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7010'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 70
  {
    id:'reponse_70_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_70_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7011'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 70
  {
    id:'reponse_70_1_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_70_1_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7012'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 70
  {
    id:'reponse_70_1_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_70_1_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7013'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 70
  {
    id:'reponse_70_1_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_70_1_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7014'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 70
  {
    id:'question_70_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_70_1',
    inputType:'radio',
    comment:'',
    options:['reponse_70_1_0', 'reponse_70_1_1', 'reponse_70_1_2', 'reponse_70_1_3', 'reponse_70_1_4'],
    nextQuestionMap:['reponse_70_1_0', 'reponse_70_1_1', 'reponse_70_1_2', 'reponse_70_1_3', 'reponse_70_1_4'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7100'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 71
  {
    id:'question_71_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_71_0',
    inputType:'radio',
    comment:'',
    options:['reponse_71_0_0'],
    nextQuestionMap:['reponse_71_0_0'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_1_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_1_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7110'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_1_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_1_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7111'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_1_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_1_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7112'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_1_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_1_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7113'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_1_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_1_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7114'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 71
  {
    id:'question_71_1',
    category:CATEGORIES.SYMPTOMS,
    text:'question_71_1',
    inputType:'radio',
    comment:'',
    options:['reponse_71_1_0', 'reponse_71_1_1', 'reponse_71_1_2', 'reponse_71_1_3', 'reponse_71_1_4'],
    nextQuestionMap:['reponse_71_1_0', 'reponse_71_1_1', 'reponse_71_1_2', 'reponse_71_1_3', 'reponse_71_1_4'],
    scoreMap:[1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_2_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_2_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7120'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_2_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_2_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7121'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 71
  {
    id:'question_71_2',
    category:CATEGORIES.SYMPTOMS,
    text:'question_71_2',
    inputType:'radio',
    comment:'',
    options:['reponse_71_2_0', 'reponse_71_2_1'],
    nextQuestionMap:['reponse_71_2_0', 'reponse_71_2_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_3_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_3_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7130'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_3_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_3_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7131'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 71
  {
    id:'question_71_3',
    category:CATEGORIES.SYMPTOMS,
    text:'question_71_3',
    inputType:'radio',
    comment:'',
    options:['reponse_71_3_0', 'reponse_71_3_1'],
    nextQuestionMap:['reponse_71_3_0', 'reponse_71_3_1'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_4_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_4_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7140'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_4_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_4_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7141'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 71
  {
    id:'reponse_71_4_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_71_4_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7142'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 71
  {
    id:'question_71_4',
    category:CATEGORIES.SYMPTOMS,
    text:'question_71_4',
    inputType:'radio',
    comment:'',
    options:['reponse_71_4_0', 'reponse_71_4_1', 'reponse_71_4_2'],
    nextQuestionMap:['reponse_71_4_0', 'reponse_71_4_1', 'reponse_71_4_2'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 72
  {
    id:'reponse_72_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_72_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7200'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 72
  {
    id:'reponse_72_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_72_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7201'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 72
  {
    id:'reponse_72_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_72_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7202'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 72
  {
    id:'question_72_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_72_0',
    inputType:'radio',
    comment:'',
    options:['reponse_72_0_0', 'reponse_72_0_1', 'reponse_72_0_2'],
    nextQuestionMap:['reponse_72_0_0', 'reponse_72_0_1', 'reponse_72_0_2'],
    scoreMap:[1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 73
  {
    id:'reponse_73_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_73_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7300'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 73
  {
    id:'reponse_73_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_73_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7301'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 73
  {
    id:'reponse_73_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_73_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7302'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 73
  {
    id:'reponse_73_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_73_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7303'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 73
  {
    id:'reponse_73_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_73_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7304'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 73
  {
    id:'reponse_73_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_73_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7305'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 73
  {
    id:'question_73_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_73_0',
    inputType:'radio',
    comment:'',
    options:['reponse_73_0_0', 'reponse_73_0_1', 'reponse_73_0_2', 'reponse_73_0_3', 'reponse_73_0_4', 'reponse_73_0_5'],
    nextQuestionMap:['reponse_73_0_0', 'reponse_73_0_1', 'reponse_73_0_2', 'reponse_73_0_3', 'reponse_73_0_4', 'reponse_73_0_5'],
    scoreMap:[1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 74
  {
    id:'reponse_74_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_74_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7400'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 74
  {
    id:'reponse_74_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_74_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7401'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 74
  {
    id:'reponse_74_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_74_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7402'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 74
  {
    id:'reponse_74_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_74_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7403'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 74
  {
    id:'reponse_74_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_74_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7404'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 74
  {
    id:'reponse_74_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_74_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7405'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 74
  {
    id:'question_74_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_74_0',
    inputType:'radio',
    comment:'',
    options:['reponse_74_0_0', 'reponse_74_0_1', 'reponse_74_0_2', 'reponse_74_0_3', 'reponse_74_0_4', 'reponse_74_0_5'],
    nextQuestionMap:['reponse_74_0_0', 'reponse_74_0_1', 'reponse_74_0_2', 'reponse_74_0_3', 'reponse_74_0_4', 'reponse_74_0_5'],
    scoreMap:[1, 1, 1, 1, 1, 1]
  },
//PARTIE REPONSE SUMMARY 75
  {
    id:'reponse_75_0_0',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_75_0_0',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7500'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 75
  {
    id:'reponse_75_0_1',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_75_0_1',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7501'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 75
  {
    id:'reponse_75_0_2',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_75_0_2',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7502'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 75
  {
    id:'reponse_75_0_3',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_75_0_3',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7503'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 75
  {
    id:'reponse_75_0_4',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_75_0_4',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7504'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 75
  {
    id:'reponse_75_0_5',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_75_0_5',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7505'],
    scoreMap:[1]
  },
//PARTIE REPONSE SUMMARY 75
  {
    id:'reponse_75_0_6',
    category:CATEGORIES.SYMPTOMS,
    text:'reponse_75_0_6',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['7506'],
    scoreMap:[1]
  },
//PARTIE DECOUPE QUESTION SUMMARY 75
  {
    id:'question_75_0',
    category:CATEGORIES.SYMPTOMS,
    text:'question_75_0',
    inputType:'radio',
    comment:'',
    options:['reponse_75_0_0', 'reponse_75_0_1', 'reponse_75_0_2', 'reponse_75_0_3', 'reponse_75_0_4', 'reponse_75_0_5', 'reponse_75_0_6'],
    nextQuestionMap:['reponse_75_0_0', 'reponse_75_0_1', 'reponse_75_0_2', 'reponse_75_0_3', 'reponse_75_0_4', 'reponse_75_0_5', 'reponse_75_0_6'],
    scoreMap:[1, 1, 1, 1, 1, 1, 1]
  },{
    id:'introduction_1',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_1',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_2',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_2',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_3',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_3',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_4',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_4',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_5',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_5',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_6',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_6',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_7',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_7',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_8',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_8',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_9',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_9',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_10',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_10',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_11',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_11',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_12',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_12',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_13',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_13',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_14',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_14',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_15',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_15',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_16',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_16',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_17',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_17',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_18',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_18',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_19',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_19',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_20',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_20',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_21',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_21',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_22',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_22',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_23',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_23',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_24',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_24',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_25',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_25',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_26',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_26',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_27',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_27',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_28',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_28',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_29',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_29',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_30',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_30',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_31',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_31',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_32',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_32',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_33',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_33',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_34',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_34',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_35',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_35',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_36',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_36',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_37',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_37',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_38',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_38',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_39',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_39',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_40',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_40',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_41',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_41',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_42',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_42',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_43',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_43',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_44',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_44',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_45',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_45',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_46',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_46',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_47',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_47',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_48',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_48',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_49',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_49',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_50',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_50',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_51',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_51',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_52',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_52',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_53',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_53',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_54',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_54',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_55',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_55',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_56',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_56',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_57',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_57',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_58',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_58',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_59',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_59',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_60',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_60',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_61',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_61',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_62',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_62',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_63',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_63',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_64',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_64',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_65',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_65',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_66',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_66',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_67',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_67',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_68',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_68',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_69',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_69',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_70',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_70',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_71',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_71',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_72',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_72',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_73',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_73',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_74',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_74',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'introduction_75',
    category:CATEGORIES.SYMPTOMS,
    text:'introduction_75',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_1',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_1',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_2',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_2',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_3',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_3',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_4',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_4',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_5',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_5',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_6',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_6',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_7',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_7',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_8',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_8',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_9',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_9',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_10',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_10',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_11',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_11',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_12',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_12',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_13',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_13',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_14',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_14',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_15',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_15',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_16',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_16',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_17',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_17',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_18',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_18',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_19',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_19',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_20',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_20',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_21',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_21',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_22',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_22',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_23',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_23',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_24',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_24',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_25',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_25',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_26',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_26',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_27',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_27',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_28',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_28',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_29',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_29',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_30',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_30',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_31',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_31',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_32',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_32',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_33',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_33',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_34',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_34',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_35',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_35',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_36',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_36',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_37',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_37',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_38',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_38',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_39',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_39',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_40',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_40',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_41',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_41',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_42',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_42',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_43',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_43',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_44',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_44',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_45',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_45',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_46',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_46',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_47',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_47',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_48',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_48',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_49',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_49',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_50',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_50',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_51',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_51',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_52',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_52',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_53',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_53',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_54',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_54',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_55',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_55',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_56',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_56',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_57',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_57',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_58',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_58',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_59',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_59',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_60',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_60',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_61',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_61',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_62',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_62',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_63',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_63',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_64',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_64',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_65',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_65',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_66',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_66',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_67',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_67',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_68',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_68',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_69',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_69',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_70',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_70',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_71',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_71',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_72',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_72',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_73',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_73',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_74',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_74',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },{
    id:'your_doctor_visit_75',
    category:CATEGORIES.SYMPTOMS,
    text:'your_doctor_visit_75',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[]
  },
]