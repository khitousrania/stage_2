Overeating

What it feels like: believing that you eat more than you should.

The amount of calories you need every day depends on your gender
and size. Normal intake for moderately active men ranges from 2200
to 2800 calories per day, and for women ranges from 1800 to 2100
calories.



YOUR DOCTOR VISIT
What your doctor will ask you about: anxiety, depression, eating to
relieve stress, changes in weight, excessive urination, excessive thirst,
ability to tolerate heat, weakness.

Your doctor will want to know if you or anyone in your family
has had any of these conditions: diabetes, thyroid disease, emo-
tional problems, obesity, recent cessation of smoking.

Your doctor will want to know why you think you eat too much,
how much food you typically eat every day, and whether you
engage in eating “binges.”

Your doctor will want to know if you’re taking any of these med-
ications: antidepressants, antipsychotics, lithium.

Your doctor will do a physical examination including the fol-
lowing: weight, height, eye exam, thorough neck exam.
ASK THE FOLLOWING QUESTION
WHAT CAN CAUSE OVEREATING, AND WHAT IS TYPICAL FOR EACH CAUSE?##

Smoking   cessation  #Recently giving up a  smoking habit # Overeating  
@  Hyper-  thyroidism   #Overactivity of the thyroid gland   # Weight loss, hot flashes,  sweating, sometimes a  swollen gland in the neck 
@  Diabetes #An inability to properly process sugar  #Frequent drinking and urination, fatigue, some- times double vision or weight loss 
@  Medication use  #Overeating as a result of  taking antidepressants,  antipsychotics, or lithium #Overeating because of a  frequent feeling of hunger  
@  Bulimia  nervosa  # Engaging in cycles of   “binging” and “purging,” in which you overeat and then starve yourself, vomit, or take laxatives # Binge eating followed by vomiting or taking laxatives  
@  Parasitic  infection of the   intestines #Infection by tapeworm  and other parasites  #   Overeating because of a   frequent feeling of hunger  

