Groin Pain
What it feels like: pain in the lower abdomen, sometimes a lump or
swelling, which changes on movement.



YOUR DOCTOR VISIT
What your doctor will ask you about: recent strenuous exercise,
scrotal mass, change in bowel habits, abnormal pain or distention,
need to strain to move bowels or urinate, recent onset of cough or
change in chronic cough. The doctor may also ask about your sexual
history, depending on the symptoms.

Your doctor will want to know if your pain is always present, or
if it appears only in certain circumstances.

Your doctor will want to know if you’re taking any medications.

Your doctor will do a physical examination including the fol-
lowing: thorough examination of the testes (in men) and groin,
checking stool for the presence of blood, digital rectal exam, thor-
ough tests of your nerves and movement. Depending on the symp-
toms, he or she may also test for infections.

If you are older than 50, your doctor may check to determine if you
have conditions that can increase pressure within the abdomen,
causing hernia. These can include prostate disease (in men) and gas-
trointestinal problems.
ASK THE FOLLOWING QUESTION
WHAT CAN CAUSE GROIN PAIN,AND WHAT ARE TYPICAL ASSOCIATED SYMPTOMS?##

Groin muscle injury # Pull or tear of groin muscles #Pain and soreness in the lower abdomen and groin area, relieved by anti- inflammatory medications such as ibuprofen, and usually disappearing after a week to several weeks 
@ Hernia # The presence of a loop of intestine outside the abdominal wall (but still inside the skin) # Bulge in the groin area that is bigger when standing and smaller when lying down, and can usually be pushed back into the abdomen with a finger. 
@ Infection #Can include sexually transmitted diseases (See chapter on Sexually Transmitted Diseases.) #Swollen lymph nodes, other painful symptoms of sexually transmitted diseases 

