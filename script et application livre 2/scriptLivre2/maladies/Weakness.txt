Weakness

What it feels like: tiredness, weakness, or giddiness, sometimes more
pronounced while standing, which can be the result of an underlying
condition or disease, such as anemia (see below) or an infection.

What can make it worse: exertion, certain medications, and stress
related to family, job, or school problems.

In adults, a very common cause of weakness or tiredness is depres-
sion, manifested as trouble waking up, the feeling that small tasks are
large obstacles, emotional instability, and difficulty sleeping or con-
centrating. If you think you may have depression, see the chapter on
Depression, Suicidal Thoughts, or Anxiety for more details. Other
associated symptoms mentioned here — such as swollen lymph nodes
— may prompt your doctor to look into other causes such as infection
or cancer. See the chapter on Swelling for more information.



YOUR DOCTOR VISIT
What your doctor will ask you about: diet, weight loss, weakness or
dizziness when standing, bruising, vomiting blood, black stools, diar-
rhea, excessive menstrual bleeding, tiredness on arising in the morning,
trouble concentrating, loss of appetite, loss of interest in sex, fever or
chills, sore throat, difficulty breathing, headache, chest or abdominal
pain, muscle weakness, excessive sleeping. He or she will also ask
whether you have ever been told you are anemic, and whether you have
ever had a bone marrow biopsy (a procedure in which a long needle is
inserted into a bone near your hip), and if so, what the results were.

Your doctor will want to know if you or anyone in your family
has had any emotional disorders or chronic diseases.

Your doctor will want to know if you’re taking any medications,
including: methyldopa (Aldomet), reserpine, beta-blockers, seda-tives, tranquilizers, antidepressants, antihistamines, aspirin, ibupro-
fen or other nonsteroidal anti-inflammatory drugs, steroids, iron,
vitamin B12, folate.

Your doctor will do a physical examination including the follow-
ing: blood pressure, pulse, temperature, weight, checking your throat
for redness, listening to your heart and chest with a stethoscope, push-
ing on your abdomen, checking your lymph nodes to see if they are
enlarged, testing your stool for blood, eye exam, thorough skin exam.

ASK THE FOLLOWING QUESTION
WHAT CAN CAUSE WEAKNESS, AND WHAT IS TYPICAL FOR EACH CAUSE?##

Extreme blood loss #Sudden injury, or internal bleeding due to a tumor ing, or other condition #Fatigue, weakness, bleed- tar-like stools, pallor 
@ Iron deficiency #Menstruation or poor diet # Fatigue, weakness 
@ Folate (a B vitamin) deficiency # Alcoholism or poor diet # Fatigue, weakness 
@ Vitamin B12 deficiency # Poor absorption of vita- min B12, which is sometimes hereditary # Fatigue, diarrhea, reduced sensation in toes 
@ Failure of the bone marrow (the place your body makes most blood cells) #Chronic disease includ- ing some kinds of cancers, exposure to chemicals such as benzene or arsenic, exposure to radiation, chemotherapy, gold shots (given for arthritis) # Fatigue, weakness 
@ Increased destruction of blood cells #Sickle-cell anemia, malaria, recent trans- fusion, family history of anemia # Fatigue, weakness, jaun- dice (skin taking on a yel- lowish appearance) 

