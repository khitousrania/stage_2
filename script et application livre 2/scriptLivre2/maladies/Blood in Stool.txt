Blood in Stool
What it looks like: a mixing of blood with bowel movements, mak-
ing the toilet water red or streaking stool or toilet paper.

Eating certain things can change the color of your stool. For
instance, beets can turn stool red, while iron pills and bismuth
(Pepto-Bismol) can turn stool black.



YOUR DOCTOR VISIT
What your doctor will ask you about: abdominal pain, changes in
bowel habits or stool, mucus or pus in stool, pain with bowel move-
ments, nausea, vomiting, heartburn, vomiting blood, bruising,
weight loss, dizziness when standing, whether you have had a bari-
um enema, proctoscope, or abdominal X-ray done in the past, and
what they showed.

Your doctor will want to know if you or anyone in your family
has had any of these conditions: hemorrhoids, diverticulosis, coli-
tis, peptic ulcers, bleeding tendency, alcoholism, colon polyps.

Your doctor will want to know if you’re taking any of these med-
ications: warfarin (Coumadin), adrenal steroids, aspirin, anti-
inflammatory drugs.

Your doctor will do a physical examination including the fol-
lowing: blood pressure, pulse, pushing on your abdomen, checking
your rectum for hemorrhoids, testing your stool for blood, thorough
skin examination.
ASK THE FOLLOWING QUESTION
IN ADULTS TITREOPTION
WHAT CAN CAUSE BLOOD IN STOOL,AND WHAT IS TYPICAL FOR EACH CAUSE?##

Hemorrhoids/anal fissure (See chapter on Anus Problems.)#Swollen blood vessels in the anus or rectum (hemorrhoids) or tears in the tissue lining the anus(fissures)#Rectal pain, light bleeding
@Angiodysplasia/ diverticular disease#Swollen, weakened blood vessels in the colon,leading to loss of blood #Bright red stool, minimal  pain
@Ulcer (See  chapter on Abdominal Pain (Adult).)#Severe irritation of the stomach or intestinal lining#Black, tar-like bowel move-ments, vomiting, burning upper abdominal pain that is worse when lying down,sometimes relieved by food or antacids and made worse by aspirin or drugs such as ibuprofen
@Ulcerative colitis#Inflammation of the colon and rectum#Low-grade fever (less than 101 degrees F), some pain in lower abdomen, blood may appear in stools,which are small and not well-formed, sometimes weight loss
@Gastritis#Infection of the stomach#Black, tar-like bowel move-ments, vomiting, upper abdominal pain and ten-derness
@Esophageal varices#Swollen blood vessels in the esophagus #Black, tar-like bowel move-ments, vomiting, vomiting blood, jaundice (skin tak-ing on a yellowish appear-ance), spiderweb-like col-lection of blood vessels near the skin surface
@Intestinal tumors or polyps#An abnormal growth of cells, may begin as a benign growth (polyp)#Blood in stools, constipa-tion, weight loss, pain

Option# IN INFANTS TITREOPTION
WHAT CAN CAUSE BLOOD IN STOOL,AND WHAT IS TYPICAL FOR EACH CAUSE?##

Swallowed  blood#Occurs while breast-feeding or during delivery#Dark, tar-like stools
@Hemorrhagic disease#Excess bleeding#Bruising, bright red blood,dark, tar-like stools
                                         
Option# IN CHILDREN TITREOPTION
WHAT CAN CAUSE BLOOD IN STOOL,AND WHAT IS TYPICAL FOR EACH CAUSE?##

Fissures or polyps#Tears in the tissue lining the anus (fissures) or benign growths (polyps)#Red blood streaks in stool
@Constipation(See chapter on  Constipation(Child).)#Inability to have bowel movements#Bright red blood in stool
@Meckel’s diverticulum#Tiny pouch located on the wall of the lower bowel, a vestige from the umbilical cord and fetal intestines; rare#No pain
@Volvulus or intussusception#Congenital shift in the position of the intestine, which sometimes leads to obstruction; rare#Vomiting, decrease in  bowel movements, abdom-inal pain
     
                      
