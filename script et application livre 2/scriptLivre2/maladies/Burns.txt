Burns
What it feels like: pain, blistering, and charred skin caused by injury
from electricity, fire, or chemicals.



YOUR DOCTOR VISIT
What your doctor will ask you about: pain, blistering, trouble
breathing, loss of consciousness. If the burn was electrical, the doc-
tor will ask where the source touched you, and what the source was.

Your doctor will want to know exactly where on your body you were
burned, and the source of the burn. If it was a flame, the doctor will
want to know if your face was burned. If the burn was chemical, the doc-
tor will want to know what kind of chemical it was, whether there was
contact to your face or eyes, and whether you swallowed any of it.

Your doctor will want to know when you had your last tetanus
shot, and how many tetanus shots you have received in your life.

Your doctor will do a physical examination including the fol-
lowing: blood pressure, pulse, breathing rate, thorough skin exam.

ASK THE FOLLOWING QUESTION
WHAT ARE THE DIFFERENT TYPES OF BURNS,AND WHAT IS TYPICAL FOR EACH TYPE?##

First-degree#Affects only the outer-most layer of skin#Pain, red and dry skin, able to feel pinprick on burned skin
@Second-degree#Affects outermost and an additional layer of skin#Mostly painful, blisters,underlying moist and red tissue, often able to feel pinprick on burned skin
@Third-degree#Burn affects deep tissues,beyond outermost layers#No pain, charred or leath-ery skin, skin may be white under surface, no feeling of pinprick on burned skin
                       
                                               
$Severe burns can cause large amounts of fluid loss, as well as infections. Burns to the
face are particularly troublesome because associated damage to the lungs, which can
occur if hot air is breathed in, can lead to breathing difficulties. Electrical burns may
look less severe than they are because some of the damage is to internal organs.

