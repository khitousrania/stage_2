Shakiness

What it feels like: involuntary rhythmic or non-rhythmic body
movements.

What can make it worse: rest, movement, anxiety, alcohol, falling
asleep, fatigue.

Your doctor may distinguish between different forms of shaki-
ness. For instance, rhythmic, involuntary movements in the arms
and legs are often called tremors, while doctors often refer to sudden,
jerking, and nonrhythmic body movements as twitches.



YOUR DOCTOR VISIT
What your doctor will ask you about: muscle weakness, recent joint
pain, fever, skin rash, convulsions, anxiety, depression, strange feel-
ings, abnormal strength or sensations, lack of equilibrium, change in
writing, yellowing of skin, results of previous tests of brain function.

Your doctor will want to know if you or anyone in your family
has had any of these conditions: alcoholism, delirium tremens,
emotional problems, liver disease, nervous system disease,
Parkinson’s disease, thyroid disease, drug addiction, syphilis, rheu-
matic fever, birth injury, mental retardation, similar shakiness.

Your doctor will want to know when you first began to notice
your shakiness, and what areas of your body are involved.

Your doctor will want to know if you’re taking any of these med-
ications: alcohol, diphenylhydantoin (Dilantin), L-dopa, ben-
zotropine (Cogentin), metoclopramide (Reglan), tranquilizers, lithi-
um (Eskalith), antidepressants, phenothiazines such as chlorpro-
mazine (Thorazine) or haloperidol (Haldol).

In rare instances, people may develop a twitching in the body after
exposure to insecticides.

Your doctor will do a physical examination including the fol-
lowing: pulse, temperature, checking neck for enlargement of the
thyroid, pushing on the abdomen, checking for rigidity, listening to
the heart with a stethoscope, testing for movement, strength, reflex-
es, facial expressions, and balance.

Your doctor may also ask you to try to suppress the shakiness.
ASK THE FOLLOWING QUESTION

Other TITREOPTION
WHAT CAN CAUSE TREMORS,AND WHAT IS TYPICAL FOR EACH CAUSE?##

Anxiety # Feeling anxious # Tremors present during movement or when hold- ing one position, sweaty palms, history of severe or chronic emotional stress 
@ Drug withdrawal #Painful symptoms that occur when coming off of an addictive substance # Tremors present during movement or when hold- ing one position, fever, delirium, history of drug addiction 
@ Inherited tremor #Tremor passed down through families, also known as “essential tremor” #Tremors present during movement or when hold- ing one position, family history of tremor, develops later in life, may disappear when drinking alcohol or taking beta-blockers 
@ Hyper- thyroidism #Overactivity of the thyroid gland # Tremors present during movement or when hold- ing one position, weight loss despite good appetite, inability to tolerate heat 
@ Medication use #Tremor induced by certain medications #Tremors present during movement or when hold- ing one position, follows use of lithium or antide- pressants 
@ Parkinson’s disease # Nervous system disease that produces tremors # Tremors present at rest and disappear with movement, shuffling, use of metoclo- pramide (Reglan) 
@ Other nervous system diseases #Certain nervous system diseases which produce tremors that increase with particular movements #No tremors at rest, tremors increase with particular movements such as finger- to-nose testing, history of neurologic disease, lack of coordination, unsteady gait 

Option# TWITCHING TITREOPTION
WHAT ARE SOME DIFFERENT TYPES OF TWITCHING,AND HOW DO THEY APPEAR?##

Tics  # Rapid, repetitive move- ments, such as blinking, sniffing, or contracting one side of the face  # Tics are more prominent  during periods of stress,  and can be voluntarily  suppressed 
@ Myoclonus #  Movements created by involuntary muscle  contractions #Rapid, irregular jerks in the arms and legs that occur when falling asleep; these movements may also occur in people with convulsive disorders or nervous sys- tem diseases 
@  Chorea # A condition marked by uncontrolled movements throughout the body #  Widespread, rapid, and   jerky movements in differ-   ent body regions, skin rash,   may occur in children after   joint pain and fever; these   movements may also occur   in people with nervous sys-   tem diseases or those tak-   ing certain medications 
@  Fasciculations # Brief, nonrhythmic contractions of muscles # Small contractions that  make the muscle appear to  shiver, common in fatigued  muscles 

