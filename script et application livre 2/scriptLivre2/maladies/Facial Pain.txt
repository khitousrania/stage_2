Facial Pain
What it feels like: varies from headache and nasal congestion to
pain located in parts of the face, such as the eye, tooth, or jaw.

What can make it worse: touching the area, chewing, lowering the
head between the legs.



YOUR DOCTOR VISIT
What your doctor will ask you about: headaches, recent facial
injury, anxiety, depression, fever, earache, eye pain, visual change,
nasal discharge, toothache.

Your doctor will want to know if you or anyone in your family
has had any of these conditions: diabetes, headaches, migraine
headaches, nervous system disease, recurrent ear infection, rheuma-
toid arthritis, glaucoma, sinus disease, dental infections.

Your doctor will want to know exactly where on your face you
are feeling pain.

Your doctor will want to know if you’re taking any of these med-
ications: diphenylhydantoin (Dilantin), ergot derivatives (Cafergot),
codeine, aspirin, other treatments for pain.

Your doctor will do a physical examination including the fol-
lowing: temperature, checking teeth for hygiene and pain, thorough
head, eye, and ear exam.
ASK THE FOLLOWING QUESTION
WHAT CAN CAUSE FACIAL PAIN, AND WHAT IS TYPICAL FOR EACH CAUSE?##

Acute sinusitis # Sudden inflammation of the sinuses #Sudden facial pain, headache, nasal conges- tion, runny nose, pain worsens when lowering the head between the legs 
@ Acute angle closure glaucoma # Sudden buildup of pressure in the eyeball  #Sudden facial pain, eye is painful and red, headache, nausea, vomiting, blurred vision, halo around lights, may be precipitated by darkness or stress 
@ Dental abscess  #Collection of pus in the gums, caused by an infection #Sudden tooth pain 
@ Herpes zoster  #Inflammation of the  nerves in the spine and  skull, causing redness on the skin; also known as shingles #Sudden facial pain, begins before, during, or after outbreak 
@ Tic douloureux #Nerve problem causing facial pain and spasms  in facial muscles #Jabs of pain, pain often occurs near the eye, may be brought on by cold, heat, or pressure over the “trigger” area 
@ Temporoman- dibular joint pain — “TMJ” #Pain in the lower jaw #Pain on chewing, some- times history of rheumatoid arthritis 
@ Chronic or acute otitis media #Infection of the middle  ear #Earache, decreased hearing 
@  Migraine  headache # Severe headache #Throbbing pain that can last several days, often experience typical “funny” feelings before pain kicks in, headache often centers in the front of the head, often preceded by nausea and vomiting, family histo- ry of migraine, may be caused by alcohol or stress (see chapter on Headache) 

