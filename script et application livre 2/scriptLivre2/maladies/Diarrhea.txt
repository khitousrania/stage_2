Diarrhea
What it feels like: frequently passing loose stools, lasting a few days
or even years.

What can make it worse: anxiety (if diarrhea is chronic or recurrent).



YOUR DOCTOR VISIT
What your doctor will ask you about: changes in weight, faintness
when rising suddenly, nausea, vomiting, fever, chills, abdominal pain,
blood or mucus or pus in stool, malaise, muscle aches, joint pain, colds,
skin rashes, back pain, anxiety or depression, changes in bowel habits,
tenderness around the rectum. Your doctor will ask how long the diar-
rhea has occurred, how often you experience diarrhea each day,
whether you can eat or drink without vomiting, and whether you have
eaten meat or dairy products within three days of the onset of symp-
toms. The doctor will also want to know if you have ever had a barium
enema, an X-ray of your abdomen, or any kind of exam in which a doc-
tor inserted a small camera into your rectum. He or she will want to
know about any recent travel to a tropical or subtropical country.

Your doctor will want to know if you or anyone in your family
has had any of these conditions: ankylosing spondylitis, emotional
problems, diverticulosis, known ulcerative colitis or regional enteri-
tis, perirectal abscess, past gastrointestinal surgery, pancreatitis, ane-
mia, diabetes, recurrent respiratory infections, cystic fibrosis.

Your doctor will want to know if you have been eating a lot of cere-
als, prunes, or roughage, which can influence bowel movements.

If your diarrhea began fairly abruptly, your doctor may ask if you
are at risk of having caught a “bug” – for instance, if you spend a
lot of time in day care centers or nurseries, if you share toilets
with others, or if anyone around you has the same problem.

Your doctor will want to know if you have recently traveled to cer-
tain regions or countries, which can increase your risk of getting a
number of conditions that cause diarrhea, such as traveler’s diar-
rhea, dysentery, or giardiasis.

If the patient is a child, your doctor will want to know about her
diet, and if she has recently changed diets, how much milk she
drinks.

Your doctor will want to know if you’re taking any medications,
including: adrenal steroids, sedatives, tranquilizers, quinidine,
colchicine sulfasalazine (Azulfidine), antibiotics (ampicillin or tetra-
cycline), antispasmodics, Imodium, laxatives, antacids.

Your doctor will do a physical examination including the fol-
lowing: temperature, pulse, blood pressure, weight, looking inside
the throat, pushing on the abdomen, thorough skin exam, rectal
exam, testing stool for the presence of blood, pus, eggs, or parasites.

ASK THE FOLLOWING QUESTION
ACUTE DIARRHEA TITREOPTION
WHAT CAN CAUSE DIARRHEA,AND WHAT IS TYPICAL FOR EACH CAUSE?##

Viral#Infection with a virus         #Vomiting, malaise, some- times fever, cold, some- times pus, blood, or mucus in stool, typically lasts 2 to 3 days 
@Bacterial       #Infection with a  bacterium, such as  Salmonella or E. coli,  usually food poisoning    #Often begins within 72 hours after eating food or drinking water that made others sick, pus and mucus often present in stool, fever, typically lasts days 
@Bacterial toxins      #Eating food contaminated  with toxins produced by   bacteria   #Usually begins within six hours of eating food that made others sick, severe nausea and vomiting, typi- cally lasts up to 36 hours 
@Giardiasis        #Intestinal illness caused     by the organism Giardia lamblia, usually after  wading or swimming in  freshwater streams or drinking contaminated water #Bulky stools, abdominal discomfort, weight loss, typically lasts days to weeks    
@Schistosomal  dysentery    #Intestinal illness caused by a parasite that can be  contracted in tropical or   #Can progress to fever, chills, cough, hives, lasts up to three months subtropical countries 
@Amebic dysentery   #Intestinal illness caused by a parasite that can be contracted in tropical or  subtropical countries  #History of recurrent diarrhea, profuse bloody diarrhea, abdominal tenderness 
@Traveler’s diarrhea      #Intestinal illness typically  seen in travelers to  Mexico and Latin America; also called “Montezuma’s Revenge” #Stools may contain blood, typically lasts up to 2 days    
@Malaria       #Blood infection caused by parasites and transmitted by mosquitoes, mostly present in tropical or subtropical countries #Cold clammy skin, pro- found weakness, fainting, jaundice (skin taking on a yellowish appearance), typically lasts days with cycling fevers 
@Cholera     #Intestinal illness caused   by Vibrio cholerae, a       bacterium mostly present   in tropical or subtropical   countries #Low blood pressure, sunken eyeballs, bluish tint to skin, typically lasts up to 7 days  
	
Option# CHRONIC OR RECURRENT DIARRHEA TITREOPTION
WHAT CAN CAUSE DIARRHEA,AND WHAT IS TYPICAL FOR EACH CAUSE?##

Irritable bowel syndrome  # Alternating diarrhea and constipation, sometimes  occurring during periods of anxiety  #  Cramping, diarrhea, consti-   pation, with minimal pain,   no fever, typically lasts   days and recurs 
@  Crohn’s disease   (See chapter on   Abdominal  Pain (Adult).) #Chronic inflammation of the intestines   #Bloody and/or frequent diarrhea, occurs more often at night, frequent abdominal pain 
@  Ulcerative  colitis # Inflammation of the rectum and colon   #Low-grade fever (less than 101 degrees F), some pain in lower abdomen, blood may appear in stools, which are small and not well-formed, sometimes weight loss 
@  Malabsorption #An inability to absorb nutrients from the   digestive system, a result  of bowel surgery or  pancreatitis (inflammation  of the pancreas) #Large and foul-smelling stools that are lightly col- ored and oily, weight loss, weakness, typically lasts years 
@  Medication use #Resulting from certain medications, including antibiotics, laxatives, Maalox, colchicine, quinidine #  Typically lasts between   weeks and months 
@  Partial obstruction  #Blockage in the intestines,  caused by tumor or the  impaction of feces #Abdominal mass, rectal mass, constipation can also be a symptom 
@   Diabetes #Problems in the  regulation of sugar in  the blood #Long history of diabetes, often nerve damage  
@   Giardiasis #Intestinal illness caused  by the organism Giardia lamblia (see description   above)  lasts days to weeks #  Bulky stools, history of   drinking contaminated   water, abdominal discom-   fort, weight loss, typically 

Option# CHRONIC OR RECURRENT DIARRHEA TITREOPTION
WHAT CAN CAUSE DIARRHEA,AND WHAT IS TYPICAL FOR EACH CAUSE?##

Cystic fibrosis (in children)   # A genetic disease in    which children become    more prone to lung  infections and digestive  problems #   History of frequent respira-   tory infections, family his-  tory of cystic fibrosis   
@ Celiac disease #     Inability to digest gluten,     found in wheat flour   #    Weight loss, usually begins    after child turns 6 months 
@  Allergy to  cow’s milk  #    Bodily reaction to     ingesting cow’s milk # Vomiting, bloody diarrhea, severe weight loss 
@  Disaccharidase  deficiency # Lack of an enzyme that acts on sugars   #  Begins soon after birth,  diarrhea is watery, explo-  sive, or frothy 


$Many of the conditions that can cause acute diarrhea and are typically seen in peo-
ple who have traveled to tropical or subtropical countries can also cause chronic or
recurrent diarrhea.

