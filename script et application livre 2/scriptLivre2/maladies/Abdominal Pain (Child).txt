Abdominal Pain (Child)
What it feels like: varies from a gnawing pain near the top of your
abdomen, to cramping pain in your lower abdomen, to sharp shoot-
ing pains in many areas.

What can make it worse: food, medications, movement, position,
bowel movements, emotional stress.

What can make it better: food or milk, antacids, medications, posi-
tion, bowel movements, passing gas, burping.



YOUR DOCTOR VISIT
What your doctor will ask you and your child about: headache,
coughing, vomiting, changes in bowel habits, the color of the stool,
weight loss, constipation, blood or worms in stool, flank pain, blood
in the urine, painful urination, joint pains, attention-seeking behavior.

Your doctor will want to know if your child or anyone in your
family has had any of these conditions: recent “stomach bug,”
sickle-cell disease, mumps, or strep throat.

Your doctor will want to know whether your home has lead-
based paint, and if you’ve seen your child eating paint chips.

Your doctor will want to know if your child is taking any med-
ications.

Your doctor will do a physical examination including the fol-
lowing: temperature, blood pressure, pulse, weight, listening to your
child’s chest with a stethoscope, pushing on your child’s abdomen,
listening to your child’s abdomen with a stethoscope, testing your
child’s stool for blood, thorough skin examination, tests of your
child’s hip joint for pain.

Your doctor may do the following blood tests: blood count, blood
chemistry, liver function tests.
ASK THE FOLLOWING QUESTION
WHAT CAN CAUSE ABDOMINAL PAIN,AND WHAT IS TYPICAL FOR EACH CAUSE?##

Gastroenteritis#Infection of the stomach or intestines#Nausea, vomiting,diarrhea, cramping, muscle aches, slight fever
@Unclear cause#Alternating diarrhea and constipation, sometimes occurring during periods of anxiety#Attention-seeking behavior,cramping, diarrhea, constipation, with minimal pain, no fever 
@Colic#Crying spells seen between the ages of 2 weeks and 4 months,probably due to abdominal pain#Crying spells, usually resolves on its own by age of 4 months
@Constipation#Constipation#Diffuse pain
@Appendicitis(unlikely before the age of 3 years)#Infection or inflammation of the appendix, a small pouch of the large intestine#Pain in the lower right part of the abdomen, low-grade fever (less than 101 degrees F)
@Pharyngitis#Sore throat, can lead to abdominal pain#Fever, enlarged “glands,”sore throat, redness in throat
@Pneumonia#Lung infection, can lead to abdominal pain because of coughing#Fever, cough
@Mumps#Infection that causes the area around the cheeks to swell, now prevented in large part by vaccination (MMR = measles, mumps, rubella) #Swollen cheeks, fever
@Lactose intolerance#Reaction to lactose, a sugar found in milk and cheese#Bloating, cramping pain
@Sickle-cell disease #Painful “crises” caused by misshapen red blood cells, an inherited disease found most often in African Americans #Severe pain in abdomen and joints, sweating, some-times fever	   
@Worms#Infection by Ascaris,hookworm, Taenia,Strongyloides species#Worms and eggs in stool,mild pain, weight loss,diarrhea	   
@Lead poisoning#Caused by exposure to  lead, most often in lead  paint in older houses#Diffuse abdominal pain,can in the long term lead to mental retardation	   
@Bowel obstruction#A blockage in the intestines#Nausea, vomiting, some-times constipation,distended belly, extreme discomfort	
@Intussusception (common between ages another of 5 months and 2 years)#“Telescoping” of tubes of intestines into one#Slight fever, acute sudden pain, vomiting, often decreased bowel movements
@Ulcer#Severe irritation of the stomach or intestinal lining#Burning upper abdominal pain that is worse when lying down, sometimes relieved by antacids and made worse by aspirin or ibuprofen	
@Henoch-Schönlein purpura#Inflammation of the blood vessels that often follows respiratory infections#Joint pain, vomiting, dis-tended belly, bruising(occurs later)	   
@Kidney disease#Congenital kidney problems (several)#Severity of pain varies, but generally over the flank	   
@Hepatitis (unlikely)#Infection or inflammation of the liver, can be caused by viruses #Weakness, fatigue, right upper abdominal pain,jaundice (skin taking on a yellowish appearance)
@Cholecystitis (unlikely in  children)#Gallstones#Fever, right upper abdomi-nal pain, sometimes pain in right shoulder, nausea,vomiting, chills, sometimes jaundice (skin taking on a yellowish appearance),dark urine
@Pancreatitis (unlikely in children)#Inflammation of the pancreas, the organ that  produces insulin, the glucose-regulating hormone, often associ-ated with gallstones #Pain in the upper abdomen, sometimes in the back, nausea, vomiting,sometimes weakness and rapid heart rate
@Ulcerative colitis (unlikely in children)#Inflammation of the rectum and colon#Low-grade fever (less than 101 degrees F), some pain in lower abdomen, blood may appear in stools,which are small and not well-formed, sometimes weight loss
@Crohn’s disease(unlikely in children)# Inflammation of the entire gastrointestinal system (can occur from  mouth to rectum)#Low-grade fever (less than 101 degrees F), pain in  lower right part of the abdomen that is often relieved by defecation of stools that are soft and not well formed
          
           
                               
                                                
                                             
