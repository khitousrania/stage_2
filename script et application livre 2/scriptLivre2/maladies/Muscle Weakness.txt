Muscle Weakness 
What it feels like: muscular fatigue, causing problems getting
around and performing day-to-day activities.

If your muscles feel strong but you feel weak, see the chapter on
Weakness for more information. If your muscle weakness has
occurred suddenly, see the chapter on Numbness, Loss of Movement,
and Trouble Talking to make sure you are not experiencing a stroke.


YOUR DOCTOR VISIT
What your doctor will ask you about: neck pain, back pain, mus-
cle pain, muscle twitching, blurred or double vision, changes in sen-
sation or speech, heat intolerance, obesity, abnormal hair growth.

Your doctor will want to know if you or anyone in your family
has had any of these conditions: chronic disease, alcoholism, dis-
ease of the discs in the back, nervous system disease, thyroid disease,
muscle weakness.

Your doctor will want to know if the weakness occurs all over, or
in particular regions of the body, and if it occurs sporadically or
has worsened with time.

Your doctor will want to know if you’re taking any of these med-
ications: steroids, statins to treat high cholesterol.

Your doctor will ask you if you have been exposed to insecticides
or received a vaccine against polio, and if you feel particularly
weak when arising from a chair.

Your doctor will do a physical examination including tests of
reflexes, movement, and sensation.
ASK THE FOLLOWING QUESTION
WHAT CAN CAUSE MUSCLE WEAKNESS,AND WHAT IS TYPICAL FOR EACH CAUSE?##

 Muscular  dystrophy  #Hereditary disease char-  acterized by progressive  muscle wasting  dystrophy #Progressive weakness, diffi- culty getting up from a chair, family history of 
@  Myositis  # Infection that causes pain  or weakness in muscles  #Weakness, pain, tenderness 
@  Disuse atrophy  #Wasting of muscles after  long disuse, perhaps  following disease  # Occurs in people with dis-  abling illness, such as  stroke or arthritis 
@  Drug use  # Weakness caused by  certain medications  #Occurs in people taking steroids, statins, and diuret- ics (“water pills”), and in heavy alcohol drinkers 
@  Endocrine  disease  #Disease affecting the  hormones  #  Heat intolerance, weight  gain in the abdomen,  abnormal hair growth 
@  Insecticide  poisoning  # Ingesting a toxic amount  of insecticides  #  Double vision, weakness  of speech, weakness wors-  ens at the end of the day,  fatigue after exercise 
@  Peripheral  neuropathy  #Disease of the nerves in  the extremities that  occurs more commonly  in people who drink heavily or have diabetes #  Weakness occurs in one  body region, change in  sensation  
@  Nervous system  disease  #Abnormalities in the  brain or spinal cord  #  Regional weakness,  abnormal sensation 
@  Guillain-Barré  syndrome  # Disease characterized by  inflammation in the  nerves  # Weakness and paralysis  that begins in the legs, may  progress rapidly 
@  Poliomyelitis  #Disease caused by the  polio virus that can lead  to paralysis  #Fever, rapid onset of wide- spread weakness, no history of immunization against the virus 
@ Amyotrophic  lateral sclerosis  #Disease of the nerve  cells that can lead to  loss of control over movements, also known as Lou Gehrig disease # Slowly progressive weak-  ness, occurs only in adults  
@ Werdnig-  Hoffman  disease  # Genetic disease that can  lead to progressive  muscle weakness  #  Generalized weakness,  lack of reflexes, occurs only in children 

