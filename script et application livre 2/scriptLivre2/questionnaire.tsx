import {
  Component,
  Event,
  EventEmitter,
  h,
  Listen,
  Prop,
  State,
} from '@stencil/core';
import { RouterHistory } from '@stencil/router';
import { LOCAL_STORAGE_KEYS, ROUTES } from '../../../global/constants';
import { QUESTIONS, QUESTION } from '../../../global/questions';
import i18next from '../../../global/utils/i18n';
import { trackEvent, TRACKING_EVENTS } from '../../../global/utils/track';
import version from '../../../global/utils/version';
import { DateInput } from '../../input-date/input-date-functional';
import {
  checkGoTo,
  checkGuard,
  getQuestionIndexById,
  updateScoreData,
  getScoreChange,
} from './utils';

export type Scores = { [key: string]: number };
export type Answers = { [key: string]: string | string[] };

@Component({
  styleUrl: 'questionnaire.css',
  tag: 'ia-questionnaire',
})
export class Questionnaire {
  @Prop() history: RouterHistory;

  @State() language: string;
  @State() currentStep: number = 0;
  @State() previousStep: number;
  @State() answerData: Answers = {};
  @State() scoreData: Scores = {};
  @Event() showLogoHeader: EventEmitter;

  formElement: HTMLFormElement;

  @Listen('changedLanguage', {
    target: 'window',
  })
  changedLanguageHandler(event: CustomEvent) {
    this.language = event.detail.code;
  }

  @Listen('popstate', {
    target: 'window',
  })
  handlePopStateChange() {
    if (this.currentStep > 0) {
      this.history.push(ROUTES.QUESTIONNAIRE, {});
      this.moveToPreviousStep();
    }
  }

  @Listen('updateFormData')
  updateFormDataHandler(event: CustomEvent) {
    const { detail } = event;
    this.setFormData(detail.key, detail.value);
  }

  // TODO: https://github.com/gesundheitscloud/infection-risk-assessment/pull/76
  // This is only a temporary fix. This should be moved/handled differently
  @Listen('onDateChange')
  onDateChangeHandler(event: CustomEvent) {
    const { currentStep } = this;
    const {
      detail: { value },
    } = event;
    this.setFormData(QUESTIONS[currentStep].id, value.split('-').join('.'));
  }

  setFormData(key: string, value: string | string[]) {
    this.answerData = {
      ...this.answerData,
      [key]: value,
    };
  }

  setLocalStorageAnswers = () => {
    localStorage.setItem(
      LOCAL_STORAGE_KEYS.ANSWERS,
      JSON.stringify(this.answerData)
    );
    localStorage.setItem(LOCAL_STORAGE_KEYS.SCORES, JSON.stringify(this.scoreData));
    localStorage.setItem(LOCAL_STORAGE_KEYS.COMPLETED, 'false')
    version.set();
  };

  get progress() {
    return Math.floor(((this.currentStep + 1) / QUESTIONS.length) * 100);
  }

  trackStepMove(isPreviousStep: Boolean) {
    const { id } = QUESTIONS[this.currentStep];
    if (isPreviousStep) {
      trackEvent([...TRACKING_EVENTS.QUESTION_PREVIOUS, id]);
    } else {
      trackEvent([...TRACKING_EVENTS.QUESTION_NEXT, id]);
    }
  }

  moveToNextStep = () => {
    const answerIndex = this.answerData[QUESTIONS[this.currentStep].id];

    if (QUESTIONS[this.currentStep].id === QUESTION.DATA_DONATION) {
      trackEvent([
        ...TRACKING_EVENTS.DATA_DONATION_CONSENT,
        this.answerData[QUESTION.DATA_DONATION] === '0' ? '1' : '0',
      ]);
    }

    this.scoreData = updateScoreData(this.currentStep, answerIndex, this.scoreData);
    this.setLocalStorageAnswers();
    if (!this.checkFinished()) {
      this.currentStep = checkGoTo(this.currentStep, answerIndex);
      const stepBeforeGuard = this.currentStep;
      this.currentStep = checkGuard(
        this.currentStep,
        this.scoreData,
        this.answerData
      );
      if (stepBeforeGuard !== this.currentStep) {
        this.checkFinished();
        return;
      }
      this.trackStepMove(false);
    }
  };

  checkFinished = () => {
    let nextQuestionId = QUESTIONS[this.currentStep].nextQuestionMap;
if (nextQuestionId == '100' || nextQuestionId == '101' || nextQuestionId == '102' || nextQuestionId == '103' || nextQuestionId == '104' || nextQuestionId == '105' || nextQuestionId == '106' || nextQuestionId == '107' || nextQuestionId == '108' || nextQuestionId == '109' || nextQuestionId == '1010' || nextQuestionId == '1011' || nextQuestionId == '1012' || nextQuestionId == '1013' || nextQuestionId == '1014' || nextQuestionId == '1015' || nextQuestionId == '1016' || nextQuestionId == '1017' || nextQuestionId == '1018' || nextQuestionId == '200' || nextQuestionId == '201' || nextQuestionId == '202' || nextQuestionId == '203' || nextQuestionId == '204' || nextQuestionId == '205' || nextQuestionId == '206' || nextQuestionId == '207' || nextQuestionId == '208' || nextQuestionId == '209' || nextQuestionId == '2010' || nextQuestionId == '2011' || nextQuestionId == '2012' || nextQuestionId == '2013' || nextQuestionId == '2014' || nextQuestionId == '2015' || nextQuestionId == '2016' || nextQuestionId == '2017' || nextQuestionId == '2018' || nextQuestionId == '2019' || nextQuestionId == '2020' || nextQuestionId == '2021' || nextQuestionId == '300' || nextQuestionId == '301' || nextQuestionId == '302' || nextQuestionId == '303' || nextQuestionId == '304' || nextQuestionId == '400' || nextQuestionId == '401' || nextQuestionId == '402' || nextQuestionId == '403' || nextQuestionId == '404' || nextQuestionId == '405' || nextQuestionId == '406' || nextQuestionId == '407' || nextQuestionId == '500' || nextQuestionId == '501' || nextQuestionId == '502' || nextQuestionId == '503' || nextQuestionId == '504' || nextQuestionId == '505' || nextQuestionId == '506' || nextQuestionId == '507' || nextQuestionId == '508' || nextQuestionId == '509' || nextQuestionId == '5010' || nextQuestionId == '5011' || nextQuestionId == '5012' || nextQuestionId == '5013' || nextQuestionId == '5014' || nextQuestionId == '5015' || nextQuestionId == '5016' || nextQuestionId == '600' || nextQuestionId == '601' || nextQuestionId == '602' || nextQuestionId == '603' || nextQuestionId == '700' || nextQuestionId == '701' || nextQuestionId == '702' || nextQuestionId == '703' || nextQuestionId == '704' || nextQuestionId == '705' || nextQuestionId == '706' || nextQuestionId == '800' || nextQuestionId == '801' || nextQuestionId == '802' || nextQuestionId == '803' || nextQuestionId == '804' || nextQuestionId == '805' || nextQuestionId == '806' || nextQuestionId == '900' || nextQuestionId == '901' || nextQuestionId == '902' || nextQuestionId == '903' || nextQuestionId == '904' || nextQuestionId == '905' || nextQuestionId == '906' || nextQuestionId == '910' || nextQuestionId == '911' || nextQuestionId == '920' || nextQuestionId == '921' || nextQuestionId == '922' || nextQuestionId == '923' || nextQuestionId == '1000' || nextQuestionId == '1001' || nextQuestionId == '1002' || nextQuestionId == '1003' || nextQuestionId == '1010' || nextQuestionId == '1011' || nextQuestionId == '1020' || nextQuestionId == '1021' || nextQuestionId == '1030' || nextQuestionId == '1031' || nextQuestionId == '1032' || nextQuestionId == '1100' || nextQuestionId == '1101' || nextQuestionId == '1102' || nextQuestionId == '1103' || nextQuestionId == '1104' || nextQuestionId == '1105' || nextQuestionId == '1106' || nextQuestionId == '1107' || nextQuestionId == '1108' || nextQuestionId == '1109' || nextQuestionId == '11010' || nextQuestionId == '11011' || nextQuestionId == '11012' || nextQuestionId == '11013' || nextQuestionId == '1200' || nextQuestionId == '1201' || nextQuestionId == '1202' || nextQuestionId == '1203' || nextQuestionId == '1204' || nextQuestionId == '1205' || nextQuestionId == '1206' || nextQuestionId == '1207' || nextQuestionId == '1208' || nextQuestionId == '1300' || nextQuestionId == '1301' || nextQuestionId == '1400' || nextQuestionId == '1401' || nextQuestionId == '1402' || nextQuestionId == '1500' || nextQuestionId == '1501' || nextQuestionId == '1502' || nextQuestionId == '1503' || nextQuestionId == '1504' || nextQuestionId == '1505' || nextQuestionId == '1506' || nextQuestionId == '1507' || nextQuestionId == '1508' || nextQuestionId == '1509' || nextQuestionId == '15010' || nextQuestionId == '15011' || nextQuestionId == '15012' || nextQuestionId == '15013' || nextQuestionId == '15014' || nextQuestionId == '15015' || nextQuestionId == '15016' || nextQuestionId == '15017' || nextQuestionId == '15018' || nextQuestionId == '15019' || nextQuestionId == '15020' || nextQuestionId == '1600' || nextQuestionId == '1601' || nextQuestionId == '1602' || nextQuestionId == '1700' || nextQuestionId == '1701' || nextQuestionId == '1702' || nextQuestionId == '1703' || nextQuestionId == '1704' || nextQuestionId == '1705' || nextQuestionId == '1706' || nextQuestionId == '1800' || nextQuestionId == '1801' || nextQuestionId == '1802' || nextQuestionId == '1803' || nextQuestionId == '1900' || nextQuestionId == '1901' || nextQuestionId == '1902' || nextQuestionId == '1903' || nextQuestionId == '1904' || nextQuestionId == '1905' || nextQuestionId == '1906' || nextQuestionId == '1907' || nextQuestionId == '1908' || nextQuestionId == '1909' || nextQuestionId == '19010' || nextQuestionId == '19011' || nextQuestionId == '2000' || nextQuestionId == '2001' || nextQuestionId == '2002' || nextQuestionId == '2003' || nextQuestionId == '2004' || nextQuestionId == '2005' || nextQuestionId == '2006' || nextQuestionId == '2007' || nextQuestionId == '2008' || nextQuestionId == '2009' || nextQuestionId == '20010' || nextQuestionId == '20011' || nextQuestionId == '20012' || nextQuestionId == '20013' || nextQuestionId == '20014' || nextQuestionId == '2300' || nextQuestionId == '2301' || nextQuestionId == '2302' || nextQuestionId == '2303' || nextQuestionId == '2304' || nextQuestionId == '2305' || nextQuestionId == '2400' || nextQuestionId == '2401' || nextQuestionId == '2402' || nextQuestionId == '2403' || nextQuestionId == '2404' || nextQuestionId == '2405' || nextQuestionId == '2406' || nextQuestionId == '2407' || nextQuestionId == '2408' || nextQuestionId == '2410' || nextQuestionId == '2411' || nextQuestionId == '2412' || nextQuestionId == '2413' || nextQuestionId == '2414' || nextQuestionId == '2415' || nextQuestionId == '2416' || nextQuestionId == '2417' || nextQuestionId == '2420' || nextQuestionId == '2421' || nextQuestionId == '2422' || nextQuestionId == '2423' || nextQuestionId == '2500' || nextQuestionId == '2501' || nextQuestionId == '2502' || nextQuestionId == '2503' || nextQuestionId == '2504' || nextQuestionId == '2505' || nextQuestionId == '2506' || nextQuestionId == '2507' || nextQuestionId == '2508' || nextQuestionId == '2509' || nextQuestionId == '2600' || nextQuestionId == '2601' || nextQuestionId == '2602' || nextQuestionId == '2603' || nextQuestionId == '2604' || nextQuestionId == '2605' || nextQuestionId == '2606' || nextQuestionId == '2607' || nextQuestionId == '2608' || nextQuestionId == '2609' || nextQuestionId == '26010' || nextQuestionId == '26011' || nextQuestionId == '26012' || nextQuestionId == '2700' || nextQuestionId == '2701' || nextQuestionId == '2702' || nextQuestionId == '2703' || nextQuestionId == '2704' || nextQuestionId == '2705' || nextQuestionId == '2706' || nextQuestionId == '2707' || nextQuestionId == '2708' || nextQuestionId == '2710' || nextQuestionId == '2711' || nextQuestionId == '2712' || nextQuestionId == '2713' || nextQuestionId == '2800' || nextQuestionId == '2801' || nextQuestionId == '2802' || nextQuestionId == '2803' || nextQuestionId == '2804' || nextQuestionId == '2805' || nextQuestionId == '2806' || nextQuestionId == '2900' || nextQuestionId == '2901' || nextQuestionId == '2902' || nextQuestionId == '2903' || nextQuestionId == '2904' || nextQuestionId == '2905' || nextQuestionId == '2906' || nextQuestionId == '2910' || nextQuestionId == '2911' || nextQuestionId == '2912' || nextQuestionId == '2913' || nextQuestionId == '2914' || nextQuestionId == '2920' || nextQuestionId == '2921' || nextQuestionId == '2922' || nextQuestionId == '2923' || nextQuestionId == '2924' || nextQuestionId == '2925' || nextQuestionId == '2926' || nextQuestionId == '2930' || nextQuestionId == '2931' || nextQuestionId == '2932' || nextQuestionId == '2933' || nextQuestionId == '2934' || nextQuestionId == '2940' || nextQuestionId == '2941' || nextQuestionId == '2942' || nextQuestionId == '2950' || nextQuestionId == '2951' || nextQuestionId == '2952' || nextQuestionId == '2953' || nextQuestionId == '2954' || nextQuestionId == '2960' || nextQuestionId == '2961' || nextQuestionId == '2962' || nextQuestionId == '3000' || nextQuestionId == '3001' || nextQuestionId == '3002' || nextQuestionId == '3003' || nextQuestionId == '3004' || nextQuestionId == '3005' || nextQuestionId == '3006' || nextQuestionId == '3007' || nextQuestionId == '3100' || nextQuestionId == '3101' || nextQuestionId == '3102' || nextQuestionId == '3103' || nextQuestionId == '3104' || nextQuestionId == '3110' || nextQuestionId == '3111' || nextQuestionId == '3112' || nextQuestionId == '3113' || nextQuestionId == '3114' || nextQuestionId == '3115' || nextQuestionId == '3116' || nextQuestionId == '3117' || nextQuestionId == '3118' || nextQuestionId == '3119' || nextQuestionId == '31110' || nextQuestionId == '31111' || nextQuestionId == '31112' || nextQuestionId == '3120' || nextQuestionId == '3121' || nextQuestionId == '3122' || nextQuestionId == '3123' || nextQuestionId == '3124' || nextQuestionId == '3125' || nextQuestionId == '3200' || nextQuestionId == '3201' || nextQuestionId == '3202' || nextQuestionId == '3203' || nextQuestionId == '3204' || nextQuestionId == '3205' || nextQuestionId == '3206' || nextQuestionId == '3207' || nextQuestionId == '3208' || nextQuestionId == '3209' || nextQuestionId == '32010' || nextQuestionId == '32011' || nextQuestionId == '32012' || nextQuestionId == '3400' || nextQuestionId == '3401' || nextQuestionId == '3402' || nextQuestionId == '3403' || nextQuestionId == '3404' || nextQuestionId == '3405' || nextQuestionId == '3406' || nextQuestionId == '3407' || nextQuestionId == '3408' || nextQuestionId == '3409' || nextQuestionId == '34010' || nextQuestionId == '34011' || nextQuestionId == '3500' || nextQuestionId == '3501' || nextQuestionId == '3502' || nextQuestionId == '3600' || nextQuestionId == '3601' || nextQuestionId == '3602' || nextQuestionId == '3603' || nextQuestionId == '3604' || nextQuestionId == '3605' || nextQuestionId == '3606' || nextQuestionId == '3607' || nextQuestionId == '3608' || nextQuestionId == '3700' || nextQuestionId == '3701' || nextQuestionId == '3702' || nextQuestionId == '3703' || nextQuestionId == '3704' || nextQuestionId == '3705' || nextQuestionId == '3706' || nextQuestionId == '3707' || nextQuestionId == '3708' || nextQuestionId == '3709' || nextQuestionId == '37010' || nextQuestionId == '37011' || nextQuestionId == '37012' || nextQuestionId == '37013' || nextQuestionId == '37014' || nextQuestionId == '37015' || nextQuestionId == '37016' || nextQuestionId == '3800' || nextQuestionId == '3801' || nextQuestionId == '3802' || nextQuestionId == '3803' || nextQuestionId == '3900' || nextQuestionId == '3901' || nextQuestionId == '3902' || nextQuestionId == '3903' || nextQuestionId == '3904' || nextQuestionId == '3905' || nextQuestionId == '3906' || nextQuestionId == '3907' || nextQuestionId == '3908' || nextQuestionId == '3909' || nextQuestionId == '3910' || nextQuestionId == '3911' || nextQuestionId == '3912' || nextQuestionId == '4000' || nextQuestionId == '4001' || nextQuestionId == '4002' || nextQuestionId == '4003' || nextQuestionId == '4004' || nextQuestionId == '4005' || nextQuestionId == '4200' || nextQuestionId == '4201' || nextQuestionId == '4300' || nextQuestionId == '4301' || nextQuestionId == '4302' || nextQuestionId == '4303' || nextQuestionId == '4304' || nextQuestionId == '4305' || nextQuestionId == '4400' || nextQuestionId == '4401' || nextQuestionId == '4402' || nextQuestionId == '4403' || nextQuestionId == '4404' || nextQuestionId == '4500' || nextQuestionId == '4501' || nextQuestionId == '4502' || nextQuestionId == '4503' || nextQuestionId == '4504' || nextQuestionId == '4505' || nextQuestionId == '4506' || nextQuestionId == '4600' || nextQuestionId == '4601' || nextQuestionId == '4610' || nextQuestionId == '4611' || nextQuestionId == '4612' || nextQuestionId == '4613' || nextQuestionId == '4614' || nextQuestionId == '4615' || nextQuestionId == '4620' || nextQuestionId == '4630' || nextQuestionId == '4631' || nextQuestionId == '4640' || nextQuestionId == '4641' || nextQuestionId == '4642' || nextQuestionId == '4650' || nextQuestionId == '4651' || nextQuestionId == '4652' || nextQuestionId == '4653' || nextQuestionId == '4654' || nextQuestionId == '4655' || nextQuestionId == '4656' || nextQuestionId == '4657' || nextQuestionId == '4658' || nextQuestionId == '4659' || nextQuestionId == '46510' || nextQuestionId == '4700' || nextQuestionId == '4701' || nextQuestionId == '4702' || nextQuestionId == '4703' || nextQuestionId == '4704' || nextQuestionId == '4705' || nextQuestionId == '4706' || nextQuestionId == '4707' || nextQuestionId == '4708' || nextQuestionId == '4709' || nextQuestionId == '47010' || nextQuestionId == '4800' || nextQuestionId == '4801' || nextQuestionId == '4802' || nextQuestionId == '4803' || nextQuestionId == '4804' || nextQuestionId == '4805' || nextQuestionId == '4806' || nextQuestionId == '4810' || nextQuestionId == '4811' || nextQuestionId == '4820' || nextQuestionId == '4821' || nextQuestionId == '4822' || nextQuestionId == '4830' || nextQuestionId == '4831' || nextQuestionId == '4832' || nextQuestionId == '4840' || nextQuestionId == '4841' || nextQuestionId == '4842' || nextQuestionId == '4850' || nextQuestionId == '4851' || nextQuestionId == '4852' || nextQuestionId == '4860' || nextQuestionId == '4861' || nextQuestionId == '4862' || nextQuestionId == '4870' || nextQuestionId == '4871' || nextQuestionId == '4900' || nextQuestionId == '4901' || nextQuestionId == '4902' || nextQuestionId == '4903' || nextQuestionId == '4904' || nextQuestionId == '4905' || nextQuestionId == '4906' || nextQuestionId == '4907' || nextQuestionId == '4908' || nextQuestionId == '4909' || nextQuestionId == '49010' || nextQuestionId == '49011' || nextQuestionId == '49012' || nextQuestionId == '49013' || nextQuestionId == '49014' || nextQuestionId == '49015' || nextQuestionId == '49016' || nextQuestionId == '49017' || nextQuestionId == '49018' || nextQuestionId == '5000' || nextQuestionId == '5001' || nextQuestionId == '5002' || nextQuestionId == '5200' || nextQuestionId == '5201' || nextQuestionId == '5202' || nextQuestionId == '5203' || nextQuestionId == '5204' || nextQuestionId == '5205' || nextQuestionId == '5206' || nextQuestionId == '5207' || nextQuestionId == '5210' || nextQuestionId == '5211' || nextQuestionId == '5220' || nextQuestionId == '5221' || nextQuestionId == '5230' || nextQuestionId == '5231' || nextQuestionId == '5240' || nextQuestionId == '5241' || nextQuestionId == '5300' || nextQuestionId == '5301' || nextQuestionId == '5302' || nextQuestionId == '5303' || nextQuestionId == '5304' || nextQuestionId == '5305' || nextQuestionId == '5306' || nextQuestionId == '5307' || nextQuestionId == '5308' || nextQuestionId == '5309' || nextQuestionId == '53010' || nextQuestionId == '53011' || nextQuestionId == '5400' || nextQuestionId == '5401' || nextQuestionId == '5402' || nextQuestionId == '5403' || nextQuestionId == '5410' || nextQuestionId == '5411' || nextQuestionId == '5420' || nextQuestionId == '5421' || nextQuestionId == '5422' || nextQuestionId == '5500' || nextQuestionId == '5501' || nextQuestionId == '5502' || nextQuestionId == '5503' || nextQuestionId == '5504' || nextQuestionId == '5505' || nextQuestionId == '5506' || nextQuestionId == '5507' || nextQuestionId == '5508' || nextQuestionId == '5509' || nextQuestionId == '5510' || nextQuestionId == '5511' || nextQuestionId == '5512' || nextQuestionId == '5513' || nextQuestionId == '5514' || nextQuestionId == '5600' || nextQuestionId == '5601' || nextQuestionId == '5602' || nextQuestionId == '5603' || nextQuestionId == '5604' || nextQuestionId == '5605' || nextQuestionId == '5606' || nextQuestionId == '5607' || nextQuestionId == '5608' || nextQuestionId == '5609' || nextQuestionId == '5610' || nextQuestionId == '5611' || nextQuestionId == '5800' || nextQuestionId == '5801' || nextQuestionId == '5802' || nextQuestionId == '5803' || nextQuestionId == '5804' || nextQuestionId == '5805' || nextQuestionId == '5806' || nextQuestionId == '6000' || nextQuestionId == '6001' || nextQuestionId == '6002' || nextQuestionId == '6003' || nextQuestionId == '6004' || nextQuestionId == '6005' || nextQuestionId == '6100' || nextQuestionId == '6101' || nextQuestionId == '6102' || nextQuestionId == '6103' || nextQuestionId == '6104' || nextQuestionId == '6105' || nextQuestionId == '6106' || nextQuestionId == '6107' || nextQuestionId == '6300' || nextQuestionId == '6301' || nextQuestionId == '6302' || nextQuestionId == '6310' || nextQuestionId == '6311' || nextQuestionId == '6312' || nextQuestionId == '6313' || nextQuestionId == '6314' || nextQuestionId == '6315' || nextQuestionId == '6400' || nextQuestionId == '6401' || nextQuestionId == '6402' || nextQuestionId == '6403' || nextQuestionId == '6404' || nextQuestionId == '6405' || nextQuestionId == '6410' || nextQuestionId == '6411' || nextQuestionId == '6500' || nextQuestionId == '6501' || nextQuestionId == '6502' || nextQuestionId == '6503' || nextQuestionId == '6504' || nextQuestionId == '6505' || nextQuestionId == '6506' || nextQuestionId == '6510' || nextQuestionId == '6511' || nextQuestionId == '6512' || nextQuestionId == '6513' || nextQuestionId == '6600' || nextQuestionId == '6601' || nextQuestionId == '6602' || nextQuestionId == '6603' || nextQuestionId == '6604' || nextQuestionId == '6605' || nextQuestionId == '6606' || nextQuestionId == '6607' || nextQuestionId == '6608' || nextQuestionId == '6609' || nextQuestionId == '66010' || nextQuestionId == '66011' || nextQuestionId == '66012' || nextQuestionId == '66013' || nextQuestionId == '66014' || nextQuestionId == '66015' || nextQuestionId == '6610' || nextQuestionId == '6611' || nextQuestionId == '6612' || nextQuestionId == '6613' || nextQuestionId == '6700' || nextQuestionId == '6701' || nextQuestionId == '6702' || nextQuestionId == '6703' || nextQuestionId == '6704' || nextQuestionId == '6705' || nextQuestionId == '6706' || nextQuestionId == '6800' || nextQuestionId == '6801' || nextQuestionId == '6802' || nextQuestionId == '6803' || nextQuestionId == '6804' || nextQuestionId == '6805' || nextQuestionId == '6806' || nextQuestionId == '6900' || nextQuestionId == '6901' || nextQuestionId == '6902' || nextQuestionId == '6903' || nextQuestionId == '6904' || nextQuestionId == '6905' || nextQuestionId == '6906' || nextQuestionId == '6907' || nextQuestionId == '6908' || nextQuestionId == '6909' || nextQuestionId == '69010' || nextQuestionId == '6910' || nextQuestionId == '6911' || nextQuestionId == '7000' || nextQuestionId == '7001' || nextQuestionId == '7002' || nextQuestionId == '7010' || nextQuestionId == '7011' || nextQuestionId == '7012' || nextQuestionId == '7013' || nextQuestionId == '7014' || nextQuestionId == '7100' || nextQuestionId == '7110' || nextQuestionId == '7111' || nextQuestionId == '7112' || nextQuestionId == '7113' || nextQuestionId == '7114' || nextQuestionId == '7120' || nextQuestionId == '7121' || nextQuestionId == '7130' || nextQuestionId == '7131' || nextQuestionId == '7140' || nextQuestionId == '7141' || nextQuestionId == '7142' || nextQuestionId == '7200' || nextQuestionId == '7201' || nextQuestionId == '7202' || nextQuestionId == '7300' || nextQuestionId == '7301' || nextQuestionId == '7302' || nextQuestionId == '7303' || nextQuestionId == '7304' || nextQuestionId == '7305' || nextQuestionId == '7400' || nextQuestionId == '7401' || nextQuestionId == '7402' || nextQuestionId == '7403' || nextQuestionId == '7404' || nextQuestionId == '7405' || nextQuestionId == '7500' || nextQuestionId == '7501' || nextQuestionId == '7502' || nextQuestionId == '7503' || nextQuestionId == '7504' || nextQuestionId == '7505' || nextQuestionId == '7506' || )localStorage.setItem(LOCAL_STORAGE_KEYS.SUMMARY_COMMENT_ID, nextQuestionId);
        this.history.push(ROUTES.SUMMARY, {});
        trackEvent(TRACKING_EVENTS.FINISH);
        return true;
      }

    return false;
  };

  // TODO: (Radimir) optimise for displaying the last answered question
  moveToPreviousStep = () => {
    if (this.currentStep === 0) {
      this.history.push(`/`, {});
      localStorage.removeItem(LOCAL_STORAGE_KEYS.ANSWERS);
      trackEvent(TRACKING_EVENTS.ABORT);
    } else {
      if (this.answerData) {
        const formDataKeys = Object.keys(this.answerData);
        const previousDataKey = formDataKeys[formDataKeys.length - 1];
        const previousQuestion = QUESTIONS[getQuestionIndexById(previousDataKey)];
        if (previousQuestion.scoreMap) {
          const previousAnswer = this.answerData[previousDataKey];
          this.scoreData[previousQuestion.category] -= getScoreChange(
            previousQuestion,
            previousAnswer
          );
        }

        delete this.answerData[previousDataKey];

        this.setLocalStorageAnswers();

        this.currentStep = previousDataKey
          ? getQuestionIndexById(previousDataKey)
          : 0;
      } else {
        this.currentStep--;
      }
      this.trackStepMove(true);
    }
  };

  submitForm = event => {
    event.preventDefault();
    event.target.querySelector('input').focus();
    this.moveToNextStep();
  };

  componentWillLoad = () => {
    this.showLogoHeader.emit({ show: false });
    if (!version.match()) {
      version.reset();
    }
    const availableAnswers = JSON.parse(
      localStorage.getItem(LOCAL_STORAGE_KEYS.ANSWERS)
    );
    const availableScores = JSON.parse(
      localStorage.getItem(LOCAL_STORAGE_KEYS.SCORES)
    );
    this.answerData = availableAnswers ? availableAnswers : {};
    this.scoreData = availableScores ? availableScores : {};

    const formDataKeys = Object.keys(this.answerData);

    if (formDataKeys.length) {
      this.currentStep = getQuestionIndexById(formDataKeys[formDataKeys.length - 1]);

      this.moveToNextStep();
    }
  };

  render() {
    const { submitForm, currentStep, moveToPreviousStep, progress } = this;

    return (
      currentStep < QUESTIONS.length && (
        <div class="questionnaire c-card-wrapper">
          <form
            onSubmit={event => submitForm(event)}
            ref={el => (this.formElement = el as HTMLFormElement)}
            data-test="questionnaireForm"
          >
            <d4l-card classes="card--desktop card--text-center">
              <div class="u-margin-horizontal--card-negative" slot="card-header">
                <ia-navigation-header
                  headline={i18next.t(QUESTIONS[currentStep].text)}
                  handleClick={() => moveToPreviousStep()}
                />
                <d4l-linear-progress data-test="progressBar" value={progress} />
              </div>
              <div
                class="questionnaire__content u-padding-vertical--medium u-text-align--left"
                slot="card-content"
              >
                <fieldset>
                  <legend class="u-visually-hidden">
                    {i18next.t(QUESTIONS[currentStep].text)}
                  </legend>
                  <div class="questionnaire__form u-padding-vertical--normal ">
                    {QUESTIONS[currentStep].inputType === 'radio' && (
                      <ia-input-radio
                        data-test="questionnaireRadioInputs"
                        question={QUESTIONS[currentStep]}
                        currentSelection={this.answerData[QUESTIONS[currentStep].id]}
                      />
                    )}
                    {QUESTIONS[currentStep].inputType === 'checkbox' && (
                      <ia-input-multiple-choice question={QUESTIONS[currentStep]} />
                    )}

                    {QUESTIONS[currentStep].inputType === 'date' && <DateInput />}
                    {QUESTIONS[currentStep].inputType === 'postal' && (
                      <ia-input-postal-code question={QUESTIONS[currentStep]} />
                    )}
                  </div>
                  {QUESTIONS[currentStep].comment && (
                    <p
                      class="questionnaire__comment"
                      innerHTML={`${i18next.t(QUESTIONS[currentStep].comment)}`}
                    ></p>
                  )}
                </fieldset>
              </div>
              <div slot="card-footer">
                <d4l-button
                  classes="button--block"
                  data-test="continueButton"
                  text={
                    currentStep === QUESTIONS.length - 1
                      ? i18next.t('questionnaire_button_generate_qr')
                      : i18next.t('questionnaire_button_next')
                  }
                />
              </div>
            </d4l-card>
          </form>
        </div>
      )
    );
  }
}
