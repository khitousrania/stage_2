import {
    Guard
  
  } from './guard';
  
  
  export type Question = {
    id: string;
    category: string;
    comment?: string;
    text: string;
    inputType: 'radio' | 'date' | 'checkbox' | 'postal';
    options?: string[] | CheckboxOption[];
    nextQuestionMap?: string | string[];
    scoreMap?: number[];
    guard?: Guard;
  };
  
  export type CheckboxOption = {
    label: string;
    id: string;
  };
  
  export const CATEGORIES = {
    PERSONAL: 'personalInfo',
    SYMPTOMS: 'symptoms',
  
  };
  export const NO_XML = 'X';
  export const QUESTION = {
    POSTAL_CODE: 'V1',
    AGE: 'P0',
    ABOVE_65: 'P1',
    LIVING_SITUATION: 'P2',
    CARING: 'P3',
    WORKSPACE: 'P4',
    CONTACT_DATE: 'CZ',
    OUT_OF_BREATH: 'SB',
    SYMPTOM_DATE: 'SZ',
    DATA_DONATION: `${NO_XML}1`,
  };
  
  export const XML_ORDER = ['V', 'P', 'C', 'S', 'D', 'M'];
  export const QUESTIONS: Question[] = [
//*******************PARTIE QUESTOIN :CHOIX PREMIERE LETTERE DE LA MALADIE************
{
	id:'question',
	category:CATEGORIES.PERSONAL,
	text:'question',
	inputType:'radio',
	comment:'',
	options:['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'L', 'M', 'N', 'O', 'P', 'S', 'T', 'U', 'V', 'W', 'Y'],
	nextQuestionMap:['A1-A4', 'B5-B14', 'C15-C21', 'D22-D26', 'E27-E29', 'F30-F33', 'G34-G35', 'H36-H45', 'I46-I47', 'J48-J48', 'L49-L49', 'M50-M53', 'N54-N58', 'O59-O60', 'P61-P62', 'S63-S69', 'T70-T70', 'U71-U71', 'V72-V73', 'W74-W74', 'Y75-Y75'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//**********AJOUT DELIMITATION MALADIE ALPHABETS DE A.. a A..****************

//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE A1-A4
{
	id:'A1-A4',
	category:CATEGORIES.PERSONAL,
	text:'A1-A4',
	inputType:'radio',
	comment:'',
	options:['A1', 'A2', 'A3', 'A4'],
	nextQuestionMap:['A1', 'A2', 'A3', 'A4'],
	scoreMap:[0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE B5-B14
{
	id:'B5-B14',
	category:CATEGORIES.PERSONAL,
	text:'B5-B14',
	inputType:'radio',
	comment:'',
	options:['B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'B12', 'B13', 'B14'],
	nextQuestionMap:['B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'B12', 'B13', 'B14'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE C15-C21
{
	id:'C15-C21',
	category:CATEGORIES.PERSONAL,
	text:'C15-C21',
	inputType:'radio',
	comment:'',
	options:['C15', 'C16', 'C17', 'C18', 'C19', 'C20', 'C21'],
	nextQuestionMap:['C15', 'C16', 'C17', 'C18', 'C19', 'C20', 'C21'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE D22-D26
{
	id:'D22-D26',
	category:CATEGORIES.PERSONAL,
	text:'D22-D26',
	inputType:'radio',
	comment:'',
	options:['D22', 'D23', 'D24', 'D25', 'D26'],
	nextQuestionMap:['D22', 'D23', 'D24', 'D25', 'D26'],
	scoreMap:[0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE E27-E29
{
	id:'E27-E29',
	category:CATEGORIES.PERSONAL,
	text:'E27-E29',
	inputType:'radio',
	comment:'',
	options:['E27', 'E28', 'E29'],
	nextQuestionMap:['E27', 'E28', 'E29'],
	scoreMap:[0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE F30-F33
{
	id:'F30-F33',
	category:CATEGORIES.PERSONAL,
	text:'F30-F33',
	inputType:'radio',
	comment:'',
	options:['F30', 'F31', 'F32', 'F33'],
	nextQuestionMap:['F30', 'F31', 'F32', 'F33'],
	scoreMap:[0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE G34-G35
{
	id:'G34-G35',
	category:CATEGORIES.PERSONAL,
	text:'G34-G35',
	inputType:'radio',
	comment:'',
	options:['G34', 'G35'],
	nextQuestionMap:['G34', 'G35'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE H36-H45
{
	id:'H36-H45',
	category:CATEGORIES.PERSONAL,
	text:'H36-H45',
	inputType:'radio',
	comment:'',
	options:['H36', 'H37', 'H38', 'H39', 'H40', 'H41', 'H42', 'H43', 'H44', 'H45'],
	nextQuestionMap:['H36', 'H37', 'H38', 'H39', 'H40', 'H41', 'H42', 'H43', 'H44', 'H45'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE I46-I47
{
	id:'I46-I47',
	category:CATEGORIES.PERSONAL,
	text:'I46-I47',
	inputType:'radio',
	comment:'',
	options:['I46', 'I47'],
	nextQuestionMap:['I46', 'I47'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE J48-J48
{
	id:'J48-J48',
	category:CATEGORIES.PERSONAL,
	text:'J48-J48',
	inputType:'radio',
	comment:'',
	options:['J48'],
	nextQuestionMap:['J48'],
	scoreMap:[0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE L49-L49
{
	id:'L49-L49',
	category:CATEGORIES.PERSONAL,
	text:'L49-L49',
	inputType:'radio',
	comment:'',
	options:['L49'],
	nextQuestionMap:['L49'],
	scoreMap:[0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE M50-M53
{
	id:'M50-M53',
	category:CATEGORIES.PERSONAL,
	text:'M50-M53',
	inputType:'radio',
	comment:'',
	options:['M50', 'M51', 'M52', 'M53'],
	nextQuestionMap:['M50', 'M51', 'M52', 'M53'],
	scoreMap:[0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE N54-N58
{
	id:'N54-N58',
	category:CATEGORIES.PERSONAL,
	text:'N54-N58',
	inputType:'radio',
	comment:'',
	options:['N54', 'N55', 'N56', 'N57', 'N58'],
	nextQuestionMap:['N54', 'N55', 'N56', 'N57', 'N58'],
	scoreMap:[0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE O59-O60
{
	id:'O59-O60',
	category:CATEGORIES.PERSONAL,
	text:'O59-O60',
	inputType:'radio',
	comment:'',
	options:['O59', 'O60'],
	nextQuestionMap:['O59', 'O60'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE P61-P62
{
	id:'P61-P62',
	category:CATEGORIES.PERSONAL,
	text:'P61-P62',
	inputType:'radio',
	comment:'',
	options:['P61', 'P62'],
	nextQuestionMap:['P61', 'P62'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE S63-S69
{
	id:'S63-S69',
	category:CATEGORIES.PERSONAL,
	text:'S63-S69',
	inputType:'radio',
	comment:'',
	options:['S63', 'S64', 'S65', 'S66', 'S67', 'S68', 'S69'],
	nextQuestionMap:['S63', 'S64', 'S65', 'S66', 'S67', 'S68', 'S69'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE T70-T70
{
	id:'T70-T70',
	category:CATEGORIES.PERSONAL,
	text:'T70-T70',
	inputType:'radio',
	comment:'',
	options:['T70'],
	nextQuestionMap:['T70'],
	scoreMap:[0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE U71-U71
{
	id:'U71-U71',
	category:CATEGORIES.PERSONAL,
	text:'U71-U71',
	inputType:'radio',
	comment:'',
	options:['U71'],
	nextQuestionMap:['U71'],
	scoreMap:[0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE V72-V73
{
	id:'V72-V73',
	category:CATEGORIES.PERSONAL,
	text:'V72-V73',
	inputType:'radio',
	comment:'',
	options:['V72', 'V73'],
	nextQuestionMap:['V72', 'V73'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE W74-W74
{
	id:'W74-W74',
	category:CATEGORIES.PERSONAL,
	text:'W74-W74',
	inputType:'radio',
	comment:'',
	options:['W74'],
	nextQuestionMap:['W74'],
	scoreMap:[0]
},
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE Y75-Y75
{
	id:'Y75-Y75',
	category:CATEGORIES.PERSONAL,
	text:'Y75-Y75',
	inputType:'radio',
	comment:'',
	options:['Y75'],
	nextQuestionMap:['Y75'],
	scoreMap:[0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE A1
{
	id:'A1',
	category:CATEGORIES.PERSONAL,
	text:'A1',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_1', 'your_doctor_visit_1', 'question_1_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE A2
{
	id:'A2',
	category:CATEGORIES.PERSONAL,
	text:'A2',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_2', 'your_doctor_visit_2', 'question_2_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE A3
{
	id:'A3',
	category:CATEGORIES.PERSONAL,
	text:'A3',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_3', 'your_doctor_visit_3', 'question_3_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE A4
{
	id:'A4',
	category:CATEGORIES.PERSONAL,
	text:'A4',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_4', 'your_doctor_visit_4', 'question_4_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B5
{
	id:'B5',
	category:CATEGORIES.PERSONAL,
	text:'B5',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_5', 'your_doctor_visit_5', 'question_5_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B6
{
	id:'B6',
	category:CATEGORIES.PERSONAL,
	text:'B6',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_6', 'your_doctor_visit_6', 'question_6_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B7
{
	id:'B7',
	category:CATEGORIES.PERSONAL,
	text:'B7',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_7', 'your_doctor_visit_7', 'question_7_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B8
{
	id:'B8',
	category:CATEGORIES.PERSONAL,
	text:'B8',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_8', 'your_doctor_visit_8', 'question_8_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B9
{
	id:'B9',
	category:CATEGORIES.PERSONAL,
	text:'B9',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_9', 'your_doctor_visit_9', 'option_9_0-option_9_2'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B10
{
	id:'B10',
	category:CATEGORIES.PERSONAL,
	text:'B10',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_10', 'your_doctor_visit_10', 'option_10_0-option_10_3'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B11
{
	id:'B11',
	category:CATEGORIES.PERSONAL,
	text:'B11',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_11', 'your_doctor_visit_11', 'question_11_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B12
{
	id:'B12',
	category:CATEGORIES.PERSONAL,
	text:'B12',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_12', 'your_doctor_visit_12', 'question_12_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B13
{
	id:'B13',
	category:CATEGORIES.PERSONAL,
	text:'B13',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_13', 'your_doctor_visit_13', 'question_13_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE B14
{
	id:'B14',
	category:CATEGORIES.PERSONAL,
	text:'B14',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_14', 'your_doctor_visit_14', 'question_14_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C15
{
	id:'C15',
	category:CATEGORIES.PERSONAL,
	text:'C15',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_15', 'your_doctor_visit_15', 'question_15_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C16
{
	id:'C16',
	category:CATEGORIES.PERSONAL,
	text:'C16',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_16', 'your_doctor_visit_16', 'question_16_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C17
{
	id:'C17',
	category:CATEGORIES.PERSONAL,
	text:'C17',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_17', 'your_doctor_visit_17', 'question_17_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C18
{
	id:'C18',
	category:CATEGORIES.PERSONAL,
	text:'C18',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_18', 'your_doctor_visit_18', 'question_18_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C19
{
	id:'C19',
	category:CATEGORIES.PERSONAL,
	text:'C19',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_19', 'your_doctor_visit_19', 'question_19_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C20
{
	id:'C20',
	category:CATEGORIES.PERSONAL,
	text:'C20',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_20', 'your_doctor_visit_20', 'question_20_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE C21
{
	id:'C21',
	category:CATEGORIES.PERSONAL,
	text:'C21',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit'],
	nextQuestionMap:['introduction_21', 'your_doctor_visit_21'],
	scoreMap:[0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE D22
{
	id:'D22',
	category:CATEGORIES.PERSONAL,
	text:'D22',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit'],
	nextQuestionMap:['introduction_22', 'your_doctor_visit_22'],
	scoreMap:[0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE D23
{
	id:'D23',
	category:CATEGORIES.PERSONAL,
	text:'D23',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_23', 'your_doctor_visit_23', 'question_23_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE D24
{
	id:'D24',
	category:CATEGORIES.PERSONAL,
	text:'D24',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_24', 'your_doctor_visit_24', 'option_24_0-option_24_2'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE D25
{
	id:'D25',
	category:CATEGORIES.PERSONAL,
	text:'D25',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_25', 'your_doctor_visit_25', 'question_25_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE D26
{
	id:'D26',
	category:CATEGORIES.PERSONAL,
	text:'D26',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_26', 'your_doctor_visit_26', 'question_26_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE E27
{
	id:'E27',
	category:CATEGORIES.PERSONAL,
	text:'E27',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_27', 'your_doctor_visit_27', 'option_27_0-option_27_1'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE E28
{
	id:'E28',
	category:CATEGORIES.PERSONAL,
	text:'E28',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_28', 'your_doctor_visit_28', 'question_28_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE E29
{
	id:'E29',
	category:CATEGORIES.PERSONAL,
	text:'E29',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_29', 'your_doctor_visit_29', 'option_29_0-option_29_6'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE F30
{
	id:'F30',
	category:CATEGORIES.PERSONAL,
	text:'F30',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_30', 'your_doctor_visit_30', 'question_30_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE F31
{
	id:'F31',
	category:CATEGORIES.PERSONAL,
	text:'F31',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_31', 'your_doctor_visit_31', 'option_31_0-option_31_2'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE F32
{
	id:'F32',
	category:CATEGORIES.PERSONAL,
	text:'F32',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_32', 'your_doctor_visit_32', 'question_32_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE F33
{
	id:'F33',
	category:CATEGORIES.PERSONAL,
	text:'F33',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit'],
	nextQuestionMap:['introduction_33', 'your_doctor_visit_33'],
	scoreMap:[0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE G34
{
	id:'G34',
	category:CATEGORIES.PERSONAL,
	text:'G34',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_34', 'your_doctor_visit_34', 'question_34_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE G35
{
	id:'G35',
	category:CATEGORIES.PERSONAL,
	text:'G35',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_35', 'your_doctor_visit_35', 'question_35_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H36
{
	id:'H36',
	category:CATEGORIES.PERSONAL,
	text:'H36',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_36', 'your_doctor_visit_36', 'question_36_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H37
{
	id:'H37',
	category:CATEGORIES.PERSONAL,
	text:'H37',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_37', 'your_doctor_visit_37', 'question_37_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H38
{
	id:'H38',
	category:CATEGORIES.PERSONAL,
	text:'H38',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_38', 'your_doctor_visit_38', 'question_38_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H39
{
	id:'H39',
	category:CATEGORIES.PERSONAL,
	text:'H39',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_39', 'your_doctor_visit_39', 'option_39_0-option_39_1'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H40
{
	id:'H40',
	category:CATEGORIES.PERSONAL,
	text:'H40',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_40', 'your_doctor_visit_40', 'question_40_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H41
{
	id:'H41',
	category:CATEGORIES.PERSONAL,
	text:'H41',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit'],
	nextQuestionMap:['introduction_41', 'your_doctor_visit_41'],
	scoreMap:[0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H42
{
	id:'H42',
	category:CATEGORIES.PERSONAL,
	text:'H42',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_42', 'your_doctor_visit_42', 'question_42_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H43
{
	id:'H43',
	category:CATEGORIES.PERSONAL,
	text:'H43',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_43', 'your_doctor_visit_43', 'question_43_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H44
{
	id:'H44',
	category:CATEGORIES.PERSONAL,
	text:'H44',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_44', 'your_doctor_visit_44', 'question_44_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE H45
{
	id:'H45',
	category:CATEGORIES.PERSONAL,
	text:'H45',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_45', 'your_doctor_visit_45', 'question_45_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE I46
{
	id:'I46',
	category:CATEGORIES.PERSONAL,
	text:'I46',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_46', 'your_doctor_visit_46', 'option_46_0-option_46_5'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE I47
{
	id:'I47',
	category:CATEGORIES.PERSONAL,
	text:'I47',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_47', 'your_doctor_visit_47', 'question_47_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE J48
{
	id:'J48',
	category:CATEGORIES.PERSONAL,
	text:'J48',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_48', 'your_doctor_visit_48', 'option_48_0-option_48_7'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE L49
{
	id:'L49',
	category:CATEGORIES.PERSONAL,
	text:'L49',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_49', 'your_doctor_visit_49', 'question_49_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE M50
{
	id:'M50',
	category:CATEGORIES.PERSONAL,
	text:'M50',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_50', 'your_doctor_visit_50', 'question_50_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE M51
{
	id:'M51',
	category:CATEGORIES.PERSONAL,
	text:'M51',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit'],
	nextQuestionMap:['introduction_51', 'your_doctor_visit_51'],
	scoreMap:[0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE M52
{
	id:'M52',
	category:CATEGORIES.PERSONAL,
	text:'M52',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_52', 'your_doctor_visit_52', 'option_52_0-option_52_4'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE M53
{
	id:'M53',
	category:CATEGORIES.PERSONAL,
	text:'M53',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_53', 'your_doctor_visit_53', 'question_53_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE N54
{
	id:'N54',
	category:CATEGORIES.PERSONAL,
	text:'N54',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_54', 'your_doctor_visit_54', 'option_54_0-option_54_2'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE N55
{
	id:'N55',
	category:CATEGORIES.PERSONAL,
	text:'N55',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_55', 'your_doctor_visit_55', 'option_55_0-option_55_1'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE N56
{
	id:'N56',
	category:CATEGORIES.PERSONAL,
	text:'N56',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_56', 'your_doctor_visit_56', 'option_56_0-option_56_1'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE N57
{
	id:'N57',
	category:CATEGORIES.PERSONAL,
	text:'N57',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit'],
	nextQuestionMap:['introduction_57', 'your_doctor_visit_57'],
	scoreMap:[0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE N58
{
	id:'N58',
	category:CATEGORIES.PERSONAL,
	text:'N58',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_58', 'your_doctor_visit_58', 'question_58_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE O59
{
	id:'O59',
	category:CATEGORIES.PERSONAL,
	text:'O59',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit'],
	nextQuestionMap:['introduction_59', 'your_doctor_visit_59'],
	scoreMap:[0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE O60
{
	id:'O60',
	category:CATEGORIES.PERSONAL,
	text:'O60',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_60', 'your_doctor_visit_60', 'question_60_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE P61
{
	id:'P61',
	category:CATEGORIES.PERSONAL,
	text:'P61',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_61', 'your_doctor_visit_61', 'question_61_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE P62
{
	id:'P62',
	category:CATEGORIES.PERSONAL,
	text:'P62',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit'],
	nextQuestionMap:['introduction_62', 'your_doctor_visit_62'],
	scoreMap:[0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S63
{
	id:'S63',
	category:CATEGORIES.PERSONAL,
	text:'S63',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_63', 'your_doctor_visit_63', 'option_63_0-option_63_1'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S64
{
	id:'S64',
	category:CATEGORIES.PERSONAL,
	text:'S64',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_64', 'your_doctor_visit_64', 'option_64_0-option_64_1'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S65
{
	id:'S65',
	category:CATEGORIES.PERSONAL,
	text:'S65',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_65', 'your_doctor_visit_65', 'option_65_0-option_65_1'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S66
{
	id:'S66',
	category:CATEGORIES.PERSONAL,
	text:'S66',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_66', 'your_doctor_visit_66', 'option_66_0-option_66_1'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S67
{
	id:'S67',
	category:CATEGORIES.PERSONAL,
	text:'S67',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_67', 'your_doctor_visit_67', 'question_67_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S68
{
	id:'S68',
	category:CATEGORIES.PERSONAL,
	text:'S68',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_68', 'your_doctor_visit_68', 'question_68_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE S69
{
	id:'S69',
	category:CATEGORIES.PERSONAL,
	text:'S69',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_69', 'your_doctor_visit_69', 'option_69_0-option_69_1'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE T70
{
	id:'T70',
	category:CATEGORIES.PERSONAL,
	text:'T70',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_70', 'your_doctor_visit_70', 'option_70_0-option_70_1'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE U71
{
	id:'U71',
	category:CATEGORIES.PERSONAL,
	text:'U71',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_71', 'your_doctor_visit_71', 'option_71_0-option_71_4'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE V72
{
	id:'V72',
	category:CATEGORIES.PERSONAL,
	text:'V72',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_72', 'your_doctor_visit_72', 'question_72_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE V73
{
	id:'V73',
	category:CATEGORIES.PERSONAL,
	text:'V73',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_73', 'your_doctor_visit_73', 'question_73_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE W74
{
	id:'W74',
	category:CATEGORIES.PERSONAL,
	text:'W74',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_74', 'your_doctor_visit_74', 'question_74_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE Y75
{
	id:'Y75',
	category:CATEGORIES.PERSONAL,
	text:'Y75',
	inputType:'radio',
	comment:'',
	options:['introduction', 'your doctor visit', 'questionnaire'],
	nextQuestionMap:['introduction_75', 'your_doctor_visit_75', 'question_75_0'],
	scoreMap:[0, 0, 0]
},
//PARTIE DECOUPE OPTION MALADIE B9
{
	id:'option_9_0-option_9_2',
	category:CATEGORIES.PERSONAL,
	text:'option_9_0-option_9_2',
	inputType:'radio',
	comment:'',
	options:['option_9_0', 'option_9_1', 'option_9_2'],
	nextQuestionMap:['question_9_0', 'question_9_1', 'question_9_2'],
	scoreMap:[0, 0, 0]
},
//PARTIE DECOUPE OPTION MALADIE B10
{
	id:'option_10_0-option_10_3',
	category:CATEGORIES.PERSONAL,
	text:'option_10_0-option_10_3',
	inputType:'radio',
	comment:'',
	options:['option_10_0', 'option_10_1', 'option_10_2', 'option_10_3'],
	nextQuestionMap:['question_10_0', 'question_10_1', 'question_10_2', 'question_10_3'],
	scoreMap:[0, 0, 0, 0]
},
//PARTIE DECOUPE OPTION MALADIE D24
{
	id:'option_24_0-option_24_2',
	category:CATEGORIES.PERSONAL,
	text:'option_24_0-option_24_2',
	inputType:'radio',
	comment:'',
	options:['option_24_0', 'option_24_1', 'option_24_2'],
	nextQuestionMap:['question_24_0', 'question_24_1', 'question_24_2'],
	scoreMap:[0, 0, 0]
},
//PARTIE DECOUPE OPTION MALADIE E27
{
	id:'option_27_0-option_27_1',
	category:CATEGORIES.PERSONAL,
	text:'option_27_0-option_27_1',
	inputType:'radio',
	comment:'',
	options:['option_27_0', 'option_27_1'],
	nextQuestionMap:['question_27_0', 'question_27_1'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPE OPTION MALADIE E29
{
	id:'option_29_0-option_29_6',
	category:CATEGORIES.PERSONAL,
	text:'option_29_0-option_29_6',
	inputType:'radio',
	comment:'',
	options:['option_29_0', 'option_29_1', 'option_29_2', 'option_29_3', 'option_29_4', 'option_29_5', 'option_29_6'],
	nextQuestionMap:['question_29_0', 'question_29_1', 'question_29_2', 'question_29_3', 'question_29_4', 'question_29_5', 'question_29_6'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPE OPTION MALADIE F31
{
	id:'option_31_0-option_31_2',
	category:CATEGORIES.PERSONAL,
	text:'option_31_0-option_31_2',
	inputType:'radio',
	comment:'',
	options:['option_31_0', 'option_31_1', 'option_31_2'],
	nextQuestionMap:['question_31_0', 'question_31_1', 'question_31_2'],
	scoreMap:[0, 0, 0]
},
//PARTIE DECOUPE OPTION MALADIE H39
{
	id:'option_39_0-option_39_1',
	category:CATEGORIES.PERSONAL,
	text:'option_39_0-option_39_1',
	inputType:'radio',
	comment:'',
	options:['option_39_0', 'option_39_1'],
	nextQuestionMap:['question_39_0', 'question_39_1'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPE OPTION MALADIE I46
{
	id:'option_46_0-option_46_5',
	category:CATEGORIES.PERSONAL,
	text:'option_46_0-option_46_5',
	inputType:'radio',
	comment:'',
	options:['option_46_0', 'option_46_1', 'option_46_2', 'option_46_3', 'option_46_4', 'option_46_5'],
	nextQuestionMap:['question_46_0', 'question_46_1', 'question_46_2', 'question_46_3', 'question_46_4', 'question_46_5'],
	scoreMap:[0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPE OPTION MALADIE J48
{
	id:'option_48_0-option_48_7',
	category:CATEGORIES.PERSONAL,
	text:'option_48_0-option_48_7',
	inputType:'radio',
	comment:'',
	options:['option_48_0', 'option_48_1', 'option_48_2', 'option_48_3', 'option_48_4', 'option_48_5', 'option_48_6', 'option_48_7'],
	nextQuestionMap:['question_48_0', 'question_48_1', 'question_48_2', 'question_48_3', 'question_48_4', 'question_48_5', 'question_48_6', 'question_48_7'],
	scoreMap:[0, 0, 0, 0, 0, 0, 0, 0]
},
//PARTIE DECOUPE OPTION MALADIE M52
{
	id:'option_52_0-option_52_4',
	category:CATEGORIES.PERSONAL,
	text:'option_52_0-option_52_4',
	inputType:'radio',
	comment:'',
	options:['option_52_0', 'option_52_1', 'option_52_2', 'option_52_3', 'option_52_4'],
	nextQuestionMap:['question_52_0', 'question_52_1', 'question_52_2', 'question_52_3', 'question_52_4'],
	scoreMap:[0, 0, 0, 0, 0]
},
//PARTIE DECOUPE OPTION MALADIE N54
{
	id:'option_54_0-option_54_2',
	category:CATEGORIES.PERSONAL,
	text:'option_54_0-option_54_2',
	inputType:'radio',
	comment:'',
	options:['option_54_0', 'option_54_1', 'option_54_2'],
	nextQuestionMap:['question_54_0', 'question_54_1', 'question_54_2'],
	scoreMap:[0, 0, 0]
},
//PARTIE DECOUPE OPTION MALADIE N55
{
	id:'option_55_0-option_55_1',
	category:CATEGORIES.PERSONAL,
	text:'option_55_0-option_55_1',
	inputType:'radio',
	comment:'',
	options:['option_55_0', 'option_55_1'],
	nextQuestionMap:['question_55_0', 'question_55_1'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPE OPTION MALADIE N56
{
	id:'option_56_0-option_56_1',
	category:CATEGORIES.PERSONAL,
	text:'option_56_0-option_56_1',
	inputType:'radio',
	comment:'',
	options:['option_56_0', 'option_56_1'],
	nextQuestionMap:['question_56_0', 'question_56_1'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPE OPTION MALADIE S63
{
	id:'option_63_0-option_63_1',
	category:CATEGORIES.PERSONAL,
	text:'option_63_0-option_63_1',
	inputType:'radio',
	comment:'',
	options:['option_63_0', 'option_63_1'],
	nextQuestionMap:['question_63_0', 'question_63_1'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPE OPTION MALADIE S64
{
	id:'option_64_0-option_64_1',
	category:CATEGORIES.PERSONAL,
	text:'option_64_0-option_64_1',
	inputType:'radio',
	comment:'',
	options:['option_64_0', 'option_64_1'],
	nextQuestionMap:['question_64_0', 'question_64_1'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPE OPTION MALADIE S65
{
	id:'option_65_0-option_65_1',
	category:CATEGORIES.PERSONAL,
	text:'option_65_0-option_65_1',
	inputType:'radio',
	comment:'',
	options:['option_65_0', 'option_65_1'],
	nextQuestionMap:['question_65_0', 'question_65_1'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPE OPTION MALADIE S66
{
	id:'option_66_0-option_66_1',
	category:CATEGORIES.PERSONAL,
	text:'option_66_0-option_66_1',
	inputType:'radio',
	comment:'',
	options:['option_66_0', 'option_66_1'],
	nextQuestionMap:['question_66_0', 'question_66_1'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPE OPTION MALADIE S69
{
	id:'option_69_0-option_69_1',
	category:CATEGORIES.PERSONAL,
	text:'option_69_0-option_69_1',
	inputType:'radio',
	comment:'',
	options:['option_69_0', 'option_69_1'],
	nextQuestionMap:['question_69_0', 'question_69_1'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPE OPTION MALADIE T70
{
	id:'option_70_0-option_70_1',
	category:CATEGORIES.PERSONAL,
	text:'option_70_0-option_70_1',
	inputType:'radio',
	comment:'',
	options:['option_70_0', 'option_70_1'],
	nextQuestionMap:['question_70_0', 'question_70_1'],
	scoreMap:[0, 0]
},
//PARTIE DECOUPE OPTION MALADIE U71
{
	id:'option_71_0-option_71_4',
	category:CATEGORIES.PERSONAL,
	text:'option_71_0-option_71_4',
	inputType:'radio',
	comment:'',
	options:['option_71_0', 'option_71_1', 'option_71_2', 'option_71_3', 'option_71_4'],
	nextQuestionMap:['question_71_0', 'question_71_1', 'question_71_2', 'question_71_3', 'question_71_4'],
	scoreMap:[0, 0, 0, 0, 0]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_100',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_100',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['100'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_101',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_101',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['101'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_102',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_102',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['102'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_103',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_103',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['103'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_104',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_104',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['104'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_105',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_105',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['105'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_106',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_106',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['106'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_107',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_107',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['107'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_108',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_108',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['108'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_109',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_109',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['109'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_1010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_1011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1011'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_1012',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1012',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1012'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_1013',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1013',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1013'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_1014',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1014',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1014'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_1015',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1015',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1015'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_1016',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1016',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1016'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_1017',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1017',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1017'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 1
{
	id:'reponse_1018',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1018',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1018'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 1
{
	id:'question_1_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_1_0',
	inputType:'radio',
	comment:'',
	options:['reponse_1_0_0', 'reponse_1_0_1', 'reponse_1_0_2', 'reponse_1_0_3', 'reponse_1_0_4', 'reponse_1_0_5', 'reponse_1_0_6', 'reponse_1_0_7', 'reponse_1_0_8', 'reponse_1_0_9', 'reponse_1_0_10', 'reponse_1_0_11', 'reponse_1_0_12', 'reponse_1_0_13', 'reponse_1_0_14', 'reponse_1_0_15', 'reponse_1_0_16', 'reponse_1_0_17', 'reponse_1_0_18'],
	nextQuestionMap:['reponse_1_0_0', 'reponse_1_0_1', 'reponse_1_0_2', 'reponse_1_0_3', 'reponse_1_0_4', 'reponse_1_0_5', 'reponse_1_0_6', 'reponse_1_0_7', 'reponse_1_0_8', 'reponse_1_0_9', 'reponse_1_0_10', 'reponse_1_0_11', 'reponse_1_0_12', 'reponse_1_0_13', 'reponse_1_0_14', 'reponse_1_0_15', 'reponse_1_0_16', 'reponse_1_0_17', 'reponse_1_0_18'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_200',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_200',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['200'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_201',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_201',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['201'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_202',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_202',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['202'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_203',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_203',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['203'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_204',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_204',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['204'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_205',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_205',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['205'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_206',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_206',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['206'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_207',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_207',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['207'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_208',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_208',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['208'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_209',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_209',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['209'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_2010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_2011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2011'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_2012',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2012',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2012'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_2013',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2013',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2013'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_2014',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2014',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2014'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_2015',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2015',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2015'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_2016',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2016',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2016'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_2017',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2017',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2017'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_2018',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2018',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2018'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_2019',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2019',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2019'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_2020',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2020',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2020'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 2
{
	id:'reponse_2021',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2021',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2021'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 2
{
	id:'question_2_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_2_0',
	inputType:'radio',
	comment:'',
	options:['reponse_2_0_0', 'reponse_2_0_1', 'reponse_2_0_2', 'reponse_2_0_3', 'reponse_2_0_4', 'reponse_2_0_5', 'reponse_2_0_6', 'reponse_2_0_7', 'reponse_2_0_8', 'reponse_2_0_9', 'reponse_2_0_10', 'reponse_2_0_11', 'reponse_2_0_12', 'reponse_2_0_13', 'reponse_2_0_14', 'reponse_2_0_15', 'reponse_2_0_16', 'reponse_2_0_17', 'reponse_2_0_18', 'reponse_2_0_19', 'reponse_2_0_20', 'reponse_2_0_21'],
	nextQuestionMap:['reponse_2_0_0', 'reponse_2_0_1', 'reponse_2_0_2', 'reponse_2_0_3', 'reponse_2_0_4', 'reponse_2_0_5', 'reponse_2_0_6', 'reponse_2_0_7', 'reponse_2_0_8', 'reponse_2_0_9', 'reponse_2_0_10', 'reponse_2_0_11', 'reponse_2_0_12', 'reponse_2_0_13', 'reponse_2_0_14', 'reponse_2_0_15', 'reponse_2_0_16', 'reponse_2_0_17', 'reponse_2_0_18', 'reponse_2_0_19', 'reponse_2_0_20', 'reponse_2_0_21'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 3
{
	id:'reponse_300',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_300',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['300'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 3
{
	id:'reponse_301',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_301',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['301'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 3
{
	id:'reponse_302',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_302',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['302'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 3
{
	id:'reponse_303',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_303',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['303'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 3
{
	id:'reponse_304',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_304',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['304'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 3
{
	id:'question_3_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_3_0',
	inputType:'radio',
	comment:'',
	options:['reponse_3_0_0', 'reponse_3_0_1', 'reponse_3_0_2', 'reponse_3_0_3', 'reponse_3_0_4'],
	nextQuestionMap:['reponse_3_0_0', 'reponse_3_0_1', 'reponse_3_0_2', 'reponse_3_0_3', 'reponse_3_0_4'],
	scoreMap:[1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 4
{
	id:'reponse_400',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_400',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['400'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 4
{
	id:'reponse_401',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_401',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['401'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 4
{
	id:'reponse_402',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_402',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['402'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 4
{
	id:'reponse_403',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_403',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['403'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 4
{
	id:'reponse_404',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_404',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['404'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 4
{
	id:'reponse_405',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_405',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['405'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 4
{
	id:'reponse_406',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_406',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['406'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 4
{
	id:'reponse_407',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_407',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['407'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 4
{
	id:'question_4_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_4_0',
	inputType:'radio',
	comment:'',
	options:['reponse_4_0_0', 'reponse_4_0_1', 'reponse_4_0_2', 'reponse_4_0_3', 'reponse_4_0_4', 'reponse_4_0_5', 'reponse_4_0_6', 'reponse_4_0_7'],
	nextQuestionMap:['reponse_4_0_0', 'reponse_4_0_1', 'reponse_4_0_2', 'reponse_4_0_3', 'reponse_4_0_4', 'reponse_4_0_5', 'reponse_4_0_6', 'reponse_4_0_7'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_500',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_500',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['500'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_501',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_501',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['501'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_502',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_502',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['502'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_503',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_503',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['503'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_504',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_504',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['504'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_505',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_505',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['505'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_506',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_506',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['506'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_507',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_507',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['507'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_508',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_508',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['508'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_509',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_509',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['509'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_5010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_5011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5011'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_5012',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5012',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5012'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_5013',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5013',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5013'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_5014',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5014',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5014'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_5015',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5015',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5015'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 5
{
	id:'reponse_5016',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5016',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5016'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 5
{
	id:'question_5_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_5_0',
	inputType:'radio',
	comment:'',
	options:['reponse_5_0_0', 'reponse_5_0_1', 'reponse_5_0_2', 'reponse_5_0_3', 'reponse_5_0_4', 'reponse_5_0_5', 'reponse_5_0_6', 'reponse_5_0_7', 'reponse_5_0_8', 'reponse_5_0_9', 'reponse_5_0_10', 'reponse_5_0_11', 'reponse_5_0_12', 'reponse_5_0_13', 'reponse_5_0_14', 'reponse_5_0_15', 'reponse_5_0_16'],
	nextQuestionMap:['reponse_5_0_0', 'reponse_5_0_1', 'reponse_5_0_2', 'reponse_5_0_3', 'reponse_5_0_4', 'reponse_5_0_5', 'reponse_5_0_6', 'reponse_5_0_7', 'reponse_5_0_8', 'reponse_5_0_9', 'reponse_5_0_10', 'reponse_5_0_11', 'reponse_5_0_12', 'reponse_5_0_13', 'reponse_5_0_14', 'reponse_5_0_15', 'reponse_5_0_16'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 6
{
	id:'reponse_600',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_600',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['600'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 6
{
	id:'reponse_601',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_601',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['601'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 6
{
	id:'reponse_602',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_602',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['602'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 6
{
	id:'reponse_603',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_603',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['603'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 6
{
	id:'question_6_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_6_0',
	inputType:'radio',
	comment:'',
	options:['reponse_6_0_0', 'reponse_6_0_1', 'reponse_6_0_2', 'reponse_6_0_3'],
	nextQuestionMap:['reponse_6_0_0', 'reponse_6_0_1', 'reponse_6_0_2', 'reponse_6_0_3'],
	scoreMap:[1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 7
{
	id:'reponse_700',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_700',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['700'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 7
{
	id:'reponse_701',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_701',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['701'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 7
{
	id:'reponse_702',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_702',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['702'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 7
{
	id:'reponse_703',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_703',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['703'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 7
{
	id:'reponse_704',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_704',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['704'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 7
{
	id:'reponse_705',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_705',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['705'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 7
{
	id:'reponse_706',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_706',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['706'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 7
{
	id:'question_7_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_7_0',
	inputType:'radio',
	comment:'',
	options:['reponse_7_0_0', 'reponse_7_0_1', 'reponse_7_0_2', 'reponse_7_0_3', 'reponse_7_0_4', 'reponse_7_0_5', 'reponse_7_0_6'],
	nextQuestionMap:['reponse_7_0_0', 'reponse_7_0_1', 'reponse_7_0_2', 'reponse_7_0_3', 'reponse_7_0_4', 'reponse_7_0_5', 'reponse_7_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 8
{
	id:'reponse_800',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_800',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['800'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 8
{
	id:'reponse_801',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_801',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['801'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 8
{
	id:'reponse_802',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_802',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['802'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 8
{
	id:'reponse_803',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_803',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['803'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 8
{
	id:'reponse_804',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_804',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['804'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 8
{
	id:'reponse_805',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_805',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['805'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 8
{
	id:'reponse_806',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_806',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['806'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 8
{
	id:'question_8_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_8_0',
	inputType:'radio',
	comment:'',
	options:['reponse_8_0_0', 'reponse_8_0_1', 'reponse_8_0_2', 'reponse_8_0_3', 'reponse_8_0_4', 'reponse_8_0_5', 'reponse_8_0_6'],
	nextQuestionMap:['reponse_8_0_0', 'reponse_8_0_1', 'reponse_8_0_2', 'reponse_8_0_3', 'reponse_8_0_4', 'reponse_8_0_5', 'reponse_8_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_900',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_900',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['900'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_901',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_901',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['901'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_902',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_902',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['902'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_903',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_903',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['903'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_904',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_904',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['904'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_905',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_905',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['905'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_906',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_906',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['906'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 9
{
	id:'question_9_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_9_0',
	inputType:'radio',
	comment:'',
	options:['reponse_9_0_0', 'reponse_9_0_1', 'reponse_9_0_2', 'reponse_9_0_3', 'reponse_9_0_4', 'reponse_9_0_5', 'reponse_9_0_6'],
	nextQuestionMap:['reponse_9_0_0', 'reponse_9_0_1', 'reponse_9_0_2', 'reponse_9_0_3', 'reponse_9_0_4', 'reponse_9_0_5', 'reponse_9_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_910',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_910',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['910'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_911',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_911',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['911'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 9
{
	id:'question_9_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_9_1',
	inputType:'radio',
	comment:'',
	options:['reponse_9_1_0', 'reponse_9_1_1'],
	nextQuestionMap:['reponse_9_1_0', 'reponse_9_1_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_920',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_920',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['920'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_921',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_921',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['921'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_922',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_922',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['922'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 9
{
	id:'reponse_923',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_923',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['923'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 9
{
	id:'question_9_2',
	category:CATEGORIES.SYMPTOMS,
	text:'question_9_2',
	inputType:'radio',
	comment:'',
	options:['reponse_9_2_0', 'reponse_9_2_1', 'reponse_9_2_2', 'reponse_9_2_3'],
	nextQuestionMap:['reponse_9_2_0', 'reponse_9_2_1', 'reponse_9_2_2', 'reponse_9_2_3'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 10
{
	id:'reponse_1000',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1000',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1000'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 10
{
	id:'reponse_1001',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1001',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1001'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 10
{
	id:'reponse_1002',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1002',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1002'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 10
{
	id:'reponse_1003',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1003',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1003'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 10
{
	id:'question_10_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_10_0',
	inputType:'radio',
	comment:'',
	options:['reponse_10_0_0', 'reponse_10_0_1', 'reponse_10_0_2', 'reponse_10_0_3'],
	nextQuestionMap:['reponse_10_0_0', 'reponse_10_0_1', 'reponse_10_0_2', 'reponse_10_0_3'],
	scoreMap:[1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 10
{
	id:'reponse_1010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 10
{
	id:'reponse_1011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1011'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 10
{
	id:'question_10_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_10_1',
	inputType:'radio',
	comment:'',
	options:['reponse_10_1_0', 'reponse_10_1_1'],
	nextQuestionMap:['reponse_10_1_0', 'reponse_10_1_1'],
	scoreMap:[1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 10
{
	id:'reponse_1020',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1020',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1020'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 10
{
	id:'reponse_1021',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1021',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1021'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 10
{
	id:'question_10_2',
	category:CATEGORIES.SYMPTOMS,
	text:'question_10_2',
	inputType:'radio',
	comment:'',
	options:['reponse_10_2_0', 'reponse_10_2_1'],
	nextQuestionMap:['reponse_10_2_0', 'reponse_10_2_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 10
{
	id:'reponse_1030',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1030',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1030'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 10
{
	id:'reponse_1031',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1031',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1031'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 10
{
	id:'reponse_1032',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1032',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1032'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 10
{
	id:'question_10_3',
	category:CATEGORIES.SYMPTOMS,
	text:'question_10_3',
	inputType:'radio',
	comment:'',
	options:['reponse_10_3_0', 'reponse_10_3_1', 'reponse_10_3_2'],
	nextQuestionMap:['reponse_10_3_0', 'reponse_10_3_1', 'reponse_10_3_2'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_1100',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1100',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1100'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_1101',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1101',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1101'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_1102',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1102',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1102'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_1103',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1103',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1103'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_1104',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1104',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1104'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_1105',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1105',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1105'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_1106',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1106',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1106'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_1107',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1107',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1107'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_1108',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1108',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1108'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_1109',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1109',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1109'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_11010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_11010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['11010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_11011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_11011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['11011'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_11012',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_11012',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['11012'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 11
{
	id:'reponse_11013',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_11013',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['11013'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 11
{
	id:'question_11_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_11_0',
	inputType:'radio',
	comment:'',
	options:['reponse_11_0_0', 'reponse_11_0_1', 'reponse_11_0_2', 'reponse_11_0_3', 'reponse_11_0_4', 'reponse_11_0_5', 'reponse_11_0_6', 'reponse_11_0_7', 'reponse_11_0_8', 'reponse_11_0_9', 'reponse_11_0_10', 'reponse_11_0_11', 'reponse_11_0_12', 'reponse_11_0_13'],
	nextQuestionMap:['reponse_11_0_0', 'reponse_11_0_1', 'reponse_11_0_2', 'reponse_11_0_3', 'reponse_11_0_4', 'reponse_11_0_5', 'reponse_11_0_6', 'reponse_11_0_7', 'reponse_11_0_8', 'reponse_11_0_9', 'reponse_11_0_10', 'reponse_11_0_11', 'reponse_11_0_12', 'reponse_11_0_13'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 12
{
	id:'reponse_1200',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1200',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1200'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 12
{
	id:'reponse_1201',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1201',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1201'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 12
{
	id:'reponse_1202',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1202',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1202'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 12
{
	id:'reponse_1203',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1203',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1203'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 12
{
	id:'reponse_1204',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1204',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1204'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 12
{
	id:'reponse_1205',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1205',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1205'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 12
{
	id:'reponse_1206',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1206',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1206'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 12
{
	id:'reponse_1207',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1207',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1207'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 12
{
	id:'reponse_1208',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1208',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1208'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 12
{
	id:'question_12_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_12_0',
	inputType:'radio',
	comment:'comment_12_0',
	options:['reponse_12_0_0', 'reponse_12_0_1', 'reponse_12_0_2', 'reponse_12_0_3', 'reponse_12_0_4', 'reponse_12_0_5', 'reponse_12_0_6', 'reponse_12_0_7', 'reponse_12_0_8'],
	nextQuestionMap:['reponse_12_0_0', 'reponse_12_0_1', 'reponse_12_0_2', 'reponse_12_0_3', 'reponse_12_0_4', 'reponse_12_0_5', 'reponse_12_0_6', 'reponse_12_0_7', 'reponse_12_0_8'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 13
{
	id:'reponse_1300',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1300',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1300'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 13
{
	id:'reponse_1301',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1301',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1301'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 13
{
	id:'question_13_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_13_0',
	inputType:'radio',
	comment:'',
	options:['reponse_13_0_0', 'reponse_13_0_1'],
	nextQuestionMap:['reponse_13_0_0', 'reponse_13_0_1'],
	scoreMap:[1, 1]
},
//PARTIE REPONSE SUMMARY 14
{
	id:'reponse_1400',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1400',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1400'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 14
{
	id:'reponse_1401',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1401',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1401'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 14
{
	id:'reponse_1402',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1402',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1402'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 14
{
	id:'question_14_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_14_0',
	inputType:'radio',
	comment:'comment_14_0',
	options:['reponse_14_0_0', 'reponse_14_0_1', 'reponse_14_0_2'],
	nextQuestionMap:['reponse_14_0_0', 'reponse_14_0_1', 'reponse_14_0_2'],
	scoreMap:[1, 1, 1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_1500',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1500',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1500'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_1501',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1501',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1501'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_1502',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1502',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1502'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_1503',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1503',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1503'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_1504',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1504',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1504'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_1505',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1505',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1505'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_1506',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1506',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1506'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_1507',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1507',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1507'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_1508',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1508',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1508'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_1509',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1509',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1509'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_15010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_15010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['15010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_15011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_15011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['15011'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_15012',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_15012',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['15012'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_15013',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_15013',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['15013'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_15014',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_15014',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['15014'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_15015',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_15015',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['15015'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_15016',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_15016',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['15016'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_15017',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_15017',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['15017'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_15018',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_15018',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['15018'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_15019',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_15019',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['15019'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 15
{
	id:'reponse_15020',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_15020',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['15020'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 15
{
	id:'question_15_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_15_0',
	inputType:'radio',
	comment:'',
	options:['reponse_15_0_0', 'reponse_15_0_1', 'reponse_15_0_2', 'reponse_15_0_3', 'reponse_15_0_4', 'reponse_15_0_5', 'reponse_15_0_6', 'reponse_15_0_7', 'reponse_15_0_8', 'reponse_15_0_9', 'reponse_15_0_10', 'reponse_15_0_11', 'reponse_15_0_12', 'reponse_15_0_13', 'reponse_15_0_14', 'reponse_15_0_15', 'reponse_15_0_16', 'reponse_15_0_17', 'reponse_15_0_18', 'reponse_15_0_19', 'reponse_15_0_20'],
	nextQuestionMap:['reponse_15_0_0', 'reponse_15_0_1', 'reponse_15_0_2', 'reponse_15_0_3', 'reponse_15_0_4', 'reponse_15_0_5', 'reponse_15_0_6', 'reponse_15_0_7', 'reponse_15_0_8', 'reponse_15_0_9', 'reponse_15_0_10', 'reponse_15_0_11', 'reponse_15_0_12', 'reponse_15_0_13', 'reponse_15_0_14', 'reponse_15_0_15', 'reponse_15_0_16', 'reponse_15_0_17', 'reponse_15_0_18', 'reponse_15_0_19', 'reponse_15_0_20'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 16
{
	id:'reponse_1600',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1600',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1600'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 16
{
	id:'reponse_1601',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1601',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1601'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 16
{
	id:'reponse_1602',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1602',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1602'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 16
{
	id:'question_16_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_16_0',
	inputType:'radio',
	comment:'',
	options:['reponse_16_0_0', 'reponse_16_0_1', 'reponse_16_0_2'],
	nextQuestionMap:['reponse_16_0_0', 'reponse_16_0_1', 'reponse_16_0_2'],
	scoreMap:[1, 1, 1]
},
//PARTIE REPONSE SUMMARY 17
{
	id:'reponse_1700',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1700',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1700'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 17
{
	id:'reponse_1701',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1701',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1701'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 17
{
	id:'reponse_1702',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1702',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1702'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 17
{
	id:'reponse_1703',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1703',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1703'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 17
{
	id:'reponse_1704',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1704',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1704'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 17
{
	id:'reponse_1705',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1705',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1705'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 17
{
	id:'reponse_1706',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1706',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1706'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 17
{
	id:'question_17_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_17_0',
	inputType:'radio',
	comment:'',
	options:['reponse_17_0_0', 'reponse_17_0_1', 'reponse_17_0_2', 'reponse_17_0_3', 'reponse_17_0_4', 'reponse_17_0_5', 'reponse_17_0_6'],
	nextQuestionMap:['reponse_17_0_0', 'reponse_17_0_1', 'reponse_17_0_2', 'reponse_17_0_3', 'reponse_17_0_4', 'reponse_17_0_5', 'reponse_17_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 18
{
	id:'reponse_1800',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1800',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1800'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 18
{
	id:'reponse_1801',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1801',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1801'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 18
{
	id:'reponse_1802',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1802',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1802'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 18
{
	id:'reponse_1803',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1803',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1803'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 18
{
	id:'question_18_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_18_0',
	inputType:'radio',
	comment:'',
	options:['reponse_18_0_0', 'reponse_18_0_1', 'reponse_18_0_2', 'reponse_18_0_3'],
	nextQuestionMap:['reponse_18_0_0', 'reponse_18_0_1', 'reponse_18_0_2', 'reponse_18_0_3'],
	scoreMap:[1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 19
{
	id:'reponse_1900',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1900',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1900'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 19
{
	id:'reponse_1901',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1901',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1901'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 19
{
	id:'reponse_1902',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1902',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1902'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 19
{
	id:'reponse_1903',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1903',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1903'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 19
{
	id:'reponse_1904',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1904',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1904'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 19
{
	id:'reponse_1905',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1905',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1905'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 19
{
	id:'reponse_1906',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1906',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1906'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 19
{
	id:'reponse_1907',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1907',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1907'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 19
{
	id:'reponse_1908',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1908',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1908'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 19
{
	id:'reponse_1909',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_1909',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['1909'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 19
{
	id:'reponse_19010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_19010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['19010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 19
{
	id:'reponse_19011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_19011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['19011'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 19
{
	id:'question_19_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_19_0',
	inputType:'radio',
	comment:'',
	options:['reponse_19_0_0', 'reponse_19_0_1', 'reponse_19_0_2', 'reponse_19_0_3', 'reponse_19_0_4', 'reponse_19_0_5', 'reponse_19_0_6', 'reponse_19_0_7', 'reponse_19_0_8', 'reponse_19_0_9', 'reponse_19_0_10', 'reponse_19_0_11'],
	nextQuestionMap:['reponse_19_0_0', 'reponse_19_0_1', 'reponse_19_0_2', 'reponse_19_0_3', 'reponse_19_0_4', 'reponse_19_0_5', 'reponse_19_0_6', 'reponse_19_0_7', 'reponse_19_0_8', 'reponse_19_0_9', 'reponse_19_0_10', 'reponse_19_0_11'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_2000',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2000',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2000'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_2001',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2001',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2001'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_2002',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2002',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2002'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_2003',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2003',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2003'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_2004',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2004',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2004'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_2005',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2005',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2005'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_2006',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2006',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2006'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_2007',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2007',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2007'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_2008',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2008',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2008'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_2009',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2009',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2009'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_20010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_20010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['20010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_20011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_20011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['20011'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_20012',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_20012',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['20012'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_20013',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_20013',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['20013'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 20
{
	id:'reponse_20014',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_20014',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['20014'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 20
{
	id:'question_20_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_20_0',
	inputType:'radio',
	comment:'',
	options:['reponse_20_0_0', 'reponse_20_0_1', 'reponse_20_0_2', 'reponse_20_0_3', 'reponse_20_0_4', 'reponse_20_0_5', 'reponse_20_0_6', 'reponse_20_0_7', 'reponse_20_0_8', 'reponse_20_0_9', 'reponse_20_0_10', 'reponse_20_0_11', 'reponse_20_0_12', 'reponse_20_0_13', 'reponse_20_0_14'],
	nextQuestionMap:['reponse_20_0_0', 'reponse_20_0_1', 'reponse_20_0_2', 'reponse_20_0_3', 'reponse_20_0_4', 'reponse_20_0_5', 'reponse_20_0_6', 'reponse_20_0_7', 'reponse_20_0_8', 'reponse_20_0_9', 'reponse_20_0_10', 'reponse_20_0_11', 'reponse_20_0_12', 'reponse_20_0_13', 'reponse_20_0_14'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 23
{
	id:'reponse_2300',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2300',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2300'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 23
{
	id:'reponse_2301',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2301',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2301'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 23
{
	id:'reponse_2302',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2302',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2302'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 23
{
	id:'reponse_2303',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2303',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2303'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 23
{
	id:'reponse_2304',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2304',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2304'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 23
{
	id:'reponse_2305',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2305',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2305'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 23
{
	id:'question_23_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_23_0',
	inputType:'radio',
	comment:'',
	options:['reponse_23_0_0', 'reponse_23_0_1', 'reponse_23_0_2', 'reponse_23_0_3', 'reponse_23_0_4', 'reponse_23_0_5'],
	nextQuestionMap:['reponse_23_0_0', 'reponse_23_0_1', 'reponse_23_0_2', 'reponse_23_0_3', 'reponse_23_0_4', 'reponse_23_0_5'],
	scoreMap:[1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2400',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2400',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2400'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2401',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2401',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2401'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2402',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2402',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2402'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2403',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2403',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2403'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2404',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2404',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2404'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2405',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2405',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2405'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2406',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2406',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2406'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2407',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2407',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2407'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2408',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2408',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2408'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 24
{
	id:'question_24_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_24_0',
	inputType:'radio',
	comment:'',
	options:['reponse_24_0_0', 'reponse_24_0_1', 'reponse_24_0_2', 'reponse_24_0_3', 'reponse_24_0_4', 'reponse_24_0_5', 'reponse_24_0_6', 'reponse_24_0_7', 'reponse_24_0_8'],
	nextQuestionMap:['reponse_24_0_0', 'reponse_24_0_1', 'reponse_24_0_2', 'reponse_24_0_3', 'reponse_24_0_4', 'reponse_24_0_5', 'reponse_24_0_6', 'reponse_24_0_7', 'reponse_24_0_8'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2410',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2410',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2410'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2411',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2411',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2411'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2412',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2412',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2412'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2413',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2413',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2413'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2414',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2414',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2414'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2415',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2415',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2415'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2416',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2416',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2416'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2417',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2417',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2417'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 24
{
	id:'question_24_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_24_1',
	inputType:'radio',
	comment:'',
	options:['reponse_24_1_0', 'reponse_24_1_1', 'reponse_24_1_2', 'reponse_24_1_3', 'reponse_24_1_4', 'reponse_24_1_5', 'reponse_24_1_6', 'reponse_24_1_7'],
	nextQuestionMap:['reponse_24_1_0', 'reponse_24_1_1', 'reponse_24_1_2', 'reponse_24_1_3', 'reponse_24_1_4', 'reponse_24_1_5', 'reponse_24_1_6', 'reponse_24_1_7'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2420',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2420',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2420'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2421',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2421',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2421'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2422',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2422',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2422'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 24
{
	id:'reponse_2423',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2423',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2423'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 24
{
	id:'question_24_2',
	category:CATEGORIES.SYMPTOMS,
	text:'question_24_2',
	inputType:'radio',
	comment:'comment_24_2',
	options:['reponse_24_2_0', 'reponse_24_2_1', 'reponse_24_2_2', 'reponse_24_2_3'],
	nextQuestionMap:['reponse_24_2_0', 'reponse_24_2_1', 'reponse_24_2_2', 'reponse_24_2_3'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 25
{
	id:'reponse_2500',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2500',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2500'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 25
{
	id:'reponse_2501',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2501',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2501'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 25
{
	id:'reponse_2502',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2502',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2502'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 25
{
	id:'reponse_2503',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2503',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2503'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 25
{
	id:'reponse_2504',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2504',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2504'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 25
{
	id:'reponse_2505',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2505',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2505'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 25
{
	id:'reponse_2506',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2506',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2506'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 25
{
	id:'reponse_2507',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2507',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2507'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 25
{
	id:'reponse_2508',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2508',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2508'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 25
{
	id:'reponse_2509',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2509',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2509'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 25
{
	id:'question_25_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_25_0',
	inputType:'radio',
	comment:'',
	options:['reponse_25_0_0', 'reponse_25_0_1', 'reponse_25_0_2', 'reponse_25_0_3', 'reponse_25_0_4', 'reponse_25_0_5', 'reponse_25_0_6', 'reponse_25_0_7', 'reponse_25_0_8', 'reponse_25_0_9'],
	nextQuestionMap:['reponse_25_0_0', 'reponse_25_0_1', 'reponse_25_0_2', 'reponse_25_0_3', 'reponse_25_0_4', 'reponse_25_0_5', 'reponse_25_0_6', 'reponse_25_0_7', 'reponse_25_0_8', 'reponse_25_0_9'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_2600',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2600',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2600'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_2601',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2601',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2601'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_2602',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2602',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2602'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_2603',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2603',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2603'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_2604',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2604',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2604'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_2605',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2605',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2605'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_2606',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2606',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2606'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_2607',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2607',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2607'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_2608',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2608',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2608'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_2609',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2609',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2609'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_26010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_26010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['26010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_26011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_26011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['26011'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 26
{
	id:'reponse_26012',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_26012',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['26012'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 26
{
	id:'question_26_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_26_0',
	inputType:'radio',
	comment:'',
	options:['reponse_26_0_0', 'reponse_26_0_1', 'reponse_26_0_2', 'reponse_26_0_3', 'reponse_26_0_4', 'reponse_26_0_5', 'reponse_26_0_6', 'reponse_26_0_7', 'reponse_26_0_8', 'reponse_26_0_9', 'reponse_26_0_10', 'reponse_26_0_11', 'reponse_26_0_12'],
	nextQuestionMap:['reponse_26_0_0', 'reponse_26_0_1', 'reponse_26_0_2', 'reponse_26_0_3', 'reponse_26_0_4', 'reponse_26_0_5', 'reponse_26_0_6', 'reponse_26_0_7', 'reponse_26_0_8', 'reponse_26_0_9', 'reponse_26_0_10', 'reponse_26_0_11', 'reponse_26_0_12'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2700',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2700',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2700'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2701',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2701',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2701'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2702',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2702',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2702'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2703',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2703',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2703'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2704',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2704',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2704'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2705',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2705',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2705'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2706',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2706',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2706'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2707',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2707',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2707'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2708',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2708',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2708'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 27
{
	id:'question_27_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_27_0',
	inputType:'radio',
	comment:'comment_27_0',
	options:['reponse_27_0_0', 'reponse_27_0_1', 'reponse_27_0_2', 'reponse_27_0_3', 'reponse_27_0_4', 'reponse_27_0_5', 'reponse_27_0_6', 'reponse_27_0_7', 'reponse_27_0_8'],
	nextQuestionMap:['reponse_27_0_0', 'reponse_27_0_1', 'reponse_27_0_2', 'reponse_27_0_3', 'reponse_27_0_4', 'reponse_27_0_5', 'reponse_27_0_6', 'reponse_27_0_7', 'reponse_27_0_8'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2710',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2710',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2710'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2711',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2711',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2711'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2712',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2712',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2712'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 27
{
	id:'reponse_2713',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2713',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2713'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 27
{
	id:'question_27_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_27_1',
	inputType:'radio',
	comment:'comment_27_0',
	options:['reponse_27_1_0', 'reponse_27_1_1', 'reponse_27_1_2', 'reponse_27_1_3'],
	nextQuestionMap:['reponse_27_1_0', 'reponse_27_1_1', 'reponse_27_1_2', 'reponse_27_1_3'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 28
{
	id:'reponse_2800',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2800',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2800'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 28
{
	id:'reponse_2801',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2801',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2801'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 28
{
	id:'reponse_2802',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2802',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2802'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 28
{
	id:'reponse_2803',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2803',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2803'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 28
{
	id:'reponse_2804',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2804',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2804'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 28
{
	id:'reponse_2805',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2805',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2805'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 28
{
	id:'reponse_2806',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2806',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2806'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 28
{
	id:'question_28_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_28_0',
	inputType:'radio',
	comment:'comment_28_0',
	options:['reponse_28_0_0', 'reponse_28_0_1', 'reponse_28_0_2', 'reponse_28_0_3', 'reponse_28_0_4', 'reponse_28_0_5', 'reponse_28_0_6'],
	nextQuestionMap:['reponse_28_0_0', 'reponse_28_0_1', 'reponse_28_0_2', 'reponse_28_0_3', 'reponse_28_0_4', 'reponse_28_0_5', 'reponse_28_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2900',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2900',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2900'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2901',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2901',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2901'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2902',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2902',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2902'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2903',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2903',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2903'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2904',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2904',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2904'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2905',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2905',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2905'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2906',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2906',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2906'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 29
{
	id:'question_29_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_29_0',
	inputType:'radio',
	comment:'',
	options:['reponse_29_0_0', 'reponse_29_0_1', 'reponse_29_0_2', 'reponse_29_0_3', 'reponse_29_0_4', 'reponse_29_0_5', 'reponse_29_0_6'],
	nextQuestionMap:['reponse_29_0_0', 'reponse_29_0_1', 'reponse_29_0_2', 'reponse_29_0_3', 'reponse_29_0_4', 'reponse_29_0_5', 'reponse_29_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2910',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2910',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2910'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2911',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2911',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2911'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2912',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2912',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2912'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2913',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2913',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2913'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2914',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2914',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2914'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 29
{
	id:'question_29_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_29_1',
	inputType:'radio',
	comment:'',
	options:['reponse_29_1_0', 'reponse_29_1_1', 'reponse_29_1_2', 'reponse_29_1_3', 'reponse_29_1_4'],
	nextQuestionMap:['reponse_29_1_0', 'reponse_29_1_1', 'reponse_29_1_2', 'reponse_29_1_3', 'reponse_29_1_4'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2920',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2920',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2920'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2921',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2921',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2921'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2922',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2922',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2922'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2923',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2923',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2923'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2924',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2924',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2924'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2925',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2925',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2925'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2926',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2926',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2926'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 29
{
	id:'question_29_2',
	category:CATEGORIES.SYMPTOMS,
	text:'question_29_2',
	inputType:'radio',
	comment:'',
	options:['reponse_29_2_0', 'reponse_29_2_1', 'reponse_29_2_2', 'reponse_29_2_3', 'reponse_29_2_4', 'reponse_29_2_5', 'reponse_29_2_6'],
	nextQuestionMap:['reponse_29_2_0', 'reponse_29_2_1', 'reponse_29_2_2', 'reponse_29_2_3', 'reponse_29_2_4', 'reponse_29_2_5', 'reponse_29_2_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2930',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2930',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2930'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2931',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2931',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2931'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2932',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2932',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2932'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2933',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2933',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2933'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2934',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2934',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2934'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 29
{
	id:'question_29_3',
	category:CATEGORIES.SYMPTOMS,
	text:'question_29_3',
	inputType:'radio',
	comment:'',
	options:['reponse_29_3_0', 'reponse_29_3_1', 'reponse_29_3_2', 'reponse_29_3_3', 'reponse_29_3_4'],
	nextQuestionMap:['reponse_29_3_0', 'reponse_29_3_1', 'reponse_29_3_2', 'reponse_29_3_3', 'reponse_29_3_4'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2940',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2940',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2940'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2941',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2941',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2941'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2942',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2942',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2942'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 29
{
	id:'question_29_4',
	category:CATEGORIES.SYMPTOMS,
	text:'question_29_4',
	inputType:'radio',
	comment:'',
	options:['reponse_29_4_0', 'reponse_29_4_1', 'reponse_29_4_2'],
	nextQuestionMap:['reponse_29_4_0', 'reponse_29_4_1', 'reponse_29_4_2'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2950',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2950',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2950'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2951',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2951',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2951'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2952',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2952',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2952'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2953',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2953',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2953'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2954',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2954',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2954'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 29
{
	id:'question_29_5',
	category:CATEGORIES.SYMPTOMS,
	text:'question_29_5',
	inputType:'radio',
	comment:'',
	options:['reponse_29_5_0', 'reponse_29_5_1', 'reponse_29_5_2', 'reponse_29_5_3', 'reponse_29_5_4'],
	nextQuestionMap:['reponse_29_5_0', 'reponse_29_5_1', 'reponse_29_5_2', 'reponse_29_5_3', 'reponse_29_5_4'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2960',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2960',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2960'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2961',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2961',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2961'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 29
{
	id:'reponse_2962',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_2962',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['2962'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 29
{
	id:'question_29_6',
	category:CATEGORIES.SYMPTOMS,
	text:'question_29_6',
	inputType:'radio',
	comment:'',
	options:['reponse_29_6_0', 'reponse_29_6_1', 'reponse_29_6_2'],
	nextQuestionMap:['reponse_29_6_0', 'reponse_29_6_1', 'reponse_29_6_2'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 30
{
	id:'reponse_3000',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3000',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3000'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 30
{
	id:'reponse_3001',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3001',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3001'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 30
{
	id:'reponse_3002',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3002',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3002'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 30
{
	id:'reponse_3003',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3003',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3003'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 30
{
	id:'reponse_3004',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3004',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3004'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 30
{
	id:'reponse_3005',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3005',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3005'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 30
{
	id:'reponse_3006',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3006',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3006'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 30
{
	id:'reponse_3007',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3007',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3007'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 30
{
	id:'question_30_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_30_0',
	inputType:'radio',
	comment:'',
	options:['reponse_30_0_0', 'reponse_30_0_1', 'reponse_30_0_2', 'reponse_30_0_3', 'reponse_30_0_4', 'reponse_30_0_5', 'reponse_30_0_6', 'reponse_30_0_7'],
	nextQuestionMap:['reponse_30_0_0', 'reponse_30_0_1', 'reponse_30_0_2', 'reponse_30_0_3', 'reponse_30_0_4', 'reponse_30_0_5', 'reponse_30_0_6', 'reponse_30_0_7'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3100',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3100',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3100'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3101',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3101',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3101'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3102',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3102',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3102'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3103',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3103',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3103'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3104',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3104',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3104'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 31
{
	id:'question_31_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_31_0',
	inputType:'radio',
	comment:'',
	options:['reponse_31_0_0', 'reponse_31_0_1', 'reponse_31_0_2', 'reponse_31_0_3', 'reponse_31_0_4'],
	nextQuestionMap:['reponse_31_0_0', 'reponse_31_0_1', 'reponse_31_0_2', 'reponse_31_0_3', 'reponse_31_0_4'],
	scoreMap:[1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3110',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3110',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3110'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3111',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3111',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3111'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3112',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3112',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3112'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3113',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3113',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3113'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3114',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3114',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3114'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3115',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3115',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3115'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3116',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3116',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3116'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3117',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3117',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3117'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3118',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3118',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3118'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3119',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3119',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3119'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_31110',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_31110',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['31110'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_31111',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_31111',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['31111'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_31112',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_31112',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['31112'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 31
{
	id:'question_31_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_31_1',
	inputType:'radio',
	comment:'',
	options:['reponse_31_1_0', 'reponse_31_1_1', 'reponse_31_1_2', 'reponse_31_1_3', 'reponse_31_1_4', 'reponse_31_1_5', 'reponse_31_1_6', 'reponse_31_1_7', 'reponse_31_1_8', 'reponse_31_1_9', 'reponse_31_1_10', 'reponse_31_1_11', 'reponse_31_1_12'],
	nextQuestionMap:['reponse_31_1_0', 'reponse_31_1_1', 'reponse_31_1_2', 'reponse_31_1_3', 'reponse_31_1_4', 'reponse_31_1_5', 'reponse_31_1_6', 'reponse_31_1_7', 'reponse_31_1_8', 'reponse_31_1_9', 'reponse_31_1_10', 'reponse_31_1_11', 'reponse_31_1_12'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3120',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3120',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3120'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3121',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3121',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3121'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3122',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3122',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3122'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3123',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3123',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3123'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3124',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3124',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3124'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 31
{
	id:'reponse_3125',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3125',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3125'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 31
{
	id:'question_31_2',
	category:CATEGORIES.SYMPTOMS,
	text:'question_31_2',
	inputType:'radio',
	comment:'',
	options:['reponse_31_2_0', 'reponse_31_2_1', 'reponse_31_2_2', 'reponse_31_2_3', 'reponse_31_2_4', 'reponse_31_2_5'],
	nextQuestionMap:['reponse_31_2_0', 'reponse_31_2_1', 'reponse_31_2_2', 'reponse_31_2_3', 'reponse_31_2_4', 'reponse_31_2_5'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_3200',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3200',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3200'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_3201',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3201',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3201'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_3202',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3202',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3202'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_3203',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3203',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3203'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_3204',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3204',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3204'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_3205',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3205',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3205'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_3206',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3206',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3206'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_3207',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3207',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3207'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_3208',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3208',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3208'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_3209',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3209',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3209'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_32010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_32010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['32010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_32011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_32011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['32011'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 32
{
	id:'reponse_32012',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_32012',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['32012'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 32
{
	id:'question_32_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_32_0',
	inputType:'radio',
	comment:'',
	options:['reponse_32_0_0', 'reponse_32_0_1', 'reponse_32_0_2', 'reponse_32_0_3', 'reponse_32_0_4', 'reponse_32_0_5', 'reponse_32_0_6', 'reponse_32_0_7', 'reponse_32_0_8', 'reponse_32_0_9', 'reponse_32_0_10', 'reponse_32_0_11', 'reponse_32_0_12'],
	nextQuestionMap:['reponse_32_0_0', 'reponse_32_0_1', 'reponse_32_0_2', 'reponse_32_0_3', 'reponse_32_0_4', 'reponse_32_0_5', 'reponse_32_0_6', 'reponse_32_0_7', 'reponse_32_0_8', 'reponse_32_0_9', 'reponse_32_0_10', 'reponse_32_0_11', 'reponse_32_0_12'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 34
{
	id:'reponse_3400',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3400',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3400'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 34
{
	id:'reponse_3401',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3401',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3401'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 34
{
	id:'reponse_3402',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3402',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3402'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 34
{
	id:'reponse_3403',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3403',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3403'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 34
{
	id:'reponse_3404',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3404',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3404'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 34
{
	id:'reponse_3405',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3405',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3405'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 34
{
	id:'reponse_3406',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3406',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3406'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 34
{
	id:'reponse_3407',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3407',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3407'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 34
{
	id:'reponse_3408',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3408',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3408'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 34
{
	id:'reponse_3409',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3409',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3409'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 34
{
	id:'reponse_34010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_34010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['34010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 34
{
	id:'reponse_34011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_34011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['34011'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 34
{
	id:'question_34_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_34_0',
	inputType:'radio',
	comment:'',
	options:['reponse_34_0_0', 'reponse_34_0_1', 'reponse_34_0_2', 'reponse_34_0_3', 'reponse_34_0_4', 'reponse_34_0_5', 'reponse_34_0_6', 'reponse_34_0_7', 'reponse_34_0_8', 'reponse_34_0_9', 'reponse_34_0_10', 'reponse_34_0_11'],
	nextQuestionMap:['reponse_34_0_0', 'reponse_34_0_1', 'reponse_34_0_2', 'reponse_34_0_3', 'reponse_34_0_4', 'reponse_34_0_5', 'reponse_34_0_6', 'reponse_34_0_7', 'reponse_34_0_8', 'reponse_34_0_9', 'reponse_34_0_10', 'reponse_34_0_11'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 35
{
	id:'reponse_3500',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3500',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3500'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 35
{
	id:'reponse_3501',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3501',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3501'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 35
{
	id:'reponse_3502',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3502',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3502'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 35
{
	id:'question_35_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_35_0',
	inputType:'radio',
	comment:'',
	options:['reponse_35_0_0', 'reponse_35_0_1', 'reponse_35_0_2'],
	nextQuestionMap:['reponse_35_0_0', 'reponse_35_0_1', 'reponse_35_0_2'],
	scoreMap:[1, 1, 1]
},
//PARTIE REPONSE SUMMARY 36
{
	id:'reponse_3600',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3600',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3600'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 36
{
	id:'reponse_3601',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3601',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3601'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 36
{
	id:'reponse_3602',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3602',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3602'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 36
{
	id:'reponse_3603',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3603',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3603'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 36
{
	id:'reponse_3604',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3604',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3604'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 36
{
	id:'reponse_3605',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3605',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3605'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 36
{
	id:'reponse_3606',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3606',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3606'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 36
{
	id:'reponse_3607',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3607',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3607'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 36
{
	id:'reponse_3608',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3608',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3608'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 36
{
	id:'question_36_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_36_0',
	inputType:'radio',
	comment:'comment_36_0',
	options:['reponse_36_0_0', 'reponse_36_0_1', 'reponse_36_0_2', 'reponse_36_0_3', 'reponse_36_0_4', 'reponse_36_0_5', 'reponse_36_0_6', 'reponse_36_0_7', 'reponse_36_0_8'],
	nextQuestionMap:['reponse_36_0_0', 'reponse_36_0_1', 'reponse_36_0_2', 'reponse_36_0_3', 'reponse_36_0_4', 'reponse_36_0_5', 'reponse_36_0_6', 'reponse_36_0_7', 'reponse_36_0_8'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_3700',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3700',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3700'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_3701',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3701',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3701'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_3702',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3702',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3702'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_3703',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3703',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3703'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_3704',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3704',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3704'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_3705',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3705',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3705'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_3706',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3706',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3706'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_3707',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3707',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3707'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_3708',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3708',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3708'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_3709',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3709',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3709'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_37010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_37010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['37010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_37011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_37011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['37011'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_37012',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_37012',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['37012'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_37013',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_37013',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['37013'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_37014',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_37014',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['37014'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_37015',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_37015',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['37015'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 37
{
	id:'reponse_37016',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_37016',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['37016'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 37
{
	id:'question_37_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_37_0',
	inputType:'radio',
	comment:'',
	options:['reponse_37_0_0', 'reponse_37_0_1', 'reponse_37_0_2', 'reponse_37_0_3', 'reponse_37_0_4', 'reponse_37_0_5', 'reponse_37_0_6', 'reponse_37_0_7', 'reponse_37_0_8', 'reponse_37_0_9', 'reponse_37_0_10', 'reponse_37_0_11', 'reponse_37_0_12', 'reponse_37_0_13', 'reponse_37_0_14', 'reponse_37_0_15', 'reponse_37_0_16'],
	nextQuestionMap:['reponse_37_0_0', 'reponse_37_0_1', 'reponse_37_0_2', 'reponse_37_0_3', 'reponse_37_0_4', 'reponse_37_0_5', 'reponse_37_0_6', 'reponse_37_0_7', 'reponse_37_0_8', 'reponse_37_0_9', 'reponse_37_0_10', 'reponse_37_0_11', 'reponse_37_0_12', 'reponse_37_0_13', 'reponse_37_0_14', 'reponse_37_0_15', 'reponse_37_0_16'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 38
{
	id:'reponse_3800',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3800',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3800'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 38
{
	id:'reponse_3801',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3801',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3801'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 38
{
	id:'reponse_3802',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3802',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3802'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 38
{
	id:'reponse_3803',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3803',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3803'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 38
{
	id:'question_38_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_38_0',
	inputType:'radio',
	comment:'',
	options:['reponse_38_0_0', 'reponse_38_0_1', 'reponse_38_0_2', 'reponse_38_0_3'],
	nextQuestionMap:['reponse_38_0_0', 'reponse_38_0_1', 'reponse_38_0_2', 'reponse_38_0_3'],
	scoreMap:[1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3900',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3900',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3900'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3901',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3901',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3901'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3902',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3902',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3902'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3903',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3903',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3903'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3904',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3904',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3904'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3905',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3905',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3905'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3906',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3906',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3906'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3907',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3907',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3907'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3908',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3908',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3908'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3909',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3909',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3909'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 39
{
	id:'question_39_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_39_0',
	inputType:'radio',
	comment:'',
	options:['reponse_39_0_0', 'reponse_39_0_1', 'reponse_39_0_2', 'reponse_39_0_3', 'reponse_39_0_4', 'reponse_39_0_5', 'reponse_39_0_6', 'reponse_39_0_7', 'reponse_39_0_8', 'reponse_39_0_9'],
	nextQuestionMap:['reponse_39_0_0', 'reponse_39_0_1', 'reponse_39_0_2', 'reponse_39_0_3', 'reponse_39_0_4', 'reponse_39_0_5', 'reponse_39_0_6', 'reponse_39_0_7', 'reponse_39_0_8', 'reponse_39_0_9'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3910',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3910',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3910'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3911',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3911',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3911'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 39
{
	id:'reponse_3912',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_3912',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['3912'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 39
{
	id:'question_39_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_39_1',
	inputType:'radio',
	comment:'',
	options:['reponse_39_1_0', 'reponse_39_1_1', 'reponse_39_1_2'],
	nextQuestionMap:['reponse_39_1_0', 'reponse_39_1_1', 'reponse_39_1_2'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 40
{
	id:'reponse_4000',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4000',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4000'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 40
{
	id:'reponse_4001',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4001',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4001'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 40
{
	id:'reponse_4002',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4002',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4002'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 40
{
	id:'reponse_4003',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4003',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4003'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 40
{
	id:'reponse_4004',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4004',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4004'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 40
{
	id:'reponse_4005',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4005',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4005'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 40
{
	id:'question_40_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_40_0',
	inputType:'radio',
	comment:'',
	options:['reponse_40_0_0', 'reponse_40_0_1', 'reponse_40_0_2', 'reponse_40_0_3', 'reponse_40_0_4', 'reponse_40_0_5'],
	nextQuestionMap:['reponse_40_0_0', 'reponse_40_0_1', 'reponse_40_0_2', 'reponse_40_0_3', 'reponse_40_0_4', 'reponse_40_0_5'],
	scoreMap:[1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 42
{
	id:'reponse_4200',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4200',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4200'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 42
{
	id:'reponse_4201',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4201',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4201'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 42
{
	id:'question_42_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_42_0',
	inputType:'radio',
	comment:'',
	options:['reponse_42_0_0', 'reponse_42_0_1'],
	nextQuestionMap:['reponse_42_0_0', 'reponse_42_0_1'],
	scoreMap:[1, 1]
},
//PARTIE REPONSE SUMMARY 43
{
	id:'reponse_4300',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4300',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4300'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 43
{
	id:'reponse_4301',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4301',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4301'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 43
{
	id:'reponse_4302',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4302',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4302'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 43
{
	id:'reponse_4303',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4303',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4303'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 43
{
	id:'reponse_4304',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4304',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4304'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 43
{
	id:'reponse_4305',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4305',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4305'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 43
{
	id:'question_43_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_43_0',
	inputType:'radio',
	comment:'',
	options:['reponse_43_0_0', 'reponse_43_0_1', 'reponse_43_0_2', 'reponse_43_0_3', 'reponse_43_0_4', 'reponse_43_0_5'],
	nextQuestionMap:['reponse_43_0_0', 'reponse_43_0_1', 'reponse_43_0_2', 'reponse_43_0_3', 'reponse_43_0_4', 'reponse_43_0_5'],
	scoreMap:[1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 44
{
	id:'reponse_4400',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4400',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4400'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 44
{
	id:'reponse_4401',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4401',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4401'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 44
{
	id:'reponse_4402',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4402',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4402'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 44
{
	id:'reponse_4403',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4403',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4403'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 44
{
	id:'reponse_4404',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4404',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4404'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 44
{
	id:'question_44_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_44_0',
	inputType:'radio',
	comment:'',
	options:['reponse_44_0_0', 'reponse_44_0_1', 'reponse_44_0_2', 'reponse_44_0_3', 'reponse_44_0_4'],
	nextQuestionMap:['reponse_44_0_0', 'reponse_44_0_1', 'reponse_44_0_2', 'reponse_44_0_3', 'reponse_44_0_4'],
	scoreMap:[1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 45
{
	id:'reponse_4500',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4500',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4500'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 45
{
	id:'reponse_4501',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4501',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4501'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 45
{
	id:'reponse_4502',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4502',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4502'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 45
{
	id:'reponse_4503',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4503',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4503'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 45
{
	id:'reponse_4504',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4504',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4504'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 45
{
	id:'reponse_4505',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4505',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4505'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 45
{
	id:'reponse_4506',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4506',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4506'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 45
{
	id:'question_45_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_45_0',
	inputType:'radio',
	comment:'',
	options:['reponse_45_0_0', 'reponse_45_0_1', 'reponse_45_0_2', 'reponse_45_0_3', 'reponse_45_0_4', 'reponse_45_0_5', 'reponse_45_0_6'],
	nextQuestionMap:['reponse_45_0_0', 'reponse_45_0_1', 'reponse_45_0_2', 'reponse_45_0_3', 'reponse_45_0_4', 'reponse_45_0_5', 'reponse_45_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4600',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4600',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4600'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4601',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4601',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4601'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 46
{
	id:'question_46_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_46_0',
	inputType:'radio',
	comment:'',
	options:['reponse_46_0_0', 'reponse_46_0_1'],
	nextQuestionMap:['reponse_46_0_0', 'reponse_46_0_1'],
	scoreMap:[1, 1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4610',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4610',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4610'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4611',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4611',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4611'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4612',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4612',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4612'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4613',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4613',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4613'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4614',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4614',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4614'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4615',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4615',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4615'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 46
{
	id:'question_46_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_46_1',
	inputType:'radio',
	comment:'',
	options:['reponse_46_1_0', 'reponse_46_1_1', 'reponse_46_1_2', 'reponse_46_1_3', 'reponse_46_1_4', 'reponse_46_1_5'],
	nextQuestionMap:['reponse_46_1_0', 'reponse_46_1_1', 'reponse_46_1_2', 'reponse_46_1_3', 'reponse_46_1_4', 'reponse_46_1_5'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4620',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4620',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4620'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 46
{
	id:'question_46_2',
	category:CATEGORIES.SYMPTOMS,
	text:'question_46_2',
	inputType:'radio',
	comment:'',
	options:['reponse_46_2_0'],
	nextQuestionMap:['reponse_46_2_0'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4630',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4630',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4630'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4631',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4631',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4631'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 46
{
	id:'question_46_3',
	category:CATEGORIES.SYMPTOMS,
	text:'question_46_3',
	inputType:'radio',
	comment:'',
	options:['reponse_46_3_0', 'reponse_46_3_1'],
	nextQuestionMap:['reponse_46_3_0', 'reponse_46_3_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4640',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4640',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4640'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4641',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4641',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4641'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4642',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4642',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4642'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 46
{
	id:'question_46_4',
	category:CATEGORIES.SYMPTOMS,
	text:'question_46_4',
	inputType:'radio',
	comment:'',
	options:['reponse_46_4_0', 'reponse_46_4_1', 'reponse_46_4_2'],
	nextQuestionMap:['reponse_46_4_0', 'reponse_46_4_1', 'reponse_46_4_2'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4650',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4650',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4650'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4651',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4651',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4651'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4652',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4652',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4652'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4653',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4653',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4653'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4654',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4654',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4654'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4655',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4655',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4655'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4656',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4656',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4656'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4657',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4657',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4657'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4658',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4658',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4658'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_4659',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4659',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4659'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 46
{
	id:'reponse_46510',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_46510',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['46510'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 46
{
	id:'question_46_5',
	category:CATEGORIES.SYMPTOMS,
	text:'question_46_5',
	inputType:'radio',
	comment:'',
	options:['reponse_46_5_0', 'reponse_46_5_1', 'reponse_46_5_2', 'reponse_46_5_3', 'reponse_46_5_4', 'reponse_46_5_5', 'reponse_46_5_6', 'reponse_46_5_7', 'reponse_46_5_8', 'reponse_46_5_9', 'reponse_46_5_10'],
	nextQuestionMap:['reponse_46_5_0', 'reponse_46_5_1', 'reponse_46_5_2', 'reponse_46_5_3', 'reponse_46_5_4', 'reponse_46_5_5', 'reponse_46_5_6', 'reponse_46_5_7', 'reponse_46_5_8', 'reponse_46_5_9', 'reponse_46_5_10'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 47
{
	id:'reponse_4700',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4700',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4700'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 47
{
	id:'reponse_4701',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4701',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4701'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 47
{
	id:'reponse_4702',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4702',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4702'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 47
{
	id:'reponse_4703',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4703',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4703'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 47
{
	id:'reponse_4704',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4704',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4704'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 47
{
	id:'reponse_4705',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4705',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4705'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 47
{
	id:'reponse_4706',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4706',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4706'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 47
{
	id:'reponse_4707',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4707',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4707'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 47
{
	id:'reponse_4708',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4708',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4708'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 47
{
	id:'reponse_4709',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4709',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4709'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 47
{
	id:'reponse_47010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_47010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['47010'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 47
{
	id:'question_47_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_47_0',
	inputType:'radio',
	comment:'',
	options:['reponse_47_0_0', 'reponse_47_0_1', 'reponse_47_0_2', 'reponse_47_0_3', 'reponse_47_0_4', 'reponse_47_0_5', 'reponse_47_0_6', 'reponse_47_0_7', 'reponse_47_0_8', 'reponse_47_0_9', 'reponse_47_0_10'],
	nextQuestionMap:['reponse_47_0_0', 'reponse_47_0_1', 'reponse_47_0_2', 'reponse_47_0_3', 'reponse_47_0_4', 'reponse_47_0_5', 'reponse_47_0_6', 'reponse_47_0_7', 'reponse_47_0_8', 'reponse_47_0_9', 'reponse_47_0_10'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4800',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4800',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4800'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4801',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4801',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4801'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4802',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4802',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4802'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4803',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4803',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4803'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4804',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4804',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4804'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4805',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4805',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4805'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4806',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4806',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4806'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 48
{
	id:'question_48_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_48_0',
	inputType:'radio',
	comment:'',
	options:['reponse_48_0_0', 'reponse_48_0_1', 'reponse_48_0_2', 'reponse_48_0_3', 'reponse_48_0_4', 'reponse_48_0_5', 'reponse_48_0_6'],
	nextQuestionMap:['reponse_48_0_0', 'reponse_48_0_1', 'reponse_48_0_2', 'reponse_48_0_3', 'reponse_48_0_4', 'reponse_48_0_5', 'reponse_48_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4810',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4810',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4810'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4811',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4811',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4811'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 48
{
	id:'question_48_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_48_1',
	inputType:'radio',
	comment:'',
	options:['reponse_48_1_0', 'reponse_48_1_1'],
	nextQuestionMap:['reponse_48_1_0', 'reponse_48_1_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4820',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4820',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4820'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4821',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4821',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4821'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4822',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4822',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4822'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 48
{
	id:'question_48_2',
	category:CATEGORIES.SYMPTOMS,
	text:'question_48_2',
	inputType:'radio',
	comment:'',
	options:['reponse_48_2_0', 'reponse_48_2_1', 'reponse_48_2_2'],
	nextQuestionMap:['reponse_48_2_0', 'reponse_48_2_1', 'reponse_48_2_2'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4830',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4830',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4830'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4831',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4831',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4831'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4832',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4832',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4832'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 48
{
	id:'question_48_3',
	category:CATEGORIES.SYMPTOMS,
	text:'question_48_3',
	inputType:'radio',
	comment:'',
	options:['reponse_48_3_0', 'reponse_48_3_1', 'reponse_48_3_2'],
	nextQuestionMap:['reponse_48_3_0', 'reponse_48_3_1', 'reponse_48_3_2'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4840',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4840',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4840'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4841',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4841',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4841'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4842',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4842',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4842'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 48
{
	id:'question_48_4',
	category:CATEGORIES.SYMPTOMS,
	text:'question_48_4',
	inputType:'radio',
	comment:'',
	options:['reponse_48_4_0', 'reponse_48_4_1', 'reponse_48_4_2'],
	nextQuestionMap:['reponse_48_4_0', 'reponse_48_4_1', 'reponse_48_4_2'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4850',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4850',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4850'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4851',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4851',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4851'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4852',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4852',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4852'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 48
{
	id:'question_48_5',
	category:CATEGORIES.SYMPTOMS,
	text:'question_48_5',
	inputType:'radio',
	comment:'',
	options:['reponse_48_5_0', 'reponse_48_5_1', 'reponse_48_5_2'],
	nextQuestionMap:['reponse_48_5_0', 'reponse_48_5_1', 'reponse_48_5_2'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4860',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4860',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4860'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4861',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4861',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4861'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4862',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4862',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4862'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 48
{
	id:'question_48_6',
	category:CATEGORIES.SYMPTOMS,
	text:'question_48_6',
	inputType:'radio',
	comment:'',
	options:['reponse_48_6_0', 'reponse_48_6_1', 'reponse_48_6_2'],
	nextQuestionMap:['reponse_48_6_0', 'reponse_48_6_1', 'reponse_48_6_2'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4870',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4870',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4870'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 48
{
	id:'reponse_4871',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4871',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4871'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 48
{
	id:'question_48_7',
	category:CATEGORIES.SYMPTOMS,
	text:'question_48_7',
	inputType:'radio',
	comment:'',
	options:['reponse_48_7_0', 'reponse_48_7_1'],
	nextQuestionMap:['reponse_48_7_0', 'reponse_48_7_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_4900',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4900',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4900'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_4901',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4901',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4901'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_4902',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4902',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4902'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_4903',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4903',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4903'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_4904',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4904',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4904'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_4905',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4905',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4905'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_4906',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4906',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4906'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_4907',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4907',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4907'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_4908',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4908',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4908'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_4909',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_4909',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['4909'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_49010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_49010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['49010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_49011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_49011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['49011'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_49012',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_49012',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['49012'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_49013',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_49013',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['49013'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_49014',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_49014',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['49014'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_49015',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_49015',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['49015'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_49016',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_49016',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['49016'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_49017',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_49017',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['49017'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 49
{
	id:'reponse_49018',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_49018',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['49018'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 49
{
	id:'question_49_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_49_0',
	inputType:'radio',
	comment:'comment_49_0',
	options:['reponse_49_0_0', 'reponse_49_0_1', 'reponse_49_0_2', 'reponse_49_0_3', 'reponse_49_0_4', 'reponse_49_0_5', 'reponse_49_0_6', 'reponse_49_0_7', 'reponse_49_0_8', 'reponse_49_0_9', 'reponse_49_0_10', 'reponse_49_0_11', 'reponse_49_0_12', 'reponse_49_0_13', 'reponse_49_0_14', 'reponse_49_0_15', 'reponse_49_0_16', 'reponse_49_0_17', 'reponse_49_0_18'],
	nextQuestionMap:['reponse_49_0_0', 'reponse_49_0_1', 'reponse_49_0_2', 'reponse_49_0_3', 'reponse_49_0_4', 'reponse_49_0_5', 'reponse_49_0_6', 'reponse_49_0_7', 'reponse_49_0_8', 'reponse_49_0_9', 'reponse_49_0_10', 'reponse_49_0_11', 'reponse_49_0_12', 'reponse_49_0_13', 'reponse_49_0_14', 'reponse_49_0_15', 'reponse_49_0_16', 'reponse_49_0_17', 'reponse_49_0_18'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 50
{
	id:'reponse_5000',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5000',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5000'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 50
{
	id:'reponse_5001',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5001',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5001'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 50
{
	id:'reponse_5002',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5002',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5002'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 50
{
	id:'question_50_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_50_0',
	inputType:'radio',
	comment:'',
	options:['reponse_50_0_0', 'reponse_50_0_1', 'reponse_50_0_2'],
	nextQuestionMap:['reponse_50_0_0', 'reponse_50_0_1', 'reponse_50_0_2'],
	scoreMap:[1, 1, 1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5200',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5200',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5200'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5201',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5201',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5201'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5202',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5202',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5202'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5203',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5203',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5203'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5204',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5204',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5204'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5205',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5205',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5205'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5206',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5206',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5206'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5207',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5207',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5207'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 52
{
	id:'question_52_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_52_0',
	inputType:'radio',
	comment:'',
	options:['reponse_52_0_0', 'reponse_52_0_1', 'reponse_52_0_2', 'reponse_52_0_3', 'reponse_52_0_4', 'reponse_52_0_5', 'reponse_52_0_6', 'reponse_52_0_7'],
	nextQuestionMap:['reponse_52_0_0', 'reponse_52_0_1', 'reponse_52_0_2', 'reponse_52_0_3', 'reponse_52_0_4', 'reponse_52_0_5', 'reponse_52_0_6', 'reponse_52_0_7'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5210',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5210',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5210'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5211',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5211',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5211'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 52
{
	id:'question_52_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_52_1',
	inputType:'radio',
	comment:'',
	options:['reponse_52_1_0', 'reponse_52_1_1'],
	nextQuestionMap:['reponse_52_1_0', 'reponse_52_1_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5220',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5220',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5220'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5221',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5221',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5221'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 52
{
	id:'question_52_2',
	category:CATEGORIES.SYMPTOMS,
	text:'question_52_2',
	inputType:'radio',
	comment:'',
	options:['reponse_52_2_0', 'reponse_52_2_1'],
	nextQuestionMap:['reponse_52_2_0', 'reponse_52_2_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5230',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5230',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5230'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5231',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5231',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5231'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 52
{
	id:'question_52_3',
	category:CATEGORIES.SYMPTOMS,
	text:'question_52_3',
	inputType:'radio',
	comment:'',
	options:['reponse_52_3_0', 'reponse_52_3_1'],
	nextQuestionMap:['reponse_52_3_0', 'reponse_52_3_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5240',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5240',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5240'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 52
{
	id:'reponse_5241',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5241',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5241'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 52
{
	id:'question_52_4',
	category:CATEGORIES.SYMPTOMS,
	text:'question_52_4',
	inputType:'radio',
	comment:'',
	options:['reponse_52_4_0', 'reponse_52_4_1'],
	nextQuestionMap:['reponse_52_4_0', 'reponse_52_4_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 53
{
	id:'reponse_5300',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5300',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5300'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 53
{
	id:'reponse_5301',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5301',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5301'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 53
{
	id:'reponse_5302',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5302',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5302'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 53
{
	id:'reponse_5303',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5303',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5303'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 53
{
	id:'reponse_5304',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5304',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5304'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 53
{
	id:'reponse_5305',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5305',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5305'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 53
{
	id:'reponse_5306',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5306',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5306'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 53
{
	id:'reponse_5307',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5307',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5307'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 53
{
	id:'reponse_5308',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5308',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5308'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 53
{
	id:'reponse_5309',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5309',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5309'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 53
{
	id:'reponse_53010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_53010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['53010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 53
{
	id:'reponse_53011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_53011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['53011'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 53
{
	id:'question_53_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_53_0',
	inputType:'radio',
	comment:'',
	options:['reponse_53_0_0', 'reponse_53_0_1', 'reponse_53_0_2', 'reponse_53_0_3', 'reponse_53_0_4', 'reponse_53_0_5', 'reponse_53_0_6', 'reponse_53_0_7', 'reponse_53_0_8', 'reponse_53_0_9', 'reponse_53_0_10', 'reponse_53_0_11'],
	nextQuestionMap:['reponse_53_0_0', 'reponse_53_0_1', 'reponse_53_0_2', 'reponse_53_0_3', 'reponse_53_0_4', 'reponse_53_0_5', 'reponse_53_0_6', 'reponse_53_0_7', 'reponse_53_0_8', 'reponse_53_0_9', 'reponse_53_0_10', 'reponse_53_0_11'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 54
{
	id:'reponse_5400',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5400',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5400'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 54
{
	id:'reponse_5401',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5401',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5401'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 54
{
	id:'reponse_5402',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5402',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5402'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 54
{
	id:'reponse_5403',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5403',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5403'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 54
{
	id:'question_54_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_54_0',
	inputType:'radio',
	comment:'',
	options:['reponse_54_0_0', 'reponse_54_0_1', 'reponse_54_0_2', 'reponse_54_0_3'],
	nextQuestionMap:['reponse_54_0_0', 'reponse_54_0_1', 'reponse_54_0_2', 'reponse_54_0_3'],
	scoreMap:[1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 54
{
	id:'reponse_5410',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5410',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5410'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 54
{
	id:'reponse_5411',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5411',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5411'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 54
{
	id:'question_54_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_54_1',
	inputType:'radio',
	comment:'',
	options:['reponse_54_1_0', 'reponse_54_1_1'],
	nextQuestionMap:['reponse_54_1_0', 'reponse_54_1_1'],
	scoreMap:[1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 54
{
	id:'reponse_5420',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5420',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5420'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 54
{
	id:'reponse_5421',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5421',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5421'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 54
{
	id:'reponse_5422',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5422',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5422'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 54
{
	id:'question_54_2',
	category:CATEGORIES.SYMPTOMS,
	text:'question_54_2',
	inputType:'radio',
	comment:'',
	options:['reponse_54_2_0', 'reponse_54_2_1', 'reponse_54_2_2'],
	nextQuestionMap:['reponse_54_2_0', 'reponse_54_2_1', 'reponse_54_2_2'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5500',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5500',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5500'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5501',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5501',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5501'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5502',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5502',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5502'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5503',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5503',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5503'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5504',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5504',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5504'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5505',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5505',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5505'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5506',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5506',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5506'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5507',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5507',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5507'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5508',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5508',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5508'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5509',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5509',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5509'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 55
{
	id:'question_55_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_55_0',
	inputType:'radio',
	comment:'',
	options:['reponse_55_0_0', 'reponse_55_0_1', 'reponse_55_0_2', 'reponse_55_0_3', 'reponse_55_0_4', 'reponse_55_0_5', 'reponse_55_0_6', 'reponse_55_0_7', 'reponse_55_0_8', 'reponse_55_0_9'],
	nextQuestionMap:['reponse_55_0_0', 'reponse_55_0_1', 'reponse_55_0_2', 'reponse_55_0_3', 'reponse_55_0_4', 'reponse_55_0_5', 'reponse_55_0_6', 'reponse_55_0_7', 'reponse_55_0_8', 'reponse_55_0_9'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5510',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5510',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5510'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5511',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5511',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5511'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5512',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5512',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5512'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5513',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5513',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5513'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 55
{
	id:'reponse_5514',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5514',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5514'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 55
{
	id:'question_55_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_55_1',
	inputType:'radio',
	comment:'',
	options:['reponse_55_1_0', 'reponse_55_1_1', 'reponse_55_1_2', 'reponse_55_1_3', 'reponse_55_1_4'],
	nextQuestionMap:['reponse_55_1_0', 'reponse_55_1_1', 'reponse_55_1_2', 'reponse_55_1_3', 'reponse_55_1_4'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 56
{
	id:'reponse_5600',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5600',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5600'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 56
{
	id:'reponse_5601',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5601',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5601'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 56
{
	id:'reponse_5602',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5602',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5602'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 56
{
	id:'reponse_5603',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5603',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5603'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 56
{
	id:'reponse_5604',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5604',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5604'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 56
{
	id:'reponse_5605',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5605',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5605'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 56
{
	id:'reponse_5606',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5606',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5606'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 56
{
	id:'reponse_5607',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5607',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5607'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 56
{
	id:'reponse_5608',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5608',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5608'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 56
{
	id:'reponse_5609',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5609',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5609'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 56
{
	id:'question_56_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_56_0',
	inputType:'radio',
	comment:'',
	options:['reponse_56_0_0', 'reponse_56_0_1', 'reponse_56_0_2', 'reponse_56_0_3', 'reponse_56_0_4', 'reponse_56_0_5', 'reponse_56_0_6', 'reponse_56_0_7', 'reponse_56_0_8', 'reponse_56_0_9'],
	nextQuestionMap:['reponse_56_0_0', 'reponse_56_0_1', 'reponse_56_0_2', 'reponse_56_0_3', 'reponse_56_0_4', 'reponse_56_0_5', 'reponse_56_0_6', 'reponse_56_0_7', 'reponse_56_0_8', 'reponse_56_0_9'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 56
{
	id:'reponse_5610',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5610',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5610'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 56
{
	id:'reponse_5611',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5611',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5611'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 56
{
	id:'question_56_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_56_1',
	inputType:'radio',
	comment:'',
	options:['reponse_56_1_0', 'reponse_56_1_1'],
	nextQuestionMap:['reponse_56_1_0', 'reponse_56_1_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 58
{
	id:'reponse_5800',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5800',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5800'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 58
{
	id:'reponse_5801',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5801',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5801'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 58
{
	id:'reponse_5802',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5802',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5802'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 58
{
	id:'reponse_5803',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5803',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5803'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 58
{
	id:'reponse_5804',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5804',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5804'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 58
{
	id:'reponse_5805',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5805',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5805'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 58
{
	id:'reponse_5806',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_5806',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['5806'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 58
{
	id:'question_58_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_58_0',
	inputType:'radio',
	comment:'comment_58_0',
	options:['reponse_58_0_0', 'reponse_58_0_1', 'reponse_58_0_2', 'reponse_58_0_3', 'reponse_58_0_4', 'reponse_58_0_5', 'reponse_58_0_6'],
	nextQuestionMap:['reponse_58_0_0', 'reponse_58_0_1', 'reponse_58_0_2', 'reponse_58_0_3', 'reponse_58_0_4', 'reponse_58_0_5', 'reponse_58_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 60
{
	id:'reponse_6000',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6000',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6000'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 60
{
	id:'reponse_6001',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6001',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6001'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 60
{
	id:'reponse_6002',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6002',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6002'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 60
{
	id:'reponse_6003',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6003',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6003'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 60
{
	id:'reponse_6004',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6004',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6004'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 60
{
	id:'reponse_6005',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6005',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6005'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 60
{
	id:'question_60_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_60_0',
	inputType:'radio',
	comment:'',
	options:['reponse_60_0_0', 'reponse_60_0_1', 'reponse_60_0_2', 'reponse_60_0_3', 'reponse_60_0_4', 'reponse_60_0_5'],
	nextQuestionMap:['reponse_60_0_0', 'reponse_60_0_1', 'reponse_60_0_2', 'reponse_60_0_3', 'reponse_60_0_4', 'reponse_60_0_5'],
	scoreMap:[1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 61
{
	id:'reponse_6100',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6100',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6100'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 61
{
	id:'reponse_6101',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6101',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6101'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 61
{
	id:'reponse_6102',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6102',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6102'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 61
{
	id:'reponse_6103',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6103',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6103'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 61
{
	id:'reponse_6104',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6104',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6104'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 61
{
	id:'reponse_6105',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6105',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6105'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 61
{
	id:'reponse_6106',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6106',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6106'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 61
{
	id:'reponse_6107',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6107',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6107'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 61
{
	id:'question_61_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_61_0',
	inputType:'radio',
	comment:'',
	options:['reponse_61_0_0', 'reponse_61_0_1', 'reponse_61_0_2', 'reponse_61_0_3', 'reponse_61_0_4', 'reponse_61_0_5', 'reponse_61_0_6', 'reponse_61_0_7'],
	nextQuestionMap:['reponse_61_0_0', 'reponse_61_0_1', 'reponse_61_0_2', 'reponse_61_0_3', 'reponse_61_0_4', 'reponse_61_0_5', 'reponse_61_0_6', 'reponse_61_0_7'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 63
{
	id:'reponse_6300',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6300',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6300'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 63
{
	id:'reponse_6301',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6301',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6301'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 63
{
	id:'reponse_6302',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6302',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6302'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 63
{
	id:'question_63_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_63_0',
	inputType:'radio',
	comment:'',
	options:['reponse_63_0_0', 'reponse_63_0_1', 'reponse_63_0_2'],
	nextQuestionMap:['reponse_63_0_0', 'reponse_63_0_1', 'reponse_63_0_2'],
	scoreMap:[1, 1, 1]
},
//PARTIE REPONSE SUMMARY 63
{
	id:'reponse_6310',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6310',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6310'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 63
{
	id:'reponse_6311',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6311',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6311'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 63
{
	id:'reponse_6312',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6312',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6312'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 63
{
	id:'reponse_6313',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6313',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6313'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 63
{
	id:'reponse_6314',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6314',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6314'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 63
{
	id:'reponse_6315',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6315',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6315'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 63
{
	id:'question_63_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_63_1',
	inputType:'radio',
	comment:'',
	options:['reponse_63_1_0', 'reponse_63_1_1', 'reponse_63_1_2', 'reponse_63_1_3', 'reponse_63_1_4', 'reponse_63_1_5'],
	nextQuestionMap:['reponse_63_1_0', 'reponse_63_1_1', 'reponse_63_1_2', 'reponse_63_1_3', 'reponse_63_1_4', 'reponse_63_1_5'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 64
{
	id:'reponse_6400',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6400',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6400'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 64
{
	id:'reponse_6401',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6401',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6401'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 64
{
	id:'reponse_6402',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6402',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6402'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 64
{
	id:'reponse_6403',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6403',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6403'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 64
{
	id:'reponse_6404',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6404',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6404'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 64
{
	id:'reponse_6405',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6405',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6405'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 64
{
	id:'question_64_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_64_0',
	inputType:'radio',
	comment:'',
	options:['reponse_64_0_0', 'reponse_64_0_1', 'reponse_64_0_2', 'reponse_64_0_3', 'reponse_64_0_4', 'reponse_64_0_5'],
	nextQuestionMap:['reponse_64_0_0', 'reponse_64_0_1', 'reponse_64_0_2', 'reponse_64_0_3', 'reponse_64_0_4', 'reponse_64_0_5'],
	scoreMap:[1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 64
{
	id:'reponse_6410',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6410',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6410'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 64
{
	id:'reponse_6411',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6411',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6411'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 64
{
	id:'question_64_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_64_1',
	inputType:'radio',
	comment:'',
	options:['reponse_64_1_0', 'reponse_64_1_1'],
	nextQuestionMap:['reponse_64_1_0', 'reponse_64_1_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 65
{
	id:'reponse_6500',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6500',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6500'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 65
{
	id:'reponse_6501',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6501',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6501'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 65
{
	id:'reponse_6502',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6502',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6502'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 65
{
	id:'reponse_6503',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6503',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6503'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 65
{
	id:'reponse_6504',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6504',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6504'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 65
{
	id:'reponse_6505',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6505',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6505'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 65
{
	id:'reponse_6506',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6506',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6506'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 65
{
	id:'question_65_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_65_0',
	inputType:'radio',
	comment:'',
	options:['reponse_65_0_0', 'reponse_65_0_1', 'reponse_65_0_2', 'reponse_65_0_3', 'reponse_65_0_4', 'reponse_65_0_5', 'reponse_65_0_6'],
	nextQuestionMap:['reponse_65_0_0', 'reponse_65_0_1', 'reponse_65_0_2', 'reponse_65_0_3', 'reponse_65_0_4', 'reponse_65_0_5', 'reponse_65_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 65
{
	id:'reponse_6510',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6510',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6510'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 65
{
	id:'reponse_6511',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6511',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6511'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 65
{
	id:'reponse_6512',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6512',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6512'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 65
{
	id:'reponse_6513',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6513',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6513'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 65
{
	id:'question_65_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_65_1',
	inputType:'radio',
	comment:'',
	options:['reponse_65_1_0', 'reponse_65_1_1', 'reponse_65_1_2', 'reponse_65_1_3'],
	nextQuestionMap:['reponse_65_1_0', 'reponse_65_1_1', 'reponse_65_1_2', 'reponse_65_1_3'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6600',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6600',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6600'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6601',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6601',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6601'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6602',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6602',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6602'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6603',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6603',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6603'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6604',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6604',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6604'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6605',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6605',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6605'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6606',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6606',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6606'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6607',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6607',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6607'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6608',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6608',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6608'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6609',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6609',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6609'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_66010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_66010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['66010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_66011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_66011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['66011'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_66012',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_66012',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['66012'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_66013',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_66013',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['66013'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_66014',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_66014',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['66014'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_66015',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_66015',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['66015'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 66
{
	id:'question_66_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_66_0',
	inputType:'radio',
	comment:'',
	options:['reponse_66_0_0', 'reponse_66_0_1', 'reponse_66_0_2', 'reponse_66_0_3', 'reponse_66_0_4', 'reponse_66_0_5', 'reponse_66_0_6', 'reponse_66_0_7', 'reponse_66_0_8', 'reponse_66_0_9', 'reponse_66_0_10', 'reponse_66_0_11', 'reponse_66_0_12', 'reponse_66_0_13', 'reponse_66_0_14', 'reponse_66_0_15'],
	nextQuestionMap:['reponse_66_0_0', 'reponse_66_0_1', 'reponse_66_0_2', 'reponse_66_0_3', 'reponse_66_0_4', 'reponse_66_0_5', 'reponse_66_0_6', 'reponse_66_0_7', 'reponse_66_0_8', 'reponse_66_0_9', 'reponse_66_0_10', 'reponse_66_0_11', 'reponse_66_0_12', 'reponse_66_0_13', 'reponse_66_0_14', 'reponse_66_0_15'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6610',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6610',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6610'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6611',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6611',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6611'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6612',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6612',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6612'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 66
{
	id:'reponse_6613',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6613',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6613'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 66
{
	id:'question_66_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_66_1',
	inputType:'radio',
	comment:'',
	options:['reponse_66_1_0', 'reponse_66_1_1', 'reponse_66_1_2', 'reponse_66_1_3'],
	nextQuestionMap:['reponse_66_1_0', 'reponse_66_1_1', 'reponse_66_1_2', 'reponse_66_1_3'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 67
{
	id:'reponse_6700',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6700',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6700'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 67
{
	id:'reponse_6701',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6701',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6701'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 67
{
	id:'reponse_6702',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6702',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6702'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 67
{
	id:'reponse_6703',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6703',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6703'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 67
{
	id:'reponse_6704',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6704',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6704'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 67
{
	id:'reponse_6705',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6705',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6705'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 67
{
	id:'reponse_6706',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6706',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6706'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 67
{
	id:'question_67_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_67_0',
	inputType:'radio',
	comment:'',
	options:['reponse_67_0_0', 'reponse_67_0_1', 'reponse_67_0_2', 'reponse_67_0_3', 'reponse_67_0_4', 'reponse_67_0_5', 'reponse_67_0_6'],
	nextQuestionMap:['reponse_67_0_0', 'reponse_67_0_1', 'reponse_67_0_2', 'reponse_67_0_3', 'reponse_67_0_4', 'reponse_67_0_5', 'reponse_67_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 68
{
	id:'reponse_6800',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6800',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6800'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 68
{
	id:'reponse_6801',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6801',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6801'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 68
{
	id:'reponse_6802',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6802',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6802'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 68
{
	id:'reponse_6803',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6803',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6803'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 68
{
	id:'reponse_6804',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6804',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6804'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 68
{
	id:'reponse_6805',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6805',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6805'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 68
{
	id:'reponse_6806',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6806',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6806'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 68
{
	id:'question_68_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_68_0',
	inputType:'radio',
	comment:'',
	options:['reponse_68_0_0', 'reponse_68_0_1', 'reponse_68_0_2', 'reponse_68_0_3', 'reponse_68_0_4', 'reponse_68_0_5', 'reponse_68_0_6'],
	nextQuestionMap:['reponse_68_0_0', 'reponse_68_0_1', 'reponse_68_0_2', 'reponse_68_0_3', 'reponse_68_0_4', 'reponse_68_0_5', 'reponse_68_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_6900',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6900',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6900'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_6901',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6901',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6901'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_6902',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6902',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6902'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_6903',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6903',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6903'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_6904',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6904',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6904'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_6905',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6905',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6905'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_6906',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6906',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6906'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_6907',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6907',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6907'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_6908',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6908',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6908'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_6909',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6909',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6909'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_69010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_69010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['69010'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 69
{
	id:'question_69_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_69_0',
	inputType:'radio',
	comment:'',
	options:['reponse_69_0_0', 'reponse_69_0_1', 'reponse_69_0_2', 'reponse_69_0_3', 'reponse_69_0_4', 'reponse_69_0_5', 'reponse_69_0_6', 'reponse_69_0_7', 'reponse_69_0_8', 'reponse_69_0_9', 'reponse_69_0_10'],
	nextQuestionMap:['reponse_69_0_0', 'reponse_69_0_1', 'reponse_69_0_2', 'reponse_69_0_3', 'reponse_69_0_4', 'reponse_69_0_5', 'reponse_69_0_6', 'reponse_69_0_7', 'reponse_69_0_8', 'reponse_69_0_9', 'reponse_69_0_10'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_6910',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6910',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6910'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 69
{
	id:'reponse_6911',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_6911',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['6911'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 69
{
	id:'question_69_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_69_1',
	inputType:'radio',
	comment:'comment_69_1',
	options:['reponse_69_1_0', 'reponse_69_1_1'],
	nextQuestionMap:['reponse_69_1_0', 'reponse_69_1_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 70
{
	id:'reponse_7000',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7000',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7000'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 70
{
	id:'reponse_7001',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7001',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7001'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 70
{
	id:'reponse_7002',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7002',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7002'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 70
{
	id:'question_70_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_70_0',
	inputType:'radio',
	comment:'',
	options:['reponse_70_0_0', 'reponse_70_0_1', 'reponse_70_0_2'],
	nextQuestionMap:['reponse_70_0_0', 'reponse_70_0_1', 'reponse_70_0_2'],
	scoreMap:[1, 1, 1]
},
//PARTIE REPONSE SUMMARY 70
{
	id:'reponse_7010',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7010',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7010'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 70
{
	id:'reponse_7011',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7011',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7011'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 70
{
	id:'reponse_7012',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7012',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7012'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 70
{
	id:'reponse_7013',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7013',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7013'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 70
{
	id:'reponse_7014',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7014',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7014'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 70
{
	id:'question_70_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_70_1',
	inputType:'radio',
	comment:'',
	options:['reponse_70_1_0', 'reponse_70_1_1', 'reponse_70_1_2', 'reponse_70_1_3', 'reponse_70_1_4'],
	nextQuestionMap:['reponse_70_1_0', 'reponse_70_1_1', 'reponse_70_1_2', 'reponse_70_1_3', 'reponse_70_1_4'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7100',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7100',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7100'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 71
{
	id:'question_71_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_71_0',
	inputType:'radio',
	comment:'',
	options:['reponse_71_0_0'],
	nextQuestionMap:['reponse_71_0_0'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7110',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7110',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7110'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7111',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7111',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7111'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7112',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7112',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7112'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7113',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7113',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7113'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7114',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7114',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7114'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 71
{
	id:'question_71_1',
	category:CATEGORIES.SYMPTOMS,
	text:'question_71_1',
	inputType:'radio',
	comment:'',
	options:['reponse_71_1_0', 'reponse_71_1_1', 'reponse_71_1_2', 'reponse_71_1_3', 'reponse_71_1_4'],
	nextQuestionMap:['reponse_71_1_0', 'reponse_71_1_1', 'reponse_71_1_2', 'reponse_71_1_3', 'reponse_71_1_4'],
	scoreMap:[1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7120',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7120',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7120'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7121',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7121',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7121'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 71
{
	id:'question_71_2',
	category:CATEGORIES.SYMPTOMS,
	text:'question_71_2',
	inputType:'radio',
	comment:'',
	options:['reponse_71_2_0', 'reponse_71_2_1'],
	nextQuestionMap:['reponse_71_2_0', 'reponse_71_2_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7130',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7130',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7130'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7131',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7131',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7131'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 71
{
	id:'question_71_3',
	category:CATEGORIES.SYMPTOMS,
	text:'question_71_3',
	inputType:'radio',
	comment:'',
	options:['reponse_71_3_0', 'reponse_71_3_1'],
	nextQuestionMap:['reponse_71_3_0', 'reponse_71_3_1'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7140',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7140',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7140'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7141',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7141',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7141'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 71
{
	id:'reponse_7142',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7142',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7142'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 71
{
	id:'question_71_4',
	category:CATEGORIES.SYMPTOMS,
	text:'question_71_4',
	inputType:'radio',
	comment:'',
	options:['reponse_71_4_0', 'reponse_71_4_1', 'reponse_71_4_2'],
	nextQuestionMap:['reponse_71_4_0', 'reponse_71_4_1', 'reponse_71_4_2'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 72
{
	id:'reponse_7200',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7200',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7200'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 72
{
	id:'reponse_7201',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7201',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7201'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 72
{
	id:'reponse_7202',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7202',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7202'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 72
{
	id:'question_72_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_72_0',
	inputType:'radio',
	comment:'',
	options:['reponse_72_0_0', 'reponse_72_0_1', 'reponse_72_0_2'],
	nextQuestionMap:['reponse_72_0_0', 'reponse_72_0_1', 'reponse_72_0_2'],
	scoreMap:[1, 1, 1]
},
//PARTIE REPONSE SUMMARY 73
{
	id:'reponse_7300',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7300',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7300'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 73
{
	id:'reponse_7301',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7301',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7301'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 73
{
	id:'reponse_7302',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7302',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7302'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 73
{
	id:'reponse_7303',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7303',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7303'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 73
{
	id:'reponse_7304',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7304',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7304'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 73
{
	id:'reponse_7305',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7305',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7305'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 73
{
	id:'question_73_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_73_0',
	inputType:'radio',
	comment:'',
	options:['reponse_73_0_0', 'reponse_73_0_1', 'reponse_73_0_2', 'reponse_73_0_3', 'reponse_73_0_4', 'reponse_73_0_5'],
	nextQuestionMap:['reponse_73_0_0', 'reponse_73_0_1', 'reponse_73_0_2', 'reponse_73_0_3', 'reponse_73_0_4', 'reponse_73_0_5'],
	scoreMap:[1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 74
{
	id:'reponse_7400',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7400',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7400'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 74
{
	id:'reponse_7401',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7401',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7401'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 74
{
	id:'reponse_7402',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7402',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7402'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 74
{
	id:'reponse_7403',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7403',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7403'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 74
{
	id:'reponse_7404',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7404',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7404'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 74
{
	id:'reponse_7405',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7405',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7405'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 74
{
	id:'question_74_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_74_0',
	inputType:'radio',
	comment:'',
	options:['reponse_74_0_0', 'reponse_74_0_1', 'reponse_74_0_2', 'reponse_74_0_3', 'reponse_74_0_4', 'reponse_74_0_5'],
	nextQuestionMap:['reponse_74_0_0', 'reponse_74_0_1', 'reponse_74_0_2', 'reponse_74_0_3', 'reponse_74_0_4', 'reponse_74_0_5'],
	scoreMap:[1, 1, 1, 1, 1, 1]
},
//PARTIE REPONSE SUMMARY 75
{
	id:'reponse_7500',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7500',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7500'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 75
{
	id:'reponse_7501',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7501',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7501'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 75
{
	id:'reponse_7502',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7502',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7502'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 75
{
	id:'reponse_7503',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7503',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7503'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 75
{
	id:'reponse_7504',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7504',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7504'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 75
{
	id:'reponse_7505',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7505',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7505'],
	scoreMap:[1]
},
//PARTIE REPONSE SUMMARY 75
{
	id:'reponse_7506',
	category:CATEGORIES.SYMPTOMS,
	text:'reponse_7506',
	inputType:'radio',
	comment:'',
	options:['ok'],
	nextQuestionMap:['7506'],
	scoreMap:[1]
},
//PARTIE DECOUPE QUESTION SUMMARY 75
{
	id:'question_75_0',
	category:CATEGORIES.SYMPTOMS,
	text:'question_75_0',
	inputType:'radio',
	comment:'',
	options:['reponse_75_0_0', 'reponse_75_0_1', 'reponse_75_0_2', 'reponse_75_0_3', 'reponse_75_0_4', 'reponse_75_0_5', 'reponse_75_0_6'],
	nextQuestionMap:['reponse_75_0_0', 'reponse_75_0_1', 'reponse_75_0_2', 'reponse_75_0_3', 'reponse_75_0_4', 'reponse_75_0_5', 'reponse_75_0_6'],
	scoreMap:[1, 1, 1, 1, 1, 1, 1]
},{
	id:'introduction_1',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_1',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_2',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_2',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_3',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_3',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_4',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_4',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_5',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_5',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_6',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_6',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_7',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_7',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_8',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_8',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_9',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_9',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_10',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_10',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_11',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_11',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_12',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_12',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_13',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_13',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_14',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_14',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_15',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_15',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_16',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_16',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_17',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_17',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_18',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_18',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_19',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_19',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_20',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_20',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_21',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_21',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_22',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_22',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_23',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_23',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_24',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_24',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_25',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_25',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_26',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_26',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_27',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_27',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_28',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_28',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_29',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_29',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_30',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_30',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_31',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_31',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_32',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_32',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_33',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_33',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_34',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_34',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_35',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_35',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_36',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_36',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_37',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_37',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_38',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_38',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_39',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_39',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_40',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_40',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_41',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_41',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_42',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_42',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_43',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_43',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_44',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_44',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_45',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_45',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_46',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_46',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_47',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_47',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_48',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_48',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_49',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_49',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_50',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_50',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_51',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_51',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_52',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_52',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_53',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_53',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_54',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_54',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_55',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_55',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_56',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_56',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_57',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_57',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_58',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_58',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_59',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_59',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_60',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_60',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_61',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_61',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_62',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_62',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_63',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_63',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_64',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_64',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_65',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_65',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_66',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_66',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_67',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_67',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_68',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_68',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_69',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_69',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_70',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_70',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_71',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_71',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_72',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_72',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_73',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_73',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_74',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_74',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'introduction_75',
	category:CATEGORIES.SYMPTOMS,
	text:'introduction_75',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_1',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_1',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_2',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_2',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_3',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_3',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_4',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_4',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_5',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_5',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_6',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_6',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_7',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_7',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_8',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_8',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_9',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_9',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_10',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_10',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_11',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_11',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_12',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_12',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_13',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_13',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_14',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_14',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_15',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_15',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_16',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_16',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_17',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_17',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_18',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_18',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_19',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_19',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_20',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_20',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_21',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_21',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_22',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_22',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_23',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_23',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_24',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_24',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_25',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_25',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_26',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_26',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_27',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_27',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_28',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_28',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_29',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_29',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_30',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_30',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_31',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_31',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_32',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_32',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_33',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_33',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_34',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_34',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_35',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_35',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_36',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_36',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_37',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_37',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_38',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_38',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_39',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_39',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_40',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_40',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_41',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_41',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_42',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_42',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_43',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_43',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_44',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_44',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_45',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_45',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_46',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_46',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_47',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_47',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_48',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_48',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_49',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_49',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_50',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_50',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_51',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_51',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_52',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_52',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_53',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_53',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_54',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_54',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_55',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_55',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_56',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_56',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_57',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_57',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_58',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_58',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_59',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_59',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_60',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_60',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_61',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_61',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_62',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_62',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_63',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_63',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_64',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_64',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_65',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_65',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_66',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_66',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_67',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_67',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_68',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_68',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_69',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_69',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_70',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_70',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_71',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_71',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_72',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_72',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_73',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_73',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_74',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_74',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},{
	id:'your_doctor_visit_75',
	category:CATEGORIES.SYMPTOMS,
	text:'your_doctor_visit_75',
	inputType:'radio',
	comment:'',
	options:['home'],
	nextQuestionMap:['question'],
	scoreMap:[]
},
]