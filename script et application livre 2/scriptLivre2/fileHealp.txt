
What it feels like: skin taking on a yellowish appearance, also called
jaundice.

What can make it worse: taking certain medications, intravenous
drugs, drinking alcohol, exposure to chemical solvents, receiving
blood products.

Jaundice occurs when you accumulate too much of a substance
called bilirubin, which results from the breakdown of red blood cells,
in the blood. Almost every newborn has at least a mild case of jaun-
dice, which typically causes no problems.



YOUR DOCTOR VISIT
What your doctor will ask you about: headache, loss of appetite,
nausea, vomiting, fever, shaking chills, abdominal pain, change in
weight, abdominal swelling, black or tarry stools, blood in stools, light-
colored stools, dark urine, change in thinking patterns, joint pain, itch-
iness, results of tests related to the problem, including a liver biopsy.

Your doctor will want to know if you or anyone in your family
has had any of these conditions: jaundice, alcoholism, gallstones,
liver disease, past gastrointestinal bleeding, drug addiction, mononu-
cleosis, blood disease, cancer.

Your doctor will want to know when you first noticed your symp-
toms, and if you have had close contact with another person
with jaundice, if you work with chemical solvents, or if you have
traveled within the past six months.

Your doctor will want to know if you’re taking any medications,
including: isoniazid (for tuberculosis), phenothiazine antipsy-
chotics such as Haldol, oral contraceptives, recent anesthetics,
cholesterol-lowering drugs, sulfa antibiotics such as sulfamethoxa-
zole, the antibiotic nitrofurantoin, the heart drug quinidine.

Your doctor will do a physical examination including the fol-
lowing: blood pressure, pulse, temperature, pushing on the
abdomen, checking stool for the presence of blood, checking size of
testicles and breasts (male), thorough skin examination. Your doctor
may also ask you questions to check your mental status, such as if
you know where you are and recent events.
ASK THE FOLLOWING QUESTION
Other TITREOPTION
WHAT CAN CAUSE YELLOW SKIN,AND WHAT IS TYPICAL FOR EACH CAUSE?##

Viral hepatitis # Infection of the liver caused by a virus such as hepatitis A, B, or C; hepatitis B and C are more common in intra- venous drug abusers and people who were in con- tact with blood products, other jaundiced people, or contaminated water; hepatitis A can be con- tracted from food and is less severe # Jaundice begins over days and weeks, upper abdomi- nal pain, nausea, vomiting, joint pain, fever, headache, dark urine, light stools 
@ Toxic hepatitis # Inflammation of the liver caused by alcohol, drugs, or chemical solvents # Jaundice begins over days and weeks, dark urine, light stools, upper abdomi- nal pain 
@ Obstruction of biliary flow # Blockage in the flow of digestive fluid that con- tains bilirubin from the liver, causing a buildup of bilirubin in the blood #Itching, light stools, dark urine, history of gallstones 
@ Gilbert’s syndrome # Inherited disorder that affects the processing of bilirubin # Jaundice occurs in periods of exercise, fasting, stress, or infection 
@ Dubin-Johnson syndrome #Inherited disorder that affects the transport of bilirubin # Mild jaundice throughout entire life, begins after puberty 
@ Chronic liver disease # Long-term liver disease, causing scarring of the liver, also known as cirrhosis, often caused by a long history of alcoholism #Chronic jaundice, history of any of the listed causes of jaundice, red palms 
@ Liver failure # Inability of the liver to function properly # History of infection or gas- trointestinal bleeding with liver disease, disorienta- tion, stupor, coma 

Option #YELLOW SKIN IN INFANTS TITREOPTION
IN INFANT,THE TIME WHEN JAUNDICE APPEARS CAN HELP DETERMINE THE CAUSE##
 
Breakdown of blood cells or any of causes listed above # #the 24 hours after birth 
@ Normal, will disappear eventually; if it persists beyond a week, it may be due to a more serious liver or gland problem # #Between 2 and 4 days after birth 
@Severe infection# # Fifth or later day after birth

