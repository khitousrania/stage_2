import json
fileDetails = open("detailsMaladie.txt",'r+',encoding="utf-8")
fileListe= open("listeMaladie.txt",'r+',encoding="utf-8")
fileHealp =open("fileHealp.txt",'w+',encoding="utf-8")

tab_ListeMladie=[]

def listingMaladie():
    i=0
    for ligne in fileListe:
        i+=1
        tab_ListeMladie.append(ligne[:-1])
      
    
listingMaladie()

dict_maladies ={}

def decoupeMaladies():
    fileHealp.write(fileDetails.read())
    for key,m in enumerate(tab_ListeMladie):
        if(key==0):
            pass
        else:
            fileHealp.seek(0,0)
            s= fileHealp.read() 
            sentences =s.split(tab_ListeMladie[key])
            fileMaladie =open("maladies/"+tab_ListeMladie[key-1][:-1]+'.txt','w+',encoding='utf-8')
            dict_maladies[tab_ListeMladie[key-1][:-1]]=sentences[0]
            fileMaladie.write(tab_ListeMladie[key-1][:-1]+sentences[0])
            fileHealp.seek(0,0)
            fileHealp.truncate(0)
            fileHealp.seek(0,0)
            fileHealp.write(sentences[1])
            if(key==len(tab_ListeMladie)-1):
                fileMaladie =open("maladies/"+tab_ListeMladie[key]+'.txt','w+',encoding='utf-8')
                dict_maladies[m]=sentences[1]
                fileMaladie.write(tab_ListeMladie[key-1][:-1]+sentences[1])
   
decoupeMaladies()


dict_maladies_bis={}

keywords =['YOUR DOCTOR VISIT','ASK THE FOLLOWING QUESTION','Option#','##','TITREOPTION','@','$','This disease has no questionnaire','What\'s the first letter of the disease that you are looking for?','No for this disease']

def decoupeReponseSummary(decoupeAlt,dict_maladies_bis,m,i,s,j):
    sentence =  decoupeAlt.split('#')
    dict_maladies_bis[m]['reponse_'+str(i+1)+'_'+str(s)+'_'+str(j)]=sentence[2] #----------------------------
    dict_maladies_bis[m]['summary_comment_'+str(i+1)+str(s)+str(j)]=sentence[0]+': '+sentence[1]


def generalDecoupeReponseSummary(dict_maladies_bis,m,i,s):
    sentenceAdecouper =dict_maladies_bis[m]['reponse_'+str(i+1)+'_'+str(s)]
    if(keywords[6] in dict_maladies_bis[m]['reponse_'+str(i+1)+'_'+str(s)]):
        sentencesDollar =dict_maladies_bis[m]['reponse_'+str(i+1)+'_'+str(s)].split(keywords[6])
        dict_maladies_bis[m]['comment_'+str(i+1)+'_'+str(s)]=sentencesDollar[1]
        sentenceAdecouper =sentencesDollar[0]
    decoupeAlt = sentenceAdecouper.split(keywords[5])
    if(len(decoupeAlt)==0):
        del dict_maladies_bis[m]['reponse_'+str(i+1)+'_'+str(s)]
        decoupeReponseSummary(decoupeAlt[0],dict_maladies_bis,m,i,s,0)
    else:
        del dict_maladies_bis[m]['reponse_'+str(i+1)+'_'+str(s)]
        for p in range(0,len(decoupeAlt)):
            decoupeReponseSummary(decoupeAlt[p],dict_maladies_bis,m,i,s,p)
    
def decoupeMaladieEnSousPartie(m,keyword,dict_maladies_bis,i):
    sentences= dict_maladies[m].split(keyword)
    dict_maladies_bis[m]['introduction_'+str(i+1)]=sentences[0]
    dict_maladies_bis[m]['your_doctor_visit_'+str(i+1)]=sentences[1]


def generalDecoupeMaladieEnSousPartie():
    for i,m in enumerate(dict_maladies):
        dict_maladies_bis[m]={}
        if(keywords[0] in dict_maladies[m]):
            decoupeMaladieEnSousPartie(m,keywords[0],dict_maladies_bis,i)
            if(keywords[1] in dict_maladies_bis[m]['your_doctor_visit_'+str(i+1)]):
                sentences =dict_maladies_bis[m]['your_doctor_visit_'+str(i+1)].split(keywords[1])
                dict_maladies_bis[m]['your_doctor_visit_'+str(i+1)]=sentences[0]
                dict_maladies_bis[m]['question_'+str(i+1)]=sentences[1]
                if(keywords[len(keywords)-1] in dict_maladies_bis[m]['question_'+str(i+1)]):
                    pass
                elif('Option#' in dict_maladies_bis[m]['question_'+str(i+1)]):
                    sentencesOption =dict_maladies_bis[m]['question_'+str(i+1)].split(keywords[2])
                    del dict_maladies_bis[m]['question_'+str(i+1)]
                    for s in range(0,len(sentencesOption)):
                        
                        sentencesOptionBis =sentencesOption[s].split(keywords[4])
                        dict_maladies_bis[m]['option_'+str(i+1)+'_'+str(s)]=sentencesOptionBis[0]
                        sentencesReponse =sentencesOptionBis[1].split(keywords[3])
                        dict_maladies_bis[m]['question_'+str(i+1)+'_'+str(s)]=sentencesReponse[0]
                        dict_maladies_bis[m]['reponse_'+str(i+1)+'_'+str(s)]=sentencesReponse[1]
                        generalDecoupeReponseSummary( dict_maladies_bis,m,i,s)
                else:
                    sentences =dict_maladies_bis[m]['question_'+str(i+1)].split(keywords[3])
                    del dict_maladies_bis[m]['question_'+str(i+1)]
                    dict_maladies_bis[m]['question_'+str(i+1)+'_0']=sentences[0]
                    dict_maladies_bis[m]['reponse_'+str(i+1)+'_0']=sentences[1]
                    generalDecoupeReponseSummary( dict_maladies_bis,m,i,'0')
                    
        
generalDecoupeMaladieEnSousPartie()

with open('dict_maladies_bis.json', 'w+',encoding="utf-16") as fp:
    json.dump(dict_maladies_bis, fp, indent=4)


with open('helperFiles/modelEn.json', 'r+',errors='ignore',encoding='utf-8') as f:
    dict_modelEn = json.load(f)

dict_basicSansQstCom =dict_modelEn.copy()

#Part en.json
def ajoutQuestionRepMaladies(k):
    for ke in dict_maladies_bis[k]:
        dict_basicSansQstCom['keys'][ke]=''
        dict_basicSansQstCom['keys'][ke]=dict_maladies_bis[k][ke]

def generalAjoutQuestionRepMaladies():
    for k in dict_maladies_bis:
        ajoutQuestionRepMaladies(k)
            

generalAjoutQuestionRepMaladies()
tabNomMaladieKey =[]

def ajoutKeysMaladies():
    for k,v in enumerate(dict_maladies_bis):
        dict_basicSansQstCom['keys'][v[0]+str(k+1)]=v
        tabNomMaladieKey.append(v[0]+str(k+1))
        dict_basicSansQstCom['keys']['introduction']='introduction'
        dict_basicSansQstCom['keys']['your doctor visit']='your doctor visit'
        dict_basicSansQstCom['keys']['questionnaire']='questionnaire'
    
ajoutKeysMaladies()

alphabets =['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'L', 'M', 'N', 'O', 'P', 'S', 'T', 'U', 'V', 'W', 'Y']


dict_decoupagekeyParAlphab={}
listingFourchetteNomMaladies=[]

def remplissageFourchette(keyAlphabet,alphabet):
    s =[]
    for key,value in enumerate(dict_maladies_bis):
        if(value[0]==alphabet.upper()):
            s.append(value[0]+str(key+1))
    dict_decoupagekeyParAlphab[str(s[0])+'-'+s[len(s)-1]]='Diseases that begin with the letter '+str(alphabet.upper())
    if(not str(s[0])+'-'+s[len(s)-1] in listingFourchetteNomMaladies ):
        listingFourchetteNomMaladies.append(str(s[0])+'-'+s[len(s)-1])
    

def remplissageFourchetteGenral():
    for keyAlphabet ,alphabet in enumerate(alphabets):
        remplissageFourchette(keyAlphabet,alphabet)

remplissageFourchetteGenral()


tab =[]

def ajoutFourchetteQuestionAlphabet():
    for key in dict_decoupagekeyParAlphab:
        dict_basicSansQstCom['keys'][key]= dict_decoupagekeyParAlphab[key]
        tab.append(key)
    dict_basicSansQstCom['keys']['question']=keywords[8]
    for alphabet in alphabets:
        dict_basicSansQstCom['keys'][alphabet]=alphabet

ajoutFourchetteQuestionAlphabet()


remplissageFourchetteGenral()




with open('helperFiles/modelDico.json', 'r+',errors='ignore',encoding='utf-8') as f:
    dict_modelDico = json.load(f)

print('en.json est pret !')

#Part 2 developpement script questions.ts
fileTs =open("questions.ts",'r+',encoding="utf-8")
fileTsBis =open("helperFiles/questionsBis.txt",'r+',encoding="utf-8")

def healperWrite_DictKey(dicto_copy):
    s='{\n'
    for key in dicto_copy:
        if(key !='options' and key!='category' and key!='nextQuestionMap'and key!='scoreMap'):
            s =s +'\t'+ str(key) +':'+"'"+str(dicto_copy[key])+"'"+',\n'
        else:
            s =s  +'\t'+ str(key) +':'+str(dicto_copy[key])+',\n'
    fileTs.write(s[:-2]+'\n},')


def listeAlphabetQuestTs():
    fileTs.truncate(0)
    fileTs.seek(0,0)
    fileTs.write(fileTsBis.read())
    fileTs.write('//*******************PARTIE QUESTOIN :CHOIX PREMIERE LETTERE DE LA MALADIE************\n')
    dict_modelDico_Bis =dict_modelDico.copy()
    dict_modelDico_Bis['id']='question'
    dict_modelDico_Bis['category']='CATEGORIES.PERSONAL'
    dict_modelDico_Bis['text']= dict_modelDico_Bis['id']
    dict_modelDico_Bis['options']=alphabets   
    dict_modelDico_Bis['nextQuestionMap']=listingFourchetteNomMaladies
    for i in range(0,len(listingFourchetteNomMaladies)):
        dict_modelDico_Bis['scoreMap'].append(0)
    healperWrite_DictKey(dict_modelDico_Bis)

listeAlphabetQuestTs()

def vidage_dict_basicTsHealper():
    for key in dict_modelDico:
        if(key !="options" and key !="nextQuestionMap" and key !="scoreMap"):
            dict_modelDico[key]=''
        else:
            dict_modelDico[key]=[]

def ajoutKeyDetailFourchette(id,nb1,nb2):
    vidage_dict_basicTsHealper()
    dict_bis =dict_modelDico.copy()
    fileTs.write('\n//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE '+str(id)+'\n')
    dict_bis['id']=id
    dict_bis['category']='CATEGORIES.PERSONAL'
    dict_bis['text']=id
    for i in range(nb1,nb2+1):
        dict_bis['options'].append(id[0]+str(i))
    for i in range(nb1,nb2+1):
         dict_bis['nextQuestionMap'].append(id[0]+str(i))
    for j in range(0,len( dict_bis['options'])):
         dict_bis['scoreMap'].append(0) 
    dict_bis['inputType']= 'radio'
    healperWrite_DictKey(dict_bis)
    


# Ces deux fonctions ont pour but d'ajouter le detail la premiere qst du formu a afficher aprés le choix de maladie
def fourchetteEnumeration(id):
    sentences=id.split('-')
    s1=sentences[0].split(id[0])
    s2=sentences[1].split(id[0])
    nb1 =s1[1]
    nb2=s2[1]
    ajoutKeyDetailFourchette(id,int(nb1),int(nb2))

def generalFoucrchetteEnumeration():
    fileTs.write('\n//**********AJOUT DELIMITATION MALADIE ALPHABETS DE A.. a A..****************\n')
    for id in listingFourchetteNomMaladies:  
        fourchetteEnumeration(id)

generalFoucrchetteEnumeration()

dict_OptionOupas ={}

def optionExisteOuPas(k,v):
    tabOptionOuQuestion =[]
    for key in dict_maladies_bis[v]:
        if('option_' in key ):
            tabOptionOuQuestion.append(key)
    if(tabOptionOuQuestion ==[]):
        tabOptionOuQuestion.append('question_'+str(k+1)+'_0')
    dict_OptionOupas[v[0]+str(k+1)]=tabOptionOuQuestion


def generalOptionExisteOuPas():
    for k,v in enumerate(dict_maladies_bis):
        optionExisteOuPas(k,v)

generalOptionExisteOuPas()

tabOptions =[]

def decoupeMaladieIntroYourDoctor(i,nom):
    vidage_dict_basicTsHealper()
    dict_bis =dict_modelDico.copy()
    fileTs.write('\n//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE '+str(nom)+'\n')
    dict_bis['id']=nom
    dict_bis['category']='CATEGORIES.PERSONAL'
    dict_bis['text']=nom
    dict_bis['options']=[]
    dict_bis['options'].append('introduction')
    dict_bis['options'].append('your doctor visit')
    test =False
    for id,value in enumerate(dict_maladies_bis):
        if(id == i):
            for ex in dict_maladies_bis[value]:
                if('question_'in ex):
                    dict_bis['options'].append('questionnaire')
                    test=True
                    break
    dict_bis['nextQuestionMap'].append('introduction_'+str(i+1))
    dict_bis['nextQuestionMap'].append('your_doctor_visit_'+str(i+1))
    if(test == True):
        if(len(dict_OptionOupas[nom])<2 ):
            dict_bis['nextQuestionMap'].append(dict_OptionOupas[nom][0])
        else:   
            dict_bis['nextQuestionMap'].append(dict_OptionOupas[nom][0]+'-'+dict_OptionOupas[nom][len(dict_OptionOupas[nom])-1])
            tabOptions.append(dict_OptionOupas[nom][0]+'-'+dict_OptionOupas[nom][len(dict_OptionOupas[nom])-1])
    for j in range(0,len( dict_bis['options'])):
        dict_bis['scoreMap'].append(0) 
    dict_bis['inputType']= 'radio'
    healperWrite_DictKey(dict_bis)
    

def generalDecoupeMaladieIntroYourDoctor():
    for i in range(0,len(tabNomMaladieKey)):
        decoupeMaladieIntroYourDoctor(i,tabNomMaladieKey[i])

generalDecoupeMaladieIntroYourDoctor()

def generalOptionFourchette():
    for i,k in enumerate(dict_OptionOupas):
        if(len(dict_OptionOupas[k])>1):
            vidage_dict_basicTsHealper()
            dict_bis =dict_modelDico.copy()
            fileTs.write('\n//PARTIE DECOUPE OPTION MALADIE '+str(k)+'\n')
            dict_bis['id']=dict_OptionOupas[k][0]+'-'+dict_OptionOupas[k][len(dict_OptionOupas[k])-1]
            dict_bis['category']='CATEGORIES.PERSONAL'
            dict_bis['text']=dict_bis['id']
            for l in dict_OptionOupas[k]:
                dict_bis['options'].append(l)
            for j in range(0,len(dict_bis['options'])):
                dict_bis['nextQuestionMap'].append('question_'+str(i+1)+'_'+str(j))
            for m in range(0,len( dict_bis['options'])):
                dict_bis['scoreMap'].append(0) 
            dict_bis['inputType']= 'radio'
            healperWrite_DictKey(dict_bis)
           

def ajoutFourchetteOptionEn():
    for id in tabOptions:
        dict_basicSansQstCom['keys'][id] ="Please choose one option "

ajoutFourchetteOptionEn()

with open('en.json', 'w+',encoding="utf-16") as fp:
    json.dump(dict_basicSansQstCom, fp, indent=4)

generalOptionFourchette()

tab_summary =[]

def decoupeReponseEnvoieSummary(id,k,i,j):
    vidage_dict_basicTsHealper()
    dict_bis =dict_modelDico.copy()
    dict_bis['id']=id
    dict_bis['text']=id
    dict_bis['category']='CATEGORIES.SYMPTOMS'
    dict_bis['inputType']='radio'
    dict_bis['options']=[]
    dict_bis['options'].append('ok')
    dict_bis['nextQuestionMap']=[]
    dict_bis['nextQuestionMap'].append(str(k)+str(i)+str(j))
    for l in range(0,len(dict_bis['options'])):
        dict_bis['scoreMap'].append(1)
    fileTs.write('\n//PARTIE REPONSE SUMMARY '+str(k)+'\n')
    healperWrite_DictKey(dict_bis)


def  decoupeQuestionRepSummaryComment(k,v):
    i=0
    j=0
    vidage_dict_basicTsHealper()
    dict_bis =dict_modelDico.copy()
    for key in dict_maladies_bis[v]:
        
        if('question_'+str(k)+'_'+str(i) in key):
            dict_bis['id']=key
            dict_bis['text']=dict_bis['id']
            dict_bis['category']='CATEGORIES.SYMPTOMS'
            dict_bis['inputType']='radio'
            dict_bis['options']=[]
            dict_bis['nextQuestionMap']=[]
            for key in dict_maladies_bis[v]:
                if('comment_'+str(k)+'_'+str(i) == key):
                    dict_bis['comment']='comment_'+str(k)+'_'+str(i)
                elif('reponse_'+str(k)+'_'+str(i)+'_'+str(j) == key):
                    dict_bis['options'].append(key)
                    dict_bis['nextQuestionMap'].append(key)
                    decoupeReponseEnvoieSummary('reponse_'+str(k)+str(i)+str(j),k,i,j)
                    tab_summary.append(str(k)+str(i)+str(j))
                    j+=1
            for l in range(0,len(dict_bis['options'])):
                dict_bis['scoreMap'].append(1)
           
            i=i+1
            j=0
            fileTs.write('\n//PARTIE DECOUPE QUESTION SUMMARY '+str(k)+'\n')
            healperWrite_DictKey(dict_bis)
            

def generalDecoupeQuestionRepSummaryComment():
    for k,v in enumerate(dict_maladies_bis):
        decoupeQuestionRepSummaryComment(k+1,v)
    
    
generalDecoupeQuestionRepSummaryComment()   




def introductionDoctorVisitTs(s):
    for k,v in enumerate(dict_maladies_bis):
        vidage_dict_basicTsHealper()
        dict_bis =dict_modelDico.copy()
        dict_bis['id']=s+str(k+1)
        dict_bis['text']=s+str(k+1)
        dict_bis['category']='CATEGORIES.SYMPTOMS'
        dict_bis['inputType']='radio'
        dict_bis['options']=['home']
        dict_bis['nextQuestionMap']=['question']
        healperWrite_DictKey(dict_bis)


introductionDoctorVisitTs('introduction_')
introductionDoctorVisitTs('your_doctor_visit_')

def endOfFile():
    fileTs.write('\n]')

endOfFile()    
print('questions.ts est pret !')
f1 =open('partie1questionnaire.txt','r+',encoding='utf-8')
f2 =open('partie2questionnaire.txt','r+',encoding='utf-8')
q =open('questionnaire.tsx','w+',encoding='utf-8')

def ifSummary():
    s ='if ('
    for k in range(0,len(tab_summary)):
        s=s+'nextQuestionId == '+"'"+str(tab_summary[k])+"'"+' || '
    s =s+')'
    return s



def remplirQustionnaireTsx():
    q.write(f1.read())
    q.write(ifSummary())
    q.write(f2.read())

remplirQustionnaireTsx()



