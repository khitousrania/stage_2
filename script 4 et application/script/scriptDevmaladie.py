import json 

with open('dictionnaire/data.json', 'r',errors='ignore',encoding='utf-8') as f:
    json_dict_maladies = json.load(f)

listdrug=open('drugliste.txt','r+',encoding='utf-8')
csV2 = open('listing.csv','w+',encoding='utf-8')
listingdrug=[]
def remlirCSV2():
    for ligne in listdrug:
        ligneLu = ligne.strip()
        if(ligneLu =='\n' or ligneLu == '' ):
            pass
        else:
            csV2.write(ligneLu.upper()+'\n')
            listingdrug.append(ligneLu) 
remlirCSV2()
keyDrug = listingdrug 




dict_decoupe_cough={}

def decoupeCough():
    bloc = json_dict_maladies['COUGH AND COLD MEDICINES']
    dict_decoupe_cough['COUGH AND COLD MEDICINES']={}
    if('#' in bloc):
        sentences =bloc.split('#')
        for keyIntro in range(0,len(sentences)):
            if(keyIntro==0):
                dict_decoupe_cough['COUGH AND COLD MEDICINES']['intro_caugh']=sentences[keyIntro]
            elif (keyIntro % 2== 0 ):

                dict_decoupe_cough['COUGH AND COLD MEDICINES']['rep_option_caugh_'+str(keyIntro-2)]=sentences[keyIntro]
            else:
                dict_decoupe_cough['COUGH AND COLD MEDICINES']['option_caugh_'+str(keyIntro-1)]=sentences[keyIntro]

decoupeCough()

with open('dictionnaire/caugh.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_cough, fp, indent=4)


dict_decoupe_pregnancy={}
def decoupePregnancy():
    bloc = json_dict_maladies['PREGNANCY']
    dict_decoupe_pregnancy['PREGNANCY']={}
    if('#' in bloc):
        sentences =bloc.split('#')
        for keyIntro in range(0,len(sentences)):
            if (keyIntro % 2== 0):
                
                dict_decoupe_pregnancy['PREGNANCY']['option_pregn_'+str(keyIntro)]=sentences[keyIntro]
            else:
                dict_decoupe_pregnancy['PREGNANCY']['rep_option_pregn_'+str(keyIntro-1)]=sentences[keyIntro]

decoupePregnancy()

with open('dictionnaire/pregnancy.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_pregnancy, fp, indent=4)

dict_decoupe_preg2={}
def decoupePregnancy2():
    bloc = dict_decoupe_pregnancy['PREGNANCY']['rep_option_pregn_6']
    dict_decoupe_preg2['option_pregn_6']={}
    if('$' in bloc):
        sentences =bloc.split('$')
        for keyIntro in range(0,len(sentences)):
            if (keyIntro % 2== 0):
               
                dict_decoupe_preg2['option_pregn_6']['sous_option_pregn_'+str(keyIntro)]=sentences[keyIntro]
            else:
                dict_decoupe_preg2['option_pregn_6']['rep_sous_op_prg_'+str(keyIntro-1)]=sentences[keyIntro]

decoupePregnancy2()

with open('dictionnaire/sous_parti_pregnancy.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_preg2, fp, indent=4)


dict_decoupe_info={}



    
def decoupeInfo():
    bloc = json_dict_maladies['INFORMATION ABOUT SUBSTANCES OF ABUSE']
    dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']={}
    if('$' in bloc):
        sentences =bloc.split('$')
        for keyIntro in range(0,len(sentences)):
            for key in sentences[keyIntro]:

                decoupeS =  sentences[keyIntro].split('TITREGENERALE')
                # print(decoupeS[0],'               ', len(decoupeS))
                dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']['optinfo_'+str(keyIntro)]=decoupeS[0]
                dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']['reponse_'+str(keyIntro)]=decoupeS[1]
    return dict_decoupe_info

decoupeInfo()

with open('dictionnaire/infoprem.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_info, fp, indent=4)







dict_decoupe_glossary={}

def decoupeGlossary():
    bloc = json_dict_maladies['GLOSSARY']
    dict_decoupe_glossary['GLOSSARY']={}
    if('#' in bloc):
        sentences =bloc.split('#')
        for keyIntro in range(0,len(sentences)):
            #print(keyIntro)
            if(keyIntro==0):
                dict_decoupe_glossary['GLOSSARY']['intro_glossary']=sentences[keyIntro]
            elif (keyIntro % 2== 0 ):

                dict_decoupe_glossary['GLOSSARY']['rep_option_glossary_'+str(keyIntro-2)]=sentences[keyIntro]
            else:
                dict_decoupe_glossary['GLOSSARY']['option_glossary_'+str(keyIntro-1)]=sentences[keyIntro]

decoupeGlossary()

with open('dictionnaire/glossary.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_glossary, fp, indent=4)

dict_decoupe_medical={}

def decoupeMedical():
    bloc = json_dict_maladies['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']
    dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']={}
    if('#' in bloc):
        sentences =bloc.split('#')
        for keyIntro in range(0,len(sentences)):
            if (keyIntro== 0):
                dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']['intro_medical']=sentences[keyIntro]
            elif (keyIntro % 2== 0 ):

                dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']['rep_option_medical_'+str(keyIntro-2)]=sentences[keyIntro]
            else:
                dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']['option_medical_'+str(keyIntro-1)]=sentences[keyIntro]
decoupeMedical()

with open('dictionnaire/medicalcond.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_medical, fp, indent=4)

dict_add ={}
#######################ADDITIONAL###############################


def additioanalSplit():
    s =json_dict_maladies['ADDITIONAL DRUG INTERACTIONS']
    if('$' in s):
        sentences = s.split('$')   # chaque drug
        for k in range(0,len(sentences)):
            titrePartie = sentences[k].split('TITREGENERALE')
            dict_add['option_add_'+str(k)] ={}
            dict_add['option_add_'+str(k)]['option_add_'+str(k)] = titrePartie[0]
            
            if('¤' in titrePartie[1]):
                tab = titrePartie[1].split('¤')
                # print(titrePartie[0], len(tab) )
                for i in range(0,len(tab)):
                    if(i == 0):
                        pass
                    else:
                        tabDrep =tab[i].split('@')
                        dict_add['option_add_'+str(k)]['sous_option_add_'+str(k)+'_'+str(i)] = tabDrep[0]
                        dict_add['option_add_'+str(k)]['rep_sous_option_add_'+str(k)+'_'+str(i)] = tabDrep[1]
            else :
                dict_add['option_add_'+str(k)]['ss_option_add_'+str(k)+'_'+str(0)] = titrePartie[1]
    return dict_add

additioanalSplit()
################################################################
with open('dict_add.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_add, fp, indent=4)



with open('dictionnaire/dict_decoupage.json', 'r+',errors='ignore',encoding='utf-8') as f:
    decoupage = json.load(f)

dict_drugChartSplit_decoupage={}
def drugChartSplit():
    
    for i,keyV in enumerate(decoupage.values()):
        dict_drugChartSplit_decoupage[keyDrug[i]]={} 
        if ('$' in keyV):
                sentences= keyV.split('$')
                
                for keyIntro in range(0,len(sentences)):
                    if (keyIntro== 0):
                        pass
                    else:
                        dict_drugChartSplit_decoupage[keyDrug[i]]['reste_a_spliter_'+str(i)+str(keyIntro)]=sentences[keyIntro]

drugChartSplit()

with open('dictionnaire/dict_decoupageChart.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_drugChartSplit_decoupage, fp, indent=4)
######################

dict_drugChartSplit_decoupage_deux={}
def drugChartSplit2():
    for x, key in enumerate(dict_drugChartSplit_decoupage):
        dict_drugChartSplit_decoupage_deux[key]={}
        
        for i,keyV in enumerate(dict_drugChartSplit_decoupage[key].values()):
            #  print(keyV)
             if ('#' in keyV):      
                    sentences= keyV.split('#')
                    for keyIntro in range(0,len(sentences)):
                        #print(keyIntro)
                        if (keyIntro== 0):
                            dict_drugChartSplit_decoupage_deux[key]['option_drug_'+str(x)+str(i)]=sentences[keyIntro]
                        elif (keyIntro % 2== 0 ):
                            dict_drugChartSplit_decoupage_deux[key]['rep_sous_option_drug_'+str(x)+str(i)+'_'+str(keyIntro-1)]=sentences[keyIntro]
                        else:
                            dict_drugChartSplit_decoupage_deux[key]['sous_option_drug_'+str(x)+str(i)+'_'+str(keyIntro)]=sentences[keyIntro]

drugChartSplit2()

with open('dictionnaire/dict_decoupageChart2.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_drugChartSplit_decoupage_deux, fp, indent=4)
    
with open('FilesHelper/modelEn.json', 'r+',errors='ignore',encoding='utf-8') as f:   
    dict_modelEn = json.load(f)
dict_json_final =dict_modelEn.copy()



def enFinal():
    
    for key in (dict_decoupe_cough['COUGH AND COLD MEDICINES']):
        if(key=='intro_caugh'):
            dict_json_final['keys']['intro_caugh']=dict_decoupe_cough['COUGH AND COLD MEDICINES'][key]
        for i in range(0, 10):
            if (i % 2 == 0):
                if(key =='option_caugh_'+str(i) ):
                    dict_json_final['keys']['option_caugh_'+str(i)]=dict_decoupe_cough['COUGH AND COLD MEDICINES'][key]
                elif(key =='rep_option_caugh_'+str(i)):
                  dict_json_final['keys']['rep_option_caugh_'+str(i)]=dict_decoupe_cough['COUGH AND COLD MEDICINES'][key]
    for key2 in dict_decoupe_pregnancy['PREGNANCY']:
        for x in range(0, 10):
            if (x % 2 == 0):
                if(key2 =='option_pregn_'+str(x) ):
                    dict_json_final['keys']['option_pregn_'+str(x)]=dict_decoupe_pregnancy['PREGNANCY'][key2]
                elif(key2 =='rep_option_pregn_'+str(x)):
                  dict_json_final['keys']['rep_option_pregn_'+str(x)]=dict_decoupe_pregnancy['PREGNANCY'][key2]

    for key3 in dict_decoupe_preg2['option_pregn_6']:
        for v in range(0, 8):
            if (v % 2 == 0):
                if(key3 =='rep_sous_op_prg_'+str(v) ):
                    dict_json_final['keys']['rep_sous_op_prg_'+str(v)]=dict_decoupe_preg2['option_pregn_6'][key3]
                elif(key3 =='sous_option_pregn_'+str(v)):
                  dict_json_final['keys']['sous_option_pregn_'+str(v)]=dict_decoupe_preg2['option_pregn_6'][key3]
    for key4 in dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']:
        for w in range(0, 503):
            if(key4=='intro_medical'):
                 dict_json_final['keys']['intro_medical']=dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS'][key4]
            if (w % 2 == 0):
                if(key4 =='option_medical_'+str(w) ):
                    dict_json_final['keys']['option_medical_'+str(w)]=dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS'][key4]
                elif(key4 =='rep_option_medical_'+str(w)):
                  dict_json_final['keys']['rep_option_medical_'+str(w)]=dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS'][key4]
    
    for key5 in dict_decoupe_glossary['GLOSSARY']:
        for w in range(0, 787):
            if(key5=='intro_glossary'):
                 dict_json_final['keys']['intro_glossary']=dict_decoupe_glossary['GLOSSARY'][key5]
            if (w % 2 == 0):
                if(key5 =='option_glossary_'+str(w) ):
                    dict_json_final['keys']['option_glossary_'+str(w)]=dict_decoupe_glossary['GLOSSARY'][key5]
                elif(key5 =='rep_option_glossary_'+str(w)):
                  dict_json_final['keys']['rep_option_glossary_'+str(w)]=dict_decoupe_glossary['GLOSSARY'][key5]

    for key6 in dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']:
        dict_json_final['keys'][key6] =dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key6]
    for key7 in dict_add:
        for key8 in dict_add[key7]:
            dict_json_final['keys'][key8] =dict_add[key7][key8]
    for key9 in dict_drugChartSplit_decoupage_deux :
        for key10 in dict_drugChartSplit_decoupage_deux[key9]:
            dict_json_final['keys'][key10] =dict_drugChartSplit_decoupage_deux[key9][key10]

    return dict_json_final                  
dict_essai={}

def jsonChart():
    for x, key in enumerate(dict_drugChartSplit_decoupage):
        for i,keyV in enumerate(dict_drugChartSplit_decoupage[key].values()):
            for u,keyV2 in enumerate(dict_drugChartSplit_decoupage_deux[key]):
                if(keyV2== 'option_drug_'+str(x)+str(i)): 
                    dict_json_final['keys']['option_drug_'+str(x)+str(i)]=dict_drugChartSplit_decoupage_deux[key][keyV2]
                for keyIntro in range(0,50):
                    if(keyV2=='sous_option_drug_'+str(x)+str(i)+'_'+str(keyIntro)):
                        
                        dict_json_final['keys']['sous_option_drug_'+str(x)+str(i)+'_'+str(keyIntro)]=dict_drugChartSplit_decoupage_deux[key][keyV2]
                    elif(keyV2=='rep_sous_option_drug_'+str(x)+str(i)+'_'+str(keyIntro)):
                     
                        dict_json_final['keys']['rep_sous_option_drug_'+str(x)+str(i)+'_'+str(keyIntro)]=dict_drugChartSplit_decoupage_deux[key][keyV2]
jsonChart()



def enFinalCHECK():
    for key in (json_dict_maladies.values()):
        if('CHECKLIST FOR SAFER DRUG USE'in key ):
            sentences=key.split('CHECKLIST FOR SAFER DRUG USE')
            dict_json_final['keys']['contenu_check']=sentences[1]
        
                

enFinal()
enFinalCHECK()

dict_list_drug_entier={}
def lisDrug(dict_list_drug_entier):
    
    for i in range(0,len(keyDrug)):
        dict_json_final['keys'][keyDrug[i][0]+str(i)]=keyDrug[i]
        dict_list_drug_entier[keyDrug[i][0]+str(i)]= keyDrug[i]
        
lisDrug(dict_list_drug_entier)
###################################################################################
alphabets =['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N','O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X','Z']


def remlissageFinalGeneral():
    dict_json_final['keys']['question']="What's the first letter of the drug that you are looking for?"
    dict_json_final['keys']['check']="Checklist for safer drug use"
    dict_json_final['keys']['prenancy']="Pregnancy"
    dict_json_final['keys']['glossary']="Glossary"
    dict_json_final['keys']['caugh']="Cough and cold medicines"
    dict_json_final['keys']['drug chart']="Drug chart"
    dict_json_final['keys']['info']="Information about substances of abuse"
    dict_json_final['keys']['additional']="Additional drug interactions"
    dict_json_final['keys']['medical']="Medical conditions and their commonly used drugs"
    dict_json_final['keys']['Intro_premiere']="Home"
    dict_json_final['keys']['drug_lettre']="alphabet"
    
    for key ,alphabet in enumerate(alphabets):
        dict_json_final['keys'][str(alphabet.upper())]= " "+alphabet.upper()


dict_decoupagekeyParAlphab={}
listingFourchetteNomMaladies=[]
def remplissageFourchette(keyAlphabet):
    s =[]
    for key in dict_list_drug_entier:
        if(key[0]==alphabets[keyAlphabet].upper()):
            s.append(key)
            
    dict_decoupagekeyParAlphab[str(s[0])+'-'+s[len(s)-1]]='Drugs that begin with the letter '+str(alphabets[keyAlphabet].upper())
    listingFourchetteNomMaladies.append(str(s[0])+'-'+s[len(s)-1])


def remplissageFourchetteGenral():
    
    for keyAlphabet ,alphabet in enumerate(alphabets):
            remplissageFourchette(keyAlphabet)

remplissageFourchetteGenral()    
remlissageFinalGeneral()
tab=[]
def ajoutFourchette():
    for key in dict_decoupagekeyParAlphab:
        dict_json_final['keys'][key]= dict_decoupagekeyParAlphab[key]
        tab.append(key)

ajoutFourchette()

fileTs = open('questions.ts','w+',encoding='utf-8',errors='ignore')
fileTsBasique = open('basicTSdebut.txt','r+',encoding='utf-8')
with open('dictionnaire/accueil.json', 'r+',errors='ignore',encoding='utf-8') as f:
    dictMaladieTs = json.load(f)
with open('dictionnaire/fourchette.json', 'w',encoding="utf-16") as fp:
      json.dump(dict_decoupagekeyParAlphab, fp, indent=4)
def healperWrite_DictKey(dicto_copy):
    s='{\n'
    for key in dicto_copy:
        if(key !='options' and key!='category' and key!='nextQuestionMap'and key!='scoreMap'):
            s =s +'\t'+ str(key) +':'+"'"+str(dicto_copy[key])+"'"+',\n'
        else:
            s =s  +'\t'+ str(key) +':'+str(dicto_copy[key])+',\n'
    fileTs.write(s[:-2]+'\n},')


tab_decoupeFourchette=[]
with open('basicTShealper.json', 'r+',errors='ignore',encoding='utf-8') as f:
    dict_basicTsHealper = json.load(f) 
with open('dictionnaire/check_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
    check = json.load(f)

def checkf():
    for i in dict_json_final['keys']:
        dic1_copy =check.copy()
        dic1_copy['text']='check'
        if('contenu_check' in i ):
            dic1_copy['comment']= str(i)
            dic1_copy['nextQuestionMap']=[]
            dic1_copy['nextQuestionMap'].append('Intro_premiere')
            healperWrite_DictKey(dic1_copy)



with open('dictionnaire/accueil.json', 'r+',errors='ignore',encoding='utf-8') as f:
    hom = json.load(f)
def detail():
            di_copy =hom.copy()
            di_copy['nextQuestionMap']=[]
            di_copy['id']=('Intro_premiere')
            di_copy['text']=('Intro_premiere')
            di_copy['nextQuestionMap']=['check']
            di_copy['nextQuestionMap'].append('prenancy')
            di_copy['nextQuestionMap'].append('glossary')
            di_copy['nextQuestionMap'].append('caugh')
            di_copy['nextQuestionMap'].append('question')
            di_copy['nextQuestionMap'].append('info')
            di_copy['nextQuestionMap'].append('additional')
            di_copy['nextQuestionMap'].append('medical')

            #print(i)
            healperWrite_DictKey(di_copy)
with open('dictionnaire/accueil_drug.json', 'r+',errors='ignore',encoding='utf-8') as f:
    acc_drug = json.load(f)

def accueil_drug(): 
            dic_rempl_ex=acc_drug.copy()
            dic_rempl_ex['nextQuestionMap']=[]
            dic_rempl_ex['id']=('drug chart')
            dic_rempl_ex['text']=('question')
            dic_rempl_ex['options']=['drug_lettre']
            dic_rempl_ex['nextQuestionMap']=['question']
            #print(i)
            healperWrite_DictKey(dic_rempl_ex)
            dic_rempl_ex={}

###########################CAUGHT########################################################
with open('dictionnaire/caugh_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
    caug = json.load(f)

def RemplirCaugh(): 
    dic_rempl_ex=caug.copy()
    dic_rempl_ex['nextQuestionMap']=[] 
    for x , key in enumerate(dict_decoupe_cough['COUGH AND COLD MEDICINES']):
        
        dic_rempl_ex['comment']='intro_caugh'
        
        if ((x) % 2==0):
            if(x==10):
                pass 
            else:
                dic_rempl_ex['options'].append('option_caugh_'+str(x))
                dic_rempl_ex['nextQuestionMap'].append('rep_option_caugh_'+str(x))
    healperWrite_DictKey(dic_rempl_ex)


with open('dictionnaire/reponse_caugh.json', 'r+',errors='ignore',encoding='utf-8') as f:
    rep_caug = json.load(f)

def rep_caugh():
    dic1_copy =rep_caug.copy()
    dic1_copy['nextQuestionMap']=[]
    dic1_copy['nextQuestionMap'].append('Intro_premiere')
    for x , key in enumerate(dict_decoupe_cough['COUGH AND COLD MEDICINES']):
       # print(x)
        if(x%2==0):
            if(x==10):
                pass
            else:
                
                dic1_copy['id']='rep_option_caugh_'+str(x)
                dic1_copy['text']='option_caugh_'+str(x)
                dic1_copy['comment']= 'rep_option_caugh_'+str(x)
                
                
                #print(dic_copy['comment'])
                healperWrite_DictKey(dic1_copy)
    dic1_copy={}    
###################GLOSSARY################################
with open('dictionnaire/glossary_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
    gloss = json.load(f)

def RemplirGlossary(): 
    dic_rempl_ex=gloss.copy()
    dic_rempl_ex['nextQuestionMap']=[] 
    for x , key in enumerate(dict_decoupe_glossary['GLOSSARY']):
        dic_rempl_ex['comment']='intro_glossary'

        if ((x) % 2==0):
                dic_rempl_ex['options'].append('option_glossary_'+str(x))
                if(x==781):
                    pass 
                else:
                     dic_rempl_ex['nextQuestionMap'].append('rep_option_glossary_'+str(x))
    healperWrite_DictKey(dic_rempl_ex)


with open('dictionnaire/reponse_glossary.json', 'r+',errors='ignore',encoding='utf-8') as f:
    rep_gloss = json.load(f)

def rep_glossary():
    dic1_copy =rep_gloss.copy()
    dic1_copy['nextQuestionMap']=[]
    dic1_copy['nextQuestionMap'].append('Intro_premiere')
    for x , key in enumerate(dict_decoupe_glossary['GLOSSARY']):
        #print(x)
        if(x%2==0):
            if(x==782):
                pass
            else:
                
                dic1_copy['id']='rep_option_glossary_'+str(x)
                dic1_copy['text']='option_glossary_'+str(x)
                dic1_copy['comment']= 'rep_option_glossary_'+str(x)
                
                
                healperWrite_DictKey(dic1_copy)
    dic1_copy={}  
#####################MEDICAL##############################
with open('dictionnaire/medical_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
    med = json.load(f)

def RemplirMedicalCond(): 
    dic_rempl_ex=med.copy()
    dic_rempl_ex['nextQuestionMap']=[] 
    dic_rempl_ex['id']="medical"
    dic_rempl_ex['text']="medical"

    for x , key in enumerate(dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']):
        dic_rempl_ex['comment']='intro_medical'

        if ((x) % 2==0):
                dic_rempl_ex['options'].append('option_medical_'+str(x))                  
                dic_rempl_ex['nextQuestionMap'].append('rep_option_medical_'+str(x))
    healperWrite_DictKey(dic_rempl_ex)


with open('dictionnaire/reponse_medical.json', 'r+',errors='ignore',encoding='utf-8') as f:
    rep_med = json.load(f)

def rep_medicalCond():
    dic1_copy =rep_med.copy()
    dic1_copy['nextQuestionMap']=[]
    dic1_copy['nextQuestionMap'].append('Intro_premiere')
    for x , key in enumerate(dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']):
       # print(x)
        if(x%2==0):
            if(x==467):
                pass
            else:                
                dic1_copy['id']='rep_option_medical_'+str(x)
                dic1_copy['text']='option_medical_'+str(x)
                dic1_copy['comment']= 'rep_option_medical_'+str(x)
                
                
                healperWrite_DictKey(dic1_copy)
    dic1_copy={}  
#####################PREGNANCY##############################
with open('dictionnaire/preg_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
    preg = json.load(f)  
def RemplirPregnancy(): 
    dic_rempl_ex=preg.copy()
    dic_rempl_ex['nextQuestionMap']=[] 
    for x , key in enumerate(dict_decoupe_pregnancy['PREGNANCY']):
        if ((x) % 2==0):
                dic_rempl_ex['options'].append('option_pregn_'+str(x))
                if(x==6):
                    dic_rempl_ex['nextQuestionMap'].append('option_pregn_6')
                else:
                     dic_rempl_ex['nextQuestionMap'].append('rep_option_pregn_'+str(x))
    healperWrite_DictKey(dic_rempl_ex)

with open('dictionnaire/reponse_pref_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
    rep_pre = json.load(f) 
def rep_preg():
    dic1_copy =rep_pre.copy()
    dic1_copy['nextQuestionMap']=[]
    dic1_copy['nextQuestionMap'].append('Intro_premiere')
    for x , key in enumerate(dict_decoupe_pregnancy['PREGNANCY']):
        if(key== 'option_pregn_6'):
            pass
        elif(x%2==0):
                if(x==6):
                    pass
                else:
                    dic1_copy['id']='rep_option_pregn_'+str(x)
                    dic1_copy['text']='option_pregn_'+str(x)
                    dic1_copy['comment']= 'rep_option_pregn_'+str(x)
                
                
                healperWrite_DictKey(dic1_copy)
    dic1_copy={}


with open('dictionnaire/sous_parti_preg_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
    opt6 = json.load(f) 
def option6():
    dic1_copy =opt6.copy()
    dic1_copy['id']='option_pregn_6'
    dic1_copy['text']='option_pregn_6'
    for x , key in enumerate(dict_decoupe_preg2['option_pregn_6']):
        if(x%2==0):
                    dic1_copy['options'].append('sous_option_pregn_'+str(x))
                    dic1_copy['nextQuestionMap'].append('rep_sous_op_prg_'+str(x))
    healperWrite_DictKey(dic1_copy)
    dic1_copy={}

with open('dictionnaire/sous_rep_prg_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
    rep_spre = json.load(f) 
def rep_sous_preg():
    dic1_copy =rep_spre.copy()

    dic1_copy['nextQuestionMap']=[]
    dic1_copy['nextQuestionMap'].append('Intro_premiere')
    dic1_copy['options'].append('Intro_premiere')
    for x , key in enumerate(dict_decoupe_preg2['option_pregn_6']):
        if(x%2==0):
                    dic1_copy['id']='rep_sous_op_prg_'+str(x)
                    dic1_copy['text']='sous_option_pregn_'+str(x)
                    dic1_copy['comment']= 'rep_sous_op_prg_'+str(x)
                
                
                    healperWrite_DictKey(dic1_copy)
    dic1_copy={}
####################INFORMATION###############################
with open('dictionnaire/information_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
    info = json.load(f)

def RemplirInformation(): 
    dic_rempl_ex=info.copy()
    dic_rempl_ex['nextQuestionMap']=[] 
    for x , key in enumerate(dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']):

        if(x>=14):
            break 
        else:
            dic_rempl_ex['options'].append('optinfo_'+str(x))
            dic_rempl_ex['nextQuestionMap'].append('reponse_'+str(x))
    healperWrite_DictKey(dic_rempl_ex)


with open('dictionnaire/reponse_information.json', 'r+',errors='ignore',encoding='utf-8') as f:
    rep_info = json.load(f)

def rep_information():
    dic1_copy =rep_info.copy()
    dic1_copy['nextQuestionMap']=[]
    dic1_copy['nextQuestionMap'].append('Intro_premiere')
    for x , key in enumerate(dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']):
        
        if(x>=15):
            break 
        else:
            dic1_copy['id']='reponse_'+str(x)
            dic1_copy['text']='optinfo_'+str(x)
            dic1_copy['comment']= 'reponse_'+str(x)
                
                
        healperWrite_DictKey(dic1_copy)
    dic1_copy={}  
####################ADDITIONAL###############################

# dictMaladieTs
def ajoutAdditionalTs():
    dict_add_ts =dictMaladieTs.copy()
    dict_add_ts['id']='additional'
    dict_add_ts['text']='additional'
    dict_add_ts['options'] =[]
    dict_add_ts['nextQuestionMap'] =[]
    for k,v in enumerate(dict_add):
        dict_add_ts['options'].append(v)
        for key , value in enumerate(dict_add[v]):
            
            if(value.split('option_add_')[0]==''):
                pass
            elif('ss_option_add' in value) :
               dict_add_ts['nextQuestionMap'].append(value) 
            elif('sous_option_add_' in value):
                dict_add_ts['nextQuestionMap'].append(v)
                break
    
    healperWrite_DictKey(dict_add_ts)

tab_reponse_drugs =[]  
def optionsAdd():
    dict_add_ts =dictMaladieTs.copy()
    for k,v in enumerate(dict_add):
        if(k == 0):
            pass
        else:
            dict_add_ts['id']=v
            dict_add_ts['text']=v
            dict_add_ts['options'] =[]
            dict_add_ts['nextQuestionMap'] =[]
            for key , value in enumerate(dict_add[v]):
                if(value.split('option_add_')[0]==''):
                    pass
                elif('ss_option_add' in value) :
                    break
                elif(value.split('sous_option_add_')[0]==''):
                    dict_add_ts['options'].append(value)
                    dict_add_ts['nextQuestionMap'].append('rep_'+str(value))
                    tab_reponse_drugs.append('rep_'+str(value))
            healperWrite_DictKey(dict_add_ts)
    return tab_reponse_drugs

def reponseDrugs(tab_reponse_drugs):
    for i in range(0,len(tab_reponse_drugs)):
        dict_add_ts =dictMaladieTs.copy()
        dict_add_ts['id']=tab_reponse_drugs[i]
        dict_add_ts['text']=tab_reponse_drugs[i].split('rep_')[1]
        dict_add_ts['comment']=tab_reponse_drugs[i]
        dict_add_ts['options'] =[]
        dict_add_ts['options'].append('home')
        dict_add_ts['nextQuestionMap'] =[]
        dict_add_ts['nextQuestionMap'].append('Intro_premiere')
        healperWrite_DictKey(dict_add_ts)


###################################################

 
with open('dictionnaire/drugChart_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
    drugChart = json.load(f)


with open('dictionnaire/reponse_drugChart.json', 'r+',errors='ignore',encoding='utf-8') as f:
    rep_drugchart = json.load(f)


####################ts: drug #################################

def tsDrugs():
    for k,v in enumerate(dict_drugChartSplit_decoupage_deux):
        print(v)
        dict_add_ts =    drugChart.copy()
        dict_add_ts['id']=v[0]+str(k)
        dict_add_ts['text']=v[0]+str(k)
        dict_add_ts['options'] =[]
        dict_add_ts['nextQuestionMap'] =[]
        for key,value in enumerate(dict_drugChartSplit_decoupage_deux[v]):
            if(value.split('option_drug_')[0] == ''):
                dict_add_ts['options'].append(value)
                dict_add_ts['nextQuestionMap'].append(value)
        healperWrite_DictKey(dict_add_ts)


tab_reponse_options_drugs =[]

def tsDrugsBis():
    for k, v in enumerate(dict_drugChartSplit_decoupage_deux):
            for key,value in enumerate(dict_drugChartSplit_decoupage_deux[v]):

                    if(value.split('option_drug_')[0]==''):   
                        
                        dict_model =drugChart.copy()
                        dict_model['id'] =value
                        dict_model['text'] =value
                        dict_model['options'] =[]
                        dict_model['nextQuestionMap'] =[]
                        for i,var in enumerate(dict_drugChartSplit_decoupage_deux[v]):
                            for j in range(0,20):
                                if('sous_'+str(value)+'_'+str(j) == var ):
                                   # print(value.split('option_drug_'+str(k))[1]==str(7))
                                    if(not(value.split('option_drug_'+str(k))[1]==str(7)) and  not(value.split('option_drug_'+str(k))[1]==str(8))and  not(value.split('option_drug_'+str(k))[1]==str(1)) and  not(value.split('option_drug_'+str(k))[1]==str(2))and  not(value.split('option_drug_'+str(k))[1]==str(5))):
                                        dict_model['options'].append(var)
                                        dict_model['nextQuestionMap'].append('rep_'+var)
                                        tab_reponse_options_drugs.append('rep_'+var)
                                    else:
                                       
                                        dict_model['nextQuestionMap'].append('Intro_premiere')
                                        dict_model['comment']=var
                                        dict_model['options'].append('home')
                                        

                        healperWrite_DictKey(dict_model)
    return tab_reponse_options_drugs

def tsDrugsBisRep(tab_reponse_options_drugs):
    for i in range(0,len(tab_reponse_options_drugs)):
        dict_add_ts =dictMaladieTs.copy()
        dict_add_ts['id']=tab_reponse_options_drugs[i]
        dict_add_ts['text']=tab_reponse_options_drugs[i].split('rep_')[1]
        dict_add_ts['comment']=tab_reponse_options_drugs[i]
        dict_add_ts['options'] =[]
        dict_add_ts['options'].append('home')
        dict_add_ts['nextQuestionMap'] =[]
        dict_add_ts['nextQuestionMap'].append('Intro_premiere')
        healperWrite_DictKey(dict_add_ts)
        

    

    
def debutQuestion_Ts():
    fileTs.truncate()
    fileTs.seek(0,0)
    fileTs.write(fileTsBasique.read())
    fileTs.write('\n//*******************PARTIE QUESTION :PREMIERE LETTRE DE LA MALADIE************\n')
    detail()
    checkf()
    RemplirPregnancy()
    rep_preg()
    option6()
    rep_sous_preg()
    RemplirCaugh()
    rep_caugh()
    accueil_drug()
    #
    RemplirGlossary()
    rep_glossary()
    #
    RemplirMedicalCond()
    rep_medicalCond()
    #
    RemplirInformation()
    rep_information()
    #
     
    ajoutAdditionalTs()
    tab_reponse_drugs =optionsAdd()
    reponseDrugs(tab_reponse_drugs)
    tsDrugs()
    tab_reponse_options_drugs =tsDrugsBis()
    tsDrugsBisRep(tab_reponse_options_drugs)
    dict_basicTsHealper['id']='question'
    dict_basicTsHealper['category']='CATEGORIES.PERSONAL'
    dict_basicTsHealper['text']= dict_basicTsHealper['id']
    dict_basicTsHealper['options']=alphabets
    for key in dict_decoupagekeyParAlphab:
        dict_basicTsHealper['nextQuestionMap'].append(key)
        tab_decoupeFourchette.append(key)
    for i in range(0,len(dict_basicTsHealper['options'])):
        dict_basicTsHealper['scoreMap'].append(0)
    healperWrite_DictKey(dict_basicTsHealper) 


############################################################################################



def vidage_dict_basicTsHealper():
    for key in dict_basicTsHealper:
        if(key !="options" and key !="nextQuestionMap" and key !="scoreMap"):
            dict_basicTsHealper[key]=''
        else:
            dict_basicTsHealper[key]=[]
           

tab_question1Formu=[]
tab_question1Formu_copieAll=[]
def recupToutePremiereKey(maladie):
    tab_question1Formu_copieAll=[]
    tab_question1Formu_copieAll=[next(iter(dict_list_drug_entier.keys()))]
    return tab_question1Formu_copieAll

def recupPremiereKeyQuestion(maladie):
    tab_question1Formu=[]
    tab_question1Formu_copieAll =recupToutePremiereKey(maladie)
    for key in tab_question1Formu_copieAll:
        if(key[0]==maladie[0]):
            tab_question1Formu.append(key)
        else:
            pass
    return tab_question1Formu
    
    
def ajoutKeyDetailFourchette(id,nb1,nb2):
    vidage_dict_basicTsHealper()
    fileTs.write('\n//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE '+str(id)+'\n')
    dict_basicTsHealper['id']=id
    dict_basicTsHealper['category']='CATEGORIES.PERSONAL'
    dict_basicTsHealper['text']= dict_basicTsHealper['id']
    for i in range(nb1,nb2+1):
        dict_basicTsHealper['options'].append(id[0]+str(i))
        dict_basicTsHealper['nextQuestionMap'].append(id[0]+str(i))

    for j in range(0,len(dict_basicTsHealper['options'])):#------------------------------aaaaaaaaaaaaa
        dict_basicTsHealper['scoreMap'].append(0) 
    dict_basicTsHealper['inputType']= 'radio'
    healperWrite_DictKey(dict_basicTsHealper)

def fourchetteEnumeration(id):
    sentences=id.split('-')
    s1=sentences[0].split(id[0])
    s2=sentences[1].split(id[0])
    nb1 =s1[1]
    nb2=s2[1]
    tab_question1Formu=[]
    ajoutKeyDetailFourchette(id,int(nb1),int(nb2))


def generalFoucrchetteEnumeration():
    fileTs.write('\n//**********AJOUT DELIMITATION MALADIE ALPHABETS DE A.. a A..****************\n')
    for id in tab_decoupeFourchette:
        fourchetteEnumeration(id)





tab_sauvegardeQuestComm=[]
#############################################################################################









print('questions.ts pret !')
if __name__ == '__main__':
    debutQuestion_Ts()
    generalFoucrchetteEnumeration()
    fileTs.write('];')







with open('basicTShealper.json', 'r+',errors='ignore',encoding='utf-8') as f:
    dict_basicTsHealper = json.load(f) 

with open('dictionnaire/dict_enFinal.json', 'w+',encoding="utf-16") as fp:
    json.dump(dict_json_final, fp, indent=4)

with open('dictionnaire/liste_drug.json', 'w+',encoding="utf-16") as fp:
    json.dump(dict_list_drug_entier, fp, indent=4)