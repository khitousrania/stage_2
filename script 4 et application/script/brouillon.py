import json 

with open('dictionnaire/data.json', 'r',errors='ignore',encoding='utf-8') as f:
    json_dict_maladies = json.load(f)

listdrug=open('drugliste.txt','r+',encoding='utf-8')
csV2 = open('listing.csv','w+',encoding='utf-8')
listingdrug=[]
def remlirCSV2():
    for ligne in listdrug:
        ligneLu = ligne.strip()
        if(ligneLu =='\n' or ligneLu == '' ):
            pass
        else:
            csV2.write(ligneLu.upper()+'\n')
            listingdrug.append(ligneLu) 
remlirCSV2()
keyDrug = listingdrug 




dict_decoupe_cough={}

def decoupeCough():
    bloc = json_dict_maladies['COUGH AND COLD MEDICINES']
    dict_decoupe_cough['COUGH AND COLD MEDICINES']={}
    if('#' in bloc):
        sentences =bloc.split('#')
        for keyIntro in range(0,len(sentences)):
            if(keyIntro==0):
                dict_decoupe_cough['COUGH AND COLD MEDICINES']['intro_caugh']=sentences[keyIntro]
            elif (keyIntro % 2== 0 ):

                dict_decoupe_cough['COUGH AND COLD MEDICINES']['rep_option_caugh_'+str(keyIntro-2)]=sentences[keyIntro]
            else:
                dict_decoupe_cough['COUGH AND COLD MEDICINES']['option_caugh_'+str(keyIntro-1)]=sentences[keyIntro]

decoupeCough()

with open('dictionnaire/caugh.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_cough, fp, indent=4)


############
# dict_decoupe_infoA={}

# def decoupeinfoA():
#     bloc = json_dict_maladies['INFORMATION ABOUT SUBSTANCES OF ABUSE']
#     dict_decoupe_infoA['INFORMATION ABOUT SUBSTANCES OF ABUSE']={}
#     if('#' in bloc):
#         sentences =bloc.split('#')
#         for keyIntro in range(0,len(sentences)):
#             #print(keyIntro)
#             if(keyIntro==0):
#                 dict_decoupe_infoA['INFORMATION ABOUT SUBSTANCES OF ABUSE']['intro_infoA']=sentences[keyIntro]
#             elif (keyIntro % 8== 0 ):

#                 dict_decoupe_infoA['INFORMATION ABOUT SUBSTANCES OF ABUSE']['rep_option_infoA_'+str(keyIntro-2)]=sentences[keyIntro]
#             else:
#                 dict_decoupe_infoA['INFORMATION ABOUT SUBSTANCES OF ABUSE']['option_infoA_'+str(keyIntro-1)]=sentences[keyIntro]

# decoupeinfoA()

# with open('dictionnaire/infoA.json', 'w',encoding="utf-16") as fp:
#     json.dump(dict_decoupe_infoA, fp, indent=4)
############
dict_decoupe_pregnancy={}
def decoupePregnancy():
    bloc = json_dict_maladies['PREGNANCY']
    dict_decoupe_pregnancy['PREGNANCY']={}
    if('#' in bloc):
        sentences =bloc.split('#')
        for keyIntro in range(0,len(sentences)):
            if (keyIntro % 2== 0):
                
                dict_decoupe_pregnancy['PREGNANCY']['option_pregn_'+str(keyIntro)]=sentences[keyIntro]
            else:
                dict_decoupe_pregnancy['PREGNANCY']['rep_option_pregn_'+str(keyIntro-1)]=sentences[keyIntro]

decoupePregnancy()

with open('dictionnaire/pregnancy.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_pregnancy, fp, indent=4)

dict_decoupe_preg2={}
def decoupePregnancy2():
    bloc = dict_decoupe_pregnancy['PREGNANCY']['rep_option_pregn_6']
    dict_decoupe_preg2['option_pregn_6']={}
    if('$' in bloc):
        sentences =bloc.split('$')
        for keyIntro in range(0,len(sentences)):
            if (keyIntro % 2== 0):
               
                dict_decoupe_preg2['option_pregn_6']['sous_option_pregn_'+str(keyIntro)]=sentences[keyIntro]
            else:
                dict_decoupe_preg2['option_pregn_6']['rep_sous_op_prg_'+str(keyIntro-1)]=sentences[keyIntro]

decoupePregnancy2()

with open('dictionnaire/sous_parti_pregnancy.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_preg2, fp, indent=4)


keyWordInfo=['Tobacco (nicotine)','Alcohol','Marijuana (cannabis, hashish)', 'Amphetamines (including ecstasy)','Anabolic Steroids', 'Barbiturates','Sedative-hypnotics (benzodiazepines, “party drugs” that include gammahydroxybutyrate and rohypnol)','Cocaine','Crystal Methamphetamine','Opiates (codeine, heroin, morphine, methadone, opium)','Phencyclidine (PCP, angeldust)','Psychedelic Drugs (LSD, mescaline)','Volatile Substances (glue, solvents, nitrous oxide,other volatile compounds)']
dict_decoupe_info={}
dict_id_drug={}

def DrugId():
    dict_id_drug['INFORMATION ABOUT SUBSTANCES OF ABUSE']={}
    for i in range (0, len(keyWordInfo)):
        
        dict_id_drug['INFORMATION ABOUT SUBSTANCES OF ABUSE']['titre_'+str(i)]=keyWordInfo[i]
        
DrugId()
with open('dictionnaire/titre_drug.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_id_drug, fp, indent=4)


    
def decoupeInfo():
    bloc = json_dict_maladies['INFORMATION ABOUT SUBSTANCES OF ABUSE']
    dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']={}
    if('#' in bloc):
        sentences =bloc.split('#')
        for keyIntro in range(0,len(sentences)):
            if (keyIntro== 0):
               
                dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']['intro_info']=sentences[keyIntro]
            else:
                dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']['suite_'+str(keyIntro-1)]=sentences[keyIntro]
    

decoupeInfo()

with open('dictionnaire/infoprem.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_info, fp, indent=4)

dict_decoupe_suite={}
def decoupeInfosuite():
    for key in dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']:
       
        if (key =='suite_10'):
            dict_decoupe_suite[key]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
            
        elif (key =='suite_11'):
            dict_decoupe_suite[key]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        elif (key =='suite_12'):
            dict_decoupe_suite[key]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        elif (key =='suite_13'):
            dict_decoupe_suite[key]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key] 
        else:
             pass


decoupeInfosuite()
with open('dictionnaire/infodeuxpourSplit.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_suite, fp, indent=4)

dict_decoupe_suite2={}
def decoupeOptionaAlcohol():
    dict_decoupe_suite2['titre_1']={}
    for i,key in enumerate(dict_decoupe_suite.values()):
        # print(key)
        if('$' in key):
                sentences =key.split('$')
                
                for keyIntro in range(0,len(sentences)):
                    
                    if (keyIntro== 0):
                        
                        dict_decoupe_suite2['titre_1']['rep_sous_option_alcohol_'+str(i-1)]=sentences[keyIntro]
                    else:
                        dict_decoupe_suite2['titre_1']['sous_option_alcohol_'+str(i)]=sentences[keyIntro]
    for key2 in dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']:
        if(key2=='suite_14'):
            dict_decoupe_suite2['titre_1']['rep_sous_option_alcohol_3']=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key2] 



decoupeOptionaAlcohol()

with open('dictionnaire/sous_option_info.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_suite2, fp, indent=4)


dict_genralInfo={}
def generaljsonInfo():
    for key in dict_decoupe_suite2['titre_1']:
        if (key== 'rep_sous_option_alcohol_-1'):
            pass
        else:
            dict_genralInfo[key]=dict_decoupe_suite2['titre_1'][key]
    for key2 in dict_id_drug['INFORMATION ABOUT SUBSTANCES OF ABUSE']:
        
        dict_genralInfo[key2]=dict_id_drug['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key2]
    for key3 in dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']:
        if(key3== 'suite_0' or key3== 'suite_9' or key3== 'suite_10' or key3== 'suite_11' or key3== 'suite_12' or key3== 'suite_13'or key3== 'suite_14' or key3== 'suite_19' or
        key3== 'suite_26' or key3== 'suite_33' or key3== 'suite_40' or
        key3== 'suite_47' or key3== 'suite_54' or key3== 'suite_61' or
        key3== 'suite_68' or key3=='suite_75' or key3=='suite_82' or key3=='suite_89' ):
            pass
    #     else:
    #         dict_genralInfo[key3]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key3]

generaljsonInfo()



def diviserDrug():
    for i, key in enumerate(dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE']): 
        
        if(key== 'suite_1' or key== 'suite_3' or
         key== 'suite_5' or key== 'suite_7' ):
            dict_genralInfo['option0_'+str(i-2)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_2' or key== 'suite_4' or
         key== 'suite_6' or key== 'suite_8' ):
            dict_genralInfo['rep_option0_'+str(i-3)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]

        if(key== 'suite_20' or key== 'suite_22' or
         key== 'suite_24'):
            dict_genralInfo['option2_'+str(i-9)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_21' or key== 'suite_23' or
         key== 'suite_25'):
            dict_genralInfo['rep_option2_'+str(i-10)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]

        if(key== 'suite_27' or key== 'suite_29' or
         key== 'suite_31'):
            dict_genralInfo['option3_'+str(i-10)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_28' or key== 'suite_30' or
         key== 'suite_32'):
            dict_genralInfo['rep_option3_'+str(i-11)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]

            
        if(key== 'suite_34' or key== 'suite_36' or
         key== 'suite_38'):
            dict_genralInfo['option4_'+str(i-11)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_35' or key== 'suite_37' or
         key== 'suite_39'):
            dict_genralInfo['rep_option4_'+str(i-12)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]

        if(key== 'suite_41' or key== 'suite_43' or
         key== 'suite_45'):
            dict_genralInfo['option5_'+str(i-12)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_42' or key== 'suite_44' or
         key== 'suite_46'):
            dict_genralInfo['rep_option5_'+str(i-13)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]


        if(key== 'suite_48' or key== 'suite_50' or
         key== 'suite_52'):
            dict_genralInfo['option6_'+str(i-13)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_49' or key== 'suite_51' or
         key== 'suite_53'):
            dict_genralInfo['rep_option6_'+str(i-14)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]


        if(key== 'suite_55' or key== 'suite_57' or
         key== 'suite_59'):
            dict_genralInfo['option7_'+str(i-14)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_56' or key== 'suite_58' or
         key== 'suite_60'):
            dict_genralInfo['rep_option7_'+str(i-15)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]


        if(key== 'suite_62' or key== 'suite_64' or
         key== 'suite_66'):
            dict_genralInfo['option8_'+str(i-15)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_63' or key== 'suite_65' or
         key== 'suite_67'):
            dict_genralInfo['rep_option8_'+str(i-16)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]

        if(key== 'suite_69' or key== 'suite_71' or
         key== 'suite_73'):
            dict_genralInfo['option9_'+str(i-16)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_70' or key== 'suite_72' or
         key== 'suite_73'):
            dict_genralInfo['rep_option9_'+str(i-17)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]

        
        if(key== 'suite_76' or key== 'suite_78' or
         key== 'suite_80'):
            dict_genralInfo['option10_'+str(i-17)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_77' or key== 'suite_79' or
         key== 'suite_81'):
            dict_genralInfo['rep_option10_'+str(i-18)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]

        if(key== 'suite_83' or key== 'suite_85' or
         key== 'suite_87'):
            dict_genralInfo['option11_'+str(i-18)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_84' or key== 'suite_86' or
         key== 'suite_88'):
            dict_genralInfo['rep_option11_'+str(i-19)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]

        if(key== 'suite_90' or key== 'suite_92' or
         key== 'suite_94'):
            dict_genralInfo['option12_'+str(i-19)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_91' or key== 'suite_93' or
         key== 'suite_95'):
            dict_genralInfo['rep_option12_'+str(i-20)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        if(key== 'suite_15' or key== 'suite_17' ):
            dict_genralInfo['option1_'+str(i-8)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]   
        if(key== 'suite_16' or key== 'suite_18' ):
                dict_genralInfo['rep_option1_'+str(i-9)]=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]
        
        if(key=='intro_info'):
            dict_genralInfo['intro_info']=dict_decoupe_info['INFORMATION ABOUT SUBSTANCES OF ABUSE'][key]




diviserDrug()
with open('dictionnaire/generalInfo.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_genralInfo, fp, indent=4)



dict_decoupe_glossary={}

def decoupeGlossary():
    bloc = json_dict_maladies['GLOSSARY']
    dict_decoupe_glossary['GLOSSARY']={}
    if('#' in bloc):
        sentences =bloc.split('#')
        for keyIntro in range(0,len(sentences)):
            #print(keyIntro)
            if(keyIntro==0):
                dict_decoupe_glossary['GLOSSARY']['intro_glossary']=sentences[keyIntro]
            elif (keyIntro % 2== 0 ):

                dict_decoupe_glossary['GLOSSARY']['rep_option_glossary_'+str(keyIntro-2)]=sentences[keyIntro]
            else:
                dict_decoupe_glossary['GLOSSARY']['option_glossary_'+str(keyIntro-1)]=sentences[keyIntro]

decoupeGlossary()

with open('dictionnaire/glossary.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_glossary, fp, indent=4)

dict_decoupe_medical={}

def decoupeMedical():
    bloc = json_dict_maladies['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']
    dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']={}
    if('#' in bloc):
        sentences =bloc.split('#')
        for keyIntro in range(0,len(sentences)):
            if (keyIntro== 0):
                dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']['intro_medical']=sentences[keyIntro]
            elif (keyIntro % 2== 0 ):

                dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']['rep_option_medical_'+str(keyIntro-2)]=sentences[keyIntro]
            else:
                dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']['option_medical_'+str(keyIntro-1)]=sentences[keyIntro]
decoupeMedical()

with open('dictionnaire/medicalcond.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_decoupe_medical, fp, indent=4)

dict_aditionnal={}
def additionalDrug():
    bloc = json_dict_maladies['ADDITIONAL DRUG INTERACTIONS']
    dict_aditionnal['ADDITIONAL DRUG INTERACTIONS']={}
    if ('$' in bloc):
        sentences= bloc.split('$')
        for keyIntro in range(0,len(sentences)):
            
            if (keyIntro== 0):
                dict_aditionnal['ADDITIONAL DRUG INTERACTIONS']['intro_additional']=sentences[keyIntro]
            elif (keyIntro ==1):
                dict_aditionnal['ADDITIONAL DRUG INTERACTIONS']['option_'+str(keyIntro)]=sentences[keyIntro]
            elif (keyIntro ==2):
                    dict_aditionnal['ADDITIONAL DRUG INTERACTIONS']['option_'+str(keyIntro)]=sentences[keyIntro]
            else:
                dict_aditionnal['ADDITIONAL DRUG INTERACTIONS']['drug_Class_'+str(keyIntro-3)]=sentences[keyIntro]
       
additionalDrug()



with open('dictionnaire/additionalDrugclass.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_aditionnal, fp, indent=4)


dict_adictionnal_decoupage={}
dict_adictionnal_decoupage_deux={}
def additionalDrugDecoupe():
    for i,keyV in enumerate(dict_aditionnal['ADDITIONAL DRUG INTERACTIONS'].values()):
       
            if ('¤' in keyV):
                sentences= keyV.split('¤')
                for keyIntro in range(0,len(sentences)):
                    if (keyIntro== 0):
                        dict_adictionnal_decoupage['drug_name_class_'+str(i-3)]=sentences[keyIntro]
                    else:
                        dict_adictionnal_decoupage['reste_a_spliter_'+str(i-3)+str(keyIntro)+str(i-3)]=sentences[keyIntro]

additionalDrugDecoupe()


with open('dictionnaire/additionnalIntermediaire.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_adictionnal_decoupage, fp, indent=4)


def additionalDrugDecoupeReste():
    for i,keyV in enumerate(dict_adictionnal_decoupage.values()):
        if ('@' in keyV):
            sentences= keyV.split('@')                            
            dict_adictionnal_decoupage_deux['sous_drug_name_class_'+str(i-1)]=sentences[0]
            dict_adictionnal_decoupage_deux['rep_sous_drug_name_class_'+str(i-1)]=sentences[1]

    

additionalDrugDecoupeReste()


with open('dictionnaire/additionnalDecoupedernier.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_adictionnal_decoupage_deux, fp, indent=4)
################################################################




with open('dictionnaire/dict_decoupage.json', 'r+',errors='ignore',encoding='utf-8') as f:
    decoupage = json.load(f)

dict_drugChartSplit_decoupage={}
def drugChartSplit():
    
    for i,keyV in enumerate(decoupage.values()):
        dict_drugChartSplit_decoupage[keyDrug[i]]={} 
        if ('$' in keyV):
                sentences= keyV.split('$')
                
                for keyIntro in range(0,len(sentences)):
                    if (keyIntro== 0):
                        pass
                    else:
                        dict_drugChartSplit_decoupage[keyDrug[i]]['reste_a_spliter_'+str(i)+str(keyIntro)]=sentences[keyIntro]

drugChartSplit()

with open('dictionnaire/dict_decoupageChart.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_drugChartSplit_decoupage, fp, indent=4)
######################

dict_drugChartSplit_decoupage_deux={}
def drugChartSplit2():
    for x, key in enumerate(dict_drugChartSplit_decoupage):
        dict_drugChartSplit_decoupage_deux[key]={}
        
        for i,keyV in enumerate(dict_drugChartSplit_decoupage[key].values()):
            #  print(keyV)
             if ('#' in keyV):      
                    sentences= keyV.split('#')
                    for keyIntro in range(0,len(sentences)):
                        #print(keyIntro)
                        if (keyIntro== 0):
                            dict_drugChartSplit_decoupage_deux[key]['grosTitre_'+str(i)+str(x)]=sentences[keyIntro]
                        elif (keyIntro % 2== 0 ):
                            dict_drugChartSplit_decoupage_deux[key]['rep_option_'+str(i)+str(keyIntro-1)+str(x)]=sentences[keyIntro]
                        else:
                            dict_drugChartSplit_decoupage_deux[key]['option_'+str(i)+str(keyIntro)+str(x)]=sentences[keyIntro]

drugChartSplit2()
with open('dictionnaire/dict_decoupageChart2.json', 'w',encoding="utf-16") as fp:
    json.dump(dict_drugChartSplit_decoupage_deux, fp, indent=4)
    
# #print(dict_drugChartSplit_decoupage_deux['ALPHA REDUCTASE INHIBITORS-5']['grosTitre_00'])



# # fileMaladie = open('ADDITIONAL DRUG INTERACTIONS.txt','r+',encoding='utf-8',errors='ignore')

# # fichier = open('fichier.txt','w+',encoding='utf-8',errors='ignore')
# # dict_drugs ={}
# # def decoupechart(): 
# #     fichier.write(fileMaladie.read())
# #     fichier.seek(0,0)
# #     s =fichier.read()
# #     for k in range(0,len(keyDrug)):
   
# #         if( k == 0):
# #             pass
# #         elif(k == 2):
# #             break
# #         else :
# #             sentences  =s.split(keyDrug[k])
# #             print(k , '                  ',keyDrug[k-1])
# #             dict_drugs[keyDrug[k-1]] =sentences[0]
# #             s =sentences[1]

# # decoupechart()
# # with open('dictionnaire/additionalDrugclass.json', 'w+',encoding="utf-16") as fp:
# #     json.dump(dict_drugs, fp, indent=4)

dict_json_final={}
def enFinal():
    dict_json_final['keys']={}
    for key in (dict_decoupe_cough['COUGH AND COLD MEDICINES']):
        if(key=='intro_caugh'):
            dict_json_final['keys']['intro_caugh']=dict_decoupe_cough['COUGH AND COLD MEDICINES'][key]
        for i in range(0, 10):
            if (i % 2 == 0):
                if(key =='option_caugh_'+str(i) ):
                    dict_json_final['keys']['option_caugh_'+str(i)]=dict_decoupe_cough['COUGH AND COLD MEDICINES'][key]
                elif(key =='rep_option_caugh_'+str(i)):
                  dict_json_final['keys']['rep_option_caugh_'+str(i)]=dict_decoupe_cough['COUGH AND COLD MEDICINES'][key]
    for key2 in dict_decoupe_pregnancy['PREGNANCY']:
        for x in range(0, 10):
            if (x % 2 == 0):
                if(key2 =='option_pregn_'+str(x) ):
                    dict_json_final['keys']['option_pregn_'+str(x)]=dict_decoupe_pregnancy['PREGNANCY'][key2]
                elif(key2 =='rep_option_pregn_'+str(x)):
                  dict_json_final['keys']['rep_option_pregn_'+str(x)]=dict_decoupe_pregnancy['PREGNANCY'][key2]

    for key3 in dict_decoupe_preg2['option_pregn_6']:
        for v in range(0, 8):
            if (v % 2 == 0):
                if(key3 =='rep_sous_op_prg_'+str(v) ):
                    dict_json_final['keys']['rep_sous_op_prg_'+str(v)]=dict_decoupe_preg2['option_pregn_6'][key3]
                elif(key3 =='sous_option_pregn_'+str(v)):
                  dict_json_final['keys']['sous_option_pregn_'+str(v)]=dict_decoupe_preg2['option_pregn_6'][key3]
    for key4 in dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']:
        for w in range(0, 503):
            if(key4=='intro_medical'):
                 dict_json_final['keys']['intro_medical']=dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS'][key4]
            if (w % 2 == 0):
                if(key4 =='option_medical_'+str(w) ):
                    dict_json_final['keys']['option_medical_'+str(w)]=dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS'][key4]
                elif(key4 =='rep_option_medical_'+str(w)):
                  dict_json_final['keys']['rep_option_medical_'+str(w)]=dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS'][key4]
    
    for key5 in dict_decoupe_glossary['GLOSSARY']:
        for w in range(0, 787):
            if(key5=='intro_glossary'):
                 dict_json_final['keys']['intro_glossary']=dict_decoupe_glossary['GLOSSARY'][key5]
            if (w % 2 == 0):
                if(key5 =='option_glossary_'+str(w) ):
                    dict_json_final['keys']['option_glossary_'+str(w)]=dict_decoupe_glossary['GLOSSARY'][key5]
                elif(key5 =='rep_option_glossary_'+str(w)):
                  dict_json_final['keys']['rep_option_glossary_'+str(w)]=dict_decoupe_glossary['GLOSSARY'][key5]   

    return dict_json_final                  
# dict_essai={}
# print(dict_json_final)
# def jsonChart():
#     for x, key in enumerate(dict_drugChartSplit_decoupage):
#         for i,keyV in enumerate(dict_drugChartSplit_decoupage[key].values()):
#             for u,keyV2 in enumerate(dict_drugChartSplit_decoupage_deux[key]):
#                 # print(keyV2)
#                 if(keyV2== 'grosTitre_'+str(i)+str(x)):
                   
                    
#                     dict_json_final['keys']['grosTitre_'+str(i)+str(x)]=dict_drugChartSplit_decoupage_deux[key][keyV2]
#                 for keyIntro in range(0,50):
#                     if(keyV2=='option_'+str(i)+str(keyIntro)+str(x)):
#                         #print(keyV2)
#                         dict_json_final['keys']['option_'+str(i)+str(keyIntro)+str(x)]=dict_drugChartSplit_decoupage_deux[key][keyV2]
#                     elif(keyV2=='rep_option_'+str(i)+str(keyIntro)+str(x)):
#                         #print(keyV2)
#                         dict_json_final['keys']['rep_option_'+str(i)+str(keyIntro)+str(x)]=dict_drugChartSplit_decoupage_deux[key][keyV2]
# jsonChart()

# with open('dictionnaire/dict_essai.json', 'w+',encoding="utf-16") as fp:
#     json.dump(dict_json_final, fp, indent=4)

def enFinalCHECK():
    for key in (json_dict_maladies.values()):
        if('CHECKLIST FOR SAFER DRUG USE'in key ):
            sentences=key.split('CHECKLIST FOR SAFER DRUG USE')
            dict_json_final['keys']['contenu_check']=sentences[1]
        
                

enFinal()
enFinalCHECK()

dict_list_drug_entier={}
def lisDrug(dict_list_drug_entier):
    
    for i in range(0,len(keyDrug)):
        dict_json_final['keys'][keyDrug[i][0]+str(i)]=keyDrug[i]
        dict_list_drug_entier[keyDrug[i][0]+str(i)]= keyDrug[i]
        
lisDrug(dict_list_drug_entier)




# #################################################################################################################
# alphabets =['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N','O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X','Z']


# def remlissageFinalGeneral():
#     dict_json_final['keys']['question']="What's the first letter of the drug that you are looking for?"
#     dict_json_final['keys']['check']="Checklist for safer drug use"
#     dict_json_final['keys']['prenancy']="Pregnancy"
#     dict_json_final['keys']['glossary']="Glossary"
#     dict_json_final['keys']['caugh']="Cough and cold medicines"
#     dict_json_final['keys']['drug chart']="Drug chart"
#     dict_json_final['keys']['info']="Information about substances of abuse"
#     dict_json_final['keys']['additional']="Additional drug interactions"
#     dict_json_final['keys']['medical']="Medical conditions and their commonly used drugs"
#     dict_json_final['keys']['Intro_premiere']="Home"
#     dict_json_final['keys']['drug_lettre']="alphabet"
    
#     for key ,alphabet in enumerate(alphabets):
#         dict_json_final['keys'][str(alphabet.upper())]= " "+alphabet.upper()


# dict_decoupagekeyParAlphab={}
# listingFourchetteNomMaladies=[]
# def remplissageFourchette(keyAlphabet):
#     s =[]
#     for key in dict_list_drug_entier:
#         if(key[0]==alphabets[keyAlphabet].upper()):
#             s.append(key)
            
#     dict_decoupagekeyParAlphab[str(s[0])+'-'+s[len(s)-1]]='Drugs that begin with the letter '+str(alphabets[keyAlphabet].upper())
#     listingFourchetteNomMaladies.append(str(s[0])+'-'+s[len(s)-1])


# def remplissageFourchetteGenral():
    
#     for keyAlphabet ,alphabet in enumerate(alphabets):
#             remplissageFourchette(keyAlphabet)

# remplissageFourchetteGenral()    
# remlissageFinalGeneral()
# tab=[]
# def ajoutFourchette():
#     for key in dict_decoupagekeyParAlphab:
#         dict_json_final['keys'][key]= dict_decoupagekeyParAlphab[key]
#         tab.append(key)

# ajoutFourchette()

# fileTs = open('questions.ts','w+',encoding='utf-8')
# fileTsBasique = open('basicTSdebut.txt','r+',encoding='utf-8')
# with open('dictionnaire/accueil.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     dictMaladieTs = json.load(f)
# with open('dictionnaire/fourchette.json', 'w',encoding="utf-16") as fp:
#       json.dump(dict_decoupagekeyParAlphab, fp, indent=4)
# def healperWrite_DictKey(dicto_copy):
#     s='{\n'
#     for key in dicto_copy:
#         if(key !='options' and key!='category' and key!='nextQuestionMap'and key!='scoreMap'):
#             s =s +'\t'+ str(key) +':'+"'"+str(dicto_copy[key])+"'"+',\n'
#         else:
#             s =s  +'\t'+ str(key) +':'+str(dicto_copy[key])+',\n'
#     fileTs.write(s[:-2]+'\n},')


# tab_decoupeFourchette=[]
# with open('basicTShealper.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     dict_basicTsHealper = json.load(f) 
# with open('dictionnaire/check_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     check = json.load(f)

# def checkf():
#     for i in dict_json_final['keys']:
#         dic1_copy =check.copy()
#         dic1_copy['text']='check'
#         if('contenu_check' in i ):
#             dic1_copy['comment']= str(i)
#             dic1_copy['nextQuestionMap']=[]
#             dic1_copy['nextQuestionMap'].append('Intro_premiere')
#             #print(dic_copy['comment'])
#             healperWrite_DictKey(dic1_copy)



# with open('dictionnaire/accueil.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     hom = json.load(f)
# def detail():
#             di_copy =hom.copy()
#             di_copy['nextQuestionMap']=[]
#             di_copy['id']=('Intro_premiere')
#             di_copy['text']=('Intro_premiere')
#             di_copy['nextQuestionMap']=['check']
#             di_copy['nextQuestionMap'].append('prenancy')
#             di_copy['nextQuestionMap'].append('glossary')
#             di_copy['nextQuestionMap'].append('caugh')
#             di_copy['nextQuestionMap'].append('question')
#             di_copy['nextQuestionMap'].append('info')
#             di_copy['nextQuestionMap'].append('additional')
#             di_copy['nextQuestionMap'].append('medical')

#             #print(i)
#             healperWrite_DictKey(di_copy)
#             di_copy={}
# with open('dictionnaire/caugh_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     caug = json.load(f)

# def RemplirCaugh(): 
#     dic_rempl_ex=caug.copy()
#     dic_rempl_ex['nextQuestionMap']=[] 
#     for x , key in enumerate(dict_decoupe_cough['COUGH AND COLD MEDICINES']):
        
#         dic_rempl_ex['comment']='intro_caugh'
        
#         if ((x) % 2==0):
#             if(x==10):
#                 pass 
#             else:
#                 dic_rempl_ex['options'].append('option_caugh_'+str(x))
#                 dic_rempl_ex['nextQuestionMap'].append('rep_option_caugh_'+str(x))
#     healperWrite_DictKey(dic_rempl_ex)



# with open('dictionnaire/accueil_drug.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     acc_drug = json.load(f)

# def accueil_drug(): 
#             dic_rempl_ex=acc_drug.copy()
#             dic_rempl_ex['nextQuestionMap']=[]
#             dic_rempl_ex['id']=('drug chart')
#             dic_rempl_ex['text']=('question')
#             dic_rempl_ex['options']=['drug_lettre']
#             dic_rempl_ex['nextQuestionMap']=['question']
#             #print(i)
#             healperWrite_DictKey(dic_rempl_ex)
#             dic_rempl_ex={}

# with open('dictionnaire/reponse_caugh.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     rep_caug = json.load(f)

# def rep_caugh():
#     dic1_copy =rep_caug.copy()
#     dic1_copy['nextQuestionMap']=[]
#     dic1_copy['nextQuestionMap'].append('Intro_premiere')
#     for x , key in enumerate(dict_decoupe_cough['COUGH AND COLD MEDICINES']):
#        # print(x)
#         if(x%2==0):
#             if(x==10):
#                 pass
#             else:
                
#                 dic1_copy['id']='rep_option_caugh_'+str(x)
#                 dic1_copy['text']='option_caugh_'+str(x)
#                 dic1_copy['comment']= 'rep_option_caugh_'+str(x)
                
                
#                 #print(dic_copy['comment'])
#                 healperWrite_DictKey(dic1_copy)
#     dic1_copy={}    
# #########
# with open('dictionnaire/glossary_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     gloss = json.load(f)

# def RemplirGlossary(): 
#     dic_rempl_ex=gloss.copy()
#     dic_rempl_ex['nextQuestionMap']=[] 
#     for x , key in enumerate(dict_decoupe_glossary['GLOSSARY']):
#         dic_rempl_ex['comment']='intro_glossary'

#         if ((x) % 2==0):
#                 dic_rempl_ex['options'].append('option_glossary_'+str(x))
#                 if(x==781):
#                     pass 
#                 else:
#                      dic_rempl_ex['nextQuestionMap'].append('rep_option_glossary_'+str(x))
#     healperWrite_DictKey(dic_rempl_ex)


# with open('dictionnaire/reponse_glossary.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     rep_gloss = json.load(f)

# def rep_glossary():
#     dic1_copy =rep_gloss.copy()
#     dic1_copy['nextQuestionMap']=[]
#     dic1_copy['nextQuestionMap'].append('Intro_premiere')
#     for x , key in enumerate(dict_decoupe_glossary['GLOSSARY']):
#         #print(x)
#         if(x%2==0):
#             if(x==782):
#                 pass
#             else:
                
#                 dic1_copy['id']='rep_option_glossary_'+str(x)
#                 dic1_copy['text']='option_glossary_'+str(x)
#                 dic1_copy['comment']= 'rep_option_glossary_'+str(x)
                
                
#                 healperWrite_DictKey(dic1_copy)
#     dic1_copy={}  
# ###########
# with open('dictionnaire/medical_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     med = json.load(f)

# def RemplirMedicalCond(): 
#     dic_rempl_ex=med.copy()
#     dic_rempl_ex['nextQuestionMap']=[] 
#     dic_rempl_ex['id']="medical"
#     dic_rempl_ex['text']="medical"

#     for x , key in enumerate(dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']):
#         dic_rempl_ex['comment']='intro_medical'

#         if ((x) % 2==0):
#                 dic_rempl_ex['options'].append('option_medical_'+str(x))                  
#                 dic_rempl_ex['nextQuestionMap'].append('rep_option_medical_'+str(x))
#     healperWrite_DictKey(dic_rempl_ex)


# with open('dictionnaire/reponse_medical.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     rep_med = json.load(f)

# def rep_medicalCond():
#     dic1_copy =rep_med.copy()
#     dic1_copy['nextQuestionMap']=[]
#     dic1_copy['nextQuestionMap'].append('Intro_premiere')
#     for x , key in enumerate(dict_decoupe_medical['MEDICAL CONDITIONS AND THEIR COMMONLY USED DRUGS']):
#        # print(x)
#         if(x%2==0):
#             if(x==467):
#                 pass
#             else:                
#                 dic1_copy['id']='rep_option_medical_'+str(x)
#                 dic1_copy['text']='option_medical_'+str(x)
#                 dic1_copy['comment']= 'rep_option_medical_'+str(x)
                
                
#                 healperWrite_DictKey(dic1_copy)
#     dic1_copy={}  
# ##########
# with open('dictionnaire/preg_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     preg = json.load(f)  
# def RemplirPregnancy(): 
#     dic_rempl_ex=preg.copy()
#     dic_rempl_ex['nextQuestionMap']=[] 
#     for x , key in enumerate(dict_decoupe_pregnancy['PREGNANCY']):
#         if ((x) % 2==0):
#                 dic_rempl_ex['options'].append('option_pregn_'+str(x))
#                 if(x==6):
#                     dic_rempl_ex['nextQuestionMap'].append('option_pregn_6')
#                 else:
#                      dic_rempl_ex['nextQuestionMap'].append('rep_option_pregn_'+str(x))
#     healperWrite_DictKey(dic_rempl_ex)

# with open('dictionnaire/reponse_pref_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     rep_pre = json.load(f) 
# def rep_preg():
#     dic1_copy =rep_pre.copy()
#     dic1_copy['nextQuestionMap']=[]
#     dic1_copy['nextQuestionMap'].append('Intro_premiere')
#     for x , key in enumerate(dict_decoupe_pregnancy['PREGNANCY']):
#         if(key== 'option_pregn_6'):
#             pass
#         elif(x%2==0):
#                 if(x==6):
#                     pass
#                 else:
#                     dic1_copy['id']='rep_option_pregn_'+str(x)
#                     dic1_copy['text']='option_pregn_'+str(x)
#                     dic1_copy['comment']= 'rep_option_pregn_'+str(x)
                
                
#                 healperWrite_DictKey(dic1_copy)
#     dic1_copy={}


# with open('dictionnaire/sous_parti_preg_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     opt6 = json.load(f) 
# def option6():
#     dic1_copy =opt6.copy()
#     dic1_copy['id']='option_pregn_6'
#     dic1_copy['text']='option_pregn_6'
#     for x , key in enumerate(dict_decoupe_preg2['option_pregn_6']):
#         if(x%2==0):
#                     dic1_copy['options'].append('sous_option_pregn_'+str(x))
#                     dic1_copy['nextQuestionMap'].append('rep_sous_op_prg_'+str(x))
#     healperWrite_DictKey(dic1_copy)
#     dic1_copy={}

# with open('dictionnaire/sous_rep_prg_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     rep_spre = json.load(f) 
# def rep_sous_preg():
#     dic1_copy =rep_spre.copy()

#     dic1_copy['nextQuestionMap']=[]
#     dic1_copy['nextQuestionMap'].append('Intro_premiere')
#     dic1_copy['options'].append('Intro_premiere')
#     for x , key in enumerate(dict_decoupe_preg2['option_pregn_6']):
#         if(x%2==0):
#                     dic1_copy['id']='rep_sous_op_prg_'+str(x)
#                     dic1_copy['text']='sous_option_pregn_'+str(x)
#                     dic1_copy['comment']= 'rep_sous_op_prg_'+str(x)
                
                
#                     healperWrite_DictKey(dic1_copy)
#     dic1_copy={}

# ##################
# def vidage_dict_basicTsHealper():
#     for key in dict_basicTsHealper:
#         if(key !="options" and key !="nextQuestionMap" and key !="scoreMap"):
#             dict_basicTsHealper[key]=''
#         else:
#             dict_basicTsHealper[key]=[]
  
# ###############
# dict_OptionOupas ={}

# def optionExisteOuPas(k,v):
#     tabOptionOuQuestion =[]
#     for key in dict_drugChartSplit_decoupage_deux[v]:
#         if('grosTitre_' in key ):
#             tabOptionOuQuestion.append(key)
#     if(tabOptionOuQuestion ==[]):
#         tabOptionOuQuestion.append('option_'+str(k+1)+'_0')
#     dict_OptionOupas[v[0]+str(k+1)]=tabOptionOuQuestion


# def generalOptionExisteOuPas():
#     for k,v in enumerate(dict_drugChartSplit_decoupage_deux):
#         optionExisteOuPas(k,v)

# generalOptionExisteOuPas()
# ######
# tabNomMaladieKey =[]

# def ajoutKeysMaladies():
#     for k,v in enumerate(dict_drugChartSplit_decoupage_deux):
#         dict_json_final['keys'][v[0]+str(k+1)]=v
#         tabNomMaladieKey.append(v[0]+str(k+1))
#         dict_json_final['keys']['grosTitre_']='gros titre'
#         dict_json_final['keys']['option_']='option'
#         dict_json_final['keys']['rep_option_']='reponse option'


    
# ajoutKeysMaladies()
# #############
# with open('dictionnaire/drugChart_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     drugChart = json.load(f)

# tabOptions =[]

# def RemplirDrugChart(i,nom):
#     vidage_dict_basicTsHealper()
#     dic_rempl_ex=drugChart.copy()
#     fileTs.write('\n//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE '+str(nom)+'\n')
#     dic_rempl_ex['id']=nom
#     dic_rempl_ex['text']=nom
#     dic_rempl_ex['options']=[]
#     dic_rempl_ex['options'].append('gros titre')
#     test =False
#     for id , value in enumerate(dict_drugChartSplit_decoupage_deux):
#         if(id == i):
#             for ex in dict_drugChartSplit_decoupage_deux[value]:
#                 if('option_' in ex):
#                     dic_rempl_ex['options'].append('questionnaire')
#                     test=True
#                     break
#     if(test == True):
#         dic_rempl_ex['nextQuestionMap'].append(dict_OptionOupas[nom][0]+'-'+dict_OptionOupas[nom][len(dict_OptionOupas[nom])-1])
#         tabOptions.append(dict_OptionOupas[nom][0]+'-'+dict_OptionOupas[nom][len(dict_OptionOupas[nom])-1])

#     for j in range(0,len( dic_rempl_ex['options'])):
#         dic_rempl_ex['scoreMap'].append(0) 
#     dic_rempl_ex['inputType']= 'radio'
#     healperWrite_DictKey(dic_rempl_ex)


# def generalDecoupeMaladieIntroYourDoctor():
#     for i in range(0,len(tabNomMaladieKey)):
#         RemplirDrugChart(i,tabNomMaladieKey[i])

# generalDecoupeMaladieIntroYourDoctor()
# # 
# # with open('dictionnaire/reponse_drugChart.json', 'r+',errors='ignore',encoding='utf-8') as f:
# #     rep_drugchart = json.load(f)

# # def rep_drugChart():
# #     dic1_copy =rep_drugchart.copy()
# #     dic1_copy['nextQuestionMap']=[]
# #     dic1_copy['nextQuestionMap'].append('Intro_premiere')
# #     for x , key in enumerate(dict_drugChartSplit_decoupage_deux):
# #        # print(x)
# #         if(x%2==0):

# #             dic1_copy['id']='rep_option_'+str(x)
# #             dic1_copy['text']='option_'+str(x)
# #             dic1_copy['comment']= 'rep_option_'+str(x)
                
                
# #     healperWrite_DictKey(dic1_copy)
# #     dic1_copy={}

# ##################
# with open('dictionnaire/additional_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     add = json.load(f)  
# def RemplirAdditional(): 
#     dic_rempl_ex=preg.copy()
#     dic_rempl_ex['nextQuestionMap']=[] 
#     for x , key in enumerate(dict_aditionnal['ADDITIONAL DRUG INTERACTIONS']):
#         if ((x) % 2==0):
#                 if(x==57257):
#                     pass
#                 else:
#                     dic_rempl_ex['options'].append('drug_name_class_'+str(x))
#                     dic_rempl_ex['nextQuestionMap'].append('reste_a_spliter_'+str(x))
#     healperWrite_DictKey(dic_rempl_ex)

# with open('dictionnaire/reponse_additional_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
#     rep_add = json.load(f) 

# def rep_additional():
#     dic1_copy =rep_add.copy()
#     dic1_copy['nextQuestionMap']=[]
#     dic1_copy['nextQuestionMap'].append('Intro_premiere')
#     for x , key in enumerate(dict_aditionnal['ADDITIONAL DRUG INTERACTIONS']):
#        # print(x)
#         if(x%2==0):
#             if(x==57257):
#                 pass
#             else:
                
#                 dic1_copy['id']='reste_a_spliter_'+str(x)
#                 dic1_copy['text']='drug_name_class_'+str(x)
#                 dic1_copy['comment']= 'reste_a_spliter_'+str(x)
                
                
#                 healperWrite_DictKey(dic1_copy)
#     dic1_copy={}    
# ##################

# def debutQuestion_Ts():
#     fileTs.truncate()
#     fileTs.seek(0,0)
#     fileTs.write(fileTsBasique.read())
#     fileTs.write('\n//*******************PARTIE QUESTION :PREMIERE LETTRE DE LA MALADIE************\n')
#     detail()
#     checkf()
#     RemplirPregnancy()
#     rep_preg()
#     option6()
#     rep_sous_preg()
#     RemplirCaugh()
#     rep_caugh()
#     # accueil_drug()
#     #
#     RemplirGlossary()
#     rep_glossary()
#     #
#     RemplirMedicalCond()
#     rep_medicalCond()
#     #
#     # RemplirAdditional()
#     # rep_additional()
#     # rep_drugChart()
#     dict_basicTsHealper['id']='question'
#     dict_basicTsHealper['category']='CATEGORIES.PERSONAL'
#     dict_basicTsHealper['text']= dict_basicTsHealper['id']
#     dict_basicTsHealper['options']=alphabets
#     for key in dict_decoupagekeyParAlphab:
#         dict_basicTsHealper['nextQuestionMap'].append(key)
#         tab_decoupeFourchette.append(key)
#     for i in range(0,len(dict_basicTsHealper['options'])):
#         dict_basicTsHealper['scoreMap'].append(0)
#     healperWrite_DictKey(dict_basicTsHealper) 

# # def vidage_dict_basicTsHealper():
# #     for key in dict_basicTsHealper:
# #         if(key !="options" and key !="nextQuestionMap" and key !="scoreMap"):
# #             dict_basicTsHealper[key]=''
# #         else:
# #             dict_basicTsHealper[key]=[]
           

# tab_question1Formu=[]
# tab_question1Formu_copieAll=[]
# def recupToutePremiereKey(maladie):
#     tab_question1Formu_copieAll=[]
#     tab_question1Formu_copieAll=[next(iter(dict_list_drug_entier.keys()))]
#     return tab_question1Formu_copieAll

# def recupPremiereKeyQuestion(maladie):
#     tab_question1Formu=[]
#     tab_question1Formu_copieAll =recupToutePremiereKey(maladie)
#     for key in tab_question1Formu_copieAll:
#         if(key[0]==maladie[0]):
#             tab_question1Formu.append(key)
#         else:
#             pass
#     return tab_question1Formu
    
    
# def ajoutKeyDetailFourchette(id,nb1,nb2):
#     vidage_dict_basicTsHealper()
#     fileTs.write('\n//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE '+str(id)+'\n')
#     dict_basicTsHealper['id']=id
#     dict_basicTsHealper['category']='CATEGORIES.PERSONAL'
#     dict_basicTsHealper['text']= dict_basicTsHealper['id']
#     for i in range(nb1,nb2+1):
#         dict_basicTsHealper['options'].append(id[0]+str(i))
#         dict_basicTsHealper['nextQuestionMap'].append(id[0]+str(i))

#     for j in range(0,len(dict_basicTsHealper['options'])):#------------------------------aaaaaaaaaaaaa
#         dict_basicTsHealper['scoreMap'].append(0) 
#     dict_basicTsHealper['inputType']= 'radio'
#     healperWrite_DictKey(dict_basicTsHealper)

# def fourchetteEnumeration(id):
#     sentences=id.split('-')
#     s1=sentences[0].split(id[0])
#     s2=sentences[1].split(id[0])
#     nb1 =s1[1]
#     nb2=s2[1]
#     tab_question1Formu=[]
#     ajoutKeyDetailFourchette(id,int(nb1),int(nb2))


# def generalFoucrchetteEnumeration():
#     fileTs.write('\n//**********AJOUT DELIMITATION MALADIE ALPHABETS DE A.. a A..****************\n')
#     for id in tab_decoupeFourchette:
#         fourchetteEnumeration(id)





# tab_sauvegardeQuestComm=[]
# ##################
# # with open('dictionnaire/drugChart_fichier.json', 'r+',errors='ignore',encoding='utf-8') as f:
# #     drugChart = json.load(f)

# # def RemplirDrugChart(): 
# #     dic_rempl_ex=drugChart.copy()
# #     dic_rempl_ex['nextQuestionMap']=[] 
# #     dic_rempl_ex['id']="drug chart"
# #     dic_rempl_ex['text']="drug chart"

# #     for x , key in enumerate(dict_decoupe_medical['DRUG CHARTS']):
# #         if ((x)==0):
# #             dic_rempl_ex['options'].append('grosTitre_'+str(x))

# #         if ((x) % 2==0):
# #                 dic_rempl_ex['options'].append('option_'+str(x))                  
# #                 dic_rempl_ex['nextQuestionMap'].append('rep_option_'+str(x))
# #     healperWrite_DictKey(dic_rempl_ex)


# # with open('dictionnaire/reponse_drugChart.json', 'r+',errors='ignore',encoding='utf-8') as f:
# #     rep_drugchart = json.load(f)

# # def rep_drugChart():
# #     dic1_copy =rep_drugchart.copy()
# #     dic1_copy['nextQuestionMap']=[]
# #     dic1_copy['nextQuestionMap'].append('Intro_premiere')
# #     for x , key in enumerate(dict_decoupe_medical['DRUG CHARTS']):
# #        # print(x)
# #         if(x%2==0):

# #             dic1_copy['id']='rep_option_'+str(x)
# #             dic1_copy['text']='option_'+str(x)
# #             dic1_copy['comment']= 'rep_option_'+str(x)
                
                
# #     healperWrite_DictKey(dic1_copy)
# #     dic1_copy={}

# # tabNomMaladieKey =[]

# # def ajoutKeysMaladies():
# #     for k,v in enumerate(dict_maladies_bis):
# #         dict_basicSansQstCom['keys'][v[0]+str(k+1)]=v
# #         tabNomMaladieKey.append(v[0]+str(k+1))
# #         dict_basicSansQstCom['keys']['introduction']='introduction'
# #         dict_basicSansQstCom['keys']['your doctor visit']='your doctor visit'
# #         dict_basicSansQstCom['keys']['questionnaire']='questionnaire'
    
# # ajoutKeysMaladies()
# # tabOptions =[]

# # def decoupeMaladieIntroYourDoctor(i,nom):
# #     vidage_dict_basicTsHealper()
# #     dict_bis =dict_basicTsHealper.copy()
# #     fileTs.write('\n//PARTIE MALADIE INTRO DOCTOR VISIT QUESTIONNAIRE '+str(nom)+'\n')
# #     dict_bis['id']=nom
# #     dict_bis['category']='CATEGORIES.PERSONAL'
# #     dict_bis['text']=nom
# #     dict_bis['options']=[]
# #     # dict_bis['options'].append('introduction')
# #     # dict_bis['options'].append('your doctor visit')
# #     test =False
# #     for id,value in enumerate(dict_drugChartSplit_decoupage_deux):
# #         if(id == i):
# #             for ex in dict_drugChartSplit_decoupage_deux[value]:
# #                 if('question_'in ex):
# #                     dict_bis['options'].append('questionnaire')
# #                     test=True
# #                     break
# #     dict_bis['nextQuestionMap'].append('grosTitre_'+str(i+1))
# #     print(id)
# #     # if(test == True):
# #     #     if(len(dict_OptionOupas[nom])<2 ):
# #     #         dict_bis['nextQuestionMap'].append(dict_OptionOupas[nom][0])
# #     #     else:   
# #     #         dict_bis['nextQuestionMap'].append(dict_OptionOupas[nom][0]+'-'+dict_OptionOupas[nom][len(dict_OptionOupas[nom])-1])
# #     #         tabOptions.append(dict_OptionOupas[nom][0]+'-'+dict_OptionOupas[nom][len(dict_OptionOupas[nom])-1])
# #     # for j in range(0,len( dict_bis['options'])):
# #     #     dict_bis['scoreMap'].append(0) 
# #     # dict_bis['inputType']= 'radio'
# #     healperWrite_DictKey(dict_bis)
    

# # def generalDecoupeMaladieIntroYourDoctor():
# #     for i in range(0,len(tabNomMaladieKey)):
# #         decoupeMaladieIntroYourDoctor(i,tabNomMaladieKey[i])

# # generalDecoupeMaladieIntroYourDoctor()
# ##################
# def maladieQuestionSuiv():
#     for x,i in enumerate(dict_list_drug_entier.keys()):
#         dict_copy =dictMaladieTs.copy()
#         dict_copy['id']=i
#         dict_copy['text']= i
#         #print(i)
#         #healperWrite_DictKey(dict_copy)
#     #for x in range (len(dict_id_maladie.keys())):
#         dict_copy['nextQuestionMap']=[]
#         dict_copy['nextQuestionMap']=[('Intro_premiere_'+ str(x))]
#         dict_copy['nextQuestionMap'].append('quest_poser_'+str(x))
#         dict_copy['nextQuestionMap'].append('exam_'+str(x))

#         dict_copy['nextQuestionMap'].append('Question_'+str(x)+'1')
#         #print(x)
#         healperWrite_DictKey(dict_copy)
#     dict_copy={}

# print('questions.ts pret !')
# if __name__ == '__main__':
#     debutQuestion_Ts()
#     generalFoucrchetteEnumeration()
#     fileTs.write('];')




with open('basicTShealper.json', 'r+',errors='ignore',encoding='utf-8') as f:
    dict_basicTsHealper = json.load(f) 

with open('dictionnaire/dict_enFinal.json', 'w+',encoding="utf-16") as fp:
    json.dump(dict_json_final, fp, indent=4)

with open('dictionnaire/liste_drug.json', 'w+',encoding="utf-16") as fp:
    json.dump(dict_list_drug_entier, fp, indent=4)