CHECKLIST FOR SAFER DRUG USE
• Tell your doctor about any drug you take (even aspirin, allergy pills,
  cough and cold preparations, antacids, laxatives, herbal
  preparations, vitamins, etc.) before you take any new drug.
• Learn all you can about drugs you may take before you take them.
  Information sources are your doctor, your nurse, your pharmacist,
  this book, other books in your public library and the Internet.
• Keep an up-to-date list of all the medicines you take in a wallet or
  purse. Include name, dose and frequency.
• Don’t take drugs prescribed for someone else—even if your
  symptoms are the same.
• Keep your prescription drugs to yourself. Your drugs may be
  harmful to someone else.
• Tell your doctor about any symptoms you believe are caused by a
  drug—prescription or nonprescription—that you take.
• Take only medicines that are necessary. Avoid taking
  nonprescription drugs while taking prescription drugs for a medical
  problem.
• Before your doctor prescribes for you, tell him about your previous
  experiences with any drug—beneficial results, side effects, adverse
  reactions or allergies.
• Take medicine in good light after you have identified it. If you wear
  glasses to read, put them on to check drug labels. It is easy to take
  the wrong drug or take a drug at the wrong time.
• Don’t keep any drugs that change mood, alertness or judgment—
  such as sedatives, narcotics or tranquilizers—by your bedside.
  These can cause accidental deaths. You may unknowingly repeat a
  dose when you are half asleep or confused.
• Know the names of your medicines. These include the generic
  name, the brand name and the generic names of all ingredients in a
  combination drug. Your doctor, nurse or pharmacist can give you
  this information.
• Study the labels on all nonprescription drugs. If the information is
  incomplete or if you have questions, ask the pharmacist for more
  details.
• If you must deviate from your prescribed dose schedule, tell your
  doctor.
• Shake liquid medicines before taking (if directed).
• Store all medicines away from moisture and heat. Bathroom
  medicine cabinets are usually unsuitable.
• If a drug needs refrigeration, don’t freeze.
• Obtain a standard measuring spoon from your pharmacy for liquid
  medicines. Kitchen teaspoons and tablespoons are not accurate
  enough.
• Follow diet instructions when you take medicines. Some work
  better on a full stomach, others on an empty stomach. Some drugs
  are more useful with special diets. For example, medicine for high
  blood pressure may be more effective if accompanied by a sodium-
  restricted diet.
• Tell your doctor about any allergies you have to any substance (e.g.,
  food) or adverse reactions to medicines you’ve had in the past. A
  previous allergy to a drug may make it dangerous to prescribe
  again. People with other allergies, such as eczema, hay fever,
  asthma, bronchitis and food allergies, are more likely to be allergic
  to drugs.
• Prior to surgery, tell your doctor, anesthesiologist or dentist about
  any drug you have taken in the past few weeks. Advise them of any
  cortisone drugs you have taken within two years.
• If you become pregnant while taking any medicine, including birth
  control pills, tell your doctor immediately.
• Avoid all drugs while you are pregnant, if possible. If you must take
  drugs during pregnancy, record names, amounts, dates and reasons.
• If you see more than one doctor, tell each one about drugs others
  have prescribed.
• When you use nonprescription drugs, report it so the information is
  on your medical record.
• Store all drugs away from the reach of children.
• Note the expiration date on each drug label. Discard outdated ones
  safely. If no expiration date appears and it has been at least one year
  since taking the medication, it may be best to discard it. Follow any
  specific disposal instructions on the drug label or patient
  information that came with the drug. 
• Pay attention to the information in the drug charts about safety
  while driving, piloting or working in dangerous places.
• Alcohol, cocaine, marijuana or other mood-altering drugs, as well
  as tobacco—mixed with some drugs—can cause a life-threatening
  interaction, prevent your medicine from being effective or delay
  your return to health. Be sure that you avoid them during illness.
• Some medications are subject to theft. For example, a repair person
  in your home who is abusing drugs may ask to use your bathroom,
  and while there “checkout” your medicine cabinet. Sedatives,
  stimulants and analgesics are especially likely to be stolen, but
  almost any medication is subject to theft.
• If possible, use the same pharmacy for all your medications. Every
  pharmacy keeps a “drug profile,” and if it is complete, the
  pharmacist may stop medications that are likely to cause serious
  interactions. Also, having a record of all your medications in one
  place helps your doctor or an emergency room doctor get a
  complete picture in case of an emergency.
• If you have a complicated medical history or a condition that might
  render you unable to communicate (e.g., diabetes or epilepsy), wear
  some type of medical identification (small tag, bracelet, neck chain
  or other) to identify your condition. Some people carry a wallet
  card with the information. A newer type of medical alert ID is the
  USB tag. It is basically a USB flash drive that contains a person’s
  medical emergency information and can store much more
  information if desired. Emergency medical people can access the
  information using any available computer.
• If you are giving medicine to children, read all instructions
  carefully. Use the specific dosing device (dropper, cup, etc.) that
  comes with the product. Don’t exceed recommended dose. It does
  not help and can cause health risks.
• Ask your pharmacist if they will provide an extra, prescription-
  labeled small bottle or container to use for one or two doses of your
  medication. It can be used when going out for a meal, during short
  trips or for a student in school. This avoids carrying all your
  medications with you.





