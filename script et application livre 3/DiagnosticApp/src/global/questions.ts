import {
  Guard

} from './guard';


export type Question = {
  id: string;
  category: string;
  comment?: string;
  text: string;
  inputType: 'radio' | 'date' | 'checkbox' | 'postal';
  options?: string[] | CheckboxOption[];
  nextQuestionMap?: string | string[];
  scoreMap?: number[];
  guard?: Guard;
};

export type CheckboxOption = {
  label: string;
  id: string;
};

export const CATEGORIES = {
  PERSONAL: 'personalInfo',
  SYMPTOMS: 'symptoms',

};
export const NO_XML = 'X';
export const QUESTION = {
  POSTAL_CODE: 'V1',
  AGE: 'P0',
  ABOVE_65: 'P1',
  LIVING_SITUATION: 'P2',
  CARING: 'P3',
  WORKSPACE: 'P4',
  CONTACT_DATE: 'CZ',
  OUT_OF_BREATH: 'SB',
  SYMPTOM_DATE: 'SZ',
  DATA_DONATION: `${NO_XML}1`,
};

export const XML_ORDER = ['V', 'P', 'C', 'S', 'D', 'M'];
export const QUESTIONS: Question[] = [
//*******************PARTIE QUESTOIN :CHOIX PREMIERE LETTERE DE LA MALADIE************
  {
    id:'question',
    category:CATEGORIES.PERSONAL,
    text:'question',
    inputType:'radio',
    comment:'',
    options:['A', 'B', 'C', 'D', 'E', 'F', 'H', 'I', 'M', 'N', 'P', 'S', 'U', 'V'],
    nextQuestionMap:['A1-A2', 'B3-B5', 'C6-C9', 'D10-D11', 'E12-E12', 'F13-F16', 'H17-H18', 'I19-I19', 'M20-M21', 'N22-N22', 'P23-P27', 'S28-S29', 'U30-U30', 'V31-V33'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//**********AJOUT DELIMITATION MALADIE ALPHABETS DE A.. a A..****************

//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE A1-A2
  {
    id:'A1-A2',
    category:CATEGORIES.PERSONAL,
    text:'A1-A2',
    inputType:'radio',
    comment:'',
    options:['A1', 'A2'],
    nextQuestionMap:['A1', 'A2'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE B3-B5
  {
    id:'B3-B5',
    category:CATEGORIES.PERSONAL,
    text:'B3-B5',
    inputType:'radio',
    comment:'',
    options:['B3', 'B4', 'B5'],
    nextQuestionMap:['B3', 'B4', 'B5'],
    scoreMap:[0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE C6-C9
  {
    id:'C6-C9',
    category:CATEGORIES.PERSONAL,
    text:'C6-C9',
    inputType:'radio',
    comment:'',
    options:['C6', 'C7', 'C8', 'C9'],
    nextQuestionMap:['C6', 'C7', 'C8', 'C9'],
    scoreMap:[0, 0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE D10-D11
  {
    id:'D10-D11',
    category:CATEGORIES.PERSONAL,
    text:'D10-D11',
    inputType:'radio',
    comment:'',
    options:['D10', 'D11'],
    nextQuestionMap:['D10', 'D11'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE E12-E12
  {
    id:'E12-E12',
    category:CATEGORIES.PERSONAL,
    text:'E12-E12',
    inputType:'radio',
    comment:'',
    options:['E12'],
    nextQuestionMap:['E12'],
    scoreMap:[0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE F13-F16
  {
    id:'F13-F16',
    category:CATEGORIES.PERSONAL,
    text:'F13-F16',
    inputType:'radio',
    comment:'',
    options:['F13', 'F14', 'F15', 'F16'],
    nextQuestionMap:['F13', 'F14', 'F15', 'F16'],
    scoreMap:[0, 0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE H17-H18
  {
    id:'H17-H18',
    category:CATEGORIES.PERSONAL,
    text:'H17-H18',
    inputType:'radio',
    comment:'',
    options:['H17', 'H18'],
    nextQuestionMap:['H17', 'H18'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE I19-I19
  {
    id:'I19-I19',
    category:CATEGORIES.PERSONAL,
    text:'I19-I19',
    inputType:'radio',
    comment:'',
    options:['I19'],
    nextQuestionMap:['I19'],
    scoreMap:[0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE M20-M21
  {
    id:'M20-M21',
    category:CATEGORIES.PERSONAL,
    text:'M20-M21',
    inputType:'radio',
    comment:'',
    options:['M20', 'M21'],
    nextQuestionMap:['M20', 'M21'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE N22-N22
  {
    id:'N22-N22',
    category:CATEGORIES.PERSONAL,
    text:'N22-N22',
    inputType:'radio',
    comment:'',
    options:['N22'],
    nextQuestionMap:['N22'],
    scoreMap:[0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE P23-P27
  {
    id:'P23-P27',
    category:CATEGORIES.PERSONAL,
    text:'P23-P27',
    inputType:'radio',
    comment:'',
    options:['P23', 'P24', 'P25', 'P26', 'P27'],
    nextQuestionMap:['P23', 'P24', 'P25', 'P26', 'P27'],
    scoreMap:[0, 0, 0, 0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE S28-S29
  {
    id:'S28-S29',
    category:CATEGORIES.PERSONAL,
    text:'S28-S29',
    inputType:'radio',
    comment:'',
    options:['S28', 'S29'],
    nextQuestionMap:['S28', 'S29'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE U30-U30
  {
    id:'U30-U30',
    category:CATEGORIES.PERSONAL,
    text:'U30-U30',
    inputType:'radio',
    comment:'',
    options:['U30'],
    nextQuestionMap:['U30'],
    scoreMap:[0]
  },
//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE V31-V33
  {
    id:'V31-V33',
    category:CATEGORIES.PERSONAL,
    text:'V31-V33',
    inputType:'radio',
    comment:'',
    options:['V31', 'V32', 'V33'],
    nextQuestionMap:['V31', 'V32', 'V33'],
    scoreMap:[0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... A1
  {
    id:'A1',
    category:CATEGORIES.PERSONAL,
    text:'A1',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_1', 'opt_nature_of_patient__1', 'opt_associated_symptoms__1', 'opt_precipitating_and_aggravating_factors__1', 'opt_ameliorating_factors__1', 'opt_physical_findings__1', 'opt_diagnostic_studies__1', 'opt_less_common_diagnostic_considerations__1', 'question_1'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... A2
  {
    id:'A2',
    category:CATEGORIES.PERSONAL,
    text:'A2',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_2', 'opt_nature_of_patient__2', 'opt_associated_symptoms__2', 'opt_precipitating_and_aggravating_factors__2', 'opt_ameliorating_factors__2', 'opt_physical_findings__2', 'opt_diagnostic_studies__2', 'opt_less_common_diagnostic_considerations__2', 'question_2'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... B3
  {
    id:'B3',
    category:CATEGORIES.PERSONAL,
    text:'B3',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_3', 'opt_nature_of_patient__3', 'opt_nature_of_symptoms_and_location_of_pain__3', 'opt_associated_symptoms__3', 'opt_precipitating_and_aggravating_factors__3', 'opt_ameliorating_factors__3', 'opt_physical_findings__3', 'opt_diagnostic_studies__3', 'opt_less_common_diagnostic_considerations__3', 'question_3'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... B4
  {
    id:'B4',
    category:CATEGORIES.PERSONAL,
    text:'B4',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_4', 'opt_nature_of_patient__4', 'opt_associated_symptoms__4', 'opt_precipitating_and_aggravating_factors__4', 'opt_ameliorating_factors__4', 'opt_physical_findings__4', 'opt_diagnostic_studies___4', 'opt_less_common_diagnostic_considerations__4', 'question_4'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... B5
  {
    id:'B5',
    category:CATEGORIES.PERSONAL,
    text:'B5',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_5', 'opt_nature_of_patient__5', 'opt_nature_of_symptoms__5', 'opt_associated_symptoms__5', 'opt_precipitating_and_aggravating_factors__5', 'opt_ameliorating_factors__5', 'opt_physical_findings__5', 'opt_diagnostic_studies__5', 'opt_less_common_diagnostic_considerations__5', 'question_5'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... C6
  {
    id:'C6',
    category:CATEGORIES.PERSONAL,
    text:'C6',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_6', 'opt_nature_of_patient__6', 'opt_associated_symptoms__6', 'opt_precipitating_and_aggravating_factors__6', 'opt_ameliorating_factors__6', 'opt_physical_findings__6', 'opt_diagnostic_studies__6', 'opt_less_common_diagnostic_considerations__6', 'question_6'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... C7
  {
    id:'C7',
    category:CATEGORIES.PERSONAL,
    text:'C7',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_7', 'opt_nature_of_patient__7', 'opt_nature_of_symptoms_and_associated_symptoms__7', 'opt_precipitating_and_aggravating_factors__7', 'opt_ameliorating_factors__7', 'opt_physical_findings__7', 'opt_diagnostic_studies__7', 'opt_less_common_diagnostic_considerations__7', 'question_7'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... C8
  {
    id:'C8',
    category:CATEGORIES.PERSONAL,
    text:'C8',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_8', 'opt_nature_of_patient__8', 'opt_nature_of_symptoms__8', 'opt_associated_symptoms__8', 'opt_precipitating_and_aggravating_factors__8', 'opt_ameliorating_factors__8', 'opt_physical_findings__8', 'opt_diagnostic_studies__8', 'opt_less_common_diagnostic_considerations__8', 'question_8'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... C9
  {
    id:'C9',
    category:CATEGORIES.PERSONAL,
    text:'C9',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_9', 'opt_nature_of_patient__9', 'opt_nature_of_symptoms__9', 'opt_associated_symptoms__9', 'opt_precipitating_and_aggravating_factors__9', 'opt_ameliorating_factors__9', 'opt_physical_findings__9', 'opt_diagnostic_studies__9', 'opt_less_common_diagnostic_considerations__9', 'question_9'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... D10
  {
    id:'D10',
    category:CATEGORIES.PERSONAL,
    text:'D10',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_10', 'opt_nature_of_patient__10', 'opt_nature_of_symptoms__10', 'opt_associated_symptoms__10', 'opt_precipitating_and_aggravating_factors__10', 'opt_ameliorating_factors__10', 'opt_physical_findings__10', 'opt_diagnostic_studies__10', 'opt_less_common_diagnostic_considerations__10', 'question_10'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... D11
  {
    id:'D11',
    category:CATEGORIES.PERSONAL,
    text:'D11',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_11', 'opt_nature_of_patient__11', 'opt_nature_of_symptoms__11', 'opt_associated_symptoms__11', 'opt_precipitating_and_aggravating_factors__11', 'opt_ameliorating_factors__11', 'opt_physical_findings__11', 'opt_diagnostic_studies__11', 'opt_less_common_diagnostic_considerations__11', 'question_11'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... E12
  {
    id:'E12',
    category:CATEGORIES.PERSONAL,
    text:'E12',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_12', 'opt_nature_of_patient__12', 'opt_nature_of_symptoms__12', 'opt_associated_symptoms__12', 'opt_precipitating_and_aggravating_factors__12', 'opt_ameliorating_factors__12', 'opt_physical_findings_and_diagnostic_studies__12', 'opt_less_common_diagnostic_considerations__12', 'question_12'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... F13
  {
    id:'F13',
    category:CATEGORIES.PERSONAL,
    text:'F13',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_13', 'opt_nature_of_patient__13', 'opt_nature_of_symptoms__13', 'opt_associated_symptoms__13', 'opt_precipitating_and_aggravating_factors__13', 'opt_ameliorating_factors__13', 'opt_physical_findings__13', 'opt_diagnostic_studies__13', 'opt_less_common_diagnostic_considerations__13', 'question_13'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... F14
  {
    id:'F14',
    category:CATEGORIES.PERSONAL,
    text:'F14',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_14', 'opt_nature_of_patient__14', 'opt_nature_of_symptoms__14', 'opt_associated_symptoms__14', 'opt_ameliorating_factors__14', 'opt_physical_findings__14', 'opt_diagnostic_studies__14', 'opt_less_common_diagnostic_considerations__14', 'question_14'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... F15
  {
    id:'F15',
    category:CATEGORIES.PERSONAL,
    text:'F15',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_15', 'opt_nature_of_patient__15', 'opt_nature_of_symptoms__15', 'opt_associated_symptoms__15', 'opt_precipitating_and_aggravating_factors__15', 'opt_physical_findings__15', 'opt_diagnostic_studies__15', 'opt_less_common_diagnostic_considerations__15', 'question_15'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... F16
  {
    id:'F16',
    category:CATEGORIES.PERSONAL,
    text:'F16',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_16', 'opt_nature_of_patient__16', 'opt_nature_of_symptoms__16', 'opt_associated_symptoms__16', 'opt_precipitating_and_aggravating_factors__16', 'opt_ameliorating_factors__16', 'opt_physical_findings__16', 'opt_diagnostic_studies__16', 'opt_less_common_diagnostic_considerations__16', 'question_16'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... H17
  {
    id:'H17',
    category:CATEGORIES.PERSONAL,
    text:'H17',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_17', 'opt_nature_of_patient__17', 'opt_associated_symptoms__17', 'opt_precipitating_and_aggravating_factors__17', 'opt_ameliorating_factors__17', 'opt_physical_findings__17', 'opt_diagnostic_studies__17', 'opt_less_common_diagnostic_considerations__17', 'question_17'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... H18
  {
    id:'H18',
    category:CATEGORIES.PERSONAL,
    text:'H18',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_18', 'opt_nature_of_patient__18', 'opt_nature_of_symptoms__18', 'opt_associated_symptoms__18', 'opt_precipitating_and_aggravating_factors__18', 'opt_ameliorating_factors__18', 'opt_physical_findings__18', 'opt_diagnostic_studies__18', 'opt_less_common_diagnostic_considerations__18', 'question_18'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... I19
  {
    id:'I19',
    category:CATEGORIES.PERSONAL,
    text:'I19',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_19', 'opt_nature_of_patient__19', 'opt_nature_of_symptoms__19', 'opt_associated_symptoms__19', 'opt_precipitating_and_aggravating_factors__19', 'opt_ameliorating_factors__19', 'opt_physical_findings__19', 'opt_diagnostic_studies__19', 'opt_less_common_diagnostic_considerations__19', 'question_19'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... M20
  {
    id:'M20',
    category:CATEGORIES.PERSONAL,
    text:'M20',
    inputType:'radio',
    comment:'',
    options:['nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_nature_of_patient__20', 'opt_nature_of_symptoms__20', 'opt_associated_symptoms__20', 'opt_precipitating_and_aggravating_factors__20', 'opt_physical_findings__20', 'opt_diagnostic_studies__20', 'opt_less_common_diagnostic_considerations__20', 'question_20'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... M21
  {
    id:'M21',
    category:CATEGORIES.PERSONAL,
    text:'M21',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_21', 'opt_nature_of_patient__21', 'opt_nature_of_symptoms__21', 'opt_associated_symptoms__21', 'opt_precipitating_and_aggravating_factors__21', 'opt_ameliorating_factors__21', 'opt_physical_findings__21', 'opt_diagnostic_studies__21', 'opt_less_common_diagnostic_considerations__21', 'question_21'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... N22
  {
    id:'N22',
    category:CATEGORIES.PERSONAL,
    text:'N22',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_22', 'opt_nature_of_patient__22', 'opt_nature_of_symptoms__22', 'opt_associated_symptoms__22', 'opt_precipitating_and_aggravating_factors__22', 'opt_ameliorating_factors__22', 'opt_physical_findings__22', 'opt_diagnostic_studies__22', 'opt_less_common_diagnostic_considerations__22', 'question_22'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... P23
  {
    id:'P23',
    category:CATEGORIES.PERSONAL,
    text:'P23',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'precipitating and aggravating factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_23', 'opt_nature_of_patient__23', 'opt_nature_of_symptoms__23', 'opt_precipitating_and_aggravating_factors__23', 'opt_physical_findings__23', 'opt_diagnostic_studies__23', 'opt_less_common_diagnostic_considerations__23', 'question_23'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... P24
  {
    id:'P24',
    category:CATEGORIES.PERSONAL,
    text:'P24',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_24', 'opt_nature_of_patient__24', 'opt_nature_of_symptoms__24', 'opt_associated_symptoms__24', 'opt_precipitating_and_aggravating_factors__24', 'opt_ameliorating_factors__24', 'opt_physical_findings_and_diagnostic_studies__24', 'opt_less_common_diagnostic_considerations__24', 'question_24'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... P25
  {
    id:'P25',
    category:CATEGORIES.PERSONAL,
    text:'P25',
    inputType:'radio',
    comment:'',
    options:['nature of patient', 'nature of symptoms', 'associated symptoms', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_nature_of_patient__25', 'opt_nature_of_symptoms__25', 'opt_associated_symptoms__25', 'opt_physical_findings__25', 'opt_diagnostic_studies__25', 'opt_less_common_diagnostic_considerations__25', 'question_25'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... P26
  {
    id:'P26',
    category:CATEGORIES.PERSONAL,
    text:'P26',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_26', 'opt_nature_of_patient__26', 'opt_nature_of_symptoms__26', 'opt_associated_symptoms__26', 'opt_precipitating_and_aggravating_factors__26', 'opt_ameliorating_factors__26', 'opt_physical_findings__26', 'opt_diagnostic_studies__26', 'opt_less_common_diagnostic_considerations__26', 'question_26'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... P27
  {
    id:'P27',
    category:CATEGORIES.PERSONAL,
    text:'P27',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_27', 'opt_nature_of_patient__27', 'opt_nature_of_symptoms__27', 'opt_associated_symptoms__27', 'opt_precipitating_and_aggravating_factors__27', 'opt_ameliorating_factors__27', 'opt_physical_findings__27', 'opt_diagnostic_studies__27', 'opt_less_common_diagnostic_considerations__27', 'question_27'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... S28
  {
    id:'S28',
    category:CATEGORIES.PERSONAL,
    text:'S28',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_28', 'opt_nature_of_patient__28', 'opt_nature_of_symptoms__28', 'opt_associated_symptoms__28', 'opt_precipitating_and_aggravating_factors__28', 'opt_ameliorating_factors__28', 'opt_physical_findings__28', 'opt_diagnostic_studies__28', 'opt_less_common_diagnostic_considerations__28', 'question_28'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... S29
  {
    id:'S29',
    category:CATEGORIES.PERSONAL,
    text:'S29',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_29', 'opt_nature_of_patient__29', 'opt_nature_of_symptoms__29', 'opt_associated_symptoms__29', 'opt_precipitating_and_aggravating_factors__29', 'opt_ameliorating_factors__29', 'opt_physical_findings__29', 'opt_diagnostic_studies__29', 'opt_less_common_diagnostic_considerations__29', 'question_29'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... U30
  {
    id:'U30',
    category:CATEGORIES.PERSONAL,
    text:'U30',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_30', 'opt_nature_of_patient__30', 'opt_nature_of_symptoms___30', 'opt_associated_symptoms__30', 'opt_precipitating_and_aggravating_factors__30', 'opt_ameliorating_factors__30', 'opt_physical_findings__30', 'opt_diagnostic_studies___30', 'opt_less_common_diagnostic_considerations__30', 'question_30'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... V31
  {
    id:'V31',
    category:CATEGORIES.PERSONAL,
    text:'V31',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_31', 'opt_nature_of_patient__31', 'opt_nature_of_symptoms__31', 'opt_associated_symptoms__31', 'opt_precipitating_and_aggravating_factors__31', 'opt_physical_findings__31', 'opt_diagnostic_studies___31', 'opt_less_common_diagnostic_considerations__31', 'question_31'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... V32
  {
    id:'V32',
    category:CATEGORIES.PERSONAL,
    text:'V32',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_32', 'opt_nature_of_patient__32', 'opt_nature_of_symptoms__32', 'opt_associated_symptoms__32', 'opt_precipitating_and_aggravating_factors__32', 'opt_ameliorating_factors__32', 'opt_physical_findings__32', 'opt_diagnostic_studies___32', 'opt_less_common_diagnostic_considerations__32', 'question_32'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... V33
  {
    id:'V33',
    category:CATEGORIES.PERSONAL,
    text:'V33',
    inputType:'radio',
    comment:'',
    options:['introduction to complain', 'nature of patient', 'nature of symptoms', 'associated symptoms', 'precipitating and aggravating factors', 'ameliorating_factors', 'physical findings', 'diagnostic studies', 'less common diagnostic considerations', 'questionnaire'],
    nextQuestionMap:['opt_introduction_to_complain_33', 'opt_nature_of_patient__33', 'opt_nature_of_symptoms__33', 'opt_associated_symptoms__33', 'opt_precipitating_and_aggravating_factors__33', 'opt_ameliorating_factors__33', 'opt_physical_findings__33', 'opt_diagnostic_studies___33', 'opt_less_common_diagnostic_considerations__33', 'question_33'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_1
  {
    id:'opt_introduction_to_complain_1',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_1',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__1
  {
    id:'opt_nature_of_patient__1',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__1',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_pain__1
  {
    id:'opt_nature_of_pain__1',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_pain__1',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_location_of_pain__1
  {
    id:'opt_location_of_pain__1',
    category:CATEGORIES.PERSONAL,
    text:'opt_location_of_pain__1',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__1
  {
    id:'opt_associated_symptoms__1',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__1',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__1
  {
    id:'opt_precipitating_and_aggravating_factors__1',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__1',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__1
  {
    id:'opt_ameliorating_factors__1',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__1',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__1
  {
    id:'opt_physical_findings__1',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__1',
    inputType:'radio',
    comment:'',
    options:['Introduction physical findings ', ' Appendicitis ', 'Cholecystitis', 'Peptic Ulcer', 'Intestinal Obstruction', 'Salpingitis ', 'Irritable Bowel ', 'Acute Diverticulitis ', 'Ureterolithiasis '],
    nextQuestionMap:['0_Introduction physical findings ', '1_ Appendicitis ', '2_Cholecystitis', '3_Peptic Ulcer', '4_Intestinal Obstruction', '5_Salpingitis ', '6_Irritable Bowel ', '7_Acute Diverticulitis ', '8_Ureterolithiasis '],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__1
  {
    id:'opt_diagnostic_studies__1',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__1',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__1
  {
    id:'opt_less_common_diagnostic_considerations__1',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__1',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_2
  {
    id:'opt_introduction_to_complain_2',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_2',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__2
  {
    id:'opt_nature_of_patient__2',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__2',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_pain__2
  {
    id:'opt_nature_of_pain__2',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_pain__2',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_location_of_pain__2
  {
    id:'opt_location_of_pain__2',
    category:CATEGORIES.PERSONAL,
    text:'opt_location_of_pain__2',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__2
  {
    id:'opt_associated_symptoms__2',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__2',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__2
  {
    id:'opt_precipitating_and_aggravating_factors__2',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__2',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__2
  {
    id:'opt_ameliorating_factors__2',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__2',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__2
  {
    id:'opt_physical_findings__2',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__2',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__2
  {
    id:'opt_diagnostic_studies__2',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__2',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__2
  {
    id:'opt_less_common_diagnostic_considerations__2',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__2',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_3
  {
    id:'opt_introduction_to_complain_3',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_3',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__3
  {
    id:'opt_nature_of_patient__3',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__3',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms_and_location_of_pain__3
  {
    id:'opt_nature_of_symptoms_and_location_of_pain__3',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms_and_location_of_pain__3',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__3
  {
    id:'opt_associated_symptoms__3',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__3',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__3
  {
    id:'opt_precipitating_and_aggravating_factors__3',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__3',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__3
  {
    id:'opt_ameliorating_factors__3',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__3',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__3
  {
    id:'opt_physical_findings__3',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__3',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__3
  {
    id:'opt_diagnostic_studies__3',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__3',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__3
  {
    id:'opt_less_common_diagnostic_considerations__3',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__3',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_4
  {
    id:'opt_introduction_to_complain_4',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_4',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__4
  {
    id:'opt_nature_of_patient__4',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__4',
    inputType:'radio',
    comment:'',
    options:['Belching patient nature ', 'Bloating, and Flatulence patient nature '],
    nextQuestionMap:['0_Belching patient nature ', '1_Bloating, and Flatulence patient nature '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__4
  {
    id:'opt_associated_symptoms__4',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__4',
    inputType:'radio',
    comment:'',
    options:['Belching sympthoms associated ', ' Bloating, and Flatulence sympthoms associated '],
    nextQuestionMap:['0_Belching sympthoms associated ', '1_ Bloating, and Flatulence sympthoms associated '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__4
  {
    id:'opt_precipitating_and_aggravating_factors__4',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__4',
    inputType:'radio',
    comment:'',
    options:['Belching factors precipitating and aggravating ', ' Bloating, and Flatulence factors precipitating and aggravating '],
    nextQuestionMap:['0_Belching factors precipitating and aggravating ', '1_ Bloating, and Flatulence factors precipitating and aggravating '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__4
  {
    id:'opt_ameliorating_factors__4',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__4',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__4
  {
    id:'opt_physical_findings__4',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__4',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies___4
  {
    id:'opt_diagnostic_studies___4',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies___4',
    inputType:'radio',
    comment:'',
    options:['Belching diagnostic  ', 'Bloating, and Flatulence  diagnostic'],
    nextQuestionMap:['0_Belching diagnostic  ', '1_Bloating, and Flatulence  diagnostic'],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__4
  {
    id:'opt_less_common_diagnostic_considerations__4',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__4',
    inputType:'radio',
    comment:'',
    options:['Belching less common diagnostic ', 'Bloating, and Flatulence less common diagnostic '],
    nextQuestionMap:['0_Belching less common diagnostic ', '1_Bloating, and Flatulence less common diagnostic '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_5
  {
    id:'opt_introduction_to_complain_5',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_5',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__5
  {
    id:'opt_nature_of_patient__5',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__5',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__5
  {
    id:'opt_nature_of_symptoms__5',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__5',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__5
  {
    id:'opt_associated_symptoms__5',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__5',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__5
  {
    id:'opt_precipitating_and_aggravating_factors__5',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__5',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__5
  {
    id:'opt_ameliorating_factors__5',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__5',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__5
  {
    id:'opt_physical_findings__5',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__5',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__5
  {
    id:'opt_diagnostic_studies__5',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__5',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__5
  {
    id:'opt_less_common_diagnostic_considerations__5',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__5',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_6
  {
    id:'opt_introduction_to_complain_6',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_6',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__6
  {
    id:'opt_nature_of_patient__6',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__6',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_pain__6
  {
    id:'opt_nature_of_pain__6',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_pain__6',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__6
  {
    id:'opt_associated_symptoms__6',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__6',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__6
  {
    id:'opt_precipitating_and_aggravating_factors__6',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__6',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__6
  {
    id:'opt_ameliorating_factors__6',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__6',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__6
  {
    id:'opt_physical_findings__6',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__6',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__6
  {
    id:'opt_diagnostic_studies__6',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__6',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__6
  {
    id:'opt_less_common_diagnostic_considerations__6',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__6',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_7
  {
    id:'opt_introduction_to_complain_7',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_7',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__7
  {
    id:'opt_nature_of_patient__7',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__7',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms_and_associated_symptoms__7
  {
    id:'opt_nature_of_symptoms_and_associated_symptoms__7',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms_and_associated_symptoms__7',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__7
  {
    id:'opt_precipitating_and_aggravating_factors__7',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__7',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__7
  {
    id:'opt_ameliorating_factors__7',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__7',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__7
  {
    id:'opt_physical_findings__7',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__7',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__7
  {
    id:'opt_diagnostic_studies__7',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__7',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__7
  {
    id:'opt_less_common_diagnostic_considerations__7',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__7',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_8
  {
    id:'opt_introduction_to_complain_8',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_8',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__8
  {
    id:'opt_nature_of_patient__8',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__8',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__8
  {
    id:'opt_nature_of_symptoms__8',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__8',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__8
  {
    id:'opt_associated_symptoms__8',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__8',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__8
  {
    id:'opt_precipitating_and_aggravating_factors__8',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__8',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__8
  {
    id:'opt_ameliorating_factors__8',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__8',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__8
  {
    id:'opt_physical_findings__8',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__8',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__8
  {
    id:'opt_diagnostic_studies__8',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__8',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__8
  {
    id:'opt_less_common_diagnostic_considerations__8',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__8',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_9
  {
    id:'opt_introduction_to_complain_9',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_9',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__9
  {
    id:'opt_nature_of_patient__9',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__9',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__9
  {
    id:'opt_nature_of_symptoms__9',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__9',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__9
  {
    id:'opt_associated_symptoms__9',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__9',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__9
  {
    id:'opt_precipitating_and_aggravating_factors__9',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__9',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__9
  {
    id:'opt_ameliorating_factors__9',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__9',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__9
  {
    id:'opt_physical_findings__9',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__9',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__9
  {
    id:'opt_diagnostic_studies__9',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__9',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__9
  {
    id:'opt_less_common_diagnostic_considerations__9',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__9',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_10
  {
    id:'opt_introduction_to_complain_10',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_10',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__10
  {
    id:'opt_nature_of_patient__10',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__10',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__10
  {
    id:'opt_nature_of_symptoms__10',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__10',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__10
  {
    id:'opt_associated_symptoms__10',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__10',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__10
  {
    id:'opt_precipitating_and_aggravating_factors__10',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__10',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__10
  {
    id:'opt_ameliorating_factors__10',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__10',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__10
  {
    id:'opt_physical_findings__10',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__10',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__10
  {
    id:'opt_diagnostic_studies__10',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__10',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__10
  {
    id:'opt_less_common_diagnostic_considerations__10',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__10',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_chronic_diarrhea_in_children__10
  {
    id:'opt_chronic_diarrhea_in_children__10',
    category:CATEGORIES.PERSONAL,
    text:'opt_chronic_diarrhea_in_children__10',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_11
  {
    id:'opt_introduction_to_complain_11',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_11',
    inputType:'radio',
    comment:'',
    options:['Dizziness ', 'Lightheadedness and Vertigo '],
    nextQuestionMap:['0_Dizziness ', '1_Lightheadedness and Vertigo '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__11
  {
    id:'opt_nature_of_patient__11',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__11',
    inputType:'radio',
    comment:'',
    options:['TRUE VERTIGO patient nature ', 'LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS patient nature '],
    nextQuestionMap:['0_TRUE VERTIGO patient nature ', '1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS patient nature '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__11
  {
    id:'opt_nature_of_symptoms__11',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__11',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__11
  {
    id:'opt_associated_symptoms__11',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__11',
    inputType:'radio',
    comment:'',
    options:['TRUE VERTIGO symptoms associated  ', 'LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS symptoms associated  '],
    nextQuestionMap:['0_TRUE VERTIGO symptoms associated  ', '1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS symptoms associated  '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__11
  {
    id:'opt_precipitating_and_aggravating_factors__11',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__11',
    inputType:'radio',
    comment:'',
    options:['TRUE VERTIGO factors precipitationg and aggravate ', 'LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS factors precipitationg and aggravate '],
    nextQuestionMap:['0_TRUE VERTIGO factors precipitationg and aggravate ', '1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS factors precipitationg and aggravate '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__11
  {
    id:'opt_ameliorating_factors__11',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__11',
    inputType:'radio',
    comment:'',
    options:['', 'LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS factors ameliorationg '],
    nextQuestionMap:['0_', '1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS factors ameliorationg '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__11
  {
    id:'opt_physical_findings__11',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__11',
    inputType:'radio',
    comment:'',
    options:['LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS findings physical '],
    nextQuestionMap:['1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS findings physical '],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__11
  {
    id:'opt_diagnostic_studies__11',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__11',
    inputType:'radio',
    comment:'',
    options:['TRUE VERTIGO diagnostic ', 'LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS diagnostic '],
    nextQuestionMap:['0_TRUE VERTIGO diagnostic ', '1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS diagnostic '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__11
  {
    id:'opt_less_common_diagnostic_considerations__11',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__11',
    inputType:'radio',
    comment:'',
    options:['TRUE VERTIGO less common diagnostic ', 'LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS less common diagnostic '],
    nextQuestionMap:['0_TRUE VERTIGO less common diagnostic ', '1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS less common diagnostic '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_12
  {
    id:'opt_introduction_to_complain_12',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_12',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__12
  {
    id:'opt_nature_of_patient__12',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__12',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__12
  {
    id:'opt_nature_of_symptoms__12',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__12',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__12
  {
    id:'opt_associated_symptoms__12',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__12',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__12
  {
    id:'opt_precipitating_and_aggravating_factors__12',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__12',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__12
  {
    id:'opt_ameliorating_factors__12',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__12',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings_and_diagnostic_studies__12
  {
    id:'opt_physical_findings_and_diagnostic_studies__12',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings_and_diagnostic_studies__12',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__12
  {
    id:'opt_less_common_diagnostic_considerations__12',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__12',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_13
  {
    id:'opt_introduction_to_complain_13',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_13',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__13
  {
    id:'opt_nature_of_patient__13',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__13',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__13
  {
    id:'opt_nature_of_symptoms__13',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__13',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__13
  {
    id:'opt_associated_symptoms__13',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__13',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__13
  {
    id:'opt_precipitating_and_aggravating_factors__13',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__13',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__13
  {
    id:'opt_ameliorating_factors__13',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__13',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__13
  {
    id:'opt_physical_findings__13',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__13',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__13
  {
    id:'opt_diagnostic_studies__13',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__13',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__13
  {
    id:'opt_less_common_diagnostic_considerations__13',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__13',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_14
  {
    id:'opt_introduction_to_complain_14',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_14',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__14
  {
    id:'opt_nature_of_patient__14',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__14',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__14
  {
    id:'opt_nature_of_symptoms__14',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__14',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__14
  {
    id:'opt_associated_symptoms__14',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__14',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_exacerbating_factors__14
  {
    id:'opt_precipitating_and_exacerbating_factors__14',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_exacerbating_factors__14',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__14
  {
    id:'opt_ameliorating_factors__14',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__14',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__14
  {
    id:'opt_physical_findings__14',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__14',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__14
  {
    id:'opt_diagnostic_studies__14',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__14',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__14
  {
    id:'opt_less_common_diagnostic_considerations__14',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__14',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_15
  {
    id:'opt_introduction_to_complain_15',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_15',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__15
  {
    id:'opt_nature_of_patient__15',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__15',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__15
  {
    id:'opt_nature_of_symptoms__15',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__15',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__15
  {
    id:'opt_associated_symptoms__15',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__15',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__15
  {
    id:'opt_precipitating_and_aggravating_factors__15',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__15',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__15
  {
    id:'opt_physical_findings__15',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__15',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__15
  {
    id:'opt_diagnostic_studies__15',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__15',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__15
  {
    id:'opt_less_common_diagnostic_considerations__15',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__15',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_16
  {
    id:'opt_introduction_to_complain_16',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_16',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__16
  {
    id:'opt_nature_of_patient__16',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__16',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__16
  {
    id:'opt_nature_of_symptoms__16',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__16',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__16
  {
    id:'opt_associated_symptoms__16',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__16',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__16
  {
    id:'opt_precipitating_and_aggravating_factors__16',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__16',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__16
  {
    id:'opt_ameliorating_factors__16',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__16',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__16
  {
    id:'opt_physical_findings__16',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__16',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__16
  {
    id:'opt_diagnostic_studies__16',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__16',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__16
  {
    id:'opt_less_common_diagnostic_considerations__16',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__16',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_17
  {
    id:'opt_introduction_to_complain_17',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_17',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__17
  {
    id:'opt_nature_of_patient__17',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__17',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_pain__17
  {
    id:'opt_nature_of_pain__17',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_pain__17',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__17
  {
    id:'opt_associated_symptoms__17',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__17',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__17
  {
    id:'opt_precipitating_and_aggravating_factors__17',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__17',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__17
  {
    id:'opt_ameliorating_factors__17',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__17',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__17
  {
    id:'opt_physical_findings__17',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__17',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__17
  {
    id:'opt_diagnostic_studies__17',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__17',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__17
  {
    id:'opt_less_common_diagnostic_considerations__17',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__17',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_18
  {
    id:'opt_introduction_to_complain_18',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_18',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__18
  {
    id:'opt_nature_of_patient__18',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__18',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__18
  {
    id:'opt_nature_of_symptoms__18',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__18',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__18
  {
    id:'opt_associated_symptoms__18',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__18',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__18
  {
    id:'opt_precipitating_and_aggravating_factors__18',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__18',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__18
  {
    id:'opt_ameliorating_factors__18',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__18',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__18
  {
    id:'opt_physical_findings__18',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__18',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__18
  {
    id:'opt_diagnostic_studies__18',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__18',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__18
  {
    id:'opt_less_common_diagnostic_considerations__18',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__18',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_19
  {
    id:'opt_introduction_to_complain_19',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_19',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__19
  {
    id:'opt_nature_of_patient__19',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__19',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__19
  {
    id:'opt_nature_of_symptoms__19',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__19',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__19
  {
    id:'opt_associated_symptoms__19',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__19',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__19
  {
    id:'opt_precipitating_and_aggravating_factors__19',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__19',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__19
  {
    id:'opt_ameliorating_factors__19',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__19',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__19
  {
    id:'opt_physical_findings__19',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__19',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__19
  {
    id:'opt_diagnostic_studies__19',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__19',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__19
  {
    id:'opt_less_common_diagnostic_considerations__19',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__19',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_20
  {
    id:'opt_20',
    category:CATEGORIES.PERSONAL,
    text:'opt_20',
    inputType:'radio',
    comment:'',
    options:['other ', 'Anovulatory Bleeding ', 'Estrogen-Withdrawal Bleeding ', 'Estrogen-Breakthrough Bleeding ', 'Progesterone-Withdrawal Bleeding ', 'Progesterone-Breakthrough Bleeding ', 'Dysfunctional Ovulatory Bleeding ', 'Bleeding Patterns '],
    nextQuestionMap:['0_other ', '1_Anovulatory Bleeding ', '2_Estrogen-Withdrawal Bleeding ', '3_Estrogen-Breakthrough Bleeding ', '4_Progesterone-Withdrawal Bleeding ', '5_Progesterone-Breakthrough Bleeding ', '6_Dysfunctional Ovulatory Bleeding ', '7_Bleeding Patterns '],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__20
  {
    id:'opt_nature_of_patient__20',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__20',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__20
  {
    id:'opt_nature_of_symptoms__20',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__20',
    inputType:'radio',
    comment:'',
    options:['other symptoms ', 'Irregular Bleeding During Menstrual Life ', 'Bleeding Before Puberty and After Menopause ', 'Bleeding During Pregnancy '],
    nextQuestionMap:['0_other symptoms ', '1_Irregular Bleeding During Menstrual Life ', '2_Bleeding Before Puberty and After Menopause ', '3_Bleeding During Pregnancy '],
    scoreMap:[0, 0, 0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__20
  {
    id:'opt_associated_symptoms__20',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__20',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__20
  {
    id:'opt_precipitating_and_aggravating_factors__20',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__20',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__20
  {
    id:'opt_physical_findings__20',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__20',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__20
  {
    id:'opt_diagnostic_studies__20',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__20',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__20
  {
    id:'opt_less_common_diagnostic_considerations__20',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__20',
    inputType:'radio',
    comment:'',
    options:['introduction ', 'AMENORRHEA '],
    nextQuestionMap:['0_introduction ', '1_AMENORRHEA '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_21
  {
    id:'opt_introduction_to_complain_21',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_21',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__21
  {
    id:'opt_nature_of_patient__21',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__21',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__21
  {
    id:'opt_nature_of_symptoms__21',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__21',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__21
  {
    id:'opt_associated_symptoms__21',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__21',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__21
  {
    id:'opt_precipitating_and_aggravating_factors__21',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__21',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__21
  {
    id:'opt_ameliorating_factors__21',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__21',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__21
  {
    id:'opt_physical_findings__21',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__21',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__21
  {
    id:'opt_diagnostic_studies__21',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__21',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__21
  {
    id:'opt_less_common_diagnostic_considerations__21',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__21',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_22
  {
    id:'opt_introduction_to_complain_22',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_22',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__22
  {
    id:'opt_nature_of_patient__22',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__22',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__22
  {
    id:'opt_nature_of_symptoms__22',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__22',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__22
  {
    id:'opt_associated_symptoms__22',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__22',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__22
  {
    id:'opt_precipitating_and_aggravating_factors__22',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__22',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__22
  {
    id:'opt_ameliorating_factors__22',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__22',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__22
  {
    id:'opt_physical_findings__22',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__22',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__22
  {
    id:'opt_diagnostic_studies__22',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__22',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__22
  {
    id:'opt_less_common_diagnostic_considerations__22',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__22',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_23
  {
    id:'opt_introduction_to_complain_23',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_23',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__23
  {
    id:'opt_nature_of_patient__23',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__23',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__23
  {
    id:'opt_nature_of_symptoms__23',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__23',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__23
  {
    id:'opt_precipitating_and_aggravating_factors__23',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__23',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__23
  {
    id:'opt_physical_findings__23',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__23',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__23
  {
    id:'opt_diagnostic_studies__23',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__23',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__23
  {
    id:'opt_less_common_diagnostic_considerations__23',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__23',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_24
  {
    id:'opt_introduction_to_complain_24',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_24',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__24
  {
    id:'opt_nature_of_patient__24',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__24',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__24
  {
    id:'opt_nature_of_symptoms__24',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__24',
    inputType:'radio',
    comment:'',
    options:['other pain symptoms ', 'Degenerative Joint Disease ', 'Sciatica ', 'Effects of Exercise and Rest on Pain ', 'Thrombophlebitis ', 'Other Conditions '],
    nextQuestionMap:['0_other pain symptoms ', '1_Degenerative Joint Disease ', '2_Sciatica ', '3_Effects of Exercise and Rest on Pain ', '4_Thrombophlebitis ', '5_Other Conditions '],
    scoreMap:[0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__24
  {
    id:'opt_associated_symptoms__24',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__24',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__24
  {
    id:'opt_precipitating_and_aggravating_factors__24',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__24',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__24
  {
    id:'opt_ameliorating_factors__24',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__24',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings_and_diagnostic_studies__24
  {
    id:'opt_physical_findings_and_diagnostic_studies__24',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings_and_diagnostic_studies__24',
    inputType:'radio',
    comment:'',
    options:['other findings physical ', 'Muscular and Ligamentous Strains and Tears ', 'Degenerative Joint Disease ', 'Sciatica ', 'Intermittent Claudication ', 'Thrombophlebitis ', 'Peripatellar '],
    nextQuestionMap:['0_other findings physical ', '1_Muscular and Ligamentous Strains and Tears ', '2_Degenerative Joint Disease ', '3_Sciatica ', '4_Intermittent Claudication ', '5_Thrombophlebitis ', '6_Peripatellar '],
    scoreMap:[0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__24
  {
    id:'opt_less_common_diagnostic_considerations__24',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__24',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_of_complain_25
  {
    id:'opt_introduction_of_complain_25',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_of_complain_25',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__25
  {
    id:'opt_nature_of_patient__25',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__25',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__25
  {
    id:'opt_nature_of_symptoms__25',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__25',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__25
  {
    id:'opt_associated_symptoms__25',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__25',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__25
  {
    id:'opt_physical_findings__25',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__25',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__25
  {
    id:'opt_diagnostic_studies__25',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__25',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__25
  {
    id:'opt_less_common_diagnostic_considerations__25',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__25',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_26
  {
    id:'opt_introduction_to_complain_26',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_26',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__26
  {
    id:'opt_nature_of_patient__26',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__26',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'opt_nature_of_symptoms__26',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__26',
    inputType:'radio',
    comment:'',
    options:['other symptoms nature ', 'Involvement of Supraspinatus Tendon ', 'Involvement of Teres Minor, Infraspinatus, and Subscapularis Mus-cles\u2003', 'Tears and Ruptures of Rotator Cuff ', 'Calcific Tendinitis ', 'Bicipital and Tricipital Tendinitis ', 'Bursitis ', 'Frozen Shoulder ', 'Subluxation or Dislocation ', 'Impingement Syndrome ', 'Site of Pain ', 'Pain from Vigorous Physical Activity ', 'Arthritis ', 'Nonarticular Pain '],
    nextQuestionMap:['0_other symptoms nature ', '1_Involvement of Supraspinatus Tendon ', '2_Involvement of Teres Minor, Infraspinatus, and Subscapularis Mus-cles\u2003', '3_Tears and Ruptures of Rotator Cuff ', '4_Calcific Tendinitis ', '5_Bicipital and Tricipital Tendinitis ', '6_Bursitis ', '7_Frozen Shoulder ', '8_Subluxation or Dislocation ', '9_Impingement Syndrome ', '10_Site of Pain ', '11_Pain from Vigorous Physical Activity ', '12_Arthritis ', '13_Nonarticular Pain '],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__26
  {
    id:'opt_associated_symptoms__26',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__26',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__26
  {
    id:'opt_precipitating_and_aggravating_factors__26',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__26',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__26
  {
    id:'opt_ameliorating_factors__26',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__26',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__26
  {
    id:'opt_physical_findings__26',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__26',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__26
  {
    id:'opt_diagnostic_studies__26',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__26',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__26
  {
    id:'opt_less_common_diagnostic_considerations__26',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__26',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_27
  {
    id:'opt_introduction_to_complain_27',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_27',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__27
  {
    id:'opt_nature_of_patient__27',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__27',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__27
  {
    id:'opt_nature_of_symptoms__27',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__27',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__27
  {
    id:'opt_associated_symptoms__27',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__27',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__27
  {
    id:'opt_precipitating_and_aggravating_factors__27',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__27',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__27
  {
    id:'opt_ameliorating_factors__27',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__27',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__27
  {
    id:'opt_physical_findings__27',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__27',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__27
  {
    id:'opt_diagnostic_studies__27',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__27',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__27
  {
    id:'opt_less_common_diagnostic_considerations__27',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__27',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_28
  {
    id:'opt_introduction_to_complain_28',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_28',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__28
  {
    id:'opt_nature_of_patient__28',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__28',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__28
  {
    id:'opt_nature_of_symptoms__28',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__28',
    inputType:'radio',
    comment:'',
    options:['other symptoms nature ', 'Acute Dyspnea ', 'Chronic Dyspnea '],
    nextQuestionMap:['0_other symptoms nature ', '1_Acute Dyspnea ', '2_Chronic Dyspnea '],
    scoreMap:[0, 0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__28
  {
    id:'opt_associated_symptoms__28',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__28',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__28
  {
    id:'opt_precipitating_and_aggravating_factors__28',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__28',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__28
  {
    id:'opt_ameliorating_factors__28',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__28',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__28
  {
    id:'opt_physical_findings__28',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__28',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__28
  {
    id:'opt_diagnostic_studies__28',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__28',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__28
  {
    id:'opt_less_common_diagnostic_considerations__28',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__28',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_29
  {
    id:'opt_introduction_to_complain_29',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_29',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__29
  {
    id:'opt_nature_of_patient__29',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__29',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__29
  {
    id:'opt_nature_of_symptoms__29',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__29',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__29
  {
    id:'opt_associated_symptoms__29',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__29',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__29
  {
    id:'opt_precipitating_and_aggravating_factors__29',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__29',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__29
  {
    id:'opt_ameliorating_factors__29',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__29',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__29
  {
    id:'opt_physical_findings__29',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__29',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies__29
  {
    id:'opt_diagnostic_studies__29',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies__29',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__29
  {
    id:'opt_less_common_diagnostic_considerations__29',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__29',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_30
  {
    id:'opt_introduction_to_complain_30',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_30',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__30
  {
    id:'opt_nature_of_patient__30',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__30',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms___30
  {
    id:'opt_nature_of_symptoms___30',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms___30',
    inputType:'radio',
    comment:'',
    options:['Cystitis ', 'Urethritis '],
    nextQuestionMap:['0_Cystitis ', '1_Urethritis '],
    scoreMap:[0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__30
  {
    id:'opt_associated_symptoms__30',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__30',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__30
  {
    id:'opt_precipitating_and_aggravating_factors__30',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__30',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__30
  {
    id:'opt_ameliorating_factors__30',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__30',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__30
  {
    id:'opt_physical_findings__30',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__30',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies___30
  {
    id:'opt_diagnostic_studies___30',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies___30',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__30
  {
    id:'opt_less_common_diagnostic_considerations__30',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__30',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_31
  {
    id:'opt_introduction_to_complain_31',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_31',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__31
  {
    id:'opt_nature_of_patient__31',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__31',
    inputType:'radio',
    comment:'',
    options:['Prepubertal Patients ', 'Adolescents ', 'Adults '],
    nextQuestionMap:['0_Prepubertal Patients ', '1_Adolescents ', '2_Adults '],
    scoreMap:[0, 0, 0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__31
  {
    id:'opt_nature_of_symptoms__31',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__31',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__31
  {
    id:'opt_associated_symptoms__31',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__31',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__31
  {
    id:'opt_precipitating_and_aggravating_factors__31',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__31',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__31
  {
    id:'opt_physical_findings__31',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__31',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies___31
  {
    id:'opt_diagnostic_studies___31',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies___31',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__31
  {
    id:'opt_less_common_diagnostic_considerations__31',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__31',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_32
  {
    id:'opt_introduction_to_complain_32',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_32',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__32
  {
    id:'opt_nature_of_patient__32',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__32',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__32
  {
    id:'opt_nature_of_symptoms__32',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__32',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__32
  {
    id:'opt_associated_symptoms__32',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__32',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__32
  {
    id:'opt_precipitating_and_aggravating_factors__32',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__32',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__32
  {
    id:'opt_ameliorating_factors__32',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__32',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__32
  {
    id:'opt_physical_findings__32',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__32',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies___32
  {
    id:'opt_diagnostic_studies___32',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies___32',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__32
  {
    id:'opt_less_common_diagnostic_considerations__32',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__32',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_introduction_to_complain_33
  {
    id:'opt_introduction_to_complain_33',
    category:CATEGORIES.PERSONAL,
    text:'opt_introduction_to_complain_33',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_patient__33
  {
    id:'opt_nature_of_patient__33',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_patient__33',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_nature_of_symptoms__33
  {
    id:'opt_nature_of_symptoms__33',
    category:CATEGORIES.PERSONAL,
    text:'opt_nature_of_symptoms__33',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_associated_symptoms__33
  {
    id:'opt_associated_symptoms__33',
    category:CATEGORIES.PERSONAL,
    text:'opt_associated_symptoms__33',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_precipitating_and_aggravating_factors__33
  {
    id:'opt_precipitating_and_aggravating_factors__33',
    category:CATEGORIES.PERSONAL,
    text:'opt_precipitating_and_aggravating_factors__33',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_ameliorating_factors__33
  {
    id:'opt_ameliorating_factors__33',
    category:CATEGORIES.PERSONAL,
    text:'opt_ameliorating_factors__33',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_physical_findings__33
  {
    id:'opt_physical_findings__33',
    category:CATEGORIES.PERSONAL,
    text:'opt_physical_findings__33',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_diagnostic_studies___33
  {
    id:'opt_diagnostic_studies___33',
    category:CATEGORIES.PERSONAL,
    text:'opt_diagnostic_studies___33',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... opt_less_common_diagnostic_considerations__33
  {
    id:'opt_less_common_diagnostic_considerations__33',
    category:CATEGORIES.PERSONAL,
    text:'opt_less_common_diagnostic_considerations__33',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings__1
  {
    id:'0_Introduction physical findings ',
    category:CATEGORIES.PERSONAL,
    text:'0_Introduction physical findings ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings__1
  {
    id:'1_ Appendicitis ',
    category:CATEGORIES.PERSONAL,
    text:'1_ Appendicitis ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings__1
  {
    id:'2_Cholecystitis',
    category:CATEGORIES.PERSONAL,
    text:'2_Cholecystitis',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings__1
  {
    id:'3_Peptic Ulcer',
    category:CATEGORIES.PERSONAL,
    text:'3_Peptic Ulcer',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings__1
  {
    id:'4_Intestinal Obstruction',
    category:CATEGORIES.PERSONAL,
    text:'4_Intestinal Obstruction',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings__1
  {
    id:'5_Salpingitis ',
    category:CATEGORIES.PERSONAL,
    text:'5_Salpingitis ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings__1
  {
    id:'6_Irritable Bowel ',
    category:CATEGORIES.PERSONAL,
    text:'6_Irritable Bowel ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings__1
  {
    id:'7_Acute Diverticulitis ',
    category:CATEGORIES.PERSONAL,
    text:'7_Acute Diverticulitis ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings__1
  {
    id:'8_Ureterolithiasis ',
    category:CATEGORIES.PERSONAL,
    text:'8_Ureterolithiasis ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_patient__4
  {
    id:'0_Belching patient nature ',
    category:CATEGORIES.PERSONAL,
    text:'0_Belching patient nature ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_patient__4
  {
    id:'1_Bloating, and Flatulence patient nature ',
    category:CATEGORIES.PERSONAL,
    text:'1_Bloating, and Flatulence patient nature ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_associated_symptoms__4
  {
    id:'0_Belching sympthoms associated ',
    category:CATEGORIES.PERSONAL,
    text:'0_Belching sympthoms associated ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_associated_symptoms__4
  {
    id:'1_ Bloating, and Flatulence sympthoms associated ',
    category:CATEGORIES.PERSONAL,
    text:'1_ Bloating, and Flatulence sympthoms associated ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_precipitating_and_aggravating_factors__4
  {
    id:'0_Belching factors precipitating and aggravating ',
    category:CATEGORIES.PERSONAL,
    text:'0_Belching factors precipitating and aggravating ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_precipitating_and_aggravating_factors__4
  {
    id:'1_ Bloating, and Flatulence factors precipitating and aggravating ',
    category:CATEGORIES.PERSONAL,
    text:'1_ Bloating, and Flatulence factors precipitating and aggravating ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_diagnostic_studies___4
  {
    id:'0_Belching diagnostic  ',
    category:CATEGORIES.PERSONAL,
    text:'0_Belching diagnostic  ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_diagnostic_studies___4
  {
    id:'1_Bloating, and Flatulence  diagnostic',
    category:CATEGORIES.PERSONAL,
    text:'1_Bloating, and Flatulence  diagnostic',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_less_common_diagnostic_considerations__4
  {
    id:'0_Belching less common diagnostic ',
    category:CATEGORIES.PERSONAL,
    text:'0_Belching less common diagnostic ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_less_common_diagnostic_considerations__4
  {
    id:'1_Bloating, and Flatulence less common diagnostic ',
    category:CATEGORIES.PERSONAL,
    text:'1_Bloating, and Flatulence less common diagnostic ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_introduction_to_complain_11
  {
    id:'0_Dizziness ',
    category:CATEGORIES.PERSONAL,
    text:'0_Dizziness ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_introduction_to_complain_11
  {
    id:'1_Lightheadedness and Vertigo ',
    category:CATEGORIES.PERSONAL,
    text:'1_Lightheadedness and Vertigo ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_patient__11
  {
    id:'0_TRUE VERTIGO patient nature ',
    category:CATEGORIES.PERSONAL,
    text:'0_TRUE VERTIGO patient nature ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_patient__11
  {
    id:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS patient nature ',
    category:CATEGORIES.PERSONAL,
    text:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS patient nature ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_associated_symptoms__11
  {
    id:'0_TRUE VERTIGO symptoms associated  ',
    category:CATEGORIES.PERSONAL,
    text:'0_TRUE VERTIGO symptoms associated  ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_associated_symptoms__11
  {
    id:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS symptoms associated  ',
    category:CATEGORIES.PERSONAL,
    text:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS symptoms associated  ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_precipitating_and_aggravating_factors__11
  {
    id:'0_TRUE VERTIGO factors precipitationg and aggravate ',
    category:CATEGORIES.PERSONAL,
    text:'0_TRUE VERTIGO factors precipitationg and aggravate ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_precipitating_and_aggravating_factors__11
  {
    id:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS factors precipitationg and aggravate ',
    category:CATEGORIES.PERSONAL,
    text:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS factors precipitationg and aggravate ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_ameliorating_factors__11
  {
    id:'0_',
    category:CATEGORIES.PERSONAL,
    text:'0_',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_ameliorating_factors__11
  {
    id:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS factors ameliorationg ',
    category:CATEGORIES.PERSONAL,
    text:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS factors ameliorationg ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings__11
  {
    id:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS findings physical ',
    category:CATEGORIES.PERSONAL,
    text:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS findings physical ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_diagnostic_studies__11
  {
    id:'0_TRUE VERTIGO diagnostic ',
    category:CATEGORIES.PERSONAL,
    text:'0_TRUE VERTIGO diagnostic ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_diagnostic_studies__11
  {
    id:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS diagnostic ',
    category:CATEGORIES.PERSONAL,
    text:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS diagnostic ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_less_common_diagnostic_considerations__11
  {
    id:'0_TRUE VERTIGO less common diagnostic ',
    category:CATEGORIES.PERSONAL,
    text:'0_TRUE VERTIGO less common diagnostic ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_less_common_diagnostic_considerations__11
  {
    id:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS less common diagnostic ',
    category:CATEGORIES.PERSONAL,
    text:'1_LIGHTHEADEDNESS/DIZZINESS AND GIDDINESS less common diagnostic ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_20
  {
    id:'0_other ',
    category:CATEGORIES.PERSONAL,
    text:'0_other ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_20
  {
    id:'1_Anovulatory Bleeding ',
    category:CATEGORIES.PERSONAL,
    text:'1_Anovulatory Bleeding ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_20
  {
    id:'2_Estrogen-Withdrawal Bleeding ',
    category:CATEGORIES.PERSONAL,
    text:'2_Estrogen-Withdrawal Bleeding ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_20
  {
    id:'3_Estrogen-Breakthrough Bleeding ',
    category:CATEGORIES.PERSONAL,
    text:'3_Estrogen-Breakthrough Bleeding ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_20
  {
    id:'4_Progesterone-Withdrawal Bleeding ',
    category:CATEGORIES.PERSONAL,
    text:'4_Progesterone-Withdrawal Bleeding ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_20
  {
    id:'5_Progesterone-Breakthrough Bleeding ',
    category:CATEGORIES.PERSONAL,
    text:'5_Progesterone-Breakthrough Bleeding ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_20
  {
    id:'6_Dysfunctional Ovulatory Bleeding ',
    category:CATEGORIES.PERSONAL,
    text:'6_Dysfunctional Ovulatory Bleeding ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_20
  {
    id:'7_Bleeding Patterns ',
    category:CATEGORIES.PERSONAL,
    text:'7_Bleeding Patterns ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__20
  {
    id:'0_other symptoms ',
    category:CATEGORIES.PERSONAL,
    text:'0_other symptoms ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__20
  {
    id:'1_Irregular Bleeding During Menstrual Life ',
    category:CATEGORIES.PERSONAL,
    text:'1_Irregular Bleeding During Menstrual Life ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__20
  {
    id:'2_Bleeding Before Puberty and After Menopause ',
    category:CATEGORIES.PERSONAL,
    text:'2_Bleeding Before Puberty and After Menopause ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__20
  {
    id:'3_Bleeding During Pregnancy ',
    category:CATEGORIES.PERSONAL,
    text:'3_Bleeding During Pregnancy ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_less_common_diagnostic_considerations__20
  {
    id:'0_introduction ',
    category:CATEGORIES.PERSONAL,
    text:'0_introduction ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_less_common_diagnostic_considerations__20
  {
    id:'1_AMENORRHEA ',
    category:CATEGORIES.PERSONAL,
    text:'1_AMENORRHEA ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__24
  {
    id:'0_other pain symptoms ',
    category:CATEGORIES.PERSONAL,
    text:'0_other pain symptoms ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__24
  {
    id:'1_Degenerative Joint Disease ',
    category:CATEGORIES.PERSONAL,
    text:'1_Degenerative Joint Disease ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__24
  {
    id:'2_Sciatica ',
    category:CATEGORIES.PERSONAL,
    text:'2_Sciatica ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__24
  {
    id:'3_Effects of Exercise and Rest on Pain ',
    category:CATEGORIES.PERSONAL,
    text:'3_Effects of Exercise and Rest on Pain ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__24
  {
    id:'4_Thrombophlebitis ',
    category:CATEGORIES.PERSONAL,
    text:'4_Thrombophlebitis ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__24
  {
    id:'5_Other Conditions ',
    category:CATEGORIES.PERSONAL,
    text:'5_Other Conditions ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings_and_diagnostic_studies__24
  {
    id:'0_other findings physical ',
    category:CATEGORIES.PERSONAL,
    text:'0_other findings physical ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings_and_diagnostic_studies__24
  {
    id:'1_Muscular and Ligamentous Strains and Tears ',
    category:CATEGORIES.PERSONAL,
    text:'1_Muscular and Ligamentous Strains and Tears ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings_and_diagnostic_studies__24
  {
    id:'2_Degenerative Joint Disease ',
    category:CATEGORIES.PERSONAL,
    text:'2_Degenerative Joint Disease ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings_and_diagnostic_studies__24
  {
    id:'3_Sciatica ',
    category:CATEGORIES.PERSONAL,
    text:'3_Sciatica ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings_and_diagnostic_studies__24
  {
    id:'4_Intermittent Claudication ',
    category:CATEGORIES.PERSONAL,
    text:'4_Intermittent Claudication ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings_and_diagnostic_studies__24
  {
    id:'5_Thrombophlebitis ',
    category:CATEGORIES.PERSONAL,
    text:'5_Thrombophlebitis ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_physical_findings_and_diagnostic_studies__24
  {
    id:'6_Peripatellar ',
    category:CATEGORIES.PERSONAL,
    text:'6_Peripatellar ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'0_other symptoms nature ',
    category:CATEGORIES.PERSONAL,
    text:'0_other symptoms nature ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'1_Involvement of Supraspinatus Tendon ',
    category:CATEGORIES.PERSONAL,
    text:'1_Involvement of Supraspinatus Tendon ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'2_Involvement of Teres Minor, Infraspinatus, and Subscapularis Mus-cles ',
    category:CATEGORIES.PERSONAL,
    text:'2_Involvement of Teres Minor, Infraspinatus, and Subscapularis Mus-cles ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'3_Tears and Ruptures of Rotator Cuff ',
    category:CATEGORIES.PERSONAL,
    text:'3_Tears and Ruptures of Rotator Cuff ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'4_Calcific Tendinitis ',
    category:CATEGORIES.PERSONAL,
    text:'4_Calcific Tendinitis ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'5_Bicipital and Tricipital Tendinitis ',
    category:CATEGORIES.PERSONAL,
    text:'5_Bicipital and Tricipital Tendinitis ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'6_Bursitis ',
    category:CATEGORIES.PERSONAL,
    text:'6_Bursitis ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'7_Frozen Shoulder ',
    category:CATEGORIES.PERSONAL,
    text:'7_Frozen Shoulder ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'8_Subluxation or Dislocation ',
    category:CATEGORIES.PERSONAL,
    text:'8_Subluxation or Dislocation ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'9_Impingement Syndrome ',
    category:CATEGORIES.PERSONAL,
    text:'9_Impingement Syndrome ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'10_Site of Pain ',
    category:CATEGORIES.PERSONAL,
    text:'10_Site of Pain ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'11_Pain from Vigorous Physical Activity ',
    category:CATEGORIES.PERSONAL,
    text:'11_Pain from Vigorous Physical Activity ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'12_Arthritis ',
    category:CATEGORIES.PERSONAL,
    text:'12_Arthritis ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__26
  {
    id:'13_Nonarticular Pain ',
    category:CATEGORIES.PERSONAL,
    text:'13_Nonarticular Pain ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__28
  {
    id:'0_other symptoms nature ',
    category:CATEGORIES.PERSONAL,
    text:'0_other symptoms nature ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__28
  {
    id:'1_Acute Dyspnea ',
    category:CATEGORIES.PERSONAL,
    text:'1_Acute Dyspnea ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms__28
  {
    id:'2_Chronic Dyspnea ',
    category:CATEGORIES.PERSONAL,
    text:'2_Chronic Dyspnea ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms___30
  {
    id:'0_Cystitis ',
    category:CATEGORIES.PERSONAL,
    text:'0_Cystitis ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_symptoms___30
  {
    id:'1_Urethritis ',
    category:CATEGORIES.PERSONAL,
    text:'1_Urethritis ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_patient__31
  {
    id:'0_Prepubertal Patients ',
    category:CATEGORIES.PERSONAL,
    text:'0_Prepubertal Patients ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_patient__31
  {
    id:'1_Adolescents ',
    category:CATEGORIES.PERSONAL,
    text:'1_Adolescents ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  SOUS OPTIONS DES OPTIONS ... opt_nature_of_patient__31
  {
    id:'2_Adults ',
    category:CATEGORIES.PERSONAL,
    text:'2_Adults ',
    inputType:'radio',
    comment:'',
    options:['home'],
    nextQuestionMap:['question'],
    scoreMap:[0]
  },
//PARTIE  QUESTION REPONSE   ... question_1
  {
    id:'question_1',
    category:CATEGORIES.PERSONAL,
    text:'question_1',
    inputType:'radio',
    comment:'',
    options:['reponse_10', 'reponse_11', 'reponse_12', 'reponse_13', 'reponse_14', 'reponse_15', 'reponse_16', 'reponse_17', 'reponse_18', 'reponse_19', 'reponse_110', 'reponse_111', 'reponse_112', 'reponse_113', 'reponse_114', 'reponse_115', 'reponse_116', 'reponse_117', 'reponse_118'],
    nextQuestionMap:['reponse_10', 'reponse_11', 'reponse_12', 'reponse_13', 'reponse_14', 'reponse_15', 'reponse_16', 'reponse_17', 'reponse_18', 'reponse_19', 'reponse_110', 'reponse_111', 'reponse_112', 'reponse_113', 'reponse_114', 'reponse_115', 'reponse_116', 'reponse_117', 'reponse_118'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_2
  {
    id:'question_2',
    category:CATEGORIES.PERSONAL,
    text:'question_2',
    inputType:'radio',
    comment:'',
    options:['reponse_20', 'reponse_21', 'reponse_22', 'reponse_23', 'reponse_24', 'reponse_25', 'reponse_26', 'reponse_27'],
    nextQuestionMap:['reponse_20', 'reponse_21', 'reponse_22', 'reponse_23', 'reponse_24', 'reponse_25', 'reponse_26', 'reponse_27'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_3
  {
    id:'question_3',
    category:CATEGORIES.PERSONAL,
    text:'question_3',
    inputType:'radio',
    comment:'',
    options:['reponse_30', 'reponse_31', 'reponse_32', 'reponse_33', 'reponse_34', 'reponse_35', 'reponse_36', 'reponse_37', 'reponse_38', 'reponse_39', 'reponse_310', 'reponse_311', 'reponse_312'],
    nextQuestionMap:['reponse_30', 'reponse_31', 'reponse_32', 'reponse_33', 'reponse_34', 'reponse_35', 'reponse_36', 'reponse_37', 'reponse_38', 'reponse_39', 'reponse_310', 'reponse_311', 'reponse_312'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_4
  {
    id:'question_4',
    category:CATEGORIES.PERSONAL,
    text:'question_4',
    inputType:'radio',
    comment:'',
    options:['reponse_40', 'reponse_41', 'reponse_42', 'reponse_43', 'reponse_44', 'reponse_45', 'reponse_46'],
    nextQuestionMap:['reponse_40', 'reponse_41', 'reponse_42', 'reponse_43', 'reponse_44', 'reponse_45', 'reponse_46'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_5
  {
    id:'question_5',
    category:CATEGORIES.PERSONAL,
    text:'question_5',
    inputType:'radio',
    comment:'',
    options:['reponse_50', 'reponse_51', 'reponse_52', 'reponse_53', 'reponse_54'],
    nextQuestionMap:['reponse_50', 'reponse_51', 'reponse_52', 'reponse_53', 'reponse_54'],
    scoreMap:[0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_6
  {
    id:'question_6',
    category:CATEGORIES.PERSONAL,
    text:'question_6',
    inputType:'radio',
    comment:'',
    options:['reponse_60', 'reponse_61', 'reponse_62', 'reponse_63', 'reponse_64', 'reponse_65', 'reponse_66', 'reponse_67', 'reponse_68', 'reponse_69', 'reponse_610', 'reponse_611', 'reponse_612', 'reponse_613', 'reponse_614'],
    nextQuestionMap:['reponse_60', 'reponse_61', 'reponse_62', 'reponse_63', 'reponse_64', 'reponse_65', 'reponse_66', 'reponse_67', 'reponse_68', 'reponse_69', 'reponse_610', 'reponse_611', 'reponse_612', 'reponse_613', 'reponse_614'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_7
  {
    id:'question_7',
    category:CATEGORIES.PERSONAL,
    text:'question_7',
    inputType:'radio',
    comment:'',
    options:['reponse_70', 'reponse_71', 'reponse_72', 'reponse_73', 'reponse_74', 'reponse_75', 'reponse_76', 'reponse_77', 'reponse_78', 'reponse_79'],
    nextQuestionMap:['reponse_70', 'reponse_71', 'reponse_72', 'reponse_73', 'reponse_74', 'reponse_75', 'reponse_76', 'reponse_77', 'reponse_78', 'reponse_79'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_8
  {
    id:'question_8',
    category:CATEGORIES.PERSONAL,
    text:'question_8',
    inputType:'radio',
    comment:'',
    options:['reponse_80', 'reponse_81', 'reponse_82', 'reponse_83', 'reponse_84', 'reponse_85', 'reponse_86', 'reponse_87', 'reponse_88', 'reponse_89', 'reponse_810', 'reponse_811'],
    nextQuestionMap:['reponse_80', 'reponse_81', 'reponse_82', 'reponse_83', 'reponse_84', 'reponse_85', 'reponse_86', 'reponse_87', 'reponse_88', 'reponse_89', 'reponse_810', 'reponse_811'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_9
  {
    id:'question_9',
    category:CATEGORIES.PERSONAL,
    text:'question_9',
    inputType:'radio',
    comment:'',
    options:['reponse_90', 'reponse_91', 'reponse_92', 'reponse_93', 'reponse_94', 'reponse_95', 'reponse_96', 'reponse_97', 'reponse_98', 'reponse_99', 'reponse_910'],
    nextQuestionMap:['reponse_90', 'reponse_91', 'reponse_92', 'reponse_93', 'reponse_94', 'reponse_95', 'reponse_96', 'reponse_97', 'reponse_98', 'reponse_99', 'reponse_910'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_10
  {
    id:'question_10',
    category:CATEGORIES.PERSONAL,
    text:'question_10',
    inputType:'radio',
    comment:'',
    options:['reponse_100', 'reponse_101', 'reponse_102', 'reponse_103', 'reponse_104', 'reponse_105', 'reponse_106', 'reponse_107', 'reponse_108', 'reponse_109', 'reponse_1010', 'reponse_1011', 'reponse_1012', 'reponse_1013', 'reponse_1014', 'reponse_1015', 'reponse_1016', 'reponse_1017'],
    nextQuestionMap:['reponse_100', 'reponse_101', 'reponse_102', 'reponse_103', 'reponse_104', 'reponse_105', 'reponse_106', 'reponse_107', 'reponse_108', 'reponse_109', 'reponse_1010', 'reponse_1011', 'reponse_1012', 'reponse_1013', 'reponse_1014', 'reponse_1015', 'reponse_1016', 'reponse_1017'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_11
  {
    id:'question_11',
    category:CATEGORIES.PERSONAL,
    text:'question_11',
    inputType:'radio',
    comment:'',
    options:['reponse_110', 'reponse_111', 'reponse_112', 'reponse_113', 'reponse_114', 'reponse_115', 'reponse_116', 'reponse_117', 'reponse_118', 'reponse_119', 'reponse_1110', 'reponse_1111', 'reponse_1112', 'reponse_1113', 'reponse_1114', 'reponse_1115', 'reponse_1116'],
    nextQuestionMap:['reponse_110', 'reponse_111', 'reponse_112', 'reponse_113', 'reponse_114', 'reponse_115', 'reponse_116', 'reponse_117', 'reponse_118', 'reponse_119', 'reponse_1110', 'reponse_1111', 'reponse_1112', 'reponse_1113', 'reponse_1114', 'reponse_1115', 'reponse_1116'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_12
  {
    id:'question_12',
    category:CATEGORIES.PERSONAL,
    text:'question_12',
    inputType:'radio',
    comment:'',
    options:['reponse_120', 'reponse_121', 'reponse_122', 'reponse_123', 'reponse_124', 'reponse_125', 'reponse_126', 'reponse_127', 'reponse_128', 'reponse_129'],
    nextQuestionMap:['reponse_120', 'reponse_121', 'reponse_122', 'reponse_123', 'reponse_124', 'reponse_125', 'reponse_126', 'reponse_127', 'reponse_128', 'reponse_129'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_13
  {
    id:'question_13',
    category:CATEGORIES.PERSONAL,
    text:'question_13',
    inputType:'radio',
    comment:'',
    options:['reponse_130', 'reponse_131', 'reponse_132', 'reponse_133', 'reponse_134', 'reponse_135', 'reponse_136', 'reponse_137', 'reponse_138', 'reponse_139', 'reponse_1310', 'reponse_1311', 'reponse_1312'],
    nextQuestionMap:['reponse_130', 'reponse_131', 'reponse_132', 'reponse_133', 'reponse_134', 'reponse_135', 'reponse_136', 'reponse_137', 'reponse_138', 'reponse_139', 'reponse_1310', 'reponse_1311', 'reponse_1312'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_14
  {
    id:'question_14',
    category:CATEGORIES.PERSONAL,
    text:'question_14',
    inputType:'radio',
    comment:'',
    options:['reponse_140', 'reponse_141', 'reponse_142', 'reponse_143', 'reponse_144', 'reponse_145', 'reponse_146', 'reponse_147', 'reponse_148', 'reponse_149', 'reponse_1410', 'reponse_1411', 'reponse_1412', 'reponse_1413', 'reponse_1414', 'reponse_1415', 'reponse_1416', 'reponse_1417'],
    nextQuestionMap:['reponse_140', 'reponse_141', 'reponse_142', 'reponse_143', 'reponse_144', 'reponse_145', 'reponse_146', 'reponse_147', 'reponse_148', 'reponse_149', 'reponse_1410', 'reponse_1411', 'reponse_1412', 'reponse_1413', 'reponse_1414', 'reponse_1415', 'reponse_1416', 'reponse_1417'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_15
  {
    id:'question_15',
    category:CATEGORIES.PERSONAL,
    text:'question_15',
    inputType:'radio',
    comment:'',
    options:['reponse_150', 'reponse_151', 'reponse_152', 'reponse_153', 'reponse_154', 'reponse_155', 'reponse_156', 'reponse_157', 'reponse_158', 'reponse_159', 'reponse_1510', 'reponse_1511', 'reponse_1512', 'reponse_1513', 'reponse_1514', 'reponse_1515'],
    nextQuestionMap:['reponse_150', 'reponse_151', 'reponse_152', 'reponse_153', 'reponse_154', 'reponse_155', 'reponse_156', 'reponse_157', 'reponse_158', 'reponse_159', 'reponse_1510', 'reponse_1511', 'reponse_1512', 'reponse_1513', 'reponse_1514', 'reponse_1515'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_16
  {
    id:'question_16',
    category:CATEGORIES.PERSONAL,
    text:'question_16',
    inputType:'radio',
    comment:'',
    options:['reponse_160', 'reponse_161', 'reponse_162', 'reponse_163', 'reponse_164', 'reponse_165', 'reponse_166', 'reponse_167'],
    nextQuestionMap:['reponse_160', 'reponse_161', 'reponse_162', 'reponse_163', 'reponse_164', 'reponse_165', 'reponse_166', 'reponse_167'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_17
  {
    id:'question_17',
    category:CATEGORIES.PERSONAL,
    text:'question_17',
    inputType:'radio',
    comment:'',
    options:['reponse_170', 'reponse_171', 'reponse_172', 'reponse_173', 'reponse_174', 'reponse_175', 'reponse_176', 'reponse_177', 'reponse_178', 'reponse_179', 'reponse_1710', 'reponse_1711', 'reponse_1712', 'reponse_1713'],
    nextQuestionMap:['reponse_170', 'reponse_171', 'reponse_172', 'reponse_173', 'reponse_174', 'reponse_175', 'reponse_176', 'reponse_177', 'reponse_178', 'reponse_179', 'reponse_1710', 'reponse_1711', 'reponse_1712', 'reponse_1713'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_18
  {
    id:'question_18',
    category:CATEGORIES.PERSONAL,
    text:'question_18',
    inputType:'radio',
    comment:'',
    options:['reponse_180', 'reponse_181', 'reponse_182', 'reponse_183', 'reponse_184', 'reponse_185', 'reponse_186', 'reponse_187'],
    nextQuestionMap:['reponse_180', 'reponse_181', 'reponse_182', 'reponse_183', 'reponse_184', 'reponse_185', 'reponse_186', 'reponse_187'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_19
  {
    id:'question_19',
    category:CATEGORIES.PERSONAL,
    text:'question_19',
    inputType:'radio',
    comment:'',
    options:['reponse_190', 'reponse_191', 'reponse_192', 'reponse_193', 'reponse_194', 'reponse_195', 'reponse_196', 'reponse_197', 'reponse_198', 'reponse_199', 'reponse_1910', 'reponse_1911', 'reponse_1912'],
    nextQuestionMap:['reponse_190', 'reponse_191', 'reponse_192', 'reponse_193', 'reponse_194', 'reponse_195', 'reponse_196', 'reponse_197', 'reponse_198', 'reponse_199', 'reponse_1910', 'reponse_1911', 'reponse_1912'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_20
  {
    id:'question_20',
    category:CATEGORIES.PERSONAL,
    text:'question_20',
    inputType:'radio',
    comment:'',
    options:['reponse_200', 'reponse_201', 'reponse_202', 'reponse_203', 'reponse_204', 'reponse_205', 'reponse_206', 'reponse_207', 'reponse_208', 'reponse_209', 'reponse_2010', 'reponse_2011', 'reponse_2012', 'reponse_2013', 'reponse_2014', 'reponse_2015', 'reponse_2016', 'reponse_2017', 'reponse_2018', 'reponse_2019', 'reponse_2020'],
    nextQuestionMap:['reponse_200', 'reponse_201', 'reponse_202', 'reponse_203', 'reponse_204', 'reponse_205', 'reponse_206', 'reponse_207', 'reponse_208', 'reponse_209', 'reponse_2010', 'reponse_2011', 'reponse_2012', 'reponse_2013', 'reponse_2014', 'reponse_2015', 'reponse_2016', 'reponse_2017', 'reponse_2018', 'reponse_2019', 'reponse_2020'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_21
  {
    id:'question_21',
    category:CATEGORIES.PERSONAL,
    text:'question_21',
    inputType:'radio',
    comment:'',
    options:['reponse_210', 'reponse_211', 'reponse_212', 'reponse_213', 'reponse_214', 'reponse_215'],
    nextQuestionMap:['reponse_210', 'reponse_211', 'reponse_212', 'reponse_213', 'reponse_214', 'reponse_215'],
    scoreMap:[0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_22
  {
    id:'question_22',
    category:CATEGORIES.PERSONAL,
    text:'question_22',
    inputType:'radio',
    comment:'',
    options:['reponse_220', 'reponse_221', 'reponse_222', 'reponse_223', 'reponse_224', 'reponse_225', 'reponse_226', 'reponse_227', 'reponse_228', 'reponse_229', 'reponse_2210', 'reponse_2211', 'reponse_2212', 'reponse_2213'],
    nextQuestionMap:['reponse_220', 'reponse_221', 'reponse_222', 'reponse_223', 'reponse_224', 'reponse_225', 'reponse_226', 'reponse_227', 'reponse_228', 'reponse_229', 'reponse_2210', 'reponse_2211', 'reponse_2212', 'reponse_2213'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_23
  {
    id:'question_23',
    category:CATEGORIES.PERSONAL,
    text:'question_23',
    inputType:'radio',
    comment:'',
    options:['reponse_230', 'reponse_231', 'reponse_232', 'reponse_233', 'reponse_234', 'reponse_235', 'reponse_236', 'reponse_237', 'reponse_238', 'reponse_239', 'reponse_2310', 'reponse_2311'],
    nextQuestionMap:['reponse_230', 'reponse_231', 'reponse_232', 'reponse_233', 'reponse_234', 'reponse_235', 'reponse_236', 'reponse_237', 'reponse_238', 'reponse_239', 'reponse_2310', 'reponse_2311'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_24
  {
    id:'question_24',
    category:CATEGORIES.PERSONAL,
    text:'question_24',
    inputType:'radio',
    comment:'',
    options:['reponse_240', 'reponse_241', 'reponse_242', 'reponse_243', 'reponse_244', 'reponse_245', 'reponse_246', 'reponse_247', 'reponse_248'],
    nextQuestionMap:['reponse_240', 'reponse_241', 'reponse_242', 'reponse_243', 'reponse_244', 'reponse_245', 'reponse_246', 'reponse_247', 'reponse_248'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_25
  {
    id:'question_25',
    category:CATEGORIES.PERSONAL,
    text:'question_25',
    inputType:'radio',
    comment:'',
    options:['reponse_250', 'reponse_251', 'reponse_252', 'reponse_253', 'reponse_254', 'reponse_255'],
    nextQuestionMap:['reponse_250', 'reponse_251', 'reponse_252', 'reponse_253', 'reponse_254', 'reponse_255'],
    scoreMap:[0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_26
  {
    id:'question_26',
    category:CATEGORIES.PERSONAL,
    text:'question_26',
    inputType:'radio',
    comment:'',
    options:['reponse_260', 'reponse_261', 'reponse_262', 'reponse_263', 'reponse_264', 'reponse_265', 'reponse_266', 'reponse_267', 'reponse_268', 'reponse_269', 'reponse_2610', 'reponse_2611', 'reponse_2612', 'reponse_2613', 'reponse_2614', 'reponse_2615', 'reponse_2616', 'reponse_2617', 'reponse_2618'],
    nextQuestionMap:['reponse_260', 'reponse_261', 'reponse_262', 'reponse_263', 'reponse_264', 'reponse_265', 'reponse_266', 'reponse_267', 'reponse_268', 'reponse_269', 'reponse_2610', 'reponse_2611', 'reponse_2612', 'reponse_2613', 'reponse_2614', 'reponse_2615', 'reponse_2616', 'reponse_2617', 'reponse_2618'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_27
  {
    id:'question_27',
    category:CATEGORIES.PERSONAL,
    text:'question_27',
    inputType:'radio',
    comment:'',
    options:['reponse_270', 'reponse_271', 'reponse_272', 'reponse_273', 'reponse_274', 'reponse_275', 'reponse_276', 'reponse_277', 'reponse_278', 'reponse_279', 'reponse_2710', 'reponse_2711', 'reponse_2712'],
    nextQuestionMap:['reponse_270', 'reponse_271', 'reponse_272', 'reponse_273', 'reponse_274', 'reponse_275', 'reponse_276', 'reponse_277', 'reponse_278', 'reponse_279', 'reponse_2710', 'reponse_2711', 'reponse_2712'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_28
  {
    id:'question_28',
    category:CATEGORIES.PERSONAL,
    text:'question_28',
    inputType:'radio',
    comment:'',
    options:['reponse_280', 'reponse_281', 'reponse_282', 'reponse_283', 'reponse_284', 'reponse_285', 'reponse_286', 'reponse_287', 'reponse_288', 'reponse_289', 'reponse_2810'],
    nextQuestionMap:['reponse_280', 'reponse_281', 'reponse_282', 'reponse_283', 'reponse_284', 'reponse_285', 'reponse_286', 'reponse_287', 'reponse_288', 'reponse_289', 'reponse_2810'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_29
  {
    id:'question_29',
    category:CATEGORIES.PERSONAL,
    text:'question_29',
    inputType:'radio',
    comment:'',
    options:['reponse_290', 'reponse_291', 'reponse_292', 'reponse_293', 'reponse_294', 'reponse_295', 'reponse_296', 'reponse_297', 'reponse_298', 'reponse_299', 'reponse_2910', 'reponse_2911', 'reponse_2912', 'reponse_2913', 'reponse_2914'],
    nextQuestionMap:['reponse_290', 'reponse_291', 'reponse_292', 'reponse_293', 'reponse_294', 'reponse_295', 'reponse_296', 'reponse_297', 'reponse_298', 'reponse_299', 'reponse_2910', 'reponse_2911', 'reponse_2912', 'reponse_2913', 'reponse_2914'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_30
  {
    id:'question_30',
    category:CATEGORIES.PERSONAL,
    text:'question_30',
    inputType:'radio',
    comment:'',
    options:['reponse_300', 'reponse_301', 'reponse_302', 'reponse_303', 'reponse_304', 'reponse_305', 'reponse_306', 'reponse_307', 'reponse_308', 'reponse_309'],
    nextQuestionMap:['reponse_300', 'reponse_301', 'reponse_302', 'reponse_303', 'reponse_304', 'reponse_305', 'reponse_306', 'reponse_307', 'reponse_308', 'reponse_309'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_31
  {
    id:'question_31',
    category:CATEGORIES.PERSONAL,
    text:'question_31',
    inputType:'radio',
    comment:'',
    options:['reponse_310', 'reponse_311', 'reponse_312', 'reponse_313', 'reponse_314', 'reponse_315', 'reponse_316', 'reponse_317', 'reponse_318', 'reponse_319'],
    nextQuestionMap:['reponse_310', 'reponse_311', 'reponse_312', 'reponse_313', 'reponse_314', 'reponse_315', 'reponse_316', 'reponse_317', 'reponse_318', 'reponse_319'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_32
  {
    id:'question_32',
    category:CATEGORIES.PERSONAL,
    text:'question_32',
    inputType:'radio',
    comment:'',
    options:['reponse_320', 'reponse_321', 'reponse_322', 'reponse_323', 'reponse_324', 'reponse_325', 'reponse_326', 'reponse_327', 'reponse_328', 'reponse_329', 'reponse_3210', 'reponse_3211', 'reponse_3212', 'reponse_3213', 'reponse_3214', 'reponse_3215', 'reponse_3216', 'reponse_3217'],
    nextQuestionMap:['reponse_320', 'reponse_321', 'reponse_322', 'reponse_323', 'reponse_324', 'reponse_325', 'reponse_326', 'reponse_327', 'reponse_328', 'reponse_329', 'reponse_3210', 'reponse_3211', 'reponse_3212', 'reponse_3213', 'reponse_3214', 'reponse_3215', 'reponse_3216', 'reponse_3217'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  QUESTION REPONSE   ... question_33
  {
    id:'question_33',
    category:CATEGORIES.PERSONAL,
    text:'question_33',
    inputType:'radio',
    comment:'',
    options:['reponse_330', 'reponse_331', 'reponse_332', 'reponse_333', 'reponse_334', 'reponse_335', 'reponse_336'],
    nextQuestionMap:['reponse_330', 'reponse_331', 'reponse_332', 'reponse_333', 'reponse_334', 'reponse_335', 'reponse_336'],
    scoreMap:[0, 0, 0, 0, 0, 0, 0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_10
  {
    id:'reponse_10',
    category:CATEGORIES.PERSONAL,
    text:'reponse_10',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['10'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_11
  {
    id:'reponse_11',
    category:CATEGORIES.PERSONAL,
    text:'reponse_11',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['11'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_12
  {
    id:'reponse_12',
    category:CATEGORIES.PERSONAL,
    text:'reponse_12',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['12'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_13
  {
    id:'reponse_13',
    category:CATEGORIES.PERSONAL,
    text:'reponse_13',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['13'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_14
  {
    id:'reponse_14',
    category:CATEGORIES.PERSONAL,
    text:'reponse_14',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['14'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_15
  {
    id:'reponse_15',
    category:CATEGORIES.PERSONAL,
    text:'reponse_15',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['15'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_16
  {
    id:'reponse_16',
    category:CATEGORIES.PERSONAL,
    text:'reponse_16',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['16'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_17
  {
    id:'reponse_17',
    category:CATEGORIES.PERSONAL,
    text:'reponse_17',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['17'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_18
  {
    id:'reponse_18',
    category:CATEGORIES.PERSONAL,
    text:'reponse_18',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['18'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_19
  {
    id:'reponse_19',
    category:CATEGORIES.PERSONAL,
    text:'reponse_19',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['19'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_110
  {
    id:'reponse_110',
    category:CATEGORIES.PERSONAL,
    text:'reponse_110',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['110'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_111
  {
    id:'reponse_111',
    category:CATEGORIES.PERSONAL,
    text:'reponse_111',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['111'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_112
  {
    id:'reponse_112',
    category:CATEGORIES.PERSONAL,
    text:'reponse_112',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['112'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_113
  {
    id:'reponse_113',
    category:CATEGORIES.PERSONAL,
    text:'reponse_113',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['113'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_114
  {
    id:'reponse_114',
    category:CATEGORIES.PERSONAL,
    text:'reponse_114',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['114'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_115
  {
    id:'reponse_115',
    category:CATEGORIES.PERSONAL,
    text:'reponse_115',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['115'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_116
  {
    id:'reponse_116',
    category:CATEGORIES.PERSONAL,
    text:'reponse_116',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['116'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_117
  {
    id:'reponse_117',
    category:CATEGORIES.PERSONAL,
    text:'reponse_117',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['117'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_118
  {
    id:'reponse_118',
    category:CATEGORIES.PERSONAL,
    text:'reponse_118',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['118'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_20
  {
    id:'reponse_20',
    category:CATEGORIES.PERSONAL,
    text:'reponse_20',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['20'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_21
  {
    id:'reponse_21',
    category:CATEGORIES.PERSONAL,
    text:'reponse_21',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['21'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_22
  {
    id:'reponse_22',
    category:CATEGORIES.PERSONAL,
    text:'reponse_22',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['22'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_23
  {
    id:'reponse_23',
    category:CATEGORIES.PERSONAL,
    text:'reponse_23',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['23'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_24
  {
    id:'reponse_24',
    category:CATEGORIES.PERSONAL,
    text:'reponse_24',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['24'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_25
  {
    id:'reponse_25',
    category:CATEGORIES.PERSONAL,
    text:'reponse_25',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['25'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_26
  {
    id:'reponse_26',
    category:CATEGORIES.PERSONAL,
    text:'reponse_26',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['26'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_27
  {
    id:'reponse_27',
    category:CATEGORIES.PERSONAL,
    text:'reponse_27',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['27'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_30
  {
    id:'reponse_30',
    category:CATEGORIES.PERSONAL,
    text:'reponse_30',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['30'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_31
  {
    id:'reponse_31',
    category:CATEGORIES.PERSONAL,
    text:'reponse_31',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['31'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_32
  {
    id:'reponse_32',
    category:CATEGORIES.PERSONAL,
    text:'reponse_32',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['32'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_33
  {
    id:'reponse_33',
    category:CATEGORIES.PERSONAL,
    text:'reponse_33',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['33'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_34
  {
    id:'reponse_34',
    category:CATEGORIES.PERSONAL,
    text:'reponse_34',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['34'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_35
  {
    id:'reponse_35',
    category:CATEGORIES.PERSONAL,
    text:'reponse_35',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['35'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_36
  {
    id:'reponse_36',
    category:CATEGORIES.PERSONAL,
    text:'reponse_36',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['36'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_37
  {
    id:'reponse_37',
    category:CATEGORIES.PERSONAL,
    text:'reponse_37',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['37'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_38
  {
    id:'reponse_38',
    category:CATEGORIES.PERSONAL,
    text:'reponse_38',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['38'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_39
  {
    id:'reponse_39',
    category:CATEGORIES.PERSONAL,
    text:'reponse_39',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['39'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_310
  {
    id:'reponse_310',
    category:CATEGORIES.PERSONAL,
    text:'reponse_310',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['310'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_311
  {
    id:'reponse_311',
    category:CATEGORIES.PERSONAL,
    text:'reponse_311',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['311'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_312
  {
    id:'reponse_312',
    category:CATEGORIES.PERSONAL,
    text:'reponse_312',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['312'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_40
  {
    id:'reponse_40',
    category:CATEGORIES.PERSONAL,
    text:'reponse_40',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['40'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_41
  {
    id:'reponse_41',
    category:CATEGORIES.PERSONAL,
    text:'reponse_41',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['41'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_42
  {
    id:'reponse_42',
    category:CATEGORIES.PERSONAL,
    text:'reponse_42',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['42'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_43
  {
    id:'reponse_43',
    category:CATEGORIES.PERSONAL,
    text:'reponse_43',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['43'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_44
  {
    id:'reponse_44',
    category:CATEGORIES.PERSONAL,
    text:'reponse_44',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['44'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_45
  {
    id:'reponse_45',
    category:CATEGORIES.PERSONAL,
    text:'reponse_45',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['45'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_46
  {
    id:'reponse_46',
    category:CATEGORIES.PERSONAL,
    text:'reponse_46',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['46'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_50
  {
    id:'reponse_50',
    category:CATEGORIES.PERSONAL,
    text:'reponse_50',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['50'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_51
  {
    id:'reponse_51',
    category:CATEGORIES.PERSONAL,
    text:'reponse_51',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['51'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_52
  {
    id:'reponse_52',
    category:CATEGORIES.PERSONAL,
    text:'reponse_52',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['52'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_53
  {
    id:'reponse_53',
    category:CATEGORIES.PERSONAL,
    text:'reponse_53',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['53'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_54
  {
    id:'reponse_54',
    category:CATEGORIES.PERSONAL,
    text:'reponse_54',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['54'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_60
  {
    id:'reponse_60',
    category:CATEGORIES.PERSONAL,
    text:'reponse_60',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['60'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_61
  {
    id:'reponse_61',
    category:CATEGORIES.PERSONAL,
    text:'reponse_61',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['61'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_62
  {
    id:'reponse_62',
    category:CATEGORIES.PERSONAL,
    text:'reponse_62',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['62'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_63
  {
    id:'reponse_63',
    category:CATEGORIES.PERSONAL,
    text:'reponse_63',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['63'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_64
  {
    id:'reponse_64',
    category:CATEGORIES.PERSONAL,
    text:'reponse_64',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['64'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_65
  {
    id:'reponse_65',
    category:CATEGORIES.PERSONAL,
    text:'reponse_65',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['65'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_66
  {
    id:'reponse_66',
    category:CATEGORIES.PERSONAL,
    text:'reponse_66',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['66'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_67
  {
    id:'reponse_67',
    category:CATEGORIES.PERSONAL,
    text:'reponse_67',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['67'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_68
  {
    id:'reponse_68',
    category:CATEGORIES.PERSONAL,
    text:'reponse_68',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['68'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_69
  {
    id:'reponse_69',
    category:CATEGORIES.PERSONAL,
    text:'reponse_69',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['69'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_610
  {
    id:'reponse_610',
    category:CATEGORIES.PERSONAL,
    text:'reponse_610',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['610'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_611
  {
    id:'reponse_611',
    category:CATEGORIES.PERSONAL,
    text:'reponse_611',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['611'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_612
  {
    id:'reponse_612',
    category:CATEGORIES.PERSONAL,
    text:'reponse_612',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['612'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_613
  {
    id:'reponse_613',
    category:CATEGORIES.PERSONAL,
    text:'reponse_613',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['613'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_614
  {
    id:'reponse_614',
    category:CATEGORIES.PERSONAL,
    text:'reponse_614',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['614'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_70
  {
    id:'reponse_70',
    category:CATEGORIES.PERSONAL,
    text:'reponse_70',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['70'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_71
  {
    id:'reponse_71',
    category:CATEGORIES.PERSONAL,
    text:'reponse_71',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['71'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_72
  {
    id:'reponse_72',
    category:CATEGORIES.PERSONAL,
    text:'reponse_72',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['72'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_73
  {
    id:'reponse_73',
    category:CATEGORIES.PERSONAL,
    text:'reponse_73',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['73'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_74
  {
    id:'reponse_74',
    category:CATEGORIES.PERSONAL,
    text:'reponse_74',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['74'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_75
  {
    id:'reponse_75',
    category:CATEGORIES.PERSONAL,
    text:'reponse_75',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['75'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_76
  {
    id:'reponse_76',
    category:CATEGORIES.PERSONAL,
    text:'reponse_76',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['76'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_77
  {
    id:'reponse_77',
    category:CATEGORIES.PERSONAL,
    text:'reponse_77',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['77'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_78
  {
    id:'reponse_78',
    category:CATEGORIES.PERSONAL,
    text:'reponse_78',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['78'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_79
  {
    id:'reponse_79',
    category:CATEGORIES.PERSONAL,
    text:'reponse_79',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['79'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_80
  {
    id:'reponse_80',
    category:CATEGORIES.PERSONAL,
    text:'reponse_80',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['80'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_81
  {
    id:'reponse_81',
    category:CATEGORIES.PERSONAL,
    text:'reponse_81',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['81'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_82
  {
    id:'reponse_82',
    category:CATEGORIES.PERSONAL,
    text:'reponse_82',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['82'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_83
  {
    id:'reponse_83',
    category:CATEGORIES.PERSONAL,
    text:'reponse_83',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['83'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_84
  {
    id:'reponse_84',
    category:CATEGORIES.PERSONAL,
    text:'reponse_84',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['84'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_85
  {
    id:'reponse_85',
    category:CATEGORIES.PERSONAL,
    text:'reponse_85',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['85'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_86
  {
    id:'reponse_86',
    category:CATEGORIES.PERSONAL,
    text:'reponse_86',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['86'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_87
  {
    id:'reponse_87',
    category:CATEGORIES.PERSONAL,
    text:'reponse_87',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['87'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_88
  {
    id:'reponse_88',
    category:CATEGORIES.PERSONAL,
    text:'reponse_88',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['88'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_89
  {
    id:'reponse_89',
    category:CATEGORIES.PERSONAL,
    text:'reponse_89',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['89'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_810
  {
    id:'reponse_810',
    category:CATEGORIES.PERSONAL,
    text:'reponse_810',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['810'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_811
  {
    id:'reponse_811',
    category:CATEGORIES.PERSONAL,
    text:'reponse_811',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['811'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_90
  {
    id:'reponse_90',
    category:CATEGORIES.PERSONAL,
    text:'reponse_90',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['90'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_91
  {
    id:'reponse_91',
    category:CATEGORIES.PERSONAL,
    text:'reponse_91',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['91'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_92
  {
    id:'reponse_92',
    category:CATEGORIES.PERSONAL,
    text:'reponse_92',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['92'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_93
  {
    id:'reponse_93',
    category:CATEGORIES.PERSONAL,
    text:'reponse_93',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['93'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_94
  {
    id:'reponse_94',
    category:CATEGORIES.PERSONAL,
    text:'reponse_94',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['94'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_95
  {
    id:'reponse_95',
    category:CATEGORIES.PERSONAL,
    text:'reponse_95',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['95'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_96
  {
    id:'reponse_96',
    category:CATEGORIES.PERSONAL,
    text:'reponse_96',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['96'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_97
  {
    id:'reponse_97',
    category:CATEGORIES.PERSONAL,
    text:'reponse_97',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['97'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_98
  {
    id:'reponse_98',
    category:CATEGORIES.PERSONAL,
    text:'reponse_98',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['98'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_99
  {
    id:'reponse_99',
    category:CATEGORIES.PERSONAL,
    text:'reponse_99',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['99'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_910
  {
    id:'reponse_910',
    category:CATEGORIES.PERSONAL,
    text:'reponse_910',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['910'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_100
  {
    id:'reponse_100',
    category:CATEGORIES.PERSONAL,
    text:'reponse_100',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['100'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_101
  {
    id:'reponse_101',
    category:CATEGORIES.PERSONAL,
    text:'reponse_101',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['101'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_102
  {
    id:'reponse_102',
    category:CATEGORIES.PERSONAL,
    text:'reponse_102',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['102'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_103
  {
    id:'reponse_103',
    category:CATEGORIES.PERSONAL,
    text:'reponse_103',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['103'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_104
  {
    id:'reponse_104',
    category:CATEGORIES.PERSONAL,
    text:'reponse_104',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['104'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_105
  {
    id:'reponse_105',
    category:CATEGORIES.PERSONAL,
    text:'reponse_105',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['105'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_106
  {
    id:'reponse_106',
    category:CATEGORIES.PERSONAL,
    text:'reponse_106',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['106'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_107
  {
    id:'reponse_107',
    category:CATEGORIES.PERSONAL,
    text:'reponse_107',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['107'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_108
  {
    id:'reponse_108',
    category:CATEGORIES.PERSONAL,
    text:'reponse_108',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['108'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_109
  {
    id:'reponse_109',
    category:CATEGORIES.PERSONAL,
    text:'reponse_109',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['109'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1010
  {
    id:'reponse_1010',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1010',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1010'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1011
  {
    id:'reponse_1011',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1011',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1011'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1012
  {
    id:'reponse_1012',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1012',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1012'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1013
  {
    id:'reponse_1013',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1013',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1013'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1014
  {
    id:'reponse_1014',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1014',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1014'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1015
  {
    id:'reponse_1015',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1015',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1015'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1016
  {
    id:'reponse_1016',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1016',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1016'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1017
  {
    id:'reponse_1017',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1017',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1017'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_110
  {
    id:'reponse_110',
    category:CATEGORIES.PERSONAL,
    text:'reponse_110',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['110'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_111
  {
    id:'reponse_111',
    category:CATEGORIES.PERSONAL,
    text:'reponse_111',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['111'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_112
  {
    id:'reponse_112',
    category:CATEGORIES.PERSONAL,
    text:'reponse_112',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['112'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_113
  {
    id:'reponse_113',
    category:CATEGORIES.PERSONAL,
    text:'reponse_113',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['113'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_114
  {
    id:'reponse_114',
    category:CATEGORIES.PERSONAL,
    text:'reponse_114',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['114'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_115
  {
    id:'reponse_115',
    category:CATEGORIES.PERSONAL,
    text:'reponse_115',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['115'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_116
  {
    id:'reponse_116',
    category:CATEGORIES.PERSONAL,
    text:'reponse_116',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['116'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_117
  {
    id:'reponse_117',
    category:CATEGORIES.PERSONAL,
    text:'reponse_117',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['117'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_118
  {
    id:'reponse_118',
    category:CATEGORIES.PERSONAL,
    text:'reponse_118',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['118'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_119
  {
    id:'reponse_119',
    category:CATEGORIES.PERSONAL,
    text:'reponse_119',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['119'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1110
  {
    id:'reponse_1110',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1110',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1110'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1111
  {
    id:'reponse_1111',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1111',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1111'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1112
  {
    id:'reponse_1112',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1112',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1112'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1113
  {
    id:'reponse_1113',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1113',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1113'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1114
  {
    id:'reponse_1114',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1114',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1114'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1115
  {
    id:'reponse_1115',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1115',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1115'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1116
  {
    id:'reponse_1116',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1116',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1116'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_120
  {
    id:'reponse_120',
    category:CATEGORIES.PERSONAL,
    text:'reponse_120',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['120'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_121
  {
    id:'reponse_121',
    category:CATEGORIES.PERSONAL,
    text:'reponse_121',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['121'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_122
  {
    id:'reponse_122',
    category:CATEGORIES.PERSONAL,
    text:'reponse_122',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['122'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_123
  {
    id:'reponse_123',
    category:CATEGORIES.PERSONAL,
    text:'reponse_123',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['123'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_124
  {
    id:'reponse_124',
    category:CATEGORIES.PERSONAL,
    text:'reponse_124',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['124'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_125
  {
    id:'reponse_125',
    category:CATEGORIES.PERSONAL,
    text:'reponse_125',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['125'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_126
  {
    id:'reponse_126',
    category:CATEGORIES.PERSONAL,
    text:'reponse_126',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['126'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_127
  {
    id:'reponse_127',
    category:CATEGORIES.PERSONAL,
    text:'reponse_127',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['127'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_128
  {
    id:'reponse_128',
    category:CATEGORIES.PERSONAL,
    text:'reponse_128',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['128'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_129
  {
    id:'reponse_129',
    category:CATEGORIES.PERSONAL,
    text:'reponse_129',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['129'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_130
  {
    id:'reponse_130',
    category:CATEGORIES.PERSONAL,
    text:'reponse_130',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['130'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_131
  {
    id:'reponse_131',
    category:CATEGORIES.PERSONAL,
    text:'reponse_131',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['131'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_132
  {
    id:'reponse_132',
    category:CATEGORIES.PERSONAL,
    text:'reponse_132',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['132'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_133
  {
    id:'reponse_133',
    category:CATEGORIES.PERSONAL,
    text:'reponse_133',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['133'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_134
  {
    id:'reponse_134',
    category:CATEGORIES.PERSONAL,
    text:'reponse_134',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['134'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_135
  {
    id:'reponse_135',
    category:CATEGORIES.PERSONAL,
    text:'reponse_135',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['135'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_136
  {
    id:'reponse_136',
    category:CATEGORIES.PERSONAL,
    text:'reponse_136',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['136'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_137
  {
    id:'reponse_137',
    category:CATEGORIES.PERSONAL,
    text:'reponse_137',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['137'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_138
  {
    id:'reponse_138',
    category:CATEGORIES.PERSONAL,
    text:'reponse_138',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['138'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_139
  {
    id:'reponse_139',
    category:CATEGORIES.PERSONAL,
    text:'reponse_139',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['139'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1310
  {
    id:'reponse_1310',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1310',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1310'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1311
  {
    id:'reponse_1311',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1311',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1311'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1312
  {
    id:'reponse_1312',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1312',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1312'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_140
  {
    id:'reponse_140',
    category:CATEGORIES.PERSONAL,
    text:'reponse_140',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['140'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_141
  {
    id:'reponse_141',
    category:CATEGORIES.PERSONAL,
    text:'reponse_141',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['141'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_142
  {
    id:'reponse_142',
    category:CATEGORIES.PERSONAL,
    text:'reponse_142',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['142'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_143
  {
    id:'reponse_143',
    category:CATEGORIES.PERSONAL,
    text:'reponse_143',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['143'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_144
  {
    id:'reponse_144',
    category:CATEGORIES.PERSONAL,
    text:'reponse_144',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['144'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_145
  {
    id:'reponse_145',
    category:CATEGORIES.PERSONAL,
    text:'reponse_145',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['145'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_146
  {
    id:'reponse_146',
    category:CATEGORIES.PERSONAL,
    text:'reponse_146',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['146'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_147
  {
    id:'reponse_147',
    category:CATEGORIES.PERSONAL,
    text:'reponse_147',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['147'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_148
  {
    id:'reponse_148',
    category:CATEGORIES.PERSONAL,
    text:'reponse_148',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['148'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_149
  {
    id:'reponse_149',
    category:CATEGORIES.PERSONAL,
    text:'reponse_149',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['149'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1410
  {
    id:'reponse_1410',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1410',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1410'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1411
  {
    id:'reponse_1411',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1411',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1411'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1412
  {
    id:'reponse_1412',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1412',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1412'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1413
  {
    id:'reponse_1413',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1413',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1413'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1414
  {
    id:'reponse_1414',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1414',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1414'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1415
  {
    id:'reponse_1415',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1415',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1415'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1416
  {
    id:'reponse_1416',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1416',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1416'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1417
  {
    id:'reponse_1417',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1417',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1417'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_150
  {
    id:'reponse_150',
    category:CATEGORIES.PERSONAL,
    text:'reponse_150',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['150'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_151
  {
    id:'reponse_151',
    category:CATEGORIES.PERSONAL,
    text:'reponse_151',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['151'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_152
  {
    id:'reponse_152',
    category:CATEGORIES.PERSONAL,
    text:'reponse_152',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['152'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_153
  {
    id:'reponse_153',
    category:CATEGORIES.PERSONAL,
    text:'reponse_153',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['153'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_154
  {
    id:'reponse_154',
    category:CATEGORIES.PERSONAL,
    text:'reponse_154',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['154'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_155
  {
    id:'reponse_155',
    category:CATEGORIES.PERSONAL,
    text:'reponse_155',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['155'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_156
  {
    id:'reponse_156',
    category:CATEGORIES.PERSONAL,
    text:'reponse_156',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['156'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_157
  {
    id:'reponse_157',
    category:CATEGORIES.PERSONAL,
    text:'reponse_157',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['157'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_158
  {
    id:'reponse_158',
    category:CATEGORIES.PERSONAL,
    text:'reponse_158',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['158'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_159
  {
    id:'reponse_159',
    category:CATEGORIES.PERSONAL,
    text:'reponse_159',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['159'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1510
  {
    id:'reponse_1510',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1510',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1510'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1511
  {
    id:'reponse_1511',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1511',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1511'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1512
  {
    id:'reponse_1512',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1512',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1512'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1513
  {
    id:'reponse_1513',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1513',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1513'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1514
  {
    id:'reponse_1514',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1514',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1514'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1515
  {
    id:'reponse_1515',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1515',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1515'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_160
  {
    id:'reponse_160',
    category:CATEGORIES.PERSONAL,
    text:'reponse_160',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['160'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_161
  {
    id:'reponse_161',
    category:CATEGORIES.PERSONAL,
    text:'reponse_161',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['161'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_162
  {
    id:'reponse_162',
    category:CATEGORIES.PERSONAL,
    text:'reponse_162',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['162'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_163
  {
    id:'reponse_163',
    category:CATEGORIES.PERSONAL,
    text:'reponse_163',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['163'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_164
  {
    id:'reponse_164',
    category:CATEGORIES.PERSONAL,
    text:'reponse_164',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['164'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_165
  {
    id:'reponse_165',
    category:CATEGORIES.PERSONAL,
    text:'reponse_165',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['165'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_166
  {
    id:'reponse_166',
    category:CATEGORIES.PERSONAL,
    text:'reponse_166',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['166'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_167
  {
    id:'reponse_167',
    category:CATEGORIES.PERSONAL,
    text:'reponse_167',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['167'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_170
  {
    id:'reponse_170',
    category:CATEGORIES.PERSONAL,
    text:'reponse_170',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['170'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_171
  {
    id:'reponse_171',
    category:CATEGORIES.PERSONAL,
    text:'reponse_171',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['171'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_172
  {
    id:'reponse_172',
    category:CATEGORIES.PERSONAL,
    text:'reponse_172',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['172'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_173
  {
    id:'reponse_173',
    category:CATEGORIES.PERSONAL,
    text:'reponse_173',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['173'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_174
  {
    id:'reponse_174',
    category:CATEGORIES.PERSONAL,
    text:'reponse_174',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['174'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_175
  {
    id:'reponse_175',
    category:CATEGORIES.PERSONAL,
    text:'reponse_175',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['175'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_176
  {
    id:'reponse_176',
    category:CATEGORIES.PERSONAL,
    text:'reponse_176',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['176'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_177
  {
    id:'reponse_177',
    category:CATEGORIES.PERSONAL,
    text:'reponse_177',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['177'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_178
  {
    id:'reponse_178',
    category:CATEGORIES.PERSONAL,
    text:'reponse_178',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['178'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_179
  {
    id:'reponse_179',
    category:CATEGORIES.PERSONAL,
    text:'reponse_179',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['179'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1710
  {
    id:'reponse_1710',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1710',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1710'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1711
  {
    id:'reponse_1711',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1711',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1711'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1712
  {
    id:'reponse_1712',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1712',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1712'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1713
  {
    id:'reponse_1713',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1713',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1713'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_180
  {
    id:'reponse_180',
    category:CATEGORIES.PERSONAL,
    text:'reponse_180',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['180'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_181
  {
    id:'reponse_181',
    category:CATEGORIES.PERSONAL,
    text:'reponse_181',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['181'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_182
  {
    id:'reponse_182',
    category:CATEGORIES.PERSONAL,
    text:'reponse_182',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['182'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_183
  {
    id:'reponse_183',
    category:CATEGORIES.PERSONAL,
    text:'reponse_183',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['183'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_184
  {
    id:'reponse_184',
    category:CATEGORIES.PERSONAL,
    text:'reponse_184',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['184'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_185
  {
    id:'reponse_185',
    category:CATEGORIES.PERSONAL,
    text:'reponse_185',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['185'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_186
  {
    id:'reponse_186',
    category:CATEGORIES.PERSONAL,
    text:'reponse_186',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['186'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_187
  {
    id:'reponse_187',
    category:CATEGORIES.PERSONAL,
    text:'reponse_187',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['187'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_190
  {
    id:'reponse_190',
    category:CATEGORIES.PERSONAL,
    text:'reponse_190',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['190'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_191
  {
    id:'reponse_191',
    category:CATEGORIES.PERSONAL,
    text:'reponse_191',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['191'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_192
  {
    id:'reponse_192',
    category:CATEGORIES.PERSONAL,
    text:'reponse_192',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['192'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_193
  {
    id:'reponse_193',
    category:CATEGORIES.PERSONAL,
    text:'reponse_193',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['193'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_194
  {
    id:'reponse_194',
    category:CATEGORIES.PERSONAL,
    text:'reponse_194',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['194'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_195
  {
    id:'reponse_195',
    category:CATEGORIES.PERSONAL,
    text:'reponse_195',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['195'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_196
  {
    id:'reponse_196',
    category:CATEGORIES.PERSONAL,
    text:'reponse_196',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['196'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_197
  {
    id:'reponse_197',
    category:CATEGORIES.PERSONAL,
    text:'reponse_197',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['197'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_198
  {
    id:'reponse_198',
    category:CATEGORIES.PERSONAL,
    text:'reponse_198',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['198'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_199
  {
    id:'reponse_199',
    category:CATEGORIES.PERSONAL,
    text:'reponse_199',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['199'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1910
  {
    id:'reponse_1910',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1910',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1910'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1911
  {
    id:'reponse_1911',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1911',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1911'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_1912
  {
    id:'reponse_1912',
    category:CATEGORIES.PERSONAL,
    text:'reponse_1912',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['1912'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_200
  {
    id:'reponse_200',
    category:CATEGORIES.PERSONAL,
    text:'reponse_200',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['200'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_201
  {
    id:'reponse_201',
    category:CATEGORIES.PERSONAL,
    text:'reponse_201',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['201'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_202
  {
    id:'reponse_202',
    category:CATEGORIES.PERSONAL,
    text:'reponse_202',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['202'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_203
  {
    id:'reponse_203',
    category:CATEGORIES.PERSONAL,
    text:'reponse_203',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['203'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_204
  {
    id:'reponse_204',
    category:CATEGORIES.PERSONAL,
    text:'reponse_204',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['204'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_205
  {
    id:'reponse_205',
    category:CATEGORIES.PERSONAL,
    text:'reponse_205',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['205'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_206
  {
    id:'reponse_206',
    category:CATEGORIES.PERSONAL,
    text:'reponse_206',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['206'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_207
  {
    id:'reponse_207',
    category:CATEGORIES.PERSONAL,
    text:'reponse_207',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['207'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_208
  {
    id:'reponse_208',
    category:CATEGORIES.PERSONAL,
    text:'reponse_208',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['208'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_209
  {
    id:'reponse_209',
    category:CATEGORIES.PERSONAL,
    text:'reponse_209',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['209'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2010
  {
    id:'reponse_2010',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2010',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2010'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2011
  {
    id:'reponse_2011',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2011',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2011'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2012
  {
    id:'reponse_2012',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2012',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2012'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2013
  {
    id:'reponse_2013',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2013',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2013'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2014
  {
    id:'reponse_2014',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2014',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2014'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2015
  {
    id:'reponse_2015',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2015',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2015'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2016
  {
    id:'reponse_2016',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2016',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2016'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2017
  {
    id:'reponse_2017',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2017',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2017'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2018
  {
    id:'reponse_2018',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2018',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2018'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2019
  {
    id:'reponse_2019',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2019',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2019'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2020
  {
    id:'reponse_2020',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2020',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2020'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_210
  {
    id:'reponse_210',
    category:CATEGORIES.PERSONAL,
    text:'reponse_210',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['210'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_211
  {
    id:'reponse_211',
    category:CATEGORIES.PERSONAL,
    text:'reponse_211',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['211'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_212
  {
    id:'reponse_212',
    category:CATEGORIES.PERSONAL,
    text:'reponse_212',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['212'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_213
  {
    id:'reponse_213',
    category:CATEGORIES.PERSONAL,
    text:'reponse_213',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['213'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_214
  {
    id:'reponse_214',
    category:CATEGORIES.PERSONAL,
    text:'reponse_214',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['214'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_215
  {
    id:'reponse_215',
    category:CATEGORIES.PERSONAL,
    text:'reponse_215',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['215'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_220
  {
    id:'reponse_220',
    category:CATEGORIES.PERSONAL,
    text:'reponse_220',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['220'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_221
  {
    id:'reponse_221',
    category:CATEGORIES.PERSONAL,
    text:'reponse_221',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['221'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_222
  {
    id:'reponse_222',
    category:CATEGORIES.PERSONAL,
    text:'reponse_222',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['222'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_223
  {
    id:'reponse_223',
    category:CATEGORIES.PERSONAL,
    text:'reponse_223',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['223'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_224
  {
    id:'reponse_224',
    category:CATEGORIES.PERSONAL,
    text:'reponse_224',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['224'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_225
  {
    id:'reponse_225',
    category:CATEGORIES.PERSONAL,
    text:'reponse_225',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['225'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_226
  {
    id:'reponse_226',
    category:CATEGORIES.PERSONAL,
    text:'reponse_226',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['226'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_227
  {
    id:'reponse_227',
    category:CATEGORIES.PERSONAL,
    text:'reponse_227',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['227'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_228
  {
    id:'reponse_228',
    category:CATEGORIES.PERSONAL,
    text:'reponse_228',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['228'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_229
  {
    id:'reponse_229',
    category:CATEGORIES.PERSONAL,
    text:'reponse_229',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['229'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2210
  {
    id:'reponse_2210',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2210',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2210'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2211
  {
    id:'reponse_2211',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2211',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2211'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2212
  {
    id:'reponse_2212',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2212',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2212'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2213
  {
    id:'reponse_2213',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2213',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2213'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_230
  {
    id:'reponse_230',
    category:CATEGORIES.PERSONAL,
    text:'reponse_230',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['230'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_231
  {
    id:'reponse_231',
    category:CATEGORIES.PERSONAL,
    text:'reponse_231',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['231'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_232
  {
    id:'reponse_232',
    category:CATEGORIES.PERSONAL,
    text:'reponse_232',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['232'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_233
  {
    id:'reponse_233',
    category:CATEGORIES.PERSONAL,
    text:'reponse_233',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['233'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_234
  {
    id:'reponse_234',
    category:CATEGORIES.PERSONAL,
    text:'reponse_234',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['234'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_235
  {
    id:'reponse_235',
    category:CATEGORIES.PERSONAL,
    text:'reponse_235',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['235'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_236
  {
    id:'reponse_236',
    category:CATEGORIES.PERSONAL,
    text:'reponse_236',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['236'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_237
  {
    id:'reponse_237',
    category:CATEGORIES.PERSONAL,
    text:'reponse_237',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['237'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_238
  {
    id:'reponse_238',
    category:CATEGORIES.PERSONAL,
    text:'reponse_238',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['238'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_239
  {
    id:'reponse_239',
    category:CATEGORIES.PERSONAL,
    text:'reponse_239',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['239'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2310
  {
    id:'reponse_2310',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2310',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2310'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2311
  {
    id:'reponse_2311',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2311',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2311'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_240
  {
    id:'reponse_240',
    category:CATEGORIES.PERSONAL,
    text:'reponse_240',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['240'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_241
  {
    id:'reponse_241',
    category:CATEGORIES.PERSONAL,
    text:'reponse_241',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['241'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_242
  {
    id:'reponse_242',
    category:CATEGORIES.PERSONAL,
    text:'reponse_242',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['242'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_243
  {
    id:'reponse_243',
    category:CATEGORIES.PERSONAL,
    text:'reponse_243',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['243'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_244
  {
    id:'reponse_244',
    category:CATEGORIES.PERSONAL,
    text:'reponse_244',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['244'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_245
  {
    id:'reponse_245',
    category:CATEGORIES.PERSONAL,
    text:'reponse_245',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['245'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_246
  {
    id:'reponse_246',
    category:CATEGORIES.PERSONAL,
    text:'reponse_246',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['246'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_247
  {
    id:'reponse_247',
    category:CATEGORIES.PERSONAL,
    text:'reponse_247',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['247'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_248
  {
    id:'reponse_248',
    category:CATEGORIES.PERSONAL,
    text:'reponse_248',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['248'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_250
  {
    id:'reponse_250',
    category:CATEGORIES.PERSONAL,
    text:'reponse_250',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['250'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_251
  {
    id:'reponse_251',
    category:CATEGORIES.PERSONAL,
    text:'reponse_251',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['251'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_252
  {
    id:'reponse_252',
    category:CATEGORIES.PERSONAL,
    text:'reponse_252',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['252'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_253
  {
    id:'reponse_253',
    category:CATEGORIES.PERSONAL,
    text:'reponse_253',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['253'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_254
  {
    id:'reponse_254',
    category:CATEGORIES.PERSONAL,
    text:'reponse_254',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['254'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_255
  {
    id:'reponse_255',
    category:CATEGORIES.PERSONAL,
    text:'reponse_255',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['255'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_260
  {
    id:'reponse_260',
    category:CATEGORIES.PERSONAL,
    text:'reponse_260',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['260'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_261
  {
    id:'reponse_261',
    category:CATEGORIES.PERSONAL,
    text:'reponse_261',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['261'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_262
  {
    id:'reponse_262',
    category:CATEGORIES.PERSONAL,
    text:'reponse_262',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['262'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_263
  {
    id:'reponse_263',
    category:CATEGORIES.PERSONAL,
    text:'reponse_263',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['263'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_264
  {
    id:'reponse_264',
    category:CATEGORIES.PERSONAL,
    text:'reponse_264',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['264'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_265
  {
    id:'reponse_265',
    category:CATEGORIES.PERSONAL,
    text:'reponse_265',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['265'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_266
  {
    id:'reponse_266',
    category:CATEGORIES.PERSONAL,
    text:'reponse_266',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['266'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_267
  {
    id:'reponse_267',
    category:CATEGORIES.PERSONAL,
    text:'reponse_267',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['267'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_268
  {
    id:'reponse_268',
    category:CATEGORIES.PERSONAL,
    text:'reponse_268',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['268'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_269
  {
    id:'reponse_269',
    category:CATEGORIES.PERSONAL,
    text:'reponse_269',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['269'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2610
  {
    id:'reponse_2610',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2610',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2610'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2611
  {
    id:'reponse_2611',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2611',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2611'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2612
  {
    id:'reponse_2612',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2612',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2612'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2613
  {
    id:'reponse_2613',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2613',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2613'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2614
  {
    id:'reponse_2614',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2614',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2614'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2615
  {
    id:'reponse_2615',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2615',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2615'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2616
  {
    id:'reponse_2616',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2616',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2616'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2617
  {
    id:'reponse_2617',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2617',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2617'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2618
  {
    id:'reponse_2618',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2618',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2618'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_270
  {
    id:'reponse_270',
    category:CATEGORIES.PERSONAL,
    text:'reponse_270',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['270'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_271
  {
    id:'reponse_271',
    category:CATEGORIES.PERSONAL,
    text:'reponse_271',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['271'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_272
  {
    id:'reponse_272',
    category:CATEGORIES.PERSONAL,
    text:'reponse_272',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['272'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_273
  {
    id:'reponse_273',
    category:CATEGORIES.PERSONAL,
    text:'reponse_273',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['273'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_274
  {
    id:'reponse_274',
    category:CATEGORIES.PERSONAL,
    text:'reponse_274',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['274'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_275
  {
    id:'reponse_275',
    category:CATEGORIES.PERSONAL,
    text:'reponse_275',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['275'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_276
  {
    id:'reponse_276',
    category:CATEGORIES.PERSONAL,
    text:'reponse_276',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['276'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_277
  {
    id:'reponse_277',
    category:CATEGORIES.PERSONAL,
    text:'reponse_277',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['277'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_278
  {
    id:'reponse_278',
    category:CATEGORIES.PERSONAL,
    text:'reponse_278',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['278'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_279
  {
    id:'reponse_279',
    category:CATEGORIES.PERSONAL,
    text:'reponse_279',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['279'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2710
  {
    id:'reponse_2710',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2710',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2710'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2711
  {
    id:'reponse_2711',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2711',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2711'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2712
  {
    id:'reponse_2712',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2712',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2712'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_280
  {
    id:'reponse_280',
    category:CATEGORIES.PERSONAL,
    text:'reponse_280',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['280'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_281
  {
    id:'reponse_281',
    category:CATEGORIES.PERSONAL,
    text:'reponse_281',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['281'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_282
  {
    id:'reponse_282',
    category:CATEGORIES.PERSONAL,
    text:'reponse_282',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['282'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_283
  {
    id:'reponse_283',
    category:CATEGORIES.PERSONAL,
    text:'reponse_283',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['283'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_284
  {
    id:'reponse_284',
    category:CATEGORIES.PERSONAL,
    text:'reponse_284',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['284'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_285
  {
    id:'reponse_285',
    category:CATEGORIES.PERSONAL,
    text:'reponse_285',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['285'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_286
  {
    id:'reponse_286',
    category:CATEGORIES.PERSONAL,
    text:'reponse_286',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['286'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_287
  {
    id:'reponse_287',
    category:CATEGORIES.PERSONAL,
    text:'reponse_287',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['287'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_288
  {
    id:'reponse_288',
    category:CATEGORIES.PERSONAL,
    text:'reponse_288',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['288'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_289
  {
    id:'reponse_289',
    category:CATEGORIES.PERSONAL,
    text:'reponse_289',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['289'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2810
  {
    id:'reponse_2810',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2810',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2810'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_290
  {
    id:'reponse_290',
    category:CATEGORIES.PERSONAL,
    text:'reponse_290',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['290'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_291
  {
    id:'reponse_291',
    category:CATEGORIES.PERSONAL,
    text:'reponse_291',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['291'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_292
  {
    id:'reponse_292',
    category:CATEGORIES.PERSONAL,
    text:'reponse_292',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['292'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_293
  {
    id:'reponse_293',
    category:CATEGORIES.PERSONAL,
    text:'reponse_293',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['293'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_294
  {
    id:'reponse_294',
    category:CATEGORIES.PERSONAL,
    text:'reponse_294',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['294'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_295
  {
    id:'reponse_295',
    category:CATEGORIES.PERSONAL,
    text:'reponse_295',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['295'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_296
  {
    id:'reponse_296',
    category:CATEGORIES.PERSONAL,
    text:'reponse_296',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['296'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_297
  {
    id:'reponse_297',
    category:CATEGORIES.PERSONAL,
    text:'reponse_297',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['297'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_298
  {
    id:'reponse_298',
    category:CATEGORIES.PERSONAL,
    text:'reponse_298',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['298'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_299
  {
    id:'reponse_299',
    category:CATEGORIES.PERSONAL,
    text:'reponse_299',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['299'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2910
  {
    id:'reponse_2910',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2910',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2910'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2911
  {
    id:'reponse_2911',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2911',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2911'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2912
  {
    id:'reponse_2912',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2912',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2912'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2913
  {
    id:'reponse_2913',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2913',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2913'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_2914
  {
    id:'reponse_2914',
    category:CATEGORIES.PERSONAL,
    text:'reponse_2914',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['2914'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_300
  {
    id:'reponse_300',
    category:CATEGORIES.PERSONAL,
    text:'reponse_300',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['300'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_301
  {
    id:'reponse_301',
    category:CATEGORIES.PERSONAL,
    text:'reponse_301',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['301'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_302
  {
    id:'reponse_302',
    category:CATEGORIES.PERSONAL,
    text:'reponse_302',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['302'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_303
  {
    id:'reponse_303',
    category:CATEGORIES.PERSONAL,
    text:'reponse_303',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['303'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_304
  {
    id:'reponse_304',
    category:CATEGORIES.PERSONAL,
    text:'reponse_304',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['304'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_305
  {
    id:'reponse_305',
    category:CATEGORIES.PERSONAL,
    text:'reponse_305',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['305'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_306
  {
    id:'reponse_306',
    category:CATEGORIES.PERSONAL,
    text:'reponse_306',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['306'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_307
  {
    id:'reponse_307',
    category:CATEGORIES.PERSONAL,
    text:'reponse_307',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['307'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_308
  {
    id:'reponse_308',
    category:CATEGORIES.PERSONAL,
    text:'reponse_308',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['308'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_309
  {
    id:'reponse_309',
    category:CATEGORIES.PERSONAL,
    text:'reponse_309',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['309'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_310
  {
    id:'reponse_310',
    category:CATEGORIES.PERSONAL,
    text:'reponse_310',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['310'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_311
  {
    id:'reponse_311',
    category:CATEGORIES.PERSONAL,
    text:'reponse_311',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['311'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_312
  {
    id:'reponse_312',
    category:CATEGORIES.PERSONAL,
    text:'reponse_312',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['312'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_313
  {
    id:'reponse_313',
    category:CATEGORIES.PERSONAL,
    text:'reponse_313',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['313'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_314
  {
    id:'reponse_314',
    category:CATEGORIES.PERSONAL,
    text:'reponse_314',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['314'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_315
  {
    id:'reponse_315',
    category:CATEGORIES.PERSONAL,
    text:'reponse_315',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['315'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_316
  {
    id:'reponse_316',
    category:CATEGORIES.PERSONAL,
    text:'reponse_316',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['316'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_317
  {
    id:'reponse_317',
    category:CATEGORIES.PERSONAL,
    text:'reponse_317',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['317'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_318
  {
    id:'reponse_318',
    category:CATEGORIES.PERSONAL,
    text:'reponse_318',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['318'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_319
  {
    id:'reponse_319',
    category:CATEGORIES.PERSONAL,
    text:'reponse_319',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['319'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_320
  {
    id:'reponse_320',
    category:CATEGORIES.PERSONAL,
    text:'reponse_320',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['320'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_321
  {
    id:'reponse_321',
    category:CATEGORIES.PERSONAL,
    text:'reponse_321',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['321'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_322
  {
    id:'reponse_322',
    category:CATEGORIES.PERSONAL,
    text:'reponse_322',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['322'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_323
  {
    id:'reponse_323',
    category:CATEGORIES.PERSONAL,
    text:'reponse_323',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['323'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_324
  {
    id:'reponse_324',
    category:CATEGORIES.PERSONAL,
    text:'reponse_324',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['324'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_325
  {
    id:'reponse_325',
    category:CATEGORIES.PERSONAL,
    text:'reponse_325',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['325'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_326
  {
    id:'reponse_326',
    category:CATEGORIES.PERSONAL,
    text:'reponse_326',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['326'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_327
  {
    id:'reponse_327',
    category:CATEGORIES.PERSONAL,
    text:'reponse_327',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['327'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_328
  {
    id:'reponse_328',
    category:CATEGORIES.PERSONAL,
    text:'reponse_328',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['328'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_329
  {
    id:'reponse_329',
    category:CATEGORIES.PERSONAL,
    text:'reponse_329',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['329'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_3210
  {
    id:'reponse_3210',
    category:CATEGORIES.PERSONAL,
    text:'reponse_3210',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3210'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_3211
  {
    id:'reponse_3211',
    category:CATEGORIES.PERSONAL,
    text:'reponse_3211',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3211'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_3212
  {
    id:'reponse_3212',
    category:CATEGORIES.PERSONAL,
    text:'reponse_3212',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3212'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_3213
  {
    id:'reponse_3213',
    category:CATEGORIES.PERSONAL,
    text:'reponse_3213',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3213'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_3214
  {
    id:'reponse_3214',
    category:CATEGORIES.PERSONAL,
    text:'reponse_3214',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3214'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_3215
  {
    id:'reponse_3215',
    category:CATEGORIES.PERSONAL,
    text:'reponse_3215',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3215'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_3216
  {
    id:'reponse_3216',
    category:CATEGORIES.PERSONAL,
    text:'reponse_3216',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3216'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_3217
  {
    id:'reponse_3217',
    category:CATEGORIES.PERSONAL,
    text:'reponse_3217',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['3217'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_330
  {
    id:'reponse_330',
    category:CATEGORIES.PERSONAL,
    text:'reponse_330',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['330'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_331
  {
    id:'reponse_331',
    category:CATEGORIES.PERSONAL,
    text:'reponse_331',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['331'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_332
  {
    id:'reponse_332',
    category:CATEGORIES.PERSONAL,
    text:'reponse_332',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['332'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_333
  {
    id:'reponse_333',
    category:CATEGORIES.PERSONAL,
    text:'reponse_333',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['333'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_334
  {
    id:'reponse_334',
    category:CATEGORIES.PERSONAL,
    text:'reponse_334',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['334'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_335
  {
    id:'reponse_335',
    category:CATEGORIES.PERSONAL,
    text:'reponse_335',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['335'],
    scoreMap:[0]
  },
//PARTIE  REPONSE SUMMARY  ... reponse_336
  {
    id:'reponse_336',
    category:CATEGORIES.PERSONAL,
    text:'reponse_336',
    inputType:'radio',
    comment:'',
    options:['show the summary'],
    nextQuestionMap:['336'],
    scoreMap:[0]
  },
]