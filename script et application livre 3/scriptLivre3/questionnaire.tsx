import {
  Component,
  Event,
  EventEmitter,
  h,
  Listen,
  Prop,
  State,
} from '@stencil/core';
import { RouterHistory } from '@stencil/router';
import { LOCAL_STORAGE_KEYS, ROUTES } from '../../../global/constants';
import { QUESTIONS, QUESTION } from '../../../global/questions';
import i18next from '../../../global/utils/i18n';
import { trackEvent, TRACKING_EVENTS } from '../../../global/utils/track';
import version from '../../../global/utils/version';
import { DateInput } from '../../input-date/input-date-functional';
import {
  checkGoTo,
  checkGuard,
  getQuestionIndexById,
  updateScoreData,
  getScoreChange,
} from './utils';

export type Scores = { [key: string]: number };
export type Answers = { [key: string]: string | string[] };

@Component({
  styleUrl: 'questionnaire.css',
  tag: 'ia-questionnaire',
})
export class Questionnaire {
  @Prop() history: RouterHistory;

  @State() language: string;
  @State() currentStep: number = 0;
  @State() previousStep: number;
  @State() answerData: Answers = {};
  @State() scoreData: Scores = {};
  @Event() showLogoHeader: EventEmitter;

  formElement: HTMLFormElement;

  @Listen('changedLanguage', {
    target: 'window',
  })
  changedLanguageHandler(event: CustomEvent) {
    this.language = event.detail.code;
  }

  @Listen('popstate', {
    target: 'window',
  })
  handlePopStateChange() {
    if (this.currentStep > 0) {
      this.history.push(ROUTES.QUESTIONNAIRE, {});
      this.moveToPreviousStep();
    }
  }

  @Listen('updateFormData')
  updateFormDataHandler(event: CustomEvent) {
    const { detail } = event;
    this.setFormData(detail.key, detail.value);
  }

  // TODO: https://github.com/gesundheitscloud/infection-risk-assessment/pull/76
  // This is only a temporary fix. This should be moved/handled differently
  @Listen('onDateChange')
  onDateChangeHandler(event: CustomEvent) {
    const { currentStep } = this;
    const {
      detail: { value },
    } = event;
    this.setFormData(QUESTIONS[currentStep].id, value.split('-').join('.'));
  }

  setFormData(key: string, value: string | string[]) {
    this.answerData = {
      ...this.answerData,
      [key]: value,
    };
  }

  setLocalStorageAnswers = () => {
    localStorage.setItem(
      LOCAL_STORAGE_KEYS.ANSWERS,
      JSON.stringify(this.answerData)
    );
    localStorage.setItem(LOCAL_STORAGE_KEYS.SCORES, JSON.stringify(this.scoreData));
    localStorage.setItem(LOCAL_STORAGE_KEYS.COMPLETED, 'false')
    version.set();
  };

  get progress() {
    return Math.floor(((this.currentStep + 1) / QUESTIONS.length) * 100);
  }

  trackStepMove(isPreviousStep: Boolean) {
    const { id } = QUESTIONS[this.currentStep];
    if (isPreviousStep) {
      trackEvent([...TRACKING_EVENTS.QUESTION_PREVIOUS, id]);
    } else {
      trackEvent([...TRACKING_EVENTS.QUESTION_NEXT, id]);
    }
  }

  moveToNextStep = () => {
    const answerIndex = this.answerData[QUESTIONS[this.currentStep].id];

    if (QUESTIONS[this.currentStep].id === QUESTION.DATA_DONATION) {
      trackEvent([
        ...TRACKING_EVENTS.DATA_DONATION_CONSENT,
        this.answerData[QUESTION.DATA_DONATION] === '0' ? '1' : '0',
      ]);
    }

    this.scoreData = updateScoreData(this.currentStep, answerIndex, this.scoreData);
    this.setLocalStorageAnswers();
    if (!this.checkFinished()) {
      this.currentStep = checkGoTo(this.currentStep, answerIndex);
      const stepBeforeGuard = this.currentStep;
      this.currentStep = checkGuard(
        this.currentStep,
        this.scoreData,
        this.answerData
      );
      if (stepBeforeGuard !== this.currentStep) {
        this.checkFinished();
        return;
      }
      this.trackStepMove(false);
    }
  };

  checkFinished = () => {
    let nextQuestionId = QUESTIONS[this.currentStep].nextQuestionMap;
if(nextQuestionId == '10' ||nextQuestionId == '11' ||nextQuestionId == '12' ||nextQuestionId == '13' ||nextQuestionId == '14' ||nextQuestionId == '15' ||nextQuestionId == '16' ||nextQuestionId == '17' ||nextQuestionId == '18' ||nextQuestionId == '19' ||nextQuestionId == '110' ||nextQuestionId == '111' ||nextQuestionId == '112' ||nextQuestionId == '113' ||nextQuestionId == '114' ||nextQuestionId == '115' ||nextQuestionId == '116' ||nextQuestionId == '117' ||nextQuestionId == '118' ||nextQuestionId == '20' ||nextQuestionId == '21' ||nextQuestionId == '22' ||nextQuestionId == '23' ||nextQuestionId == '24' ||nextQuestionId == '25' ||nextQuestionId == '26' ||nextQuestionId == '27' ||nextQuestionId == '30' ||nextQuestionId == '31' ||nextQuestionId == '32' ||nextQuestionId == '33' ||nextQuestionId == '34' ||nextQuestionId == '35' ||nextQuestionId == '36' ||nextQuestionId == '37' ||nextQuestionId == '38' ||nextQuestionId == '39' ||nextQuestionId == '310' ||nextQuestionId == '311' ||nextQuestionId == '312' ||nextQuestionId == '40' ||nextQuestionId == '41' ||nextQuestionId == '42' ||nextQuestionId == '43' ||nextQuestionId == '44' ||nextQuestionId == '45' ||nextQuestionId == '46' ||nextQuestionId == '50' ||nextQuestionId == '51' ||nextQuestionId == '52' ||nextQuestionId == '53' ||nextQuestionId == '54' ||nextQuestionId == '60' ||nextQuestionId == '61' ||nextQuestionId == '62' ||nextQuestionId == '63' ||nextQuestionId == '64' ||nextQuestionId == '65' ||nextQuestionId == '66' ||nextQuestionId == '67' ||nextQuestionId == '68' ||nextQuestionId == '69' ||nextQuestionId == '610' ||nextQuestionId == '611' ||nextQuestionId == '612' ||nextQuestionId == '613' ||nextQuestionId == '614' ||nextQuestionId == '70' ||nextQuestionId == '71' ||nextQuestionId == '72' ||nextQuestionId == '73' ||nextQuestionId == '74' ||nextQuestionId == '75' ||nextQuestionId == '76' ||nextQuestionId == '77' ||nextQuestionId == '78' ||nextQuestionId == '79' ||nextQuestionId == '80' ||nextQuestionId == '81' ||nextQuestionId == '82' ||nextQuestionId == '83' ||nextQuestionId == '84' ||nextQuestionId == '85' ||nextQuestionId == '86' ||nextQuestionId == '87' ||nextQuestionId == '88' ||nextQuestionId == '89' ||nextQuestionId == '810' ||nextQuestionId == '811' ||nextQuestionId == '90' ||nextQuestionId == '91' ||nextQuestionId == '92' ||nextQuestionId == '93' ||nextQuestionId == '94' ||nextQuestionId == '95' ||nextQuestionId == '96' ||nextQuestionId == '97' ||nextQuestionId == '98' ||nextQuestionId == '99' ||nextQuestionId == '910' ||nextQuestionId == '100' ||nextQuestionId == '101' ||nextQuestionId == '102' ||nextQuestionId == '103' ||nextQuestionId == '104' ||nextQuestionId == '105' ||nextQuestionId == '106' ||nextQuestionId == '107' ||nextQuestionId == '108' ||nextQuestionId == '109' ||nextQuestionId == '1010' ||nextQuestionId == '1011' ||nextQuestionId == '1012' ||nextQuestionId == '1013' ||nextQuestionId == '1014' ||nextQuestionId == '1015' ||nextQuestionId == '1016' ||nextQuestionId == '1017' ||nextQuestionId == '110' ||nextQuestionId == '111' ||nextQuestionId == '112' ||nextQuestionId == '113' ||nextQuestionId == '114' ||nextQuestionId == '115' ||nextQuestionId == '116' ||nextQuestionId == '117' ||nextQuestionId == '118' ||nextQuestionId == '119' ||nextQuestionId == '1110' ||nextQuestionId == '1111' ||nextQuestionId == '1112' ||nextQuestionId == '1113' ||nextQuestionId == '1114' ||nextQuestionId == '1115' ||nextQuestionId == '1116' ||nextQuestionId == '120' ||nextQuestionId == '121' ||nextQuestionId == '122' ||nextQuestionId == '123' ||nextQuestionId == '124' ||nextQuestionId == '125' ||nextQuestionId == '126' ||nextQuestionId == '127' ||nextQuestionId == '128' ||nextQuestionId == '129' ||nextQuestionId == '130' ||nextQuestionId == '131' ||nextQuestionId == '132' ||nextQuestionId == '133' ||nextQuestionId == '134' ||nextQuestionId == '135' ||nextQuestionId == '136' ||nextQuestionId == '137' ||nextQuestionId == '138' ||nextQuestionId == '139' ||nextQuestionId == '1310' ||nextQuestionId == '1311' ||nextQuestionId == '1312' ||nextQuestionId == '140' ||nextQuestionId == '141' ||nextQuestionId == '142' ||nextQuestionId == '143' ||nextQuestionId == '144' ||nextQuestionId == '145' ||nextQuestionId == '146' ||nextQuestionId == '147' ||nextQuestionId == '148' ||nextQuestionId == '149' ||nextQuestionId == '1410' ||nextQuestionId == '1411' ||nextQuestionId == '1412' ||nextQuestionId == '1413' ||nextQuestionId == '1414' ||nextQuestionId == '1415' ||nextQuestionId == '1416' ||nextQuestionId == '1417' ||nextQuestionId == '150' ||nextQuestionId == '151' ||nextQuestionId == '152' ||nextQuestionId == '153' ||nextQuestionId == '154' ||nextQuestionId == '155' ||nextQuestionId == '156' ||nextQuestionId == '157' ||nextQuestionId == '158' ||nextQuestionId == '159' ||nextQuestionId == '1510' ||nextQuestionId == '1511' ||nextQuestionId == '1512' ||nextQuestionId == '1513' ||nextQuestionId == '1514' ||nextQuestionId == '1515' ||nextQuestionId == '160' ||nextQuestionId == '161' ||nextQuestionId == '162' ||nextQuestionId == '163' ||nextQuestionId == '164' ||nextQuestionId == '165' ||nextQuestionId == '166' ||nextQuestionId == '167' ||nextQuestionId == '170' ||nextQuestionId == '171' ||nextQuestionId == '172' ||nextQuestionId == '173' ||nextQuestionId == '174' ||nextQuestionId == '175' ||nextQuestionId == '176' ||nextQuestionId == '177' ||nextQuestionId == '178' ||nextQuestionId == '179' ||nextQuestionId == '1710' ||nextQuestionId == '1711' ||nextQuestionId == '1712' ||nextQuestionId == '1713' ||nextQuestionId == '180' ||nextQuestionId == '181' ||nextQuestionId == '182' ||nextQuestionId == '183' ||nextQuestionId == '184' ||nextQuestionId == '185' ||nextQuestionId == '186' ||nextQuestionId == '187' ||nextQuestionId == '190' ||nextQuestionId == '191' ||nextQuestionId == '192' ||nextQuestionId == '193' ||nextQuestionId == '194' ||nextQuestionId == '195' ||nextQuestionId == '196' ||nextQuestionId == '197' ||nextQuestionId == '198' ||nextQuestionId == '199' ||nextQuestionId == '1910' ||nextQuestionId == '1911' ||nextQuestionId == '1912' ||nextQuestionId == '200' ||nextQuestionId == '201' ||nextQuestionId == '202' ||nextQuestionId == '203' ||nextQuestionId == '204' ||nextQuestionId == '205' ||nextQuestionId == '206' ||nextQuestionId == '207' ||nextQuestionId == '208' ||nextQuestionId == '209' ||nextQuestionId == '2010' ||nextQuestionId == '2011' ||nextQuestionId == '2012' ||nextQuestionId == '2013' ||nextQuestionId == '2014' ||nextQuestionId == '2015' ||nextQuestionId == '2016' ||nextQuestionId == '2017' ||nextQuestionId == '2018' ||nextQuestionId == '2019' ||nextQuestionId == '2020' ||nextQuestionId == '210' ||nextQuestionId == '211' ||nextQuestionId == '212' ||nextQuestionId == '213' ||nextQuestionId == '214' ||nextQuestionId == '215' ||nextQuestionId == '220' ||nextQuestionId == '221' ||nextQuestionId == '222' ||nextQuestionId == '223' ||nextQuestionId == '224' ||nextQuestionId == '225' ||nextQuestionId == '226' ||nextQuestionId == '227' ||nextQuestionId == '228' ||nextQuestionId == '229' ||nextQuestionId == '2210' ||nextQuestionId == '2211' ||nextQuestionId == '2212' ||nextQuestionId == '2213' ||nextQuestionId == '230' ||nextQuestionId == '231' ||nextQuestionId == '232' ||nextQuestionId == '233' ||nextQuestionId == '234' ||nextQuestionId == '235' ||nextQuestionId == '236' ||nextQuestionId == '237' ||nextQuestionId == '238' ||nextQuestionId == '239' ||nextQuestionId == '2310' ||nextQuestionId == '2311' ||nextQuestionId == '240' ||nextQuestionId == '241' ||nextQuestionId == '242' ||nextQuestionId == '243' ||nextQuestionId == '244' ||nextQuestionId == '245' ||nextQuestionId == '246' ||nextQuestionId == '247' ||nextQuestionId == '248' ||nextQuestionId == '250' ||nextQuestionId == '251' ||nextQuestionId == '252' ||nextQuestionId == '253' ||nextQuestionId == '254' ||nextQuestionId == '255' ||nextQuestionId == '260' ||nextQuestionId == '261' ||nextQuestionId == '262' ||nextQuestionId == '263' ||nextQuestionId == '264' ||nextQuestionId == '265' ||nextQuestionId == '266' ||nextQuestionId == '267' ||nextQuestionId == '268' ||nextQuestionId == '269' ||nextQuestionId == '2610' ||nextQuestionId == '2611' ||nextQuestionId == '2612' ||nextQuestionId == '2613' ||nextQuestionId == '2614' ||nextQuestionId == '2615' ||nextQuestionId == '2616' ||nextQuestionId == '2617' ||nextQuestionId == '2618' ||nextQuestionId == '270' ||nextQuestionId == '271' ||nextQuestionId == '272' ||nextQuestionId == '273' ||nextQuestionId == '274' ||nextQuestionId == '275' ||nextQuestionId == '276' ||nextQuestionId == '277' ||nextQuestionId == '278' ||nextQuestionId == '279' ||nextQuestionId == '2710' ||nextQuestionId == '2711' ||nextQuestionId == '2712' ||nextQuestionId == '280' ||nextQuestionId == '281' ||nextQuestionId == '282' ||nextQuestionId == '283' ||nextQuestionId == '284' ||nextQuestionId == '285' ||nextQuestionId == '286' ||nextQuestionId == '287' ||nextQuestionId == '288' ||nextQuestionId == '289' ||nextQuestionId == '2810' ||nextQuestionId == '290' ||nextQuestionId == '291' ||nextQuestionId == '292' ||nextQuestionId == '293' ||nextQuestionId == '294' ||nextQuestionId == '295' ||nextQuestionId == '296' ||nextQuestionId == '297' ||nextQuestionId == '298' ||nextQuestionId == '299' ||nextQuestionId == '2910' ||nextQuestionId == '2911' ||nextQuestionId == '2912' ||nextQuestionId == '2913' ||nextQuestionId == '2914' ||nextQuestionId == '300' ||nextQuestionId == '301' ||nextQuestionId == '302' ||nextQuestionId == '303' ||nextQuestionId == '304' ||nextQuestionId == '305' ||nextQuestionId == '306' ||nextQuestionId == '307' ||nextQuestionId == '308' ||nextQuestionId == '309' ||nextQuestionId == '310' ||nextQuestionId == '311' ||nextQuestionId == '312' ||nextQuestionId == '313' ||nextQuestionId == '314' ||nextQuestionId == '315' ||nextQuestionId == '316' ||nextQuestionId == '317' ||nextQuestionId == '318' ||nextQuestionId == '319' ||nextQuestionId == '320' ||nextQuestionId == '321' ||nextQuestionId == '322' ||nextQuestionId == '323' ||nextQuestionId == '324' ||nextQuestionId == '325' ||nextQuestionId == '326' ||nextQuestionId == '327' ||nextQuestionId == '328' ||nextQuestionId == '329' ||nextQuestionId == '3210' ||nextQuestionId == '3211' ||nextQuestionId == '3212' ||nextQuestionId == '3213' ||nextQuestionId == '3214' ||nextQuestionId == '3215' ||nextQuestionId == '3216' ||nextQuestionId == '3217' ||nextQuestionId == '330' ||nextQuestionId == '331' ||nextQuestionId == '332' ||nextQuestionId == '333' ||nextQuestionId == '334' ||nextQuestionId == '335' ||nextQuestionId == '336' ){localStorage.setItem(LOCAL_STORAGE_KEYS.SUMMARY_COMMENT_ID, nextQuestionId);
        this.history.push(ROUTES.SUMMARY, {});
        trackEvent(TRACKING_EVENTS.FINISH);
        return true;
      }

    return false;
  };

  // TODO: (Radimir) optimise for displaying the last answered question
  moveToPreviousStep = () => {
    if (this.currentStep === 0) {
      this.history.push(`/`, {});
      localStorage.removeItem(LOCAL_STORAGE_KEYS.ANSWERS);
      trackEvent(TRACKING_EVENTS.ABORT);
    } else {
      if (this.answerData) {
        const formDataKeys = Object.keys(this.answerData);
        const previousDataKey = formDataKeys[formDataKeys.length - 1];
        const previousQuestion = QUESTIONS[getQuestionIndexById(previousDataKey)];
        if (previousQuestion.scoreMap) {
          const previousAnswer = this.answerData[previousDataKey];
          this.scoreData[previousQuestion.category] -= getScoreChange(
            previousQuestion,
            previousAnswer
          );
        }

        delete this.answerData[previousDataKey];

        this.setLocalStorageAnswers();

        this.currentStep = previousDataKey
          ? getQuestionIndexById(previousDataKey)
          : 0;
      } else {
        this.currentStep--;
      }
      this.trackStepMove(true);
    }
  };

  submitForm = event => {
    event.preventDefault();
    event.target.querySelector('input').focus();
    this.moveToNextStep();
  };

  componentWillLoad = () => {
    this.showLogoHeader.emit({ show: false });
    if (!version.match()) {
      version.reset();
    }
    const availableAnswers = JSON.parse(
      localStorage.getItem(LOCAL_STORAGE_KEYS.ANSWERS)
    );
    const availableScores = JSON.parse(
      localStorage.getItem(LOCAL_STORAGE_KEYS.SCORES)
    );
    this.answerData = availableAnswers ? availableAnswers : {};
    this.scoreData = availableScores ? availableScores : {};

    const formDataKeys = Object.keys(this.answerData);

    if (formDataKeys.length) {
      this.currentStep = getQuestionIndexById(formDataKeys[formDataKeys.length - 1]);

      this.moveToNextStep();
    }
  };

  render() {
    const { submitForm, currentStep, moveToPreviousStep, progress } = this;

    return (
      currentStep < QUESTIONS.length && (
        <div class="questionnaire c-card-wrapper">
          <form
            onSubmit={event => submitForm(event)}
            ref={el => (this.formElement = el as HTMLFormElement)}
            data-test="questionnaireForm"
          >
            <d4l-card classes="card--desktop card--text-center">
              <div class="u-margin-horizontal--card-negative" slot="card-header">
                <ia-navigation-header
                  headline={i18next.t(QUESTIONS[currentStep].text)}
                  handleClick={() => moveToPreviousStep()}
                />
                <d4l-linear-progress data-test="progressBar" value={progress} />
              </div>
              <div
                class="questionnaire__content u-padding-vertical--medium u-text-align--left"
                slot="card-content"
              >
                <fieldset>
                  <legend class="u-visually-hidden">
                    {i18next.t(QUESTIONS[currentStep].text)}
                  </legend>
                  <div class="questionnaire__form u-padding-vertical--normal ">
                    {QUESTIONS[currentStep].inputType === 'radio' && (
                      <ia-input-radio
                        data-test="questionnaireRadioInputs"
                        question={QUESTIONS[currentStep]}
                        currentSelection={this.answerData[QUESTIONS[currentStep].id]}
                      />
                    )}
                    {QUESTIONS[currentStep].inputType === 'checkbox' && (
                      <ia-input-multiple-choice question={QUESTIONS[currentStep]} />
                    )}

                    {QUESTIONS[currentStep].inputType === 'date' && <DateInput />}
                    {QUESTIONS[currentStep].inputType === 'postal' && (
                      <ia-input-postal-code question={QUESTIONS[currentStep]} />
                    )}
                  </div>
                  {QUESTIONS[currentStep].comment && (
                    <p
                      class="questionnaire__comment"
                      innerHTML={`${i18next.t(QUESTIONS[currentStep].comment)}`}
                    ></p>
                  )}
                </fieldset>
              </div>
              <div slot="card-footer">
                <d4l-button
                  classes="button--block"
                  data-test="continueButton"
                  text={
                    currentStep === QUESTIONS.length - 1
                      ? i18next.t('questionnaire_button_generate_qr')
                      : i18next.t('questionnaire_button_next')
                  }
                />
              </div>
            </d4l-card>
          </form>
        </div>
      )
    );
  }
}
