Constipation

INTRODUCTION TO COMPLAIN TITREGENERALE
It is common in Western society. The frequency of constipation diagnoses depends on both the patient’s and the physician’s definitions of the problem. One bowel movement each day is what most patients consider normal.
However, many physicians may accept two or three hard and dry stools a week
as normal if this pattern is usual for the individual. Some experts believe that
anyone who strains to defecate or does not effortlessly pass at least one soft
stool daily is constipated. By this definition, constipation is very common.
Constipation may also be defined as a change in the person’s normal bowel
pattern to less frequent or more difficult defecation. In this chapter, constipation
is defined as straining with bowel movements or bowel movements that occur
fewer than three times per week. The American College of Gastroenterology
defines constipation as unsatisfactory defecation characterized by infrequent
stools, difficult stool passage, or both.” The Rome III criteria are generally used
to define chronic functional constipation in adults, infants, and young children.
They define chronic as the presence of these symptoms for at least 3 months.
Constipation has been attributed to the deficiency of dietary fiber in the customary Western diet, which has replaced fiber with excessive ingestion of refined
carbohydrates.
Constipation is usually not caused by a serious disease. In most patients it can
be corrected by decreasing the amount of refined carbohydrates and increasing
the amount of fiber ingested. It is particularly important for the examiner to ask
whether the constipation is of recent onset and constitutes a change in previous
bowel habits, since such findings may indicate serious disease.
The most common causes of constipation are laxative habit, diets high in
refined carbohydrates and low in fiber, change in daily habits or environment,
drug use, irritable bowel syndrome (IBS), and painful defecation due to a local
anorectal problem. In many instances, it is important to differentiate IBS from
chronic constipation. Pain or abdominal discomfort must be present to diagnose
IBS. In many cases, multiple factors contribute to the patient’s constipation. Less
common causes include bowel tumors, fecal impaction, pregnancy, and metabolic disorders (e.g., hypothyroidism, diabetes, hypercalcemia).
The common causes of constipation can be divided into three categories:
simple constipation, disordered motility, and secondary constipation. Simple
constipation usually results from a diet that contains excessive refined carbohydrates and is deficient in fiber. It may be influenced by environmental factors.
Disordered motility, the next most common category, is seen with idiopathic
slow transit (most common in elderly persons), idiopathic megacolon and
megarectum (more common in children), irritable bowel syndrome, and uncomplicated diverticular disease. Secondary constipation can be a response to drugs
(especially codeine, opiates, and calcium channel blockers); chronic use of laxatives; prolonged immobilization; and organic disease of the anus, rectum, or
colon (anal fissures, strictures, and carcinoma).

$NATURE OF PATIENT TITREGENERALE
Constipation is uncommon in neonates and is usually caused by anal fissures or feeding problems. It rarely results from congenital problems such as
Hirschsprung’s disease. Cow’s milk occasionally causes constipation. In children the most common cause of constipation is a change in daily habits or
environment. The earlier that constipation begins in childhood, the greater the
likelihood of an organic cause.
Disorders of defecation in children are common, difficult to manage, and
negatively perceived by the family. Approximately 85% to 95% of constipation in children is functional. In children ages 6 months to 3 years, it frequently
results from some psychological cause. Early diagnosis of psychological causes
of constipation in children facilitates treatment. For example, toilet training may
have occurred recently, or constipation may have developed because the child
had a condition, such as a rectal fissure, that caused painful defecation. Fearing
painful defecation, the child suppressed the urge to defecate, perpetuating the
production of hard, dry stools, which continued to irritate the rectal fissure.
In adults, most constipation is caused by dietary fiber deficiency or laxative
habit. Laxative use is more common in women than men. Constipation is a
common complaint in elderly persons, although 80% to 90% of patients older
than 60 years have one or more bowel movements per day. Despite this fact,
50% of patients older than 60 years use laxatives. This apparent discrepancy
may be a result of older persons’ greater preoccupation with bowel function in
comparison with younger people. Constipation in elderly patients most often is
caused by inappropriate diet, decreased activity, poor dentition, use of constipating medications, and impaired motility. Constipation ranks with joint pain,
dizziness, forgetfulness, depression, and bladder problems as one of the major
causes of misery in older patients.
Irritable bowel syndrome occurs more frequently in patients with chronic
fatigue syndrome, fibromyalgia, and temporomandibular joint (TMJ) syndrome.
Obstipation (regular passage of hard stools at 3- to 5-day intervals) and
fecal impaction are most common in elderly patients. Some patients with fecal
impaction have continuous soiling (liquid passing around hard stools), which
they describe as diarrhea. Elderly patients who are confined to bed, drink small
amounts of fluid, or take constipating drugs are particularly vulnerable to fecal
impaction. Although fecal impaction is more common in elderly persons, the
physician must be aware that it can occur in any patient subjected to sudden
immobility, bed rest, or a marked change in diet or fluid consumption.
Tumors of the bowel are uncommon in children and young adults, but their
frequency increases after age 40 years. In any patient older than 40 years who
presents with recent constipation or a marked change in bowel habits, the physician must rule out carcinoma of the colon and rectum.

$NATURE OF SYMPTOMS TITREGENERALE
The most important factor in determining whether constipation should be investigated for a serious cause or treated symptomatically and by dietary modification is whether it is chronic or of recent onset. The second most important
factor is the presence of alarm symptoms. Patients who have long-standing constipation and those whose constipation developed with a recent depressive illness, change in diet, recent debility, or ingestion of constipating medicine can be
treated symptomatically. Their clinical status should be reassessed after a few
weeks of appropriate therapy. Patients with unexplained constipation of recent
onset or a sudden aggravation of existing constipation associated with abdominal pain, unexplained weight loss, passing of blood or mucus, a progressive
decrease in the number of bowel movements per day, or a substantial increase
in laxative requirements should be investigated even in the absence of abnormal physical findings. These patients should undergo colonoscopy. Constipation
occurs in less than one third of patients with carcinoma of the colon and is less
common than diarrhea in these patients.
Ribbon-like stools suggest a motility disorder. They can also be caused by an
organic narrowing of the distal or sigmoid colon. A progressive decrease in the
caliber of the stools suggests an organic lesion. If the patient complains of stools
that have a toothpaste-like caliber, fecal impaction should be suspected.
Tumors of the descending colon are more common in older adults with rightsided lesions, increasing in frequency as the patient grows older. One third of
colon cancers in patients older than 80 years are right sided, and fewer than half
of these patients have altered bowel habits. They usually present late with an
anemia or a palpable mass on the right side of the colon.

$ASSOCIATED SYMPTOMS TITREGENERALE
In children, simple constipation is often associated with abdominal distention,
decreased appetite, irritability, reduced activity, and soiling. Obstipated children
(i.e., those who pass hard, dry stools every 3 to 5 days) may also have overflow
soiling. If soiling persists after the constipation or obstipation has improved, the
physician should search for some psychological disturbance; fecal impaction is
relatively rare in children. When children have abdominal distention associated
with constipation, the problem is usually caused by improper diet or decreased
motility; intestinal obstruction is a rare cause of constipation in children. Acting
out, school problems, and difficulties with interpersonal relationships should
suggest that the constipation has a psychological cause.
If constipation is associated with steatorrhea and greenish yellow stools, a
small-bowel or pancreatic lesion should be suspected. Constipation with earlymorning rushes, hypogastric cramps, tenesmus, and passage of blood suggests
that the constipation is of colonic origin.
In adults, colicky pain; abdominal distention; anemia; decreased effectiveness
of laxatives; weight loss; anorexia; and blood, pus, or mucus in the stool should
suggest a colonic tumor. Weight loss, anorexia, and anemia are usually observed
only in the late phases of colonic tumors. Rectal discomfort, leakage of stool,
bleeding, urgency, tenesmus, diarrhea alternating with constipation, and rectal
prolapse all suggest a rectosigmoid tumor. In contrast, abdominal distention
and sudden onset of obstipation suggest a more proximal colon lesion.
If constipation alternates with diarrhea or is accompanied by passage of hard
scybala or intermittent left- or right-sided iliac pain that occasionally radiates
through the torso to the back, constipation is probably caused by irritable bowel
syndrome. A change in stool frequency or consistency associated with abdominal pain also suggests an irritable colon, as do abdominal distention and mucus
in the stools.
Fecal incontinence, which some patients actually describe as diarrhea, should
suggest fecal impaction. Most patients with this condition must strain during
defecation, do not have normally formed stools, often have a toothpaste-like
stool, and often have continuous soiling. Rectal examination usually reveals
large quantities of hard feces in the rectal ampulla.
Constipation that develops after painful hemorrhoids suggests that the constipation is secondary to suppression of defecation because it induces pain.
Patients with this condition, as well as others with local anorectal problems,
often report blood in the bowel movements. When questioned, they usually recognize that the blood is on the surface of the stool, on the toilet paper, or in the
toilet bowl rather than mixed in with the stool.

$PRECIPITATING AND AGGRAVATING FACTORS TITREGENERALE
Patients and their family members should be questioned about stressful problems such as divorce, death of a spouse, and loss of a job. Twenty percent of
children have moderate constipation when they begin to become effectively toilet trained. Functional constipation may be precipitated by hospital admission,
traveling, suppression of the urge to defecate, uncomfortable lavatory environment (e.g., latrine), and/or immobilization. Reduction in laxative dose or change
in brand of laxative occasionally precipitates constipation.
In elderly patients, constipation can be aggravated by limited variety in meals,
decreased fluid or fiber intake, poor dentition, increased costs of fresh high-fiber
foods (e.g., fruits, vegetables), impaired motility, debilitating illnesses, confinement to bed, and frequent ingestion of constipating drugs.
Certain drugs typically cause constipation. These include antacids that contain aluminum hydroxide or calcium, anticholinergics, antiparkinsonian drugs,
diuretics, tricyclic antidepressants, opiates (in analgesics or cough medicine),
phenytoin, cough medications calcium channel blockers, iron-containing
compounds, and certain antihypertensive and antiseizure drugs. The patient
may be taking one of these constipating agents but may experience symptoms
only when a second drug is added.

$AMELIORATING FACTORS TITREGENERALE
A diet high in fiber (e.g., whole-grain cereals, bran, vegetable fiber) and low
in refined sugar often alleviates constipation. Many patients find that weight
reduction diets that are low in carbohydrates and high in fiber relieve constipation. Increased fluid intake, particularly in patients who do not drink enough
fluids or take diuretics, is often helpful. Relief of painful anorectal problems
frequently alleviates constipation. If a change in daily routine has caused constipation, instructing the patient to readjust daily activities to permit bowel movements at the time of the urge to defecate is often effective.

$PHYSICAL FINDINGS TITREGENERALE
A rectal examination should be performed in all patients with a significant
complaint of constipation. This examination allows the physician to determine
whether the rectum is full or empty, whether an anorectal problem exists (e.g.,
fissure, hemorrhoids), and whether a rectal mass is present. A stool specimen
should be obtained to check for occult blood.
The physician should also conduct an abdominal examination, with special attention to the colon, to detect palpable masses (fecal or otherwise) or
abdominal tenderness. Normally, the colon is neither palpable nor tender. If
the colon can be palpated and it is tender, particularly over the descending
or ascending regions, an irritable bowel syndrome is probable. Stigmata of
myxedema and abdominal distention should suggest that the constipation is
secondary to hypothyroidism. Anemia, manifested by tachycardia and conjunctival pallor, should suggest a right-sided colonic lesion. Abnormal bowel
sounds, abdominal distention, or surgical scars suggest a specific cause for the
constipation.

$DIAGNOSTIC STUDIES TITREGENERALE
In most instances of chronic constipation, a trial of dietary changes, including
increased fiber and fluids and decreased refined carbohydrates, will alleviate the
constipation and the need for additional tests. A stool diary (date, frequency,
time, symptoms of constipation [straining, bloating, incomplete evacuation],
and medication) may be helpful. Examination of stools for occult blood and
measurement of the hematocrit or hemoglobin and the erythrocyte sedimentation rate are appropriate, inexpensive, and noninvasive tests to evaluate constipation. In instances of alarm symptoms, colonoscopy and barium enema study
are also helpful.
Newer studies, considered cost-effective in specific instances, are a colonic
transit study with radiopaque markers, anorectal manometry, and balloon
expulsion tests.

$LESS COMMON DIAGNOSTIC CONSIDERATIONS TITREGENERALE
In neonates, Hirschsprung’s disease (aganglionic megacolon) is the most prevalent of the less common causes of constipation. Early diagnosis is important.
To differentiate between Hirschsprung’s disease and chronic constipation, the
physician should be aware that each condition is associated with characteristic
symptoms. In Hirschsprung’s disease, symptoms usually date from birth, soiling
is absent, and the rectum is empty. With chronic constipation, onset may occur
from ages 6 months to 3 years, soiling is common, and the rectum is usually full
of feces.
Other less common causes of constipation in children include anorectal stenosis, chronic lead poisoning, hypothyroidism, hyperparathyroidism, medication, anal fissures, allergy to cow’s milk, and neurologic abnormalities such as
spina bifida.
If constipation is associated with nausea, polyuria, fatigue, and sometimes a
history of kidney stones, hypercalcemia, though uncommon, may be the cause.
Other metabolic and endocrinologic causes of constipation are diabetes, pregnancy, and hypothyroidism. Constipation may be the presenting symptom in
patients with classic myxedema. Myxedematous patients frequently have an
adynamic bowel with moderate abdominal distention. An adynamic bowel
also occurs with senility, scleroderma, autonomic neuropathy, and spinal cord
injuries and diseases. Idiopathic megacolon and megarectum can also produce
severe constipation.
Psychiatric causes of constipation include depressive illness and anorexia nervosa. Occasionally, constipation is induced in depressed patients as the result of
the anticholinergic effect of antidepressant drugs.
Marked prostatic enlargement may contribute to constipation. Disease or
weakness of the abdominal muscles, neurologic diseases, diverticular diseases,
occult rectal prolapse, pancreatic carcinoma, and lead poisoning are all rare
causes of constipation.

ASK THE FOLLOWING QUESTION
What is the condition of?
Low dietary fiber#NATURE OF PATIENT :Most common cause of constipation in adults, NATURE OF SYMPTOMS :Dry, hard stools Stool frequency less than 5 times/wk Straining during stool, ASSOCIATED SYMPTOMS :Children: may be associated with abdominal distention#PRECIPITATING AND AGGRAVATING FACTORS :Excessive intake of refined carbohydrates, AMELIORATING FACTORS :Increased dietary fiber Decreased intake of refined carbohydrates, PHYSICAL FINDINGS :Ascending or descending colon tender to palpation Feces palpable in sigmoid colon
@Functional (environmental and daily-habit changes)#NATURE OF PATIENT :Most common cause of constipation in children Common cause in adults, NATURE OF SYMPTOMS :Vague abdominal discomfort, ASSOCIATED SYMPTOMS :Children: Abdominal distention Decreased appetite Irritability Decreased activity Soiling#PRECIPITATING AND AGGRAVATING FACTORS :Toilet training Traveling Uncomfortable lavatories Immobilization, AMELIORATING FACTORS :Resume prior habits
@Irritable bowel syndrome#NATURE OF PATIENT :Any age, especially young adults More frequent in patients with chronic fatigue syndrome, fibromyalgia, and TMJ syndrome, NATURE OF SYMPTOMS :Change in stool frequency and consistency Constipation alternates with diarrhea Mucus in stools, ASSOCIATED SYMPTOMS :Abdominal distention Abdominal pain#PRECIPITATING AND AGGRAVATING FACTORS :Stress, AMELIORATING FACTORS :Increased dietary fiber Decreased environmental stress, PHYSICAL FINDINGS :Colon tender on palpation
@Obstipation/fecal impaction#NATURE OF PATIENT :Most common in elderly and those subject to sudden immobility, bed rest, or decreased fluid consumption, NATURE OF SYMPTOMS :Regular passage of hard stools at 3- to 5-day intervals Patients describe continuous soiling as diarrhea Toothpaste-caliber stools Fecal incontinence Straining during stool#PHYSICAL FINDINGS :Large quantities of hard feces in rectal ampulla
@Idiopathic slow transit#NATURE OF PATIENT :Most common in elderly people, NATURE OF SYMPTOMS :Dry, hard stools Decreased stool frequency#PRECIPITATING AND AGGRAVATING FACTORS :Decreased exercise Decreased dietary fiber Decreased fluid intake, AMELIORATING FACTORS :Increased exercise Increased dietary fiber Increased fluids Improved dentition
@Hirschsprung’s disease (aganglionic megacolon)#NATURE OF PATIENT :Rare cause of constipation in children, NATURE OF SYMPTOMS :Present from birth Soiling absent#PHYSICAL FINDINGS :Rectum empty, DIAGNOSTIC STUDIES :Barium enema
@Anal fissure#NATURE OF PATIENT :Affects any age, NATURE OF SYMPTOMS :Patient suppresses painful defecation Hard stools, ASSOCIATED SYMPTOMS :Blood on surface of feces, on toilet paper, or in toilet#PHYSICAL FINDINGS :Anal fissure, DIAGNOSTIC STUDIES :Anoscopy
@Hemorrhoids#NATURE OF PATIENT :Adults, NATURE OF SYMPTOMS :Patient suppresses painful defecation, ASSOCIATED SYMPTOMS :Blood on surface of feces, on toilet paper, or in toilet#PRECIPITATING AND AGGRAVATING FACTORS :Pregnancy Prior hemorrhoid problems, PHYSICAL FINDINGS :Hemorrhoids, DIAGNOSTIC STUDIES :Anoscopy
@Drug-induced constipation#NATURE OF PATIENT :Chronic laxative users Patients taking other medications Most common in elderly patients#PRECIPITATING AND AGGRAVATING FACTORS :Laxatives Codeine Morphine Analgesics NSAIDs Aluminum-containing antacids Antiparkinsonian drugs Cough mixtures Antidepressants Iron and calcium supplements Antihypertensives Calcium channel blockers
@Bowel tumors#NATURE OF PATIENT :Uncommon in children Frequency increases after age 40 yr, NATURE OF SYMPTOMS :Constipation in less than one third of patients with colon cancer; diarrhea more common Early-morning rushes Decreased laxative effectiveness Blood, pus, or mucus in stool, ASSOCIATED SYMPTOMS :Elderly patients with right-sided lesions may present late with anemia Weight loss Anorexia Colicky abdominal pain and distention#PHYSICAL FINDINGS :Possible palpable mass, DIAGNOSTIC STUDIES :Rectal examination Stool examination for occult blood Colonoscopy Barium enema
@Rectosigmoid tumors#NATURE OF SYMPTOMS :Rectal discomfort Stool leakage Urgency, tenesmus Progressive narrowing of stool caliber, ASSOCIATED SYMPTOMS :Rectal bleeding Rectal prolapse#DIAGNOSTIC STUDIES :Sigmoidoscopy
@Proximal colon tumors#NATURE OF SYMPTOMS :Sudden onset Obstipation, ASSOCIATED SYMPTOMS :Abdominal distention#PHYSICAL FINDINGS :Anemia, DIAGNOSTIC STUDIES :Colonoscopy Barium enema

