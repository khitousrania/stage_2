import json

fileDetails = open("detailsMaladie.txt",'r+',encoding="utf-8")
fileListe= open("listeMaladie.txt",'r+',encoding="utf-8")
fileHealp =open("fileHealp.txt",'w+',encoding="utf-8")

tab_ListeMladie=[]

def listingMaladie():
    i=0
    for ligne in fileListe:
        i+=1
        if('\n' in ligne):
            tab_ListeMladie.append(ligne[:-1])
        else:
            tab_ListeMladie.append(ligne)
      

listingMaladie()

dict_maladies ={}

def decoupeMaladies():
    fileHealp.write(fileDetails.read())
    for key,m in enumerate(tab_ListeMladie):
        if(key==0):
            pass
        else:
            # print(tab_ListeMladie[key-1])
            fileHealp.seek(0,0)
            s= fileHealp.read() 
            sentences =s.split(tab_ListeMladie[key])
            fileMaladie =open("maladies/"+tab_ListeMladie[key-1][:-1]+'.txt','w+',encoding='utf-8')
            dict_maladies[tab_ListeMladie[key-1][:-1]]=sentences[0]
            fileMaladie.write(tab_ListeMladie[key-1][:-1]+'\n'+sentences[0])
            fileHealp.seek(0,0)
            fileHealp.truncate(0)
            fileHealp.seek(0,0)
            fileHealp.write(sentences[1])
            if(key==len(tab_ListeMladie)-1):
                fileMaladie =open("maladies/"+tab_ListeMladie[key][:-1]+'.txt','w+',encoding='utf-8')
                dict_maladies[m]=sentences[1]
                fileMaladie.write(tab_ListeMladie[key][:-1]+'\n'+sentences[1])
                
   
decoupeMaladies()

dict_maladies_bis={}

keywords =['ASK THE FOLLOWING QUESTION','GENERAL INFORMATION','$','TITREGENERALE','\n','?','@','#','OPTION_','TITREOPTION']


def generateTitleKeys(titre):
    titreGeneral =''
    print(titre)
    if(' ' in titre):
        titreTab = titre.split(' ')
        for i in range(0,len(titreTab)):
            titreGeneral= titreGeneral +titreTab[i]+'_'
    
    return titreGeneral.lower()


def decoupeGeneralInformationEnOption(s,m,dict_maladies_bis,i):
    
    sentence =s.split(keywords[2])       
    for k in range(0,len(sentence)):
        sentenceBis =sentence[k].split(keywords[3]) 
        
        titre = sentenceBis[0]
        if(keywords[4] in titre):
            titre =titre.split(keywords[4])[1][:-1]
       
        if(keywords[8] in sentenceBis[1]):
            dict_maladies_bis[m]['opt_'+generateTitleKeys(titre)+str(i+1)]=titre
            
            sentenceOption = sentenceBis[1].split(keywords[8])
            for key in range(0,len(sentenceOption)):
                sentenceOptionBis =sentenceOption[key].split(keywords[9])
                titreOption = sentenceOptionBis[0]
                if(keywords[4] in sentenceOptionBis[0]):
                    titreOption =titreOption.split(keywords[4])[1]
                dict_maladies_bis[m][str(key)+'_'+titreOption]=sentenceOptionBis[1]     
        else:
            dict_maladies_bis[m]['opt_'+generateTitleKeys(titre)+str(i+1)]=sentenceBis[1]
            
            
tab_reponse =[]
def  decoupeQuestionReponseComplementSummary(m,s,i):
    sDecoupeQuestTab =s.split(keywords[5])
    dict_maladies_bis[m]['question_'+str(i+1)]=sDecoupeQuestTab[0]+keywords[5]
    
    summary =((dict_maladies_bis[m]['question_'+str(i+1)]).split('What is the')[1]).split('of?')[0]
    sentence =sDecoupeQuestTab[1].split(keywords[6])
    for k in range(0,len(sentence)):
        sentenceDecoupeSummaryRep =sentence[k].split(keywords[7])
        dict_maladies_bis[m]['reponse_'+str(i+1)+str(k)]=sentenceDecoupeSummaryRep[1]
        tab_reponse.append('reponse_'+str(i+1)+str(k))
        your_summary =sentenceDecoupeSummaryRep[0]
        if('\n' in your_summary):
            your_summary=sentenceDecoupeSummaryRep[0].split('\n')[1]
        if(len(sentenceDecoupeSummaryRep[2]) != 0):
            dict_maladies_bis[m]['summary_comment_'+str(i+1)+str(k)]='The '+ summary + ':'+your_summary+".\nMore details :\n"+sentenceDecoupeSummaryRep[2]
        else:
            dict_maladies_bis[m]['summary_comment_'+str(i+1)+str(k)]='The '+ summary + ':'+your_summary

def decoupeMaladieEnSousPartie(dict_maladies_bis,keyword,m,i):
    s= dict_maladies[m].split(keyword)
    decoupeGeneralInformationEnOption(s[0],m,dict_maladies_bis,i)
    decoupeQuestionReponseComplementSummary(m,s[1],i)


def generalDecoupeMaladieEnSousPartie():
    for i,m in enumerate(dict_maladies):
        
            
        dict_maladies_bis[m]={}
        if(keywords[0] in dict_maladies[m]):
            decoupeMaladieEnSousPartie(dict_maladies_bis,keywords[0],m,i)
           
                
generalDecoupeMaladieEnSousPartie()

with open('dict_maladies_bis.json', 'w+',encoding="utf-8") as fp:
    json.dump(dict_maladies_bis, fp, indent=4)

with open('helperFiles/modelEn.json', 'r+',errors='ignore',encoding='utf-8') as f:
    dict_modelEn = json.load(f)

dict_basicSansQstCom =dict_modelEn.copy()

#Part en.json
def ajoutQuestionRepMaladies(k):
    for ke in dict_maladies_bis[k]:
        dict_basicSansQstCom['keys'][ke]=''
        dict_basicSansQstCom['keys'][ke]=dict_maladies_bis[k][ke]

def generalAjoutQuestionRepMaladies():
    for k in dict_maladies_bis:
        ajoutQuestionRepMaladies(k)
            

generalAjoutQuestionRepMaladies()


keywords_livre =['introduction_to_complain','nature_of_patient','nature_of_symptoms','associated_symptoms','predisposing_factors','precipitating_and_aggravating_factors','ameliorating_factors','physical_findings','diagnostic_studies','less_common_diagnostic_considerations','questionnaire']
tabNomMaladieKey =[]

def ajoutKeysMaladies():    
    for key in range(0,len(keywords_livre)):
        titre =''
        if('_' in keywords_livre[key] ):
            titreOpt =keywords_livre[key].split('_')
            for t in range(0,len(titreOpt)):
                titre =titre+' '+titreOpt[t]
            dict_basicSansQstCom['keys'][titre[1:]]=titre
        else:
            dict_basicSansQstCom['keys'][keywords_livre[key]]=keywords_livre[key]
    for k,v in enumerate(dict_maladies_bis):
        dict_basicSansQstCom['keys'][v[0]+str(k+1)]=v
        tabNomMaladieKey.append(v[0]+str(k+1))
      
    
ajoutKeysMaladies()


alphabets =['A','B','C','D','E','F','H','I','M','N','P','S','U','V']
  
dict_decoupagekeyParAlphab={}
listingFourchetteNomMaladies=[]

def remplissageFourchette(keyAlphabet,alphabet):
    s =[]
    for key,value in enumerate(dict_maladies_bis):
        if(value[0]==alphabet.upper()):
            s.append(value[0]+str(key+1))
    dict_decoupagekeyParAlphab[str(s[0])+'-'+s[len(s)-1]]='Diseases that begin with the letter '+str(alphabet.upper())
    if(not str(s[0])+'-'+s[len(s)-1] in listingFourchetteNomMaladies ):
        listingFourchetteNomMaladies.append(str(s[0])+'-'+s[len(s)-1])
    

def remplissageFourchetteGenral():
    for keyAlphabet ,alphabet in enumerate(alphabets):
        remplissageFourchette(keyAlphabet,alphabet)

remplissageFourchetteGenral()


tab =[]

def ajoutFourchetteQuestionAlphabet():
    for key in dict_decoupagekeyParAlphab:
        dict_basicSansQstCom['keys'][key]= dict_decoupagekeyParAlphab[key]
        tab.append(key)
    dict_basicSansQstCom['keys']['question']='What\'s the first letter of the disease that you are looking for?'
    for alphabet in alphabets:
        dict_basicSansQstCom['keys'][alphabet]=alphabet

ajoutFourchetteQuestionAlphabet()
remplissageFourchetteGenral()


#Part 2 : questions.ts

tab_options =[]
dict_option={}  #contient tout ce qu'il faut  pour les sous option des option 

def tabOptions(k,v):
    for key,value in enumerate(dict_maladies_bis[v]):
        if('opt_' in value):
            tab_options.append(value)
            dict_option[value]=[]
        elif (value.split('_')[0]).isdigit() :
            dict_option[tab_options[len(tab_options)-1]].append(value)
    return dict_option



def generalTabOption():
    for k, v in enumerate(dict_maladies_bis):
        tabOptions(k,v)
        
generalTabOption()

#Part 2 developpement script questions.ts

with open('helperFiles/modelDico.json', 'r+',errors='ignore',encoding='utf-8') as f:
    dict_modelDico = json.load(f)

fileTs =open("questions.ts",'w+',encoding="utf-8")
fileTsBis =open("helperFiles/questionsBis.txt",'r+',encoding="utf-8")

def healperWrite_DictKey(dicto_copy):
    s='{\n'
    for key in dicto_copy:
        if(key !='options' and key!='category' and key!='nextQuestionMap'and key!='scoreMap'):
            s =s +'\t'+ str(key) +':'+"'"+str(dicto_copy[key])+"'"+',\n'
        else:
            s =s  +'\t'+ str(key) +':'+str(dicto_copy[key])+',\n'
    fileTs.write(s[:-2]+'\n},')


def listeAlphabetQuestTs():
    fileTs.truncate(0)
    fileTs.seek(0,0)
    fileTs.write(fileTsBis.read())
    fileTs.write('//*******************PARTIE QUESTOIN :CHOIX PREMIERE LETTERE DE LA MALADIE************\n')
    dict_modelDico_Bis =dict_modelDico.copy()
    dict_modelDico_Bis['id']='question'
    dict_modelDico_Bis['category']='CATEGORIES.PERSONAL'
    dict_modelDico_Bis['text']= dict_modelDico_Bis['id']
    dict_modelDico_Bis['options']=alphabets   
    dict_modelDico_Bis['nextQuestionMap']=listingFourchetteNomMaladies
    for i in range(0,len(listingFourchetteNomMaladies)):
        dict_modelDico_Bis['scoreMap'].append(0)
    healperWrite_DictKey(dict_modelDico_Bis)

listeAlphabetQuestTs()

def vidage_dict_basicTsHealper():
    for key in dict_modelDico:
        if(key !="options" and key !="nextQuestionMap" and key !="scoreMap"):
            dict_modelDico[key]=''
        else:
            dict_modelDico[key]=[]

def ajoutKeyDetailFourchette(id,nb1,nb2):
    vidage_dict_basicTsHealper()
    dict_bis =dict_modelDico.copy()
    fileTs.write('\n//PARTIE DECOUPAGE FOURCHETTE AVEC PREMIERE QUESTION POUR CHAQUE FORMULAIRE '+str(id)+'\n')
    dict_bis['id']=id
    dict_bis['category']='CATEGORIES.PERSONAL'
    dict_bis['text']=id
    for i in range(nb1,nb2+1):
        dict_bis['options'].append(id[0]+str(i))
    for i in range(nb1,nb2+1):
         dict_bis['nextQuestionMap'].append(id[0]+str(i))
    for j in range(0,len( dict_bis['options'])):
         dict_bis['scoreMap'].append(0) 
    dict_bis['inputType']= 'radio'
    healperWrite_DictKey(dict_bis)
    


# Ces deux fonctions ont pour but d'ajouter le detail la premiere qst du formu a afficher aprés le choix de maladie
def fourchetteEnumeration(id):
    sentences=id.split('-')
    s1=sentences[0].split(id[0])
    s2=sentences[1].split(id[0])
    nb1 =s1[1]
    nb2=s2[1]
    ajoutKeyDetailFourchette(id,int(nb1),int(nb2))

def generalFoucrchetteEnumeration():
    fileTs.write('\n//**********AJOUT DELIMITATION MALADIE ALPHABETS DE A.. a A..****************\n')
    for id in listingFourchetteNomMaladies:  
        fourchetteEnumeration(id)

generalFoucrchetteEnumeration()


keywords_livre_bis =['introduction to complain','nature of patient','nature of symptoms','associated symptoms','predisposing factors','precipitating and aggravating factors','ameliorating_factors','physical findings','diagnostic studies','less common diagnostic considerations','questionnaire']

def decoupeMaladieSousPartie(k,id,v):
    vidage_dict_basicTsHealper()
    dict_bis =dict_modelDico.copy()
    fileTs.write('\n//PARTIE MALADIE INTRO NATURE PATIENT, SYMPTHOMS ... '+str(id)+'\n')
    dict_bis['id']=id
    dict_bis['category']='CATEGORIES.PERSONAL'
    dict_bis['text']=id
    dict_bis['options']=[]
    dict_bis['nextQuestionMap']=[]
    for key in dict_maladies_bis[v]:
        for t in range(0,len(keywords_livre)):
            if(('opt_'+keywords_livre[t]) in key  ):
                dict_bis['options'].append(keywords_livre_bis[t])
                dict_bis['nextQuestionMap'].append(key)
            else:
                pass
    dict_bis['options'].append('questionnaire')
    dict_bis['nextQuestionMap'].append('question_'+str(k+1))
    dict_bis['inputType']= 'radio'
    for j in range(0,len( dict_bis['options'])):
        dict_bis['scoreMap'].append(0) 
    healperWrite_DictKey(dict_bis)


def generalDecoupeMaladieSousPartie():
    for k,v  in enumerate(dict_maladies_bis):
        decoupeMaladieSousPartie(k,v[0]+str(k+1),v)

generalDecoupeMaladieSousPartie()

tabBisSousOptBis =[]
def decoupeOptionSlash(tab):
    tabBisSousOpt =[]
    for k in range(0,len(tab)):
        s =tab[k].split('_')
        tabBisSousOpt.append(s[1])
        tabBisSousOptBis.append(s[1])
    # print(tabBisSousOptBis)
    return tabBisSousOpt

def ajoutSousOptionDesOptions():
    for key, value in enumerate(dict_option):
        vidage_dict_basicTsHealper()
        dict_bis =dict_modelDico.copy()
        fileTs.write('\n//PARTIE DECOUPE  OPTION MALADIE A LEURS SOUS OPTIONS ... '+str(value)+'\n')
        if(len(dict_option[value])!=0):
            dict_bis['id']=value
            dict_bis['category']='CATEGORIES.PERSONAL'
            dict_bis['text']=value
            dict_bis['options']=[]
            dict_bis['nextQuestionMap']=[]
            dict_bis['options'] = decoupeOptionSlash(dict_option[value])
            dict_bis['nextQuestionMap'] = dict_option[value]
        else:
            dict_bis['id']=value
            dict_bis['category']='CATEGORIES.PERSONAL'
            dict_bis['text']=value
            dict_bis['options']=[]
            dict_bis['nextQuestionMap']=[]
            dict_bis['options'].append('home')
            dict_bis['nextQuestionMap'].append('question')
        dict_bis['inputType']= 'radio'
        for j in range(0,len( dict_bis['options'])):
            dict_bis['scoreMap'].append(0) 
        healperWrite_DictKey(dict_bis)


ajoutSousOptionDesOptions()

def ajoutTabBisEn():
    for t in range(0,len(tabBisSousOptBis)):
        dict_basicSansQstCom['keys'][tabBisSousOptBis[t]]=tabBisSousOptBis[t]
    
ajoutTabBisEn()

with open('en.json', 'w+',encoding="utf-16") as fp:
    json.dump(dict_basicSansQstCom, fp, indent=4)

print("en.json ready !")


def ajoutSousOptionDetailTs():
    for key, value in enumerate(dict_option):
        if(len(dict_option[value]) != 0):
            for k in dict_option[value]:
                vidage_dict_basicTsHealper()
                dict_bis =dict_modelDico.copy()
                fileTs.write('\n//PARTIE  SOUS OPTIONS DES OPTIONS ... '+str(value)+'\n')
                dict_bis['id']=k
                dict_bis['category']='CATEGORIES.PERSONAL'
                dict_bis['text']=k
                dict_bis['options']=[]
                dict_bis['nextQuestionMap']=[]
                dict_bis['options'].append('home')
                dict_bis['nextQuestionMap'].append('question')
                dict_bis['inputType']= 'radio'
                for j in range(0,len( dict_bis['options'])):
                    dict_bis['scoreMap'].append(0) 
                healperWrite_DictKey(dict_bis)

ajoutSousOptionDetailTs()

def ajoutQuestionReponseTs(k,v):
    fileTs.write('\n//PARTIE  QUESTION REPONSE   ... '+str('question_'+str(k+1))+'\n')
    vidage_dict_basicTsHealper()
    dict_bis =dict_modelDico.copy()
    dict_bis['id']='question_'+str(k+1)
    dict_bis['category']='CATEGORIES.PERSONAL'
    dict_bis['text']='question_'+str(k+1)
    dict_bis['options']=[]
    dict_bis['nextQuestionMap']=[]
    for key in dict_maladies_bis[v]:
        
        if('reponse_'+str(k+1) in key):
            dict_bis['options'].append(key)
            dict_bis['nextQuestionMap'].append(key)
    dict_bis['inputType']= 'radio'
    for j in range(0,len( dict_bis['options'])):
        dict_bis['scoreMap'].append(0) 
    healperWrite_DictKey(dict_bis)
        
def generalAjoutQuestionReponseTs():
    for k,v in enumerate(dict_maladies_bis):
        ajoutQuestionReponseTs(k,v)

generalAjoutQuestionReponseTs()


def ajoutReponseSummary():
    for i in range(0,len(tab_reponse)):
        fileTs.write('\n//PARTIE  REPONSE SUMMARY  ... '+ tab_reponse[i] +'\n')
        vidage_dict_basicTsHealper()
        dict_bis =dict_modelDico.copy()
        dict_bis['id']=tab_reponse[i]
        dict_bis['category']='CATEGORIES.PERSONAL'
        dict_bis['text']=tab_reponse[i]
        dict_bis['options']=[]
        dict_bis['nextQuestionMap']=[]
        dict_bis['options'].append('show the summary')
    
        dict_bis['nextQuestionMap'].append(tab_reponse[i].split('_')[1])
       
        dict_bis['inputType']= 'radio'
        for j in range(0,len( dict_bis['options'])):
            dict_bis['scoreMap'].append(0) 
        healperWrite_DictKey(dict_bis)
    fileTs.write('\n ]')

ajoutReponseSummary()

print('questions.ts pret !')

f1 =open('partie1questionnaire.txt','r+',encoding='utf-8')
f2 =open('partie2questionnaire.txt','r+',encoding='utf-8')
q =open('questionnaire.tsx','w+',encoding='utf-8')
def automateFonctionApp():
    s='if('
    for k in range(0,len(tab_reponse)):
        s = s +'nextQuestionId == '+ "'" +str((tab_reponse[k].split('_'))[1]) + "'" +' ||'
    return s[:-2] +'){'



def remplirQustionnaireTsx():
    q.write(f1.read())
    q.write(automateFonctionApp())
    q.write(f2.read())

remplirQustionnaireTsx()





